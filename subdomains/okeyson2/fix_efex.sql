-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Apr 27, 2017 at 05:19 PM
-- Server version: 5.6.35
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `okeyson_2`
--

-- --------------------------------------------------------

--
-- Table structure for table `agents`
--

CREATE TABLE `agents` (
  `id` int(10) UNSIGNED NOT NULL,
  `operator_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `address` text COLLATE utf8_unicode_ci,
  `notes` text COLLATE utf8_unicode_ci,
  `park_id` int(11) DEFAULT NULL,
  `percentage` double(8,2) NOT NULL DEFAULT '0.00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `agent_transactions`
--

CREATE TABLE `agent_transactions` (
  `id` int(10) UNSIGNED NOT NULL,
  `operator_id` int(10) UNSIGNED NOT NULL,
  `agent_id` int(10) UNSIGNED NOT NULL,
  `type` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `value` double NOT NULL,
  `amount_paid` double NOT NULL,
  `approved` tinyint(1) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `banks`
--

CREATE TABLE `banks` (
  `id` int(10) UNSIGNED NOT NULL,
  `operator_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `acount_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `account_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `rank` int(11) NOT NULL DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `bookings`
--

CREATE TABLE `bookings` (
  `id` int(10) UNSIGNED NOT NULL,
  `operator_id` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `trip_id` int(10) UNSIGNED NOT NULL,
  `date` date NOT NULL,
  `passenger_count` int(11) NOT NULL,
  `unit_cost` double NOT NULL,
  `final_cost` double NOT NULL,
  `payment_method_id` int(10) UNSIGNED DEFAULT NULL,
  `bank_id` int(10) UNSIGNED DEFAULT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'PENDING',
  `source` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `paid_date` datetime DEFAULT NULL,
  `booking_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `contact_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contact_phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contact_email` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contact_address` text COLLATE utf8_unicode_ci,
  `gender` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `seat_no` int(11) NOT NULL,
  `next_of_kin` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `next_of_kin_phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `parent_booking_id` int(10) UNSIGNED DEFAULT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `daily_trip_id` int(10) UNSIGNED DEFAULT NULL,
  `receipt_no` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `passport_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `luggage_weight` double(8,2) NOT NULL DEFAULT '0.00',
  `luggage_cost` double NOT NULL DEFAULT '0',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_round_trip` tinyint(1) NOT NULL DEFAULT '0',
  `departure_time` varchar(40) CHARACTER SET utf8 DEFAULT NULL,
  `trip_type` int(11) DEFAULT '0',
  `return_date` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_flexi` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `bookings`
--

INSERT INTO `bookings` (`id`, `operator_id`, `trip_id`, `date`, `passenger_count`, `unit_cost`, `final_cost`, `payment_method_id`, `bank_id`, `status`, `source`, `paid_date`, `booking_code`, `contact_name`, `contact_phone`, `contact_email`, `contact_address`, `gender`, `seat_no`, `next_of_kin`, `next_of_kin_phone`, `parent_booking_id`, `user_id`, `daily_trip_id`, `receipt_no`, `passport_type`, `luggage_weight`, `luggage_cost`, `deleted_at`, `created_at`, `updated_at`, `is_round_trip`, `departure_time`, `trip_type`, `return_date`, `is_flexi`) VALUES
(52, 1, 137, '2017-04-25', 2, 7000, 14000, NULL, NULL, 'PENDING', NULL, NULL, 'OKY531351', 'Ifeoluwa', '08187393185', NULL, '', 'male', 6, '', '08079865906', NULL, 10, 21, '', NULL, 0.00, 0, NULL, '2017-04-25 09:46:29', '2017-04-25 09:46:29', 0, NULL, 0, '25 April, 2017', 0),
(51, 1, 137, '2017-04-25', 2, 7000, 14000, NULL, NULL, 'PENDING', NULL, NULL, 'OKY765879', 'Ifeoluwa', '08187393185', NULL, '', 'male', 6, '', '08079865906', NULL, 10, NULL, '', NULL, 0.00, 0, NULL, '2017-04-25 09:44:47', '2017-04-25 09:44:47', 0, NULL, 0, '25 April, 2017', 0),
(50, 1, 137, '2017-04-25', 2, 7000, 14000, NULL, NULL, 'PENDING', NULL, NULL, 'OKY177042', 'Ifeoluwa', '08187393185', NULL, '', 'male', 6, '', '08079865906', NULL, 10, NULL, '', NULL, 0.00, 0, NULL, '2017-04-25 09:43:28', '2017-04-25 09:43:28', 0, NULL, 0, '25 April, 2017', 0),
(49, 1, 137, '2017-04-25', 2, 7000, 14000, NULL, NULL, 'PENDING', NULL, NULL, 'OKY644920', 'Ifeoluwa', '08187393185', NULL, '', 'male', 6, '', '08079865906', NULL, 10, NULL, '', NULL, 0.00, 0, NULL, '2017-04-25 09:41:15', '2017-04-25 09:41:15', 0, NULL, 0, '25 April, 2017', 0),
(46, 1, 137, '2017-04-25', 2, 7000, 14000, NULL, NULL, 'PENDING', NULL, NULL, 'OKY476121', 'Ifeoluwa', '08187393185', NULL, '', 'male', 6, '', '08079865906', NULL, 10, NULL, '', NULL, 0.00, 0, NULL, '2017-04-25 09:39:18', '2017-04-25 09:39:18', 0, NULL, 0, '25 April, 2017', 0),
(47, 1, 137, '2017-04-25', 2, 7000, 14000, NULL, NULL, 'PENDING', NULL, NULL, 'OKY798358', 'Ifeoluwa', '08187393185', NULL, '', 'male', 6, '', '08079865906', NULL, 10, NULL, '', NULL, 0.00, 0, NULL, '2017-04-25 09:39:55', '2017-04-25 09:39:55', 0, NULL, 0, '25 April, 2017', 0),
(48, 1, 137, '2017-04-25', 2, 7000, 14000, NULL, NULL, 'PENDING', NULL, NULL, 'OKY931091', 'Ifeoluwa', '08187393185', NULL, '', 'male', 6, '', '08079865906', NULL, 10, NULL, '', NULL, 0.00, 0, NULL, '2017-04-25 09:40:22', '2017-04-25 09:40:22', 0, NULL, 0, '25 April, 2017', 0),
(39, 1, 114, '2017-04-22', 1, 5000, 5000, 2, NULL, 'PAID', NULL, '2017-04-21 16:04:06', 'OKY136389', 'ify', '08065511282', NULL, '', 'female', 2, '', '08027610615', NULL, 10, 19, '', NULL, 0.00, 0, NULL, '2017-04-21 15:55:25', '2017-04-21 15:56:06', 0, NULL, 0, '22-04-2017', 0),
(38, 1, 114, '2017-04-22', 2, 5000, 10500, 2, NULL, 'PAID', NULL, '2017-04-21 16:46:05', 'OKY636936', 'ify', '08065511282', NULL, '', 'female', 6, '', '08027610615', NULL, 10, 19, '', NULL, 30.00, 500, NULL, '2017-04-21 15:46:05', '2017-04-21 15:46:05', 0, NULL, 0, '2017-04-22', 0),
(37, 1, 114, '2017-04-22', 2, 5000, 10000, 2, NULL, 'PAID', NULL, '2017-04-21 14:54:19', 'OKY784792', 'Winny', '08187393184', NULL, '', 'female', 5, '', '08187393185', NULL, 10, 19, '', NULL, 0.00, 0, NULL, '2017-04-21 13:54:19', '2017-04-21 13:54:19', 0, NULL, 0, '2017-04-22', 0),
(36, 1, 114, '2017-04-22', 3, 5000, 15000, 2, NULL, 'PAID', NULL, '2017-04-21 14:04:58', 'OKY557149', 'Kayode', '08079865906', NULL, '', 'male', 8, '', '08187393185', NULL, 10, 19, '', NULL, 0.00, 0, NULL, '2017-04-21 13:47:51', '2017-04-21 13:47:58', 0, NULL, 0, '22 April, 2017', 0),
(35, 1, 114, '2017-04-22', 3, 5000, 15500, 2, NULL, 'PAID', NULL, '2017-04-21 14:04:46', 'OKY751241', 'Ifeoluwa', '08187393185', NULL, '', 'male', 10, '', '08079865906', NULL, 10, 19, '', NULL, 40.00, 500, NULL, '2017-04-21 13:46:35', '2017-04-21 13:46:46', 0, NULL, 0, '22 April, 2017', 0),
(53, 1, 137, '2017-04-25', 2, 7000, 14000, 2, NULL, 'PAID', NULL, '2017-04-25 10:54:41', 'OKY851028', 'Ifeoluwa', '08187393185', NULL, '', 'male', 10, '', '08079865906', NULL, 10, 21, '', NULL, 0.00, 0, NULL, '2017-04-25 09:54:41', '2017-04-25 09:54:41', 0, NULL, 0, '2017-04-25', 0);

-- --------------------------------------------------------

--
-- Table structure for table `booking_notes`
--

CREATE TABLE `booking_notes` (
  `id` int(10) UNSIGNED NOT NULL,
  `operator_id` int(10) UNSIGNED NOT NULL,
  `booking_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `notes` text COLLATE utf8_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `booking_refunds`
--

CREATE TABLE `booking_refunds` (
  `id` int(10) UNSIGNED NOT NULL,
  `operator_id` int(10) UNSIGNED NOT NULL,
  `booking_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `booking_amount` double NOT NULL,
  `refunded_amount` double NOT NULL,
  `comment` text COLLATE utf8_unicode_ci NOT NULL,
  `approved` tinyint(1) NOT NULL DEFAULT '0',
  `approved_user_id` int(10) UNSIGNED NOT NULL,
  `approved_date` datetime DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `buses`
--

CREATE TABLE `buses` (
  `id` int(10) UNSIGNED NOT NULL,
  `operator_id` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `bus_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `number_plate` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `chassis_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `engine_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `no_of_seats` int(11) DEFAULT NULL,
  `bus_type_id` int(10) UNSIGNED NOT NULL,
  `vin` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `other_details` text COLLATE utf8_unicode_ci NOT NULL,
  `default_park_id` int(10) UNSIGNED DEFAULT NULL,
  `default_driver_id` int(10) UNSIGNED DEFAULT NULL,
  `default_trip_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `bus_mileage` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `buses`
--

INSERT INTO `buses` (`id`, `operator_id`, `bus_number`, `number_plate`, `chassis_number`, `engine_number`, `no_of_seats`, `bus_type_id`, `vin`, `other_details`, `default_park_id`, `default_driver_id`, `default_trip_id`, `created_at`, `updated_at`, `bus_mileage`) VALUES
(1, 1, '001', 'EKY586XU', 'G660360', '3TZ200367', 15, 7, '', '', 1, 1, NULL, '2017-04-03 13:33:14', '2017-04-03 13:33:14', NULL),
(2, 1, '002', 'AKD287XT', 'G660347', '3TZ2003521', 15, 7, '', '', 1, 2, NULL, '2017-04-03 14:03:31', '2017-04-03 14:03:31', NULL),
(3, 1, '003', 'AAA209XR', 'JTGSX23P1EG148965', '8652046', 15, 2, '', '', 1, 3, NULL, '2017-04-03 15:11:09', '2017-04-03 15:11:09', NULL),
(4, 1, '004', 'SMK200XT', 'JTGSX23PID6137611', '2TR8514713', 15, 2, '', '', 1, 4, NULL, '2017-04-03 15:12:13', '2017-04-03 15:12:13', NULL),
(5, 1, '005', 'LND829XT', 'JTGSX23PF6161072', '2TR9006649', 15, 2, '', '', 1, 5, NULL, '2017-04-03 15:13:18', '2017-04-03 15:13:18', NULL),
(6, 1, '006', 'AGL64XS', 'JTFS22P106159799', '2TR9000303', 15, 2, '', '', 1, 6, NULL, '2017-04-03 15:14:17', '2017-04-03 15:14:17', NULL),
(7, 1, '007', 'KTU878XU', 'LSUAFL2P4EGG660363', '3TZ003424', 15, 7, '', '', 1, 7, NULL, '2017-04-03 15:15:20', '2017-04-03 15:15:20', NULL),
(8, 1, '008', 'EKY587XU', 'LSUPTT2PE866038', '3TZ003411', 15, 7, '', '', 1, 8, NULL, '2017-04-03 15:16:21', '2017-04-03 15:16:21', NULL),
(9, 1, '009', 'SMK132XT', 'JTGSX23E6157661', '2TR9048849', 15, 2, '', '', 1, 9, NULL, '2017-04-03 15:17:30', '2017-04-03 15:17:30', NULL),
(10, 1, '010', 'FKJ649SK', 'JTFSX2234D157464', '2TR8714912', 15, 2, '', '', 1, 10, NULL, '2017-04-03 15:18:28', '2017-04-03 15:18:28', NULL),
(11, 1, '011', 'BDG257XT', 'LSUAF2P8EG660', '3TRZ003505', 15, 7, '', '', 1, 11, NULL, '2017-04-03 15:38:13', '2017-04-03 15:38:13', NULL),
(12, 1, '012', 'GGE102XS', 'JTFSX22PX06106112', '2TR9002276', 15, 2, '', '', 1, 12, NULL, '2017-04-03 15:39:35', '2017-04-03 15:39:35', NULL),
(13, 1, '013', 'FKJ182XQ', 'JTGSX2POE6153543', '2TR8692758', 15, 2, '', '', 1, 13, NULL, '2017-04-03 15:40:35', '2017-04-03 15:41:19', NULL),
(14, 1, '014', 'AKD58XU', 'LSUAFL2E7EG660356', '3TZ00351', 15, 7, '', '', 1, 14, NULL, '2017-04-03 15:42:27', '2017-04-03 15:42:27', NULL),
(15, 1, '015', 'KTU877XU', 'LSUAL2PSEG660369', '3TZ003471', 15, 7, '', '', 1, 15, NULL, '2017-04-03 15:43:39', '2017-04-03 15:43:39', NULL),
(16, 1, '016', 'LND720XP', '6151300', '8673567', 15, 2, '', '', 1, 16, NULL, '2017-04-03 15:44:49', '2017-04-03 15:44:49', NULL),
(17, 1, '017', 'KTU705ST', 'JTGSX23P3G6168458', '2TR9053899', 15, 2, '', '', 1, 17, NULL, '2017-04-03 15:47:39', '2017-04-03 15:47:39', NULL),
(18, 1, '018', 'AGL453SR', '606158471', '2TR8720410', 15, 2, '', '', 1, 18, NULL, '2017-04-03 15:49:04', '2017-04-03 15:49:04', NULL),
(19, 1, '019', 'KTU683XS', '6158227', '2TR8719244', 15, 2, '', '', 1, 19, NULL, '2017-04-03 15:50:26', '2017-04-03 15:50:26', NULL),
(20, 1, '020', 'LSD601XR', 'D614025', '2TR8543098', 15, 2, '', '', 1, 20, NULL, '2017-04-03 15:52:01', '2017-04-03 15:52:01', NULL),
(21, 1, '021', 'KJA389XU', 'JTGSX23P5G6169188', '2TR9057623', 15, 2, '', '', 1, 21, 0, '2017-04-03 15:53:38', '2017-04-03 15:54:27', NULL),
(22, 1, '022', 'LSD112XQ', 'LSUAFLSP4EG660413', '3TZ003533', 15, 7, '', '', 1, 22, NULL, '2017-04-03 15:55:47', '2017-04-03 15:55:47', NULL),
(23, 1, '023', 'KSF628XU', 'JTGSX23P976054001', '2TR8171723', 15, 2, '', '', 1, 0, 0, '2017-04-03 15:57:00', '2017-04-03 15:57:22', NULL),
(24, 1, '024', 'AKD582XU', 'LXUARP8EG6603X8', '3TZ003536', 15, 7, '', '', 1, 23, NULL, '2017-04-03 15:58:38', '2017-04-03 15:58:38', NULL),
(25, 1, '025', 'KSF229XU', 'JTFSX22P506157487', '2TR8715506', 15, 2, '', '', 1, 24, NULL, '2017-04-03 16:14:51', '2017-04-03 16:14:51', NULL),
(26, 1, '026', 'GGE427XR', 'JTFX22PX06158912', '2TR8722325', 15, 2, '', '', 1, 25, NULL, '2017-04-03 16:17:23', '2017-04-03 16:17:23', NULL),
(27, 1, '027', 'KSF98XR', 'G660361', '3TZ003451', 15, 7, '', '', 1, 25, NULL, '2017-04-03 16:19:02', '2017-04-03 16:19:02', NULL),
(28, 1, '028', 'KSF230XU', 'JTFSX22P706157474', '2TR8714911', 15, 2, '', '', 1, 27, NULL, '2017-04-03 16:21:07', '2017-04-03 16:21:07', NULL),
(29, 1, '029', 'KJA830XP', '615478', '2TR8702469', 15, 2, '', '', 1, 28, NULL, '2017-04-03 16:23:12', '2017-04-03 16:23:12', NULL),
(30, 1, '030', 'KRD445XS', '6159737', '2TR9000689', 15, 2, '', '', 1, 29, NULL, '2017-04-03 17:04:37', '2017-04-03 17:04:37', NULL),
(31, 1, '', 'KRD446XS', '6159799', '2TR9000303', 15, 2, '', '', 1, 30, NULL, '2017-04-03 17:08:23', '2017-04-03 17:08:23', NULL),
(32, 1, '030', 'KJA989XS', 'JTFSX22PA406159795', '2TR9000375', 15, 2, '', '', 1, 31, NULL, '2017-04-03 17:20:12', '2017-04-03 17:20:12', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `bus_types`
--

CREATE TABLE `bus_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `operator_id` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `show` tinyint(1) NOT NULL DEFAULT '1',
  `no_of_seats` int(11) NOT NULL DEFAULT '14',
  `no_of_columns` int(11) NOT NULL DEFAULT '4',
  `no_of_rows` int(11) NOT NULL DEFAULT '4',
  `seat_width` int(11) NOT NULL DEFAULT '125',
  `seat_height` int(11) NOT NULL DEFAULT '45',
  `holder_width` int(11) NOT NULL DEFAULT '573',
  `holder_height` int(11) NOT NULL DEFAULT '317',
  `no_seats` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '-1, 0',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `bus_types`
--

INSERT INTO `bus_types` (`id`, `operator_id`, `name`, `show`, `no_of_seats`, `no_of_columns`, `no_of_rows`, `seat_width`, `seat_height`, `holder_width`, `holder_height`, `no_seats`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 1, 'NISSAN URVAN', 1, 15, 4, 4, 125, 45, 573, 317, '-1, 0', NULL, NULL, NULL),
(2, 1, 'TOYOTA', 1, 15, 4, 4, 125, 45, 573, 317, '-1, 0', NULL, NULL, NULL),
(3, 1, 'JOYLONG', 1, 15, 4, 4, 125, 45, 573, 317, '-1, 0', NULL, NULL, NULL),
(4, 1, 'NISSAN CIVILIAN', 1, 30, 8, 5, 125, 45, 645, 337, '-1, 0', NULL, NULL, NULL),
(5, 1, 'MCV BENZ', 1, 30, 8, 5, 125, 45, 645, 337, '-1, 0', NULL, NULL, NULL),
(6, 1, 'NISSAN CIVILIAN2', 1, 15, 4, 4, 125, 45, 573, 317, '-1, 0', NULL, NULL, NULL),
(7, 1, 'CHANA', 1, 15, 4, 4, 125, 45, 573, 317, '-1, 0', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `call_logs`
--

CREATE TABLE `call_logs` (
  `id` int(10) UNSIGNED NOT NULL,
  `operator_id` int(10) UNSIGNED NOT NULL,
  `booking_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `details` text COLLATE utf8_unicode_ci NOT NULL,
  `duration` int(11) DEFAULT NULL,
  `call_date` datetime DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cash_offices`
--

CREATE TABLE `cash_offices` (
  `id` int(10) UNSIGNED NOT NULL,
  `operator_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contact_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` text COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `charter_bookings`
--

CREATE TABLE `charter_bookings` (
  `id` int(10) UNSIGNED NOT NULL,
  `operator_id` int(10) UNSIGNED NOT NULL,
  `booking_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `travel_date` date NOT NULL,
  `pickup_location` text COLLATE utf8_unicode_ci NOT NULL,
  `destination` text COLLATE utf8_unicode_ci NOT NULL,
  `park_id` int(10) UNSIGNED DEFAULT NULL,
  `agreed_price` double DEFAULT NULL,
  `bus_id` int(10) UNSIGNED DEFAULT NULL,
  `driver_id` int(10) UNSIGNED DEFAULT NULL,
  `user_id` int(10) NOT NULL,
  `contact_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contact_phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contact_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `number_of_passengers` int(11) NOT NULL DEFAULT '0',
  `status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `paid_date` date DEFAULT NULL,
  `payment_method_id` int(10) UNSIGNED DEFAULT NULL,
  `receipt_no` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `comment` text COLLATE utf8_unicode_ci,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `charter_bookings`
--

INSERT INTO `charter_bookings` (`id`, `operator_id`, `booking_code`, `travel_date`, `pickup_location`, `destination`, `park_id`, `agreed_price`, `bus_id`, `driver_id`, `user_id`, `contact_name`, `contact_phone`, `contact_email`, `number_of_passengers`, `status`, `paid_date`, `payment_method_id`, `receipt_no`, `comment`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 0, 'OKY996093', '2017-04-06', 'VI', 'Ibadan', 1, 4000, 2, 2, 10, 'Kingsley', '08187393185', NULL, 2, 'PENDING', '1970-01-01', 2, '', '', NULL, '2017-04-06 10:41:39', '2017-04-06 10:41:39');

-- --------------------------------------------------------

--
-- Table structure for table `daily_trips`
--

CREATE TABLE `daily_trips` (
  `id` int(10) UNSIGNED NOT NULL,
  `operator_id` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `trip_date` date NOT NULL,
  `trip_id` int(10) UNSIGNED NOT NULL,
  `driver_id` int(10) UNSIGNED DEFAULT NULL,
  `bus_id` int(10) UNSIGNED DEFAULT NULL,
  `booked_seats` int(11) NOT NULL DEFAULT '0',
  `total_seats` int(11) NOT NULL DEFAULT '0',
  `total_fare` double NOT NULL DEFAULT '0',
  `driver_allowance` double NOT NULL DEFAULT '0',
  `other_expense` double NOT NULL DEFAULT '0',
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'LOADING',
  `departure_time` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `trip_transit` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ticketer_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `daily_trips`
--

INSERT INTO `daily_trips` (`id`, `operator_id`, `trip_date`, `trip_id`, `driver_id`, `bus_id`, `booked_seats`, `total_seats`, `total_fare`, `driver_allowance`, `other_expense`, `status`, `departure_time`, `trip_transit`, `ticketer_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(21, 1, '2017-04-25', 19, 19, 19, 4, 15, 28000, 0, 0, 'LOADING', NULL, 'First Bus', NULL, '2017-04-25 09:46:29', '2017-04-25 09:54:41', NULL),
(19, 1, '2017-04-22', 16, 16, 16, 11, 15, 56000, 0, 0, 'TRANSITED', NULL, 'First Bus', NULL, '2017-04-21 13:46:35', '2017-04-21 15:55:25', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `daily_trip_expenses`
--

CREATE TABLE `daily_trip_expenses` (
  `id` int(10) UNSIGNED NOT NULL,
  `daily_trip_id` int(11) NOT NULL,
  `expense_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `amount` double NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `daily_trip_expenses`
--

INSERT INTO `daily_trip_expenses` (`id`, `daily_trip_id`, `expense_name`, `amount`, `created_at`, `updated_at`) VALUES
(19, 19, 'Drivers allocation', 500, '2017-04-21 14:35:19', '2017-04-21 14:35:19');

-- --------------------------------------------------------

--
-- Table structure for table `devices`
--

CREATE TABLE `devices` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `assigned_to` int(10) UNSIGNED DEFAULT NULL,
  `operator_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `serial_number` text COLLATE utf8_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `discounts`
--

CREATE TABLE `discounts` (
  `id` int(10) UNSIGNED NOT NULL,
  `operator_id` int(10) UNSIGNED NOT NULL,
  `age_one` int(11) NOT NULL,
  `age_two` int(11) NOT NULL,
  `discount` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `discounts`
--

INSERT INTO `discounts` (`id`, `operator_id`, `age_one`, `age_two`, `discount`, `created_at`, `updated_at`) VALUES
(1, 1, 0, 4, 100, '2017-04-12 11:24:20', '2017-04-12 11:24:20'),
(2, 1, 5, 9, 50, '2017-04-12 11:24:43', '2017-04-12 11:24:43');

-- --------------------------------------------------------

--
-- Table structure for table `drivers`
--

CREATE TABLE `drivers` (
  `id` int(10) UNSIGNED NOT NULL,
  `operator_id` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone_2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8_unicode_ci NOT NULL,
  `default_park_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `drivers`
--

INSERT INTO `drivers` (`id`, `operator_id`, `name`, `phone`, `phone_2`, `address`, `default_park_id`, `created_at`, `updated_at`) VALUES
(1, 1, 'JIDEOFOR', '08080663012', '', '', 1, '2017-04-03 13:29:28', '2017-04-03 13:29:28'),
(2, 1, 'IYKE', '08100035667', '', '', 1, '2017-04-03 14:02:10', '2017-04-03 14:02:10'),
(3, 1, 'OKEY UMEDU', '07037364275', '', '', 1, '2017-04-03 15:05:38', '2017-04-03 15:05:38'),
(4, 1, 'NNAMDI', '07036168168', '', '', 1, '2017-04-03 15:06:10', '2017-04-03 15:06:10'),
(5, 1, 'IJELE', '07036826333', '', '', 1, '2017-04-03 15:06:52', '2017-04-03 15:06:52'),
(6, 1, 'DE-LAW', '09098708800', '', '', 1, '2017-04-03 15:07:30', '2017-04-03 15:07:30'),
(7, 1, 'EMEKA', '08148345702', '', '', 1, '2017-04-03 15:07:58', '2017-04-03 15:07:58'),
(8, 1, 'SEGUN', '09075604869', '', '', 1, '2017-04-03 15:08:27', '2017-04-03 15:08:27'),
(9, 1, 'INNOCENT', '08060662072', '', '', 1, '2017-04-03 15:09:09', '2017-04-03 15:09:09'),
(10, 1, 'OKADIGBO', '07089979003', '', '', 1, '2017-04-03 15:09:39', '2017-04-03 15:09:39'),
(11, 1, 'INSPECTOR', '07031367950', '', '', 1, '2017-04-03 15:22:49', '2017-04-03 15:22:49'),
(12, 1, 'NWACHWUKWU', '08139283838', '', '', 1, '2017-04-03 15:23:31', '2017-04-03 15:23:31'),
(13, 1, 'OKEY', '08103071610', '', '', 1, '2017-04-03 15:24:07', '2017-04-03 15:24:07'),
(14, 1, 'PETER DE ROCK', '08103071610', '', '', 1, '2017-04-03 15:24:39', '2017-04-03 15:24:39'),
(15, 1, 'FRANK', '07037744072', '', '', 1, '2017-04-03 15:25:20', '2017-04-03 15:25:20'),
(16, 1, 'ISAAC', '08036099989', '', '', 1, '2017-04-03 15:25:58', '2017-04-03 15:25:58'),
(17, 1, 'UBA', '08033952970', '', '', 1, '2017-04-03 15:26:29', '2017-04-03 15:26:29'),
(18, 1, 'NZE', '07032319965', '', '', 1, '2017-04-03 15:27:06', '2017-04-03 15:27:06'),
(19, 1, 'PETER AWGU', '08177204842', '', '', 1, '2017-04-03 15:27:37', '2017-04-03 15:27:37'),
(20, 1, 'DENNIS', '08163180071', '', '', 1, '2017-04-03 15:28:12', '2017-04-03 15:28:12'),
(21, 1, 'EDMUND', '08032409029', '', '', 1, '2017-04-03 15:28:51', '2017-04-03 15:28:51'),
(22, 1, 'EBERE', '08035309231', '', '', 1, '2017-04-03 15:29:22', '2017-04-03 15:29:22'),
(23, 1, 'OTUMBA', '08077039035', '', '', 1, '2017-04-03 15:29:59', '2017-04-03 15:29:59'),
(24, 1, 'SHARON', '08036371804', '', '', 1, '2017-04-03 15:31:13', '2017-04-03 15:31:13'),
(25, 1, 'FEDERAL', '08033855529', '', '', 1, '2017-04-03 15:31:46', '2017-04-03 15:31:46'),
(26, 1, 'EDDY', '07064445979', '', '', 1, '2017-04-03 15:32:26', '2017-04-03 15:32:26'),
(27, 1, 'ORJI', '09035008048', '', '', 1, '2017-04-03 15:33:10', '2017-04-03 15:33:10'),
(28, 1, 'M.DOLLAR', '08030813052', '', '', 1, '2017-04-03 15:33:50', '2017-04-03 15:33:50'),
(29, 1, 'CHUKWUDI', '08133119900', '', '', 1, '2017-04-03 15:34:29', '2017-04-03 15:34:29'),
(30, 1, 'CHINEDU ABAGANA', '07069230068', '', '', 1, '2017-04-03 15:35:17', '2017-04-03 15:35:17'),
(31, 1, 'CHUKWUMA NATHWEST', '08038448790', '', '', 1, '2017-04-03 15:35:51', '2017-04-03 15:35:51');

-- --------------------------------------------------------

--
-- Table structure for table `extra_luggages`
--

CREATE TABLE `extra_luggages` (
  `id` int(10) UNSIGNED NOT NULL,
  `booking_id` int(11) NOT NULL,
  `luggage_weight` double NOT NULL DEFAULT '0',
  `amount` double NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `extra_luggages`
--

INSERT INTO `extra_luggages` (`id`, `booking_id`, `luggage_weight`, `amount`, `created_at`, `updated_at`) VALUES
(3, 37, 40, 1500, '2017-04-21 13:56:37', '2017-04-21 13:56:37'),
(4, 35, 50, 1500, '2017-04-21 13:57:04', '2017-04-21 13:57:04'),
(5, 39, 20, 1000, '2017-04-21 16:00:39', '2017-04-21 16:00:39');

-- --------------------------------------------------------

--
-- Table structure for table `incidents`
--

CREATE TABLE `incidents` (
  `id` int(10) UNSIGNED NOT NULL,
  `operator_id` int(10) UNSIGNED NOT NULL,
  `incident_type_id` int(10) UNSIGNED NOT NULL,
  `details` text COLLATE utf8_unicode_ci,
  `park_id` int(11) DEFAULT NULL,
  `bus_id` int(11) DEFAULT NULL,
  `expense_amount` double NOT NULL DEFAULT '0',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `incidents`
--

INSERT INTO `incidents` (`id`, `operator_id`, `incident_type_id`, `details`, `park_id`, `bus_id`, `expense_amount`, `deleted_at`, `created_at`, `updated_at`) VALUES
(2, 1, 1, 'biuvyfe eifewofoe doiW DWKEDOWEIJ', 1, 1, 0, NULL, '2017-04-19 08:29:02', '2017-04-19 08:29:02');

-- --------------------------------------------------------

--
-- Table structure for table `incident_types`
--

CREATE TABLE `incident_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `operator_id` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `details` text COLLATE utf8_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `incident_types`
--

INSERT INTO `incident_types` (`id`, `operator_id`, `name`, `details`, `is_active`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 1, 'Body work', 'Body work', 1, NULL, '2016-09-21 13:05:36', '2016-09-21 13:05:36'),
(2, 1, 'Maintenance', 'Routine', 1, NULL, '2016-10-25 05:03:42', '2016-10-25 05:03:42'),
(3, 1, 'Faulty Tires', 'Bitrate	AVGPsnr	GLBPsnr	AVPsnrP	GLPsnrP	VPXSSIM	  Time(us)\r\n689.521	 42.678	 42.174	 42.754	 42.239	 82.524	 1575048', 1, NULL, '2017-04-19 08:20:23', '2017-04-19 08:20:23');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` int(10) UNSIGNED NOT NULL,
  `operator_id` int(10) UNSIGNED NOT NULL,
  `sender_id` int(10) UNSIGNED NOT NULL,
  `recipient_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `message_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `read` tinyint(1) NOT NULL DEFAULT '0',
  `read_date` datetime DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2016_10_17_123336_create_agent_transactions_table', 0),
('2016_10_17_123336_create_agents_table', 0),
('2016_10_17_123336_create_banks_table', 0),
('2016_10_17_123336_create_booking_notes_table', 0),
('2016_10_17_123336_create_booking_refunds_table', 0),
('2016_10_17_123336_create_bookings_table', 0),
('2016_10_17_123336_create_bus_types_table', 0),
('2016_10_17_123336_create_buses_table', 0),
('2016_10_17_123336_create_call_logs_table', 0),
('2016_10_17_123336_create_cash_offices_table', 0),
('2016_10_17_123336_create_charter_bookings_table', 0),
('2016_10_17_123336_create_daily_trips_table', 0),
('2016_10_17_123336_create_devices_table', 0),
('2016_10_17_123336_create_drivers_table', 0),
('2016_10_17_123336_create_failed_queue_jobs_table', 0),
('2016_10_17_123336_create_incident_types_table', 0),
('2016_10_17_123336_create_incidents_table', 0),
('2016_10_17_123336_create_messages_table', 0),
('2016_10_17_123336_create_operators_table', 0),
('2016_10_17_123336_create_park_stops_table', 0),
('2016_10_17_123336_create_parks_table', 0),
('2016_10_17_123336_create_passengers_table', 0),
('2016_10_17_123336_create_password_resets_table', 0),
('2016_10_17_123336_create_payment_methods_table', 0),
('2016_10_17_123336_create_queue_jobs_table', 0),
('2016_10_17_123336_create_roles_table', 0),
('2016_10_17_123336_create_seats_table', 0),
('2016_10_17_123336_create_settings_table', 0),
('2016_10_17_123336_create_states_table', 0),
('2016_10_17_123336_create_sub_trips_table', 0),
('2016_10_17_123336_create_transactions_table', 0),
('2016_10_17_123336_create_trip_stops_table', 0),
('2016_10_17_123336_create_trips_table', 0),
('2016_10_17_123336_create_user_logs_table', 0),
('2016_10_17_123336_create_users_table', 0),
('2016_10_17_123336_create_users_destinations_table', 0),
('2016_10_17_123336_create_users_parks_table', 0),
('2016_10_17_123356_create_daily_trip_expenses_table', 1),
('2016_10_17_132156_add_intl_fields_to_trips_table', 2),
('2016_10_17_143836_add_passport_type_to_bookings_table', 3),
('2016_10_17_150840_add_luggage_fields_to_bookings_table', 4),
('2016_10_18_140559_move_intl_so_subtrips', 5),
('2016_10_19_130523_add_soft_delete_to_daily_trips', 6),
('2016_10_20_010301_add_extra_luggages', 7),
('2016_10_28_180052_add_holder_dimen_to_bus_types', 7),
('2017_02_20_151020_add_operater_id_to_agents_table', 8),
('2017_02_20_151831_add_operater_id_to_agent_transactions_table', 9),
('2017_02_20_151948_add_operater_id_to_banks_table', 9),
('2017_02_20_152045_add_operater_id_to_bookings_table', 9),
('2017_02_20_152151_add_operater_id_to_booking_notes_table', 9),
('2017_02_20_152242_add_operater_id_to_booking_refunds_table', 9),
('2017_02_20_152335_add_operater_id_to_buses_table', 9),
('2017_02_20_152437_add_operater_id_to_bus_types_table', 9),
('2017_02_20_152529_add_operater_id_to_call_logs_table', 9),
('2017_02_20_152628_add_operater_id_to_cash_offices_table', 9),
('2017_02_20_152727_add_operater_id_to_charter_bookings_table', 9),
('2017_02_20_152825_add_operater_id_to_daily_trips_table', 9),
('2017_02_20_153235_add_operater_id_to_drivers_table', 10),
('2017_02_20_153512_add_operater_id_to_failed_queue_jobs_table', 11),
('2017_02_20_153609_add_operater_id_to_incidents_table', 11),
('2017_02_20_153715_add_operater_id_to_incident_types_table', 11),
('2017_02_20_153801_add_operater_id_to_messages_table', 11),
('2017_02_20_153907_add_operater_id_to_parks_table', 11),
('2017_02_20_153957_add_operater_id_to_park_stops_table', 11),
('2017_02_20_154110_add_operater_id_to_passengers_table', 11),
('2017_02_20_154814_add_operater_id_to_queue_jobs_table', 12),
('2017_02_20_154944_add_operater_id_to_roles_table', 12),
('2017_02_20_155037_add_operater_id_to_seats_table', 12),
('2017_02_20_155158_add_operater_id_to_settings_table', 12),
('2017_02_20_155433_add_operater_id_to_states_table', 12),
('2017_02_20_155908_add_operater_id_to_transactions_table', 12),
('2017_02_20_160055_add_operater_id_to_trip_stops_table', 12),
('2017_02_20_160237_add_operater_id_to_users_table', 12),
('2017_02_20_160320_add_operater_id_to_users_destinations_table', 12),
('2017_02_20_160513_add_operater_id_to_users_parks_table', 12),
('2017_02_20_160655_add_operater_id_to_user_logs_table', 12),
('2017_03_25_090905_add_phone_2_column_to_drivers_table', 13),
('2017_03_25_091524_add_columns_to_buses_table', 14),
('2017_03_25_091716_add_phone_2_column_to_drivers_table', 14),
('2017_03_25_094643_add_phone_2_column_to_drivers_table', 15),
('2017_03_27_132548_update_settings_table', 16),
('2017_03_27_134511_update_settings_table', 17),
('2017_03_27_134829_add_operator_id_settings_table', 18),
('2017_03_30_122721_update_subtrips_table', 19),
('2017_04_09_133258_create_discounts_table', 20),
('2017_04_10_104803_add_track_discount_settings_table', 21),
('2017_04_11_070351_update_bookings_table', 22),
('2017_04_11_080858_update_bookingsww_table', 23),
('2017_04_11_110117_add_paymentmethod_table', 24),
('2017_04_20_111435_add_seat_no_bookings_table', 25),
('2017_04_20_121400_add_seat_no_passengers_table', 26);

-- --------------------------------------------------------

--
-- Table structure for table `operators`
--

CREATE TABLE `operators` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `access_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8_unicode_ci NOT NULL,
  `contact_person` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `details` text COLLATE utf8_unicode_ci NOT NULL,
  `website` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `commission` double(8,2) DEFAULT NULL,
  `img` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `portal_link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `operators`
--

INSERT INTO `operators` (`id`, `name`, `access_code`, `code`, `address`, `contact_person`, `phone`, `email`, `details`, `website`, `commission`, `img`, `portal_link`, `active`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'OKEYSON', '', 'OKY', '400 M/M WAY YABA, LAGOS', 'EZEKIEL', '08024132111', 'info@efex-executive.com.ng', '(INTER-STATE TRANSPORT DIVISION). ROUTES: Onitsha, Owerri, Umuahia,Aba &Port-Harcourt.CONDITION: Assured Journey Mercies.This company takes no liability for good or luggage whether paid for or not. Appropriate fare must be paid for the journey and no refund of money after payment.Valid for one journey.', '', 0.00, 'okeyson_logo.png', '', 1, NULL, NULL, '2017-04-03 14:44:20');

-- --------------------------------------------------------

--
-- Table structure for table `parks`
--

CREATE TABLE `parks` (
  `id` int(10) UNSIGNED NOT NULL,
  `operator_id` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `state_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` text COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `boardable` tinyint(1) NOT NULL DEFAULT '1',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `parks`
--

INSERT INTO `parks` (`id`, `operator_id`, `state_id`, `name`, `address`, `active`, `boardable`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'OKOTA [IN LAGOS]', 'OKOTA, LAGOS', 1, 1, '2016-08-16 23:00:00', '2016-08-16 23:00:00', '2016-08-16 23:00:00'),
(22, 1, 13, 'OJI-RIVER [IN ENUGU]', '', 1, 0, NULL, '2017-04-03 14:08:41', '2017-04-03 14:08:41'),
(21, 1, 13, '9TH  MILE [IN ENUGU]', '', 1, 0, NULL, '2017-04-03 14:07:45', '2017-04-03 14:07:45'),
(20, 1, 5, 'AWKUZU JUNCTION [IN ANAMBRA]', '', 1, 0, NULL, '2017-04-03 14:06:22', '2017-04-03 14:06:58'),
(19, 1, 11, 'IGBARIAM [IN DELTA]', '', 1, 0, NULL, '2017-04-03 14:05:27', '2017-04-06 08:10:35'),
(18, 1, 13, 'ENUGU [IN ENUGU]', '', 1, 0, NULL, '2017-04-03 13:54:56', '2017-04-03 13:55:16'),
(17, 1, 11, 'AGBOR [IN DELTA]', '', 1, 0, NULL, '2017-04-03 13:21:28', '2017-04-03 13:21:28'),
(16, 1, 5, 'ONITSHA [IN ANAMBRA]', '', 1, 0, NULL, '2017-04-03 13:20:05', '2017-04-03 13:20:05'),
(15, 1, 11, 'ASABA [IN DELTA]', '', 1, 0, NULL, '2017-04-03 13:19:11', '2017-04-03 13:19:11'),
(14, 1, 5, 'AWKA [IN ANAMBRA]', '', 1, 0, NULL, '2017-04-03 13:16:08', '2017-04-03 13:16:08'),
(23, 1, 13, 'NSUKKA [IN ENUGU]', '', 1, 0, NULL, '2017-04-03 14:52:47', '2017-04-03 14:52:47'),
(24, 1, 35, 'ABAKALIKI [IN EBONYI]', '', 1, 0, NULL, '2017-04-03 14:59:45', '2017-04-03 14:59:45'),
(25, 1, 5, 'NNEWI [IN ANAMBRA]', '', 1, 0, NULL, '2017-04-04 09:38:30', '2017-04-04 09:38:30'),
(26, 1, 5, 'EKWULOBIA [IN ANAMBRA]', '', 1, 0, NULL, '2017-04-04 09:41:47', '2017-04-04 09:41:47'),
(27, 1, 35, 'AFIKPO [IN EBONYI]', '', 1, 0, NULL, '2017-04-04 09:44:46', '2017-04-04 09:44:46'),
(28, 1, 5, 'UMUEZE [IN ANAMBRA]', '', 1, 0, NULL, '2017-04-04 09:47:39', '2017-04-04 09:47:39'),
(29, 1, 5, 'UGA [IN ANAMBRA]', '', 1, 0, NULL, '2017-04-04 09:48:43', '2017-04-04 09:48:43'),
(30, 1, 10, 'UGEP [IN CROSS-RIVER]', '', 1, 0, NULL, '2017-04-04 09:50:22', '2017-04-04 09:50:22'),
(31, 1, 13, 'AWGU [IN ENUGU]', '', 1, 0, NULL, '2017-04-04 09:56:44', '2017-04-04 09:56:44'),
(32, 1, 35, 'OKPOSI [IN EBONYI]', '', 1, 0, NULL, '2017-04-04 10:03:35', '2017-04-04 10:03:35'),
(33, 1, 10, 'OGOJA [IN CROSS-RIVER]', '', 1, 0, NULL, '2017-04-04 10:09:12', '2017-04-04 10:09:12'),
(34, 1, 10, 'IKOM [IN CROSS-RIVER]', '', 1, 0, NULL, '2017-04-04 10:13:43', '2017-04-04 10:13:43'),
(35, 1, 15, 'OWERRI [IN IMO]', '', 1, 0, NULL, '2017-04-04 10:20:30', '2017-04-04 10:20:30'),
(36, 1, 5, 'IHIALA [IN ANAMBRA]', '', 1, 0, NULL, '2017-04-04 10:23:12', '2017-04-04 10:23:12'),
(37, 1, 15, 'OGBAKU [IN IMO]', '', 1, 0, NULL, '2017-04-04 10:24:36', '2017-04-04 10:24:36'),
(38, 1, 15, 'MGBIDI [IN IMO]', '', 1, 0, NULL, '2017-04-04 10:25:22', '2017-04-04 10:25:22'),
(39, 1, 15, 'ORLU JUNCTION [IN IMO]', '', 1, 0, NULL, '2017-04-04 10:27:45', '2017-04-04 10:27:45'),
(40, 1, 2, 'ABA [IN ABIA]', '', 1, 0, NULL, '2017-04-04 10:29:48', '2017-04-04 10:29:48'),
(41, 1, 2, 'UMUAHIA [IN ABIA]', '', 1, 0, NULL, '2017-04-04 10:34:08', '2017-04-04 10:34:08'),
(42, 1, 15, 'MBISE [IN IMO]', '', 1, 0, NULL, '2017-04-04 10:35:56', '2017-04-04 10:35:56'),
(43, 1, 15, 'Mbano [IN IMO]', '', 1, 0, NULL, '2017-04-04 10:36:25', '2017-04-04 10:36:25'),
(44, 1, 15, 'NKWERE [IN IMO]', '', 1, 0, NULL, '2017-04-04 10:36:55', '2017-04-04 10:36:55'),
(45, 1, 15, 'ORLU INSIDE [IN IMO]', '', 1, 0, NULL, '2017-04-04 10:38:25', '2017-04-04 10:38:25'),
(46, 1, 29, 'PORT-HARCOURT [IN RIVERS]', '', 1, 0, NULL, '2017-04-04 10:39:30', '2017-04-04 10:39:30'),
(47, 1, 34, 'ABUJA [IN ABUJA]', '', 1, 0, NULL, '2017-04-04 10:44:34', '2017-04-04 10:44:34'),
(48, 1, 21, 'LOKOJA [IN KOGI]', '', 1, 0, NULL, '2017-04-04 10:45:35', '2017-04-04 10:45:35'),
(49, 1, 21, 'OKENE [IN KOGI]', '', 1, 0, NULL, '2017-04-04 10:46:30', '2017-04-04 10:46:30'),
(50, 1, 34, 'GWAGWALADA [IN ABUJA]', '', 1, 0, NULL, '2017-04-04 10:47:20', '2017-04-04 10:47:20'),
(51, 1, 34, 'UTAKO [IN ABUJA]', '', 1, 0, NULL, '2017-04-04 10:47:49', '2017-04-04 10:47:49'),
(52, 1, 34, 'AIRPORT ROAD ZUBA [IN ABUJA]', '', 1, 0, NULL, '2017-04-04 10:48:55', '2017-04-04 10:48:55'),
(53, 1, 13, '4 CORNER [IN ENUGU]', '', 1, 0, NULL, '2017-04-06 08:12:49', '2017-04-06 08:12:49'),
(54, 1, 15, 'AWO JUNCTION [IN IMO]', '', 1, 0, NULL, '2017-04-06 08:17:00', '2017-04-06 08:17:00');

-- --------------------------------------------------------

--
-- Table structure for table `park_stops`
--

CREATE TABLE `park_stops` (
  `id` int(10) UNSIGNED NOT NULL,
  `operator_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `park_id` int(10) UNSIGNED DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `passengers`
--

CREATE TABLE `passengers` (
  `id` int(10) UNSIGNED NOT NULL,
  `operator_id` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `booking_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `gender` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `seat_no` int(11) NOT NULL,
  `age` int(10) NOT NULL,
  `daily_trip_id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `passengers`
--

INSERT INTO `passengers` (`id`, `operator_id`, `booking_id`, `name`, `gender`, `seat_no`, `age`, `daily_trip_id`, `deleted_at`, `created_at`, `updated_at`) VALUES
(56, 1, 53, 'Winnne', 'female', 0, 0, 21, NULL, '2017-04-25 09:54:41', '2017-04-25 09:54:41'),
(55, 1, 53, 'Qak', 'male', 0, 3, 21, NULL, '2017-04-25 09:54:41', '2017-04-25 09:54:41'),
(54, 1, 52, 'Sink', 'female', 0, 3, 21, NULL, '2017-04-25 09:46:29', '2017-04-25 09:46:29'),
(53, 1, 52, 'Danny', 'male', 9, 30, 21, NULL, '2017-04-25 09:46:29', '2017-04-25 09:46:29'),
(49, 1, 38, 'Ada', 'female', 0, 0, 19, NULL, '2017-04-21 15:46:05', '2017-04-21 15:46:05'),
(48, 1, 38, 'Baby', 'male', 0, 3, 19, NULL, '2017-04-21 15:46:05', '2017-04-21 15:46:05'),
(47, 1, 37, 'Dan', 'male', 0, 3, 19, NULL, '2017-04-21 13:54:19', '2017-04-21 13:54:19'),
(46, 1, 37, 'Qassim', 'male', 9, 30, 19, NULL, '2017-04-21 13:54:19', '2017-04-21 13:54:19'),
(45, 1, 36, 'Ada', 'female', 15, 30, 19, NULL, '2017-04-21 13:47:51', '2017-04-21 13:47:51'),
(44, 1, 36, 'Freeze', 'male', 11, 30, 19, NULL, '2017-04-21 13:47:51', '2017-04-21 13:47:51'),
(43, 1, 35, 'Angelo', 'male', 7, 30, 19, NULL, '2017-04-21 13:46:35', '2017-04-21 13:46:35'),
(42, 1, 35, 'Sharon', 'female', 0, 3, 19, NULL, '2017-04-21 13:46:35', '2017-04-21 13:46:35'),
(41, 1, 35, 'Abel', 'male', 4, 30, 19, NULL, '2017-04-21 13:46:35', '2017-04-21 13:46:35');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payment_methods`
--

CREATE TABLE `payment_methods` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `is_fulfilment` tinyint(1) NOT NULL DEFAULT '1',
  `bank_id` int(10) UNSIGNED DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `payment_methods`
--

INSERT INTO `payment_methods` (`id`, `name`, `description`, `active`, `is_fulfilment`, `bank_id`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Book On Hold', 'Bank Deposit', 1, 0, NULL, NULL, NULL, NULL),
(2, 'Cash', 'Online Payment', 1, 1, NULL, NULL, NULL, NULL),
(3, 'Card', '', 1, 1, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `queue_jobs`
--

CREATE TABLE `queue_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `operator_id` int(10) UNSIGNED NOT NULL,
  `queue` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `payload` text COLLATE utf8_unicode_ci NOT NULL,
  `attempts` tinyint(1) NOT NULL,
  `reserved` tinyint(1) NOT NULL,
  `reserved_at` int(10) UNSIGNED DEFAULT NULL,
  `available_at` int(10) UNSIGNED NOT NULL,
  `created_at` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `operator_id` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `operator_id`, `name`, `permissions`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 1, 'Super User', 'book_trip,view_bookings,view_booking_details,mark_bookings_paid,cancel_booking,view_charter_bookings,charter_bookings,add_trip,view_own_park_trips,view_all_trips,upload trip,edit_trip,add_incident,manage_incidents,book_from_any_park,book_to_any_park,set_total_fare,view_daily_trips_report,view_drivers_report,view_buses_report,view_manifests_report,view_travelers_report,view_bookings_report,view_charter_bookings_report,view_parks_report,view_ticketers_report,view_payments_report,view_incidents_report,view_other_parks_report,list_buses,manage_buses,list_drivers,manage_drivers,setup_roles,setup_users', NULL, NULL, '2016-10-07 11:44:48'),
(2, 1, 'Ticketer', 'book_trip,view_bookings,view_booking_details,mark_bookings_paid,cancel_booking,view_charter_bookings,charter_bookings,book_from_any_park,book_to_any_park', NULL, '2016-09-27 08:40:56', '2016-10-07 10:51:54'),
(3, 1, 'OPERATIONS', 'add_trip,view_own_park_trips,view_all_trips,upload trip,edit_trip,book_from_any_park,book_to_any_park,set_total_fare,view_daily_trips_report,view_drivers_report,view_buses_report,view_manifests_report,view_travelers_report,view_bookings_report,view_charter_bookings_report,view_parks_report,view_ticketers_report,view_payments_report,view_incidents_report,view_other_parks_report,list_buses,manage_buses', NULL, '2016-10-07 11:07:17', '2016-10-07 11:07:17'),
(4, 1, 'Operations 1', 'view_daily_trips_report,view_drivers_report,view_buses_report,view_manifests_report,view_travelers_report,view_bookings_report,view_charter_bookings_report,view_parks_report,view_ticketers_report,view_payments_report,view_incidents_report,view_other_parks_report', NULL, '2016-10-07 11:09:01', '2016-10-07 11:09:01'),
(5, 1, 'operator', '', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `seats`
--

CREATE TABLE `seats` (
  `id` int(10) UNSIGNED NOT NULL,
  `operator_id` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `seat_no` int(11) NOT NULL,
  `daily_trip_id` int(10) UNSIGNED NOT NULL,
  `booking_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `seats`
--

INSERT INTO `seats` (`id`, `operator_id`, `seat_no`, `daily_trip_id`, `booking_id`, `created_at`, `updated_at`) VALUES
(77, 1, 13, 21, 53, '2017-04-25 09:54:41', '2017-04-25 09:54:41'),
(76, 1, 10, 21, 53, '2017-04-25 09:54:41', '2017-04-25 09:54:41'),
(75, 1, 9, 21, 52, '2017-04-25 09:46:29', '2017-04-25 09:46:29'),
(74, 1, 6, 21, 52, '2017-04-25 09:46:29', '2017-04-25 09:46:29'),
(69, 1, 2, 19, 39, '2017-04-21 15:55:25', '2017-04-21 15:55:25'),
(68, 1, 12, 19, 38, '2017-04-21 15:46:05', '2017-04-21 15:46:05'),
(67, 1, 6, 19, 38, '2017-04-21 15:46:05', '2017-04-21 15:46:05'),
(66, 1, 9, 19, 37, '2017-04-21 13:54:19', '2017-04-21 13:54:19'),
(65, 1, 5, 19, 37, '2017-04-21 13:54:19', '2017-04-21 13:54:19'),
(64, 1, 15, 19, 36, '2017-04-21 13:47:51', '2017-04-21 13:47:51'),
(63, 1, 11, 19, 36, '2017-04-21 13:47:51', '2017-04-21 13:47:51'),
(62, 1, 8, 19, 36, '2017-04-21 13:47:51', '2017-04-21 13:47:51'),
(61, 1, 7, 19, 35, '2017-04-21 13:46:35', '2017-04-21 13:46:35'),
(60, 1, 4, 19, 35, '2017-04-21 13:46:35', '2017-04-21 13:46:35'),
(59, 1, 10, 19, 35, '2017-04-21 13:46:35', '2017-04-21 13:46:35');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `operator_id` int(10) UNSIGNED NOT NULL,
  `key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` text COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `exposed` tinyint(1) NOT NULL DEFAULT '1',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `luggage_limit` int(11) DEFAULT NULL,
  `intl_cost_per_kg` int(11) DEFAULT NULL,
  `local_cost_per_kg` int(11) DEFAULT NULL,
  `daily_trip_expense_headers` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `discount_active` enum('true','false') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'false',
  `track_luggage` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `track_infant` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `infant_discount` int(11) DEFAULT NULL,
  `infant_age_range` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `operator_id`, `key`, `value`, `description`, `exposed`, `deleted_at`, `created_at`, `updated_at`, `luggage_limit`, `intl_cost_per_kg`, `local_cost_per_kg`, `daily_trip_expense_headers`, `discount_active`, `track_luggage`, `track_infant`, `infant_discount`, `infant_age_range`) VALUES
(1, 0, 'luggage_limit', '10', 'general threshold for luggage (in kg)', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'false', NULL, NULL, NULL, NULL),
(2, 0, 'intl_cost_per_kg', '100', 'cost per kg (cpk)', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'false', NULL, NULL, NULL, NULL),
(3, 0, 'local_cost_per_kg', '50', 'cost per kg (cpk)', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 'false', NULL, NULL, NULL, NULL),
(4, 0, 'daily_trip_expense_headers', 'Fuel Expense,Driver allowance, Driver Dispatch,Borders,Passenger food,NDLEA,Photocopy,Vehicle Particulars,Commission,Others\n', 'Headers for daily trip expenses per bus. It should be comma separated. Not space in between.', 0, NULL, '2016-09-21 21:17:43', '2017-03-25 09:03:16', NULL, NULL, NULL, NULL, 'false', NULL, NULL, NULL, NULL),
(5, 0, 'track_luggage', 'true', 'Does operator track customer luggage and maybe charge for it?   \'true\' or \'false\'', 1, NULL, '2016-10-17 14:13:37', '2016-10-17 14:13:37', NULL, NULL, NULL, NULL, 'false', NULL, NULL, NULL, NULL),
(8, 1, '', '', '', 1, NULL, '2017-03-31 11:25:06', '2017-04-12 11:23:52', 10, 20, 50, 'Drivers allocation,Feeding allowance,Commission,Others', 'true', 'True', 'True', 20, '');

-- --------------------------------------------------------

--
-- Table structure for table `states`
--

CREATE TABLE `states` (
  `id` int(10) UNSIGNED NOT NULL,
  `operator_id` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `region` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `boardable` tinyint(1) NOT NULL DEFAULT '1',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `states`
--

INSERT INTO `states` (`id`, `operator_id`, `name`, `region`, `active`, `boardable`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 1, 'Lagos', 'Nigeria', 1, 1, NULL, '2016-08-09 04:28:33', '2016-08-09 04:28:33'),
(2, 1, 'Abia', 'Nigeria', 1, 1, NULL, '2016-08-09 04:28:33', '2016-08-09 04:28:33'),
(3, 1, 'Adamawa', 'Nigeria', 1, 1, NULL, '2016-08-09 04:28:33', '2016-08-09 04:28:33'),
(4, 1, 'Akwa-Ibom', 'Nigeria', 1, 1, NULL, '2016-08-09 04:28:33', '2016-08-09 04:28:33'),
(5, 1, 'Anambra', 'Nigeria', 1, 1, NULL, '2016-08-09 04:28:33', '2016-08-09 04:28:33'),
(6, 1, 'Bauchi', 'Nigeria', 1, 1, NULL, '2016-08-09 04:28:33', '2016-08-09 04:28:33'),
(7, 1, 'Bayelsa', 'Nigeria', 1, 1, NULL, '2016-08-09 04:28:33', '2016-08-09 04:28:33'),
(8, 1, 'Benue', 'Nigeria', 1, 1, NULL, '2016-08-09 04:28:33', '2016-08-09 04:28:33'),
(9, 1, 'Borno', 'Nigeria', 1, 1, NULL, '2016-08-09 04:28:33', '2016-08-09 04:28:33'),
(10, 1, 'Cross-River', 'Nigeria', 1, 1, NULL, '2016-08-09 04:28:33', '2016-08-09 04:28:33'),
(11, 1, 'Delta', 'Nigeria', 1, 1, NULL, '2016-08-09 04:28:33', '2016-08-09 04:28:33'),
(12, 1, 'Edo', 'Nigeria', 1, 1, NULL, '2016-08-09 04:28:33', '2016-08-09 04:28:33'),
(13, 1, 'Enugu', 'Nigeria', 1, 1, NULL, '2016-08-09 04:28:33', '2016-08-09 04:28:33'),
(14, 1, 'Gombe', 'Nigeria', 1, 1, NULL, '2016-08-09 04:28:33', '2016-08-09 04:28:33'),
(15, 1, 'Imo', 'Nigeria', 1, 0, NULL, '2016-08-09 04:28:33', '2016-08-09 04:28:33'),
(16, 1, 'Jigawa', 'Nigeria', 1, 1, NULL, '2016-08-09 04:28:33', '2016-08-09 04:28:33'),
(17, 1, 'Kaduna', 'Nigeria', 1, 1, NULL, '2016-08-09 04:28:33', '2016-08-09 04:28:33'),
(18, 1, 'Kano', 'Nigeria', 1, 1, NULL, '2016-08-09 04:28:33', '2016-08-09 04:28:33'),
(19, 1, 'Katsina', 'Nigeria', 1, 1, NULL, '2016-08-09 04:28:33', '2016-08-09 04:28:33'),
(20, 1, 'Kebbi', 'Nigeria', 1, 1, NULL, '2016-08-09 04:28:33', '2016-08-09 04:28:33'),
(21, 1, 'Kogi', 'Nigeria', 1, 1, NULL, '2016-08-09 04:28:33', '2016-08-09 04:28:33'),
(22, 1, 'Kwara', 'Nigeria', 1, 1, NULL, '2016-08-09 04:28:33', '2016-08-09 04:28:33'),
(23, 1, 'Nasarawa', 'Nigeria', 1, 1, NULL, '2016-08-09 04:28:33', '2016-08-09 04:28:33'),
(24, 1, 'Niger', 'Nigeria', 1, 1, NULL, '2016-08-09 04:28:33', '2016-08-09 04:28:33'),
(25, 1, 'Ogun', 'Nigeria', 1, 1, NULL, '2016-08-09 04:28:33', '2016-08-09 04:28:33'),
(26, 1, 'Osun', 'Nigeria', 1, 1, NULL, '2016-08-09 04:28:33', '2016-08-09 04:28:33'),
(27, 1, 'Oyo', 'Nigeria', 1, 1, NULL, '2016-08-09 04:28:33', '2016-08-09 04:28:33'),
(28, 1, 'Plateau', 'Nigeria', 1, 1, NULL, '2016-08-09 04:28:33', '2016-08-09 04:28:33'),
(29, 1, 'Rivers', 'Nigeria', 1, 1, NULL, '2016-08-09 04:28:33', '2016-08-09 04:28:33'),
(30, 1, 'Sokoto', 'Nigeria', 1, 1, NULL, '2016-08-09 04:28:33', '2016-08-09 04:28:33'),
(31, 1, 'Taraba', 'Nigeria', 1, 1, NULL, '2016-08-09 04:28:33', '2016-08-09 04:28:33'),
(32, 1, 'Yobe', 'Nigeria', 1, 1, NULL, '2016-08-09 04:28:33', '2016-08-09 04:28:33'),
(33, 1, 'Zamfara', 'Nigeria', 1, 1, NULL, '2016-08-09 04:28:33', '2016-08-09 04:28:33'),
(34, 1, 'Abuja', 'Nigeria', 1, 1, NULL, '2016-08-09 04:28:33', '2016-08-09 04:28:33'),
(35, 1, 'Ebonyi', 'Nigeria', 1, 1, NULL, '2016-08-09 04:28:33', '2016-08-09 04:28:33'),
(36, 1, 'Ekiti', 'Nigeria', 1, 1, NULL, '2016-08-09 04:28:33', '2016-08-09 04:28:33'),
(37, 1, 'Ondo', 'Nigeria', 1, 1, NULL, '2016-08-09 04:28:33', '2016-08-09 04:28:33'),
(38, 1, 'Cotonou - Benin Republic', 'Republic of Benin', 1, 0, NULL, '2016-08-09 04:28:33', '2016-08-09 04:28:33'),
(39, 1, 'Accra', 'Ghana', 1, 0, NULL, '2016-08-09 04:28:33', '2016-08-09 04:28:33'),
(41, 1, 'Lome - Togo', 'Togo', 1, 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sub_trips`
--

CREATE TABLE `sub_trips` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `operator_id` int(10) UNSIGNED NOT NULL,
  `state_id` int(10) UNSIGNED NOT NULL,
  `source_park_id` int(10) UNSIGNED NOT NULL,
  `dest_park_id` int(10) UNSIGNED NOT NULL,
  `fare` double(8,2) NOT NULL,
  `parent_trip_id` int(10) UNSIGNED NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `operator_trip_id` int(11) NOT NULL DEFAULT '1',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `trip_transit` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `round_trip_fare` double DEFAULT NULL,
  `round_trip_status` tinyint(1) DEFAULT NULL,
  `is_intl_trip` tinyint(1) NOT NULL DEFAULT '0',
  `virgin_passport_fare` decimal(8,2) DEFAULT NULL,
  `no_passport_fare` decimal(8,2) DEFAULT NULL,
  `round_trip_virgin_passport_fare` decimal(10,2) DEFAULT NULL,
  `round_trip_no_passport_fare` decimal(10,2) DEFAULT NULL,
  `is_parent_trip` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sub_trips`
--

INSERT INTO `sub_trips` (`id`, `name`, `operator_id`, `state_id`, `source_park_id`, `dest_park_id`, `fare`, `parent_trip_id`, `active`, `operator_trip_id`, `deleted_at`, `created_at`, `updated_at`, `trip_transit`, `round_trip_fare`, `round_trip_status`, `is_intl_trip`, `virgin_passport_fare`, `no_passport_fare`, `round_trip_virgin_passport_fare`, `round_trip_no_passport_fare`, `is_parent_trip`) VALUES
(1, 'AWKA [IN ANAMBRA]', 1, 5, 1, 14, 5000.00, 1, 1, 1, NULL, '2017-04-03 13:16:55', '2017-04-03 13:16:55', 'First Bus', 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 1),
(3, 'ASABA [IN DELTA]', 1, 11, 1, 15, 4500.00, 1, 1, 1, NULL, '2017-04-03 13:26:23', '2017-04-03 13:26:23', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(4, 'ONITSHA [IN ANAMBRA]', 1, 5, 1, 16, 4500.00, 1, 1, 1, NULL, '2017-04-03 13:26:45', '2017-04-03 13:26:45', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(5, 'AGBOR [IN DELTA]', 1, 11, 1, 17, 4500.00, 1, 1, 1, NULL, '2017-04-03 13:27:07', '2017-04-03 13:27:07', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(6, 'ENUGU [IN ENUGU]', 1, 0, 1, 18, 5000.00, 3, 1, 1, NULL, '2017-04-03 13:56:09', '2017-04-03 13:56:09', 'First Bus', 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 1),
(7, 'AGBOR [IN DELTA]', 1, 0, 1, 17, 4500.00, 3, 1, 1, NULL, '2017-04-03 13:58:08', '2017-04-03 14:09:03', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(8, 'AWKA [IN ANAMBRA]', 1, 0, 1, 14, 5000.00, 3, 1, 1, NULL, '2017-04-03 13:59:02', '2017-04-03 14:09:03', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(9, 'ASABA [IN DELTA]', 1, 0, 1, 15, 4500.00, 3, 1, 1, NULL, '2017-04-03 13:59:18', '2017-04-03 14:09:03', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(10, 'ONITSHA [IN ANAMBRA]', 1, 0, 1, 16, 4500.00, 3, 1, 1, NULL, '2017-04-03 13:59:53', '2017-04-03 14:09:03', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(11, 'IGBARIAM [IN ANAMBRA]', 1, 0, 1, 19, 5000.00, 3, 1, 1, NULL, '2017-04-03 14:09:49', '2017-04-03 14:09:49', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(12, 'AWKUZU JUNCTION [IN ANAMBRA]', 1, 0, 1, 20, 5000.00, 3, 1, 1, NULL, '2017-04-03 14:10:11', '2017-04-03 14:10:11', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(13, '9TH  MILE [IN ENUGU]', 1, 0, 1, 21, 5000.00, 3, 1, 1, NULL, '2017-04-03 14:10:27', '2017-04-03 14:10:27', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(14, 'OJI-RIVER [IN ENUGU]', 1, 0, 1, 22, 5000.00, 3, 1, 1, NULL, '2017-04-03 14:10:45', '2017-04-03 14:10:45', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(15, 'NSUKKA [IN ENUGU]', 1, 0, 1, 23, 5000.00, 4, 1, 1, NULL, '2017-04-03 14:54:07', '2017-04-03 14:54:07', 'First Bus', 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 1),
(16, 'ASABA [IN DELTA]', 1, 0, 1, 15, 4500.00, 4, 1, 1, NULL, '2017-04-03 14:54:27', '2017-04-03 14:54:27', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(17, 'ONITSHA [IN ANAMBRA]', 1, 0, 1, 16, 4500.00, 4, 1, 1, NULL, '2017-04-03 14:54:50', '2017-04-03 14:54:50', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(18, 'AGBOR [IN DELTA]', 1, 0, 1, 17, 4500.00, 4, 1, 1, NULL, '2017-04-03 14:55:04', '2017-04-03 14:55:04', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(19, 'AWKA [IN ANAMBRA]', 1, 0, 1, 14, 5000.00, 4, 1, 1, NULL, '2017-04-03 14:55:25', '2017-04-03 14:57:16', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(20, 'IGBARIAM [IN ANAMBRA]', 1, 0, 1, 19, 5000.00, 4, 1, 1, NULL, '2017-04-03 14:55:38', '2017-04-03 14:57:34', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(21, 'AWKUZU JUNCTION [IN ANAMBRA]', 1, 0, 1, 20, 5000.00, 4, 1, 1, NULL, '2017-04-03 14:56:03', '2017-04-03 14:57:50', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(22, '9TH  MILE [IN ENUGU]', 1, 0, 1, 21, 5000.00, 4, 1, 1, NULL, '2017-04-03 14:56:15', '2017-04-03 14:58:00', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(23, 'OJI-RIVER [IN ENUGU]', 1, 0, 1, 22, 4500.00, 4, 1, 1, NULL, '2017-04-03 14:56:28', '2017-04-03 14:56:28', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(24, 'ABAKALIKI [IN EBONYI]', 1, 0, 1, 24, 5500.00, 5, 1, 1, NULL, '2017-04-03 15:00:40', '2017-04-03 15:00:40', 'First Bus', 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 1),
(25, 'ASABA [IN DELTA]', 1, 0, 1, 15, 4500.00, 5, 1, 1, NULL, '2017-04-03 15:01:23', '2017-04-03 15:01:23', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(26, 'ONITSHA [IN ANAMBRA]', 1, 0, 1, 16, 4500.00, 5, 1, 1, NULL, '2017-04-03 15:01:35', '2017-04-03 15:01:35', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(27, 'AGBOR [IN DELTA]', 1, 0, 1, 17, 4500.00, 5, 1, 1, NULL, '2017-04-03 15:01:57', '2017-04-03 15:01:57', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(28, 'AWKA [IN ANAMBRA]', 1, 0, 1, 14, 5000.00, 5, 1, 1, NULL, '2017-04-03 15:02:41', '2017-04-03 15:02:41', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(29, 'IGBARIAM [IN ANAMBRA]', 1, 0, 1, 19, 5000.00, 5, 1, 1, NULL, '2017-04-03 15:03:05', '2017-04-03 15:03:05', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(30, 'AWKUZU JUNCTION [IN ANAMBRA]', 1, 0, 1, 20, 5000.00, 5, 1, 1, NULL, '2017-04-03 15:03:25', '2017-04-03 15:03:25', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(31, '9TH  MILE [IN ENUGU]', 1, 0, 1, 21, 5000.00, 5, 1, 1, NULL, '2017-04-03 15:03:43', '2017-04-03 15:03:43', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(32, 'OJI-RIVER [IN ENUGU]', 1, 0, 1, 22, 5000.00, 5, 1, 1, NULL, '2017-04-03 15:03:59', '2017-04-03 15:03:59', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(33, 'NNEWI [IN ANAMBRA]', 1, 0, 1, 25, 4800.00, 6, 1, 1, NULL, '2017-04-04 09:39:35', '2017-04-04 09:39:35', 'First Bus', 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 1),
(34, 'ASABA [IN DELTA]', 1, 0, 1, 15, 4500.00, 6, 1, 1, NULL, '2017-04-04 09:39:56', '2017-04-04 09:39:56', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(35, 'ONITSHA [IN ANAMBRA]', 1, 0, 1, 16, 4500.00, 6, 1, 1, NULL, '2017-04-04 09:40:13', '2017-04-04 09:40:13', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(36, 'AGBOR [IN DELTA]', 1, 0, 1, 17, 4500.00, 6, 1, 1, NULL, '2017-04-04 09:40:31', '2017-04-04 09:40:31', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(37, 'EKWULOBIA [IN ANAMBRA]', 1, 0, 1, 26, 4800.00, 6, 1, 1, NULL, '2017-04-04 09:42:02', '2017-04-04 09:42:02', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(38, 'EKWULOBIA [IN ANAMBRA]', 1, 0, 1, 26, 4800.00, 7, 1, 1, NULL, '2017-04-04 09:42:47', '2017-04-04 09:42:47', 'First Bus', 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 1),
(39, 'ASABA [IN DELTA]', 1, 0, 1, 15, 4500.00, 7, 1, 1, NULL, '2017-04-04 09:43:11', '2017-04-04 09:43:11', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(40, 'ONITSHA [IN ANAMBRA]', 1, 0, 1, 16, 4500.00, 7, 1, 1, NULL, '2017-04-04 09:43:27', '2017-04-04 09:43:27', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(41, 'AGBOR [IN DELTA]', 1, 0, 1, 17, 4500.00, 7, 1, 1, NULL, '2017-04-04 09:43:45', '2017-04-04 09:43:45', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(42, 'AFIKPO [IN EBONYI]', 1, 0, 1, 27, 6000.00, 8, 1, 1, NULL, '2017-04-04 09:45:30', '2017-04-04 09:45:30', 'First Bus', 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 1),
(43, 'ASABA [IN DELTA]', 1, 0, 1, 15, 4500.00, 8, 1, 1, NULL, '2017-04-04 09:45:56', '2017-04-04 09:45:56', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(44, 'ONITSHA [IN ANAMBRA]', 1, 0, 1, 16, 4500.00, 8, 1, 1, NULL, '2017-04-04 09:46:05', '2017-04-04 09:46:05', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(45, 'AGBOR [IN DELTA]', 1, 0, 1, 17, 4500.00, 8, 1, 1, NULL, '2017-04-04 09:46:19', '2017-04-04 09:46:19', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(46, 'NNEWI [IN ANAMBRA]', 1, 0, 1, 25, 4800.00, 8, 1, 1, NULL, '2017-04-04 09:46:35', '2017-04-04 09:46:35', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(47, 'EKWULOBIA [IN ANAMBRA]', 1, 0, 1, 26, 4800.00, 8, 1, 1, NULL, '2017-04-04 09:46:47', '2017-04-04 09:46:47', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(48, 'UMUEZE [IN ANAMBRA]', 1, 0, 1, 28, 5000.00, 8, 1, 1, NULL, '2017-04-04 09:48:09', '2017-04-04 09:48:09', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(49, 'UGA [IN ANAMBRA]', 1, 0, 1, 29, 5000.00, 8, 1, 1, NULL, '2017-04-04 09:49:03', '2017-04-04 09:49:03', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(50, 'UGEP [IN CROSS-RIVER]', 1, 0, 1, 30, 7000.00, 9, 1, 1, NULL, '2017-04-04 09:50:54', '2017-04-04 09:50:54', 'First Bus', 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 1),
(51, 'ASABA [IN DELTA]', 1, 0, 1, 15, 4500.00, 9, 1, 1, NULL, '2017-04-04 09:51:16', '2017-04-04 09:51:16', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(52, 'ONITSHA [IN ANAMBRA]', 1, 0, 1, 16, 4500.00, 9, 1, 1, NULL, '2017-04-04 09:51:32', '2017-04-04 09:51:32', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(53, 'AGBOR [IN DELTA]', 1, 0, 1, 17, 4500.00, 9, 1, 1, NULL, '2017-04-04 09:51:44', '2017-04-04 09:51:44', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(54, 'NNEWI [IN ANAMBRA]', 1, 0, 1, 25, 4800.00, 9, 1, 1, NULL, '2017-04-04 09:52:02', '2017-04-04 09:52:02', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(55, 'EKWULOBIA [IN ANAMBRA]', 1, 0, 1, 26, 4800.00, 9, 1, 1, NULL, '2017-04-04 09:52:15', '2017-04-04 09:52:15', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(56, 'UMUEZE [IN ANAMBRA]', 1, 0, 1, 28, 5000.00, 9, 1, 1, NULL, '2017-04-04 09:52:44', '2017-04-04 09:52:44', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(57, 'UGA [IN ANAMBRA]', 1, 0, 1, 29, 5000.00, 9, 1, 1, NULL, '2017-04-04 09:52:58', '2017-04-04 09:52:58', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(58, 'AFIKPO [IN EBONYI]', 1, 0, 1, 27, 5000.00, 9, 1, 1, NULL, '2017-04-04 09:53:13', '2017-04-04 09:53:13', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(59, 'OJI-RIVER [IN ENUGU]', 1, 0, 1, 22, 5000.00, 10, 1, 1, NULL, '2017-04-04 09:53:50', '2017-04-04 09:53:50', 'First Bus', 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 1),
(60, 'ASABA [IN DELTA]', 1, 0, 1, 15, 4500.00, 10, 1, 1, NULL, '2017-04-04 09:54:10', '2017-04-04 09:54:10', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(61, 'ONITSHA [IN ANAMBRA]', 1, 0, 1, 16, 4500.00, 10, 1, 1, NULL, '2017-04-04 09:54:22', '2017-04-04 09:54:22', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(62, 'AGBOR [IN DELTA]', 1, 0, 1, 17, 4500.00, 10, 1, 1, NULL, '2017-04-04 09:54:33', '2017-04-04 09:54:33', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(63, 'AWKA [IN ANAMBRA]', 1, 0, 1, 14, 5000.00, 10, 1, 1, NULL, '2017-04-04 09:54:53', '2017-04-04 09:54:53', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(64, 'IGBARIAM [IN ANAMBRA]', 1, 0, 1, 19, 5000.00, 10, 1, 1, NULL, '2017-04-04 09:55:04', '2017-04-04 09:55:04', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(65, 'AWKUZU JUNCTION [IN ANAMBRA]', 1, 0, 1, 20, 5000.00, 10, 1, 1, NULL, '2017-04-04 09:55:21', '2017-04-04 09:55:21', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(66, 'AWGU [IN ENUGU]', 1, 0, 1, 31, 5000.00, 10, 1, 1, NULL, '2017-04-04 09:57:22', '2017-04-04 09:57:22', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(67, 'AWGU [IN ENUGU]', 1, 0, 1, 31, 5000.00, 11, 1, 1, NULL, '2017-04-04 09:58:39', '2017-04-04 09:58:39', 'First Bus', 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 1),
(68, 'ASABA [IN DELTA]', 1, 0, 1, 15, 4500.00, 11, 1, 1, NULL, '2017-04-04 09:58:59', '2017-04-04 09:58:59', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(69, 'ONITSHA [IN ANAMBRA]', 1, 0, 1, 16, 4500.00, 11, 1, 1, NULL, '2017-04-04 09:59:15', '2017-04-04 09:59:15', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(70, 'AGBOR [IN DELTA]', 1, 0, 1, 17, 4500.00, 11, 1, 1, NULL, '2017-04-04 09:59:28', '2017-04-04 09:59:28', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(71, 'AWKA [IN ANAMBRA]', 1, 0, 1, 14, 5000.00, 11, 1, 1, NULL, '2017-04-04 10:00:05', '2017-04-04 10:00:05', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(72, 'IGBARIAM [IN ANAMBRA]', 1, 0, 1, 19, 5000.00, 11, 1, 1, NULL, '2017-04-04 10:00:31', '2017-04-04 10:00:31', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(73, 'AWKUZU JUNCTION [IN ANAMBRA]', 1, 0, 1, 20, 5000.00, 11, 1, 1, NULL, '2017-04-04 10:00:44', '2017-04-04 10:00:44', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(74, 'OJI-RIVER [IN ENUGU]', 1, 0, 1, 22, 5000.00, 11, 1, 1, NULL, '2017-04-04 10:01:04', '2017-04-04 10:01:04', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(75, 'ENUGU [IN ENUGU]', 1, 0, 1, 18, 5000.00, 11, 1, 1, NULL, '2017-04-04 10:01:24', '2017-04-04 10:01:24', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(76, 'OKPOSI [IN EBONYI]', 1, 0, 1, 32, 5500.00, 12, 1, 1, NULL, '2017-04-04 10:04:57', '2017-04-04 10:04:57', 'First Bus', 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 1),
(77, 'ASABA [IN DELTA]', 1, 0, 1, 15, 4500.00, 12, 1, 1, NULL, '2017-04-04 10:05:47', '2017-04-04 10:05:47', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(78, 'ONITSHA [IN ANAMBRA]', 1, 0, 1, 16, 4500.00, 12, 1, 1, NULL, '2017-04-04 10:05:58', '2017-04-04 10:05:58', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(79, 'AGBOR [IN DELTA]', 1, 0, 1, 17, 4500.00, 12, 1, 1, NULL, '2017-04-04 10:06:10', '2017-04-04 10:06:10', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(80, 'AWKA [IN ANAMBRA]', 1, 0, 1, 14, 5000.00, 12, 1, 1, NULL, '2017-04-04 10:06:29', '2017-04-04 10:06:29', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(81, 'IGBARIAM [IN ANAMBRA]', 1, 0, 1, 19, 5000.00, 12, 1, 1, NULL, '2017-04-04 10:06:47', '2017-04-04 10:06:47', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(82, 'AWKUZU JUNCTION [IN ANAMBRA]', 1, 0, 1, 20, 5000.00, 12, 1, 1, NULL, '2017-04-04 10:07:01', '2017-04-04 10:07:01', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(83, 'AWGU [IN ENUGU]', 1, 0, 1, 31, 5000.00, 12, 1, 1, NULL, '2017-04-04 10:07:18', '2017-04-04 10:07:18', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(84, 'OJI-RIVER [IN ENUGU]', 1, 0, 1, 22, 4500.00, 12, 1, 1, NULL, '2017-04-04 10:07:44', '2017-04-04 10:07:44', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(85, 'ENUGU [IN ENUGU]', 1, 0, 1, 18, 5000.00, 12, 1, 1, NULL, '2017-04-04 10:07:59', '2017-04-04 10:07:59', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(86, 'OGOJA [IN CROSS-RIVER]', 1, 0, 1, 33, 7000.00, 13, 1, 1, NULL, '2017-04-04 10:10:08', '2017-04-04 10:10:08', 'First Bus', 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 1),
(87, 'ASABA [IN DELTA]', 1, 0, 1, 15, 4500.00, 13, 1, 1, NULL, '2017-04-04 10:10:28', '2017-04-04 10:10:28', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(88, 'ONITSHA [IN ANAMBRA]', 1, 0, 1, 16, 4500.00, 13, 1, 1, NULL, '2017-04-04 10:10:44', '2017-04-04 10:10:44', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(89, 'AGBOR [IN DELTA]', 1, 0, 1, 17, 4500.00, 13, 1, 1, NULL, '2017-04-04 10:11:01', '2017-04-04 10:11:01', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(90, 'AWKA [IN ANAMBRA]', 1, 0, 1, 14, 5000.00, 13, 1, 1, NULL, '2017-04-04 10:11:21', '2017-04-04 10:11:21', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(91, 'IGBARIAM [IN ANAMBRA]', 1, 0, 1, 19, 5000.00, 13, 1, 1, NULL, '2017-04-04 10:11:35', '2017-04-04 10:11:35', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(92, 'AWKUZU JUNCTION [IN ANAMBRA]', 1, 0, 1, 20, 5000.00, 13, 1, 1, NULL, '2017-04-04 10:11:44', '2017-04-04 10:11:44', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(93, 'OJI-RIVER [IN ENUGU]', 1, 0, 1, 22, 5000.00, 13, 1, 1, NULL, '2017-04-04 10:12:00', '2017-04-04 10:12:00', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(94, 'ENUGU [IN ENUGU]', 1, 0, 1, 18, 5000.00, 13, 1, 1, NULL, '2017-04-04 10:12:14', '2017-04-04 10:12:14', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(95, 'ABAKALIKI [IN EBONYI]', 1, 0, 1, 24, 5500.00, 13, 1, 1, NULL, '2017-04-04 10:12:29', '2017-04-04 10:12:29', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(96, 'IKOM [IN CROSS-RIVER]', 1, 0, 1, 34, 7000.00, 14, 1, 1, NULL, '2017-04-04 10:14:17', '2017-04-04 10:14:17', 'First Bus', 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 1),
(97, 'ASABA [IN DELTA]', 1, 0, 1, 15, 4500.00, 14, 1, 1, NULL, '2017-04-04 10:14:37', '2017-04-04 10:14:37', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(98, 'ONITSHA [IN ANAMBRA]', 1, 0, 1, 16, 4500.00, 14, 1, 1, NULL, '2017-04-04 10:14:47', '2017-04-04 10:14:47', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(99, 'AGBOR [IN DELTA]', 1, 0, 1, 17, 4500.00, 14, 1, 1, NULL, '2017-04-04 10:15:07', '2017-04-04 10:15:07', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(100, 'AWKA [IN ANAMBRA]', 1, 0, 1, 14, 5000.00, 14, 1, 1, NULL, '2017-04-04 10:15:23', '2017-04-04 10:15:23', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(101, 'IGBARIAM [IN ANAMBRA]', 1, 0, 1, 19, 5000.00, 14, 1, 1, NULL, '2017-04-04 10:15:33', '2017-04-04 10:15:33', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(102, 'AWKUZU JUNCTION [IN ANAMBRA]', 1, 0, 1, 20, 5000.00, 14, 1, 1, NULL, '2017-04-04 10:15:51', '2017-04-04 10:15:51', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(103, 'OJI-RIVER [IN ENUGU]', 1, 0, 1, 22, 5000.00, 14, 1, 1, NULL, '2017-04-04 10:16:02', '2017-04-04 10:16:02', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(104, 'ENUGU [IN ENUGU]', 1, 0, 1, 18, 5000.00, 14, 1, 1, NULL, '2017-04-04 10:16:17', '2017-04-04 10:16:17', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(105, 'ABAKALIKI [IN EBONYI]', 1, 0, 1, 24, 5500.00, 14, 1, 1, NULL, '2017-04-04 10:16:34', '2017-04-04 10:16:34', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(106, 'OWERRI [IN IMO]', 1, 0, 1, 35, 4800.00, 15, 1, 1, NULL, '2017-04-04 10:21:34', '2017-04-04 10:21:34', 'First Bus', 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 1),
(107, 'ASABA [IN DELTA]', 1, 0, 1, 15, 4500.00, 15, 1, 1, NULL, '2017-04-04 10:21:56', '2017-04-04 10:21:56', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(108, 'ONITSHA [IN ANAMBRA]', 1, 0, 1, 16, 4500.00, 15, 1, 1, NULL, '2017-04-04 10:22:08', '2017-04-04 10:22:08', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(109, 'AGBOR [IN DELTA]', 1, 0, 1, 17, 4500.00, 15, 1, 1, NULL, '2017-04-04 10:22:24', '2017-04-04 10:22:24', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(110, 'IHIALA [IN ANAMBRA]', 1, 0, 1, 36, 4800.00, 15, 1, 1, NULL, '2017-04-04 10:26:15', '2017-04-04 10:26:15', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(111, 'ORLU JUNCTION [IN IMO]', 1, 0, 1, 39, 4800.00, 15, 1, 1, NULL, '2017-04-04 10:28:10', '2017-04-04 10:28:10', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(112, 'OGBAKU [IN IMO]', 1, 0, 1, 37, 4800.00, 15, 1, 1, NULL, '2017-04-04 10:28:42', '2017-04-04 10:28:42', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(113, 'MGBIDI [IN IMO]', 1, 0, 1, 38, 4800.00, 15, 1, 1, NULL, '2017-04-04 10:29:01', '2017-04-04 10:29:01', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(114, 'ABA [IN ABIA]', 1, 0, 1, 40, 5000.00, 16, 1, 1, NULL, '2017-04-04 10:30:26', '2017-04-04 10:30:26', 'First Bus', 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 1),
(115, 'ASABA [IN DELTA]', 1, 0, 1, 15, 4500.00, 16, 1, 1, NULL, '2017-04-04 10:31:01', '2017-04-04 10:31:01', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(116, 'ONITSHA [IN ANAMBRA]', 1, 0, 1, 16, 4500.00, 16, 1, 1, NULL, '2017-04-04 10:31:17', '2017-04-04 10:31:17', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(117, 'AGBOR [IN DELTA]', 1, 0, 1, 17, 4500.00, 16, 1, 1, NULL, '2017-04-04 10:31:30', '2017-04-04 10:31:30', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(118, 'ORLU JUNCTION [IN IMO]', 1, 0, 1, 39, 4800.00, 16, 1, 1, NULL, '2017-04-04 10:32:11', '2017-04-04 10:32:11', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(119, 'IHIALA [IN ANAMBRA]', 1, 0, 1, 36, 4800.00, 16, 1, 1, NULL, '2017-04-04 10:32:32', '2017-04-04 10:32:32', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(120, 'OGBAKU [IN IMO]', 1, 0, 1, 37, 4800.00, 16, 1, 1, NULL, '2017-04-04 10:32:54', '2017-04-04 10:32:54', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(121, 'MGBIDI [IN IMO]', 1, 0, 1, 38, 4800.00, 16, 1, 1, NULL, '2017-04-04 10:33:07', '2017-04-04 10:33:07', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(122, 'OWERRI [IN IMO]', 1, 0, 1, 35, 4800.00, 16, 1, 1, NULL, '2017-04-04 10:33:25', '2017-04-04 10:33:25', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(123, 'UMUAHIA [IN ABIA]', 1, 0, 1, 41, 5000.00, 17, 1, 1, NULL, '2017-04-04 10:34:53', '2017-04-04 10:34:53', 'First Bus', 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 1),
(124, 'MBISE [IN IMO]', 1, 0, 1, 42, 5000.00, 17, 1, 1, NULL, '2017-04-04 10:37:19', '2017-04-04 10:37:19', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(125, 'Mbano [IN IMO]', 1, 0, 1, 43, 5000.00, 17, 1, 1, NULL, '2017-04-04 10:37:31', '2017-04-04 10:37:31', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(126, 'NKWERE [IN IMO]', 1, 0, 1, 44, 5000.00, 17, 1, 1, NULL, '2017-04-04 10:37:42', '2017-04-04 10:37:42', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(127, 'ORLU INSIDE [IN IMO]', 1, 0, 1, 45, 5000.00, 17, 1, 1, NULL, '2017-04-04 10:38:42', '2017-04-04 10:38:42', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(128, 'PORT-HARCOURT [IN RIVERS]', 1, 0, 1, 46, 5500.00, 18, 1, 1, NULL, '2017-04-04 10:40:24', '2017-04-04 10:40:24', 'First Bus', 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 1),
(129, 'ASABA [IN DELTA]', 1, 0, 1, 15, 4500.00, 18, 1, 1, NULL, '2017-04-04 10:41:09', '2017-04-04 10:41:09', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(130, 'ONITSHA [IN ANAMBRA]', 1, 0, 1, 16, 4500.00, 18, 1, 1, NULL, '2017-04-04 10:41:24', '2017-04-04 10:41:24', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(131, 'AGBOR [IN DELTA]', 1, 0, 1, 17, 4500.00, 18, 1, 1, NULL, '2017-04-04 10:41:37', '2017-04-04 10:41:37', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(132, 'IHIALA [IN ANAMBRA]', 1, 0, 1, 36, 4800.00, 18, 1, 1, NULL, '2017-04-04 10:42:14', '2017-04-04 10:42:14', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(133, 'ORLU JUNCTION [IN IMO]', 1, 0, 1, 39, 4800.00, 18, 1, 1, NULL, '2017-04-04 10:42:29', '2017-04-04 10:42:29', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(134, 'OGBAKU [IN IMO]', 1, 0, 1, 37, 4800.00, 18, 1, 1, NULL, '2017-04-04 10:42:50', '2017-04-04 10:42:50', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(135, 'MGBIDI [IN IMO]', 1, 0, 1, 38, 4800.00, 18, 1, 1, NULL, '2017-04-04 10:43:03', '2017-04-04 10:43:03', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(136, 'OWERRI [IN IMO]', 1, 0, 1, 35, 4800.00, 18, 1, 1, NULL, '2017-04-04 10:43:25', '2017-04-04 10:43:25', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(137, 'ABUJA [IN ABUJA]', 1, 0, 1, 47, 7000.00, 19, 1, 1, NULL, '2017-04-04 10:45:05', '2017-04-04 10:45:05', 'First Bus', 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 1),
(138, 'LOKOJA [IN KOGI]', 1, 0, 1, 48, 7000.00, 19, 1, 1, NULL, '2017-04-04 10:49:23', '2017-04-04 10:49:23', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(139, 'OKENE [IN KOGI]', 1, 0, 1, 49, 7000.00, 19, 1, 1, NULL, '2017-04-04 10:49:40', '2017-04-04 10:49:40', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(140, 'GWAGWALADA [IN ABUJA]', 1, 0, 1, 50, 7000.00, 19, 1, 1, NULL, '2017-04-04 10:49:55', '2017-04-04 10:49:55', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(141, 'UTAKO [IN ABUJA]', 1, 0, 1, 51, 7000.00, 19, 1, 1, NULL, '2017-04-04 10:50:10', '2017-04-04 10:50:10', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(142, 'AIRPORT ROAD ZUBA [IN ABUJA]', 1, 0, 1, 52, 7000.00, 19, 1, 1, NULL, '2017-04-04 10:50:23', '2017-04-04 10:50:23', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(143, 'ONITSHA [IN ANAMBRA]', 1, 0, 1, 16, 3000.00, 20, 1, 1, NULL, '2017-04-04 13:20:40', '2017-04-04 13:20:40', 'First Bus', 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 1),
(144, '4 CORNER [IN ENUGU]', 1, 0, 1, 53, 5000.00, 10, 1, 1, NULL, '2017-04-06 08:13:48', '2017-04-06 08:13:48', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(145, '4 CORNER [IN ENUGU]', 1, 0, 1, 53, 5000.00, 12, 1, 1, NULL, '2017-04-06 08:15:54', '2017-04-06 08:15:54', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(146, 'AWO JUNCTION [IN IMO]', 1, 0, 1, 54, 4800.00, 15, 1, 1, NULL, '2017-04-06 08:18:03', '2017-04-06 08:18:03', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0),
(147, 'AWO JUNCTION [IN IMO]', 1, 0, 1, 54, 4800.00, 16, 1, 1, NULL, '2017-04-06 08:19:42', '2017-04-06 08:19:42', NULL, 0, 0, 0, '0.00', '0.00', '0.00', '0.00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE `transactions` (
  `id` int(10) UNSIGNED NOT NULL,
  `operator_id` int(10) UNSIGNED NOT NULL,
  `booking_id` int(10) UNSIGNED NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `response` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `trips`
--

CREATE TABLE `trips` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `operator_id` int(10) UNSIGNED NOT NULL,
  `source_park_id` int(10) UNSIGNED NOT NULL,
  `dest_park_id` int(10) UNSIGNED NOT NULL,
  `bus_id` int(10) UNSIGNED DEFAULT NULL,
  `driver_id` int(10) UNSIGNED DEFAULT NULL,
  `departure_time` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `duration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fare` double(8,2) NOT NULL,
  `no_of_seats` int(11) NOT NULL,
  `parent_trip_id` int(10) UNSIGNED DEFAULT NULL,
  `ac` tinyint(1) NOT NULL DEFAULT '0',
  `security` tinyint(1) NOT NULL DEFAULT '0',
  `tv` tinyint(1) NOT NULL DEFAULT '0',
  `insurance` tinyint(1) NOT NULL DEFAULT '0',
  `passport` tinyint(1) NOT NULL DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `operator_trip_id` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `trip_transit` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `trips`
--

INSERT INTO `trips` (`id`, `name`, `operator_id`, `source_park_id`, `dest_park_id`, `bus_id`, `driver_id`, `departure_time`, `duration`, `fare`, `no_of_seats`, `parent_trip_id`, `ac`, `security`, `tv`, `insurance`, `passport`, `active`, `operator_trip_id`, `deleted_at`, `created_at`, `updated_at`, `trip_transit`) VALUES
(1, 'OKOTA [IN LAGOS]__AWKA->5000', 1, 1, 14, 1, 1, '', '', 5000.00, 0, NULL, 1, 0, 1, 1, 0, 1, 0, NULL, '2017-04-03 13:16:55', '2017-04-03 13:16:55', NULL),
(3, 'OKOTA [IN LAGOS]__ENUGU [IN ENUGU]->5000', 1, 1, 18, 1, 1, '', '', 5000.00, 0, NULL, 1, 0, 1, 1, 0, 1, 0, NULL, '2017-04-03 13:56:09', '2017-04-03 14:09:03', NULL),
(4, 'OKOTA [IN LAGOS]__NSUKKA [IN ENUGU]->5000', 1, 1, 23, 2, 2, '', '', 5000.00, 0, NULL, 1, 0, 1, 1, 0, 1, 0, NULL, '2017-04-03 14:54:07', '2017-04-03 14:54:07', NULL),
(5, 'OKOTA [IN LAGOS]__ABAKALIKI [IN EBONYI]->5500', 1, 1, 24, 2, 2, '', '', 5500.00, 0, NULL, 1, 0, 1, 1, 0, 1, 0, NULL, '2017-04-03 15:00:40', '2017-04-03 15:00:40', NULL),
(6, 'OKOTA [IN LAGOS]__NNEWI [IN ANAMBRA]->4800', 1, 1, 25, 5, 5, '', '', 4800.00, 0, NULL, 1, 0, 1, 1, 0, 1, 0, NULL, '2017-04-04 09:39:35', '2017-04-04 09:39:35', NULL),
(7, 'OKOTA [IN LAGOS]__EKWULOBIA [IN ANAMBRA]->4800', 1, 1, 26, 6, 6, '', '', 4800.00, 0, NULL, 1, 0, 1, 1, 0, 1, 0, NULL, '2017-04-04 09:42:47', '2017-04-04 09:42:47', NULL),
(8, 'OKOTA [IN LAGOS]__AFIKPO [IN EBONYI]->6000', 1, 1, 27, 7, 7, '', '', 6000.00, 0, NULL, 1, 0, 1, 1, 0, 1, 0, NULL, '2017-04-04 09:45:30', '2017-04-04 09:45:30', NULL),
(9, 'OKOTA [IN LAGOS]__UGEP [IN CROSS-RIVER]->7000', 1, 1, 30, 8, 8, '', '', 7000.00, 0, NULL, 1, 0, 1, 1, 0, 1, 0, NULL, '2017-04-04 09:50:54', '2017-04-04 09:50:54', NULL),
(10, 'OKOTA [IN LAGOS]__OJI-RIVER [IN ENUGU]->5000', 1, 1, 22, 10, 10, '', '', 5000.00, 0, NULL, 1, 0, 1, 1, 0, 1, 0, NULL, '2017-04-04 09:53:50', '2017-04-04 09:53:50', NULL),
(11, 'OKOTA [IN LAGOS]__AWGU [IN ENUGU]->5000', 1, 1, 31, 11, 11, '', '', 5000.00, 0, NULL, 1, 0, 1, 1, 0, 1, 0, NULL, '2017-04-04 09:58:39', '2017-04-04 09:58:39', NULL),
(12, 'OKOTA [IN LAGOS]__OKPOSI [IN EBONYI]->5500', 1, 1, 32, 12, 12, '', '', 5500.00, 0, NULL, 1, 0, 1, 1, 0, 1, 0, NULL, '2017-04-04 10:04:57', '2017-04-04 10:04:57', NULL),
(13, 'OKOTA [IN LAGOS]__OGOJA [IN CROSS-RIVER]->7000', 1, 1, 33, 13, 13, '', '', 7000.00, 0, NULL, 1, 0, 1, 1, 0, 1, 0, NULL, '2017-04-04 10:10:08', '2017-04-04 10:10:08', NULL),
(14, 'OKOTA [IN LAGOS]__IKOM [IN CROSS-RIVER]->7000', 1, 1, 34, 14, 14, '', '', 7000.00, 0, NULL, 1, 0, 1, 1, 0, 1, 0, NULL, '2017-04-04 10:14:17', '2017-04-04 10:14:17', NULL),
(15, 'OKOTA [IN LAGOS]__OWERRI [IN IMO]->4800', 1, 1, 35, 15, 15, '', '', 4800.00, 0, NULL, 1, 0, 1, 1, 0, 1, 0, NULL, '2017-04-04 10:21:34', '2017-04-04 10:21:34', NULL),
(16, 'OKOTA [IN LAGOS]__ABA [IN ABIA]->5000', 1, 1, 40, 16, 16, '', '', 5000.00, 0, NULL, 1, 0, 1, 1, 0, 1, 0, NULL, '2017-04-04 10:30:26', '2017-04-04 10:30:26', NULL),
(17, 'OKOTA [IN LAGOS]__UMUAHIA [IN ABIA]->5000', 1, 1, 41, 17, 17, '', '', 5000.00, 0, NULL, 1, 0, 1, 1, 0, 1, 0, NULL, '2017-04-04 10:34:53', '2017-04-04 10:34:53', NULL),
(18, 'OKOTA [IN LAGOS]__PORT-HARCOURT [IN RIVERS]->5500', 1, 1, 46, 18, 18, '', '', 5500.00, 0, NULL, 1, 0, 1, 1, 0, 1, 0, NULL, '2017-04-04 10:40:24', '2017-04-04 10:40:24', NULL),
(19, 'OKOTA [IN LAGOS]__ABUJA [IN ABUJA]->7000', 1, 1, 47, 19, 19, '', '', 7000.00, 0, NULL, 1, 0, 1, 1, 0, 1, 0, NULL, '2017-04-04 10:45:05', '2017-04-04 10:45:05', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `trip_stops`
--

CREATE TABLE `trip_stops` (
  `id` int(10) UNSIGNED NOT NULL,
  `operator_id` int(10) UNSIGNED NOT NULL,
  `trip_id` int(10) UNSIGNED NOT NULL,
  `stops` text COLLATE utf8_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `operator_id` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `role_id` int(10) UNSIGNED NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `exposed` int(4) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `operator_id`, `first_name`, `last_name`, `email`, `password`, `phone`, `address`, `active`, `role_id`, `remember_token`, `deleted_at`, `created_at`, `updated_at`, `exposed`) VALUES
(15, 1, 'Chidinma', 'Egwuenu', 'chidinma@efex.com', '$2y$10$QRv0AablzgSl.Fhtl70S4O0Bxin.swqPiRSXBJQtBX/KoOFjkDKrO', '07063969675', 'Yaba', 1, 5, 'B2hcSJu7VqyVwi4bpr9lMlLjzdPvqogEgbk0pfzPYEvuRUi5sObH0L44KgCF', NULL, '2016-12-06 16:40:22', '2017-04-25 11:20:57', 1),
(12, 1, 'onyinye', 'mbamare', 'onyinye@efex.com', '$2y$10$zK19N4ls4JGJnkhmpGh6T.Wt5uuMKWZo07s4L3yqN2LlFHH9Nvw0O', '08073283002', 'efex', 1, 2, 'qFag3nBeXmSChesuVyIUWY3yfBywPGxHOmdvn6tFdo3mLcYy9QWkBSEjc1nU', NULL, '2016-11-07 17:24:14', '2017-01-05 15:05:12', 1),
(13, 1, 'Eucharia', 'onwuagbazu', 'Eucharia@efex.com', '$2y$10$0QbXwU5BO082S24eLPevJ.5IdrqllyUvqzOIlUASd2J6zAgTZCEwq', '08077790262', 'efex', 1, 2, NULL, NULL, '2016-11-07 17:35:17', '2016-11-07 17:35:17', 1),
(14, 1, 'Anne', 'Ogbemudia', 'Anne@efex.com', '$2y$10$Pi0nbFgXNwmZnxVddFRZX.Fi4HNSDoAzsaqHOsGiSjlw9kqKeHC.K', '08055060678', 'efex', 1, 2, 'wXL6LXUxmDUWN1TSt9cOMRFm9i6vKoK5DImZpjN7BJrCh2oNlLYT2CQWRm3G', NULL, '2016-11-07 17:44:39', '2017-04-07 08:46:30', 1),
(10, 1, 'Admin', 'Bus', 'admin@bus.com', '$2y$10$60tqlsORxMEblkri.8a.zub6lP3xNnWeJ0JardkPbx/kVFHyQuASC', '08025455574', '', 1, 1, 'dcGdl63tKI550kewzBxSNvgnYL4ISLGMm8WTG2cEfoQCmHCvPYW4b2GimMss', NULL, NULL, '2017-04-25 10:32:05', 0),
(11, 1, 'Jonathan', 'Efex', 'jonathan@efex.com', '$2y$10$xYOsuFvKKsZ1owzORx8lGODN71wC.vOdSfHWiY1MDXSnEQNtpIAoK', '080', '', 1, 1, 'JW4vY1mljbOImmQuQFQHqZjcXxYjYvrKlxzyeWiUmL6Z6g6TqtJxCUki4SRm', NULL, '2016-11-01 14:16:56', '2017-01-04 12:12:27', 1),
(6, 0, 'Ayodele', 'Oshunlana', 'sophie@ayolana.com', '$2y$10$60tqlsORxMEblkri.8a.zub6lP3xNnWeJ0JardkPbx/kVFHyQuASC', '+2348038953794', '13, Arigbanla street,', 1, 1, 'n9pdvTEzmF4vb5ymSxgjOaL11yzho1J7NkUBLyaHQNnF47hbC4nCLaS56ykA', NULL, '2016-03-09 07:43:42', '2016-03-22 10:33:12', 1),
(7, 0, 'Ayula', 'Bosede', 'me@ayolana.com', '$2y$10$0XqVj6YGiRVezmMr3DDwLuOpfLf5gy/ZJujl0SG7XblosMe6/yF96', '08133772946', '', 1, 1, 'y8cChupAkTRchuTVnhfP9lgJpqOOko7nxI10RKH8tplGG5ph7xrdaG00pTSN', NULL, '2016-03-12 16:58:29', '2016-03-12 17:11:49', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users_destinations`
--

CREATE TABLE `users_destinations` (
  `id` int(10) UNSIGNED NOT NULL,
  `operator_id` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `user_id` int(10) UNSIGNED NOT NULL,
  `park_id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users_destinations`
--

INSERT INTO `users_destinations` (`id`, `operator_id`, `user_id`, `park_id`) VALUES
(316, 1, 2, 51),
(315, 1, 2, 50),
(314, 1, 2, 49),
(313, 1, 2, 48),
(312, 1, 2, 47),
(311, 1, 2, 46),
(310, 1, 2, 45),
(309, 1, 2, 44),
(308, 1, 2, 43),
(307, 1, 2, 42),
(306, 1, 2, 41),
(305, 1, 2, 40),
(304, 1, 2, 39),
(303, 1, 2, 38),
(302, 1, 2, 37),
(301, 1, 2, 36),
(300, 1, 2, 35),
(299, 1, 2, 34),
(298, 1, 2, 33),
(297, 1, 2, 32),
(296, 1, 2, 31),
(295, 1, 2, 30),
(294, 1, 2, 29),
(293, 1, 2, 28),
(292, 1, 2, 27),
(291, 1, 2, 26),
(290, 1, 2, 24),
(289, 1, 2, 23),
(288, 1, 2, 22),
(287, 1, 2, 21),
(286, 1, 2, 20),
(285, 1, 2, 19),
(284, 1, 2, 18),
(283, 1, 2, 17),
(282, 1, 2, 16),
(281, 1, 2, 15),
(280, 1, 2, 14),
(279, 1, 2, 13),
(278, 1, 2, 12),
(277, 1, 2, 11),
(276, 1, 2, 10),
(275, 1, 2, 9),
(274, 1, 2, 8),
(273, 1, 2, 7),
(272, 1, 2, 6),
(271, 1, 2, 5),
(270, 1, 2, 4),
(269, 1, 2, 3),
(268, 1, 2, 2),
(267, 1, 18, 1),
(52, 1, 4, 2),
(53, 1, 4, 3),
(54, 1, 4, 4),
(55, 1, 4, 5),
(56, 1, 4, 6),
(57, 1, 4, 7),
(58, 1, 4, 8),
(59, 1, 4, 9),
(60, 1, 4, 10),
(61, 1, 4, 11),
(62, 1, 4, 12),
(63, 1, 4, 13),
(64, 1, 4, 14),
(65, 1, 4, 15),
(66, 1, 4, 16),
(67, 1, 4, 17),
(68, 1, 4, 18),
(69, 1, 4, 19),
(70, 1, 4, 20),
(71, 1, 4, 21),
(72, 1, 4, 22),
(73, 1, 4, 23),
(74, 1, 4, 24),
(75, 1, 4, 26),
(76, 1, 4, 27),
(77, 1, 4, 28),
(78, 1, 4, 29),
(79, 1, 4, 30),
(80, 1, 4, 31),
(81, 1, 4, 32),
(82, 1, 4, 33),
(83, 1, 4, 34),
(84, 1, 4, 35),
(85, 1, 4, 36),
(86, 1, 4, 37),
(87, 1, 4, 38),
(88, 1, 4, 39),
(89, 1, 4, 40),
(90, 1, 4, 41),
(91, 1, 4, 42),
(92, 1, 4, 43),
(93, 1, 4, 44),
(94, 1, 4, 45),
(95, 1, 5, 2),
(96, 1, 5, 3),
(97, 1, 5, 4),
(98, 1, 5, 5),
(99, 1, 5, 6),
(100, 1, 5, 7),
(101, 1, 5, 8),
(102, 1, 5, 9),
(103, 1, 5, 10),
(104, 1, 5, 11),
(105, 1, 5, 12),
(106, 1, 5, 13),
(107, 1, 5, 14),
(108, 1, 5, 15),
(109, 1, 5, 16),
(110, 1, 5, 17),
(111, 1, 5, 18),
(112, 1, 5, 19),
(113, 1, 5, 20),
(114, 1, 5, 21),
(115, 1, 5, 22),
(116, 1, 5, 23),
(117, 1, 5, 24),
(118, 1, 5, 26),
(119, 1, 5, 27),
(120, 1, 5, 28),
(121, 1, 5, 29),
(122, 1, 5, 30),
(123, 1, 5, 31),
(124, 1, 5, 32),
(125, 1, 5, 33),
(126, 1, 5, 34),
(127, 1, 5, 35),
(128, 1, 5, 36),
(129, 1, 5, 37),
(130, 1, 5, 38),
(131, 1, 5, 39),
(132, 1, 5, 40),
(133, 1, 5, 41),
(134, 1, 5, 42),
(135, 1, 5, 43),
(136, 1, 5, 44),
(137, 1, 5, 45),
(138, 1, 7, 2),
(139, 1, 7, 3),
(140, 1, 7, 4),
(141, 1, 7, 5),
(142, 1, 7, 6),
(143, 1, 7, 7),
(144, 1, 7, 8),
(145, 1, 7, 9),
(146, 1, 7, 10),
(147, 1, 7, 11),
(148, 1, 7, 12),
(149, 1, 7, 13),
(150, 1, 7, 14),
(151, 1, 7, 15),
(152, 1, 7, 16),
(153, 1, 7, 17),
(154, 1, 7, 18),
(155, 1, 7, 19),
(156, 1, 7, 20),
(157, 1, 7, 21),
(158, 1, 7, 22),
(159, 1, 7, 23),
(160, 1, 7, 24),
(161, 1, 7, 26),
(162, 1, 7, 27),
(163, 1, 7, 28),
(164, 1, 7, 29),
(165, 1, 7, 30),
(166, 1, 7, 31),
(167, 1, 7, 32),
(168, 1, 7, 33),
(169, 1, 7, 34),
(170, 1, 7, 35),
(171, 1, 7, 36),
(172, 1, 7, 37),
(173, 1, 7, 38),
(174, 1, 7, 39),
(175, 1, 7, 40),
(176, 1, 7, 41),
(177, 1, 7, 42),
(178, 1, 7, 43),
(179, 1, 7, 44),
(180, 1, 7, 45),
(181, 1, 9, 2),
(182, 1, 9, 3),
(183, 1, 9, 4),
(184, 1, 9, 5),
(185, 1, 9, 6),
(186, 1, 9, 7),
(187, 1, 9, 8),
(188, 1, 9, 9),
(189, 1, 9, 10),
(190, 1, 9, 11),
(191, 1, 9, 12),
(192, 1, 9, 13),
(193, 1, 9, 14),
(194, 1, 9, 15),
(195, 1, 9, 16),
(196, 1, 9, 17),
(197, 1, 9, 18),
(198, 1, 9, 19),
(199, 1, 9, 20),
(200, 1, 9, 21),
(201, 1, 9, 22),
(202, 1, 9, 23),
(203, 1, 9, 24),
(204, 1, 9, 26),
(205, 1, 9, 27),
(206, 1, 9, 28),
(207, 1, 9, 29),
(208, 1, 9, 30),
(209, 1, 9, 31),
(210, 1, 9, 32),
(211, 1, 9, 33),
(212, 1, 9, 34),
(213, 1, 9, 35),
(214, 1, 9, 36),
(215, 1, 9, 37),
(216, 1, 9, 38),
(217, 1, 9, 39),
(218, 1, 9, 40),
(219, 1, 9, 41),
(220, 1, 9, 42),
(221, 1, 9, 43),
(222, 1, 9, 44),
(223, 1, 9, 45),
(224, 1, 10, 2),
(225, 1, 10, 3),
(226, 1, 10, 4),
(227, 1, 10, 5),
(228, 1, 10, 6),
(229, 1, 10, 7),
(230, 1, 10, 8),
(231, 1, 10, 9),
(232, 1, 10, 10),
(233, 1, 10, 11),
(234, 1, 10, 12),
(235, 1, 10, 13),
(236, 1, 10, 14),
(237, 1, 10, 15),
(238, 1, 10, 16),
(239, 1, 10, 17),
(240, 1, 10, 18),
(241, 1, 10, 19),
(242, 1, 10, 20),
(243, 1, 10, 21),
(244, 1, 10, 22),
(245, 1, 10, 23),
(246, 1, 10, 24),
(247, 1, 10, 26),
(248, 1, 10, 27),
(249, 1, 10, 28),
(250, 1, 10, 29),
(251, 1, 10, 30),
(252, 1, 10, 31),
(253, 1, 10, 32),
(254, 1, 10, 33),
(255, 1, 10, 34),
(256, 1, 10, 35),
(257, 1, 10, 36),
(258, 1, 10, 37),
(259, 1, 10, 38),
(260, 1, 10, 39),
(261, 1, 10, 40),
(262, 1, 10, 41),
(263, 1, 10, 42),
(264, 1, 10, 43),
(265, 1, 10, 44),
(266, 1, 10, 45),
(317, 1, 14, 22),
(318, 1, 14, 18),
(320, 1, 14, 20),
(323, 1, 15, 19),
(324, 1, 15, 15);

-- --------------------------------------------------------

--
-- Table structure for table `users_parks`
--

CREATE TABLE `users_parks` (
  `id` int(10) UNSIGNED NOT NULL,
  `operator_id` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `user_id` int(10) UNSIGNED NOT NULL,
  `park_id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users_parks`
--

INSERT INTO `users_parks` (`id`, `operator_id`, `user_id`, `park_id`) VALUES
(1, 1, 2, 1),
(17, 1, 18, 1),
(16, 1, 17, 1),
(15, 1, 16, 1),
(14, 1, 15, 1),
(22, 1, 14, 1),
(20, 1, 13, 3),
(11, 1, 12, 1),
(10, 1, 11, 1),
(18, 1, 19, 1),
(19, 1, 10, 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_logs`
--

CREATE TABLE `user_logs` (
  `id` int(10) UNSIGNED NOT NULL,
  `operator_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `log_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `details` text COLLATE utf8_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `agents`
--
ALTER TABLE `agents`
  ADD PRIMARY KEY (`id`),
  ADD KEY `agents_operator_id_foreign` (`operator_id`);

--
-- Indexes for table `agent_transactions`
--
ALTER TABLE `agent_transactions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `agent_purchases_agent_id_index` (`agent_id`),
  ADD KEY `agent_transactions_operator_id_foreign` (`operator_id`);

--
-- Indexes for table `banks`
--
ALTER TABLE `banks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `banks_operator_id_foreign` (`operator_id`);

--
-- Indexes for table `bookings`
--
ALTER TABLE `bookings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `bookings_payment_method_id_index` (`payment_method_id`),
  ADD KEY `bookings_bank_id_index` (`bank_id`),
  ADD KEY `bookings_parent_booking_id_index` (`parent_booking_id`),
  ADD KEY `bookings_daily_trip_id_index` (`daily_trip_id`),
  ADD KEY `bookings_operator_id_foreign` (`operator_id`);

--
-- Indexes for table `booking_notes`
--
ALTER TABLE `booking_notes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `booking_notes_booking_id_index` (`booking_id`),
  ADD KEY `booking_notes_user_id_index` (`user_id`),
  ADD KEY `booking_notes_operator_id_foreign` (`operator_id`);

--
-- Indexes for table `booking_refunds`
--
ALTER TABLE `booking_refunds`
  ADD PRIMARY KEY (`id`),
  ADD KEY `booking_refunds_booking_id_index` (`booking_id`),
  ADD KEY `booking_refunds_user_id_index` (`user_id`),
  ADD KEY `booking_refunds_approved_user_id_index` (`approved_user_id`),
  ADD KEY `booking_refunds_operator_id_foreign` (`operator_id`);

--
-- Indexes for table `buses`
--
ALTER TABLE `buses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `buses_bus_type_id_index` (`bus_type_id`),
  ADD KEY `buses_default_park_id_index` (`default_park_id`),
  ADD KEY `buses_default_driver_id_index` (`default_driver_id`),
  ADD KEY `buses_default_trip_id_index` (`default_trip_id`),
  ADD KEY `buses_operator_id_foreign` (`operator_id`);

--
-- Indexes for table `bus_types`
--
ALTER TABLE `bus_types`
  ADD PRIMARY KEY (`id`),
  ADD KEY `bus_types_operator_id_foreign` (`operator_id`);

--
-- Indexes for table `call_logs`
--
ALTER TABLE `call_logs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `call_logs_booking_id_index` (`booking_id`),
  ADD KEY `call_logs_user_id_index` (`user_id`),
  ADD KEY `call_logs_operator_id_foreign` (`operator_id`);

--
-- Indexes for table `cash_offices`
--
ALTER TABLE `cash_offices`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cash_offices_operator_id_foreign` (`operator_id`);

--
-- Indexes for table `charter_bookings`
--
ALTER TABLE `charter_bookings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `charter_bookings_park_id_index` (`park_id`),
  ADD KEY `charter_bookings_bus_id_index` (`bus_id`),
  ADD KEY `charter_bookings_driver_id_index` (`driver_id`),
  ADD KEY `charter_bookings_payment_method_id_index` (`payment_method_id`),
  ADD KEY `charter_bookings_operator_id_foreign` (`operator_id`);

--
-- Indexes for table `daily_trips`
--
ALTER TABLE `daily_trips`
  ADD PRIMARY KEY (`id`),
  ADD KEY `daily_trips_trip_date_index` (`trip_date`),
  ADD KEY `daily_trips_trip_id_index` (`trip_id`),
  ADD KEY `daily_trips_driver_id_index` (`driver_id`),
  ADD KEY `daily_trips_bus_id_index` (`bus_id`),
  ADD KEY `daily_trips_status_index` (`status`),
  ADD KEY `daily_trips_operator_id_foreign` (`operator_id`);

--
-- Indexes for table `daily_trip_expenses`
--
ALTER TABLE `daily_trip_expenses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `devices`
--
ALTER TABLE `devices`
  ADD PRIMARY KEY (`id`),
  ADD KEY `devices_assigned_to_index` (`assigned_to`),
  ADD KEY `devices_operator_id_index` (`operator_id`);

--
-- Indexes for table `discounts`
--
ALTER TABLE `discounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `drivers`
--
ALTER TABLE `drivers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `drivers_default_park_id_index` (`default_park_id`),
  ADD KEY `drivers_operator_id_foreign` (`operator_id`);

--
-- Indexes for table `extra_luggages`
--
ALTER TABLE `extra_luggages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `incidents`
--
ALTER TABLE `incidents`
  ADD PRIMARY KEY (`id`),
  ADD KEY `incidents_incident_type_id_index` (`incident_type_id`),
  ADD KEY `incidents_operator_id_foreign` (`operator_id`);

--
-- Indexes for table `incident_types`
--
ALTER TABLE `incident_types`
  ADD PRIMARY KEY (`id`),
  ADD KEY `incident_types_operator_id_foreign` (`operator_id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `messages_sender_id_index` (`sender_id`),
  ADD KEY `messages_recipient_id_index` (`recipient_id`),
  ADD KEY `messages_operator_id_foreign` (`operator_id`);

--
-- Indexes for table `operators`
--
ALTER TABLE `operators`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `parks`
--
ALTER TABLE `parks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parks_state_id_index` (`state_id`),
  ADD KEY `parks_operator_id_foreign` (`operator_id`);

--
-- Indexes for table `park_stops`
--
ALTER TABLE `park_stops`
  ADD PRIMARY KEY (`id`),
  ADD KEY `park_stops_park_id_index` (`park_id`),
  ADD KEY `park_stops_operator_id_foreign` (`operator_id`);

--
-- Indexes for table `passengers`
--
ALTER TABLE `passengers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `passengers_booking_id_index` (`booking_id`),
  ADD KEY `passengers_operator_id_foreign` (`operator_id`),
  ADD KEY `passengers_daily_trip_id_foreign` (`daily_trip_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `payment_methods`
--
ALTER TABLE `payment_methods`
  ADD PRIMARY KEY (`id`),
  ADD KEY `payment_methods_bank_id_index` (`bank_id`);

--
-- Indexes for table `queue_jobs`
--
ALTER TABLE `queue_jobs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `queue_jobs_queue_reserved_reserved_at_index` (`queue`,`reserved`,`reserved_at`),
  ADD KEY `queue_jobs_operator_id_foreign` (`operator_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `roles_operator_id_foreign` (`operator_id`);

--
-- Indexes for table `seats`
--
ALTER TABLE `seats`
  ADD PRIMARY KEY (`id`),
  ADD KEY `seats_daily_trip_id_index` (`daily_trip_id`),
  ADD KEY `seats_booking_id_index` (`booking_id`),
  ADD KEY `seats_operator_id_foreign` (`operator_id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `settings_operator_id_foreign` (`operator_id`);

--
-- Indexes for table `states`
--
ALTER TABLE `states`
  ADD PRIMARY KEY (`id`),
  ADD KEY `states_operator_id_foreign` (`operator_id`);

--
-- Indexes for table `sub_trips`
--
ALTER TABLE `sub_trips`
  ADD PRIMARY KEY (`id`),
  ADD KEY `subtrips_operator_id_index` (`operator_id`),
  ADD KEY `subtrips_source_park_id_index` (`source_park_id`),
  ADD KEY `subtrips_dest_park_id_index` (`dest_park_id`),
  ADD KEY `subtrips_parent_trip_id_index` (`parent_trip_id`),
  ADD KEY `sub_trips_state_id_foreign` (`state_id`);

--
-- Indexes for table `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `transactions_booking_id_index` (`booking_id`),
  ADD KEY `transactions_operator_id_foreign` (`operator_id`);

--
-- Indexes for table `trips`
--
ALTER TABLE `trips`
  ADD PRIMARY KEY (`id`),
  ADD KEY `trips_operator_id_index` (`operator_id`),
  ADD KEY `trips_source_park_id_index` (`source_park_id`),
  ADD KEY `trips_dest_park_id_index` (`dest_park_id`),
  ADD KEY `trips_parent_trip_id_index` (`parent_trip_id`),
  ADD KEY `trips_bus_id_index` (`bus_id`),
  ADD KEY `trips_driver_id_index` (`driver_id`);

--
-- Indexes for table `trip_stops`
--
ALTER TABLE `trip_stops`
  ADD PRIMARY KEY (`id`),
  ADD KEY `trip_stops_trip_id_index` (`trip_id`),
  ADD KEY `trip_stops_operator_id_foreign` (`operator_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_role_id_index` (`role_id`),
  ADD KEY `users_operator_id_foreign` (`operator_id`);

--
-- Indexes for table `users_destinations`
--
ALTER TABLE `users_destinations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `users_destinations_user_id_index` (`user_id`),
  ADD KEY `users_destinations_park_id_index` (`park_id`),
  ADD KEY `users_destinations_operator_id_foreign` (`operator_id`);

--
-- Indexes for table `users_parks`
--
ALTER TABLE `users_parks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `users_parks_user_id_index` (`user_id`),
  ADD KEY `users_parks_park_id_index` (`park_id`),
  ADD KEY `users_parks_operator_id_foreign` (`operator_id`);

--
-- Indexes for table `user_logs`
--
ALTER TABLE `user_logs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_logs_user_id_index` (`user_id`),
  ADD KEY `user_logs_operator_id_foreign` (`operator_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `agents`
--
ALTER TABLE `agents`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `agent_transactions`
--
ALTER TABLE `agent_transactions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `banks`
--
ALTER TABLE `banks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `bookings`
--
ALTER TABLE `bookings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;
--
-- AUTO_INCREMENT for table `booking_notes`
--
ALTER TABLE `booking_notes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `booking_refunds`
--
ALTER TABLE `booking_refunds`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `buses`
--
ALTER TABLE `buses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT for table `bus_types`
--
ALTER TABLE `bus_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `call_logs`
--
ALTER TABLE `call_logs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cash_offices`
--
ALTER TABLE `cash_offices`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `charter_bookings`
--
ALTER TABLE `charter_bookings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `daily_trips`
--
ALTER TABLE `daily_trips`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `daily_trip_expenses`
--
ALTER TABLE `daily_trip_expenses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `devices`
--
ALTER TABLE `devices`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `discounts`
--
ALTER TABLE `discounts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `drivers`
--
ALTER TABLE `drivers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `extra_luggages`
--
ALTER TABLE `extra_luggages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `incidents`
--
ALTER TABLE `incidents`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `incident_types`
--
ALTER TABLE `incident_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `operators`
--
ALTER TABLE `operators`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `parks`
--
ALTER TABLE `parks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;
--
-- AUTO_INCREMENT for table `park_stops`
--
ALTER TABLE `park_stops`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `passengers`
--
ALTER TABLE `passengers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;
--
-- AUTO_INCREMENT for table `payment_methods`
--
ALTER TABLE `payment_methods`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `queue_jobs`
--
ALTER TABLE `queue_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `seats`
--
ALTER TABLE `seats`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=78;
--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `states`
--
ALTER TABLE `states`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;
--
-- AUTO_INCREMENT for table `sub_trips`
--
ALTER TABLE `sub_trips`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=148;
--
-- AUTO_INCREMENT for table `transactions`
--
ALTER TABLE `transactions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `trips`
--
ALTER TABLE `trips`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `trip_stops`
--
ALTER TABLE `trip_stops`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `users_destinations`
--
ALTER TABLE `users_destinations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=325;
--
-- AUTO_INCREMENT for table `users_parks`
--
ALTER TABLE `users_parks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `user_logs`
--
ALTER TABLE `user_logs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;