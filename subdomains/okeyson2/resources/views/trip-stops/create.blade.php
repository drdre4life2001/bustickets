@extends('layouts.master')

@section('content')

  


   <div class="content-header">
        <h2 class="content-header-title">Add Trip Stops </h2>
        <ol class="breadcrumb">
          <li><a href="{{ url('/') }}">Dashboard</a></li>
          <li><a href="{{ url('trips') }}">Trip Stops</a></li>
          <li class="active">Add Trip Stop</li>
        </ol>
      </div> <!-- /.content-header -->

<script>
$(document).ready(function(){
var globalArr =[];
    $("#tripDetails").hide();
    $("#errorDiv").hide();
   var trimmedStops;

    // blur event on destination park
    $("#dest_park").on('blur', function(){
     
     var source_park =  $("#source_park").val();
     
     var dest_park =  $("#dest_park").val();

     if(source_park =="Choose Park" ||dest_park =="Choose Park")
     {
        alert("Park has not been selected")
     }
         else
         {

             // use source and dest to perform ajax request (get trip details)
               $.ajax({
                            url: '{{ route("get-all-trip-details") }}',
                            dataType: 'text',
                            type: 'GET',
                            contentType: 'application/x-www-form-urlencoded',
                            data: {"source_park":source_park,"dest_park":dest_park},
                            success: function (rslts, textStatus, jQxhr)
                            {
                              
                                var obj = JSON.parse(rslts);
                                var arr = new Array(obj.length);
                                for(var i =0; i<arr.length; i++)
                                {
                                  arr[i] =obj[i].id;
                                  console.log(arr[i]);
                                  globalArr.push(arr[i]);
                                }


                            },
                            error: function (jqXhr,textStatus, errorThrown)
                            {
                                console.log(errorThrown);
                                console.log("no such trip");
                                 $("#trip_stops").prop("readonly", true);
                            }

              });
               console.log("====================");
         }
 
      });

    // form submission
    $("#trip_stops").on('blur', function(){
      // make sure format is right
      var stops = $("#trip_stops").val();
      var stopsArr = stops.split(",");
     
      if(stopsArr[stopsArr.length-1] =="")
      {
        stopsArr.length -= 1;
      }
      // new string trimmed
      trimmedStops = stopsArr.join();
     // to chack for invalid comma
      var re = new RegExp(".*?,,+.*"); 
      
      if (re.test(trimmedStops)) {
        $("#subBtn").attr('disabled',true);
       alert("Error: Consecutive commmas"); 
       }
       else
       {
         $("#subBtn").attr('disabled',false);
       }
    });

   // send to db
    $("#subBtn").on('click', function(e){
        e.preventDefault();
        // bulk insert on globalArr
var tripstops = $("#trip_stops").val();
        $.ajax({
                            url: '{{ route("save-trip-stops") }}',
                            dataType: 'text',
                            type: 'GET',
                            contentType: 'application/x-www-form-urlencoded',
                            data: { paramName: globalArr.toString(), trip_stops:tripstops },
                            success: function (rslts, textStatus, jQxhr)
                            {
                              window.location.href= '../';
                            },
                            error: function (jqXhr,textStatus, errorThrown)
                            {
                                console.log(errorThrown);
                            }

              });
       

    });
});
</script>      

      <div class="row">

        <div class="col-md-10 col-sm-8">

          <div class="portlet">

            <div class="portlet-content">

                {!! Form::open(['class' => 'form-horizontal']) !!}

                <input type="hidden" name="operator_id" value="1">

               <input type="hidden" name="trip_id" id="trip_id">
            <div class="form-group {{ $errors->has('source_park') ? 'has-error' : ''}}">
                {!! Form::label('source_park', 'Source Park: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    <select name="source_park" id="source_park" class="form-control">
                        <option value="">Choose Park</option>
                        @foreach($boardable_parks as $park)
                        <option value="{{ $park->id }}">{{ $park->name }}</option>
                        @endforeach
                    </select>
                {!! $errors->first('source_park', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>


             <div class="form-group {{ $errors->has('dest_park') ? 'has-error' : ''}}">
                {!! Form::label('dest_park', 'Destination Park: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    <select name="dest_park" id="dest_park" class="form-control">
                        <option value="">Choose Park</option>
                        @foreach($all_parks as $park)
                        <option value="{{ $park->id }}">{{ $park->name }}</option>
                        @endforeach
                    </select>
                {!! $errors->first('dest_park', '<span class="parsley-error-list">:message</span>') !!}
               
                </div>
            </div>
            
            
             <div class="form-group {{ $errors->has('trip_stops') ? 'has-error' : ''}}" id="stopDiv">
                {!! Form::label('trip_stops_label', 'Trip Stops: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    <input type="text" name="trip_stops" id="trip_stops" class="form-control">
                {!! $errors->first('trip_stops', '<span class="parsley-error-list">:message</span>') !!}
                <center><label class="label label-default">Comma Separated: Stop 1, Stop 2,....Stop n</label></center>
                </div><br>
            </div>

            
    

                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-3">
                        {!! Form::submit('Create', ['class' => 'btn btn-primary form-control', 'id'=>'subBtn']) !!}
                    </div>
                </div>
                {!! Form::close() !!}

                @if ($errors->any())
                    <ul class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                @endif
             </div> <!-- /.portlet-content -->

          </div> <!-- /.portlet -->

        

        </div> <!-- /.col -->

        <div class="col-md-2 col-sm-4">

            <ul id="myTab" class="nav nav-pills nav-stacked">
            <li class="active">
              <a href="{{ url('trip-stops') }}">
                <i class="fa fa-bars"></i> 
                List Trip Stops
              </a>
            </li>
          </ul>

        </div>

      </div> <!-- /.row -->   
    

@endsection


