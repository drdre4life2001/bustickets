@extends('layouts.master')

@section('content')

   <div class="content-header">
        <h2 class="content-header-title">Add Incident </h2>
        <ol class="breadcrumb">
          <li><a href="{{ url('/') }}">Dashboard</a></li>
          <li><a href="{{ url('incidents') }}">Incidents </a></li>
          <li class="active">Add Incident </li>
        </ol>
      </div> <!-- /.content-header -->

      

      <div class="row">

        <div class="col-md-10 col-sm-8">

          <div class="portlet">

            <div class="portlet-content">

                {!! Form::open(['url' => 'incidents', 'class' => 'form-horizontal']) !!}

            <div class="form-group {{ $errors->has('incident_type_id') ? 'has-error' : ''}}">
                {!! Form::label('incident_type_id', trans('incidents.incident_type_id'), ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::select('incident_type_id', $incident_types,null, ['class' => 'form-control select22', 'id'=>"select2", 'required' => 'required']) !!}
                    {!! $errors->first('incident_type_id', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('details') ? 'has-error' : ''}}">
                {!! Form::label('details', trans('incidents.details'), ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::textarea('details', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('details', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('park_id') ? 'has-error' : ''}}">
                {!! Form::label('park_id', trans('incidents.park_id'), ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::select('park_id', $parks,null, ['class' => 'form-control select22', 'id'=>"select2", 'required' => 'required']) !!}
                    {!! $errors->first('park_id', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('bus_id') ? 'has-error' : ''}}">
                {!! Form::label('bus_id', trans('incidents.bus_id'), ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::select('bus_id', $buses,null, ['class' => 'form-control select22', 'id'=>"select2", 'required' => 'required']) !!}
                    {!! $errors->first('bus_id', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('expense_amount') ? 'has-error' : ''}}">
                {!! Form::label('expense_amount', trans('incidents.expense_amount'), ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::number('expense_amount', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('expense_amount', '<p class="help-block">:message</p>') !!}
                </div>
            </div>


                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-3">
                        {!! Form::submit('Create', ['class' => 'btn btn-primary form-control']) !!}
                    </div>
                </div>
                {!! Form::close() !!}

                @if ($errors->any())
                    <ul class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                @endif
             </div> <!-- /.portlet-content -->

          </div> <!-- /.portlet -->

        

        </div> <!-- /.col -->

        <div class="col-md-2 col-sm-4">

            <ul id="myTab" class="nav nav-pills nav-stacked">
            <li class="active">
              <a href="{{ url('incidents') }}">
                <i class="fa fa-bars"></i> 
                List Incidents
              </a>
            </li>
          </ul>

        </div>

      </div> <!-- /.row -->   

@endsection