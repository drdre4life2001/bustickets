@extends('layouts.master')

@section('content')

    <div class="content-header">
        <h2 class="content-header-title">Settings </h2>
        <ol class="breadcrumb">
          <li><a href="{{ url('/') }}">Dashboard</a></li>
          <li class="active">Settings </li>
        </ol>
      </div> <!-- /.content-header -->



      <div class="row">

        <div class="col-md-10 col-sm-8">

          <div class="portlet">

            <div class="portlet-content">

              <div class="table-responsive">

              <table
                class="table table-striped table-bordered table-hover table-highlight table-checkable"
                data-provide="datatable"
                data-display-rows="10"
                data-info="true"
                data-search="true"
                data-length-change="true"
                data-paginate="true"
              >
                  <thead>
                    <tr>
                      <!-- <th>S.No</th> --><th>Track Luggage</th><th>Luggage limit (kg)</th><th>Intl cost per_kg</th><th>Local cost per_kg</th><th>Expenses</th><th>Enable discount (%)</th><th>Actions</th>
                    </tr>
                  </thead>
                  <tbody>

                        <tr>
                            <td><a href="{{ url('settings', $settings->id) }}">{{ $settings->track_luggage }}</a></td>
                            <td>{{ $settings->luggage_limit }}</td>
                            <td>{{ $settings->intl_cost_per_kg }}</td>
                            <td>{{ $settings->local_cost_per_kg }}</td>
                            <td>{{ $settings->daily_trip_expense_headers }}</td>
                            <td>{{ $settings->discount_active }}</td>
                            <td>
                                <a href="{{ url('settings/' . $settings->id . '/edit') }}" title="Edit">
                                    <button type="submit" class="btn btn-default btn-xs"><i class="fa fa-edit"></i></button>
                                </a>
                            </td>
                        </tr>
                  </tbody>
                </table>
              </div> <!-- /.table-responsive -->


            </div> <!-- /.portlet-content -->

          </div> <!-- /.portlet -->



        </div> <!-- /.col -->

@if(!session('settings'))
        <div class="col-md-2 col-sm-4">

            <ul id="myTab" class="nav nav-pills nav-stacked">
            <li class="active">
              <a href="{{ url('settings/create') }}">
                <i class="fa fa-plus"></i>
                Add New Setting
              </a>
            </li>
          </ul>

        </div>
@endif

      </div> <!-- /.row -->



@endsection
