@extends('layouts.master')

@section('content')

    <h1>Setting</h1>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>track_luggage</th><th>luggage_limit</th><th>intl_cost_per_kg</th><th>local_cost_per_kg</th><th>expenses</th>
                    <th>track_infant</th><th>infant_discount</th><th>infant_age_range </th>
            </thead>
            <tbody>
                <tr>
                    <td>{{ $setting->track_luggage }}</td>
                    <td>{{ $setting->luggage_limit }}</td>
                    <td>{{ $setting->intl_cost_per_kg }}</td>
                    <td>{{ $setting->local_cost_per_kg }}</td>
                    <td>{{ $setting->daily_trip_expense_headers }}</td>
                    <td>{{ $setting->track_infant }}</td>
                    <td>{{ $setting->infant_discount }}</td>
                    <td>{{ $setting->infant_age_range }}</td>
                </tr>
            </tbody>
        </table>
    </div>

@endsection