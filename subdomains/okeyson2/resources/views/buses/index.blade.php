@extends('layouts.master')

@section('content')

    <div class="content-header">
        <h2 class="content-header-title">Buses </h2>
        <ol class="breadcrumb">
          <li><a href="{{ url('/') }}">Dashboard</a></li>
          <li class="active">Buses </li>
        </ol>
      </div> <!-- /.content-header -->



      <div class="row">

        <div class="col-md-10 col-sm-8">
          @if (Session::has('status'))
              <div class="alert alert-success" role="alert">
                {!! session('status') !!}
              </div>
          @endif
          <form style="border: 4px solid #a1a1a1;margin-top: 15px;padding: 10px;"
            action="/buses/upload"
            class="form-horizontal" 
            method="POST" 
            enctype="multipart/form-data">
              <input type="file" name="userfile" />
              <br/>
              <button class="btn btn-primary">Upload Buses</button>
            </form>

          <div class="portlet">

            <div class="portlet-content">

              <div class="table-responsive">

              <table
                class="table table-striped table-bordered table-hover table-highlight table-checkable"
               
                data-display-rows="20"
                data-info="true"
                data-search="true"
                data-length-change="true"
                data-paginate="true"
              >
                  <thead>
                    <tr>
                      <th>{{ trans('buses.bus_number') }}</th>
                      <th>{{ trans('buses.number_plate') }}</th>
                      <th>{{ trans('buses.bus_type_id') }}</th>
                      <!-- <th>Bus Roof</th> -->
                      <th>Default Driver</th>
                      <th>Default Park</th>
                      <th>Default Trip</th>
                      <th>Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    {{-- */$x=0;/* --}}
                    @foreach($buses as $item)
                        {{-- */$x++;/* --}}
                        <tr>
                            <td><a href="{{ url('buses', $item->id) }}">{{ $item->bus_number }}</a></td>
                            <td>{{ $item->number_plate }}</td>
                            <td>{{ $item->bus_type->name or ''}}</td>
                            <!-- <td>{{ $item->bus_roof }}</td> -->
                            <td>
                                {{ empty($item->default_driver)?'':$item->default_driver->name }}
                                <a type="button" class=" " data-toggle="modal" data-target="#driverModal{{$item->id}}"> Assign a driver</a>
                            </td>
                            <td>
                              {{ empty($item->default_park)?'':$item->default_park->name }}
                              <a type="button" class=" " data-toggle="modal" data-target="#parkModal{{$item->id}}"> Assign to a park</a>
                            </td>
                            <td>
                              {{ empty($item->default_trip)?'':str_ireplace("OKEYSON_", '', $item->default_trip->name) }}
                              <a type="button" class=" " data-toggle="modal" data-target="#tripModal{{$item->id}}"> Assign to a trip</a>
                            </td>
                            <td>
                                <a href="{{ url('buses/' . $item->id . '/edit') }}" title="Edit">
                                    <button type="submit" class="btn btn-default btn-xs"><i class="fa fa-edit"></i></button>
                                </a>
                                <!--
                                {!! Form::open([
                                    'method'=>'DELETE',
                                    'url' => ['buses', $item->id],
                                    'style' => 'display:inline'
                                ]) !!}
                                    {!! Form::button('<i class="fa fa-times"></i>', ['class' => 'btn btn-danger btn-xs', 'escape'=>false,'title'=>'Delete', 'type'=>'submit']) !!}
                                {!! Form::close() !!}

                                -->
                            </td>
                        </tr>


<div class = "modal fade" id="driverModal{{$item->id}}" tabindex = "-1" role = "dialog" aria-labelledby = "myModalLabel" aria-hidden = "true">
   <div class = "modal-dialog">
      <div class = "modal-content">
         <div class = "modal-header">
            <button type = "button" class = "close" data-dismiss = "modal" aria-hidden = "true">
                  &times;
            </button>
            <h4 class = "modal-title" id = "myModalLabel">
               Assign a default Driver
            </h4>
         </div>
         {!! Form::model($item, [
                    'method' => 'PATCH',
                    'url' => ['buses', $item->id],
                    'class' => 'form-horizontal'
                ]) !!}
         <div class = "modal-body" id="contentArea">
              <input type="hidden" id="SubBtn" name="_token" value="<?php echo csrf_token(); ?>">
              <div class="form-group {{ $errors->has('default_driver_id') ? 'has-error' : ''}}">
                {!! Form::label('default_driver_id', 'Default Driver', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::select('default_driver_id', $drivers, null, ['class' => 'form-control', 'required' => 'required']) !!}
                    {!! $errors->first('default_driver_id', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
         </div>
         <div class = "modal-footer">
            <button type = "button" class = "btn btn-default" data-dismiss = "modal">
               Cancel
            </button>
            <button type = "submit" id="NoteSub" class = "btn btn-primary">
               Assign Driver
            </button>
         </form>

         </div>
      </div><!-- /.modal-content -->
   </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<div class = "modal fade" id="parkModal{{$item->id}}" tabindex = "-1" role = "dialog" aria-labelledby = "myModalLabel" aria-hidden = "true">
   <div class = "modal-dialog">
      <div class = "modal-content">
         <div class = "modal-header">
            <button type = "button" class = "close" data-dismiss = "modal" aria-hidden = "true">
                  &times;
            </button>
            <h4 class = "modal-title" id = "myModalLabel">
               Assign to a default Park
            </h4>
         </div>
         {!! Form::model($item, [
                    'method' => 'PATCH',
                    'url' => ['buses', $item->id],
                    'class' => 'form-horizontal'
                ]) !!}
         <div class = "modal-body" id="contentArea">
              <input type="hidden" id="SubBtn" name="_token" value="<?php echo csrf_token(); ?>">
              <div class="form-group {{ $errors->has('default_park_id') ? 'has-error' : ''}}">
                {!! Form::label('default_park_id', trans('buses.default_park_id'), ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::select('default_park_id', $parks,null, ['class' => 'form-control', 'required' => 'required']) !!}
                    {!! $errors->first('default_park_id', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
         </div>
         <div class = "modal-footer">
            <button type = "button" class = "btn btn-default" data-dismiss = "modal">
               Cancel
            </button>
            <button type = "submit" id="NoteSub" class = "btn btn-primary">
               Assign Driver
            </button>
         </form>

         </div>
      </div><!-- /.modal-content -->
   </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class = "modal fade" id="tripModal{{$item->id}}" tabindex = "-1" role = "dialog" aria-labelledby = "myModalLabel" aria-hidden = "true">
   <div class = "modal-dialog">
      <div class = "modal-content">
         <div class = "modal-header">
            <button type = "button" class = "close" data-dismiss = "modal" aria-hidden = "true">
                  &times;
            </button>
            <h4 class = "modal-title" id = "myModalLabel">
               Assign to a default Trip
            </h4>
         </div>
         {!! Form::model($item, [
                    'method' => 'PATCH',
                    'url' => ['buses', $item->id],
                    'class' => 'form-horizontal'
                ]) !!}
         <div class = "modal-body" id="contentArea">
              <input type="hidden" id="SubBtn" name="_token" value="<?php echo csrf_token(); ?>">
              <div class="form-group {{ $errors->has('default_trip_id') ? 'has-error' : ''}}">
                {!! Form::label('default_trip_id', 'Default trip', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-9">
                    {!! Form::select('default_trip_id', $trips, null, ['class' => 'form-control', 'required' => 'required']) !!}
                    {!! $errors->first('default_trip_id', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
         </div>
         <div class = "modal-footer">
            <button type = "button" class = "btn btn-default" data-dismiss = "modal">
               Cancel
            </button>
            <button type = "submit" id="NoteSub" class = "btn btn-primary">
               Assign to a Trip
            </button>
         </form>

         </div>
      </div><!-- /.modal-content -->
   </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


                    @endforeach
                  </tbody>
                </table>
              </div> <!-- /.table-responsive -->


            </div> <!-- /.portlet-content -->

          </div> <!-- /.portlet -->



        </div> <!-- /.col -->

        <div class="col-md-2 col-sm-4">

            <ul id="myTab" class="nav nav-pills nav-stacked">
            <li class="active">
              <a href="{{ url('buses/create') }}">
                <i class="fa fa-plus"></i>
                Add New Bus
              </a>
            </li>
            <li class="active">
              <a href="{{ URL::to('/downloads/buses_template.xlsx')  }}" >
              <i class="fa fa-download"></i>
                Download Excel
              </a>
            </li>
          </ul>

        </div>

      </div> <!-- /.row -->



@endsection
