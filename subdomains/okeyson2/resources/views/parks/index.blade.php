@extends('layouts.master')

@section('content')

    <div class="content-header">
        <h2 class="content-header-title">{{ ($is_destination)?'Destinations':'Parks' }} </h2>
        <ol class="breadcrumb">
          <li><a href="{{ url('/') }}">Dashboard</a></li>
          <li class="active">{{ ($is_destination)?'Destinations':'Parks' }} </li>
        </ol>
      </div> <!-- /.content-header -->

      

      <div class="row">

         @if($is_destination)
        <div class="col-md-2 col-sm-4">

            <ul id="myTab" class="nav nav-pills nav-stacked">
            <li class="active">
              <a href="{{ url('parks/create') }}">
                <i class="fa fa-plus"></i> 
                Add New Destination
              </a>
            </li>
          </ul>

        </div>
        @endif

        <div class="col-md-12 col-sm-8">

          <div class="portlet">

            <div class="portlet-content">           

              <div class="table-responsive">

              <table 
                class="table table-striped table-bordered table-hover table-highlight table-checkable" 
                data-provide="datatable" 
                data-display-rows="10"
                data-info="true"
                data-search="true"
                data-length-change="true"
                data-paginate="true"
              >
                  <thead>
                    <tr>
                      <th>Name</th>
                      <th>State</th>
                      <th>{{ ($is_destination)?'Stops':'Address' }}</th>
                      @if($is_destination)
                      <th>Actions</th>
                      @endif
                    </tr>
                  </thead>
                  <tbody>
                    {{-- */$x=0;/* --}}
                    @foreach($parks as $item)
                        {{-- */$x++;/* --}}
                        <tr>
                            <td>{{ $item->name }}</td>
                            <td>{{ $item->state->name}}</td>
                            @if(!$is_destination)
                              <td>{{ $item->address }}</td>
                            @else
                              <td>
                                @foreach($item->stops as $stop)
                                  {{ $stop->name }} | 
                                @endforeach
                              </td>
                             @endif 


                            @if($is_destination)
                            <td>
                                <a href="{{ url('parks', $item->id) }}">
                                  <button type="button" class="btn btn-primary btn-xs"><i class="fa fa-eye"></i></button>
                                </a>
                                <a href="{{ url('parks/' . $item->id . '/edit') }}" title="Edit">
                                    <button type="submit" class="btn btn-default btn-xs"><i class="fa fa-edit"></i></button>
                                </a> 
                                {!! Form::open([
                                    'method'=>'DELETE',
                                    'url' => ['parks', $item->id],
                                    'style' => 'display:inline'
                                ]) !!}
                                    {!! Form::button('<i class="fa fa-times"></i>', ['class' => 'btn btn-danger btn-xs', 'escape'=>false,'title'=>'Delete', 'type'=>'submit']) !!}
                                {!! Form::close() !!}
                            </td>
                            @endif

                        </tr>
                    @endforeach
                  </tbody>
                </table>
              </div> <!-- /.table-responsive -->
              

            </div> <!-- /.portlet-content -->

          </div> <!-- /.portlet -->

        

        </div> <!-- /.col -->



      </div> <!-- /.row -->    



@endsection
