@extends('layouts.master')

@section('content')

    <h1>Trip Schedule</h1>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>ID.</th> <th>{{ trans('daily-trips.trip_date') }}</th><th>{{ trans('daily-trips.trip_id') }}</th><th>{{ trans('daily-trips.driver_id') }}</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{ $daily-trip->id }}</td> <td> {{ $daily-trip->trip_date }} </td><td> {{ $daily-trip->trip_id }} </td><td> {{ $daily-trip->driver_id }} </td>
                </tr>
            </tbody>    
        </table>
    </div>

@endsection