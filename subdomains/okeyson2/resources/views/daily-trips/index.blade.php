@extends('layouts.master')

@section('content')

<script>
$(document).ready(function(){


});
</script>

    <div class="content-header">
        <h2 class="content-header-title">Daily Trips</h2>
        <ol class="breadcrumb">
          <li><a href="{{ url('/') }}">Dashboard</a></li>
          <li class="active">Daily Trips </li>
           <li><a  href="{{ url('trips/search') }}">Book another Ticket</a></li>
           <li><a  href="{{ url('quick-book') }}">Quick Book</a></li>
           <li><a href="{{ url('bookings/status/PAID') }}">Paid Bookings</a> </li>
        </ol>
      </div> <!-- /.content-header -->

      

      <div class="row">

        <div class="col-md-12 col-sm-12">


        <div class="btn-group pull-right">
                  <div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
                      <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
                      <span></span> <b class="caret"></b>
                  </div>
              </div>
              <h4 class="heading-inline pull-right">
          &nbsp;&nbsp;<small>Choose date</small>
          &nbsp;&nbsp;</h4>

              <br/><br/><br/>


              <div class="well">
                <h4><span class="text-primary">Summary</span></h4>
                
                  <p>
                    <span class="text-primary">Total Trips: </span> <strong id="totalTrips"> </strong>
                    <span class="text-primary">Total Revenue: </span><strong id="totalRev"> </strong>| 
                    <span class="text-primary">Total Expense: </span><strong  id="totalExp"> </strong> | 
                    <span class="text-primary">Net Revenue: </span><strong id="netRev"> </strong> | 
                      
                       {{-- */$x=0; $extlug =0;$expnz=0; /* --}}
                      @foreach($dailytrips as $dailytrip)
                      
                          @foreach($dailytrip->bookings as $b)
                      {{-- */
                                        $extlug += $b->extra_luggages->sum('amount');
                       /* --}}
                                    @endforeach
                      
                      @foreach($dailytrip->expenses as $exps)
                      {{-- */
                                        $expnz += $exps->amount;
                       /* --}}
                                    @endforeach
                                   
                                @endforeach
                      
                      <span class="text-primary">Extra Luggages Revenue: </span><strong >{{$extlug}} </strong>
                    
                  </p>

              </div>

              <!-- <a href="{{ url('populate-empty-trips/'.$start_date) }}" class="btn btn-primary pull-right" style="margin-left:10px;" onclick="return confirm('This will populate all trips with default buses or no drivers for the selected date.')" >Populate empty trips</a> &nbsp;&nbsp;
              
              <a href="{{ url('translate-prev-trip/'.$start_date) }}" class="btn btn-default pull-right" onclick="return confirm('This will move yesterday\'s trips which are not in the this list, so that it can be assigned to buses and drivers.')" >Migrate previous day trips</a>
              <br/><br/> -->



          <div class="portlet">

            <div class="portlet-content">           


           
              <div class="table-responsive">

              <table 
                class="table table-striped table-bordered table-hover table-highlight table-checkable" 
                data-provide="datatable" 
                data-display-rows="10"
                data-info="true"
                data-search="true"
                data-length-change="true"
                data-paginate="true"
              >
                  <thead>
                    <tr>
                      
                      <th>Trip Details</th>
                      <th>Bookings Details</th>
                      <th>Driver Dispatch</th>
                      <th>Financial Details</th>
                      <th>Status</th>
                      <th>Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    {{-- */$x=0; $tf =0; $de=0; $fuel=0; $oe = 0; /* --}}
                    @foreach($dailytrips as $item)
                        {{-- */

                           $tf += $item->total_fare;     
                          $de += $item->driver_allowance;     
                          $fuel += $item->fuel_expense;     
                          $oe += $item->other_expense; 

                          $x +=  intVal($item->booked_seats);



                          /* --}}
                        <tr>
                            <td>
                              <strong>Trip Date:</strong> {{ date('D, M d, Y',strtotime($item->trip_date)) }}<br/>
                              <strong>From:</strong> {{ $item->trip['sourcepark']['name'] }}<br/>
                                <strong></strong>
                              <strong>To:</strong>  @for($i=0; $i < count($item->trip['subTrips']); $i++) 
                          {{ $item->trip['subTrips'][$i]['destpark']['name'] }}<br/> 
                          @endfor
                              <!-- <strong>Time:</strong> {{ date('h:iA', strtotime($item->trip['departure_time'])) }}<br/> -->
                              <strong>Bus Type:</strong> {{ (!empty($item->bus))?$item->bus['bus_type']['name']:'' }}<br/>
                              <strong>Fare:</strong> &#8358;{{ number_format($item->trip['fare'], 2) }}<br/>
                            </td>
                            <td>
                              <strong>Booked seats:</strong> {{ $item->booked_seats }}<br/>
                              <strong>Total seats:</strong> {{ $item->total_seats }}<br/>
                              <strong>Total Fare:</strong><span style="color:green;"> &#8358;{{ number_format($item->total_fare, 2) }}</span><br/>
                            </td>
                            <td>
                              <strong>Driver:</strong> <label class='drvName'>{{ (!empty($item->driver))?$item->driver->name:'' }}</label><br/>
                              <strong>Bus Number:</strong> {{ (!empty($item->bus))?$item->bus->number_plate:"" }}<br/>
                              <strong>Transit:</strong> {{ $item->trip_transit }}<br/>
                               
                            </td>
                            <td>
                               @foreach($item->expenses as $e)
                                <?php if(empty($e->amount)) continue; ?>
                                <strong>{{ $e->expense_name }}:</strong><span style="color:red;"> &#8358;{{ number_format($e->amount, 2) }}</span><br/>

                              @endforeach
                                
                                {{-- */$x=0; $extExp=0; /* --}}
                               @foreach($item->bookings as $bk)
                                @foreach($bk->extra_luggages as $lug)
                                
                                {{-- */
                                        $extExp += $lug->amount;
                                   /* --}}
                                
                                 @endforeach
                    
                                @endforeach
                                                                


                              <strong>Net Revenue:</strong><span style="color:green;"> &#8358;{{ number_format((($item->total_fare +$extExp) - $item->expenses()->sum('amount')), 2) }}</span><br/>
                              {{$item->bookings->extra_luggages->amount or ''}}
                            </td>
                            
                            <td>{{ $item->status }}</td>
                            <td>
                              <a type="button" class="assignDriver" href="#" data-toggle="modal" data-target="#driverModal{{$item->id}}"> Assign a driver</a> <br/>
                              <a type="button" href="#" class=" " data-toggle="modal" data-target="#busModal{{$item->id}}"> Change Vehicle</a> <br/>
                              <a type="button" href="#" class=" " data-toggle="modal" data-target="#expenseModal{{$item->id}}"> Update expenses</a>
                              <br/>
                              <a type="button" href="{{ route('manifest', $item->id) }}" target="_blank" > Generate Manifest</a> <br/>
                              <a type="button" href="#" class=" " data-toggle="modal" data-target="#statusModal{{$item->id}}"> Update status</a> 
                              <br/>
                             
                              <a type="button" href="#" class=" " data-toggle="modal" data-target="#tickReportModal{{$item->id}}" data-id="'.$row['ID'].'">Ticketer's Report</a> 
                              <br/>

                              
                               <!-- <a type="button" href="#" class=" " data-toggle="modal" data-target="#mileageModal{{$item->id}}">Update Bus Mileage</a>  -->
                            <!-- 
                                <a href="{{ url('daily-trips/' . $item->id . '/edit') }}" title="Edit">
                                    <button type="submit" class="btn btn-default btn-xs"><i class="fa fa-edit"></i></button>
                                </a> 
                                {!! Form::open([
                                    'method'=>'DELETE',
                                    'url' => ['daily-trips', $item->id],
                                    'style' => 'display:inline'
                                ]) !!}
                                    {!! Form::button('<i class="fa fa-times"></i>', ['class' => 'btn btn-danger btn-xs', 'escape'=>false,'title'=>'Delete', 'type'=>'submit']) !!}
                                {!! Form::close() !!} -->
                            </td>
                        </tr>

<div class = "modal fade" id="driverModal{{$item->id}}" tabindex = "-1" role = "dialog" aria-labelledby = "myModalLabel" aria-hidden = "true">
   <div class = "modal-dialog">
      <div class = "modal-content">
         <div class = "modal-header">
            <button type = "button" class = "close" data-dismiss = "modal" aria-hidden = "true">
                  &times;
            </button>
            <h4 class = "modal-title" id = "myModalLabel">
               Assign a Driver to this Trip
            </h4>
         </div>
         {!! Form::model($item, [
                    'method' => 'PATCH',
                    'url' => ['daily-trips', $item->id],
                    'class' => 'form-horizontal'
                ]) !!}
         <div class = "modal-body" id="contentArea">
              <input type="hidden" id="SubBtn" name="_token" value="<?php echo csrf_token(); ?>">
              <input type="hidden" id="fmrDriver" name="fmrDriver" value="<?php echo $item->id ?>">
              
              <div class="form-group {{ $errors->has('default_driver_id') ? 'has-error' : ''}}">
                {!! Form::label('driver_id', 'Driver', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::select('driver_id', $drivers, null, ['class' => 'form-control', 'required' => 'required']) !!}
                    {!! $errors->first('driver_id', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
         </div>
         <div class = "modal-footer">
            <button type = "button" class = "btn btn-default" data-dismiss = "modal">
               Cancel
            </button>
            <button type = "submit" name="assignDriver" class = "btn btn-primary">
               Assign Driver
            </button>
         </form>

         </div>
      </div><!-- /.modal-content -->
   </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!--  expenses modal -->
<div class = "modal fade" id="expenseModal{{$item->id}}" tabindex = "-1" role = "dialog" aria-labelledby = "myModalLabel" aria-hidden = "true">
   <div class = "modal-dialog">
      <div class = "modal-content">
         <div class = "modal-header">
            <button type = "button" class = "close" data-dismiss = "modal" aria-hidden = "true">
                  &times;
            </button>
            <h4 class = "modal-title" id = "myModalLabel">
               Expenses for this Trip

               <a href="{{url('settings')}}" class="pull-right">Edit Expenses</a>
            </h4>
         </div>
         {!! Form::model($item, [
                    'method' => 'PATCH',
                    'url' => ['daily-trips', $item->id],
                    'class' => 'form-horizontal'
                ]) !!}
         <div class = "modal-body" id="contentArea">
              <input type="hidden" id="SubBtn" name="_token" value="<?php echo csrf_token(); ?>">
              <input type="hidden"  name="update_expenses" value="1">


              @foreach($dt_expense_headers as $h)

                <?php 
                  $val = 0;
                  foreach($item->expenses as $e)
                    if($e->expense_name == $h) 
                      $val = $e->amount;

                ?>


                <div class="form-group row {{ $errors->has('driver_allowance') ? 'has-error' : ''}}">
                    {!! Form::label(str_slug($h, '_'), $h, ['class' => 'col-sm-3 control-label']) !!}
                    <div class="col-sm-8">
                        {!! Form::number(str_slug($h, '_'), $val, ['class' => 'form-control']) !!}
                        {!! $errors->first(str_slug($h, '_'), '<p class="help-block">:message</p>') !!}
                    </div>
                </div>

              @endforeach


         </div>
         <div class = "modal-footer">
            <button type = "button" class = "btn btn-default" data-dismiss = "modal">
               Cancel
            </button>
            <button type = "submit" id="NoteSub" class = "btn btn-primary">
               Update Expenses
            </button>
         </form>

         </div>
      </div><!-- /.modal-content -->
   </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class = "modal fade" id="busModal{{$item->id}}" tabindex = "-1" role = "dialog" aria-labelledby = "myModalLabel" aria-hidden = "true">
   <div class = "modal-dialog">
      <div class = "modal-content">
         <div class = "modal-header">
            <button type = "button" class = "close" data-dismiss = "modal" aria-hidden = "true">
                  &times;
            </button>
            <h4 class = "modal-title" id = "myModalLabel">
               Assign a Bus to this Trip
            </h4>
         </div>
         {!! Form::model($item, [
                    'method' => 'PATCH',
                    'url' => ['daily-trips', $item->id],
                    'class' => 'form-horizontal'
                ]) !!}
         <div class = "modal-body" id="contentArea">
              <input type="hidden" id="SubBtn" name="_token" value="<?php echo csrf_token(); ?>">
              <input type="hidden" id="dtObj" name="dtObj" value="{{ $dailytrips[$item->id] }}">
               <input type="hidden" id="fmrBus" name="fmrBus" value="<?php echo $item->id ?>">
             
              
              <div class="form-group {{ $errors->has('bus_id') ? 'has-error' : ''}}">
                {!! Form::label('bus_id', 'Bus', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-8">
                    {!! Form::select('bus_id', $buses, null, ['class' => 'form-control','required' => 'required']) !!}
                    {!! $errors->first('bus_id', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <!-- <br style="clear:both;" />

            <div class="form-group row {{ $errors->has('driver_allowance') ? 'has-error' : ''}}">
                    {!! Form::label('mileage', 'Bus Mileage', ['class' => 'col-sm-3 control-label']) !!}
                    <div class="col-sm-8">
                        {!! Form::number('mileage', null, ['class' => 'form-control']) !!}
                        <span class="help-block"><em>Leave blank to use the existing mileage of the selected bus</em></span>
                        {!! $errors->first('mileage', '<p class="help-block">:message</p>') !!}
                    </div>
                </div> -->
            
            
         </div>
         <div class = "modal-footer">
            <button type = "button" class = "btn btn-default" data-dismiss = "modal">
               Cancel
            </button>
            <button type = "submit" id="NoteSub" class = "btn btn-primary">
               Assign Bus
            </button>
         </form>

         </div>
      </div><!-- /.modal-content -->
   </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<div class = "modal fade" id="statusModal{{$item->id}}" tabindex = "-1" role = "dialog" aria-labelledby = "myModalLabel" aria-hidden = "true">
   <div class = "modal-dialog">
      <div class = "modal-content">
         <div class = "modal-header">
            <button type = "button" class = "close" data-dismiss = "modal" aria-hidden = "true">
                  &times;
            </button>
            <h4 class = "modal-title" id = "myModalLabel">
               Update Trip Status
            </h4>
         </div>
         {!! Form::model($item, [
                    'method' => 'PATCH',
                    'url' => ['daily-trips', $item->id],
                    'class' => 'form-horizontal'
                ]) !!}
         <div class = "modal-body" id="contentArea">
              <input type="hidden" id="SubBtn" name="_token" value="<?php echo csrf_token(); ?>">
              <div class="form-group {{ $errors->has('driver_id') ? 'has-error' : ''}}">
                {!! Form::label('status', 'Trip Status', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-8">
                    {!! Form::select('status', $statuses, null, ['class' => 'form-control']) !!}
                    {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            
         </div>
         <div class = "modal-footer">
            <button type = "button" class = "btn btn-default" data-dismiss = "modal">
               Cancel
            </button>
            <button type = "submit" id="NoteSub" class = "btn btn-primary">
               Update Status
            </button>
         </form>

         </div>
      </div><!-- /.modal-content -->
   </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- mileage modal comes here-->
<div class = "modal fade" id="mileageModal{{$item->id}}" tabindex = "-1" role = "dialog" aria-labelledby = "myModalLabel" aria-hidden = "true">
   <div class = "modal-dialog">
      <div class = "modal-content">
         <div class = "modal-header">
            <button type = "button" class = "close" data-dismiss = "modal" aria-hidden = "true">
                  &times;
            </button>
            <h4 class = "modal-title" id = "myModalLabel">
               Update Bus Mileage
            </h4>
         </div>
         {!! Form::open(array( 'method' => 'POST', 'route' => 'update-bus-mileage', 'class' => 'form-horizontal' )) 
         !!}
         <div class = "modal-body" id="contentArea">
              <input type="hidden" id="SubBtn" name="_token" value="<?php echo csrf_token(); ?>">
              <input type="hidden" id="busid" name="busid" value="{{ $dailytrips[$item->id]['bus']['id'] }}">
              
              <div class="form-group {{ $errors->has('bus_mileage') ? 'has-error' : ''}}">
                {!! Form::label('bus_mileage', 'Bus Mileage', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-8">
                    {!! Form::text('bus_mileage', $dailytrips[$item->id]['bus']['bus_mileage'], null, array('class' => 'form-control', 'required' => 'required')) !!}
                    {!! $errors->first('bus_mileage', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            
         </div>
         <div class = "modal-footer">
            <button type = "button" class = "btn btn-default" data-dismiss = "modal">
               Cancel
            </button>
            <button type = "submit" id="NoteSub" class = "btn btn-primary">
               Update Bus Mileage
            </button>
         </form>

         </div>
      </div><!-- /.modal-content -->
   </div><!-- /.modal-dialog -->
</div><!-- /.modal-->

<!-- mileage modal comes here-->
<div class = "modal fade" id="tickReportModal{{$item->id}}" tabindex = "-1" role = "dialog" aria-labelledby = "myModalLabel" aria-hidden = "true">
   <div class = "modal-dialog">
      <div class = "modal-content">
         <div class = "modal-header">
            <button type = "button" class = "close" data-dismiss = "modal" aria-hidden = "true">
                  &times;
            </button>
            <h4 class = "modal-title" id = "myModalLabel">
               Ticketer's Report
            </h4>
         </div>
         
         <div class = "modal-body" id="contentArea">
             <b>From:</b> {{ $item->trip->sourcepark->name }} <br>
              <b>Date:</b>{{date('d-m-y', strtotime($item->trip_date))}}<br>
            
              <b>Final Destination:</b> {{ $item->trip->destpark->name }} <br>
              <b>Vehicle No: </b>
                 @if(isset($item->bus))
                <strong>{{ $item->bus->number_plate }}</strong> {{ $item->bus->bus_type->no_of_seats }} seater ({{$item->bus->bus_roof }}) 
              @endif
              <br>
            
             
             <!-- show the breakkdown -->
            <b><u>DESTINATION BREAKDOWN</u></b><br>
            
            <?php
                $destAmount = array();
                $destUnit = array();
                $destCount = array();

                // dump($item->toArray());
            ?>
            
             @foreach($item->bookings as $b)
                
              <?php 
              if(isset($destAmount[$b->trip->destpark->name]))
                 $destAmount[$b->trip->destpark->name] += $b->final_cost; 
               else
                $destAmount[$b->trip->destpark->name] = $b->final_cost;

              if(isset($destUnit[$b->trip->destpark->name]))
                 $destUnit[$b->trip->destpark->name] = $b->unit_cost; 
               else
                $destUnit[$b->trip->destpark->name] = $b->unit_cost; 

               if(isset($destCount[$b->trip->destpark->name]))
                 $destCount[$b->trip->destpark->name] += $b->passenger_count; 
               else
                $destCount[$b->trip->destpark->name] = $b->passenger_count; 

              ?>
             
             

              
             @if($b['is_flexi']) 
                {{$b->trip->destpark->name}}&nbsp;&nbsp;&nbsp;&nbsp; - &#8358;{{ number_format($b->final_cost) }} &nbsp;&nbsp;&nbsp;&nbsp;  ({{ $b->passenger_count }} passengers at &#8358;{{ $b->final_cost }} <strong>For Flexi Booking</strong>)<br>
             
              @endif


            
             
             @endforeach
             
             

                
@foreach($destAmount as $k=>$v)
             {{$k}} &nbsp;&nbsp;&nbsp;&nbsp; - &#8358;{{ number_format($v) }} &nbsp;&nbsp;&nbsp;&nbsp;  ({{ $destCount[$k] }} passengers at &#8358;{{ number_format($destUnit[$k]) }} each )<br>
             @endforeach

      
        
             <b><u>SEATS</u> </b><br> 
             Booked: {{$item->booked_seats}}<br>
             Remaining:{{intval($item->total_seats) - intval($item->booked_seats)}}<br>
             <b><u>ACCOUNTS</u> </b><br> 
             <b>Total Fare: </b><span style="color:green">&#8358;{{number_format($item->total_fare)}} </span><br>

             @foreach($item->expenses as $e)
                <?php if(empty($e->amount)) continue; ?>
                <b>{{ $e->expense_name }}:</b> <span style="color:red">&#8358;{{number_format($e->amount,2)}}</span><br>
              @endforeach

              <?php $sum=0; ?>
              @foreach($item->expenses as $e)
                <?php $sum+= $e->amount ?>
              @endforeach
              


            <strong>Net Revenue:</strong><span style="color:green;"> &#8358;{{ number_format(($item->total_fare+$extlug) - ($sum)) }}</span><br/>

            
             @if(!empty($item->ticketer))
               <br/><br/><b><u>TICKETER</u> </b><br> 
                {{ $item->ticketer->first_name.' '.$item->ticketer->last_name }}
             @endif

         </div>
         <div class = "modal-footer">
            <button type = "button" class = "btn btn-default" data-dismiss = "modal">
               Close
            </button>

            <a type = "button" class = "btn btn-primary" href="{{ url('tick-report/'.$item->id) }}" target="_blank" >
               Print
            </a>
            
         </form>

         </div>
      </div><!-- /.modal-content -->
   </div><!-- /.modal-dialog -->
</div><!-- /.modal-->
                    @endforeach
                  </tbody>
                </table>


                

              </div> <!-- /.table-responsive -->

              


              

            </div> <!-- /.portlet-content -->

          </div> <!-- /.portlet -->


          

        

        </div> <!-- /.col -->

      

      </div> <!-- /.row -->   


 <!-- setting totals on top -->
                <script type="text/javascript">

                  $(function() {
                    $("#totalRev").html("&#8358;{{ number_format($tf, 2) }}");
                    $("#totalExp").html("&#8358;{{ number_format(($de + $fuel + $oe), 2) }}");
                    $("#netRev").html("&#8358;{{ number_format((($tf + $extlug) - $expnz), 2) }}");
                    $("#totalTrips").html("{{ number_format(count($dailytrips)) }}");
                  });
                </script>

<script type="text/javascript">

function updateUrl(name, value){
            window.location.search = jQuery.query.set(name, value);

            return false;
       } 

function refreshReport(){

  console.log($('#select-input').val());
  window.location = '{{ url("daily-trips") }}/'+$('#select-input').val();


}


$(function() {

    function cb(start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    }
    cb(moment("{{ $start_date }}"), moment("{{ $end_date }}"));

    $('#reportrange').daterangepicker({
        ranges: {
           'Next Tomorrow': [moment().add(2, 'days'), moment().add(2, 'days')],
           'Tomorrow': [moment().add(1, 'days'), moment().add(1, 'days')],
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cb);

    $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
        console.log(picker.startDate.format('YYYY-MM-DD'));
        console.log(picker.endDate.format('YYYY-MM-DD'));
        
        updateUrl('daterange', picker.startDate.format('YYYY-MM-DD')+' '+picker.endDate.format('YYYY-MM-DD'));
    });

});
</script>





@endsection
