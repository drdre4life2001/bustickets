@extends('layouts.master')

@section('content')
<script>

function printDiv(divName) {
     var printContents = document.getElementById(divName).innerHTML;
     var originalContents = document.body.innerHTML;

     document.body.innerHTML = printContents;

     window.print();

     document.body.innerHTML = originalContents;
}

</script>
    <div class="content-header">
        <!--h2 class="content-header-title">Trip Manifest </h2-->
        <ol class="breadcrumb">
          <li><a href="{{ url('/') }}">Dashboard</a></li>
          <li><a href="{{ url('/daily-trips') }}">Trip Schedules</a></li>
          <li class="active">Ticketer's Report </li>
        </ol>
      </div> <!-- /.content-header -->

      

      <div class="row">


        <div class="col-md-10">

        <div class="portlet">

            <div class="portlet-content" id ="print-area-1">
            <img src ="{{ asset('logos/'.$opImg) }}" height="50px;" width="115px;" align="right"><br>
             <center style="line-height: 0.72em;">
                   <h5 style="font-size: 25px; margin-left: 100px;">TICKETER'S REPORT</h5>
               </center>
            <br>
            <div align="justify">
           
             
              <b>From:</b> {{ $dailytrip->trip->sourcepark->name }} <br>
              <b>Date:</b>{{date('d-m-y', strtotime($dailytrip->trip_date))}}<br>
            
              <b>Final Destination:</b> {{ $dailytrip->trip->destpark->name }} <br>
              <b>Vehicle No: </b>
                 @if(isset($dailytrip->bus))
                <strong>{{ $dailytrip->bus->bus_number }}</strong> {{ $dailytrip->bus->bus_type->no_of_seats }} seater ({{$dailytrip->bus->bus_roof }}) 
              @endif
              <br>
            
             
             <!-- show the breakkdown -->
            <b><u>DESTINATION BREAKDOWN</u></b><br>
            
            <?php
                $destAmount = array();
                $destUnit = array();
                $destCount = array();

                // dump($item->toArray());
            ?>
            
             @foreach($dailytrip->bookings as $b)
                
              <?php 
              if(isset($destAmount[$b->trip->destpark->name]))
                 $destAmount[$b->trip->destpark->name] += $b->final_cost; 
               else
                $destAmount[$b->trip->destpark->name] = $b->final_cost;

              if(isset($destUnit[$b->trip->destpark->name]))
                 $destUnit[$b->trip->destpark->name] = $b->unit_cost; 
               else
                $destUnit[$b->trip->destpark->name] = $b->unit_cost; 

               if(isset($destCount[$b->trip->destpark->name]))
                 $destCount[$b->trip->destpark->name] += $b->passenger_count; 
               else
                $destCount[$b->trip->destpark->name] = $b->passenger_count; 

                // dump($destUnit);

              ?> 
             @endforeach
             
             @foreach($destAmount as $k=>$v)
                @if($b['is_flexi']) 
                {{$k}} &nbsp;&nbsp;&nbsp;&nbsp; - &#8358;{{ number_format($v) }} &nbsp;&nbsp;&nbsp;&nbsp;  ({{ $destCount[$k] }} passengers at &#8358;{{ number_format($v) }} <strong>For Flexi Booking<strong>)<br>
              @else
                {{$k}} &nbsp;&nbsp;&nbsp;&nbsp; - &#8358;{{ number_format($v) }} &nbsp;&nbsp;&nbsp;&nbsp;  ({{ $destCount[$k] }} passengers at &#8358;{{ number_format($destUnit[$k]) }} each )<br>
                @endif
                

                
             @endforeach

      
        
             <b><u>SEATS</u> </b><br> 
             Booked: {{$dailytrip->booked_seats}}<br>
             Remaining:{{intval($dailytrip->total_seats) - intval($dailytrip->booked_seats)}}<br>
             <b><u>ACCOUNTS</u> </b><br> 
             <b>Total Fare: </b><span style="color:green">&#8358;{{number_format($dailytrip->total_fare)}} </span><br>
              @foreach($dailytrip->expenses as $e)
                <?php if(empty($e->amount)) continue; ?>
                <b>{{ $e->expense_name }}:</b> <span style="color:red">&#8358;{{number_format($e->amount,2)}}</span><br>
              @endforeach  

            <strong>Net Revenue:</strong><span style="color:green;"> &#8358;{{ number_format($dailytrip->total_fare - ($dailytrip->driver_allowance + $dailytrip->other_expense)) }}</span><br/>


             @if(!empty($dailytrip->ticketer))
               <br/><br/><b><u>TICKETER</u> </b><br> 
                {{ $dailytrip->ticketer->first_name.' '.$dailytrip->ticketer->last_name }}
             @endif

            
             
             
            
             </div>

          
          

          

          </div>
        </div>

        </div> <!-- /.col -->


        <div class="col-md-2 col-sidebar-right">

          <p><a onclick="printDiv('print-area-1')" id="" class="btn btn-primary"> <i class="fa fa-print"></i> Print Report <br /></a></p>

          <!-- <p><a href="javascript:;" class="btn btn-lg btn-tertiary btn-block">Close Invoice</a></p> -->

          <br><br>

            


          <br />


          
        </div> <!-- /.col -->


      </div> <!-- /.row -->




@endsection
