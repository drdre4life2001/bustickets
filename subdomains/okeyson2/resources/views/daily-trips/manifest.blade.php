@extends('layouts.master')

@section('content')
<script>
$(document).ready(function()
{
   //sortData();
});
function printDiv(divName) {
     var printContents = document.getElementById(divName).innerHTML;
     var originalContents = document.body.innerHTML;

     document.body.innerHTML = printContents;

     window.print();

     document.body.innerHTML = originalContents;
}

 // Table data sorting starts....
        function sortData(){
            //alert();
            var tableData = document.getElementById('manifest').getElementsByTagName('tbody').item(0);
            var rowData = tableData.getElementsByTagName('tr');            
            for(var i = 0; i < rowData.length - 1; i++){
                for(var j = 0; j < rowData.length - (i + 1); j++){
                             if(parseInt(rowData.item(j).getElementsByTagName('td').item(0).innerHTML) > parseInt(rowData.item(j+1).getElementsByTagName('td').item(0).innerHTML)){
                        tableData.insertBefore(rowData.item(j+1),rowData.item(j));
                    }
                }
            }
        }
</script>
    <div class="content-header">
        <!--h2 class="content-header-title">Trip Manifest </h2-->
        <ol class="breadcrumb">
          <li><a href="{{ url('/') }}">Dashboard</a></li>
          <li><a href="{{ url('/daily-trips') }}">Daily trips</a></li>
          <li class="active">Manifest </li>
        </ol>
      </div> <!-- /.content-header -->

      

      <div class="row">


        <div class="col-md-10">

        <div class="portlet">

            <div class="portlet-content" id ="print-area-1">
              <!-- <p class="pull-right">EFOSA EXPRESS LTD <small>RC120517</small>  </p> --> 
            <center><img src ="{{ asset('logos/'.session('operator')->img)}}" }}" height="70px;" width="115px;"><br>
              <b>VEHICLE PASSENGER MANIFEST</b>
            </center>
             
            
            <center>
            <table class="table" align="center" style="font-size:10px;">
            <tr>
             <td width="50%" align="left"><b>Lagos Office:</b> {{session('operator')->address}} 
             <br><b>Tel:</b> {{session('operator')->phone}}<br>
              <b>Email:</b> {{ session('operator')->email }}<br><b>Website:</b> {{ session('operator')->website }}</td>
            
            </tr>
            <tr>
           <td width="100%" colspan="2" align="justify">
             
              <b>MAKE OF VEHICLE: </b>{{ $dailytrip->bus->bus_type->name }}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <b>TYPE OF VEHICLE: </b>{{ $dailytrip->bus->bus_type->name }}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <b>COLOUR:</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <b>NUMBER PLATE: </b>{{ $dailytrip->bus->number_plate }}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <b>ENGINE NO:</b>{{ $dailytrip->bus->engine_number }}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <b>CHASSIS NO:</b>{{ $dailytrip->bus->chassis_number }}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <b>DEPARTURE POINT: </b> {{ $dailytrip->trip->sourcepark->name }}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
            <b>DESTINATION: </b>{{ $dailytrip->trip->destpark->name }}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <b>DEPARTURE TIME: </b> {{ $dailytrip->trip->departure_time }}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
            <b>ARRIVAL TIME:</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <b>DATE:</b> {{date('d-m-y', strtotime($dailytrip->trip_date))}}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
            </td>
            </tr>
            </table>
            </center>
            
           
            <!-- <div>
              <div align="left" class="pull-left">
              <b>From:</b> {{ $dailytrip->trip->sourcepark->name }} <br>
              <b>Date:</b>{{date('d-m-y', strtotime($dailytrip->trip_date))}}
              </div>
              
              <div align="right" class="pull-right">
              <b>To:</b> {{ $dailytrip->trip->destpark->name }} <br>
              <b>Vehicle No: </b>
                 @if(isset($dailytrip->bus))
                <strong>{{ $dailytrip->bus->bus_number }}</strong> <br />
              @endif
              </div>
            </div> -->
            
           
         <!--  <h3>#{{ $operator->code.'-'.date('dmy', strtotime($dailytrip->trip_date)). $dailytrip->id }} <small class="pull-right">{{ date('M dS, Y',  strtotime($dailytrip->trip_date)) }}</small></h3> -->




          <!-- <div class="row">

            <div class="col-md-4 col-sm-4">

              <p><strong>Trip Details:</strong></p>


              <table class="table">
                <tbody>
                  <tr>
                    <td style="width: 90px">
                      <div class="thumbnail">
                      <i class="fa fa-road fa-3x"></i>
                     <img src="./img/avatars/avatar-1.jpg" alt="Avatar" style="width: 90px;" /> 
                      </div>
                    </td>
                    <td>
                      <strong>{{ $operator->name }}</strong> <br />
                      
                      {{ date('M dS, Y',  strtotime($dailytrip->trip_date)) }} - 
                      {{ date('h:iA',  strtotime($dailytrip->trip->departure_time)) }}
                    </td>
                  </tr>
                </tbody>
              </table>  


            </div> 

            <div class="col-md-4 col-sm-4">

              <p><strong>Driver Details:</strong></p>


              <table class="table">
                <tbody>
                  <tr>
                    <td style="width: 90px">
                    <div class="thumbnail">
                    <i class="fa fa-user fa-3x"></i>
                    <!-- <img src="./img/avatars/avatar-2.jpg" alt="Avatar" style="width: 90px;" /> 
                      </div>
                  </td>
                    <td>
                      @if(isset($dailytrip->driver))
                      <strong>{{ $dailytrip->driver->name }}</strong> <br />
                      {{ $dailytrip->driver->phone }} <br />

                      @endif
                      
                    </td>
                  </tr>
                </tbody>
              </table>


            </div> <!-- /.col 

            <div class="col-md-4 col-sm-4">

              <p><strong>Vehicle Details:</strong></p>
               <table class="table">
                <tbody>
                  <tr>
                    <td style="width: 90px">
                    <div class="thumbnail">
                    <i class="fa fa-bus fa-3x"></i>
                    <!-- <img src="./img/avatars/avatar-2.jpg" alt="Avatar" style="width: 90px;" /> 
                      </div>
                  </td>
                    <td>
                      @if(isset($dailytrip->bus))
                      <strong>{{ $dailytrip->bus->bus_number }}</strong> <br />
                      {{ $dailytrip->bus->number_plate }} <br />
                      {{ $dailytrip->bus->vin }} <br />
                      @endif
                    </td>
                  </tr>
                </tbody>
              </table>
            </div> <!-- /.col 
          </div> <!-- /.row --> 
          

          <!-- <h3 align ="left" class="pull-left"> {{ $dailytrip->booked_seats  }} Passengers</h3> -->

          <div>
          <table class="table table-bordered" id="manifest" style="font-size:10px;line-height: 0.9em;">
            <thead>
              <tr>
              <th>S/N.</th>
                <th>Seat No.</th>
                <th>PASSENGER'S <br/> NAME</th>
                <th>PHONE NO</th>
                <th>SEX</th>
                <th>DESTINATION</th>
                <th>ADDRESS</th>
                <th>NEXT OF KIN</th>
                <th>PHONE OF <br/> NEXT OF KIN</th>
                
              </tr>
            </thead>  
            <tbody>
            <div align ="center" style="font-size: 10px;">
              @foreach($dailytrip->bookings as $b)
                
              <?php 
              if(isset($destAmount[$b->trip->destpark->name]))
                 $destAmount[$b->trip->destpark->name] += $b->final_cost; 
               else
                $destAmount[$b->trip->destpark->name] = $b->final_cost;
                
                //dd($destAmount);

              if(isset($destCount[$b->trip->destpark->name]))
                 $destCount[$b->trip->destpark->name] += $b->passenger_count; 
               else
               $destCount[$b->trip->destpark->name] = $b->passenger_count; 
              ?> 
             @endforeach
             
             @foreach($destAmount as $k=>$v)
                {{$k}} - ({{ $destCount[$k] }} pax) &nbsp;&nbsp;
             @endforeach
             </div>
                {{-- */$x=0; $tf =0; $de=0; $fuel=0; $oe = 0; /* --}}
             @foreach($passengerObj as $pass)
                {{-- */ $x++; /* --}}
                <tr>
                <td> {{ $x}}  </td>
                <td> {{ $pass->seat_no}}  </td>
                <td>{{ $pass->contact_name }} </td>               
                <td>{{ $pass->contact_phone }}</td>
                <td>{{ ucfirst(substr($pass->gender,0,1)) }}</td>
                <td>{{ $pass->destPark }}</td> 
                <th>{{ $pass->contact_address }}</th>
                <td>{{ $pass->next_of_kin }}</td>
                <td>{{ $pass->next_of_kin_phone }}</td>
                </tr>
                @if(isset($pass->children))
                    @foreach($pass->children as $child)
                 {{-- */ $x++; /* --}}
                        <tr>
                            <td> {{ $x}}  </td>
                            <td>  </td>
                            <td>{{ $child->contact_name }} @if(($child->age >= $excludePassenger->age_one) && ($child->age <= $excludePassenger->age_two)) (Infant) @endif</td>               
                            <td>{{ $child->contact_phone }}</td>
                            <td>{{ ucfirst(substr($child->gender,0,1)) }}</td>
                            <td>{{ $child->destPark }}</td>
                            <td>{{ $child->contact_address }}</td>
                            <td>{{ $child->next_of_kin }}</td>
                            <td>{{ $child->next_of_kin_phone }}</td>
                            </tr>
                    @endforeach
                @endif
             @endforeach
            </tbody>
          </table>
          </div> <!-- /.table-resonsive -->


        
          <table width="100%" style="font-size:9px;">
            <tr>
                {{-- */$x=0; $extlug =0;$sum=0; /* --}}
                      
                          @foreach($dailytrip->bookings as $b)
                      {{-- */
                                        $extlug += $b->extra_luggages->sum('amount');
                       /* --}}
                                    @endforeach
                @foreach($dailytrip->expenses as $e)
                {{-- */
                 $sum+= $e->amount;
                /* --}}
              @endforeach
                                   
            <td width="33%" align="left"><b>Total Amount: </b> &#8358;{{ number_format($dailytrip->total_fare+$extlug- ($sum)) }}</td><br>
            <td width="33%" align="left">&nbsp;</td>
            <td width="33%" align="left"><b>Driver's Name: </b>{{ $dailytrip->driver->name }} </td>
            </tr>  

             <tr>
            <td width="33%" align="left">
              
            </td>
            <td width="33%" align="left"><b>Manager's Sign</b> ________________</td>
            <td width="33%" align="left"><b>Driver's Sign</b> _____________</td>
            </tr>  



            
          </table>
         

          
          

         

          </div>
        </div>

        </div> <!-- /.col -->


        <div class="col-md-2 col-sidebar-right">

          <p><a onclick="printDiv('print-area-1')" id="" class="btn btn-primary"> <i class="fa fa-print"></i> Print Manifest <br /></a></p>

          <!-- <p><a href="javascript:;" class="btn btn-lg btn-tertiary btn-block">Close Invoice</a></p> -->

          <br><br>

            


          <br />


          
        </div> <!-- /.col -->


      </div> <!-- /.row -->




@endsection
