@extends('layouts.master')

@section('content')

    <div class="content-header">
        <h2 class="content-header-title">Edit Daily-trip </h2>
        <ol class="breadcrumb">
          <li><a href="{{ url('/') }}">Dashboard</a></li>
          <li><a href="{{ url('daily-trips') }}">Trip Schedules </a></li>
          <li class="active">Edit Trip Schedule </li>
        </ol>
      </div> <!-- /.content-header -->

      

      <div class="row">

        <div class="col-md-10 col-sm-8">

          <div class="portlet">

            <div class="portlet-content">

                {!! Form::model($daily-trip, [
                    'method' => 'PATCH',
                    'url' => ['daily-trips', $daily-trip->id],
                    'class' => 'form-horizontal'
                ]) !!}

                            <div class="form-group {{ $errors->has('trip_date') ? 'has-error' : ''}}">
                {!! Form::label('trip_date', trans('daily-trips.trip_date'), ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::date('trip_date', null, ['class' => 'form-control', 'required' => 'required']) !!}
                    {!! $errors->first('trip_date', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('trip_id') ? 'has-error' : ''}}">
                {!! Form::label('trip_id', trans('daily-trips.trip_id'), ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::number('trip_id', null, ['class' => 'form-control', 'required' => 'required']) !!}
                    {!! $errors->first('trip_id', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('driver_id') ? 'has-error' : ''}}">
                {!! Form::label('bus_id', trans('daily-trips.driver_id'), ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::number('bus_id', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('bus_id', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('vehicle_id') ? 'has-error' : ''}}">
                {!! Form::label('vehicle_id', trans('daily-trips.vehicle_id'), ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::number('vehicle_id', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('vehicle_id', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('total_fare') ? 'has-error' : ''}}">
                {!! Form::label('total_fare', trans('daily-trips.total_fare'), ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::number('total_fare', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('total_fare', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('driver_allowance') ? 'has-error' : ''}}">
                {!! Form::label('driver_allowance', trans('daily-trips.driver_allowance'), ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::number('driver_allowance', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('driver_allowance', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('fuel_expense') ? 'has-error' : ''}}">
                {!! Form::label('fuel_expense', trans('daily-trips.fuel_expense'), ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::number('fuel_expense', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('fuel_expense', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('other_expense') ? 'has-error' : ''}}">
                {!! Form::label('other_expense', trans('daily-trips.other_expense'), ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::number('other_expense', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('other_expense', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('fulfilled') ? 'has-error' : ''}}">
                {!! Form::label('fulfilled', trans('daily-trips.fulfilled'), ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                                <div class="checkbox">
                <label>{!! Form::radio('fulfilled', '1') !!} Yes</label>
            </div>
            <div class="checkbox">
                <label>{!! Form::radio('fulfilled', '0', true) !!} No</label>
            </div>
                    {!! $errors->first('fulfilled', '<p class="help-block">:message</p>') !!}
                </div>
            </div>


                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-3">
                        {!! Form::submit('Update', ['class' => 'btn btn-primary form-control']) !!}
                    </div>
                </div>
                {!! Form::close() !!}

                @if ($errors->any())
                    <ul class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                @endif

             </div> <!-- /.portlet-content -->

          </div> <!-- /.portlet -->

        

        </div> <!-- /.col -->

        <div class="col-md-2 col-sm-4">

            <ul id="myTab" class="nav nav-pills nav-stacked">
            <li class="active">
                {!! Form::open([
                    'method'=>'DELETE',
                    'url' => ['daily-trips', $daily-trip->id],
                    'style' => 'display:inline'
                ]) !!}
                    {!! Form::button('<i class="fa fa-times"></i> Delete Daily-trip', ['class' => 'btn btn-danger btn-xs', 'escape'=>false,'title'=>'Delete', 'type'=>'submit']) !!}
                {!! Form::close() !!}
            </li>
            <li>
              <a href="{{ url('daily-trips') }}">
                <i class="fa fa-bars"></i> 
                List Trip Schedules
              </a>
            </li>
            <li class="">
              <a href="{{ url('daily-trips/create') }}">
                <i class="fa fa-plus"></i> 
                Add Trip Schedule
              </a>
            </li>
          </ul>

        </div>

      </div> <!-- /.row -->   

@endsection