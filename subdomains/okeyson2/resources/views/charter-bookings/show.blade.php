@extends('layouts.master')

@section('content')

 <script language="javascript" type="text/javascript">
        function printDiv(divName) {
     var printContents = document.getElementById(divName).innerHTML;
     var originalContents = document.body.innerHTML;

     document.body.innerHTML = printContents;

     window.print();

     document.body.innerHTML = originalContents;
}
    </script>

<style type="text/css">
  

  #holder{  
   height:200px;   
   width:550px;
   background-color:#F5F5F5;
   border:1px solid #A4A4A4;
   margin-left:10px;  
  }
   #place {
   position:relative;
   margin:7px;
   }
     #place a{
   font-size:0.6em;
   }
     #place li
     {
         list-style: none outside none;
         position: absolute;   
     }    
     #place li:hover
     {
        background-color:yellow;      
     } 
   #place .seat{
   background:url("{{ asset('bckend/img/available_seat_img.gif') }}") no-repeat scroll 0 0 transparent;
   height:33px;
   width:33px;
   display:block;  
   }

   #place .no-seat{
   /*background:url("{{ asset('bckend/img/available_seat_img.gif') }}") no-repeat scroll 0 0 transparent;*/
   height:33px;
   width:33px;
   display:block;  
   }


      #place .selectedSeat
      { 
    background-image:url('{{ asset('bckend/img/booked_seat_img.gif') }}');        
      }
     #place .selectingSeat
      { 
    background-image:url("{{asset('bckend/img/selected_seat_img.gif') }}");        
      }
    #place .row-3, #place .row-4{
    margin-top:10px;
    }
   #seatDescription{
   padding:0px;
   }
    #seatDescription li{
    verticle-align:middle;    
    list-style: none outside none;
     padding-left:35px;
    height:35px;
    float:left;
    }
    </style>


<body>
<div class="container">
    <h1>Charter Booking Details</h1>
    
<div class="col-md-2 col-sm-2 pull-right">
    @if($charterbooking['status'] == 'PAID')
    <div class="portlet" id="cancelDiv">
        <div class="portlet-content">
            <a onclick="javascript:printDiv('print-area-1')" class="btn btn-block btn-success">
              <i class="fa fa-print"></i>
              Print Ticket
            </a>
        </div>
    </div>
  @else
        <div class="portlet">
                <div class="portlet-header">
                  <h3>
                    <i class="fa fa-reorder"></i>
                    Actions
                  </h3>
                </div> <!-- /.portlet-header -->
                <div class="portlet-content">
                  <!-- <a href="#" class="btn btn-danger">ADD Booking Notes</a> -->
                  <a type="button" class="btn btn-primary btn-block" href="{{ url('charter-bookings/' . $charterbooking['id'] . '/edit') }}"> Edit  </a>
                </div> <!-- /.portlet-content -->
        </div>
  @endif
</div>
<div class="alert alert-success hide" id="paidAlertDiv">
  Booking has been marked as PAID. <!--You can now print the Ticket-->
</div>

<div>
    <div class="portlet" style="border:0px;">
            <div class="portlet-content" style="width:300px; font-size:16px" align="justify" id ="print-area-1">
               <div align="left" style="width:300px;">
               <small>
               
               <table align="justify" class="table table-bordered table-condensed">
                
                   <tr><td>
                   <img class="pull-right" src ="{{ asset('logos/'.session('operator')->img)}}" height="100px;" width="115px;"><br>
                  <center><b>CHARTER BOOKING TICKET</b></center>
                   </td></tr>
                   
                 <tr>
                  <td><b>Date:</b> {{ date('D, d/m/Y', strtotime($charterbooking->travel_date)) }}</td>
                 </tr>
                <!-- <tr>
                  <td><b>Time:</b></td><td>&nbsp;</td>
               </tr> -->
                <tr>
                  <td><b>Fare Paid:</b> &#8358;{{ number_format($charterbooking->agreed_price) }}</td>
               </tr>
              <tr>
                  <td width="100%"><b>Booking Code:</b> {{ $charterbooking->booking_code }}</td>
               </tr>
                

            <tr>
              <td width="100%"><b>Station:</b> {{ (!empty($charterbooking->park))?$charterbooking->park->name:'No Selected yet' }} </td>
              </tr>
              
              <tr>
              <td width="100%"><b>Vehicle No:</b> <center style="font-size: 200%;">{{ (!empty($charterbooking->bus))?$charterbooking->bus->bus_number:'No Selected yet' }}</center></td>
              </tr>
              
              <tr>
              <td width="100%"><b>Destination:</b>{{ $charterbooking->destination }}</td>
              </tr>
            
               <tr>
                  <td width="100%"><b>Contact Person:</b> {{ $charterbooking->contact_name }}</td>
               </tr>
             <tr>
            <td width="100%"><b>Pickup Location: </b>{{ $charterbooking->pickup_location }}</td>
            </tr> 
            <tr>
            <td width="100%"><b>Amount</b>  &#8358;{{ number_format($charterbooking->agreed_price) }}</td>
            </tr>
            
           <!--  <tr>
            <td>{{ $condition  }}</td>
           
            
            </tr>
            --> 
            <tr>
            <td class="col-sm-12" align="justify">This company takes no liability for good or luggage whether paid for or not Appropriate fare must be paid for the journey and no refund of money after payment</td>
            </tr>
            <tr align="justify">
            <td><small>
             <b>OFFICE</b><br>{{session('operator')->address}}<br>
             <b>TELEPHONE</b> <br>{{session('operator')->phone}}<br>
             <b>EMAIL</b> <br>{{session('operator')->email}}<br>
         </td>
     </tr>
              <!-- <tr>
              <td class="col-sm-12 col-offset-6" align="left" ><div class="pull-right"><b> {{ $condition4 }}</b></div>
              </td>
              
              </tr> -->
            <tr>
            <td class="col-sm-12 col-offset-6" align="left" >
            <!-- <div class="pull-right"><b>Seat No</b> &nbsp;</div><br> -->

            <div class="pull-right"><b>Ticketer: {{ $charterbooking['ticketer']['first_name'].' '.$charterbooking['ticketer']['last_name'] }}</b> &nbsp;</div>

            
            </td>
            
            </tr>
            </table>
            </small>
    
                 
              <!-- <dl>
                <dt>Booking Code</dt>
                <dd> {{ $charterbooking->booking_code }} </dd>
               
                <dt>Date of Book</dt>
               
                <dd> {{ date('D, d/m/Y', strtotime($charterbooking->created_at)) }} </dd>
                

                <dt>Date of Travel</dt>
                <dd> {{ date('D, d/m/Y', strtotime($charterbooking->travel_date)) }} </dd>

                <br/><br/>

                <dt>STATUS</dt>
                <dd><span  id='statusField' class="label label-warning">{{ $charterbooking->status }}</span> </dd>
                <br/><br/>

                <dt>Bus </dt>
                <dd>{{ (!empty($charterbooking->bus))?$charterbooking->bus->bus_number:'No Selected yet' }}</dd>

                <dt>Driver </dt>
                <dd>{{ (!empty($charterbooking->driver))?$charterbooking->driver->name:'No Selected yet' }}</dd>

                <dt>Park </dt>
                <dd>{{ (!empty($charterbooking->park))?$charterbooking->park->name:'No Selected yet' }}</dd>

                <dt>Pick Up Location </dt>
                <dd>{{ $charterbooking->pickup_location }}</dd>

                <dt>Destination </dt>
                <dd>{{ $charterbooking->destination }}</dd>

                

                <dt>Contact Name </dt>
                <dd>{{ $charterbooking->contact_name }}</dd>

                <dt>Contact Email </dt>
                <dd>{{ $charterbooking->contact_email}}</dd>

                <dt>Contact Phone </dt>
                <dd>{{ $charterbooking->contact_phone }}</dd>

                <dt>Num of Passengers </dt>
                <dd>{{ $charterbooking->number_of_passengers }}</dd>
                
                <dt>Agreed Price</dt>
                <dd>&#8358;{{ number_format($charterbooking->agreed_price) }}</dd>
                
              </dl> -->
            </div> 
    </div>
</div>
</div>
@endsection