@extends('layouts.master')

@section('content')

    <div class="content-header">
        <h2 class="content-header-title">Edit Charter-booking </h2>
        <ol class="breadcrumb">
          <li><a href="{{ url('/') }}">Dashboard</a></li>
          <li><a href="{{ url('charter-bookings') }}">Charter-bookings </a></li>
          <li class="active">Edit Charter-booking </li>
        </ol>
      </div> <!-- /.content-header -->

      

      <div class="row">

        <div class="col-md-10 col-sm-8">

          <div class="portlet">

            <div class="portlet-content">

                {!! Form::model($charterbooking, [
                    'method' => 'PATCH',
                    'url' => ['charter-bookings', $charterbooking->id],
                    'class' => 'form-horizontal'
                ]) !!}

             <div class="form-group {{ $errors->has('pickup_location') ? 'has-error' : ''}}">    

                {!! Form::label('travel_date', trans('charter-bookings.travel_date'), ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                     <div id="dp-ex-3" class="input-group date" data-auto-close="true" data-date-format="dd-mm-yyyy" data-date-autoclose="true">
                        {!! Form::date('travel_date', null, ['class' => 'datepicker form-control']) !!}
                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                    </div>     
                    
                    {!! $errors->first('travel_date', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('pickup_location') ? 'has-error' : ''}}">
                {!! Form::label('pickup_location', trans('charter-bookings.pickup_location'), ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::textarea('pickup_location', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('pickup_location', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('destination') ? 'has-error' : ''}}">
                {!! Form::label('destination', trans('charter-bookings.destination'), ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::textarea('destination', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('destination', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('park_id') ? 'has-error' : ''}}">
                {!! Form::label('park_id', 'Park', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::select('park_id', $parks,null, ['class' => 'form-control', 'id'=>"select22", 'required' => 'required']) !!}
                    {!! $errors->first('park_id', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('bus_id') ? 'has-error' : ''}}">
                {!! Form::label('bus_id', 'Bus', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::select('bus_id', $buses,null, ['class' => 'form-control', 'id'=>"select22", 'required' => 'required']) !!}
                    {!! $errors->first('bus_id', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('driver_id') ? 'has-error' : ''}}">
                {!! Form::label('driver_id', 'Driver', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::select('driver_id', $drivers, null, ['class' => 'form-control', 'id'=>"select22", 'required' => 'required']) !!}
                    {!! $errors->first('driver_id', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('contact_name') ? 'has-error' : ''}}">
                {!! Form::label('contact_name', trans('charter-bookings.contact_name'), ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('contact_name', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('contact_name', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('contact_phone') ? 'has-error' : ''}}">
                {!! Form::label('contact_phone', trans('charter-bookings.contact_phone'), ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('contact_phone', null, ['class' => 'form-control','maxlength'=>'11']) !!}
                    {!! $errors->first('contact_phone', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('contact_email') ? 'has-error' : ''}}">
                {!! Form::label('contact_email', trans('charter-bookings.contact_email'), ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('contact_email', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('contact_email', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('number_of_passengers') ? 'has-error' : ''}}">
                {!! Form::label('number_of_passengers', trans('charter-bookings.number_of_passengers'), ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::number('number_of_passengers', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('number_of_passengers', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('agreed_price') ? 'has-error' : ''}}">
                {!! Form::label('agreed_price', trans('charter-bookings.agreed_price'), ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::number('agreed_price', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('agreed_price', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            
            <div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
                {!! Form::label('status', trans('charter-bookings.status'), ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('status', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('paid_date') ? 'has-error' : ''}}">
                {!! Form::label('paid_date', trans('charter-bookings.paid_date'), ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    <div id="dp-ex-2" class="input-group date" data-auto-close="true" data-date-format="dd-mm-yyyy" data-date-autoclose="true">
                        {!! Form::date('paid_date', null, ['class' => 'form-control']) !!}
                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                    </div>     
                    
                    {!! $errors->first('paid_date', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('payment_method_id') ? 'has-error' : ''}}">
                {!! Form::label('payment_method_id', 'Payment Menthod', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::select('payment_method_id', $payment_methods, null, ['class' => 'form-control', 'id'=>"payment_method_id", 'required' => 'required']) !!}
                    {!! $errors->first('payment_method_id', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('comment') ? 'has-error' : ''}}">
                {!! Form::label('comment', trans('charter-bookings.comment'), ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::textarea('comment', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('comment', '<p class="help-block">:message</p>') !!}
                </div>
            </div>


                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-3">
                        {!! Form::submit('Update', ['class' => 'btn btn-primary form-control']) !!}
                    </div>
                </div>
                {!! Form::close() !!}

                @if ($errors->any())
                    <ul class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                @endif

             </div> <!-- /.portlet-content -->

          </div> <!-- /.portlet -->

        

        </div> <!-- /.col -->

        <div class="col-md-2 col-sm-4">

            <ul id="myTab" class="nav nav-pills nav-stacked">
            <li class="active">
                {!! Form::open([
                    'method'=>'DELETE',
                    'url' => ['charter-bookings', $charterbooking->id],
                    'style' => 'display:inline'
                ]) !!}
                    {!! Form::button('<i class="fa fa-times"></i> Delete Charter-booking', ['class' => 'btn btn-danger btn-xs', 'escape'=>false,'title'=>'Delete', 'type'=>'submit']) !!}
                {!! Form::close() !!}
            </li>
            <li>
              <a href="{{ url('charter-bookings') }}">
                <i class="fa fa-bars"></i> 
                List Charter-bookings
              </a>
            </li>
            <li class="">
              <a href="{{ url('charter-bookings/create') }}">
                <i class="fa fa-plus"></i> 
                Add Charter-booking
              </a>
            </li>
          </ul>

        </div>

      </div> <!-- /.row -->   

@endsection