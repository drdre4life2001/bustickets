@extends('layouts.master')

@section('content')

   <div class="content-header">
        <h2 class="content-header-title">Add Charter-booking </h2>
        <ol class="breadcrumb">
          <li><a href="{{ url('/') }}">Dashboard</a></li>
          <li><a href="{{ url('charter-bookings') }}">Charter-bookings </a></li>
          <li class="active">Add Charter-booking </li>
        </ol>
      </div> <!-- /.content-header -->



      <div class="row">

        <div class="col-md-10 col-sm-8">

          <div class="portlet">

            <div class="portlet-content">

            {!! Form::open(['url' => 'charter-bookings', 'class' => 'form-horizontal']) !!}

            <div class="form-group {{ $errors->has('travel_date') ? 'has-error' : ''}}">


                {!! Form::label('travel_date', trans('charter-bookings.travel_date'), ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                <div id="dp-ex-3" class="input-group date" data-auto-close="true" data-date="dd/mm/yyyy" data-date-format="dd-mm-yyyy" data-date-autoclose="true">
                    <input class="form-control" type="text" name="travel_date" id="travel_date">
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>

               </div> <!-- /.col -->

                    {!! $errors->first('travel_date', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('pickup_location') ? 'has-error' : ''}}">
                {!! Form::label('pickup_location', trans('charter-bookings.pickup_location'), ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::textarea('pickup_location', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('pickup_location', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('destination') ? 'has-error' : ''}}">
                {!! Form::label('destination', trans('charter-bookings.destination'), ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::textarea('destination', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('destination', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('park_id') ? 'has-error' : ''}}">
                {!! Form::label('park_id', 'Park', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::select('park_id', $parks,null, ['class' => 'form-control', 'id'=>"select22", 'required' => 'required']) !!}
                    {!! $errors->first('park_id', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('bus_id') ? 'has-error' : ''}}">
                {!! Form::label('bus_id', 'Bus', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::select('bus_id', $buses,null, ['class' => 'form-control',  'required' => 'required']) !!}
                    {!! $errors->first('bus_id', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('bus_id') ? 'has-error' : ''}}">
                {!! Form::label('bus_seats', 'Bus Seat Capacity', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    <input type="text" readonly="readonly" class="form-control" name="bus_seats" id="bus_seats">
                    {!! $errors->first('bus_seats', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('driver_id') ? 'has-error' : ''}}">
                {!! Form::label('driver_id', 'Driver', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::select('driver_id', $drivers, null, ['class' => 'form-control', 'id'=>"driver_id", 'required' => 'required']) !!}
                    {!! $errors->first('driver_id', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('contact_name') ? 'has-error' : ''}}">
                {!! Form::label('contact_name', 'Passenger Name', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('contact_name', '', ['class' => 'form-control']) !!}
                    {!! $errors->first('contact_name', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('contact_phone') ? 'has-error' : ''}}">
                {!! Form::label('contact_phone', 'Passenger Phone', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::number('contact_phone', '', ['class' => 'form-control','id'=>'contact_phone','maxlength'=>'11']) !!}
                     <span> Mobile (0xxxxxxxxxx)</span>
                    {!! $errors->first('contact_phone', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('contact_email') ? 'has-error' : ''}}">
                {!! Form::label('contact_email', 'Passenger Email', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::email('contact_email', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('contact_email', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('number_of_passengers') ? 'has-error' : ''}}">
                {!! Form::label('number_of_passengers', trans('charter-bookings.number_of_passengers'), ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::number('number_of_passengers', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('number_of_passengers', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('agreed_price') ? 'has-error' : ''}}">
                {!! Form::label('agreed_price', trans('charter-bookings.agreed_price'), ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::number('agreed_price', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('agreed_price', '<p class="help-block">:message</p>') !!}
                </div>
            </div>

            <div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
                {!! Form::label('status', trans('charter-bookings.status'), ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::select('status', $statuses, null, ['class' => 'form-control', 'id'=>"select22", 'required' => 'required']) !!}
                    {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('paid_date') ? 'has-error' : ''}}">


                {!! Form::label('paid_date', trans('charter-bookings.paid_date'), ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                <div id="dp-ex-4" class="input-group date" data-auto-close="true" data-date="dd/mm/yyyy" data-date-format="dd-mm-yyyy" data-date-autoclose="true">
                    <input class="form-control" type="text" name="paid_date" id="paid_date">
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>

               </div> <!-- /.col -->

                    {!! $errors->first('paid_date', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('payment_method_id') ? 'has-error' : ''}}">
                {!! Form::label('payment_method_id', 'Payment Method', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::select('payment_method_id', $payment_methods, null, ['class' => 'form-control', 'id'=>"payment_method_id", 'required' => 'required']) !!}
                    {!! $errors->first('payment_method_id', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('receipt_no') ? 'has-error' : ''}}" id="receipt">
              {!! Form::label('receiptNo', 'ReceiptNo: ', ['class' => 'col-sm-3 control-label']) !!}
              <div class="col-sm-6">
              <input type="text" id="receipt_no" name="receipt_no" class="form-control" maxlength="50">
              {!! $errors->first('receiptNo', '<span class="parsley-error-list">:message</span>') !!}
              </div>
              </div>

            <div class="form-group {{ $errors->has('comment') ? 'has-error' : ''}}">
                {!! Form::label('comment', trans('charter-bookings.comment'), ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::textarea('comment', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('comment', '<p class="help-block">:message</p>') !!}
                </div>
            </div>


                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-3">
                        {!! Form::submit('Create', ['class' => 'btn btn-primary form-control', 'id'=>'sub']) !!}
                    </div>
                </div>
                {!! Form::close() !!}

                @if ($errors->any())
                    <ul class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                @endif
             </div> <!-- /.portlet-content -->

          </div> <!-- /.portlet -->



        </div> <!-- /.col -->

        <div class="col-md-2 col-sm-4">

            <ul id="myTab" class="nav nav-pills nav-stacked">
            <li class="active">
              <a href="{{ url('charter-bookings') }}">
                <i class="fa fa-bars"></i>
                List Charter-bookings
              </a>
            </li>
          </ul>

        </div>

      </div> <!-- /.row -->

<script>
$(document).ready(function(){

//caters for bulk sms sending


// caters for receipt if card
$("#receipt").hide();
// on change
$("#payment_method_id").on('change', function(){
  // get what is method is being used
  var paymtMthd =  $("#payment_method_id").val();
  if(paymtMthd == 3)
  {
    $("#receipt").show();
  }
  else
    $("#receipt").hide();

});

// validate bus capacity against no of passengers

$("#number_of_passengers").on('blur', function(){
  var noP = $("#number_of_passengers").val();
  var seatCap = eval($("#bus_seats").val());

 if(noP > seatCap){
    $("#number_of_passengers").focus();
  alert("Number of Passengers is more than bus capacity");

}
});

$("#bus_id").on('change', function()
{
var busNo = $("#bus_id").val();
// get the text
var busSelected = $('#bus_id').find('option:selected').text();
// perform an ajax call

            $.ajax({
                    url: '{{ route("get-charter-bus-details") }}',
                    dataType: 'text',
                    type: 'GET',
                    contentType: 'application/x-www-form-urlencoded',
                    data: "bus_id="+(busNo),
                    success: function (rslts, textStatus, jQxhr)
                    {
                     $("#bus_seats").val(rslts);

                    },
                    error: function (jqXhr,textStatus, errorThrown)
                    {
                        console.log(errorThrown);
                    }
      });


$('#driver_id option')[busNo-1].selected = true;
});

$("#driver_id").on('change', function(){
var driverId = $("#driver_id").val();
$('#bus_id option')[driverId-1].selected = true;
});



});
</script>

 <!-- <script>
    // this part does the auto remember of customer details
    $("#contact_phone").on('blur', function()
   {
      var phone =  $("#contact_phone").val();
      if(phone !="")
      {
           // use phone value to perform ajax request
            $.ajax({
                    url: '{{ route("get-charter-cust-details") }}',
                    dataType: 'text',
                    type: 'GET',
                    contentType: 'application/x-www-form-urlencoded',
                    data: "ph="+phone,
                    success: function (rslts, textStatus, jQxhr)
                    {

                      var rsltsArr = rslts.split("|");
                        // DISPLAY THOSE RESULTS
                        $("#contact_name").val(rsltsArr[0]);
                        $("#contact_email").val(rsltsArr[1]);

                    },
                    error: function (jqXhr,textStatus, errorThrown)
                    {
                        console.log(errorThrown);
                        $("#contact_name").val("");
                        $("#contact_email").val("");
                    }

      });
    }
  });
    </script> -->
@endsection
