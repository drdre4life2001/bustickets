@extends('layouts.master')

@section('content')

<div class="content-header">
        <h2 class="content-header-title">Ticketers Reports</h2>
        <ol class="breadcrumb">
          <li><a href="./index.html">Dashboard</a></li>
          <li class="active">Reports</li>
        </ol>
      </div> <!-- /.content-header -->

      

      <div class="row">

        <div class="col-md-12 col-sm-8">

          <div class="tab-content stacked-content">

            <div class="tab-pane fade in active" id="profile-tab">
              
              <h3 class="">Tickters Report</h3>
              <br/>

              <h4 class="heading-inline pull-left">
          &nbsp;&nbsp;<small>Choose Ticketer</small>
          &nbsp;&nbsp;</h4>
              <div class="btn-group pull-left">
                  <div class="form-group">
                    <select id="select-input" class="form-control" onchange="refreshReport();">
                      <option value="0" @if(empty($ticketer_id)) selected="selected" @endif > -ALL- </option>
                      @foreach($ticketers as $key => $value)
                        <option value="{{ $key }}" @if($key == $ticketer_id) selected="selected" @endif>{{ $value }}</option>
                      @endforeach
                    </select>
              </div>
              </div>

              <h4 class="heading-inline pull-left">
          &nbsp;&nbsp;<small>Park</small>
          &nbsp;&nbsp;</h4>
              <div class="btn-group pull-left">
                  <div class="form-group">
                    <select id="parkSel" class="form-control" onchange="refreshReport();">
                      <option value="0" > -ALL- </option>
                      @foreach($parks as $key => $value)
                        <option value="{{ $key }}" @if($park_id == $key) selected="selected" @endif>{{ $value }}</option>
                      @endforeach
                      
                    </select>
              </div>
              </div>


              <h4 class="heading-inline pull-left">
          &nbsp;&nbsp;<small>Destination</small>
          &nbsp;&nbsp;</h4>
              <div class="btn-group pull-left">
                  <div class="form-group">
                    <select id="destSel" class="form-control" onchange="refreshReport();">
                      <option value="0" > -ALL- </option>
                      @foreach($destinations as $key => $value)
                        <option value="{{ $key }}" @if($dest_id == $key) selected="selected" @endif>{{ $value }}</option>
                      @endforeach
                      
                    </select>
              </div>
              </div>

              <div class="btn-group pull-right">
                  <div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
                      <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
                      <span></span> <b class="caret"></b>
                  </div>
              </div>
          <h4 class="heading-inline pull-right">
          &nbsp;&nbsp;<small>Choose date range</small>
          &nbsp;&nbsp;</h4>



              <hr />

              <br /><br />
              <div class="well">
                <h4><span class="text-primary">Summary</span></h4>
                
                  <p>
                    <span class="text-primary">Total Number Paid: </span><strong  id="totalPaid">0</strong> | 
                    <span class="text-primary">Total Paid Revenue: </span>&#8358;<strong id="paidRev">0</strong>
                    
                    
                  </p>

              </div>

              <div class="table-responsive">

              <table id="tDataTable"
                class="table table-striped table-bordered table-hover table-highlight table-checkable" 
                data-provide="datatable1" 
                data-display-rows="10"
                data-info="true"
                data-search="true"
                data-length-change="true"
                data-paginate="true"
              >
                  <thead>
                    <tr>
                      <th>Ticketer Name</th>
                      <th>Total Bookings</th>
                      <th>Total Number of Paid</th>
                      <th>Total Number of Pending</th>
                      <th>Total Number of Cancelled</th>
                      <th>Total Paid Revenue</th>
                      <th>Park</th>
                      <th>Destination</th>
                      
                      
                    </tr>
                  </thead>
                  <tbody>
                    {{-- */$x=0;/* --}}
                    @foreach($users as $item)

                        {{-- */$x++;/* --}}
                        <tr id="trips_num{{ $item->id }}" class="loadTicketerInfo" data-user-id="{{ $item->id }}" data-user-name="{{ $item->first_name.' '.$item->last_name }}" >
                            <?php if(empty($item->first_name)) continue; ?>
                            <td>{{ $item->first_name.' '.$item->last_name }}</td>
                            <td colspan=7 >
                              loading....
                              
                            </td>
                        </tr>
                    @endforeach
                  </tbody>
                </table>


            </div> <!-- /.tab-pane --> 

               <!--  <div class="btn-group">
                <button data-toggle="dropdown" class="btn btn-info dropdown-toggle" type="button">
                  <i class="fa fa-download"></i> Download <span class="caret"></span>
                </button>
                <ul role="menu" class="dropdown-menu">
                  <li><a href="javascript:;">Download as PDF</a></li>
                  <li><a href="javascript:;">Download as Excel</a></li>
                  
                </ul>
              </div> -->

            </div>

        </div> <!-- /.col -->

      </div> <!-- /.row -->


<script type="text/javascript">
$("#select-input").on('change',function(e){
  e.preventDefault();
  var user = $('#select-input').val();
    $.ajax
         (
             {
                type: "POST",
                url: "{{ url('get-user-park') }}",
                data: ({ user : user, _token: '{{ csrf_token() }}' }),
                success: function(html)
                {
                    
                
                }
          });

});

function updateUrl(name, value){
            window.location.search = jQuery.query.set(name, value);

            return false;
       } 

function refreshReport(){

    window.location = '{{ route("ticketers-reports") }}/'+$('#select-input').val()+'/'+$('#parkSel').val()+'/'+$('#destSel').val();

}


$(function() {

    function cb(start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    }
    cb(moment("{{ $start_date }}"), moment("{{ $end_date }}"));

    $('#reportrange').daterangepicker({
        ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cb);

    $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
        console.log(picker.startDate.format('YYYY-MM-DD'));
        console.log(picker.endDate.format('YYYY-MM-DD'));
        
        updateUrl('daterange', picker.startDate.format('YYYY-MM-DD')+' '+picker.endDate.format('YYYY-MM-DD'));
    });

     $( ".loadTicketerInfo" ).each(function( index, element ) {
         var user_id = $(element).attr('data-user-id');
         var user_name = $(element).attr('data-user-name');
         // $( element ).html( '<span style="font-style:italic;font-size:80%;"><img width="15px" src="{{ asset("desktop/img/ajax-loader-fb.gif") }}">');
         $.ajax
         (
             {
                type: "POST",
                url: "{{ url('get-ticketers-bookings') }}",
                data: ({ rnd : Math.random() * 100000, user_id: user_id, user_name: user_name,start_date: '{{ $start_date }}' , end_date: '{{ $end_date }}', _token: '{{ csrf_token() }}', park_id:'{{ $park_id }}', dest_id: '{{ $dest_id }}' }),
                success: function(html)
                {
                     $( element ).html(html);
                     $(element).removeClass('loadBusInfo');

                }
          });

     });

});
</script>


<script type="text/javascript">
  
setTimeout(function(){
  $("#tDataTable").dataTable ( {
        dom: 'Bfrtip',
        "order": [[ 3, "desc" ]],
        buttons: [
            'excel', 'pdf', 'print'
        ]
    })
}, 5000);

</script>




 @endsection     