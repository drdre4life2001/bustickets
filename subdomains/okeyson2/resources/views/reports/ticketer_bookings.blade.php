<td>
  {{  $user_name }}
</td>
<td>
  {{ number_format(count($paids) + count($pendings) + count($cancels)) }} booking(s)
</td>
<td>
  {{-- */$x=0;/* --}}
  <div class = "modal fade" id="paidModal{{ $user_id }}" tabindex = "-1" role = "dialog" aria-labelledby = "myModalLabel" aria-hidden = "true">
   <div class = "modal-dialog" style="width:1000px;">
      <div class = "modal-content">
         <div class = "modal-header">
            <button type = "button" class = "close" data-dismiss = "modal" aria-hidden = "true">
                  &times;
            </button>
            <h4 class = "modal-title" id = "myModalLabel">
               Paid Trips for {{ $user_name }} between {{ date('D, M d, Y',strtotime($start_date)) }} and {{ date('D, M d, Y',strtotime($end_date)) }}  :: {{ count($paids) }} Trip(s)
            </h4>
         </div>

         <div class="modal-body">

         <div class="table-responsive">

              <table 
                class="table table-striped table-bordered table-hover table-highlight table-checkable" 
                data-provide="datatable" 
                data-display-rows="10"
                data-info="true"
                data-search="true"
                data-length-change="true"
                data-paginate="true"
              >
                  <thead>
                    <tr>
                      <th>Booking code</th>
                      <th>From</th>
                      <th>To</th>
                      <th>Date</th>
                      <th>Final Cost</th>
                      <th>Status</th>
                      <th>Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    
                    @foreach($paids as $booking)

                        {{-- */ $x += $booking->final_cost  ;/* --}}
                        <tr>
                            <td>{{ $booking->booking_code }}</td>
                            <td>{{ $booking->trip->sourcepark->name }}</td>
                            <td>{{ $booking->trip->destpark->name }}</td>
                            <td>{{ date('M dS', strtotime($booking->date)) }}</td>
                            <td>&#8358;{{ number_format($booking->final_cost) }}</td>
                            <td>{{ $booking->status}} </td>
                            <td>
                                <a href="{{ url('bookings/' . $booking->id ) }}" target="_blank" title="View">
                                    <button type="submit" class="btn btn-primary"><i class="fa fa-eye"></i></button>
                                </a> 

                               
                            </td>
                        </tr>
                    @endforeach
                  </tbody>
                </table>


            </div> <!-- /.tab-pane --> 
            </div>
         
         <div class = "modal-footer">
            <button type = "button" class = "btn btn-default" data-dismiss = "modal">
               Close
            </button>
         </div>
      </div><!-- /.modal-content -->
   </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

  <a type="button" href="#" class=" " data-toggle="modal" data-target="#paidModal{{ $user_id }}">{{ number_format(count($paids)) }} booking(s)</a>
</td>
<td>
  <div class = "modal fade" id="pendingModal{{ $user_id }}" tabindex = "-1" role = "dialog" aria-labelledby = "myModalLabel" aria-hidden = "true">
   <div class = "modal-dialog" style="width:1000px;">
      <div class = "modal-content">
         <div class = "modal-header">
            <button type = "button" class = "close" data-dismiss = "modal" aria-hidden = "true">
                  &times;
            </button>
            <h4 class = "modal-title" id = "myModalLabel">
               Pending Trips for {{ $user_name }} between {{ date('D, M d, Y',strtotime($start_date)) }} and {{ date('D, M d, Y',strtotime($end_date)) }}  :: {{ count($paids) }} Trip(s)
            </h4>
         </div>

         <div class="modal-body">

         <div class="table-responsive">

              <table 
                class="table table-striped table-bordered table-hover table-highlight table-checkable" 
                data-provide="datatable" 
                data-display-rows="10"
                data-info="true"
                data-search="true"
                data-length-change="true"
                data-paginate="true"
              >
                  <thead>
                    <tr>
                      <th>Booking code</th>
                      <th>From</th>
                      <th>To</th>
                      <th>Date</th>
                      <th>Final Cost</th>
                      <th>Status</th>
                      <th>Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    
                    @foreach($pendings as $booking)

                        {{-- */ $x += $booking->final_cost  ;/* --}}
                        <tr>
                            <td>{{ $booking->booking_code }}</td>
                            <td>{{ $booking->trip->sourcepark->name }}</td>
                            <td>{{ $booking->trip->destpark->name }}</td>
                            <td>{{ date('M dS', strtotime($booking->date)) }}</td>
                            <td>&#8358;{{ number_format($booking->final_cost) }}</td>
                            <td>{{ $booking->status}} </td>
                            <td>
                                <a href="{{ url('bookings/' . $booking->id ) }}" target="_blank" title="View">
                                    <button type="submit" class="btn btn-primary"><i class="fa fa-eye"></i></button>
                                </a> 

                               
                            </td>
                        </tr>
                    @endforeach
                  </tbody>
                </table>


            </div> <!-- /.tab-pane --> 
            </div>
         
         <div class = "modal-footer">
            <button type = "button" class = "btn btn-default" data-dismiss = "modal">
               Close
            </button>
         </div>
      </div><!-- /.modal-content -->
   </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

  <a type="button" href="#" class=" " data-toggle="modal" data-target="#pendingModal{{ $user_id }}">{{ number_format( count($pendings) ) }} booking(s)</a>
</td>
<td>
  <div class = "modal fade" id="cancelModal{{ $user_id }}" tabindex = "-1" role = "dialog" aria-labelledby = "myModalLabel" aria-hidden = "true">
   <div class = "modal-dialog" style="width:1000px;">
      <div class = "modal-content">
         <div class = "modal-header">
            <button type = "button" class = "close" data-dismiss = "modal" aria-hidden = "true">
                  &times;
            </button>
            <h4 class = "modal-title" id = "myModalLabel">
               Cancelled Trips for {{ $user_name }} between {{ date('D, M d, Y',strtotime($start_date)) }} and {{ date('D, M d, Y',strtotime($end_date)) }}  :: {{ count($paids) }} Trip(s)
            </h4>
         </div>

         <div class="modal-body">

         <div class="table-responsive">

              <table 
                class="table table-striped table-bordered table-hover table-highlight table-checkable" 
                data-provide="datatable" 
                data-display-rows="10"
                data-info="true"
                data-search="true"
                data-length-change="true"
                data-paginate="true"
              >
                  <thead>
                    <tr>
                      <th>Booking code</th>
                      <th>From</th>
                      <th>To</th>
                      <th>Date</th>
                      <th>Final Cost</th>
                      <th>Status</th>
                      <th>Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    
                    @foreach($cancels as $booking)

                        {{-- */ $x += $booking->final_cost  ;/* --}}
                        <tr>
                            <td>{{ $booking->booking_code }}</td>
                            <td>{{ $booking->trip->sourcepark->name }}</td>
                            <td>{{ $booking->trip->destpark->name }}</td>
                            <td>{{ date('M dS', strtotime($booking->date)) }}</td>
                            <td>&#8358;{{ number_format($booking->final_cost) }}</td>
                            <td>{{ $booking->status}} </td>
                            <td>
                                <a href="{{ url('bookings/' . $booking->id ) }}" target="_blank" title="View">
                                    <button type="submit" class="btn btn-primary"><i class="fa fa-eye"></i></button>
                                </a> 

                               
                            </td>
                        </tr>
                    @endforeach
                  </tbody>
                </table>


            </div> <!-- /.tab-pane --> 
            </div>
         
         <div class = "modal-footer">
            <button type = "button" class = "btn btn-default" data-dismiss = "modal">
               Close
            </button>
         </div>
      </div><!-- /.modal-content -->
   </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

  <a type="button" href="#" class=" " data-toggle="modal" data-target="#cancelModal{{ $user_id }}">{{ number_format( count($cancels) ) }} booking(s)</a>
</td>
<td>
  <span style="color:green;">  &#8358;{{ number_format($x) }}</span>
</td>
<td>{{ $park_name }}</td>
<td>{{ $dest_name }}</td>


<script type="text/javascript">

  $(function() {
     console.log($("#totalTrips").html()) ;
     console.log(parseInt($("#totalTrips").html())) ;
    var tTrips = parseInt($("#totalPaid").html()) + parseInt({{ count($paids) }});
    $("#totalPaid").html(tTrips);

    var tTrips = parseInt($("#paidRev").html()) + parseInt({{ $x }});
    $("#paidRev").html(tTrips);

  });
</script>





