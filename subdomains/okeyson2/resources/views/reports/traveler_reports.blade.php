@extends('layouts.master')

@section('content')

<div class="content-header">
        <h2 class="content-header-title">Reports</h2>
        <ol class="breadcrumb">
          <li><a href="./index.html">Dashboard</a></li>
          <li class="active">Reports</li>
        </ol>
      </div> <!-- /.content-header -->

      

      <div class="row">

      

        <div class="col-md-12 col-sm-8">

          <div class="tab-content stacked-content">

            <div class="tab-pane fade in active" id="profile-tab">
              
              <h3 class="">Travelers Report</h3>

              <div class="btn-group pull-right">
              <div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
                  <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
                  <span></span> <b class="caret"></b>
              </div>
          </div>
          <h4 class="heading-inline pull-left">
          &nbsp;&nbsp;<small>Park</small>
          &nbsp;&nbsp;</h4>
              <div class="btn-group pull-left">
                  <div class="form-group">
                    <select id="parkSel" class="form-control" onchange="refreshReport();">
                      <option value="0" > -ALL- </option>
                      @foreach($parks as $key => $value)
                        <option value="{{ $key }}" @if($park_id == $key) selected="selected" @endif>{{ $value }}</option>
                      @endforeach
                      
                    </select>
              </div>
              </div>


          <h4 class="heading-inline pull-right">
          &nbsp;&nbsp;<small>Choose date range</small>
          &nbsp;&nbsp;</h4>



              <hr />

              <br /><br />

              <div class="table-responsive">

              <table id="tReportTable"
                class="table table-striped table-bordered table-hover table-highlight table-checkable" 
                data-provide="datatable1" 
                data-display-rows="10"
                data-info="true"
                data-search="true"
                data-length-change="true"
                data-paginate="true"
              >
                  <thead>
                    <tr>
                      <th>Traveler Name</th>
                      <th>Traveler Phone</th>
                      <th>Traveler Email</th>
                      <th>Travel Times</th>
                      
                    </tr>
                  </thead>
                  <tbody>
                    {{-- */$x=0;/* --}}
                    @foreach($bookings as $item)

                        {{-- */$x++;/* --}}
                        <tr>
                            <td>{{ $item['contact_name'] }}</td>
                            <td>{{ $item['contact_phone'] }}</td>
                            <td>{{ $item['contact_email'] }}</td>
                            <td><span class="travel-times" data-phone="{{ $item['contact_phone'] }}" ></span></td>
                            
                        </tr>

                      
                    @endforeach
                  </tbody>
                </table>


            </div> <!-- /.tab-pane --> 

           <!--  <div class="btn-group">
                <button data-toggle="dropdown" class="btn btn-info dropdown-toggle" type="button">
                  <i class="fa fa-download"></i> Download <span class="caret"></span>
                </button>
                <ul role="menu" class="dropdown-menu">
                  <li><a href="javascript:;">Download as PDF</a></li>
                  <li><a href="javascript:;">Download as Excel</a></li>
                  
                </ul>
              </div>

            </div> -->

        </div> <!-- /.col -->

      </div> <!-- /.row -->

      </div>


<script type="text/javascript">

function updateUrl(name, value){
            window.location.search = jQuery.query.set(name, value);

            return false;
       }


function refreshReport(){

    window.location = '{{ route("traveler-reports") }}/'+$('#parkSel').val();

} 


$(function() {

    function cb(start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    }
    cb(moment("{{ $start_date }}"), moment("{{ $end_date }}"));

    $('#reportrange').daterangepicker({
        ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cb);

    $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
        console.log(picker.startDate.format('YYYY-MM-DD'));
        console.log(picker.endDate.format('YYYY-MM-DD'));
        
        updateUrl('daterange', picker.startDate.format('YYYY-MM-DD')+' '+picker.endDate.format('YYYY-MM-DD'));
    });


    $( ".travel-times" ).each(function( index, element ) {
         var phone = $(element).attr('data-phone');
         // $( element ).html( '<span style="font-style:italic;font-size:80%;"><img width="15px" src="{{ asset("desktop/img/ajax-loader-fb.gif") }}">');
         $.ajax
         (
             {
                type: "POST",
                url: "{{ url('get-phone-bookings-count') }}",
                data: ({ rnd : Math.random() * 100000, phone: phone, start_date: '{{ $start_date }}' , end_date: '{{ $end_date }}', _token: '{{ csrf_token() }}' }),
                success: function(html)
                {
                     $( element ).html(html);
                     $(element).removeClass('travel-times');

                }
          });

     });

});


</script>

<script type="text/javascript">
  
setTimeout(function(){
  $("#tReportTable").dataTable ( {
        dom: 'Bfrtip',
        "order": [[ 3, "desc" ]],
        buttons: [
            'excel', 'pdf', 'print'
        ]
    })
}, 5000);

</script>




 @endsection     