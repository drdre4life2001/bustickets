@extends('layouts.master')

@section('content')

<div class="content-header">
        <h2 class="content-header-title">Reports</h2>
        <ol class="breadcrumb">
          <li><a href="./index.html">Dashboard</a></li>
          <li class="active">Reports</li>
        </ol>
      </div> <!-- /.content-header -->

      

      <div class="row">

        <div class="col-md-12 col-sm-8">

          <div class="tab-content stacked-content">

            <div class="tab-pane fade in active" id="profile-tab">
              
              <h3 class="">Bookings Report</h3>
              <br/>

              <h4 class="heading-inline pull-left">
          &nbsp;&nbsp;<small>Choose Status</small>
          &nbsp;&nbsp;</h4>
              <div class="btn-group pull-left">
                  <div class="form-group">
                    <select id="select-input" class="form-control" onchange="refreshReport();">
                      <option value="ALL" @if($status == 'ALL') selected="selected" @endif > -ALL- </option>
                      <option @if($status == 'PENDING') selected="selected" @endif >PENDING</option>
                      <option @if($status == 'PAID') selected="selected" @endif>PAID</option>
                      <option @if($status == 'CANCELLED') selected="selected" @endif>CANCELLED</option>
                    </select>
              </div>
              </div>

              <h4 class="heading-inline pull-left">
          &nbsp;&nbsp;<small>Choose Park</small>
          &nbsp;&nbsp;</h4>
              <div class="btn-group pull-left">
                  <div class="form-group">
                    <select id="park-input" class="form-control" onchange="refreshReport();">
                      <option value="0" @if(empty($park_id)) selected="selected" @endif > -ALL- </option>
                      @foreach($parks as $key => $value)
                        <option value="{{ $key }}" @if($park_id == $key) selected="selected" @endif>{{ $value }}</option>
                      @endforeach
                      
                    </select>
                  </div>

                  
              </div>





              <div class="btn-group pull-right">
                  <div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
                      <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
                      <span></span> <b class="caret"></b>
                  </div>
              </div>
          <h4 class="heading-inline pull-right">
          &nbsp;&nbsp;<small>Choose date range</small>
          &nbsp;&nbsp;</h4>



              <hr />

              <br /><br />
              <div class="well">
                <h4><span class="text-primary">Summary</span></h4>
                
                  <p>
                    <span class="text-primary">Total Pending: </span> <strong id="totalPending"></strong> | 
                    <span class="text-primary">Total Paid: </span> <strong id="totalPaid"></strong> | 
                    <span class="text-primary">Total Cancelled: </span> <strong id="totalCancelled"></strong>
                    
                    
                  </p>

              </div>

              <div class="table-responsive">

              <table 
                class="table table-striped table-bordered table-hover table-highlight table-checkable" 
                data-provide="datatable" 
                data-display-rows="10"
                data-info="true"
                data-search="true"
                data-length-change="true"
                data-paginate="true"
              >
                  <thead>
                    <tr>
                      <th>Booking code</th>
                      <th>From</th>
                      <th>To</th>
                      <th>Booking Date</th>
                      <th>Final Cost</th>
                      <th>Traveler Name</th>
                      <th>Passenger Count</th>
                      <th>Traveler Phone</th>
                      <th>Traveler Email</th>
                      <th>Status</th>
                      <th>Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    {{-- */$x=$pend = $pendAmount = $paid = $paidAmount = $cancel = $cancelAmount =  0;  /* --}}
                    @foreach($bookings as $item)

                        {{-- */
                          $x++;
                          switch($item['status']){
                            case 'PENDING':
                              $pend++;
                              $pendAmount += $item['final_cost'];
                              break;
                            case 'PAID':
                              $paid++;
                              $paidAmount += $item['final_cost'];
                              break;
                            case 'CANCELLED':
                              $cancel++;
                              $cancelAmount += $item['final_cost'];
                              break;
                          }

                        /* --}}
                        <tr>

                            <td>{{ $item['booking_code'] }}</td>
                            <td>{{ $item['trip']['sourcepark']['name'] }}</td>
                            <td>{{ $item['trip']['destpark']['name'] }}</td>
                            <td>{{ date('M dS', strtotime($item['date'])) }}</td>
                            <td>&#8358;{{ number_format($item['final_cost']) }} @if($item['is_flexi']) <strong>(Flexi)</strong> @endif</td>
                            <td>{{ $item['contact_name'] }}</td>
                            <td>{{ $item['passenger_count'] }}</td>
                            <td>{{ $item['contact_phone'] }}</td>
                            <td>{{ $item['contact_email'] }}</td>
                            <td>{{ $item['status']}} </td>
                            <td>
                                <a href="{{ url('bookings/' . $item['id'] ) }}" target="_blank" title="View">
                                    <button type="submit" class="btn btn-primary"><i class="fa fa-eye"></i></button>
                                </a> 

                                <!-- <a href="{{ url('bookings/' . $item['id'] . '/edit') }}" title="Edit">
                                    <button type="submit" class="btn btn-default"><i class="fa fa-edit"></i></button>
                                </a> 
                                
                                {!! Form::open([
                                    'method'=>'DELETE',
                                    'url' => ['bookings', $item['id']],
                                    'style' => 'display:inline'
                                ]) !!}
                                    {!! Form::button('<i class="fa fa-times"></i>', ['class' => 'btn btn-danger btn-xs', 'escape'=>false,'title'=>'Delete', 'type'=>'submit', 'onclick'=>'ConfirmDelete()']) !!}
                                {!! Form::close() !!} -->
                                
                            </td>
                        </tr>
                    @endforeach
                  </tbody>
                </table>


            </div> <!-- /.tab-pane --> 

                <!-- setting totals on top -->
                <script type="text/javascript">

                  $(function() {
                    $("#totalPending").html("{{ number_format($pend) }} trip(s) : &#8358;{{ number_format($pendAmount, 2) }}");
                    $("#totalPaid").html("{{ number_format($paid) }} trip(s) : &#8358;{{ number_format($paidAmount, 2) }}");
                    $("#totalCancelled").html("{{ number_format($cancel) }} trip(s) : &#8358;{{ number_format($cancelAmount, 2) }}");
                    
                    
                  });
                </script>


            </div>

        </div> <!-- /.col -->

      </div> <!-- /.row -->


<script type="text/javascript">

function updateUrl(name, value){
            window.location.search = jQuery.query.set(name, value);

            return false;
       } 

function refreshReport(){

  console.log($('#select-input').val());
  window.location = '{{ route("booking-reports") }}/'+$('#select-input').val()+'/'+$('#park-input').val();


}


$(function() {

    function cb(start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    }
    cb(moment("{{ $start_date }}"), moment("{{ $end_date }}"));

    $('#reportrange').daterangepicker({
        ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cb);

    $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
        console.log(picker.startDate.format('YYYY-MM-DD'));
        console.log(picker.endDate.format('YYYY-MM-DD'));
        
        updateUrl('daterange', picker.startDate.format('YYYY-MM-DD')+' '+picker.endDate.format('YYYY-MM-DD'));
    });

});
</script>




 @endsection     