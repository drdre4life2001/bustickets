



          <ul id="myTab" class="nav nav-pills  nav-stacked">
            <li>
              <a href="{{ route('trips-reports') }}">
                <i class="fa fa-shooping-cart"></i> 
                &nbsp;&nbsp;Daily Trips Reports
              </a>
            </li>
            <li>
              <a href="{{ route('drivers-reports') }}">
                <i class="fa fa-shooping-cart"></i> 
                &nbsp;&nbsp;Drivers Reports
              </a>
            </li>
            <li>
              <a href="{{ route('vehicles-reports') }}">
                <i class="fa fa-shooping-cart"></i> 
                &nbsp;&nbsp;Buses Reports
              </a>
            </li>
            <li>
              <a href="{{ route('traveler-reports') }}" >
                <i class="fa fa-users"></i> 
                &nbsp;&nbsp;Travelers Reports
              </a>
            </li>
            <li class="active">
              <a href="{{ route('booking-reports') }}">
                <i class="fa fa-files-o"></i> 
                &nbsp;&nbsp;Bookings Reports
              </a>
            </li>
            <li>
              <a href="{{ route('park-reports') }}">
                <i class="fa fa-map-marker"></i> 
                &nbsp;&nbsp;Park Reports
              </a>
            </li>
            <li>
              <a href="{{ route('payment-reports') }}">
                <i class="fa fa-shooping-cart"></i> 
                &nbsp;&nbsp;Payment methods
              </a>
            </li>
            
            <!--li>
              <a href="#reports" data-toggle="tab">
                <i class="fa fa-signal"></i> 
                &nbsp;&nbsp;Configure Reports
              </a>
            </li-->
          </ul>