@extends('layouts.master')

@section('content')

<div class="content-header">
        <h2 class="content-header-title">Reports</h2>
        <ol class="breadcrumb">
          <li><a href="./index.html">Dashboard</a></li>
          <li class="active">Reports</li>
        </ol>
      </div> <!-- /.content-header -->

      

      <div class="row">

        <div class="col-md-12 col-sm-8">

          <div class="tab-content stacked-content">

            <div class="tab-pane fade in active" id="profile-tab">
              
              <h3 class="">Luggages Report</h3>
              <br/>

              
              <h4 class="heading-inline pull-left">
          &nbsp;&nbsp;<small>Select Luggage weight range</small>
          &nbsp;&nbsp;</h4>
              <div class="btn-group pull-left">
                  <div class="form-group">
                    <select id="select-input" class="form-control" onchange="refreshReport();">
                      <option> -ALL- </option>
                      <option value="0-10">0-10 kg</option>
                      <option value="10-50">10-50 kg</option>
                      <option value="50-100">50-100 kg</option>
                    </select>
              </div>
              </div>

              <h4 class="heading-inline pull-left">
          &nbsp;&nbsp;<small>Choose Parks</small>
          &nbsp;&nbsp;</h4>
              <div class="btn-group pull-left">
                  <div class="form-group">
                    <select id="parkSel" class="form-control" onchange="refreshReport();">
                      <option value="0"> -All- </option>
                      @foreach($parks as $key => $value)
                        <option value="{{ $key }}" @if($park_id == $key) selected="selected" @endif>{{ $value }}</option>
                      @endforeach
                    </select>
              </div>
              </div>



              <div class="btn-group pull-right">
                  <div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
                      <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
                      <span></span> <b class="caret"></b>
                  </div>
              </div>
          <h4 class="heading-inline pull-right">
          &nbsp;&nbsp;<small>Choose date range</small>
          &nbsp;&nbsp;</h4>



              <hr />

              <br /><br />
              <div class="well">
                <h4><span class="text-primary">Summary</span></h4>
                
                  <p>
                    <span class="text-primary">Total Luggage weight: </span> <strong id="luggage_weight">{{$total_luggage_weight}} kg</strong> | 
                    <span class="text-primary">Total Luggage cost: </span> <strong id="luggage_cost">&#8358;{{$total_luggage_cost}}</strong> | 
                    <span class="text-primary">Total Extra Luggage weight: </span> <strong id="extra_luggage_weight">{{$total_extra_luggage_weight}} kg</strong> |
                    <span class="text-primary">Total Extra Luggage cost: </span> <strong id="extra_luggage_cost">&#8358;{{$total_extra_luggage_cost}}</strong>
                    
                    
                  </p>

              </div>

                      <div class="table-responsive" >

                      <table id="reportTable" 
                        class="table table-striped table-bordered table-hover table-highlight table-checkable" 
                        data-provide="datatable1" 
                        data-display-rows="10"
                        data-info="true"
                        data-search="true"
                        data-length-change="true"
                        data-paginate="true"
                      >
                          <thead>
                            
                            <tr>
                              
                              <th>Booking code</th>
                              <th>Ticketer</th>
                              <th>Passenger</th>
                              <th>Luggage Weight</th>
                              <th>Luggage Cost</th>
                              <th>Extra Luggage weight</th>
                              <th>Extra Luggage cost</th>
                              
                            </tr>
                          </thead>
                          <tbody>
                          @foreach($bookings as $booking)
                               <tr>
                                 <td>{{$booking->booking_code or ''}}</td>
                                 <td>{{$booking->ticketer->first_name or '' }} {{$booking->ticketer->last_name or '' }}</td>
                                 <td>{{$booking->contact_name or '' }}<br/>
                                 @foreach($booking->passengers as $passenger)
                                 [ {{$passenger->name or ''}} - {{$passenger->gender or ''}}

                                  @if(($passenger->age >= $excludePassenger->age_one) && ($passenger->age <= $excludePassenger->age_two)) (Infant) @endif

                                  ]<br/>
                                 @endforeach
                                 </td>
                                 <td>{{$booking->luggage_weight or '' }} kg</td>
                                 <td>&#8358;{{$booking->luggage_cost or '' }} </td>
                                 <td>{{$booking->extra_luggages->sum('luggage_weight') }} kg</td>                                    
                                 <td>&#8358;{{$booking->extra_luggages->sum('amount') }}</td>                                    
                                </tr>
                          @endforeach
                                
                          </tbody>
                        </table>


                    </div> <!-- /.tab-pane --> 

                    </div>

                </div> <!-- /.col -->

              </div> <!-- /.row -->


<script type="text/javascript">
   function refreshReport(){
    if($('#select-input').val() != "ALL")
      window.location = '{{ route("luggages-reports") }}/'+$('#select-input').val();

     //window.location = '{{ route("park-reports") }}/'+$('#parkSel').val()+'/'+$('#destSel').val();

    }
$(function() {

  function updateUrl(name, value){
            window.location.search = jQuery.query.set(name, value);

            return false;
   }



    function cb(start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    }
    cb(moment("{{ $start_date }}"), moment("{{ $end_date }}"));

    $('#reportrange').daterangepicker({
        ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cb);

    $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
        console.log(picker.startDate.format('YYYY-MM-DD'));
        console.log(picker.endDate.format('YYYY-MM-DD'));
        
        updateUrl('daterange', picker.startDate.format('YYYY-MM-DD')+' '+picker.endDate.format('YYYY-MM-DD'));
    });

});
</script>


@endsection