{{-- */ $ae = 0; /* --}}

<td>

<div class = "modal fade" id="manModal{{ $bus_id }}" tabindex = "-1" role = "dialog" aria-labelledby = "myModalLabel" aria-hidden = "true">
   <div class = "modal-dialog" style="width:1000px;">
      <div class = "modal-content">
         <div class = "modal-header">
            <button type = "button" class = "close" data-dismiss = "modal" aria-hidden = "true">
                  &times;
            </button>
            <h4 class = "modal-title" id = "myModalLabel">
               Trips for {{ $bus_number }} between {{ date('D, M d, Y',strtotime($start_date)) }} and {{ date('D, M d, Y',strtotime($end_date)) }}  :: {{ count($daily_trips) }} Trip(s)
            </h4>
         </div>

         <div class="modal-body">

         <div class="table-responsive">

              <table 
                class="table table-striped table-bordered table-hover table-highlight table-checkable" 
                data-provide="datatable" 
                data-display-rows="10"
                data-info="true"
                data-search="true"
                data-length-change="true"
                data-paginate="true"
              >
                  <thead>
                    <tr>
                      <th>Trip Details</th>
                      <th>Bookings Details</th>
                      <th>Driver Details</th>
                      <th>Financial Details</th>
                      <th>Status</th>
                      <th>Actions</th>

                    </tr>
                  </thead>
                  <tbody>
                    {{-- */$x=0;$extlug=0;/* --}}
                    @foreach($daily_trips as $item)

                        {{-- */
                            $x++;
                            $ae += $item->expenses()->sum('amount');  
                        /* --}}
                      
                       @foreach($item->bookings as $b)
                      {{-- */
                                        $extlug += $b->extra_luggages->sum('amount');
                       /* --}}
                        @endforeach
                      
                      
                        <tr>
                            <td>
                              <strong>Trip Date:</strong> {{ date('D, M d, Y',strtotime($item->trip_date)) }}<br/>
                              <strong>From:</strong> {{ $item->trip->sourcepark->name }}<br/>
                              <strong>To:</strong> @foreach($item->trip->subTrips as $st) {{ $st->destpark->name }}<br/> @endforeach
                              <strong>Time:</strong> {{ date('h:iA', strtotime($item->trip->departure_time)) }}<br/>
                              <strong>Bus Type:</strong> {{ $item->bus->bus_type->name }}<br/>
                              <strong>Fare:</strong> &#8358;{{ number_format($item->trip->fare, 2) }}<br/>
                            </td>
                            <td>
                              <strong>Booked seats:</strong> {{ $item->booked_seats }}<br/>
                              <strong>Total seats:</strong> {{ $item->total_seats }}<br/>
                              <strong>Total Fare:</strong><span style="color:green;"> &#8358;{{ number_format($item->total_fare, 2) }}</span><br/>
                            </td>
                            <td>
                              <strong>Driver:</strong> {{ (!empty($item->driver))?$item->driver->name:'' }}<br/>
                              <strong>Bus Number:</strong> {{ (!empty($item->bus))?$item->bus->bus_number:"" }}<br/>
                              <strong>Bus number plate:</strong> {{ (!empty($item->bus))?$item->bus->number_plate:'' }}<br/>
                              
                            </td>
                            <td>
                              @foreach($item->expenses as $e)
                                <?php if(empty($e->amount)) continue; ?>
                                <strong>{{ $e->expense_name }}:</strong><span style="color:red;"> &#8358;{{ number_format($e->amount, 2) }}</span><br/>

                              @endforeach 
                              <strong>Net Revenue:</strong><span style="color:green;"> &#8358;{{ number_format((($item->total_fare+$extlug) - $ae), 2) }}</span><br/>
                              
                            </td>
                            
                            <td>{{ $item->status }}</td>
                            <td>
                              
                              <a type="button" href="{{ route('manifest', $item->id) }}" target="_blank" > View Manifest</a> <br/>
                              
                            </td>
                        </tr>
                    @endforeach
                  </tbody>
                </table>


            </div> <!-- /.tab-pane --> 
            </div>
         
         <div class = "modal-footer">
            <button type = "button" class = "btn btn-default" data-dismiss = "modal">
               Close
            </button>
         </div>
      </div><!-- /.modal-content -->
   </div><!-- /.modal-dialog -->
</div><!-- /.modal -->




  {{  $bus_number }}
</td>
<td>
  {{ count($daily_trips) }} trip(s)
</td>
<td>
  {{ number_format($daily_trips->sum('booked_seats')) }}
</td>
<td>
  <span style="color:green;">  &#8358;{{ number_format($daily_trips->sum('total_fare')) }}</span>
</td>
<td>
  <span style="color:red;"> - &#8358;{{ number_format($ae)  }}</span>
</td>

<td>
  <span style="color:green;">  &#8358;{{ number_format(($daily_trips->sum('total_fare')+$extlug) - $ae) }} </span>
</td>
<td>
  <a type="button" href="#" class=" " data-toggle="modal" data-target="#manModal{{ $bus_id }}">View details</a> 
  
</td>

<script type="text/javascript">

  $(function() {
     console.log($("#totalTrips").html()) ;
     console.log(parseInt($("#totalTrips").html())) ;
    var tTrips = parseInt($("#totalTrips").html()) + parseInt({{ count($daily_trips) }});
    $("#totalTrips").html(tTrips);
  });
</script>

