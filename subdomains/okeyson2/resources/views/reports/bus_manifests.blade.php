<td>
  {{  $bus_number }}
</td>
<td>
  {{ number_format($daily_trips->sum('booked_seats')) }}
</td>
<td>
  {{ count($daily_trips) }} trip(s)
</td>
<td>
  <span style="color:green;">  &#8358;{{ number_format($daily_trips->sum('total_fare')) }}</span>
</td>
<td>
  <a type="button" href="#" class=" " data-toggle="modal" data-target="#manModal{{ $bus_id }}">View Manifest</a>	
  <div class = "modal fade" id="manModal{{ $bus_id }}" tabindex = "-1" role = "dialog" aria-labelledby = "myModalLabel" aria-hidden = "true">
   <div class = "modal-dialog" style="width:1000px;">
      <div class = "modal-content">
         <div class = "modal-header">
            <button type = "button" class = "close" data-dismiss = "modal" aria-hidden = "true">
                  &times;
            </button>
            <h4 class = "modal-title" id = "myModalLabel">
               Trips for {{ $bus_number }} between {{ date('D, M d, Y',strtotime($start_date)) }} and {{ date('D, M d, Y',strtotime($end_date)) }}  :: {{ count($daily_trips) }} Trip(s)
            </h4>
         </div>

         <div class="modal-body">

         <div class="table-responsive">

              <table id="mtable{{ $bus_id }}" 
                class="table table-striped table-bordered table-hover table-highlight table-checkable" 
                data-provide="datatable" 
                data-display-rows="10"
                data-info="true"
                data-search="true"
                data-length-change="true"
                data-paginate="true"
              >
                  <thead>
                    <tr>
                      <th>Booking Date</th>
                      <th>Travel Date</th>
                      <th>Passenger Name</th>
                      <th>Fare</th>
                      <th>Park</th>
                      <th>Destination</th>
                      

                    </tr>
                  </thead>
                  <tbody>
                    
                    @foreach($daily_trips as $dt)

                        @foreach($dt->bookings as $booking)
                          <tr>
                              <td> {{ date('D, M d, Y',strtotime($booking->created_at)) }} </td>
                              <td> {{ date('D, M d, Y',strtotime($booking->date)) }} </td>
                              <td> {{ $booking->contact_name }} </td>
                              <td>   &#8358;{{ number_format($booking->unit_cost) }} </td>
                              <td> {{ $dt->trip->sourcepark->name }}</td>
                              <td> {{ $dt->trip->destpark->name }} </td>
                          </tr>

                          @foreach($booking->passengers as $p)

                          <tr>
                              <td> {{ date('D, M d, Y',strtotime($booking->created_at)) }} </td>
                              <td> {{ date('D, M d, Y',strtotime($booking->date)) }} </td>
                              
                              @if(($p->age >= $excludePassenger->age_one) && ($p->age <= $excludePassenger->age_two))
                              <td> {{ $p->name }}  (Infant)</td>
                              <td> </td>
                              @else
                               <td> {{ $p->name }}</td>
                              <td>   &#8358;{{ number_format($booking->unit_cost) }} </td>
                              @endif
                              <td> {{ $dt->trip->sourcepark->name }}</td>
                              <td> {{ $dt->trip->destpark->name }} </td>
                          </tr>

                          @endforeach


                        @endforeach
                    @endforeach
                  </tbody>
                </table>


            </div> <!-- /.tab-pane --> 
            </div>
         
         <div class = "modal-footer">
            <button type = "button" class = "btn btn-default" data-dismiss = "modal">
               Close
            </button>
         </div>
      </div><!-- /.modal-content -->
   </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
</td>

<script type="text/javascript">
  
setTimeout(function(){
  $("#mtable{{ $bus_id }}").dataTable ( {
        dom: 'Bfrtip',
        "ordering": false,
        buttons: [
            'excel', 'pdf', 'print'
        ]
    })
}, 5000);

</script>
