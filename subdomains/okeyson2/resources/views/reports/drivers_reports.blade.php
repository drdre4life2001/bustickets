@extends('layouts.master')

@section('content')

<div class="content-header">
        <h2 class="content-header-title">Drivers Reports</h2>
        <ol class="breadcrumb">
          <li><a href="./index.html">Dashboard</a></li>
          <li class="active">Reports</li>
        </ol>
      </div> <!-- /.content-header -->

      

      <div class="row">

       

        <div class="col-md-12 col-sm-8">

          <div class="tab-content stacked-content">

            <div class="tab-pane fade in active" id="profile-tab">
              
              
              <h4 class="heading-inline pull-left">
          &nbsp;&nbsp;<small>Choose Driver</small>
          &nbsp;&nbsp;</h4>
              <div class="btn-group pull-left">
                  <div class="form-group">
                    <select id="select-input" style="width:300px;" class="form-control select22" onchange="refreshReport();">
                      <option value="ALL" @if(empty($driver_id)) selected="selected" @endif > -ALL- </option>
                      @foreach($drivers as $key => $value)
                        <option value="{{ $key }}" @if($key == $driver_id) selected="selected" @endif>{{ $value }}</option>
                      @endforeach
                    </select>
              </div>
              </div>

              <h4 class="heading-inline pull-left">
          &nbsp;&nbsp;<small>Choose Park</small>
          &nbsp;&nbsp;</h4>
              <div class="btn-group pull-left">
                  <div class="form-group">
                    <select id="park-input" class="form-control" onchange="refreshReport();">
                      <option value="0" @if(empty($park_id)) selected="selected" @endif > -ALL- </option>
                      @foreach($parks as $key => $value)
                        <option value="{{ $key }}" @if($park_id == $key) selected="selected" @endif>{{ $value }}</option>
                      @endforeach
                      
                    </select>
                  </div>

                  
              </div>



              <div class="btn-group pull-right">
                  <div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
                      <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
                      <span></span> <b class="caret"></b>
                  </div>
              </div>
          <h4 class="heading-inline pull-right">
          &nbsp;&nbsp;<small>Choose date range</small>
          &nbsp;&nbsp;</h4>



              <hr />

              <br /><br />

              <h3 class="">{{ (isset($driver))?$driver:"All Drivers" }} Report</h3>
              <br/>
              <div class="well">
                <h4><span class="text-primary">Summary</span></h4>
                
                  <p>
                    <span class="text-primary">Total Driver Expense: </span>&#8358;<strong  id="totalExp">0</strong> | 
                    <span class="text-primary">Net Revenue: </span>&#8358;<strong id="netRev">0</strong> | 
                    <span class="text-primary">Total Transited Trips: </span> <strong id="totalTrips">0</strong>
                    
                  </p>

              </div>
              <input type="hidden" name="data-park-id" id="data-park-id" value="{{$park_id}}">

              <div class="table-responsive" >

              <table id="reportTable" 
                class="table table-striped table-bordered table-hover table-highlight table-checkable" 
                data-provide="datatable1" 
                data-display-rows="10"
                data-info="true"
                data-search="true"
                data-length-change="true"
                data-paginate="true"
              >
                  <thead>
                    
                    <tr>
                      
                      <th>Driver</th>
                      <th>Trips</th>
                      <th>Total passengers</th>
                      <th>Total Revenue</th>
                      <th>All Expenses</th>
                      <th>Net Revenue</th>
                      <th>&nbsp;</th>
                      
                    </tr>
                  </thead>
                  <tbody>
                    {{-- */$x=0; $tf =0; $de=0; $fuel=0; $oe = 0; /* --}}
                    @foreach($dailytrips as $item)
                        <?php if(empty($item->driver)) continue; ?>

                        {{-- */  
                          

                        /* --}}
                        <tr  id="trips_num{{ $item->driver_id }}" class="loadBusInfo" data-driver-id="{{ $item->driver_id }}" data-driver-name="{{ $item->driver->name }}" >
                            <td>
                              {{ (!empty($item->driver))?$item->driver->name:"" }}
                            </td>
                            
                            <td colspan=7 >
                              loading....
                              
                            </td>
                            
                        </tr>
                    @endforeach
                        
                  </tbody>
                </table>


            </div> <!-- /.tab-pane --> 


            <!-- setting totals on top -->
                <script type="text/javascript">

                  $(function() {
                    // $("#totalRev").html("&#8358;{{ number_format($tf, 2) }}");
                    // $("#totalExp").html("&#8358;{{ number_format(($de), 2) }}");
                    // $("#netRev").html("&#8358;{{ number_format(($tf - $de- $fuel - $oe), 2) }}");
                    // $("#totalTrips").html("{{ number_format(count($dailytrips)) }}");
                  });
                </script>


                <!-- <div class="btn-group">
                <button data-toggle="dropdown" class="btn btn-info dropdown-toggle" type="button">
                  <i class="fa fa-download"></i> Download <span class="caret"></span>
                </button>
                <ul role="menu" class="dropdown-menu">
                  <li><a href="javascript:;">Download as PDF</a></li>
                  <li><a href="javascript:;">Download as Excel</a></li>
                  
                </ul>
              </div> -->

            </div>

        </div> <!-- /.col -->

      </div> <!-- /.row -->


<script type="text/javascript">

function updateUrl(name, value){
            window.location.search = jQuery.query.set(name, value);

            return false;
       } 

function refreshReport(){

  console.log($('#select-input').val());
  if($('#select-input').val() == "ALL")
    window.location = '{{ url("drivers-reports") }}';
  else if($('#park-input').val() == "ALL")
    window.location = '{{ url("drivers-reports") }}/'+$('#select-input').val();
  else
    window.location = '{{ url("drivers-reports") }}/'+$('#select-input').val()+'/'+$('#park-input').val();

}

// function refreshReport2(){

//   if($('#park-input').val() != "ALL")
//     window.location = '{{ url("drivers-reports/1/".'') }}'+$('#park-input').val();
// }


$(function() {

    function cb(start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    }
    cb(moment("{{ $start_date }}"), moment("{{ $end_date }}"));

    $('#reportrange').daterangepicker({
        ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cb);

    $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
        console.log(picker.startDate.format('YYYY-MM-DD'));
        console.log(picker.endDate.format('YYYY-MM-DD'));
        
        updateUrl('daterange', picker.startDate.format('YYYY-MM-DD')+' '+picker.endDate.format('YYYY-MM-DD'));
    });

     $( ".loadBusInfo" ).each(function( index, element ) {
         var driver_id = $(element).attr('data-driver-id');
         var driver_name = $(element).attr('data-driver-name');
         var park_id = $('#data-park-id').val();
         //alert(park_id);
         // $( element ).html( '<span style="font-style:italic;font-size:80%;"><img width="15px" src="{{ asset("desktop/img/ajax-loader-fb.gif") }}">');
         $.ajax
         (
             {
                type: "POST",
                url: "{{ url('get-drivers-trips') }}",
                data: ({ rnd : Math.random() * 100000, park_id: park_id, driver_id: driver_id, driver_name: driver_name,start_date: '{{ $start_date }}' , end_date: '{{ $end_date }}', _token: '{{ csrf_token() }}' }),
                success: function(html)
                {
                  
                     $( element ).html(html);
                     $(element).removeClass('loadBusInfo');

                }
          });

     });

});


setTimeout(function(){
  $("#reportTable").dataTable ( {
        dom: 'Bfrtip',
        "ordering": false,
        buttons: [
            'excel', 'pdf', 'print'
        ]
    })
}, 5000);

</script>




 @endsection     