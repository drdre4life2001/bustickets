@extends('layouts.master')

@section('content')

<div class="content-header">
        <h2 class="content-header-title">Reports</h2>
        <ol class="breadcrumb">
          <li><a href="./index.html">Dashboard</a></li>
          <li class="active">Reports</li>
        </ol>
      </div> <!-- /.content-header -->

      

      <div class="row">

        <div class="col-md-12 col-sm-8">

          <div class="tab-content stacked-content">

            <div class="tab-pane fade in active" id="profile-tab">
              
              <h3 class="">Incident Report</h3>
              <br/>

              <h4 class="heading-inline pull-left">
          &nbsp;&nbsp;<small>Choose type</small>
          &nbsp;&nbsp;</h4>
              <div class="btn-group pull-left">
                  <div class="form-group">
                    <select id="select-input" class="form-control" onchange="refreshReport();">
                      <option value="0" @if(empty($type_id)) selected="selected" @endif > -ALL- </option>
                      @foreach($types as $key => $value)
                        <option value="{{ $key }}" @if($type_id == $key) selected="selected" @endif>{{ $value }}</option>
                      @endforeach
                      
                    </select>
                  </div>


              </div>

              <h4 class="heading-inline pull-left">
          &nbsp;&nbsp;<small>Choose Bus</small>
          &nbsp;&nbsp;</h4>
              <div class="btn-group pull-left">
                  <div class="form-group">
                    <select id="bus-input" class="form-control" onchange="refreshReport();">
                      <option value="0" @if(empty($bus_id)) selected="selected" @endif > -ALL- </option>
                      @foreach($buses as $key => $value)
                        <option value="{{ $key }}" @if($bus_id == $key) selected="selected" @endif>{{ $value }}</option>
                      @endforeach
                      
                    </select>
                  </div>

                  
              </div>

              <h4 class="heading-inline pull-left">
          &nbsp;&nbsp;<small>Choose Park</small>
          &nbsp;&nbsp;</h4>
              <div class="btn-group pull-left">
                  <div class="form-group">
                    <select id="park-input" class="form-control" onchange="refreshReport();">
                      <option value="0" @if(empty($park_id)) selected="selected" @endif > -ALL- </option>
                      @foreach($parks as $key => $value)
                        <option value="{{ $key }}" @if($park_id == $key) selected="selected" @endif>{{ $value }}</option>
                      @endforeach
                      
                    </select>
                  </div>

                  
              </div>

              <div class="btn-group pull-right">
                  <div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
                      <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
                      <span></span> <b class="caret"></b>
                  </div>
              </div>
          <h4 class="heading-inline pull-right">
          &nbsp;&nbsp;<small>Choose date range</small>
          &nbsp;&nbsp;</h4>



              <hr />

              <br /><br />
              <div class="well">
                <h4><span class="text-primary">Summary</span></h4>
                
                  <p>
                    <span class="text-primary">Total Incidents: </span> <strong id="totalPending">{{ count($incidents) }}</strong> | 
                    <span class="text-primary">Total Expenses: </span> <strong id="totalExp"></strong>
                    
                    
                    
                  </p>

              </div>

              <div class="table-responsive">

              <table 
                class="table table-striped table-bordered table-hover table-highlight table-checkable" 
                data-provide="datatable" 
                data-display-rows="10"
                data-info="true"
                data-search="true"
                data-length-change="true"
                data-paginate="true"
              >
                  <thead>
                    <tr>
                      <th>Incident date</th>
                      <th>Incident type</th>
                      <th>Park</th>
                      <th>Bus</th>
                      <th>Details</th>
                      <th>Expense Amount</th>
                      <!-- <th>Actions</th> -->
                    </tr>
                  </thead>
                  <tbody>
                    {{-- */$x=$pend = $pendAmount = $paid = $paidAmount = $cancel = $cancelAmount =  0;  /* --}}
                    @foreach($incidents as $incident)

                        {{-- */
                          $x += $incident->expense_amount;
                          

                        /* --}}
                        <tr>
                            <td>{{ date('M dS', strtotime($incident->created_at)) }}</td>
                            <td> @if(!empty($incident->incidentType)) {{ $incident->incidentType->name }} @endif</td>
                            <td> @if(!empty($incident->park)) {{ $incident->park->name }} @endif</td>
                            <td> {{ (empty($incident->bus))?'None':$incident->bus->bus_number }}</td>
                            <td>{{ $incident->details }}</td>
                            <td>&#8358;{{ number_format($incident->expense_amount) }}</td>
                            
                            
                        </tr>
                    @endforeach
                  </tbody>
                </table>


            </div> <!-- /.tab-pane --> 

                <!-- setting totals on top -->
                <script type="text/javascript">

                  $(function() {
                    $("#totalExp").html(" &#8358;{{ number_format($x, 2) }}");
                    // $("#totalCancelled").html("{{ number_format($cancel) }} trip(s) : &#8358;{{ number_format($cancelAmount, 2) }}");
                    
                    
                  });
                </script>


            </div>

        </div> <!-- /.col -->

      </div> <!-- /.row -->


<script type="text/javascript">

function updateUrl(name, value){
            window.location.search = jQuery.query.set(name, value);

            return false;
       } 

function refreshReport(){

  console.log($('#select-input').val());
  window.location = '{{ route("incident-reports") }}/'+$('#select-input').val()+'/'+$('#bus-input').val()+'/'+$('#park-input').val();


}


$(function() {

    function cb(start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    }
    cb(moment("{{ $start_date }}"), moment("{{ $end_date }}"));

    $('#reportrange').daterangepicker({
        ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cb);

    $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
        console.log(picker.startDate.format('YYYY-MM-DD'));
        console.log(picker.endDate.format('YYYY-MM-DD'));
        
        updateUrl('daterange', picker.startDate.format('YYYY-MM-DD')+' '+picker.endDate.format('YYYY-MM-DD'));
    });

});
</script>




 @endsection     