<td>
  {{ number_format($total_pass_count) }}
</td>
<td>
  {{  $name }}
</td>
<td>
  {{ number_format($pass_count_b) }}
</td>
<td>
  {{ number_format( $pass_count - $pass_count_b ) }}
</td>
<td>
  &#8358;{{ number_format($pass_amount) }}
</td>
<td>{{ $park_name }}</td>
<td>{{ $dest_name }}</td>
<td>
  {{-- */$x=0;/* --}}
  <div class = "modal fade" id="modal{{ str_ireplace('.','',$name) }}" tabindex = "-1" role = "dialog" aria-labelledby = "myModalLabel" aria-hidden = "true">
   <div class = "modal-dialog" style="width:1000px;">
      <div class = "modal-content">
         <div class = "modal-header">
            <button type = "button" class = "close" data-dismiss = "modal" aria-hidden = "true">
                  &times;
            </button>
            <h4 class = "modal-title" id = "myModalLabel">
               Trips for {{ $name }} Payments between {{ date('D, M d, Y',strtotime($start_date)) }} and {{ date('D, M d, Y',strtotime($end_date)) }}  :: {{ count($bookings) }} Trip(s)
            </h4>
         </div>

         <div class="modal-body">

         <div class="table-responsive">

              <table 
                class="table table-striped table-bordered table-hover table-highlight table-checkable" 
                data-provide="datatable" 
                data-display-rows="10"
                data-info="true"
                data-search="true"
                data-length-change="true"
                data-paginate="true"

              >
                  <thead>
                    <tr>
                      <th>Booking code</th>
                      <th>From</th>
                      <th>To</th>
                      <th>Date</th>
                      <th>Number of passengers</th>
                      <th>Final Cost</th>
                      <th>Status</th>
                      <th>Paid By</th>
                      <th>Receipt No</th>
                      <th>Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    
                    @foreach($bookings as $booking)

                        {{-- */ $x += $booking->final_cost  ;/* --}}
                        <tr>
                            <td>{{ $booking->booking_code }}</td>
                            <td>{{ $booking->trip->sourcepark->name }}</td>
                            <td>{{ $booking->trip->destpark->name }}</td>
                            <td>{{ date('M dS', strtotime($booking->date)) }}</td>
                            <td>{{ number_format($booking->passenger_count) }}</td>
                            <td>&#8358;{{ number_format($booking->final_cost) }}</td>
                            <td>{{ $booking->status}} </td>
                            <td>{{ $booking->paymentmthd->name }} </td>
                            <td>{{ $booking->receipt_no }} </td>
                            <td>
                                <a href="{{ url('bookings/' . $booking->id ) }}" target="_blank" title="View">
                                    <button type="submit" class="btn btn-primary"><i class="fa fa-eye"></i></button>
                                </a> 

                               
                            </td>
                        </tr>
                    @endforeach

                  </tbody>
                </table>


            </div> <!-- /.tab-pane --> 
            </div>
         
         <div class = "modal-footer">
            <button type = "button" class = "btn btn-default" data-dismiss = "modal">
               Close
            </button>
         </div>
      </div><!-- /.modal-content -->
   </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

  <a type="button" href="#" class=" " data-toggle="modal" data-target="#modal{{ str_ireplace('.','',$name) }}">View Details</a>
</td>

<script type="text/javascript">

  $(function() {
     
    var tTrips = parseInt($("#total{{ $name }}").html()) + parseInt({{ count($bookings) }});
    $("#total{{ $name }}").html(tTrips);

    var tTrips = parseInt($("#pass{{ $name }}").html()) + parseInt({{ $pass_count }});
    $("#pass{{ $name }}").html(tTrips);

    
    

  });
</script>





