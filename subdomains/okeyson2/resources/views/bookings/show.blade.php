@extends('layouts.master')

@section('content')

<div class="container">
    <h1>Booking</h1>

    <ol class="breadcrumb">
          <li><a href="{{ url('/') }}">Dashboard</a></li>
          <li class="active">{{ $page_title }} </li>
          <li><a  href="{{ url('daily-trips') }}">Daily Trips</a></li>
          <li><a  href="{{ url('bookings/status/PAID') }}">Paid Bookings</a></li>
          <li><a  href="{{ url('quick-book') }}">Quick Book</a></li>
          <li><a href="{{ url('bookings') }}">Bookings </a></li>
          <li><a  href="{{ url('trips/search') }}">Book another Ticket</a></li>
          
        </ol>

<br><br>

<div class="alert alert-success hide" id="paidAlertDiv">
  Booking has been marked as PAID. <!--You can now print the Ticket-->
</div>
@if ($booking['is_round_trip'] == 0 )
       <input type ="hidden" id="rt" value="{{'0'}}">
@else
      <input type ="hidden" id="rt" value="{{'1'}}">
@endif
<div class="col-md-8 col-sm-8">
    
    <div class="portlet">

            <div class="portlet-header">

              <h1>
                <i class="fa fa-bus"></i>
                <span>
                Booking Code: <span id="bookingcode">{{ $booking['booking_code'] }}</span>
                </span>
              </h1>

            </div> <!-- /.portlet-header -->
            
            <div class="portlet-content" class="col-sm-12">
             <div class="clearfix"></div>
              
              <div class="row">

                <div class="col-md-4" id= "dateDiv">
                <dl>
                <!-- change made by laolu -->
                <dt>Booking Date</dt>
               
                <dd name="bookingdate"> {{ date('D, d/m/Y h:iA', strtotime($booking['created_at'])) }} </dd>
                <!-- change made by laolu -->

          
                <dt>Departure Date</dt>
                <dd name="departureDate"> {{ date('D, d/m/Y', strtotime($booking['date'])) }} </dd>
                
                @if($booking['is_round_trip'] == 1)
                  <dt>Return Date</dt>
                  <dd name="returnDate"> {{ date('D, d/m/Y', strtotime($booking['return_date']))  }} </dd>
                @endif

                <dt>STATUS</dt>
                <dd><span id='statusField' name="statusField" class="label label-warning">{{ $booking['status'] }}</span> </dd>
                <br/><br/>
                </dl>
                </div>

                 <div class="col-md-4" id="costDiv">
                 <dl>
                <dt>Num of Passengers </dt>
                <dd id="nop" name="nop">{{ $booking['passenger_count'] }}</dd>

                <dt>Unit Cost</dt>
                <dd>&#8358;{{ number_format($booking['unit_cost']) }}</dd>
                

                

                @if(!empty($booking['luggage_cost']))
                <dt>Luggage Cost</dt>
                <dd name="luggcost">&#8358;{{ number_format($booking['luggage_cost']) }}</dd>
                @endif


                <dt>Final Cost</dt>
                <dd name="finalcost">&#8358;{{ number_format($booking['final_cost']) }} @if($booking['is_flexi']) <strong>- Flexi booking</strong> @endif</dd>

                 @if(isset($booking['luggage_weight']))
                <dt>Luggage Weight</dt>
                <dd name="luggage_weight"> {{ $booking['luggage_weight'] }}kg</dd>
                @endif

                @if($booking['status'] == 'PAID')
                <dt>Payment Date</dt>
                <dd name="paymentdate"> <?php echo date('D d/m/Y h:iA', strtotime($booking['paid_date'])) ?></dd>
                @endif
                </dl>
                </div>
               
                
                <div class="col-md-4" id="tripDiv">
                <h4>Trip Details</h4>
                <dl>
                <dt>From:</dt>
                <dd name="from">{{ $booking['trip']['sourcepark']['name'] }}</dd>

                <dt>To:</dt>
                <dd name="to">{{ $booking['trip']['destpark']['name'] }}</dd>

                @if($booking['trip']['is_intl_trip'] == 1)

                <dt>Passport type:</dt>
                <dd>{{ $booking['passport_type'] }}</dd>
                @endif
                
                 @if($booking['trip']['parenttrip']['trip_transit'] != "")
                 <dt>Trip Transit:</dt>
                <dd>{{ $booking['trip']['parenttrip']['trip_transit'] }}</dd>
                @endif

                <dt>Trip Type:</dt>
                <dd>{{ ($booking['is_round_trip'] )?"Round Trip":"One Way" }}</dd>
                   

                <dt>Bus type</dt>
                <dd name="bustype">{{ $booking['trip']['parenttrip']['bus']['bus_type']['name'] }}</dd>

                <dt>Number Plate</dt>
                <dd name="busnumber">{{ $booking['daily_trip']['bus']['number_plate'] }}</dd>

                <dt>Bus Features</dt>
                <dd>
                  
                  @if($booking['trip']['parenttrip']['ac'])
                                                    <span<i class="fa fa-check-square-o font-green-jungle"></i>  AC</span>
                                                    @else
                                                    <span><i class="fa fa-times-circle font-red-thunderbird"></i>  AC</span>
                                                    @endif
                                                    
                                                    &nbsp;&middot;&nbsp;

                                                   <!--  @if($booking['trip']['parenttrip']['security'])
                                                        <span><i class="fa fa-check-square-o font-green-jungle"></i>  Security</span>
                                                    @else
                                                    <span ><i class="fa fa-times-circle font-red-thunderbird"></i>  Security</span>
                                                    @endif
                                                    
                                                    &nbsp;&middot;&nbsp;
                
                                                    @if($booking['trip']['parenttrip']['insurance'])
                                                    <span ><i class="fa fa-check-square-o font-green-jungle"></i>  Insurance</span>
                                                    @else
                                                    <span><i class="fa fa-times-circle font-red-thunderbird"></i>  Insurance</span>
                                                    @endif
                
                                                    &nbsp;&middot;&nbsp;
                            
                                                    <br/>
                                                    @if($booking['trip']['parenttrip']['tv'])
                                                    <span><i class="fa fa-check-square-o font-green-jungle"></i>  TV</span>
                                                    @else
                                                    <span><i class="fa fa-times-circle font-red-thunderbird"></i>  TV</span>
                                                    @endif
                                                    
                                                    &nbsp;&middot;&nbsp;

                                                    @if($booking['trip']['parenttrip']['passport'])
                                                    <span><i class="fa fa-check-square-o font-green-jungle"></i>  Passport</span>
                                                    @else
                                                    <span><i class="fa fa-times-circle font-red-thunderbird"></i>  Passport</span>
                                                    @endif -->
                </dd>
                </span<i></dd></dl></div></div>

                <hr>

                <div class="col-md-6">

                <h4>Passenger Details</h4>
                
                <dt>Passenger Name</dt>
                <dd name="contactname">{{ $booking['contact_name'] }}</dd>

                <dt>Passenger Phone</dt>
                <dd name="phone">{{ $booking['contact_phone'] }}</dd>


                @if(isset($booking['contact_email']))
                <dt>Passenger Email</dt>
                <dd name="email">{{ $booking['contact_email'] }}</dd>                
                @endif


                @if(isset($booking['contact_address']))
                <dt>Passenger Address</dt>
                <dd name="contactaddress">{{ $booking['contact_address'] }}</dd>                
                @endif
                
                <dt>Gender</dt>
                <dd name="gender">{{ $booking['gender'] }}</dd>
                
                <!-- <dt>Next of Kin Name</dt>
                <dd name="nextofkin">{{ $booking['next_of_kin'] }}</dd>
 -->
                <dt>Next of Kin Phone</dt>
                <dd name="nextofkinphone">{{ $booking['next_of_kin_phone'] }}</dd>

               <dt>Other Passsenger(s):</dt>
                 @for($i=0; $i<count($booking['passengers']); $i++)

                 <!-- Check if passenger age is within free range -->
                 @if(!empty($free_age_range))
                   @if($booking['passengers'][$i]['age'] >= intval($free_age_range->age_one) && $booking['passengers'][$i]['age'] <= intval($free_age_range->age_two))
                    <dd> {{ $booking['passengers'][$i]['name']. ' | '.$booking['passengers'][$i]['gender'] }} (Infant)</dd>
                   @else
                    <dd> {{ $booking['passengers'][$i]['name']. ' | '.$booking['passengers'][$i]['gender'] }}</dd> 
                   @endif
                  @else
                   <dd> {{ $booking['passengers'][$i]['name']. ' | '.$booking['passengers'][$i]['gender'] }}</dd>
                @endif
                 @endfor


                </div>
              <div class="col-md-6"> 

                  @if(!empty($booking['extra_luggages']))  

                <h4>Extra Luggages</h4>

                <dl>

                @foreach($booking['extra_luggages'] as $e)

                <dt>Added on {{ date('D, d/m/Y h:iA', strtotime($e['created_at'])) }}</dt>
                <dd name="gender">{{ $e['luggage_weight'].'KG  ==> &#8358;'.number_format($e['amount']) }}</dd>

                @endforeach

                </dl>

              @endif  

              </div>

              <br style="clear:both;" />


              <hr/>

              <input type="hidden" name="bookedSeats" value="{{$booked_seats}}" id="bookedSeats"/>
              <input type="hidden" name="no_of_seats" value="{{$booking['trip']['parenttrip']['bus']['no_of_seats']}}" id="no_of_seats"/>
            
              @if(isset($booking['status']) && $booking['status'] != 'CANCELLED')
              <div id="bookedSeats">
              <h4>Chosen seat(s):</h4>  <span id="bkdSeats">Seat {{ $booked_seats }}</span>
              </div>
              @endif
              
            <div id=""  class="col-sm-9">
                   <ul id="bus-seat"></ul>
            </div>
            
            </div> <!-- /.portlet-content -->

          </div>
</div>









<div class="col-md-3 col-sm-3">

    
  @if($booking['status'] == 'PENDING')
    <div class="portlet">
        <div class="portlet-header">
          <div id="updateStatusDiv"></div>
              <h3>
                <i class="fa fa-reorder"></i>
                Mark as Paid
              </h3>
            </div> <!-- /.portlet-header -->

            <div class="portlet-content">
            
          <!-- AJAX FORM -->
            <form action="{{ route('update-status') }}" id="updateStatus" method="post">

              <input type="hidden" name="booking_id" value="{{ $booking['id'] }}">
              <div class="form-group {{ $errors->has('payment_method_id') ? 'has-error' : ''}}">
                {!! Form::label('payment_method_id', 'Payment Method: ', ['class' => 'col-sm-12 control-label']) !!}
                <div class="col-sm-12">
                    {!! Form::select('payment_method_id', $payment_methods,null, ['class' => 'form-control','required'=>'required']) !!}
                {!! $errors->first('payment_method_id', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>
              <div class="form-group {{ $errors->has('receiptNO') ? 'has-error' : ''}}" id="receipt">
              {!! Form::label('receiptNo', 'ReceiptNo: ', ['class' => 'col-sm-12 control-label']) !!}
              <div class="col-sm-12">
              <input type="text" id="receiptNo" name="receiptNo" class="form-control" maxlength="50">
              {!! $errors->first('receiptNo', '<span class="parsley-error-list">:message</span>') !!}
              </div>
              </div>

            <br style="clear: both;" /><br style="clear: both;" />
              
              <input type="hidden" id="SubBtn" name="_token" value="<?php echo csrf_token(); ?>">



              <input type="submit" class="btn btn-primary" value="Mark as PAID" id="btnSub">
            </form>
            </div>
    </div>
    @endif


     
     @if($booking['status'] == 'PENDING')
     <div class="portlet" id="cancelDiv">
     <div id="updateStatusDiv"></div>
        <div class="portlet-content">
        <input type="submit" class="btn btn-primary" value="Book on Hold" id="btnBookHold">
        </div>
    </div>
    @endif
    <div class="portlet">
     <div id="response"></div>
        <div class="portlet-content">
        <div id="updateStatusDiv"></div>
        <input type="submit" class="btn btn-primary" value="Send SMS" id="btnResend">
        </div>
    </div>
    <div class="portlet" id="cancelDiv">
    
        <div class="portlet-content">
            @if($booking['status'] == 'PAID')
            <a href="{{  url('ticket/'.$booking['id']) }}" target="_blank" class="btn btn-block btn-success">
              <i class="fa fa-print"></i>
              Print Ticket
            </a>
             <a href="{{ route('cancel-booking', $booking['booking_code']) }}" class="btn btn-block btn-primary">
              Cancel Booking
            </a>
            @elseif($booking['status'] != 'CANCELLED')
            <a href="{{ route('cancel-booking', $booking['booking_code']) }}" class="btn btn-block btn-primary" id="cancelBtn">
              Cancel Booking
            </a>
            @endif
            
        </div>

    </div>

        <div class="portlet">

                <div class="portlet-header">
                  <h3>
                    <i class="fa fa-reorder"></i>
                    Actions
                  </h3>
                </div> <!-- /.portlet-header -->

                <div class="portlet-content">
                  <!-- <a href="#" class="btn btn-danger">ADD Booking Notes</a> -->
                   <button type="button" class="btn btn-default btn-block " data-toggle="modal" data-target="#lugggageModal"> Add Extra Luggage </button>

                   <a type="button" class="btn btn-primary btn-block" href="{{ url('bookings/' . $booking['id'] . '/edit') }}"> Edit passenger details </a>

                  <button type="button" class="btn btn-default btn-block " data-toggle="modal" data-target="#myModal"> Add Booking Notes </button>
                </div> <!-- /.portlet-content -->

        </div>


</div>
    
    <div id="showNote">
         <div class="col-md-6 col-sm-8">
          <div class="portlet">

            <div class="portlet-header">

              <h3>
                <i class="fa fa-reorder"></i>
                Notes attached to this Booking
              </h3>

            </div> <!-- /.portlet-header -->

            <div class="portlet-content">           


              <div class="table-responsive">

              <table 
                class="table table-striped table-bordered table-hover table-highlight table-checkable" 
                data-provide="datatable" 
                data-display-rows="10"
                data-info="true"
                data-search="true"
                data-length-change="true"
                data-paginate="true"
              >
                  <thead>
                    <tr>
                      <th>S.No</th><th>User_id</th><th>Notes</th><th>Date</th>
                    </tr>
                  </thead>
                  <tbody>
                    {{-- */$x=0;/* --}}
                    @foreach($notes as $item)
                        {{-- */$x++;/* --}}
                        <tr>
                            <td>{{ $x }}</td>
                            <td>{{ $item->user->first_name }}</td>
                            <td>{{ $item->notes }}</td>
                            <td>{{ $item->created_at }}</td>                      
                        </tr>
                    @endforeach
                  </tbody>
                </table>
              </div> <!-- /.table-responsive -->
            </div> <!-- /.portlet-content -->

          </div> <!-- /.portlet -->
</div> <!-- /.col -->
    </div>
<div class="col-md-5 col-sm-8">
          <div class="portlet">
            <div class="portlet-header">
              <h3>
                <i class="fa fa-reorder"></i>
                Online Transactions Attempts
              </h3>
            </div> <!-- /.portlet-header -->
            <div class="portlet-content">           
              <div class="table-responsive">
              <table 
                class="table table-striped table-bordered table-hover table-highlight table-checkable" 
                data-provide="datatable" 
                data-display-rows="10"
                data-info="true"
                data-search="true"
                data-length-change="true"
                data-paginate="true"
              >
                  <thead>
                    <tr>
                      <th>S.No</th><th>Status</th><th>Response</th><th>Date</th>
                    </tr>
                  </thead>
                  <tbody>
                    {{-- */$x=0;/* --}}
                   
                  </tbody>
                </table>
              </div> <!-- /.table-responsive -->
              

            </div> <!-- /.portlet-content -->

          </div> <!-- /.portlet -->
    </div> <!-- /.col -->
</div>

<div class = "modal fade" id="myModal" tabindex = "-1" role = "dialog" aria-labelledby = "myModalLabel" aria-hidden = "true">
   <div class = "modal-dialog">
      <div class = "modal-content">
         <div class = "modal-header">
            <button type = "button" class = "close" data-dismiss = "modal" aria-hidden = "true">
                  &times;
            </button>
            <h4 class = "modal-title" id = "myModalLabel">
               Add Booking note
            </h4>
         </div>
         <form action="{{ route('booking-note') }}" method="post" id="bookingNotes">
         <div class = "modal-body" id="contentArea">
              <input type="hidden" id="SubBtn" name="_token" value="<?php echo csrf_token(); ?>">
          <input type="hidden" name="booking_id" value="{{ $booking['id'] }}">
            Notes: <textarea rows="7" cols="50" name="notes" class="form-control"></textarea>
         </div>
         <div class = "modal-footer">
            <button type = "button" class = "btn btn-default" data-dismiss = "modal">
               Close
            </button>
            <button type = "submit" id="NoteSub" class = "btn btn-primary">
               Submit changes
            </button>
         </form>

         </div>
      </div><!-- /.modal-content -->
   </div><!-- /.modal-dialog -->
</div><!-- /.modal -->




<div class = "modal fade" id="lugggageModal" tabindex = "-1" role = "dialog" aria-labelledby = "myModalLabel" aria-hidden = "true">
   <div class = "modal-dialog">
      <div class = "modal-content">
         <div class = "modal-header">
            <button type = "button" class = "close" data-dismiss = "modal" aria-hidden = "true">
                  &times;
            </button>
            <h4 class = "modal-title" id = "myModalLabel">
               Add Extra Luggage
            </h4>
         </div>
         <form action="{{ route('add-extra-luggage') }}" method="post" id="bookingNotes">
         <div class = "modal-body" id="contentArea">
              <input type="hidden" id="SubBtn" name="_token" value="<?php echo csrf_token(); ?>">
          
          <input type="hidden" name="booking_id" value="{{ $booking['id'] }}">
            
             <div class="form-group {{ $errors->has('luggage_weight') ? 'has-error' : ''}}">
                {!! Form::label('luggage_weight', 'Luggage Weight: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    <input type="text" name="luggage_weight" class="form-control" required />
                {!! $errors->first('luggage_weight', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>

            <br style="clear:both;" />

             <div class="form-group {{ $errors->has('amount') ? 'has-error' : ''}}">
                {!! Form::label('amount', 'Amount: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    <input type="text" name="amount" class="form-control" required />
                {!! $errors->first('amount', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>

            <br style="clear:both;" />


         </div>
         <div class = "modal-footer">
            <button type = "button" class = "btn btn-default" data-dismiss = "modal">
               Close
            </button>
            <button type = "submit" id="NoteSub" class = "btn btn-primary">
               Submit changes
            </button>
         </form>

         </div>
      </div><!-- /.modal-content -->
   </div><!-- /.modal-dialog -->
</div><!-- /.modal -->



<script type="text/javascript">

$(function () {
var bookedSeat = $('#bookedSeats').val();
    var d = [];
    var noOfSeats = $('#no_of_seats').val();
    noOfSeats = parseInt(noOfSeats);
        if(bookedSeat != ""){
             d = bookedSeat.split(',').map(function(el){ return +el;});
            //alert(d);
        }
        
        
      var formArray = [];
      

      function displayBusSeat(li, div) {
            
        for (var i = 1; i <= li; i++) {
          $('#bus-seat').append('<li class="li-seat clearfix"></li>');
        }
        for (var i = 0; i <= div; i++) {
          if (d.indexOf(i) > -1) {
                    $('.li-seat').append('<div class="forbuses"><input type="checkbox" id='+i+' disabled/><label for='+i+'><i class="fa fa-car" aria-hidden="true"></i>'+i+'</label></div>');
          }
          else if (i == 0) {
            continue;
          }else if (i == 1) {
            $('.li-seat').append('<div class="forbuses"><input type="checkbox" id="2" /><label for="2"><i class="fa fa-car" aria-hidden="true"></i>2</label></div>');
          }else if (i == 2) {
            $('.li-seat').append('<div class="forbuses"><input type="checkbox" id="1"/><label for="1"><i class="fa fa-car" aria-hidden="true"></i>1</label></div>');
          }
          else{
            $('.li-seat').append('<div class="forbuses"><input type="checkbox" id='+i+' /><label for='+i+'><i class="fa fa-car" aria-hidden="true"></i>'+i+'</label></div>');
          }
        }

      }

        displayBusSeat(1, noOfSeats);
      $('input[type="checkbox"]').change(function() {
          var newValue = parseInt($(this).attr('id'));
            if (this.checked) {
                formArray.push(newValue);
                console.log(formArray);
            }else {
                    var index = $.inArray(newValue, formArray);
                    formArray.splice(index, 1);
                    //formArray = ary.filter(function(e) { return e !== newValue })
              console.log(formArray);
            }
                $("#selSeats").val(formArray.join(','));
                  checkChosenSeats(formArray);
        });

            function checkChosenSeats(str){
                var passenger_count = $("#passenger_count").val();
                if(str.length == passenger_count){
                  $('#submitBtn').removeClass('disabled');
                  console.log('equal');
                }else{
                   $('#submitBtn').addClass('disabled');
                   console.log('not equal');
                }
            }

});



</script>


<script>

// payment method wahala

$(document).ready(function(){
 $("#receipt").hide();

 var rt = $("#rt").val();

 var landline ="+2348074490452";
 
      var bookingcode = $("#bookingcode").html();
      var busNo = "{{ $booking['daily_trip']['bus']['bus_number'] }}"
      var phone = $("dd[name=phone]").text();
      var fromArr = $("dd[name=from]").text().split("[");
      var from = fromArr[0];
      var toArr = $("dd[name=to]").text().split("[");
      var to = toArr[0];
      var bookingdate = $("dd[name=bookingdate]").text();
      var departureDate = $("dd[name=departureDate]").text();
      var returnDate = $("dd[name=returnDate]").text();
      var status = $("span[name=statusField]").html();
      var finalcost = $("dd[name=finalcost]").text();
      finalcost = finalcost.substr(1);
      var bustype = $("dd[name=bustype]").text();
      var contactname = $("dd[name=contactname]").text();
      var bookedSeats = $("#bkdSeats").html();

      var gender = $("dd[name=gender]").text().charAt(0).toLowerCase();
      var op = "{{ $op }}";
      var title="";
      var nop = $("dd[name=nop]").text();
      if(gender == 'm')
        title = "Sir";
      else 
        title = "Ma";
  
 // $("#btnSub").on('click', function(){
 //   window.location.reload();
 // });

    $("#btnBookHold").on('click', function(){


       $("#updateStatusDiv").addClass('alert alert-info').html('Please wait....');

     var msg = op + " book on hold: "+" Book Code: "+bookingcode+ " from: "+" "+
               from +"to: "+ to +
                ". Name: "+contactname+
               ". Bus:"+busNo+
               ". Seat:"+bookedSeats+
               ". Passengers: "+nop+
               ". Final Cost: NGN "+ finalcost+" "+
               ". Contact: "+ landline;
             var rtbuild = "Return Date "+returnDate;
            var rtStatus = (rt == 1) ? rtbuild: " ";
            msg+=rtStatus;
    

       
         $.ajax({
                    url: '{{ route("send-sms") }}',
                    dataType: 'text',
                    type: 'GET',
                    contentType: 'application/x-www-form-urlencoded',
                    data: {"phone":phone,"msgg":msg},
                    success: function (rslts, textStatus, jQxhr)
                    {
                      if(rslts == 200)
                      {
                        $("#updateStatusDiv").addClass('alert alert-success').html('SMS Sent');
                         $("#updateStatusDiv").delay(5000).fadeOut(); 
                      }
                      else
                      {
                        $("#updateStatusDiv").addClass('alert alert-danger').html('SMS not sent');
                        $("#updateStatusDiv").delay(5000).fadeOut();
                      } 
                      window.location.reload();
                    },
                    error: function (jqXhr,textStatus, errorThrown)
                    {
                       $("#updateStatusDiv").addClass('alert alert-danger').html('SMS not sent');
                       $("#updateStatusDiv").delay(5000).fadeOut();
                    } 
              });
      });

   
    

    $("#btnResend").on('click', function(){
     
     // get status
      var status = $("#statusField").html();
      $("#updateStatusDiv").addClass('alert alert-info').html('Please wait....');
     
     if(status == "PENDING")
     { 
    
    var msg = op + " book on hold: "+" Book Code: "+bookingcode+ " from: "+" "+
               from +"to "+ to +
                ". Name: "+contactname+
               ". Bus: "+busNo+
               ". Seat:"+bookedSeats+
               ". Passengers: "+nop+
               ". Final Cost: NGN"+ finalcost+" "+
               ". Contact: "+ landline;
              var rtbuild = "Return Date "+returnDate;
            var rtStatus = (rt == 1) ? rtbuild: " ";
            msg+=rtStatus;
         $.ajax({
                    url: '{{ route("send-sms") }}',
                    dataType: 'text',
                    type: 'GET',
                    contentType: 'application/x-www-form-urlencoded',
                    data: {"phone":phone,"msgg":msg},
                    success: function (rslts, textStatus, jQxhr)
                    {
                      if(rslts == 200)
                      {
                       
                        $("#updateStatusDiv").addClass('alert alert-success').html('SMS Sent');
                         $("#updateStatusDiv").delay(5000).fadeOut(); 
                      }
                      else
                      {
                        $("#updateStatusDiv").addClass('alert alert-danger').html('SMS not sent');
                        $("#updateStatusDiv").delay(5000).fadeOut();
                      } 
                      window.location.reload();
                    },
                    error: function (jqXhr,textStatus, errorThrown)
                    {
                       $("#updateStatusDiv").addClass('alert alert-danger').html('SMS not sent');
                       $("#updateStatusDiv").delay(5000).fadeOut();
                    } 
              });
        }

        else if(status == "PAID")
        {

         var msg = op + " purchase notice : "+" Book Code: "+bookingcode+ " from: "+" "+
               from +"to "+ to +
                ". Name: "+contactname+
               ". Bus: "+busNo+
               ". Seat:"+bookedSeats+
               ". Passengers: "+nop+
               ". Final Cost: NGN"+ finalcost+" "+
               ". Contact: "+ landline;
               var rtbuild = "Return Date "+returnDate;
            var rtStatus = (rt == 1) ? rtbuild: " ";
            msg+=rtStatus;
          
         $.ajax({
                    url: '{{ route("send-sms") }}',
                    dataType: 'text',
                    type: 'GET',
                    contentType: 'application/x-www-form-urlencoded',
                    data: {"phone":phone,"msgg":msg},
                    success: function (rslts, textStatus, jQxhr)
                    {
                      if(rslts == 200)
                      {
                        $("#updateStatusDiv").addClass('alert alert-success').html('SMS Sent');
                         $("#updateStatusDiv").delay(5000).fadeOut(); 
                      }
                      else
                      {
                        $("#updateStatusDiv").addClass('alert alert-danger').html('SMS not sent');
                        $("#updateStatusDiv").delay(5000).fadeOut();
                      } 
                      window.location.reload();
                    },
                    error: function (jqXhr,textStatus, errorThrown)
                    {
                       $("#updateStatusDiv").addClass('alert alert-danger').html('SMS not sent');
                       $("#updateStatusDiv").delay(5000).fadeOut();
                    } 
              });

          // CALL mark as paid
          $('#updateStatus').ajaxForm({
                beforeSubmit: genPreSubmit,
                success: function(response){
                  
                  console.log(response);
                  $('#statusField').html('<span class="label label-success">PAID</span> ');
                  $('#flash').html('Booking Status Successfully updated')
                  $('#updateStatusDiv').hide('slow');
                  $('#cancelDiv').hide('slow');
                  $('#paidAlertDiv').removeClass('hide');
                  window.location.reload();
                },
                 reset: true
         });
        }
       });
  
    // on change
    $("#payment_method_id").on('change', function(){
      // get what is method is being used
      var paymtMthd =  $("#payment_method_id").val();
      if(paymtMthd == 3)
      {
        $("#receipt").show();
      }
      else
        $("#receipt").hide();

    });

$('#updateStatus').ajaxForm({
                beforeSubmit: genPreSubmit,
                success: function(response){
                  console.log(response);
                  $('#statusField').html('<span class="label label-success">PAID</span> ');
                  $('#flash').html('Booking Status Successfully updated')
                  $('#updateStatusDiv').hide('slow');
                  $('#cancelDiv').hide('slow');
                  $('#paidAlertDiv').removeClass('hide');
                  
                  location.reload(true);
               
                },
                 reset: true

         });

         $('#bookingNotes').ajaxForm({
                beforeSubmit: genPreSubmit,
                success: function(response){
                  console.log(response);
                  $('#myModal').modal('hide')
                  $('#showNote').html(response);
                  $('#flash').html('Note Added Successfully')

                },
                 reset: true
         }); 

      function genPreSubmit(){
        
          console.log("We are here....");
          $("#SubBtn").html('please wait...');
         

        }
      
        // send cancel sms
        $("#cancelBtn").on('click',function(){note
         // send sms
         var msg = op + " cancel notice: "+" Book Code: "+bookingcode+ " from: "+" "+
               from +"to "+ to +
                ". Name: "+contactname+
               ". Bus: "+busNo+
               ". Seat:"+bookedSeats+
               ". Passengers: "+nop+
               ". Final Cost: NGN "+finalcost+" "+
               ". Call: "+ landline;
                var rtbuild = "Return Date "+returnDate;
            var rtStatus = (rt == 1) ? rtbuild: " ";
            msg+=rtStatus;

         $.ajax({
                    url: '{{ route("send-sms") }}',
                    dataType: 'text',
                    type: 'GET',
                    contentType: 'application/x-www-form-urlencoded',
                    data: {"phone":phone,"msgg":msg},
                    success: function (rslts, textStatus, jQxhr)
                    {
                      if(rslts == 200)
                      {
                        $("#updateStatusDiv").addClass('alert alert-success').html('SMS Sent');
                         $("#updateStatusDiv").delay(5000).fadeOut(); 
                      }
                      else
                      {
                        $("#updateStatusDiv").addClass('alert alert-danger').html('SMS not sent');
                        $("#updateStatusDiv").delay(5000).fadeOut();
                      } 
                    },
                    error: function (jqXhr,textStatus, errorThrown)
                    {
                       $("#updateStatusDiv").addClass('alert alert-danger').html('SMS not sent');
                       $("#updateStatusDiv").delay(5000).fadeOut();
                    } 
              });
        });

});
</script>

  <style type="text/css">
    li {
      list-style: none;
    }
#seat-explain label {
      display: inline-block;
      position: relative;
      width: 50px;
      text-align: center;
      font-size: 14px;
      font-weight: bold;
      line-height: 1.5rem;
      padding: 10px;
      border-radius: 5px;
      animation-duration: 300ms;
      animation-fill-mode: both;
  }
#seat-explain li:first-of-type label {  
      background: steelblue;
  }
#seat-explain li:nth-of-type(2) label {  
      background: #F42536;
  }
#seat-explain li:last-of-type label {  
      background: #bada55;
  }
  </style>
@endsection