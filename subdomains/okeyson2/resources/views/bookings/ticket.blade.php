<html><head><title> {{session('operator')->name}} TICKET</title>
<link  rel="stylesheet" type="text/css" href="{{ asset('bckend/css/bootstrap.min.css')}}" >



<script>
function printDiv(divName) {
     var printContents = document.getElementById(divName).innerHTML;
     var originalContents = document.body.innerHTML;

     document.body.innerHTML = printContents;

     window.print();

     document.body.innerHTML = originalContents;
}
</script>

<style type="text/css">
    </style>

</head>
<body style="width:380px;font-size:200%;">
    <!-- <div class="container" style="margin-left:0px;"> -->
        <!-- <div class="col-sm-4 col-md-4 col-lg-3"> -->
            <div class="portlet">
                <div class="portlet-content" id ="print-area-1">
                    <left><small><img src="{{ asset('logos/'.session('operator')->img)}}" height="auto"><br>
                        <b style="font-size:150%;">{{ucwords(session('operator')->name)}}</b>
                        <h6><b  style="font-size:150%;">{{ $booking['trip']['sourcepark']['name'] }} - {{ $booking['trip']['destpark']['name'] }} (&#8358;{{ number_format($booking['trip']['fare'])  }})</b></h6>
                        <b style="font-size:100%;">PASSENGER SINGLE TICKET</b></small>
                    </left>
                    <center>
                        <table align="justify" class="table table-bordered table-condensed" style="
                        font-size: 18px;">
                            <tr>
                                <td><small><b>Booking Code:</b> {{ $booking['booking_code'] }} </small></td>
                            </tr>
                            <tr>
                                <td><small><b>Bus Number:</b> <b style="font-size: 100%">
                                  {{ $booking['daily_trip']['bus']['number_plate'] }} </b>
                                </small>
                            </td>
                        </tr>
                        @if($booking['trip']['parenttrip']['trip_transit'] !="")
               <tr>

                            <td><small><b>Bus Transit:</b>{{ $booking['trip']['parenttrip']['trip_transit']  }}</small> </td>

              </tr>
              @endif
                        <tr>
                            <td><small><b>Departure Date and Time:</b>{{ date('D, d/m/Y', strtotime($booking['date'])). ' '.$booking['trip']['parenttrip']['departure_time'] }}</small> </td>
                        </tr>
                          @if($booking['is_round_trip'] == 1)
                           <tr>
                            <td width="100%" colspan="3"><b>Return Date: </b> {{ date("Y-m-d", strtotime($booking['return_date'])) }} </td>
                            </tr>
                          @endif
                        <tr>
                            <td><small><b>Booking Date:</b> {{ date('D, d/m/Y h:iA', strtotime($booking['created_at'])) }}</small> </td>
                        </tr>

                        <tr>
                            <td><small><b>Name:</b> {{ $booking['contact_name'] }}</small></td>
                        </tr>
                        <!-- <tr>
                         <td><small><b>Other Passenger(s):</b><br>
                         @if(count($booking['passengers']) >= 1)
                         
                         @for($i=1; $i <count($booking['passengers']); $i++)
                          {{ $booking['passengers'][$i]['name'] ."|". $booking['passengers'][$i]['gender'] ."|". ($booking['passengers'][$i]['age'] > 3)? $booking['passengers'][$i]['age']:"(I)"  }} 
                         @endfor
                         
                         @else
                         {{ "None" }}
                         @endif
                        </small></td>
                    </tr> -->
                   @if($booking['trip']['is_intl_trip'] == 1)
                   <tr>
                    <td width="100%" colspan="3"><b>Passport type Date: </b> {{ $booking['passport_type'] }} </td>
                    </tr>

                   <tr>
                   @endif
                    <tr>
                      <td width="100%" colspan="3"><small><b>Luggage Quantity:</b>  
                         @if(isset($booking['luggage_weight']))
                         {{ $booking['luggage_weight'] ."kg"}}
                         @endif
                     </small></td>
                    </tr>   
                    <tr>
                      <td width="100%" colspan="3"><small><b>Luggage Cost:</b>  
                         @if(isset($booking['luggage_cost']))
                            &#8358;{{ number_format($booking['luggage_cost'])}}
                         @endif
                     </small></td>
                    </tr>   

                @if(!empty($booking['extra_luggages']))       
         <tr>
            <td><small><b>Extra Luggage(s):</b><br>

             
              @foreach($booking['extra_luggages'] as $e)

                {{ $e['luggage_weight'].'KG  ==> &#8358;'.number_format($e['amount']) }}
                <br/>

              @endforeach


            </small></td>
        </tr>
        @endif        
            <!-- <tr>
            <td><small><b>Tel:</b> {{ $booking['contact_phone'] }} </small></td>
        </tr> -->

        <tr>
            <td><small><b>Amount Paid:</b>&#8358;{{ number_format($booking['final_cost']) }}</small></td>
        </tr>
        <tr>
            <td><small><b>Amount Breakdown  @if($booking['is_flexi']) (Flexi booking) @endif:</b><br>

            {{ $booking['contact_name'] }}: &#8358;{{number_format($booking['unit_cost']) }}<br>
             
              @if( $booking['luggage_weight'] > ( $l* $booking['passenger_count']) )
             Luggage Cost: &#8358;{{  number_format($booking['luggage_weight'] - ( $l* $booking['passenger_count'] )) * $lC }}<br>
             @else
              Luggage Cost: &#8358;0<br>
             @endif 
               @if( $booking['luggage_weight'] > ( $l* $booking['passenger_count']) )
              Passenger Cost: &#8358; {{ $booking['final_cost'] -$booking['unit_cost']  -(number_format($booking['luggage_weight'] - ( $l* $booking['passenger_count'] )) * $lC ) }}
              @else
               Passenger Cost: &#8358; {{ $booking['final_cost'] -$booking['unit_cost']  - 0 }}
              @endif
             
              @if($booking['passenger_count'] > 1)

              <b><br>Passengers<br></b>
             @for($i=0; $i < count($booking['passengers']); $i++)
                          {{ $booking['passengers'][$i]['name'] ." | ". $booking['passengers'][$i]['gender']  }} 
              @if(!empty($free_age_range))
                    @if ($booking['passengers'][$i]['age'] >= intval($free_age_range->age_one) && $booking['passengers'][$i]['age'] <= intval($free_age_range->age_two) ) 
                      {{ "| ". $booking['passengers'][$i]['age'] }} yrs old <b> {{ "(Infant)" }} </b> <br>
                    @endif
              @endif
             @endfor 
             @endif
                         
             



            </small></td>
        </tr>


        <tr>
            <td><small><b>Seat No:</b> <b style="font-size: 150%">{{ $booked_seats }}</b> </small></td>
        </tr>
        <tr>
                                <td><small><b>Driver:</b> <b style="font-size: 100%">
                                  {{ $booking['daily_trip']['driver']['name'] }} </b>
                                </small>
                            </td>

        <tr>
            <td><small><b>Ticket Clerk Name: </b>{{ $booking['ticketer']['first_name'].' '.$booking['ticketer']['last_name'] }}</small></td>
            <!-- <td><b>Signature:</b></td> -->
        </tr>

        <tr align="justify">
            <td><small>
             <b>OFFICE</b><br>{{session('operator')->address}}<br>
             <b>TELEPHONE</b> <br>{{session('operator')->phone}}<br>
             <b>EMAIL</b> <br>{{session('operator')->email}}<br>
         </td>
     </tr>

      <tr>
        <td><small>
          <b>Luggage at Owner's risk</b>
      </td>
  </tr>

</table>
<p align="right" class="pull-right"></p>
</center>

</div>

</div>
<!-- </div> -->
<!-- </div> -->

<script type="text/javascript">

 

  setTimeout(function () { 
   // get no of passengers
    var noPassengers = {{ count($booking['passengers']) }}
    
  // print based on no of passengers
  for(var i=0; i < noPassengers+1; i++)
  {
    window.print();
  }
   }, 500);
  window.onfocus = function () { setTimeout(function () { window.close(); }, 500); }
</script>
</body>
</html>