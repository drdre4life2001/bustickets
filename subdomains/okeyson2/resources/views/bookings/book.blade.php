@extends('layouts.master')

@section('content')

<div class="content-header">
        <h2 class="content-header-title">Book a Trip </h2>
        <ol class="breadcrumb">
          <li><a href="{{ url('/') }}">Dashboard</a></li>
          <li><a href="{{ url('bookings/status/PAID') }}">Paid Bookings </a></li>
          <li><a  href="{{ url('daily-trips') }}">Daily Trip</a></li>
          <li><a  href="{{ url('trips/search') }}">Normal Booking</a></li>
          <!-- <li class="active"><a href ="" onclick="goBack()">Back</a> </li> -->
        </ol>
      </div> <!-- /.content-header -->





 <form class="form-horizontal" action="{{ url('transload') }}" method="POST" id="bookingForm">
         <div class = "modal-body" id="TranscontentArea">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              
            <div class="form-group {{ $errors->has('default_driver_id') ? 'has-error' : ''}}">
                {!! Form::label('dt_id', 'Choose new bus', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::select('dt_id', $buses, null, ['class' => 'form-control select22', 'required' => 'required', 'onchange'=>'loadBusDetails(this)']) !!}
                    {!! $errors->first('dt_id', '<p class="help-block">:message</p>') !!}
                </div>
            </div>

            <div id="bus-details"></div>




         </div>
         


         </form>




         <script type="text/javascript">
           
           function loadBusDetails(elem) {
              var dt_id = elem.value;
              // console.log(elem);

              $("#bus-details").html("Please wait....");

              $.ajax({
                url: '{{ route("new-book") }}',
                dataType: 'text',
                type: 'POST',
                contentType: 'application/x-www-form-urlencoded',
                data: {'dt_id':dt_id, '_token':'{{ csrf_token() }}'},
                success: function (response)
                {
                  $("#bus-details").html(response);
                     
                },
                error: function (jqXhr,textStatus, errorThrown)
                {
                   
                } 
              });


           }


            

           </script>


         

@endsection