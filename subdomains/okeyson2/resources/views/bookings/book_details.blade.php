
 <script>
  $(document).ready(function(){

    function calcPassengers(){

        var psgCount = 1;
        var cost = {{ $trip->fare }};
        var age_one = {{$excludePassenger->age_one or "null"}};
        
        var totalFare = cost;

        $( ".age-control" ).each(function( index ) {

            var age = parseInt($(this).val());

            if (age_one != null) {
              var age_two = {{$excludePassenger->age_two or 'null' }}
              if(age >= age_one && age <= age_two) {
                //psgCount = psgCount <= 1?1:psgCount-1;
              }else{
                  psgCount += 1;
              }
            }

        });

        $("#passenger_count").val(psgCount);

    }
    

    $("#retDate_div").hide();
    $('input:radio[name="is_round_trip"]').on('change',function(){
    if($(this).val() == 1)
    {
      $("#retDate_div").show(); 
    }
    else
    {
      $("#retDate_div").hide();
    }
});

      var more = 1;

      $('#add').click(function (e) {
        e.preventDefault();
        var psgCount = $("#passenger_count").val();
        psgCount = parseInt(psgCount);
        psgCount+=1;
        $("#passenger_count").val(psgCount);

        var noo = $('.eachField').length;
        console.log(noo+1);
        var table = $(this).closest('table');
        if (table.find('input:text').length < 7) {

       table.append('<tr class="eachField"> <td style="width:100px;" align="right">Passenger '+(noo + 1)+'&nbsp;&nbsp;&nbsp; </td/><td style="width:200px;"> <input type="text" class="form-control" id="current Name" name="psg'+noo+'" value="" /></td>&nbsp;&nbsp;&nbsp;<td style="width:100px;"> <select name="psggender'+noo+'" id="gender" required class="form-control"><option value=""> -gender- </option><option value="male">MALE</option><option value="female">FEMALE</option></select></td><td align="left"><input class="form-control age-control" value="30" placeholder="age" style="width:60px;" type="number" name="age'+noo+'" id="age'+noo+'"></td></tr>');

        more+1;
            
            //enable if selected seats is equal to passenger count
            if(formArray.length ==  psgCount){
               $('#submitBtn').removeClass('disabled');
            }else{
                   $('#submitBtn').addClass('disabled');
            }


    }

  /// listen for events after add click

  @if (!empty($excludePassenger)) 

      $('#age'+noo).on('change', function(){

          calcPassengers();
          
          var psgCount = $("#passenger_count").val();
          if(formArray.length ==  psgCount){
               $('#submitBtn').removeClass('disabled');
            }else{
                   $('#submitBtn').addClass('disabled');
            }

      });

  @endif

        return false;
    });

    $('#del').click(function (e) {//Decrease passenger count
    e.preventDefault();
    var psgCount = $("#passenger_count").val();
    psgCount = parseInt(psgCount);
    if (psgCount > 1) {
        psgCount-=1;

    }

    $("#passenger_count").val(psgCount);
    var noo = $('.eachField').length
    console.log(noo-1);
    var table = $(this).closest('table');
    if (table.find('input:text').length >= 1) {
        table.find('input:text').last().closest('tr').remove();
    }
        
        if(formArray.length ==  psgCount){
               $('#submitBtn').removeClass('disabled');
            }else{
                   $('#submitBtn').addClass('disabled');
            }

    return false;
});






  });
  </script>
<link rel="stylesheet" href="{{ url('bckend/js/plugins/datepicker/datepicker.css') }}">

<!-- <script src="{{ asset('bckend/js/libs/jquery-1.10.1.min.js') }}"></script> -->

<script>
function goBack() {
    window.history.back();
}
</script>


      
           <div class="form-group {{ $errors->has('contact_name') ? 'has-error' : ''}}">
                {!! Form::label('final_cost', 'Final Price: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('final_cost', '&#8358;  '.number_format($trip->fare), ['class' => 'form-control', 'disabled'=>'disabled']) !!}
                {!! $errors->first('final_cost', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>

      

      <div class="row">

        <div class="col-md-12 col-sm-7">

          <div class="portlet">

            <div class="portlet-content">

            @if(Session::has('error'))
                <div class="alert alert-danger">
                    <a class="close" data-dismiss="alert" href="#" aria-hidden="true">Ã—</a>
                    <strong>Oh snap!</strong>  {!! session('error') !!}
                  </div>

            @endif

            @if ($errors->any())
                    <ul class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                @endif

                {!! Form::open(['class' => 'form-horizontal', 'id'=>'newBookForm' ]) !!}

           
            <!-- <div class="form-group {{ $errors->has('trip_stop') ? 'has-error' : ''}}">
                {!! Form::label('trip_stop', 'Trip Stop: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                <select id ="tripStop" name="tripStop" class="form-control">
                  
                </select>
                {!! $errors->first('trip_name', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div> -->

            <div>
            <input type="hidden" name="bookedSeats" value="{{$booked_seats}}" id="bookedSeats"/>
            <input type="hidden" name="no_of_seats" value="{{$trip->parenttrip->bus->no_of_seats}}" id="no_of_seats"/>
            <input type="hidden" name="selSeats" value="" id="selSeats" />




<!-- <h3>Choose seats</h3>
<hr/>


      <div style="font-size:1.2em;"> Choose seats by clicking the corresponding seat in the layout below:</div>
       <div id="holder"  class="col-md-9"> 
        <ul  id="place">
            
        </ul>    
  </div>
   

   <div class="col-md-3"> 
     <ul id="seatDescription">
      <li style="background:url('{{ asset("bckend/img/available_seat_img.gif") }}') no-repeat scroll 0 0 transparent;">Available Seat</li>
      <li style="background:url('{{ asset("bckend/img/booked_seat_img.gif") }}') no-repeat scroll 0 0 transparent;">Booked Seat</li>
      <li style="background:url('{{ asset("bckend/img/selected_seat_img.gif") }}') no-repeat scroll 0 0 transparent;">Selected Seat</li>
     </ul>        
   </div>
   
   <hr /><br>

            </div>
            <br style="clear:both;" />
            <br><br><br>
           <hr/> -->
<h3>Choose seats</h3>
<hr/>
 <div style="font-size:1.2em;">
        Choose seats by clicking the corresponding seat in the layout below:
      </div>
       <div id=""  class="col-sm-8">
                   <ul id="bus-seat"></ul>
       </div>


   <div class="col-sm-3" id="seat-explain">
     <!-- <ul id="seatDescription"> -->
      <li class=""><label><i class="fa fa-car" aria-hidden="true"></i></label> Available Seat</li>
      <li class=""><label><i class="fa fa-car" aria-hidden="true"></i></label> Booked Seat</li>
      <li class=""><label><i class="fa fa-car" aria-hidden="true"></i></label> Selected Seat</li>
    <!--  </ul> -->
   </div>

   <br>


           <div class="col-md-9 col-sm-5">

            @if($trip->round_trip_status)
             <div class="form-group {{ $errors->has('trip_type') ? 'has-error' : ''}}">
                {!! Form::label('is_round_trip', 'Trip Type : ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
             <div class="checkbox">
                <label>{!! Form::radio('is_round_trip', '0', true) !!} One Way</label>
            </div>
            <div class="checkbox">
                <label>{!! Form::radio('is_round_trip', '1') !!} Round Trip</label>
            </div>
                {!! $errors->first('is_round_trip', '<span class="parsley-error-list">:message</span>') !!}
                </div>
                
            </div>

           @else
            
            <input type="hidden" name="is_round_trip" value="0" id="is_round_trip"> 

           @endif 
           



            
            <div class="form-group {{ $errors->has('booking_date') ? 'has-error' : ''}}">
                {!! Form::label('booking_date', 'Departure Date: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                <input type="text" name="trip" value="{{ $date }}" class="form-control" disabled>
                {!! $errors->first('booking_date', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>


            <!-- <div class="form-group {{ $errors->has('departure_time') ? 'has-error' : ''}}">
                {!! Form::label('departure_time', 'Departure Time: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6 input-group bootstrap-timepicker">
                <input id="tp-ex-1" type="text" class="form-control" name="departure_time" value="{{ (!empty($dt))?$dt->departure_time:$trip->parenttrip->departure_time }}">
                    <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                   
                {!! $errors->first('departure_time', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div> -->
           
            <div class="form-group {{ $errors->has('return_date') ? 'has-error' : ''}}" id ="retDate_div">
                {!! Form::label('return_date', 'Return Date: ', ['class' => 'col-sm-3 control-label']) !!}
                
                <div id="dp-ex-3" class="input-group date col-sm-6" data-auto-close="true"  data-date-format="dd-mm-yyyy" data-date-autoclose="true">
                    <input class="form-control" type="text" name="return_date" value="{{ $date }}">
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                </div>
                {!! $errors->first('return_date', '<span class="parsley-error-list">:message</span>') !!}
                <!-- <span class="help-block">dd-mm-yyyy</span> -->
            
            </div>


            @if($flexi)
            <div class="form-group {{ $errors->has('contact_name') ? 'has-error' : ''}}">
                {!! Form::label('final_cost', 'Final Price: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('final_cost', $trip->fare, ['class' => 'form-control']) !!}
                {!! $errors->first('final_cost', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>

            @endif

            
            

            <!--div class="form-group {{ $errors->has('paid_date') ? 'has-error' : ''}}">
                {!! Form::label('paid_date', 'Paid Date: ', ['class' => 'col-sm-3 control-label']) !!}

                <div class="col-sm-4">

                <div id="dp-ex-3" class="input-group date" data-auto-close="true"  data-date-format="dd-mm-yyyy" data-date-autoclose="true">
                    <input class="form-control" type="text" name="paid_date">
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                </div>
                </div>
            </div-->
            
            <div class="form-group {{ $errors->has('contact_phone') ? 'has-error' : ''}}">
                {!! Form::label('contact_phone', 'Passenger Phone: ', ['class' => 'col-sm-3 control-label', 'required'=>'required']) !!}
                <div class="col-sm-6">
                    {!! Form::text('contact_phone', null, ['class' => 'form-control', 'maxlength'=>'11']) !!}
                    <span> Mobile (0xxxxxxxxxx)</span>
                {!! $errors->first('contact_phone', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>

             <div class="form-group {{ $errors->has('contact_name') ? 'has-error' : ''}}">
                {!! Form::label('contact_name', 'Passenger Name: ', ['class' => 'col-sm-3 control-label', 'required'=>'required']) !!}
                <div class="col-sm-6">
                    {!! Form::text('contact_name', null, ['class' => 'form-control']) !!}
                {!! $errors->first('contact_name', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>

            
            <!-- div class="form-group {{ $errors->has('contact_email') ? 'has-error' : ''}}">
                {!! Form::label('contact_email', 'Passenger Email: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::email('contact_email', null, ['class' => 'form-control']) !!}
                {!! $errors->first('contact_email', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div> -->

             <div class="form-group {{ $errors->has('contact_address') ? 'has-error' : ''}}">
                {!! Form::label('contact_address', 'Passenger Address: ', ['class' => 'col-sm-3 control-label', 'required'=>'required']) !!}
                <div class="col-sm-6">
                    {!!  Form::textarea('contact_address', null, ['class' => 'form-control', 'size' => '7x5'])  !!}
                {!! $errors->first('contact_address', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>

            <div class="form-group {{ $errors->has('contact_address') ? 'has-error' : ''}}">
                {!! Form::label('gender', 'Gender: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                   <select name="gender" id="gender" class="form-control" required="required">
                     <option value="male">MALE</option>
                     <option value="female">FEMALE</option>  
                   </select>
                {!! $errors->first('gender', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>


             @if($trip->is_intl_trip)
          

            <div class="form-group {{ $errors->has('passport_type') ? 'has-error' : ''}}">
                {!! Form::label('passport_type', 'Passport type: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
             <div class="checkbox">
                <label>{!! Form::radio('passport_type', 'Regular', true) !!} Regular</label>
            </div>
            <div class="checkbox">
                <label>{!! Form::radio('passport_type', 'Virgin passport') !!} Virgin passport</label>
            </div>
            <div class="checkbox">
                <label>{!! Form::radio('passport_type', 'ID card/ No passport') !!} ID card/ No passport</label>
            </div>
                {!! $errors->first('passport_type', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>

            @endif
           
             <div class="form-group {{ $errors->has('next_of_kin') ? 'has-error' : ''}}">
                {!! Form::label('next_of_kin', 'Next Of Kin Name: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('next_of_kin', null, ['class' => 'form-control', 'required'=>'required']) !!}
                {!! $errors->first('next_of_kin', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div> 
            <div class="form-group {{ $errors->has('next_of_kin_phone') ? 'has-error' : ''}}">
                {!! Form::label('next_of_kin_phone', 'Next Of Kin Phone: ', ['class' => 'col-sm-3 control-label', ]) !!}
                <div class="col-sm-6">
                    {!! Form::text('next_of_kin_phone', null, ['class' => 'form-control','maxlength'=>'11', 'required'=>'required']) !!} <span> Mobile (0xxxxxxxxxx)</span>
                {!! $errors->first('next_of_kin_phone', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>


            <div class="form-group {{ $errors->has('ac') ? 'has-error' : ''}}">
                {!! Form::label('payment_method', 'Payment Method: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
             <div class="checkbox">
                <label>{!! Form::radio('payment_method', 'Cash', true, ['onchange'=>'toggleReceiptNoFld(this)']) !!} Cash</label>
            </div>
            <div class="checkbox" >
                <label>{!! Form::radio('payment_method', 'Card', false, ['onchange'=>'toggleReceiptNoFld(this)']) !!} Card</label>
            </div>
            <div class="checkbox">
                <label>{!! Form::radio('payment_method', 'Book on Hold', false, ['onchange'=>'toggleReceiptNoFld(this)']) !!} Book on Hold</label>
            </div>
                {!! $errors->first('ac', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>

            <div class="form-group {{ $errors->has('receipt_number') ? 'has-error' : ''}} collapse" id="receiptNoDiv">
                {!! Form::label('receipt_number', 'Receipt No: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('receipt_number', null, ['class' => 'form-control','maxlength'=>'11']) !!} 
                {!! $errors->first('receipt_number', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>

            <!-- <div class="form-group {{ $errors->has('payment_method_id') ? 'has-error' : ''}}">
                {!! Form::label('payment_method_id', 'Payment Method: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::select('payment_method_id', $payment_methods,null, ['class' => 'form-control']) !!}
                {!! $errors->first('payment_method_id', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div> -->

             @if($track_luggage)
            <div class="form-group {{ $errors->has('passenger_count') ? 'has-error' : ''}}">
                {!! Form::label('luggage_weight', 'Luggage Weight (in kg): ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('luggage_weight', null, ['class' => 'form-control', 'id'=>'luggWeightFld', 'onkeyup'=>'calcFinalFare()']) !!}
                {!! $errors->first('luggage_weight', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>
            @endif


            <div class="form-group {{ $errors->has('passenger_count') ? 'has-error' : ''}}">
                {!! Form::label('passenger_count', 'Passenger Count: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::number('passenger_count', 1, ['class' => 'form-control','readonly'=>'readonly']) !!}
                {!! $errors->first('passenger_count', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>
            

            
             
             <hr>  

             <h3>Other passengers</h3>
                
                <table border="0" cellspacing="2">
    <tr class="eachField">
        <td style="width:200px;" align="right"> 
            <td>
                <!-- <input type="text" id="current Name" value="" /> -->
            </td>
        </td>
    </tr>
    
    <tr>
        <td align="right"></td>
        <td>
            <a href="#" id="add" value="Add">Add Passenger</a> | 
            <a href="#" id="del" value="Del" >Remove</a>
        </td>
    </tr>
    <tr>
        <td style="height:3px" colspan="2"></td>
    </tr>
    <tr style="background-color: #383838">
        <td></td>
    </tr>
    <tr></tr>
    <tr>
        </div>
        </div>
        </td>
    </tr>
</table>

<br><br><br>

<hr/>




  <!--div style="width:580px;text-align:left;margin:5px">  
    <input type="button" id="btnShowNew" value="Show Selected Seats" /><input type="button" id="btnShow" value="Show All" />            </div-->


<br style="clear:both;" /><br/><br/>
<div class="alert alert-success collapse" id="success-notif">
            Passenger has been transloaded to the new bus.
        </div>

        <div class="alert alert-danger collapse" id="error-notif">
            
        </div>


                <div class="form-group">
                    
                    <div class="col-sm-offset-3 col-sm-3">
                        {!! Form::submit('Book', ['id'=>'submitBtn', 'class' => 'disabled btn btn-primary form-control']) !!}
                    </div>
                </div>
                {!! Form::close() !!}




                </div>
                <div class="col-md-3 col-sm-5">



                       <div class="portlet" style="margin-top:200px;">

                        <div class="portlet-header">

                          <h3>
                            <i class="fa fa-reorder"></i>
                            Trip Details
                          </h3>

                        </div> <!-- /.portlet-header -->

                        <div class="portlet-content">

                          <dl>
                            <dt>From</dt>
                            <dd>{{ $trip->sourcepark->name }}</dd>
                            <dt>To</dt>
                            <dd>{{ $trip->destpark->name }}</dd>
                           <!--  <dt>Departure Time</dt> -->
                            <!-- <dd> {{ $trip->parenttrip->departure_time }} </dd> -->

                            <dt>Bus</dt>
                            <dd> {{ (!empty($dt))?$dt->bus->number_plate:$trip->parenttrip->bus->number_plate }} </dd>
                            
                            <dt>Total Fare</dt>
                            <dd> &#8358; <span id="fareDisp">{{ number_format($trip->fare) }}</span> </dd>

                            

                          </dl>

                          

                        </div> <!-- /.portlet-content -->

                      </div>

                    </div>

                
             </div> <!-- /.portlet-content -->

          </div> <!-- /.portlet -->

        

        </div> <!-- /.col -->

        

      </div> <!-- /.row -->   

  <script src="{{ url('bckend/js/libs/jquery-1.10.1.min.js') }}"></script>
  <script src="{{ url('bckend/js/libs/jquery.form.js') }}"></script>

  <script>
  $(document).ready(function(){
    $('input:radio[name="is_round_trip"]').on('change',function(){
        calcFinalFare();
    });

    $('input:radio[name="passport_type"]').on('change',function(){
        calcFinalFare();
    });





    var bookedSeat = $('#bookedSeats').val();
    var d = [];
    var noOfSeats = $('#no_of_seats').val();
    noOfSeats = parseInt(noOfSeats);
        if(bookedSeat != ""){
             d = bookedSeat.split(',').map(function(el){ return +el;});
            //alert(d);
        }
        
        
     formArray = [];
      

      function displayBusSeat(li, div) {
            
        for (var i = 1; i <= li; i++) {
          $('#bus-seat').append('<li class="li-seat clearfix"></li>');
        }
        for (var i = 0; i <= div; i++) {
          if (d.indexOf(i) > -1) {
                    $('.li-seat').append('<div class="forbuses"><input type="checkbox" id='+i+' disabled/><label for='+i+'><i class="fa fa-car" aria-hidden="true"></i>'+i+'</label></div>');
          }
          else if (i == 0) {
            continue;
          }
          else{
            $('.li-seat').append('<div class="forbuses"><input type="checkbox" id='+i+' /><label for='+i+'><i class="fa fa-car" aria-hidden="true"></i>'+i+'</label></div>');
          }
        }

      }

        displayBusSeat(1, noOfSeats);
        
      $('input[type="checkbox"]').change(function() {
          var newValue = parseInt($(this).attr('id'));
            if (this.checked) {
                formArray.push(newValue);
                console.log(formArray);
            }else {
                    var index = $.inArray(newValue, formArray);
                    formArray.splice(index, 1);
                    //formArray = ary.filter(function(e) { return e !== newValue })
              console.log(formArray);
            }
                $("#selSeats").val(formArray.join(','));
                  checkChosenSeats(formArray);
        });

            function checkChosenSeats(str){
                var passenger_count = $("#passenger_count").val();
                if(str.length == passenger_count){
                  $('#submitBtn').removeClass('disabled');
                  console.log('equal');
                }else{
                   $('#submitBtn').addClass('disabled');
                   console.log('not equal');
                }
            }



  });


  function calcFinalFare(){

     var roundtrip = $('input[name=is_round_trip]:checked').val();
     var passType = $('input[name=passport_type]:checked').val();
     var passenger_count = $("#passenger_count").val();

     // console.log(roundtrip);
     // console.log(passType);
     var luggage_cost = 0;

     @if($track_luggage)
        var totalLuggLimit = passenger_count * {{ $luggage_limit }};
        var luggweight = $("#luggWeightFld").val();
        luggage_cost = (luggweight - totalLuggLimit) * {{ $cpk }};
        if(luggage_cost < 0)
            luggage_cost  = 0;
        



     @endif

     if(roundtrip == 1){

        if(passType == 'Virgin passport')
            $("#fareDisp").html(currency_format(({{ $trip->round_trip_virgin_passport_fare }} * passenger_count)+ luggage_cost));
        else if(passType == 'ID card/ No passport')
            $("#fareDisp").html(currency_format(({{ $trip->round_trip_no_passport_fare }} * passenger_count)+ luggage_cost));
        else
            $("#fareDisp").html(currency_format(({{ $trip->round_trip_fare }} * passenger_count)+ luggage_cost).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,"));

     }else{

         if(passType == 'Virgin passport')
            $("#fareDisp").html(currency_format(({{ $trip->virgin_passport_fare }} * passenger_count)+ luggage_cost));
        else if(passType == 'ID card/ No passport')
            $("#fareDisp").html(currency_format(({{ $trip->no_passport_fare }} * passenger_count)+ luggage_cost));
        else
            $("#fareDisp").html(currency_format(({{ $trip->fare }} * passenger_count)+ luggage_cost));

     }


  }



  </script>

<script type="text/javascript">

  //     var more = 1;

  //     $('#add').click(function (e) {
  //       e.preventDefault();
  //       var psgCount = $("#passenger_count").val();
  //       psgCount = parseInt(psgCount);
  //       psgCount+=1;
  //       $("#passenger_count").val(psgCount);

  //       var noo = $('.eachField').length;
  //       console.log(noo+1);
  //       var table = $(this).closest('table');
  //       if (table.find('input:text').length < 7) {

  //      table.append('<tr class="eachField"> <td style="width:100px;" align="right">Passenger '+(noo + 1)+'&nbsp;&nbsp;&nbsp; </td/><td style="width:200px;"> <input type="text" class="form-control" id="current Name" name="psg'+noo+'" value="" /></td>&nbsp;&nbsp;&nbsp;<td style="width:100px;"> <select name="psggender'+noo+'" id="gender" required class="form-control"><option value=""> -gender- </option><option value="male">MALE</option><option value="female">FEMALE</option></select></td><td align="left"><input class="form-control age-control" value="30" placeholder="age" style="width:60px;" type ="text" name="age'+noo+'" id="age'+noo+'"></td></tr>');

  //       more+1;
  //   }
  // /// listen for events after add click

  //     $('#age'+noo).change(function(){
  //         calcPassengers();

  //     });

  //       return false;
  //   });

// $('#del').click(function (e) {
//     e.preventDefault();
//     var psgCount = $("#passenger_count").val();
//     psgCount = parseInt(psgCount);
//     if (psgCount > 1) {
//         psgCount-=1;

//     }

//     $("#passenger_count").val(psgCount);
//     var noo = $('.eachField').length
//     console.log(noo-1);
//     var table = $(this).closest('table');
//     if (table.find('input:text').length >= 1) {
//         table.find('input:text').last().closest('tr').remove();
//     }

//     return false;
// });

    // function calcPassengers(){

    //     // var psgCount = $("#passenger_count").val();
    //     var psgCount = 1;
    //     var cost = {{ $trip->fare }};
    //     var totalFare = cost;

    //     $( ".age-control" ).each(function( index ) {

    //         var age = parseInt($(this).val());

    //         //If age is greater than 3, then passenger will be added in passenger count
    //         //else if is below 3, passenger is considered a baby and wont be added to count
    //         if(age > 4)
    //           psgCount += 1;

    //         //Calculate Discount for infants between the ages 3 and 9
    //         if(age > 0 && age <= 4)
    //           //totalFare += (cost * 0.9);
    //         else if(age > 9)
    //          // totalFare += cost;


    //         console.log(totalFare);

    //     });

    //     $("#passenger_count").val(psgCount);
    //     // $("#totalFare").html(totalFare);

    // }

</script>
<!-- 
<script type="text/javascript">

    
    </script> -->

    <script>
    // this part does the auto remember of customer details
    $("#contact_phone").on('blur', function()
   {
      var phone =  $("#contact_phone").val();
      if(phone !="")
      {
           // use phone value to perform ajax request
            $.ajax({
                    url: '{{ route("get-cust-details") }}',
                    dataType: 'text',
                    type: 'GET',
                    contentType: 'application/x-www-form-urlencoded',
                    data: "ph="+phone,
                    success: function (rslts, textStatus, jQxhr)
                    {

                      
                      if(rslts != ""){
                          var rsltsArr = rslts.split("|");
                          // DISPLAY THOSE RESULTS
                          $("#contact_name").val(rsltsArr[0]);
                          $("#contact_email").val(rsltsArr[1]);
                          $("#contact_address").val(rsltsArr[2]);
                          if(rsltsArr[3] == "male")
                          {
                            $("#gender option:eq(0)").attr("selected", "selected");
                          }
                          else
                          {
                            $("#gender option:eq(1)").attr("selected", "selected");
                          }
                          
                          $("#next_of_kin").val(rsltsArr[4]);
                          $("#next_of_kin_phone").val(rsltsArr[5]);  
                      }
                      

                    },
                    error: function (jqXhr,textStatus, errorThrown)
                    {
                        console.log(errorThrown);
                        $("#contact_name").val("");
                        $("#contact_email").val("");
                        $("#contact_address").val("");
                         $("#next_of_kin").val("");
                        $("#next_of_kin_phone").val("");  
                    }
      });
    }
  });


    function toggleReceiptNoFld(elem){

      console.log(elem.value);

      if(elem.value == 'Card')  
        $("#receiptNoDiv").removeClass('collapse');
      else
        $("#receiptNoDiv").addClass('collapse');


    }



     $(document).ready(function(){ 
           $('#newBookForm').ajaxForm({
                beforeSubmit: preSubmit,
                success: function(response){

                   console.log(response); 
                  
                  if(response.startsWith("success")){
                  // if(false){
                      // $("#submitBtn").addClass('collapse');  
                      // $("#TransCloseBtn").addClass('collapse');  
                      // $("#TransRefreshBtn").removeClass('collapse');  
                      // $('#TranscontentArea').addClass('collapse');
                      // $("#error-notif").addClass("collapse");
                      $("#error-notif").addClass("collapse");
                      $("#success-notif").html('Booking was successful!');
                      $("#success-notif").removeClass("collapse");

                      var booking_id = response.replace('success--', '');

                      $("#destSelect").trigger("change");

                      window.open('{{ url("ticket") }}/'+booking_id, '_blank');


                  }else{

                      $("#success-notif").addClass("collapse");
                      $("#error-notif").html(response);
                      $("#error-notif").removeClass("collapse");
                      $("#submitBtn").html('Transload');



                  }
                  
                  
                },
                error: function(response){
                  
                  console.log(response);
                   $("#error-notif").html(response.responseText);
                   $("#success-notif").addClass("collapse");
                      $("#error-notif").removeClass("collapse");
                      $("#submitBtn").html('Transload');
                  
                },
                reset: true
            });
          });


        function preSubmit(){
          
          // console.log('I am here...');
          $("#submitBtn").html('Please wait...');
          $("#error-notif").addClass("collapse");
          $("#success-notif").removeClass("collapse");
          $("#success-notif").html("Please wait....");

          // console.log('Please wait....');
         

        }

    </script>



    <style type="text/css">
    li {
      list-style: none;
    }
#seat-explain label {
      display: inline-block;
      position: relative;
      width: 50px;
      text-align: center;
      font-size: 14px;
      font-weight: bold;
      line-height: 1.5rem;
      padding: 10px;
      border-radius: 5px;
      animation-duration: 300ms;
      animation-fill-mode: both;
  }
#seat-explain li:first-of-type label {  
      background: steelblue;
  }
#seat-explain li:nth-of-type(2) label {  
      background: #F42536;
  }
#seat-explain li:last-of-type label {  
      background: #bada55;
  }
  </style>
