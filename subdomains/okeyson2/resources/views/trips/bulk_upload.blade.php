@extends('layouts.master')

@section('content')

 <div class="content-header">
        <h2 class="content-header-title">Trips </h2>
        <ol class="breadcrumb">
          <li><a href="{{ url('/') }}">Dashboard</a></li>
          <li class="active">Trips </li>
        </ol>
      </div> <!-- /.content-header -->




      <div class="row">

        <div class="col-md-10 col-sm-8">

          <div class="portlet">

            <div class="portlet-content">



                @if(!empty($flash))
                    @foreach($flash as $f)
                  <div class="alert alert-info">
                        <?php echo $f; ?></div>
                    @endforeach
                @endif

             <h1>Bulk Upload Trips</h1>
             @if (Session::has('status'))
                 <div class="alert alert-success" role="alert">
                   {!! session('status') !!}
                 </div>
             @endif

                <form style="border: 4px solid #a1a1a1;margin-top: 15px;padding: 10px;"
                  action="/trips/upload"
                  class="form-horizontal" method="post" enctype="multipart/form-data">
                    <input type="file" name="userfile" />
                    <br/>
                    <button class="btn btn-primary">Import Trips</button>
                  </form>





        <div class="col-md-2 col-sm-4">

            <ul id="myTab" class="nav nav-pills nav-stacked">
              <li class="active">
              <a href="{{ URL::to('/downloads/trips_template.xlsx')  }}" >
              <i class="fa fa-download"></i>
                Download Excel
              </a>
            </li>
          </ul>

        </div>
            </div>
        </div>
        </div>
    </div>


@endsection
