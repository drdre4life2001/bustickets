@extends('layouts.master')

@section('content')
  <link rel="stylesheet" href="{{ url('bckend/js/plugins/datepicker/datepicker.css') }}">
   <div class="content-header">
        <h2 class="content-header-title">Book a Ticket </h2>
        <ol class="breadcrumb">
          <li><a href="{{ url('/') }}">Dashboard</a></li>
          <li class="active">Book a Ticket </li>
          <li><a href="{{ url('bookings/status/PAID') }}">Paid Bookings</a> </li>
            <li><a href="{{ url('daily-trips') }}">Daily Trips</a> </li>
            <li><a href="{{ url('quick-book') }}">Quick Book</a> </li>
        </ol>

        <!-- pulll right -->
       
      </div> <!-- /.content-header -->

      <div class ="row" align="right">
      <div class="col-md-12 col-sm-12 pull-right">                
                <form method="post" action="">
                
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                <input type="hidden" name="trip_date" value="{{ $date }}">
                <div class="col-sm-2 pull-right">
               <!--  <strong align="left"><center>Search By Bus</center> </strong>
        
                            <input type="text" name="bus" class="form-control" value="{{ (isset($b))?$b:'' }}">
 -->                                <!-- <option value="">Choose Park</option> --> 
                       <!--  {!! $errors->first('bus', '<span class="parsley-error-list">:message</span>') !!} -->
                        <!-- <span class="input-group-addon"><i class="fa fa-calendar"></i></span> -->  
            </div>
              
              </form>
              
              </div>
      </div>
      <br>

      <div class="row">

        <div class="col-md-12 col-sm-10">

          <div class="portlet">

            <div class="portlet-content">
                
                <form method="post" action="">
                
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

                
             <div class="col-sm-3">
                <strong>Source Park </strong>
                <div  class="input-group date" data-auto-close="true" data-date="12-02-2012" data-date-format="dd-mm-yyyy" data-date-autoclose="true">
                            <select name="source_park" class="form-control" id="select22">
                                <!-- <option value="">Choose Park</option> -->
                                @foreach($boardable_parks as $park)
                                <option value="{{ $park->id }}" {{ ($userPark == $park->id)?'selected="selected"':'' }} >{{ $park->name }}</option>
                                @endforeach
                            </select> 
                        {!! $errors->first('source_park', '<span class="parsley-error-list">:message</span>') !!}
                        <!-- <span class="input-group-addon"><i class="fa fa-calendar"></i></span> -->
                </div>
            </div>

            <div class="col-sm-3">
                <strong>Destination Park</strong>
                <div  class="input-group date" data-auto-close="true" data-date="12-02-2012" data-date-format="dd-mm-yyyy" data-date-autoclose="true">
                    <select name="dest_park" class="form-control">
                                <option value="">Choose Park</option>
                                @foreach($all_parks as $park)
                                <option value="{{ $park->id }}" {{ ($dest_park_id == $park->id)?'selected="selected"':'' }}>{{ $park->name }}</option>
                                @endforeach
                    </select>
                    {!! $errors->first('dest_park', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>
                    
            <div class="col-sm-3">
                <strong>Departure Date</strong>
                <div id="dp-ex-3" class="input-group date" data-auto-close="true"  data-date-format="dd-mm-yyyy" data-date-autoclose="true">
                    <input class="form-control" type="text" name="departure_date" value="{{ $date }}">
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                </div>
                <!-- <span class="help-block">dd-mm-yyyy</span> -->
            </div>


<br>
                <input type="submit" value="Search Trip" class="btn btn-danger">               
                    
    
                </form>
             </div> <!-- /.portlet-content -->

          </div> <!-- /.portlet -->
    
         

        </div> <!-- /.col -->

      

      </div> <!-- /.row -->   

@if(!empty($trips))
 <div class="row">

        <div class="col-md-10 col-sm-8">

          <div class="portlet">

            <div class="portlet-content">           

              <div class="table-responsive">

              <table 
                class="table table-striped table-bordered table-hover table-highlight table-checkable" 
                data-provide="datatable" 
                data-display-rows="10"
                data-info="true"
                data-search="true"
                data-length-change="true"
                data-paginate="true"
              >
                  <thead>
                    <tr>
                      <th>Bus Number</th>
                      <!-- <th>Leaving From</th> -->
                      <th>Going to</th>
                      <!-- <th>Departure time</th> -->
                      <th width="150px">Trip transit</th>
                       <!-- <th>Trip type</th> -->
                      <th width="180px">One-Way Fare</th>
                       <th width="180px">Round Trip Fare</th>
                      <th>Features</th>
                      <th>Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    {{-- */$x=0;/* --}}
                    @foreach($trips as $item)
                        {{-- */$x++;  /* --}}

                        @if(count($item['parenttrip']['dailyTrips']) == 0)
                        <tr>
                            <!--td>{{ $x }}</td-->
                            <td>
                                {{ $item['parenttrip']['bus']['number_plate'] != ""?$item['parenttrip']['bus']['number_plate']:"Not Assigned" }} <br/>
                                <!-- <small>[{{ $item['parenttrip']['trip_transit'] }}]</small> -->
                            </td>
                            <!-- <td>{{ $item['sourcepark']['name'] }}</td> -->
                            <td>{{ $item['destpark']['name'] }}<br/><br/><small>Final destination: [{{  $item['parenttrip']['destpark']['name'] }}]</small></td>
                          <!--   <td>{{ $item['parenttrip']['departure_time'] }}</td> -->
                            <td>{{ empty($item['trip_transit'])?"First Bus":$item['trip_transit'] }}</td>
                            <!-- <td>{{ ($item['round_trip_status'] == 1)?  "Round Trip"   : "One -Way" }}</td> -->
                            <td> 
                                &#8358; {{ number_format($item['fare']) }}<br/>

                                @if($item['is_intl_trip'])
                                  <small>Virgin Passport: &#8358; {{ number_format($item['virgin_passport_fare']) }}</small><br/>
                                  <small>ID/ No Passport: &#8358; {{ number_format($item['no_passport_fare']) }}</small>
                                @endif

                               
                            </td>
                           <td>
                              
                                {{ ($item['round_trip_status'])?"&#8358; ".number_format($item['round_trip_fare']):'' }}<br/><!--  <small>(Round trip)</small> -->

                                @if($item['is_intl_trip'] && $item['round_trip_status'])
                                  <small>Virgin Passport: &#8358; {{number_format($item['round_trip_virgin_passport_fare']) }}</small><br/>
                                  <small>ID/ No Passport: &#8358; {{ number_format($item['round_trip_no_passport_fare']) }}</small>
                                @endif
                              
                           </td>
                            <td>
                               @if($item['ac'])
                                <span<i class="fa fa-check-square-o font-green-jungle"></i>  AC</span>
                                @else
                                <span><i class="fa fa-times-circle font-red-thunderbird"></i>  AC</span>
                                @endif
                                
                                &nbsp;&middot;&nbsp;

                               <!--  @if($item['security'])
                                    <span><i class="fa fa-check-square-o font-green-jungle"></i>  Security</span>
                                @else
                                <span ><i class="fa fa-times-circle font-red-thunderbird"></i>  Security</span>
                                @endif
                                
                                &nbsp;&middot;&nbsp;

                                @if($item['insurance'])
                                <span ><i class="fa fa-check-square-o font-green-jungle"></i>  Insurance</span>
                                @else
                                <span><i class="fa fa-times-circle font-red-thunderbird"></i>  Insurance</span>
                                @endif

                                &nbsp;&middot;&nbsp;
        
                                <br/>
                                @if($item['tv'])
                                <span><i class="fa fa-check-square-o font-green-jungle"></i>  TV</span>
                                @else
                                <span><i class="fa fa-times-circle font-red-thunderbird"></i>  TV</span>
                                @endif
                                
                                &nbsp;&middot;&nbsp;

                                @if($item['passport'])
                                <span><i class="fa fa-check-square-o font-green-jungle"></i>  Passport</span>
                                @else
                                <span><i class="fa fa-times-circle font-red-thunderbird"></i>  Passport</span>
                                @endif -->
                            </td>
                            
                            

                            <td>
                                <!--
                                <a href="{{ url('trips/' . $item['id'] . '/edit') }}" title="Edit">
                                    <button type="submit" class="btn btn-default btn-xs"><i class="fa fa-edit"></i></button>
                                </a>
                                {!! Form::open([
                                    'method'=>'DELETE',
                                    'url' => ['trips', $item['id']],
                                    'style' => 'display:inline'
                                ]) !!}
                                    {!! Form::button('<i class="fa fa-times"></i>', ['class' => 'btn btn-danger btn-xs', 'onclick'=>'ConfirmDelete()', 'escape'=>false,'title'=>'Delete', 'type'=>'submit']) !!}
                                {!! Form::close() !!}
                                -->
                                
                                <a href="{{ route('admin-book', [$item['id'], $d_date, 0]) }}" class="btn btn-primary btn-block btn-large">BOOK</a> 


                                
                                @if(!(strpos($permissions, 'book_from_any_park') === FALSE))
                                  <a href="{{ route('admin-book', [$item['id'], $d_date, 1]) }}" class="btn btn-default  btn-block btn-large">FLEXI BOOK</a> 
                                @endif

                            </td>
                        </tr>


                        @else

                           
                           @foreach($item['parenttrip']['dailyTrips'] as $dt )



                           <?php  
                              if(isset($b) && (strpos($dt['bus']['number_plate'], $b) === FALSE))
                                continue;


                            ?>




                              <tr>
                            <!--td>{{ $x }}</td-->
                            <td>
                                {{ $dt['bus']['number_plate'] != ""?$dt['bus']['number_plate']:"Not Assigned" }} <br/>
                                <small>[{{ $dt['trip_transit'] }}]</small>
                            </td>
                            <!-- <td>{{ $item['sourcepark']['name'] }}</td> -->
                            <td>{{ $item['destpark']['name'] }}<br/><br/><small>Final destination: [{{ $item['parenttrip']['destpark']['name'] }}]</small></td>
                            <!-- <td>{{ $dt['departure_time'] }}</td> -->
                            <td>{{ $dt['trip_transit'] }}</td>
                            <!-- <td>{{ ($item['round_trip_status'] == 1)?  "Round Trip"   : "One -Way" }}</td> -->
                            <td> 
                                &#8358; {{ number_format($item['fare']) }}<br/>

                                @if($item['is_intl_trip'])
                                  <small>Virgin Passport: &#8358; {{ number_format($item['virgin_passport_fare']) }}</small><br/>
                                  <small>ID/ No Passport: &#8358; {{ number_format($item['no_passport_fare']) }}</small>
                                @endif

                               
                            </td>
                           <td>
                              
                                {{ ($item['round_trip_status'])?"&#8358; ".number_format($item['round_trip_fare']):'' }}<br/><!--  <small>(Round trip)</small> -->

                                @if($item['is_intl_trip'] && $item['round_trip_status'])
                                  <small>Virgin Passport: &#8358; {{number_format($item['round_trip_virgin_passport_fare']) }}</small><br/>
                                  <small>ID/ No Passport: &#8358; {{ number_format($item['round_trip_no_passport_fare']) }}</small>
                                @endif
                              
                           </td>
                            <td>
                               @if($item['ac'])
                                <span<i class="fa fa-check-square-o font-green-jungle"></i>  AC</span>
                                @else
                                <span><i class="fa fa-times-circle font-red-thunderbird"></i>  AC</span>
                                @endif
                                
                                &nbsp;&middot;&nbsp;

                            </td>
                            
                            

                            <td>
                                <!--
                                <a href="{{ url('trips/' . $item['id'] . '/edit') }}" title="Edit">
                                    <button type="submit" class="btn btn-default btn-xs"><i class="fa fa-edit"></i></button>
                                </a>
                                {!! Form::open([
                                    'method'=>'DELETE',
                                    'url' => ['trips', $item['id']],
                                    'style' => 'display:inline'
                                ]) !!}
                                    {!! Form::button('<i class="fa fa-times"></i>', ['class' => 'btn btn-danger btn-xs', 'onclick'=>'ConfirmDelete()', 'escape'=>false,'title'=>'Delete', 'type'=>'submit']) !!}
                                {!! Form::close() !!}
                                -->
                                
                                <a href="{{ route('admin-book', [$item['id'], $d_date, 0, $dt['id']]) }}" class="btn btn-primary btn-block btn-large">BOOK</a> 


                                
                                @if(!(strpos($permissions, 'book_from_any_park') === FALSE))
                                  <a href="{{ route('admin-book', [$item['id'], $d_date, 1, $dt['id']]) }}" class="btn btn-default  btn-block btn-large">FLEXI BOOK</a> 
                                @endif

                            </td>
                        </tr>


                           @endforeach




                        @endif
                    @endforeach
                  </tbody>
                </table>
              </div> <!-- /.table-responsive -->
              

            </div> <!-- /.portlet-content -->

          </div> <!-- /.portlet -->

        

        </div> <!-- /.col -->

        <div class="col-md-2 col-sm-4">

           <div class="portlet">

            <div class="portlet-content"> 

                FILTER SECTION  
                
                <strong> </strong>
                
                <form method="post" action="">
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                <input type="hidden" name="source_park" value="<?php echo $source_park ?>">
                <input type="hidden" name="dest_park" value="<?php echo $dest_park; ?>">

                     <select name="operator_filter" class="form-control">
                        <option value="" >Choose park</option>
                        
                     </select>
                    <br>
                    <input type="submit" value="Filter" class="btn btn-danger">                    
                 </form>

            </div>
           </div>

        </div>

      </div> <!-- /.row -->  
@endif
  
 
  

  
@endsection