@extends('layouts.master')

@section('content')

    <div class="content-header">
        <h2 class="content-header-title">Edit Trip </h2>
        <ol class="breadcrumb">
          <li><a href="{{ url('/') }}">Dashboard</a></li>
          <li><a href="{{ url('trips') }}">Trips </a></li>
          <li class="active">Edit Trip </li>
        </ol>
      </div> <!-- /.content-header -->

      

      <div class="row">

        <div class="col-md-10 col-sm-8">

          <div class="portlet">

            <div class="portlet-content">

                {!! Form::model($trip, [
                    'method' => 'PATCH',
                    'url' => ['trips', $trip->id],
                    'class' => 'form-horizontal'
                ]) !!}


                <h3>Parent Trip</h3>
                <input type="hidden" name="trip_id" value="{{ $trip->id }}">
                <input type="hidden" name="active0" value="{{ $trip->active }}">

                
            <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                {!! Form::label('name', 'Source Park: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('source', $trip->sourcepark->name, ['class' => 'form-control', 'disabled'=>'disabled']) !!}
                {!! $errors->first('name', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>

            <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                {!! Form::label('name', 'Destination: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('dest', $trip->subTrips[0]->destpark->name, ['class' => 'form-control', 'disabled'=>'disabled']) !!}
                {!! $errors->first('name', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>
            
            <div class="form-group {{ $errors->has('fare') ? 'has-error' : ''}}">
                {!! Form::label('fare', 'Fare: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::number('fare', $trip->subTrips[0]->fare, ['class' => 'form-control']) !!}
                {!! $errors->first('fare', '<span class="parsley-error-list">:message</span>') !!}
                </div>
                
            </div>

            <div class="form-group {{ $errors->has('rt_activate') ? 'has-error' : ''}}">
                {!! Form::label('rt_activate', 'Activate Round Trip? : ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
             <div class="checkbox">
                <label>{!! Form::radio('rt_activate', '1') !!} Yes</label>
            </div>
            <div class="checkbox">
                <label>{!! Form::radio('rt_activate', '0', true) !!} No</label>
            </div>
                {!! $errors->first('rt_activate', '<span class="parsley-error-list">:message</span>') !!}
                </div>
                
            </div>

            
            <div class="form-group {{ $errors->has('rt_fare') ? 'has-error' : ''}}">
                {!! Form::label('rt_fare', 'Round Trip Fare: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::number('rt_fare', $trip->subTrips[0]->round_trip_fare, ['class' => 'form-control']) !!}
                {!! $errors->first('rt_fare', '<span class="parsley-error-list">:message</span>') !!}
                </div>
                
            </div>


             <div class="form-group {{ $errors->has('ac') ? 'has-error' : ''}}">
                {!! Form::label('is_intl_trip', 'Is International Trip?: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
             <div class="checkbox">
                <label>{!! Form::radio('is_intl_trip', '1', $trip->subTrips[0]->is_intl_trip == 1) !!} Yes</label>
            </div>
            <div class="checkbox">
                <label>{!! Form::radio('is_intl_trip', '0', $trip->subTrips[0]->is_intl_trip != 1) !!} No</label>
            </div>
                {!! $errors->first('is_intl_trip', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>

            <div class="intl_div {{ ($trip->subTrips[0]->is_intl_trip)?'':'collapse' }}" id="intl_div" >
            <hr>  
             <h3> International Rates</h3>
             <div class="form-group {{ $errors->has('virgin_passport_fare') ? 'has-error' : ''}}">
                {!! Form::label('virgin_passport_fare', 'Virgin Passport Fare:', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                     {!! Form::text('virgin_passport_fare',  $trip->subTrips[0]->virgin_passport_fare, ['class' => 'form-control', 'id'=>"vPass"] ) !!}
                     <p class="help-block">Leave blank or 0 is not applicable.</p>
                    {!! $errors->first('virgin_passport_fare', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('round_trip_virgin_passport_fare') ? 'has-error' : ''}}">
                {!! Form::label('round_trip_virgin_passport_fare', 'Virgin Passport Round Trip Fare:', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                     {!! Form::text('round_trip_virgin_passport_fare',  $trip->subTrips[0]->round_trip_virgin_passport_fare, ['class' => 'form-control', 'id'=>"vPass"] ) !!}
                     <p class="help-block">Leave blank or 0 is not applicable.</p>
                    {!! $errors->first('round_trip_virgin_passport_fare', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('no_passport_fare') ? 'has-error' : ''}}">
                {!! Form::label('no_passport_fare', 'Id Card/ No Passport Fare:', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                     {!! Form::text('no_passport_fare',  $trip->subTrips[0]->no_passport_fare, ['class' => 'form-control', 'id'=>"noPass"] ) !!}
                     <p class="help-block">Leave blank or 0 is not applicable.</p>
                    {!! $errors->first('no_passport_fare', '<p class="help-block">:message</p>') !!}
                </div>
            </div>

            <div class="form-group {{ $errors->has('round_trip_no_passport_fare') ? 'has-error' : ''}}">
                {!! Form::label('round_trip_no_passport_fare', 'Id Card/ No Passport Round Trip Fare:', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                     {!! Form::text('round_trip_no_passport_fare',  $trip->subTrips[0]->round_trip_no_passport_fare, ['class' => 'form-control', 'id'=>"noPass"] ) !!}
                     <p class="help-block">Leave blank or 0 is not applicable.</p>
                    {!! $errors->first('round_trip_no_passport_fare', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
             <hr>
            </div>


            <!-- <div class="form-group {{ $errors->has('departure_time') ? 'has-error' : ''}}">
                {!! \Html::decode(Form::label('departure_time', 'Departure Time: <br/><small>Optional</small>', ['class' => 'col-sm-3 control-label'])); !!}
                
                <div class="col-sm-6 input-group bootstrap-timepicker">
                <input id="tp-ex-1" type="text" class="form-control" name="departure_time" 
                 value="{{ $trip->departure_time }}">
                    <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                   
                {!! $errors->first('departure_time', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div> -->
            
            <!-- <div class="form-group {{ $errors->has('trip_transit') ? 'has-error' : ''}}">
                {!! Form::label('trip_transit', 'Trip Transit: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('trip_transit', $trip->subTrips[0]->trip_transit, ['class' => 'form-control', 'id'=>'tripTransit']) !!}
                {!! $errors->first('trip_transit', '<span class="parsley-error-list">:message</span>') !!}
                (First Bus, Second Bus, etc)
                </div>
            </div> -->
            <div class="form-group {{ $errors->has('default_bus_id') ? 'has-error' : ''}}">
                {!! Form::label('bus_id', 'Default Bus', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::select('bus_id', $buses,  $trip->bus->id, ['class' => 'form-control', 'id'=>"busInput", 'placeholder' => '-Choose-']) !!}
                    {!! $errors->first('bus_id', '<p class="help-block">:message</p>') !!}
                </div>
            </div>

             <div class="form-group {{ $errors->has('no_of_seats') ? 'has-error' : ''}}">
                {!! Form::label('no_of_seats', 'No of Seats: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('no_of_seats', null, ['class' => 'form-control', 'disabled'=>'disabled', 'id'=>'no_of_seats']) !!}
                {!! $errors->first('no_of_seats', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>

            <div class="form-group {{ $errors->has('no_of_seats') ? 'has-error' : ''}}">
                {!! Form::label('no_of_seats', 'Number Plate: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('numberPlate', null, ['class' => 'form-control', 'disabled'=>'disabled', 'id'=>'numberPlate']) !!}
                {!! $errors->first('no_of_seats', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>

            <div class="form-group {{ $errors->has('no_of_seats') ? 'has-error' : ''}}">
                {!! Form::label('no_of_seats', 'Bus Type: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('bus_type', null, ['class' => 'form-control', 'disabled'=>'disabled', 'id'=>'busType']) !!}
                {!! $errors->first('no_of_seats', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('default_driver_id') ? 'has-error' : ''}}">
                {!! Form::label('driver_id', 'Default Driver', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::select('driver_id', $drivers, $trip->driver->id, ['class' => 'form-control', 'id'=>"driverSelect", 'placeholder' => '-Choose-']) !!}
                    {!! $errors->first('driver_id', '<p class="help-block">:message</p>') !!}
                </div>
            </div>

            <div class="form-group {{ $errors->has('ac') ? 'has-error' : ''}}">
                {!! Form::label('ac', 'Ac: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                                <div class="checkbox">
                <label>{!! Form::radio('ac', '1', $trip->ac == 1) !!} Yes</label>
            </div>
            <div class="checkbox">
                <label>{!! Form::radio('ac', '0', $trip->ac != 1) !!} No</label>
            </div>
                {!! $errors->first('ac', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>
            @if($trip->passport == 1)
            <div class="form-group {{ $errors->has('passport') ? 'has-error' : ''}}">
                {!! Form::label('passport', 'Passport: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                                <div class="checkbox">
                <label>{!! Form::radio('passport', '1', $trip->passport == 1) !!} Yes</label>
            </div>
            <div class="checkbox">
                <label>{!! Form::radio('passport', '0', $trip->passport != 1) !!} No</label>
            </div>
                {!! $errors->first('passport', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>
            @endif
            <div class="form-group {{ $errors->has('active') ? 'has-error' : ''}}">
                {!! Form::label('active', 'Active: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    <div class="checkbox">
                <label>{!! Form::radio('active', '1', $trip->active == 1) !!} Yes</label>
            </div>
            <div class="checkbox">
                <label>{!! Form::radio('active', '0', $trip->active != 1) !!} No</label>
            </div>
                {!! $errors->first('active', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>


           




            
             @if( count($trip->subTrips) > 1 )

             <hr>  

             <h3>Stops</h3>

               @for($i =1; $i <count($trip->subTrips); $i++)
                 
                 <div class="form-group {{ $errors->has('dest_park') ? 'has-error' : ''}}">
                {!! Form::label('subtrip', 'Stop '.($i).":", ['class' => 'col-sm-3 control-label']) !!}
                
                <div class="col-sm-6">
                 {!! Form::text('subtripname'.$i, $assocSubTrips[$i]['destpark']['name'], ['class' => 'form-control', 'disabled'=>'disabled']) !!}
                {!! $errors->first('subtrip'.$i, '<span class="parsley-error-list">:message</span>') !!}
                </div>
                </div>
                <div class=" form-group {{ $errors->has('dest_park') ? 'has-error' : ''}}">
                  {!! Form::label('fare', 'Fare '.$i, ['class' => 'col-sm-3 control-label']) !!}
                  <div class="col-sm-6">
                   {!! Form::text('fare'.$i, $assocSubTrips[$i]['fare'], ['class' => 'form-control']) !!}
                  {!! $errors->first('fare'.$i, '<span class="parsley-error-list">:message</span>') !!}
                  </div>


                </div>

                <div class="form-group {{ $errors->has('ac') ? 'has-error' : ''}}">
                    {!! Form::label('active'.$i, 'Active: ', ['class' => 'col-sm-3 control-label']) !!}
                    <div class="col-sm-6">
                        <div class="checkbox">
                    <label>{!! Form::radio('active'.$i, '1', $assocSubTrips[$i]['active'] == 1) !!} Yes</label>
                </div>
                <div class="checkbox">
                    <label>{!! Form::radio('active'.$i, '0', $assocSubTrips[$i]['active'] != 1) !!} No</label>
                </div>
                    {!! $errors->first('active'.$i, '<span class="parsley-error-list">:message</span>') !!}
                    </div>
                </div>



                <div class="form-group {{ $errors->has('has_round_trip'.$i) ? 'has-error' : ''}}">
                    {!! Form::label('has_round_trip'.$i, 'Activate Round Trip? : ', ['class' => 'col-sm-3 control-label']) !!}
                    <div class="col-sm-6">
                 <div class="checkbox">
                    <label>{!! Form::radio('has_round_trip'.$i, '1', $assocSubTrips[$i]['round_trip_status'] == 1) !!} Yes</label>
                </div>
                <div class="checkbox">
                    <label>{!! Form::radio('has_round_trip'.$i, '0', $assocSubTrips[$i]['round_trip_status'] != 1) !!} No</label>
                </div>
                    {!! $errors->first('has_round_trip'.$i, '<span class="parsley-error-list">:message</span>') !!}
                    </div>
                    
                </div>

                
                
                <div class="form-group {{ $errors->has('round_trip_fare'.$i) ? 'has-error' : ''}}">
                    {!! Form::label('round_trip_fare'.$i, 'Round Trip Fare: ', ['class' => 'col-sm-3 control-label']) !!}
                    <div class="col-sm-6">
                        {!! Form::number('round_trip_fare'.$i, $assocSubTrips[$i]['round_trip_fare'], ['class' => 'form-control']) !!}
                    {!! $errors->first('round_trip_fare'.$i, '<span class="parsley-error-list">:message</span>') !!}
                    </div>
                    
                </div>


                 <div class="form-group {{ $errors->has('is_intl_trip'.$i) ? 'has-error' : ''}}">
                {!! Form::label('is_intl_trip'.$i, 'Is International Trip?: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
             <div class="checkbox">
                <label>{!! Form::radio('is_intl_trip'.$i, '1', $assocSubTrips[$i]['is_intl_trip'] == 1) !!} Yes</label>
            </div>
            <div class="checkbox">
                <label>{!! Form::radio('is_intl_trip'.$i, '0', $assocSubTrips[$i]['is_intl_trip'] != 1) !!} No</label>
            </div>
                {!! $errors->first('is_intl_trip', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>

            <div class="intl_div {{ ($trip->subTrips[0]->is_intl_trip)?'':'collapse' }}" id="intl_div{{ $i }}" >
            <hr>  
             <h3> International Rates</h3>
             <div class="form-group {{ $errors->has('virgin_passport_fare'.$i) ? 'has-error' : ''}}">
                {!! Form::label('virgin_passport_fare'.$i, 'Virgin Passport Fare:', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                     {!! Form::text('virgin_passport_fare'.$i,  $assocSubTrips[$i]['virgin_passport_fare'], ['class' => 'form-control', 'id'=>"vPass"] ) !!}
                     <p class="help-block">Leave blank or 0 is not applicable.</p>
                    {!! $errors->first('virgin_passport_fare'.$i, '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('round_trip_virgin_passport_fare'.$i) ? 'has-error' : ''}}">
                {!! Form::label('round_trip_virgin_passport_fare'.$i, 'Virgin Passport Round Trip Fare:', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                     {!! Form::text('round_trip_virgin_passport_fare'.$i,  $assocSubTrips[$i]['round_trip_virgin_passport_fare'], ['class' => 'form-control', 'id'=>"vPass"] ) !!}
                     <p class="help-block">Leave blank or 0 is not applicable.</p>
                    {!! $errors->first('round_trip_virgin_passport_fare'.$i, '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('no_passport_fare'.$i) ? 'has-error' : ''}}">
                {!! Form::label('no_passport_fare'.$i, 'Id Card/ No Passport Fare:', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                     {!! Form::text('no_passport_fare'.$i,  $assocSubTrips[$i]['no_passport_fare'], ['class' => 'form-control', 'id'=>"noPass"] ) !!}
                     <p class="help-block">Leave blank or 0 is not applicable.</p>
                    {!! $errors->first('no_passport_fare'.$i, '<p class="help-block">:message</p>') !!}
                </div>
            </div>

            <div class="form-group {{ $errors->has('round_trip_no_passport_fare'.$i) ? 'has-error' : ''}}">
                {!! Form::label('round_trip_no_passport_fare'.$i, 'Id Card/ No Passport Round Trip Fare:', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                     {!! Form::text('round_trip_no_passport_fare'.$i,  $assocSubTrips[$i]['round_trip_no_passport_fare'], ['class' => 'form-control', 'id'=>"noPass"] ) !!}
                     <p class="help-block">Leave blank or 0 is not applicable.</p>
                    {!! $errors->first('round_trip_no_passport_fare'.$i, '<p class="help-block">:message</p>') !!}
                </div>
            </div>
             <hr>
            </div>
            

                <script>
                  $(document).ready(function(){

                    $('input:radio[name="is_intl_trip{{ $i }}"]').on('change',function(){
                        // console.log("intl is: "+$(this).val());

                        if($(this).val() == 1)
                        {
                          $("#intl_div{{ $i }}").show(); 
                        }
                        else
                        {
                          $("#intl_div{{ $i }}").hide();
                        }

                    });

                    

                  });
                </script>
                

                <hr> 





               @endfor

               <br/><br/>


            @endif
            
            <!-- added by laolu (logic for adding stops not created at trip creation)-->
             

             <h3>Add New Stop(s)</h3>

               <center><button type="button" class="btn btn-success " data-toggle="modal" data-target="#addStopModal1">
                Add Stop
            </button></center>
              <div>
              </div><br><br>

            
 
            <!-- addition stops here -->
           


                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-3">
                        {!! Form::submit('Update', ['class' => 'btn btn-primary form-control']) !!}
                    </div>
                </div>
                {!! Form::close() !!}

                @if ($errors->any())
                    <ul class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                @endif

             </div> <!-- /.portlet-content -->

          </div> <!-- /.portlet -->

        

        </div> <!-- /.col -->

        <div class="col-md-2 col-sm-4">

            <ul id="myTab" class="nav nav-pills nav-stacked">
            <li class="active">
                {!! Form::open([
                    'method'=>'DELETE',
                    'url' => ['trips', $trip->id],
                    'style' => 'display:inline'
                ]) !!}
                    {!! Form::button('<i class="fa fa-times"></i> Delete Trip', ['class' => 'btn btn-danger btn-xs', 'escape'=>false,'title'=>'Delete', 'type'=>'submit']) !!}
                {!! Form::close() !!}
            </li>
            <li>
              <a href="{{ url('trips') }}">
                <i class="fa fa-bars"></i> 
                List Trips
              </a>
            </li>
            <li class="">
              <a href="{{ url('trips/create') }}">
                <i class="fa fa-plus"></i> 
                Add Trip
              </a>
            </li>
          </ul>

        </div>

      </div> <!-- /.row -->   
<script type="text/javascript">
        $(function () {
          var bus_id = $("#busInput").val();
             // use phone value to perform ajax request
                        $.ajax({
                                url: '{{ route("get-bus-details") }}',
                                dataType: 'text',
                                type: 'GET',
                                contentType: 'application/x-www-form-urlencoded',
                                data: "bus_id="+bus_id,
                                success: function (resp, textStatus, jQxhr)
                                {
                                   var bus = $.parseJSON(resp);   
                                   console.log(bus.number_plate) ;
                                   // $("#driverSelect option[value='"+  +"']").prop('selected', true);
                                   $("#driverSelect").val(bus.default_driver_id);
                                   $("#numberPlate").val(bus.number_plate);
                                   $("#vin").val(bus.vin);
                                   $("#busType").val(bus.bus_type.name);
                                   $("#no_of_seats").val(bus.bus_type.no_of_seats);
                                  // var rsltsArr = rslts.split("|");
                                  //   // DISPLAY THOSE RESULTS
                                  //   $("#contact_name").val(rsltsArr[0]);
                                  //   $("#contact_email").val(rsltsArr[1]);
                                  //   $("#contact_address").val(rsltsArr[2]);
                                  //   $("#next_of_kin").val(rsltsArr[3]);
                                  //   $("#next_of_kin_phone").val(rsltsArr[4]);  
                                },
                                error: function (jqXhr,textStatus, errorThrown)
                                {
                                    console.log(errorThrown);
                                }
                  });

            $("#busInput").on('change', function(){
                console.log($("#busInput").val());
                var bus_id = $("#busInput").val();
                 if(bus_id !="")
                  {
                       // use phone value to perform ajax request
                        $.ajax({
                                url: '{{ route("get-bus-details") }}',
                                dataType: 'text',
                                type: 'GET',
                                contentType: 'application/x-www-form-urlencoded',
                                data: "bus_id="+bus_id,
                                success: function (resp, textStatus, jQxhr)
                                {
                                   var bus = $.parseJSON(resp);   
                                   console.log(bus.number_plate) ;
                                   // $("#driverSelect option[value='"+  +"']").prop('selected', true);
                                   $("#driverSelect").val(bus.default_driver_id);
                                   $("#numberPlate").val(bus.number_plate);
                                   $("#vin").val(bus.vin);
                                   $("#busType").val(bus.bus_type.name);
                                   $("#no_of_seats").val(bus.bus_type.no_of_seats);
                                  // var rsltsArr = rslts.split("|");
                                  //   // DISPLAY THOSE RESULTS
                                  //   $("#contact_name").val(rsltsArr[0]);
                                  //   $("#contact_email").val(rsltsArr[1]);
                                  //   $("#contact_address").val(rsltsArr[2]);
                                  //   $("#next_of_kin").val(rsltsArr[3]);
                                  //   $("#next_of_kin_phone").val(rsltsArr[4]);  
                                },
                                error: function (jqXhr,textStatus, errorThrown)
                                {
                                    console.log(errorThrown);
                                }
                  });
                }
            });

             $("#busInput").on('blur', function(){
                console.log($("#busInput").val());
                var bus_id = $("#busInput").val();
                 if(bus_id !="")
                  {
                       // use phone value to perform ajax request
                        $.ajax({
                                url: '{{ route("get-bus-details") }}',
                                dataType: 'text',
                                type: 'GET',
                                contentType: 'application/x-www-form-urlencoded',
                                data: "bus_id="+bus_id,
                                success: function (resp, textStatus, jQxhr)
                                {
                                   var bus = $.parseJSON(resp);   
                                   console.log(bus.number_plate) ;
                                   // $("#driverSelect option[value='"+  +"']").prop('selected', true);
                                   $("#driverSelect").val(bus.default_driver_id);
                                   $("#numberPlate").val(bus.number_plate);
                                   $("#vin").val(bus.vin);
                                   $("#busType").val(bus.bus_type.name);
                                   $("#no_of_seats").val(bus.bus_type.no_of_seats);
                                  // var rsltsArr = rslts.split("|");
                                  //   // DISPLAY THOSE RESULTS
                                  //   $("#contact_name").val(rsltsArr[0]);
                                  //   $("#contact_email").val(rsltsArr[1]);
                                  //   $("#contact_address").val(rsltsArr[2]);
                                  //   $("#next_of_kin").val(rsltsArr[3]);
                                  //   $("#next_of_kin_phone").val(rsltsArr[4]);  
                                },
                                error: function (jqXhr,textStatus, errorThrown)
                                {
                                    console.log(errorThrown);
                                }
                  });
                }
            });



        });
          
      </script>

      <div class = "modal fade" id="addStopModal1" tabindex = "-1" role = "dialog" aria-labelledby = "myModalLabel" aria-hidden = "true">
   <div class = "modal-dialog">
      <div class = "modal-content">
         <div class = "modal-header">
            <button type = "button" class = "close" data-dismiss = "modal" aria-hidden = "true">
                  &times;
            </button>
            <h4 class = "modal-title" id = "myModalLabel">
               Add Stop
            </h4>
         </div>
         <form action="{{ url('add-new-stop') }}" method="post" id="bookingNotes33">
         <div class = "modal-body" id="contentArea">
              <input type="hidden" id="SubBtn" name="_token" value="<?php echo csrf_token(); ?>">
          <input type="hidden" name="parent_trip_id" value="{{ $trip->id }}">
          <input type="hidden" name="state_id" value="{{ $trip->destpark->state_id }}">
          <input type="hidden" name="source_park_id" value="{{ $trip->sourcepark->id }}">
          
             <div class="form-group {{ $errors->has('dest_park') ? 'has-error' : ''}}">
                {!! Form::label('dest_park_id', 'Destination Park: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    <select name="dest_park_id" class="form-control" required="">
                        <option value="">Choose Park</option>
                        @foreach($all_parks as $park)
                        <option value="{{ $park->id }}">{{ $park->name }}</option>
                        @endforeach
                    </select>
                {!! $errors->first('dest_park_id', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>
            <br style="clear:both;" />

             <div class="form-group {{ $errors->has('dest_park') ? 'has-error' : ''}}">
                {!! Form::label('fare', 'Fare: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    <input type="text" name="fare" class="form-control" required />
                {!! $errors->first('fare', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>

            <br style="clear:both;" />

            <div class="form-group {{ $errors->has('has_round_trip') ? 'has-error' : ''}}">
                {!! Form::label('has_round_trip', 'Activate Round Trip? : ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
             <div class="checkbox">
                <label>{!! Form::radio('has_round_trip', '1') !!} Yes</label>
            </div>
            <div class="checkbox">
                <label>{!! Form::radio('has_round_trip', '0', true) !!} No</label>
            </div>
                {!! $errors->first('has_round_trip', '<span class="parsley-error-list">:message</span>') !!}
                </div>
                
            </div>

            <br style="clear:both;" />`

            
            <div class="form-group {{ $errors->has('round_trip_fare') ? 'has-error' : ''}}">
                {!! Form::label('round_trip_fare', 'Round Trip Fare: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::number('round_trip_fare', null, ['class' => 'form-control']) !!}
                {!! $errors->first('round_trip_fare', '<span class="parsley-error-list">:message</span>') !!}
                </div>
                
            </div>

             <br style="clear:both;" />


            <div class="form-group {{ $errors->has('ac') ? 'has-error' : ''}}">
                {!! Form::label('is_intl_trip0', 'Is International Trip?: ', ['class' => 'col-sm-4 control-label']) !!}
                <div class="col-sm-7">
             <div class="checkbox">
                <label>{!! Form::radio('is_intl_trip0', '1') !!} Yes</label>
            </div>
            <div class="checkbox">
                <label>{!! Form::radio('is_intl_trip0', '0', true) !!} No</label>
            </div>
                {!! $errors->first('is_intl_trip0', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>

            <br style="clear:both;" />

            <div class="intl_div collapse" id="intl_div0">
            <hr>  
             <h3> International Rates</h3>
             <div class="form-group {{ $errors->has('virgin_passport_fare') ? 'has-error' : ''}}">
                {!! Form::label('virgin_passport_fare', 'Virgin Passport Fare:', ['class' => 'col-sm-4 control-label']) !!}
                <div class="col-sm-7">
                     {!! Form::text('virgin_passport_fare',  0.00, ['class' => 'form-control', 'id'=>"vPass"] ) !!}
                     <p class="help-block">Leave blank or 0 is not applicable.</p>
                    {!! $errors->first('virgin_passport_fare', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <br style="clear:both;" />
            <div class="form-group {{ $errors->has('round_trip_virgin_passport_fare') ? 'has-error' : ''}}">
                {!! Form::label('round_trip_virgin_passport_fare', 'Virgin Passport Round Trip Fare:', ['class' => 'col-sm-4 control-label']) !!}
                <div class="col-sm-7">
                     {!! Form::text('round_trip_virgin_passport_fare',  0.00, ['class' => 'form-control', 'id'=>"vPass"] ) !!}
                     <p class="help-block">Leave blank or 0 is not applicable.</p>
                    {!! $errors->first('round_trip_virgin_passport_fare', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <br style="clear:both;" />
            <div class="form-group {{ $errors->has('no_passport_fare') ? 'has-error' : ''}}">
                {!! Form::label('no_passport_fare', 'Id Card/ No Passport Fare:', ['class' => 'col-sm-4 control-label']) !!}
                <div class="col-sm-7">
                     {!! Form::text('no_passport_fare',  0.00, ['class' => 'form-control', 'id'=>"noPass"] ) !!}
                     <p class="help-block">Leave blank or 0 is not applicable.</p>
                    {!! $errors->first('no_passport_fare', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <br style="clear:both;" />

            <div class="form-group {{ $errors->has('round_trip_no_passport_fare') ? 'has-error' : ''}}">
                {!! Form::label('round_trip_no_passport_fare', 'Id Card/ No Passport Round Trip Fare:', ['class' => 'col-sm-4 control-label']) !!}
                <div class="col-sm-7">
                     {!! Form::text('round_trip_no_passport_fare',  0.00, ['class' => 'form-control', 'id'=>"noPass"] ) !!}
                     <p class="help-block">Leave blank or 0 is not applicable.</p>
                    {!! $errors->first('round_trip_no_passport_fare', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
             <hr>
            </div>

            <br style="clear:both;" />

            <script>
                  $(document).ready(function(){

                    $('input:radio[name="is_intl_trip0"]').on('change',function(){
                        // console.log("intl is: "+$(this).val());

                        if($(this).val() == 1)
                        {
                          $("#intl_div0").show(); 
                        }
                        else
                        {
                          $("#intl_div0").hide();
                        }

                    });

                    

                  });
                </script>
            
            
         </div>
         <div class = "modal-footer">
            <button type = "button" class = "btn btn-default" data-dismiss = "modal">
               Close
            </button>
            <button type = "submit" id="" class = "btn btn-primary">
               Save
            </button>
         </form>

         </div>
      </div><!-- /.modal-content -->
   </div>


<script>
  $(document).ready(function(){

    $('input:radio[name="is_intl_trip"]').on('change',function(){
        console.log("intl is: "+$(this).val());

        if($(this).val() == 1)
        {
          $("#intl_div").show(); 
        }
        else
        {
          $("#intl_div").hide();
        }

    });

    

  });
</script>
@endsection