@extends('layouts.master')

@section('content')


    <div class="content-header">
        <h2 class="content-header-title">Trips </h2>
        <ol class="breadcrumb">
          <li><a href="{{ url('/') }}">Dashboard</a></li>
          <li class="active">Trips </li>
        </ol>
      </div> <!-- /.content-header -->




      <div class="row">
        @if (Session::has('status'))
            <div class="alert alert-success" role="alert">
              {!! session('status') !!}
            </div>
        @endif
        

         <div class="col-md-2 col-sm-4 col-offset-md-10">



            <ul id="myTab" class="nav nav-pills nav-stacked">
              <li class="active">
                <a href="{{ url('trips/create') }}">
                  <i class="fa fa-plus"></i>
                  Add New Trip
                </a>
              </li>
              <li class="active">
                  <a href="{{ url('trips/upload') }}">
                    <i class="fa fa-upload"></i>
                    Upload Trips
                  </a>
                </li>
          </ul>

        </div>
        <div class="col-md-2 col-sm-2 col-offset-md-9 pull-right">



        </div>

        <div class="col-md-12 col-sm-8">

          <div class="portlet">

            <div class="portlet-content">

              <div class="table-responsive">

              <table
                class="table table-striped table-bordered table-hover table-highlight table-checkable"
                data-provide="datatable"
                data-display-rows="10"
                data-info="true"
                data-search="true"
                data-length-change="true"
                data-paginate="true"
              >
                  <span id='flash' ></span>
                  <thead>
                    <tr>
                      <th>From</th>
                      <th>To</th>
                      <!-- <th>Departure Time</th> -->
                      <th>Fare</th>
                      <th>Bus</th>
                      <th>Trip Type</th>
                      <th>Driver</th>
                      <th>Active?</th>
                      <th>Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    {{-- */$x=0;/* --}}
                    @foreach($trips['data'] as $item)
                        {{-- */$x++;/* --}}
                        <tr>
                            <td>{{ $item['sourcepark']['name'] }}</td>
                            <td>{{ $item['destpark']['name'] }}</td>
                            <!-- <td>{{ $item['departure_time'] }}</td> -->
                            <td>
                                @if($item['sub_trips'][0]['is_intl_trip'])
                                  <small>Regular: </small>&#8358; {{ number_format($item['sub_trips'][0]['fare']) }}  <br/>
                                  <small>Virgin Passport:</small> &#8358; {{ number_format($item['sub_trips'][0]['virgin_passport_fare']) }}  <br/>
                                  <small>No Passport:</small> &#8358; {{ number_format($item['sub_trips'][0]['no_passport_fare']) }}  <br/>

                                @else
                                &#8358; {{ number_format($item['sub_trips'][0]['fare']) }}

                                @endif


                            </td>

                            <td>{{ (!empty($item['bus']))? $item['bus']['number_plate']:'' }}</td>
                            <td>{{ ($item['sub_trips'][0]['is_intl_trip'])?"International":"Domestic" }}</td>
                            <td>{{ (!empty($item['driver']))?$item['driver']['name']:'' }}</td>
                            <td>{{ ($item['active'])?"Yes":"No" }}</td>

                            <td>


                                <!-- <a type="button" title="Adjust Price"  class="btn btn-default" data-toggle="modal" data-target="#myModal<?php echo $item['id'] ?>"> <i class="fa fa-money"></i> </a> -->


                                 <a href="{{ url('trips/' . $item['id'] . '/edit') }}" title="Edit">
                                    <button type="submit" class="btn btn-default"><i class="fa fa-edit"></i></button>
                                </a>


                               <!--  {!! Form::open([
                                    'method'=>'DELETE',
                                    'url' => ['trips', $item['id']],
                                    'style' => 'display:inline'
                                ]) !!}
                                    {!! Form::button('<i class="fa fa-times"></i>', ['class' => 'btn btn-danger btn-xs', 'onclick'=>'ConfirmDelete()', 'escape'=>false,'title'=>'Delete', 'type'=>'submit']) !!}
                                {!! Form::close() !!} -->


                            </td>
                        </tr>
                        <!-- Modal -->
<div class = "modal fade" id="myModal<?php echo $item['id'] ?>" tabindex = "-1" role = "dialog" aria-labelledby = "myModalLabel" aria-hidden = "true">
   <div class = "modal-dialog">
      <div class = "modal-content">
         <div class = "modal-header">
            <button type = "button" class = "close" data-dismiss = "modal" aria-hidden = "true">
                  &times;
            </button>
            <h4 class = "modal-title" id = "myModalLabel">
               Adjust {{ $item['name'] }} Price
            </h4>
         </div>
         <form action="" method="post">
         <div class = "modal-body" id="contentArea">
           Fare: <input type="text" id="fare{{ $item['id'] }}" value="{{ $item['fare'] }}" class="form-control">
         </div>
         <div class = "modal-footer">
            <button type = "button" class = "btn btn-default" data-dismiss = "modal">
               Close
            </button>
            <button type = "button" id="fareSub{{$item['id'] }}" class = "btn btn-primary">
               Submit changes
            </button>
         </div>
         </form>
      </div><!-- /.modal-content -->
   </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
  <script src="{{ url('bckend/js/libs/jquery-1.10.1.min.js') }}"></script>

<script type="text/javascript">
  $(document).ready(function(){

    $('#fareSub<?php echo $item["id"] ?>').click(function(){
      var fare = $("#fare<?php echo $item['id'] ?>").val()
      var id = "{{ $item['id'] }}"

        $("#contentArea").html('<img src="{{ asset("bckend/img/facebook.gif") }}" width="100px" /> please wait...');

                                var url = "{{ route('adjust-prices') }}";
                                      $.ajax
                                      ({
                                        type: "POST",
                                        url: url,
                                        data: ({ rnd : Math.random() * 100000, "_token":"{{ csrf_token() }}", fare:fare, id:id}),
                                        success: function(response){
                                          console.log(response);
                                          $('#myModal<?php echo $item["id"] ?>').modal('hide')
                                           // location.reload();
                                          $('#update<?php echo $item["id"] ?>').text(fare);
                                          $('#flash').text('Fare successfully updated');

                                          location.reload();

                                        }
                                    });
        })

  })

</script>

                    @endforeach
                  </tbody>
                </table>
              </div> <!-- /.table-responsive -->


            </div> <!-- /.portlet-content -->

          </div> <!-- /.portlet -->

        <!-- Modal -->


        </div> <!-- /.col -->



      </div> <!-- /.row -->





@endsection
