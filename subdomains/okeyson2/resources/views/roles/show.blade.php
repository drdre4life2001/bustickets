@extends('layouts.master')

@section('content')

    <h1>Role</h1>
    <div class="table-responsive">
        <dl class="dl-horizontal">
            
            <dt><?php echo 'Name'; ?></dt>
            <dd>
                <?php echo ($role->name); ?>
                &nbsp;
            </dd>
            
        </dl>

        <fieldset>
        <legend><i class="fa fa-angle-right"></i> Permissions</legend>
        </fieldset>

        @foreach (config('constant.all_permissions') as $group_key => $perms)
        
        <div class="form-group" style="height:auto;overflow:auto" >
            <label class="col-md-3 control-label"><?php echo $group_key; ?></label>
            <div class="col-md-9">
                @foreach ($perms as $key => $value)
                <div class="checkbox">
                    <label>
                        <input type="checkbox" disabled="disabled" name="perm[]"  value="<?php echo $key; ?>" <?php echo (strpos($role->permissions, $key) === FALSE)?'':'checked="checked"'; ?> > 
                            <?php echo $value; ?>
                    </label>
                </div>
                @endforeach
            </div>
        </div>

    @endforeach
    </div>

@endsection