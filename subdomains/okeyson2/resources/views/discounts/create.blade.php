@extends('layouts.master')

@section('content')

   <div class="content-header">
        <h2 class="content-header-title">Add Discount </h2>
        <ol class="breadcrumb">
          <li><a href="{{ url('/') }}">Dashboard</a></li>
          <li><a href="{{ url('discounts') }}">Discounts </a></li>
          <li class="active">Add Discount </li>
        </ol>
      </div> <!-- /.content-header -->

      

      <div class="row">

        <div class="col-md-10 col-sm-8">

          <div class="portlet">

            <div class="portlet-content">

                {!! Form::open(['url' => 'discounts', 'class' => 'form-horizontal']) !!}

                            <div class="form-group {{ $errors->has('age_one') ? 'has-error' : ''}}">
                {!! Form::label('age_one', 'Age from: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    <input type="number" name="age_one" class="form-control" required="required">
                {!! $errors->first('age_one', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>


             <div class="form-group {{ $errors->has('age_two') ? 'has-error' : ''}}">
                {!! Form::label('age_two', 'Age to: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                <input type="number" name="age_two" class="form-control" required="required">
                {!! $errors->first('age_two', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>

            <div class="form-group {{ $errors->has('discount') ? 'has-error' : ''}}">
                {!! Form::label('discount', 'Discount(%): ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    <input type="number" name="discount" class="form-control" required="required">
                {!! $errors->first('discount', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>

            <input type="hidden" name="operator_id" value="{{Auth::user()->operator_id}}">
            
           


                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-3">
                        {!! Form::submit('Create', ['class' => 'btn btn-primary form-control']) !!}
                    </div>
                </div>
                {!! Form::close() !!}

                @if ($errors->any())
                    <ul class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                @endif
             </div> <!-- /.portlet-content -->

          </div> <!-- /.portlet -->

        

        </div> <!-- /.col -->

        <div class="col-md-2 col-sm-4">

            <ul id="myTab" class="nav nav-pills nav-stacked">
            <li class="active">
              <a href="{{ url('discounts') }}">
                <i class="fa fa-bars"></i> 
                List Discounts
              </a>
            </li>
          </ul>

        </div>

      </div> <!-- /.row -->   

@endsection