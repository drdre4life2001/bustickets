@extends('layouts.master')

@section('content')

    <div class="content-header">
        <h2 class="content-header-title">Edit Discount </h2>
        <ol class="breadcrumb">
          <li><a href="{{ url('/') }}">Dashboard</a></li>
          <li><a href="{{ url('discounts') }}">Discounts </a></li>
          <li class="active">Edit Discount </li>
        </ol>
      </div> <!-- /.content-header -->

      

      <div class="row">

        <div class="col-md-10 col-sm-8">

          <div class="portlet">

            <div class="portlet-content">

                {!! Form::model($discount, [
                    'method' => 'PATCH',
                    'url' => ['discounts', $discount->id],
                    'class' => 'form-horizontal'
                ]) !!}

                            <div class="form-group {{ $errors->has('age_one') ? 'has-error' : ''}}">
                {!! Form::label('age_one', 'Age from: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::number('age_one', null, ['class' => 'form-control']) !!}
                {!! $errors->first('age_one', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('age_two') ? 'has-error' : ''}}">
                {!! Form::label('age_two', 'Age to: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::number('age_two', null, ['class' => 'form-control']) !!}
                {!! $errors->first('age_two', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('discount') ? 'has-error' : ''}}">
                {!! Form::label('discount', 'Discount(%): ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::number('discount', null, ['class' => 'form-control']) !!}
                {!! $errors->first('discount', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>


                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-3">
                        {!! Form::submit('Update', ['class' => 'btn btn-primary form-control']) !!}
                    </div>
                </div>
                {!! Form::close() !!}

                @if ($errors->any())
                    <ul class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                @endif

             </div> <!-- /.portlet-content -->

          </div> <!-- /.portlet -->

        

        </div> <!-- /.col -->

        <div class="col-md-2 col-sm-4">

            <ul id="myTab" class="nav nav-pills nav-stacked">
            <li class="active">
                {!! Form::open([
                    'method'=>'DELETE',
                    'url' => ['discounts', $discount->id],
                    'style' => 'display:inline'
                ]) !!}
                    {!! Form::button('<i class="fa fa-times"></i> Delete Discount', ['class' => 'btn btn-danger btn-xs', 'escape'=>false,'title'=>'Delete', 'type'=>'submit']) !!}
                {!! Form::close() !!}
            </li>
            <li>
              <a href="{{ url('discounts') }}">
                <i class="fa fa-bars"></i> 
                List Discounts
              </a>
            </li>
            <li class="">
              <a href="{{ url('discounts/create') }}">
                <i class="fa fa-plus"></i> 
                Add Discount
              </a>
            </li>
          </ul>

        </div>

      </div> <!-- /.row -->   

@endsection