@extends('layouts.master')

@section('content')

    <h1>Payment-method</h1>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>ID.</th> <th>{{ trans('payment-methods.name') }}</th><th>{{ trans('payment-methods.is_fulfilled') }}</th><th>{{ trans('payment-methods.rank') }}</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{ $payment-method->id }}</td> <td> {{ $payment-method->name }} </td><td> {{ $payment-method->is_fulfilled }} </td><td> {{ $payment-method->rank }} </td>
                </tr>
            </tbody>    
        </table>
    </div>

@endsection