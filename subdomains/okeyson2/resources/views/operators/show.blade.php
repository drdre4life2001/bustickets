@extends('layouts.master')

@section('content')

    <h1>Operator</h1>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>ID.</th> <th>Name</th><th>Access Code</th><th>Address</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{ $operator->id }}</td> <td> {{ $operator->name }} </td><td> {{ $operator->access_code }} </td><td> {{ $operator->address }} </td>
                </tr>
            </tbody>    
        </table>
    </div>

@endsection