<?php

return [
    'bus_number' => 'Bus Number',
'number_plate' => 'Number Plate',
'bus_type_id' => 'Bus Type',
'vin' => 'Vin',
'other_details' => 'Other Details',
'default_park_id' => 'Default Park',
];
