<?php

return [
    'trip_date' => 'Trip Date',
'trip_id' => 'Trip Id',
'driver_id' => 'Driver Id',
'vehicle_id' => 'Vehicle Id',
'total_fare' => 'Total Fare',
'driver_allowance' => 'Driver Allowance',
'fuel_expense' => 'Fuel Expense',
'other_expense' => 'Other Expense',
'fulfilled' => 'Fulfilled',
];
