<?php

return [
    'travel_date' => 'Travel Date',
'pickup_location' => 'Pickup Location',
'destination' => 'Destination',
'park_id' => 'Park Id',
'agreed_price' => 'Agreed Price',
'bus_id' => 'Bus Id',
'driver_id' => 'Driver Id',
'contact_name' => 'Contact Name',
'contact_phone' => 'Contact Phone',
'contact_email' => 'Contact Email',
'number_of_passengers' => 'Number Of Passengers',
'status' => 'Status',
'paid_date' => 'Paid Date',
'payment_method_id' => 'Payment Method Id',
'comment' => 'Comment',
];
