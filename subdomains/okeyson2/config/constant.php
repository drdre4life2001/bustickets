<?php

return [


    'all_permissions' => [
    		'Booking' => [
    			'book_trip' =>'Book A Trip',
    			'view_bookings'=>'View Bookings',
				'view_booking_details'=>'View Bookings Details',
				'mark_bookings_paid'=>'Mark Booking as Paid',
				'cancel_booking'=>'Cancel a Booking',
                'view_charter_bookings'=>'View Charter Bookings',
                'charter_bookings'=>'Book a Charter Booking',
				

    		],
    		
    		'Trips' => [
    			'add_trip' =>'Add a Trip',
    			'view_own_park_trips'=>'View Assigned Park Trips',
				'view_all_trips'=>'View All Trips',
				'upload trip'=>'Upload Trips',
				'edit_trip'=>'Edit Trip',
				

    		],

            'Incidents' => [
                'add_incident' =>'Add an Incident',
                'manage_incidents'=>'Manage Incidents',

            ],

            'Manager' => [
                'book_from_any_park' =>'Book From Any Park',
                'book_to_any_park'=>'Book To Any Park',
                'set_total_fare'=>'Set Ticket Prices',
                

            ],

    		'Reports' => [
    			'view_daily_trips_report' =>'View Daily Trips Report',
                'view_drivers_report' =>'View Drivers Report',
                'view_buses_report' =>'View Buses Report',
                'view_manifests_report' =>'View Manifests Report',
    			'view_travelers_report' =>'View Travelers Report',
    			'view_bookings_report' =>'View Bookings Report',
                'view_charter_bookings_report' =>'View Charter Bookings Report',
    			'view_parks_report' =>'View Parks Report',
                'view_ticketers_report' =>'View Ticketers Report',
                'view_payments_report' =>'View Payments Report',
                'view_incidents_report' =>'View Incidents Report',
                'view_other_parks_report' =>'View Other Parks Report',
    			
				
    		],
            'Setup'=>[
                'list_buses'=> 'View Buses',
                'manage_buses'=>'Add/ Edit Buses',
                'list_drivers'=> 'View Drivers',
                'manage_drivers'=>'Add/ Edit Drivers',
                'setup_roles'=>'Setup Roles',
                'setup_users'=>'Setup Users',
                   
            ]

    ]

    	
];
