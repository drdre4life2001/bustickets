<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DailyTripExpense extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'daily_trip_expenses';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['daily_trip_id', 'expense_name', 'amount'];

    public function dailytrip(){
        return $this->belongsTo('App\Models\DailyTrip');
    }

    

}
