<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Discount extends Model
{
	protected $table = 'discounts';


    protected $fillable = ['operator_id', 'age_one', 'age_two', 'discount'];
}
