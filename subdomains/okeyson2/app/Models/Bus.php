<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Bus extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'buses';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'bus_number', 
    'destination_park_state',  
    'operator_id',
    'source_park_state',  
    'source_park',
    'destination_park',
    'default_park',
    'number_plate', 
    'chassis_number', 
    'engine_number', 
    'no_of_seats', 
    'bus_mileage', 
    'bus_type_id',
    'bus_roof', 
    'vin', 
    'other_details', 
    'default_park_id',
    'default_driver_id',
    'default_trip_id',
    'bus_model',
    'other_details'
    ];

    public function default_driver(){

        return $this->belongsTo('App\Models\Driver', 'default_driver_id');
    }

    public function default_park(){

        return $this->belongsTo('App\Models\Park', 'default_park_id');
    }

    public function bus_type(){

        return $this->belongsTo('App\Models\BusType', 'bus_type_id');
    }

    public function default_trip(){

        return $this->belongsTo('App\Models\Trip', 'default_trip_id');
    }



}
