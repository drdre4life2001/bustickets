<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DailyTrip extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'daily_trips';


    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];


    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['trip_date', 'trip_id', 'driver_id', 'bus_id', 'total_fare', 'driver_allowance', 'fuel_expense', 'other_expense', 'status', 'booked_seats', 'total_seats', 'trip_transit', 'departure_time', 'ticketer_id'];

    public function driver(){
        return $this->belongsTo('App\Models\Driver');
    }

    public function trip(){
        return $this->belongsTo('App\Models\Trip');
    }

    public function bus(){
        return $this->belongsTo('App\Models\Bus');
    }

    public function bookings(){
        return $this->hasMany('App\Models\BusBooking');
    }

    public function seats(){
        return $this->hasMany('App\Models\Seat');
    }

    public function ticketer(){
        return $this->belongsTo('App\Models\User');
    }

    public function expenses(){
        return $this->hasMany('App\Models\DailyTripExpense');
        
    }

}
