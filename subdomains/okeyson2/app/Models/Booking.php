<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'bookings';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['trip_id', 'date', 'passenger_count', 'unit_cost', 'final_cost','trip_type', 'status', 'paid_date', 'booking_code','contact_email', 'contact_phone', 'contact_name', 'next_of_kin', 'next_of_kin_phone', 'source', 'user_id','contact_address','gender', 'payment_method_id', 'is_round_trip', 'departure_time', 'luggage_weight','luggage_cost','passport_type','is_flexi', 'seat_no'];
    
    public function trip(){
        return $this->belongsTo('App\Models\SubTrip', 'trip_id');
    }
    
    public function paymentmthd(){
        return $this->belongsTo('App\Models\PaymentMethod', 'payment_method_id');
    }

    public function paymentmethod(){
        return $this->belongsTo('App\Models\PaymentMethod', 'payment_method_id');
    }

    public function transactions(){
        return $this->hasMany('App\Models\Transaction');
    }

    public function passengers(){
        return $this->hasMany('App\Models\Passenger');
    }

    public function daily_trip(){
        return $this->belongsTo('App\Models\DailyTrip');
    }

    public function ticketer(){
        return $this->belongsTo('App\Models\User','user_id');
    }
    public function extra_luggages(){
        return $this->hasMany('App\Models\ExtraLuggage', 'booking_id');
    }
}
