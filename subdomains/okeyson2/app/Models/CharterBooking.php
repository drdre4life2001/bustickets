<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CharterBooking extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'charter_bookings';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['travel_date', 'pickup_location', 'destination', 'park_id', 'agreed_price', 'bus_id','booking_code', 'driver_id', 'contact_name', 'contact_phone', 'number_of_passengers', 'status', 'paid_date', 'payment_method_id','user_id','receipt_no', 'comment'];

    public function bus(){
        return $this->belongsTo('App\Models\Bus');
    }

    public function driver(){
        return $this->belongsTo('App\Models\Driver');
    }

    public function park(){
        return $this->belongsTo('App\Models\Park');
    }

    public function ticketer(){
        return $this->belongsTo('App\Models\User', 'user_id');
    }

}
