<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Seat extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'seats';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['seat_no', 'booking_id', 'default_trip_id'];

     public function booking(){

        return $this->belongsTo('App\Models\Booking');
    }

}
