<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;



class Passenger extends Model
{
    protected $fillable = ['name', 'booking_id', 'age', 'gender', 'address','infant','seat_no','daily_trip_id'];

	
}
