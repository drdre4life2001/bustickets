<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'last_name', 'first_name', 'email', 'password', 'phone', 'address', 'active', 'role_id'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function role(){
        return $this->belongsTo('App\Models\Role', 'role_id');
    }

    public function parks(){
        return $this->belongsToMany('App\Models\Park', 'users_parks', 'user_id', 'park_id');
    }

    public function destinations(){
        return $this->belongsToMany('App\Models\Park', 'users_destinations', 'user_id', 'park_id');
    }

    public function logs(){
        return $this->hasMany('App\Models\UserLog', 'user_id');
    }

    public function bookings(){
        return $this->hasMany('App\Models\BusBooking', 'user_id');
    }
}
