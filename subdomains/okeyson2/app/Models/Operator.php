<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Operator extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'operators';

    protected $fillable = ['name', 'access_code', 'address', 'contact_person', 'phone', 'email', 'details', 'website', 'img', 'commission', 'portal_link', 'active'];


    public function trip(){
        return $this->hasMany('App\Models\Trip');
    }

    public function settings(){
        return $this->belongsTo('App\Models\Setting');
    }
}

