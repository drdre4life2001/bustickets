<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Agent extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'agents';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'username', 'email', 'password', 'phone', 'active', 'address', 'park_id', 'device_id'];

     

    public function park(){
        return $this->belongsTo('App\Models\Park', 'park_id');
    }

}
