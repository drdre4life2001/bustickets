<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\SubTrip;
use App\Models\Trip;
use App\Models\DailyTrip;
use App\Models\DailyTripExpense;
use App\Models\Driver;
use App\Models\Discount;
use App\Models\Bus;
use App\Models\Park;
use App\Models\Booking;
use App\Models\Seat;
use App\Models\Setting;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;
use Auth;

class DailyTripsController extends Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth');
    }
    
    public function getScheduleDriverDetails(Request $request)
    {
       // dd($request->all());
       $b = Bus::findOrFail($request->bus_id);
       return $b->default_driver_id;        
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {

        $start_date = date('Y-m-d 00:00:00');
        $end_date = date('Y-m-d 23:59:59');
        $discounts;

         $track_discounts = ($this->settings->discount_active == 'true')?TRUE:FALSE;
         if ($track_discounts) {
              $discounts = Discount::where('operator_id', Auth::user()->operator_id)->get();
         }
         

        if(isset($request->daterange)){


            $arr = explode(' ', $request->daterange);
            $start_date = $arr[0].' 00:00:00';
            $end_date = $arr[1].' 23:59:59';
        }


        $dailytrips = DailyTrip::with(['driver', 'trip.sourcepark', 'trip.destpark', 'trip.subTrips.destpark','bus','ticketer', 'bookings'=>function($query){ $query->where('status', 'PAID'); }])
                        ->where('trip_date', '>=', $start_date)
                        ->where('trip_date', '<=', $end_date)
                        ->orderBy('trip_date', 'desc')->paginate(100);

        $buses = Bus::orderBy('bus_number', 'asc')->lists('number_plate', 'id')->toArray();
        $drivers = Driver::orderBy('name', 'asc')->lists('name', 'id')->toArray();

        $statuses = ['LOADING'=>'LOADING', 'TRANSITED'=>'TRANSITED', 'POSTPONED'=>'POSTPONED', 'CANCELLED'=>'CANCELLED'];

        $transits = ['First Bus'=>'First Bus', 'Second Bus'=>'Second Bus', 'Third Bus'=>'Third Bus', 'Fourth Bus'=>'Fourth Bus', 'Fifth Bus'=>'Fifth Bus', 'Sixth Bus'=>'Sixth Bus', 'Seventh Bus'=>'Seventh Bus', 'Eigth Bus'=>'Eigth Bus'];

        $page_title = 'trip schedules';
        $dt_expense_headers = explode(',', $this->settings->daily_trip_expense_headers);
        
        
        // dump($dailytrips->toArray());
        //dd($dailytrips);

        return view('daily-trips.index', compact('dailytrips', 'page_title', 'buses','drivers', 'statuses', 'start_date', 'end_date', 'transits','destCountArray','dt_expense_headers', 'track_discounts', 'discounts'));
    }


    public function driversDestRport(Request $request, $park_id = '')
    {

        $start_date = date('Y-m-d 00:00:00');
        $end_date = date('Y-m-d 23:59:59');

        if(isset($request->daterange)){


            $arr = explode(' ', $request->daterange);
            $start_date = $arr[0].' 00:00:00';
            $end_date = $arr[1].' 23:59:59';
        }


        $dailytrips = DailyTrip::with('driver', 'trip.sourcepark', 'trip.destpark', 'trip.subTrips.destpark','bus')
                        ->where('trip_date', '>=', $start_date)
                        ->where('trip_date', '<=', $end_date)
                        ->orderBy('trip_date', 'desc')->paginate(100);

        $page_title = 'Drivers Destination Report';
        $parks = Auth::user()->parks()->lists('parks.name', 'parks.id')->toArray();


        return view('daily-trips.drivers_dest_report', compact('dailytrips', 'page_title', 'start_date', 'end_date', 'parks', 'park_id'));
    }


    public function tripSchedules(Request $request, $park_id = '')
    {

        $start_date = date('Y-m-d 00:00:00');
        $end_date = date('Y-m-d 23:59:59');

        if(isset($request->daterange)){


            $arr = explode(' ', $request->daterange);
            $start_date = $arr[0].' 00:00:00';
            $end_date = $arr[1].' 23:59:59';
        }


        $park_name = 'ALL';

        if(!empty($park_id)){
            $park_name = Park::find($park_id)->name;
            
        }


        $trips = Trip::with(['sourcepark', 'destpark', 'subTrips.destpark','bus', 'driver',
                            'dailyTrips' =>function($query) use ($start_date, $end_date){
                                $query->where('trip_date', '>=', $start_date)
                                ->where('trip_date', '<=', $end_date);
                            },  
                            'dailyTrips.bus','dailyTrips.driver'])
                        ->where('active', 1);

        if(!empty($park_id))                
            $trips = $trips->where('source_park_id',  $park_id);                        


        $trips = $trips->orderBy('updated_at', 'desc')->get();

        // dump($trips->toArray());                        

        $buses = Bus::lists('number_plate', 'id')->toArray();
        $drivers = Driver::lists('name', 'id')->toArray();

        $page_title = 'trip schedules';
        $parks = Park::where('boardable', 1)->lists('name', 'id')->toArray();

        // dump($dailytrips->toArray());
        $trips_count = Trip::where('active', 1)->count();
        $transits = ['First Bus'=>'First Bus', 'Second Bus'=>'Second Bus', 'Third Bus'=>'Third Bus', 'Fourth Bus'=>'Fourth Bus', 'Fifth Bus'=>'Fifth Bus'];

        return view('daily-trips.schedules', compact('trips', 'page_title', 'buses','drivers', 'statuses', 'start_date', 'end_date', 'trips_count', 'park_id', 'parks', 'transits', 'park_name'));
    }

    public function populateEmptyTrips($date){

        $trips = Trip::with('bus.bus_type')->where('active', 1)->get();
        foreach ($trips as $trip) {
            
            //checking if trip exists...
            $dt = DailyTrip::where('trip_id', $trip->id)->where('trip_date', date('Y-m-d', strtotime($date)))->first(); 
            if(empty($dt)){ // nope, then populate

                $inserts = [
                        'trip_id'=>$trip->id,
                        'trip_date'=>date('Y-m-d', strtotime($date)),
                        'booked_seats'=>0,
                        'total_seats'=>$trip->bus->no_of_seats,
                        'trip_transit'=>'First Bus',
                        // 'departure_time'=>$trip->departure_time,

                    ];
                $driver_id = $bus_id = '';
                if(!empty($trip->bus)){
                    $inserts['driver_id'] = $trip->bus->default_driver_id;
                    $inserts['bus_id'] = $trip->bus->id;
                }

                DailyTrip::firstOrCreate($inserts);

            }

        }


        return back()->withInput();


    }

    public function translatePrevTrip($date){

        $prevDay = date('Y-m-d', strtotime($date.' - 1 day'));

        //find prev day's trips
        $dts = DailyTrip::where('trip_date', $prevDay)->get(); 
        foreach ($dts as $dt) {
            
            //checking if trip exists...
            $dtas = DailyTrip::where('trip_id', $dt->trip_id)->where('trip_date', date('Y-m-d', strtotime($date)))->get(); 
            foreach ($dtas as $key => $dta) {
              
            // if(empty($dta)){ // nope, then populate

                $inserts = [
                        'trip_id'=>$dt->trip_id,
                        'trip_date'=>date('Y-m-d', strtotime($date)),
                        'booked_seats'=>0,
                        'total_seats'=>$dt->total_seats,
                        'driver_id'=>$dt->driver_id,
                        'bus_id'=>$dt->bus_id,
                        'trip_transit'=>$dt->trip_transit,
                        // 'departure_time'=>$dt->departure_time,

                    ];
                DailyTrip::firstOrCreate($inserts);

            }

        }


        return back()->withInput();


    }

    public function tripScheduleExists($d)
    {
     
      // check if bus has been selected before for same
      $busid = $d['bus_id'];
      $driverid = $d['driver_id'];
      $transit = $d['trip_transit'];

      $busNumber = Bus::where('id',$busid)->get(['bus_number'])->toArray();
      $driverName = Driver::where('id',$driverid)->get(['name'])->toArray();

      // get trip with same bus id, driver id a as created if exists, flag error
        $existingDt = DailyTrip::with('bus', 'trip.sourcepark', 'trip.destpark')
                        ->where('trip_date', $d['trip_date'])
                        ->where('bus_id', $busid)
                        ->where('driver_id', $driverid)
                        ->first(); 
                      
         return count($existingDt);
    }

    public function removeBusFromSchedule($dtid,$date)
    {

      $dt = DailyTrip::findOrFail($dtid);
      
      // get the bus concerned from the dailytrip id   (bus_id)
       $dt2 = DailyTrip::with('bus', 'trip.sourcepark', 'trip.destpark')
                        ->where('trip_date', $date)
                        ->where('bus_id', $dt['bus_id'])
                        ->where('driver_id', $dt['driver_id'])
                        ->first();
      
      $status = $this->tripScheduleExists($dt2);
      
      //remove the bus from the schedule
      if($this->tripScheduleExists($dt2) > 0 )
      {
          
      }

      
      
      
      // refresh the page

    }

    public function addTripSchedule(Request $request, $id =null){


      $d = $request->all();
       // check if bus has been selected before for same
      $busid = $d['bus_id'];
      $driverid = $d['driver_id'];
      $transit = $d['trip_transit'];

      $busNumber = Bus::where('id',$busid)->get(['bus_number']);
      $driverName = Driver::where('id',$driverid)->get(['name']);
   

      //
      // check if bus has been selected before for same
      
      // if($this->tripScheduleExists($d) > 0 )
      if( false )
      {
        Session::flash('error', 'Trip Schedule for '.$busNumber[0]['bus_number'] ." and driver ".$driverName[0]['name']. " already exists");
         return back()->withInput();
      } 

      else
      {

      unset($d['_token']);
      
      if(!empty($id)){


        $dt = DailyTrip::with('bus', 'trip.sourcepark', 'trip.destpark')->where('id', $id)->first();

        $fmr_bus_num = $dt->bus->bus_number;

        $dt->update($d);
        $dt = DailyTrip::with('bus', 'trip.sourcepark', 'trip.destpark')->where('id', $id)->first();

        $new_bus_num = $dt->bus->bus_number;

        // dump($dt->bookings())

        foreach ($dt->bookings()->get() as $b) {

            if($b->status != 'PAID')
                continue;

            $booked_seats = '';
            foreach ($b->seats()->get() as $seat) {
               $booked_seats .= $seat->seat_no.',';
            }
            $booked_seats = substr($booked_seats, 0, strlen($booked_seats) - 1);

            $msg = "OKEYSON PURCHASE NOTICE\nDear ".$b->contact_name.", this is to inform you that the bus for your trip from ".$dt->trip->sourcepark->name." to ".$dt->trip->destpark->name." on ". date('M dS', strtotime($b->date)) ." has been changed from ".$fmr_bus_num." to ".$new_bus_num.". Your seat number(s): ".$booked_seats." and other info remain same. Contact:012905460";

            // dump($msg);

            //send sms here....
            $this->send_sms($b->contact_phone, $msg);    
                           
        }       

        // dd('got here');    

      

  }else
        DailyTrip::FirstOrCreate($d);

       return back()->withInput();

    }

}




    public function manifest($id)
    {
        $op =$this->operator->name;
        // get operator details
        $opImg = $this->operator->img;
        
        /*$seats = Seat::with(['booking.daily_trip'
        ])->where('daily_trip_id', $id)->orderBy('seat_no')->get(); */
        
        //dd($seats);
        
        $dailytrip = DailyTrip::with(['driver', 'trip.sourcepark', 'trip.destpark', 'trip.busType', 'bus',
            'bookings'=>function($query){ $query->where('status','=', 'PAID'); },
            'bookings.passengers', 'bookings.trip.destpark','bookings.seats'
                                     ])->find($id);
        
        if(empty($dailytrip)){
            return redirect('daily-trips');
        }
        //Pass
        
        $excludePassenger = Discount::where('operator_id', Auth::user()->operator_id)->where('discount', 100)->first();
        //dd($dailytrip);

      // dd($dailytrip->expenses()->where('expense_name','Driver allowance')->first()->amount);
        // trim the dailytrip to reflect  only paid bookings

        //$Array = array();
        //$Array = array();
        $passengerObj = [];
        // $i =0;
        //dd($dailytrip->bookings);
         foreach($dailytrip->bookings as $booking)
         {
              
                  //dd($booking);
                  $children = [];
                  
                  $passengerObj[$booking->seat_no] = $booking;
                  $passengerObj[$booking->seat_no]->destPark = $booking->trip->destpark->name;
             //dump('All '. $booking);
                  foreach($booking->passengers as $passenger) {
                      $passenger->contact_name = $passenger->name;
                      $passenger->contact_phone = $booking->contact_phone;
                      $passenger->contact_address = $booking->contact_address;
                      $passenger->destPark = $booking->trip->destpark->name;
                      $passenger->next_of_kin_phone = $booking->next_of_kin_phone;
                      $passenger->next_of_kin = $booking->next_of_kin;
                      //dump('One '.$passenger);
                      if(($passenger->age >= $excludePassenger->age_one) && ($passenger->age <= $excludePassenger->age_two)){
                          //$passengerObj[$booking->seat_no]->children = [$passenger->id => $passenger];
                          array_push($children,$passenger);
                        //dump('First').  dump($passengerObj[$booking->seat_no]->children);
                          //continue;
                      }else{
                    $passengerObj[$passenger->seat_no] = $passenger;
                    //$Array[] = array('passenger' => $booking->contact_name);
                          //dump('Second'). dump($passengerObj[$booking->seat_no]->children);
                      }
                      
                  }
             
              $passengerObj[$booking->seat_no]->children = $children;
             //dump($children);
         }
        
        ksort($passengerObj);
        
        //dd($passengerObj);

        //dd($dailytrip->bookings->seats);
         $dt = SubTrip::with('parenttrip')->where('id',$dailytrip->trip_id)->get(['parent_trip_id']); 
         // $pTripId = $dt[0]['parent_trip_id'];
         // $tripDet = Trip::findOrFail($pTripId)->first();
         // $depTime = $tripDet->departure_time;  
         // // use parent trip id  
         //  $typeArr = explode(" ",$dailytrip->bus->bus_type->name);
         //  $type = $typeArr[0];

         // dump($dailytrip->toArray());
        
        
        $page_title = 'View Trip Manifest';
        return view('daily-trips.manifest', compact('dailytrip','op','opImg','passengerObj', 'excludePassenger', 'page_title', 'seats'));
    }

    public function generateTicketerReport($id)
    {
        $op =$this->operator->name;
        // get operator details
        $opImg = $this->operator->img;
        $dailytrip = DailyTrip::with(['driver', 'trip.sourcepark','trip.subTrips', 'trip.destpark', 'trip.busType', 'bus',
            'bookings'=>function($query){ $query->where('status', 'PAID'); },
            'bookings.passengers', 'bookings.trip.destpark','bookings.seats'])->find($id);
       
        $destArray = array();
        $fares = array();

        
        foreach($dailytrip->bookings as $b)
        {   
            $destArray[] = $b->trip->destpark->name;  
        }
                //sort the destination array by distinct values
        $destCountArray = array_count_values($destArray);
        foreach($dailytrip->bookings as $b)
        {   
            $destArray[] = $b->trip->destpark->name;  
        }
       // dd($destCountArray);
        // remaining seats
        $remSeats = $dailytrip->total_seats - $dailytrip->booked_seats;
        $totalFare = $dailytrip->total_fare;
        $driverAllw = $dailytrip->driver_allowance;
        $bookedSeats = $dailytrip->booked_seats;
        $other= $dailytrip->other_expense;

        if(empty($dailytrip))
            return redirect('daily-trips');
        $page_title = 'Ticketer Report';
        return view('daily-trips.ticketers_report', compact('dailytrip','op','opImg','type','page_title','depTime','destCountArray','remSeats','bookedSeats','totalFare','driverAllw','other'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $page_title = 'Add dailytrip';

        return view('daily-trips.create', compact('page_title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, ['trip_date' => 'required', 'trip_id' => 'required', ]);

        DailyTrip::create($request->all());

        Session::flash('flash_message', 'DailyTrip added!');

        return redirect('daily-trips');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function show($id)
    {
        $dailytrip = DailyTrip::findOrFail($id);

        $page_title = 'View dailytrip';
        return view('daily-trips.show', compact('dailytrip', 'page_title'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $dailytrip = DailyTrip::findOrFail($id);

        $page_title = 'Edit dailytrip';
        return view('daily-trips.edit', compact('dailytrip', 'page_title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
   public function sendSms($phone, $msg)
    { 
        
       //settings
        $cid ="";
        $user = "dejjy";
        $senderArray = explode(" ", $this->operator->name);
        $senderId = $senderArray[0];
        $to = $phone;
        $pass ="P@55W0rd";   
       $ch = curl_init();
       $postdata = 'user='.$user.'&pass='.$pass.'&from='.$senderId.'&to='.$to.'&msg='.$msg; //initialize the request variable
     // echo $postdata;

       $url = 'http://cloud.nuobjects.com/api/send/'; //this is the url of the gateway's interface
        $ch = curl_init(); //initialize curl handle
        curl_setopt($ch, CURLOPT_URL, $url); //set the url
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1); //return as a variable
        curl_setopt($ch, CURLOPT_POST, 1); //set POST method
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata); //set the POST variables
        $response = curl_exec($ch); // grab URL and pass it to the browser. Run the whole process and return the response
        curl_close($ch); //close the curl handle

        $strHeader = get_headers($url)[0];

        $statusCode = substr($strHeader, 9, 3 );
       
       return $statusCode;

    }

    public function update($id, Request $request)
    {
        
        //expenses....
        if(!empty($request->update_expenses)){



            $dt_expense_headers = explode(',', $this->settings->daily_trip_expense_headers);
            foreach ($dt_expense_headers as $h) {
                $hr = str_slug($h, '_');

                $amount = $request->$hr;

                $dte = DailyTripExpense::where('daily_trip_id', $id)
                                ->where('expense_name', $h)->first();

                if(!empty($amount) || !empty($dte)) {
                    
                    if(empty($dte))
                      $dte = DailyTripExpense::FirstOrCreate([
                          'daily_trip_id'=>$id,
                          'expense_name'=>$h
                      ]);

                    // dump($dte);

                    $dte->amount = $amount;
                    $dte->save();
    
                }
                
            }


            //update status to transited
            $dt = DailyTrip::find($id);
            $dt->status = 'TRANSITED';
            $dt->save();


            // dd($request->all());



            Session::flash('flash_message', 'DailyTrip updated!');

            return back()->withInput();
        }



        // if id's are different, send sms to both drivers
        if(isset($request->fmrDriver) && isset($request->driver_id) )
        {
            $formerDriver = $request->fmrDriver;
            $newDriver = $request->driver_id;
            
            if($formerDriver != $newDriver)
            {
                // get former driver details
                $driverDets = Driver::findOrFail($formerDriver);
                $fmrDrvphone = $driverDets->phone;
                // get new driver details
                $driverDets2 = Driver::findOrFail($newDriver);
                $newDrvphone = $driverDets2->phone;
                // get dailytrip details in order to send message
                $dtDetails = DailyTrip::findOrFail($id);
                // get bus on trip
                $busId = $dtDetails->bus_id; 
                //get bus details
                $busDetails =Bus::findOrFail($busId);
                $busNumber = $busDetails->number_plate;
                $tripDetails = Trip::findOrFail($dtDetails->trip_id);

                // departure time
                $depTime =$tripDetails->trip_transit; 
                $tripDet = explode("->",$tripDetails->name);
                // get source and destination
                $trip = $tripDet[0];
                $tripBreakdown = explode("__", $trip);
                $from = $tripBreakdown[0];
                $to = $tripBreakdown[1];
                
                if($newDrvphone != "" && $newDrvphone != "NULL" )
                {
                $newdrivermsg = "Trip Schedule Notice Trip: from ". $from." to ".$to. " Bus to Drive: ".$busNumber." (".$depTime.") Thanks";
                // send sms to new driver
                $this->sendSms($newDrvphone,$newdrivermsg);
                }
                
                if($fmrDrvphone != "" && $fmrDrvphone != "NULL" )
                {
                // old driver message
                 $olddrivermsg = "Trip Schedule Notice. Bus ". $busNumber ." has been re-scheduled for another driver for trip from ".$from ."to ".$to. " thanks";
                // send sms to old driver
                $this->sendSms($fmrDrvphone,$olddrivermsg);
                }
            }
        }
       
        // caters for changing bus details
        if(isset($request->fmrBus) && isset($request->bus_id) )
        {
           $formerBus = $request->fmrBus;
           $newBus = $request->bus_id;

           if($formerBus != $newBus)
           {
             // get dailytrip details in order to send message
                $dtDetails = DailyTrip::with('driver', 'trip.sourcepark', 'trip.destpark', 'trip.busType', 'bus', 'bookings.passengers', 'bookings.trip.destpark')->find($id);
                
                $source = $dtDetails->trip->sourcepark['name'];
                $dest = $dtDetails->trip->destpark['name'];
                $bus = Bus::findOrFail($newBus);
               $busNumber = $bus['number_plate'];
                
            $passengers = $dtDetails->bookings;
            $i=0;
            foreach ($passengers as $key => $value) 
            {
                if($i==0)
                {
                    $phone = $passengers[$key]->contact_phone;
                    if($phone != "NULL" && $phone != "")
                    {
                     $psgMsg = $this->operator->name. " trip notice: "."Your trip from ".$source. " to ".$dest ." is now on bus ". $busNumber." Thanks";
                  // send msg to phone
                     $this->sendSms($phone,$psgMsg);
                    }
                }
            }

           }

           // send sms to both drivers
        
                // get former driver details
                $driverDets = Driver::findOrFail($formerBus);
                $fmrDrvphone = $driverDets->phone;
                // get new driver details
                $driverDets2 = Driver::findOrFail($newBus);
                $newDrvphone = $driverDets2->phone;
                // get dailytrip details in order to send message
                $dtDetails = DailyTrip::findOrFail($id);
                // get bus on trip
                $busId = $dtDetails->bus_id; 
                //get bus details
                $busDetails =Bus::findOrFail($busId);
                $busNumber = $busDetails->number_plate;
                $tripDetails = Trip::findOrFail($dtDetails->trip_id);
                // departure time
                $depTime =$tripDetails->trip_transit; 
                $tripDet = explode("->",$tripDetails->name);
                // get source and destination
                $trip = $tripDet[0];
                $tripBreakdown = explode("__", $trip);
                $from = $tripBreakdown[0];
                $to = $tripBreakdown[1];
                
                if($newDrvphone != "" && $newDrvphone != "NULL" )
                {
                $newdrivermsg = "Trip Schedule Notice Trip: from ". $from." to ".$to. " Bus to Drive: ".$busNumber." (".$depTime.") Thanks";
                // send sms to new driver
                $this->sendSms($newDrvphone,$newdrivermsg);
                }
                
                if($fmrDrvphone != "" && $fmrDrvphone != "NULL" )
                {
                // old driver message
                 $olddrivermsg = "Trip Schedule Notice. Bus ". $busNumber ." has been re-scheduled for another driver for trip from ".$from ."to ".$to. " thanks";
                // send sms to old driver
                $this->sendSms($fmrDrvphone,$olddrivermsg);
                }



            $dt = DailyTrip::with('bus', 'trip.sourcepark', 'trip.destpark')->where('id', $id)->first();
            
            $fmr_bus = Bus::findOrFail($formerBus);
            $fmr_bus_num = $fmr_bus->bus_number;

            $new_bus = Bus::findOrFail($newBus);
            $new_bus_num = $new_bus->bus_number;

            // dump($dt->bookings())

            foreach ($dt->bookings()->get() as $b) {

                if($b->status != 'PAID')
                    continue;

                $booked_seats = '';
                foreach ($b->seats()->get() as $seat) {
                   $booked_seats .= $seat->seat_no.',';
                }
                $booked_seats = substr($booked_seats, 0, strlen($booked_seats) - 1);

                $msg = "OKEYSON PURCHASE NOTICE\nDear ".$b->contact_name.", this is to inform you that the bus for your trip from ".$dt->trip->sourcepark->name." to ".$dt->trip->destpark->name." on ". date('M dS', strtotime($b->date)) ." has been changed from ".$fmr_bus_num." to ".$new_bus_num.". Your seat number(s): ".$booked_seats." and other info remain same. Contact:012905460";

                // dump($msg);

                //send sms here....
                $this->send_sms($b->contact_phone, $msg);    
                               
            }           
        
        }


        // $this->validate($request, ['trip_date' => 'required', 'trip_id' => 'required', ]);
       
        $dailytrip = DailyTrip::findOrFail($id);

        //updating total seat number....
        if(!empty($request->bus_id)){
            $b = Bus::with('bus_type')->find($request->bus_id);
            $dailytrip->total_seats = $b->no_of_seats;
        }

        

        $rd = $request->all();

        if(Auth::user()->role->name = 'Ticketer')
          $rd['ticketer_id'] = Auth::user()->id;

        // dd($rd);


        $dailytrip->update($rd);

        Session::flash('flash_message', 'DailyTrip updated!');

        // return redirect('daily-trips');
        return back()->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        DailyTrip::destroy($id);

        Session::flash('flash_message', 'DailyTrip deleted!');

        // return redirect('daily-trips');
        return back()->withInput();
    }
   

   public function getTripDets(Request $request)
    {
        $tripid =  $request->id;
        $date = $request->date;
        $DailytripDetails = DailyTrip::where('trip_id',$tripid)->where('trip_date',$date)->get();
     
        foreach ($DailytripDetails as $t)
        {
          // send smss to all
          $driverid = $t->driver_id;
           $busid = $t->bus_id;
          // get driver's name
          $driver = Driver::findOrFail($driverid);
          $driverPhone = $driver->phone;
          $driverName = $driver->name;
          // get bus details
          $bus = Bus::findOrFail($busid);
          $busNumber = $bus->bus_number;
          // get trip details
          $trip = Trip::findOrFail($tripid);
          // get park details
          $source = Park::find($trip->source_park_id);
          $dest = Park::find($trip->dest_park_id);
          $sourcename = $source->name;
          $destname = $dest->name;

          $msg = "Hi, ".$driverName ." Your next trip would be on ".date("D, M j Y", strtotime($date)) . " from ".$sourcename. " to ".$destname. " with bus number ".$busNumber. " (". $t->trip_transit.")";

          $ans = $this->send_sms($driverPhone, $msg);

        }

        return $ans;
            

    }

}
