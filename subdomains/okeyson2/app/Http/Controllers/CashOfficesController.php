<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\CashOffice;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;

class CashOfficesController extends Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth');
    }    /**

    
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $cash_offices = CashOffice::paginate(400);

        $page_title = 'Cash Offices';

        return view('cash-offices.index', compact('cash_offices', 'page_title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $page_title = 'Add cashoffice';

        return view('cash-offices.create', compact('page_title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, ['name' => 'required', ]);

        CashOffice::create($request->all());

        Session::flash('flash_message', 'CashOffice added!');

        return redirect('cash-offices');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function show($id)
    {
        $cashoffice = CashOffice::findOrFail($id);

        $page_title = 'View cashoffice';
        return view('cash-offices.show', compact('cashoffice', 'page_title'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $cashoffice = CashOffice::findOrFail($id);

        $page_title = 'Edit cashoffice';
        return view('cash-offices.edit', compact('cashoffice', 'page_title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        $this->validate($request, ['name' => 'required', ]);

        $cashoffice = CashOffice::findOrFail($id);
        $cashoffice->update($request->all());

        Session::flash('flash_message', 'CashOffice updated!');

        return redirect('cash-offices');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        CashOffice::destroy($id);

        Session::flash('flash_message', 'CashOffice deleted!');

        return redirect('cash-offices');
    }

}
