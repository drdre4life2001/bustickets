<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Agent;
use App\Models\Park;
use App\Models\Device;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;

class AgentsController extends Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $agents = Agent::with('park')->paginate(1000);

        $page_title = 'agents';

        return view('agents.index', compact('agents', 'page_title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $page_title = 'Add agent';

        $devices = Device::get();
        $parks = Park::get();

        $agents = Agent::get();
        return view('agents.create', compact('page_title', 'agents', 'devices', 'parks'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $d = Device::find($request->device);
        if(!empty($d->assigned_to))
            return redirect()->back()->with('flash_message', 'Device assigned to someone already');

        $agent = Agent::create($request->all());
        $res =Device::where('id', $request->device)
                    ->update(['assigned_to' => $agent->id]);
        Session::flash('flash_message', 'Agent added!');
        return redirect('agents');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function show($id)
    {
        $agent = Agent::findOrFail($id);
        $page_title = 'View agent';
        return view('agents.show', compact('agent', 'page_title'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $agent = Agent::findOrFail($id);
        $devices = Device::get();
        $parks = Park::get();
        $page_title = 'Edit agent';
        return view('agents.edit', compact('agent', 'page_title', 'devices', 'parks'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        $agent = Agent::findOrFail($id);
        $agent->update($request->all());   
        Session::flash('flash_message', 'Agent updated!');
        return redirect('agents');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        Agent::destroy($id);
        Session::flash('flash_message', 'Agent deleted!');
        return redirect('agents');
    }

}
