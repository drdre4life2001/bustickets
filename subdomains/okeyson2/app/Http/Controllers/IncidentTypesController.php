<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use App\Models\IncidentType;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;

class IncidentTypesController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $incidenttypes = IncidentType::paginate(100);

        $page_title = 'incidenttypes';

        return view('incident-types.index', compact('incidenttypes', 'page_title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $page_title = 'Add incidenttype';

        return view('incident-types.create', compact('page_title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, ['name' => 'required', ]);

        $request['operator_id'] = Auth::user()->operator_id;

        IncidentType::create($request->all());

        Session::flash('flash_message', 'IncidentType added!');

        return redirect('incident-types');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function show($id)
    {
        $incidenttype = IncidentType::findOrFail($id);

        $page_title = 'View incidenttype';
        return view('incident-types.show', compact('incidenttype', 'page_title'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $incidenttype = IncidentType::findOrFail($id);

        $page_title = 'Edit incidenttype';
        return view('incident-types.edit', compact('incidenttype', 'page_title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        $this->validate($request, ['name' => 'required', ]);

        $incidenttype = IncidentType::findOrFail($id);
        $incidenttype->update($request->all());

        Session::flash('flash_message', 'IncidentType updated!');

        return redirect('incident-types');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        IncidentType::destroy($id);

        Session::flash('flash_message', 'IncidentType deleted!');

        return redirect('incident-types');
    }

}
