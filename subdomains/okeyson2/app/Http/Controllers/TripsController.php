<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Illuminate\Support\Facades\Input;
use App\Models\Trip;
use App\Models\SubTrip;
use App\Models\DailyTrip;
use App\Models\TripStop;
use App\Models\Park;
use App\Models\State;
use App\Models\Operator;
use App\Models\BusType;
use App\Models\Bus;
use App\Models\Driver;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;
use Auth;
use Excel;

class TripsController extends Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth');
    }



    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $page_title = 'trips';

        // $trips = SubTrip::with(['sourcepark', 'destpark', 'parenttrip.bus.bus_type', 'parenttrip.driver'])->orderBy('created_at', 'desc')->paginate(15)->toArray();


    // *********** this section is for loading subtrips and parent trips

        $trips = Trip::with(['sourcepark', 'destpark', 'bus.bus_type', 'driver', 'subTrips'])->orderBy('created_at', 'desc')->paginate(1000)->toArray();
    //*******************************************************************************************************

    //*******************************

        // dump($trips['data']);

        return view('trips.index', compact('trips', 'page_title', 'opr'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $page_title = 'Add trip';

        // $boardable_parks = Auth::user()->parks()->get();
        $boardable_parks = Park::where('active', 1)->where('boardable', 1)->get();
        $all_parks = Park::where('active', 1)->where('boardable', 0)->get();
        $buses = Bus::lists('bus_number', 'id')->toArray();
        $drivers = Driver::lists('name', 'id')->toArray();

        return view('trips.create', compact('page_title', 'boardable_parks', 'all_parks', 'buses', 'drivers'));
    }

    public function tripExists(Request $request)
    {

        $source = Park::find($request->source_park)->id;
        $dest = Park::find($request->dest_park)->id;

        $exists = DB::table('sub_trips')
                  ->where('source_park_id', $source)
                  ->where('dest_park_id', $dest)
                  ->first();
     //dd($exists);
         return count($exists);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
        'source_park' => 'required',
        'dest_park' => 'required',
        'fare' => 'required',
        ]);
        //Generating name for Trip
        $s = Park::find($request->source_park);
        $d = Park::find($request->dest_park);

        // $b = BusType::find($request->bus_type);
        // return $s;
       $trip_name = $s->name.'__'.$d->name.'->'.$request->fare;
        // Hiace_Asaba[in Lagos]_Asaba [in Delta]_3000

        // Trip::create($request->all());
        $trip = Trip::firstOrCreate([
              'name' => $trip_name,
              'source_park_id' => $request->source_park,
              'dest_park_id' => $request->dest_park,
              // 'departure_time' => $request->departure_time,
              'fare' => $request->fare,
              'bus_id' => $request->bus_id,
              'driver_id' => $request->driver_id,
              'operator_id' => Auth::user()->operator_id,
              'ac' => 1,
              'tv' => 1,
              'insurance' => 1,
              'passport' =>$request->passport,
              'security' => 0
              ]);

        $name  = Park::find($request->dest_park)->name;


        // we should remember that a round trip will be displayed as a subtrip when booking so the price must tally
         // add the roundtrip details to table
        $subtrip = SubTrip::firstOrCreate([
                                         'name' => $name,
                                         'operator_id' => Auth::user()->operator_id,
                                         'source_park_id' => $request->source_park,
                                         'dest_park_id' => $request->dest_park,
                                         'fare' => $request->fare,
                                         'round_trip_fare' => $request->rt_fare,
                                         'round_trip_status' => $request->rt_activate,
                                         'trip_transit'=>'First Bus',
                                         'parent_trip_id'=>$trip->id,
                                         'active'=>1,
                                         'is_intl_trip' =>$request->is_intl_trip,
                                         'virgin_passport_fare' =>$request->virgin_passport_fare,
                                         'no_passport_fare' =>$request->no_passport_fare,
                                         'round_trip_virgin_passport_fare' =>$request->round_trip_virgin_passport_fare,
                                         'round_trip_no_passport_fare' =>$request->round_trip_no_passport_fare,
                                         'is_parent_trip'=>1

                                       ]);
        // add the normal trip details to table

        Session::flash('flash_message', 'Trip added!');

       // send sms to driver
        //$this->sendSmsToDriver($trip->id);


        return redirect()->action('TripsController@show', [$trip->id]);


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function show($id)
    {
        $trip = Trip::with('sourcepark','destpark','subTrips')->findOrFail($id);

        $page_title = 'View trip';
        $all_parks = Park::where('active', 1)->where('boardable', 0)->get();


        // dd($trip->toArray());

        return view('trips.show', compact('trip', 'page_title', 'all_parks'));
    }

    public function addStop(Request $request)
    {
        //dd($request->all());
         $name  = Park::find($request->dest_park_id)->name;
         $subtrip = SubTrip::firstOrCreate([
                                        'name'=>$name,
                                        'operator_id' => Auth::user()->operator_id,
                                        'source_park_id' => $request->source_park_id,
                                        'dest_park_id' => $request->dest_park_id,
                                        'fare' => $request->fare,
                                        'parent_trip_id'=>$request->parent_trip_id,
                                        'round_trip_fare'=>$request->round_trip_fare,
                                        'round_trip_status'=>$request->has_round_trip,
                                        'is_intl_trip' =>$request->is_intl_trip,
                                        'virgin_passport_fare' =>$request->virgin_passport_fare,
                                        'no_passport_fare' =>$request->no_passport_fare,
                                        'round_trip_virgin_passport_fare' =>$request->round_trip_virgin_passport_fare,
                                        'round_trip_no_passport_fare' =>$request->round_trip_no_passport_fare,
                                        'active'=>1

                                       ]);

        return redirect()->action('TripsController@show', [$request->parent_trip_id]);
    }

    // this is used when a stop was not added at trip creation
     public function addNewStop(Request $request)
    {


         $name  = Park::find($request->dest_park_id)->name;
         $subtrip = SubTrip::firstOrCreate([
                                          'name'=>$name,
                                         'operator_id' => Auth::user()->operator_id,
                                         'source_park_id' => $request->source_park_id,
                                         'dest_park_id' => $request->dest_park_id,
                                         'fare' => $request->fare,
                                        'parent_trip_id'=>$request->parent_trip_id,
                                        'round_trip_fare'=>$request->round_trip_fare,
                                         'round_trip_status'=>$request->has_round_trip,
                                        'is_intl_trip' =>$request->is_intl_trip0,
                                         'virgin_passport_fare' =>$request->virgin_passport_fare,
                                         'no_passport_fare' =>$request->no_passport_fare,
                                         'round_trip_virgin_passport_fare' =>$request->round_trip_virgin_passport_fare,
                                         'round_trip_no_passport_fare' =>$request->round_trip_no_passport_fare,
                                         'active'=>1

                                       ]);

        $trip = Trip::with('subTrips.destpark','bus','driver')->findOrFail($request->parent_trip_id);
        $boardable_parks = Park::where('active', 1)->where('boardable', 1)->get();
        $all_parks = Park::where('active', 1)->get();
        $bus_types = BusType::where('show', 1)->get();
        $buses = Bus::lists('bus_number', 'id')->toArray();
        $drivers = Driver::lists('name', 'id')->toArray();
        $page_title = 'Edit trip';
        $assocSubTrips = $trip->subTrips->toArray();
       return view('trips.edit', compact('trip','assocSubTrips','buses','drivers', 'page_title', 'boardable_parks', 'all_parks'));

    }


    public function editStop(Request $request)
    {
        // dd($request->all());

        $stop = SubTrip::find($request->stop_id);


         $stop->update([
                                         // 'id' => $request->stop_id,
                                         'operator_id' => Auth::user()->operator_id,
                                         'source_park_id' => $request->source_park_id,
                                         'dest_park_id' => $request->dest_park_id,
                                         'fare' => $request->fare,
                                        'parent_trip_id'=>$request->parent_trip_id,
                                        'active'=>1

                                       ]);


        return redirect()->action('TripsController@show', [$request->parent_trip_id]);


    }

    public function deleteStop(Request $request)
    {
        // dd($request->all());

        SubTrip::destroy($request->id);

        Session::flash('flash_message', 'Stop deleted!');

        return redirect()->action('TripsController@show', [$request->parent_trip_id]);
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
   public function edit($id)
    {
        // $trip = SubTrip::findOrFail($id);
        // // get parent trip ID
        // $pTripId = $trip->parent_trip_id;
        // get the parent trip
        $trip = Trip::with('subTrips.destpark','bus','driver')->findOrFail($id);

        $boardable_parks = Park::where('active', 1)->where('boardable', 1)->get();
        $all_parks = Park::where('active', 1)->get();
        $bus_types = BusType::where('show', 1)->get();
        $buses = Bus::lists('bus_number', 'id')->toArray();
        $drivers = Driver::lists('name', 'id')->toArray();
        $page_title = 'Edit trip';
        // $pTripNameArr = explode("__",$pTrip['name']);
       // dd($pTripNameArr);

        // $trip = $pTrip->subTrips[0];
        $assocSubTrips = $trip->subTrips->toArray();

       if (true)
       {
        return view('trips.edit', compact('trip','id','acStatus','assocSubTrips','assignedBus','assignedDriver','buses','drivers','activeStatus', 'page_title', 'boardable_parks', 'all_parks'));
       }
       else
       {
        return view('trips.subtripEdit', compact('trip','acStatus','assocSubTrips','assignedBus','assignedDriver','buses','drivers','activeStatus', 'page_title', 'boardable_parks', 'all_parks'));
       }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function update($id, Request $request)
    {

        // dump($request->all());

        $trip = Trip::with('subTrips')->findOrFail($request->trip_id);

        $trip->bus_id = $request->bus_id;
        $trip->driver_id = $request->driver_id;
        // $trip->departure_time = $request->departure_time;
        $trip->active = $request->active;
        $trip->ac = $request->ac;
        $trip->passport = $request->passport;
        $trip->fare= $request->fare;




        for ($i=0; $i < count($trip->subTrips); $i++) {

            $sTrip = $trip->subTrips[$i];

            if($i==0)
            {
             $sTrip->round_trip_fare = $request->input('rt_fare');
             $sTrip->fare = $request->input('fare');
             $sTrip->round_trip_status = $request->input('rt_activate');
             $sTrip->is_intl_trip= $request->is_intl_trip;
             $sTrip->virgin_passport_fare = $request->virgin_passport_fare;
             $sTrip->no_passport_fare = $request->no_passport_fare;
             $sTrip->round_trip_virgin_passport_fare = $request->round_trip_virgin_passport_fare;
             $sTrip->round_trip_no_passport_fare = $request->round_trip_no_passport_fare;

            }
            else
            {

                $sTrip->fare = $request->input('fare'.$i);
                $sTrip->active = ($request->active && $request->input('active'.$i));
                $sTrip->round_trip_fare = $request->input('round_trip_fare'.$i);
                $sTrip->round_trip_status = $request->input('has_round_trip'.$i);
                $sTrip->is_intl_trip= $request->input('is_intl_trip'.$i);
                 $sTrip->virgin_passport_fare = $request->input('virgin_passport_fare'.$i);
                 $sTrip->no_passport_fare = $request->input('no_passport_fare'.$i);
                 $sTrip->round_trip_virgin_passport_fare = $request->input('round_trip_virgin_passport_fare'.$i);
                 $sTrip->round_trip_no_passport_fare = $request->input('round_trip_no_passport_fare'.$i);
            // dump($sTrip->toArray());
            }
            $sTrip->save();
        }

        $trip->save();
        Session::flash('flash_message', 'Trip updated!');
        return redirect('trips');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        Trip::destroy($id);

        Session::flash('flash_message', 'Trip deleted!');

        return redirect('trips');
    }

    public function getUserPark()
    {
        $user = Auth::user()->id;
        // get park
        $parkId = DB::select('select park_id from users_parks where user_id = ?',[$user]);
        $userPark = $parkId[0]->park_id;

        return $userPark;
    }

     public function Search(Request $request)
     {
        $userPark = $this->getUserPark();
        // get users park

         // get all buses
        $buses =   Bus::all();

        $page_title = 'Search trip';
        $op_id = $request->session()->get('operator_id');

         $permissions = Auth::user()->role->permissions;


         if(strpos($permissions, 'book_from_any_park') === FALSE)
           $boardable_parks = Auth::user()->parks()->orderBy('name','asc')->get();
         else
          $boardable_parks = Park::where('active', 1)->where('boardable', 1)->orderBy('name','asc')->get();



        if(strpos($permissions, 'book_to_any_park') === FALSE)
            $all_parks = Auth::user()->destinations()->orderBy('name','asc')->get();
         else
           $all_parks  = Park::where('active', true)->orderBy('name','asc')->get();

        // dd($boardable_parks);
        // return $bus_type;
        if(time() > strtotime("11:00am"))
            $date = (date('d F, Y', strtotime('+1day')));
        else
            $date = (date('d F, Y'));


       // search by bus

        $source_park_id = $dest_park_id = 0;




        if ($request->isMethod('post')) {

            if( isset($request->bus))
            {

                $b =  $request->bus;

                $d_date = $request->departure_date;
                $trip_date = date('Y-m-d', strtotime($date));

             // get bus id
                $bus =  Bus::where('bus_number','LIKE', '%'.$b. '%')->get(['id'])->toArray();
                if (!empty($bus)) {
                 $busid = $bus[0]['id'];
                 // get the dailytriip
                 $dt =  DailyTrip:: where('bus_id','=',$busid)->where('trip_date', $trip_date   )->get();


                 if(count($dt) > 0){

                     // get the trip id
                     $tripId = $dt[0]->trip_id;
                     // query the parent trip
                     $pTrip = Trip::findOrFail($tripId);

                     // get source park
                     $source_park = $pTrip->source_park_id;
                     // get dest park
                     $dest_park = $pTrip->dest_park_id;

                    // get all dailytrips with bus id
                    $page_title = 'Search trip';
                    $op_id = $this->settings->operator_id;

                     $permissions = Auth::user()->role->permissions;


                     if(strpos($permissions, 'book_from_any_park') === FALSE)
                       $boardable_parks = Auth::user()->parks()->orderBy('name','asc')->get();
                     else
                      $boardable_parks = Park::where('active', 1)->where('boardable', 1)->orderBy('name','asc')->get();



                    if(strpos($permissions, 'book_to_any_park') === FALSE)
                        $all_parks = Auth::user()->destinations()->orderBy('name','asc')->get();
                     else
                       $all_parks  = Park::where('active', true)->orderBy('name','asc')->get();

                    // dd($boardable_parks);
                    // return $bus_type;


                     // $bus = $request->bus;
                     // $trip_date = $request->trip_date;
                     // $d_date= $trip_date;
                     // use bus to query
                      $trips = SubTrip::with(['parenttrip.operator', 'parenttrip.destpark', 'sourcepark', 'destpark', 'parenttrip.bus', 'parenttrip.default_bus',
                                'parenttrip.dailyTrips'=>function($query) use ($trip_date){
                                    $query->where('trip_date', $trip_date);

                                },'parenttrip.dailyTrips.bus','parenttrip.dailyTrips.driver'])
                                        ->where('active',1)
                                        ->where('parent_trip_id','=',$tripId)
                                        ->paginate(100);

                      $operators =array();


                      //dump($trips->toArray());

                }


            }

                if(!isset($trips))
                    $trips = array();

                return view('trips.search', compact('trips', 'buses', 'operators', 'date', 'd_date', 'page_title', 'boardable_parks', 'all_parks', 'userPark', 'source_park', 'dest_park', 'permissions', 'b', 'source_park_id', 'dest_park_id'));
    }


            // dd($request->request);
            $source_park = $request->source_park;
            $dest_park = $request->dest_park;

            $source_park_id =  $request->source_park;
            $dest_park_id =  $request->dest_park;

            $d_date = $request->departure_date;
            $trip_date = date('Y-m-d', strtotime($date));


            $trips = SubTrip::with(['parenttrip.operator', 'parenttrip.destpark', 'sourcepark', 'destpark', 'parenttrip.bus', 'parenttrip.default_bus',
                                'parenttrip.dailyTrips'=>function($query) use ($trip_date){
                                    $query->where('trip_date', $trip_date);

                                },'parenttrip.dailyTrips.bus','parenttrip.dailyTrips.driver'])

                            ->where('source_park_id', $request->source_park)
                            ->where('dest_park_id', $request->dest_park)
                            ->where('active',1)
                            ->paginate(100);

            // dump($trips->toArray());

            $operators =array();
            // $operators = array();
            // foreach ($trips as $t) {
            //     $operator_name = ($t->parenttrip->operator->name);
            //     $operators[] = ['id'=>$t->parenttrip->operator->id, 'name'=>$operator_name];
            // }
            // dd($operators);

        }


        // dd('I am here...');



        return view('trips.search', compact('trips', 'buses', 'operators', 'date', 'd_date', 'page_title', 'boardable_parks', 'all_parks', 'source_park', 'dest_park', 'permissions', 'source_park_id', 'dest_park_id','userPark'));
    }



    public function AdjustPrices(Request $request){

        if ($request->isMethod('post')) {

            $res =SubTrip::where('id', $request->id)
                    ->update(['fare' => $request->fare]);
            if($res)
                echo 'True';
            else
                echo 'False';
        }
    }

     public function uploadfile(Request $Request){
      ///  echo 'Yesssss';
    }




    public function UploadTrips(Request $request){
           if(Input::hasFile('userfile')){

             $path = Input::file('userfile')->getRealPath();
             $data = Excel::load($path, function($reader) {})->get();

             if(!empty($data) && $data->count()){

               foreach ($data as $key => $value) {

         // Verify Source Park
         if(empty($value->source_park)){
           continue;
         }
         $source_park = strtoupper($value->source_park.' [in '. $value->source_park_state).']';
         $dest_park = strtoupper($value->destination_park.' [in '. $value->destination_park_state).']';

                 //check if park and destination exists
                 $source_park_state = State::where('name', ucfirst($value->source_park_state))->where('active', 1)->first();
                 $dest_park_state = State::where('name', ucfirst($value->destination_park_state))->where('active', 1)->first();
                 //$source_park_state = State::where('name', ucwords($value->source_park_state))->where('active', 1)->first();


if($source_park_state && $dest_park_state){
                $s_prk = Park::firstOrCreate([
                   'name' => $source_park,
                   'boardable'=>1, //this differentiates source park from destination park
                   'state_id'=> $source_park_state->id,
                   'operator_id'=> Auth::user()->operator_id,
                   'active'=>1
                ]);

                 $d_prk = Park::firstOrCreate([
                    'name' => $dest_park,
                    'boardable'=>0,
                    'state_id'=> $dest_park_state->id,
                    'operator_id'=> Auth::user()->operator_id,
                    'active'=>1

                 ]);

               }else{
                 continue;
                 $flash[] = "You made a mistake in the spelling of a state or you left it blank. You spelt either of these two wrong => 1.". $value->source_park_state . " or ".$value->$dest_park_state ;

               }


               //this code may break if you remove this if else statement below,
               //mecause that's where $s_park and $d_park is first called to execute
                 $flash = array();
                 if ($d_prk == null){ //if destination park doesnt exist , exit and throw error
                   $flash[] = "Error, destination park does not exist or is not active. ";
                   continue;
                 } else if($s_prk == null){
                   $flash[] = "Error, source park does not exist or is not active. ";
                   continue;
                 }


                  //create Trip
                 $trip_name = strtoupper($source_park.'_'.$dest_park);
                 //retrive driver ID
                $trip =  Trip::firstOrCreate([
                   'operator_id' => Auth::user()->operator_id,
                   'name' => $trip_name,
                   'source_park_id' => $s_prk->id,
                   'dest_park_id' => $d_prk->id,
                   'fare' => $value->fare,
                   'active'=>1
                 ]);

                 if($trip){ //if trip success
                   //create subtrips if there are subtrips
                   if($value->source_park != '' AND  $value->source_park != null){
                   if($value->sub_destination_state){

                     $subtrip_state = State::where('name', ucfirst($value->sub_destination_state))->where('active', 1)->first();
                      //dd($subtrip_state);
                   }else{
                     continue; //i know I should close early, but i like to cum late
                   }
                   //the first destination is a subtrip

                   $createFirstSubTrip = SubTrip::firstOrCreate([
                      'name' => $dest_park,
                      'state_id'=> $subtrip_state->id,
                      'source_park_id'=> $s_prk->id,
                      'dest_park_id'=> $d_prk->id,
                      'fare'=> $value->sub_destination_fare,
                      'parent_trip_id'=> $trip->id,
                      'operator_id'=> Auth::user()->operator_id,
                      'active'=>1
                   ]);

             if($createFirstSubTrip){ //if the first subtrip is created, then create the rest



                  if($subtrip_state){
                    //create sub trips
                    $createSubTrip = SubTrip::firstOrCreate([
                       'name' => strtoupper($value->sub_destination_park.' [in '.$value->sub_destination_state.']'),
                       'state_id'=> $subtrip_state->id,
                       'source_park_id'=> $s_prk->id,
                       'dest_park_id'=> $d_prk->id,
                       'fare'=> $value->sub_destination_fare,
                       'parent_trip_id'=> $trip->id,
                       'operator_id'=> Auth::user()->operator_id,
                       'active'=>1
                    ]);
                  }

                  }


              }

                   \Session::flash('status', 'Upload was successful! Trips uploaded.');

                 }

               }




             }
           }


          return view('trips.bulk_upload', compact('flash'));
      }


     public function getAllTripDetails(Request $request){

        $sourcepark = $request->source_park;
        $destpark = $request->dest_park;

        // in preparation for a bulk insert
    //$allTrips = Trip::where('source_park_id', $sourcepark)->where('dest_park_id', $destpark)->get(['id']);
    $allTrips = DB::table('trips')
                     ->select(DB::raw('id'))
                     ->where('source_park_id', '=', $sourcepark)
                     ->where('dest_park_id', '=', $destpark)
                     ->get();
    return json_encode($allTrips);

  }


     public function getTripDetails(Request $request){

        $sourcepark = $request->source_park;
        $destpark = $request->dest_park;


$trip = Trip::where('source_park_id', $sourcepark)->where('dest_park_id', $destpark)->orderBy('id', 'asc')->first();
        return $trip->name."|"./*$trip->departure_time."|".*/$trip->fare."|".$trip->no_of_seats."|".$trip->id;
  }

  public function getTripFrom(Request $request){

        $tripId = $request->trip_id;
        $tripFrom = Trip::where('id', $tripId)->first();
        $ret =$tripFrom->name;
        return $ret;
  }
  public function getTripTo(Request $request){
        $tripId = $request->trip_id;
          $tripTo = Trip::where('id', $tripId)->first();
        $ret = $tripTo->name;
        return $ret;
  }

   public function saveTripStops(Request $request){

        //get all trip Details in that source/dest route ireespective of tripid

        $allIds = $request->paramName;
        $allIds2 = explode(",", $allIds);
        $stopss = $request->trip_stops;

        $data = array();
        $subdata = array_fill_keys(array('trip_id','stops'), '');


        for($i =0; $i<count($allIds2); $i++)
        {
           $subdata['trip_id'][$i] = $allIds2[$i];
           $subdata['stops'][$i] = $stopss;
        }
         //combine
        // perform a bulk insert
        for($i =0; $i<count($allIds2); $i++)
        {
             $data[$i] = array('trip_id'=>$subdata['trip_id'][$i],
                                'stops'=>$stopss,
                                'created_at'=> date('Y-m-d H:i:s'),
                                'updated_at'=> date('Y-m-d H:i:s') );
        }

        TripStop::insert($data); // Eloquent

        return redirect('trip-stops');

  }

   public function getTripStops(Request $request){

       $tripId = $request->trip_id;

       $st = TripStop::select('stops')->where('trip_id',$tripId)->orderBy('id', 'asc')->first();

       return $st->stops;

  }

  public function sendSms($phone, $msg)
    {

        //settings
        $cid ="";
        $user = "bus_account";
        $senderArray = explode(" ", $this->operator->name);
        $senderId = $senderArray[0];
        $pass ="buspass";
       $ch = curl_init();
       $data = array('username'=>$user,
              'password'=>$pass,
              'recipient'=>$phone,
              'sender'=>$senderId,
              'message'=>$msg);

$newdata = http_build_query($data);
       $url = "http://www.nigerianbulksms.com/components/com_spc/smsapi.php?".$newdata;
       curl_setopt($ch,CURLOPT_URL, $url);
       curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
       curl_setopt($ch, CURLOPT_POST, 1);
       $buffer = curl_exec($ch);

       curl_close($ch);

        $strHeader = get_headers($url)[0];

    $statusCode = substr($strHeader, 9, 3 );

       return $statusCode;

    }


  public function sendSmsToDriver($trip_id)
  {
     // get all phone numbers of driver

        // get next day date
        $departuredate = date('M j, Y', strtotime(' +1 day'));

        // get all trips

        $trip = Trip::findOrFail($trip_id);

        // get driver phone
        $phone = Driver::where('id',$trip->driver_id)->pluck('phone');
        $ph =$phone[0];
        // get bus id
        $bus = Bus::where('id',$trip->bus_id)->pluck('bus_number');
        $b = $bus[0];

        // get source and destination of trips
             $s = Park::find($trip->source_park_id);
             $d = Park::find($trip->dest_park_id);
             $op = $this->operator->name;
             $msg = $op . " trip notice: ". " FROM: ".
             $s->name ." TO: ".  $d->name .
             ".  Bus: ". $b.
             // ".  Departure Time: ".$trip->departure_time.
             ".  Departure Date: ".$departuredate
            ;



            if($ph !="" && $ph !="null")
            {
                $statusCode = $this->sendSms($ph,$msg);

                     if($statusCode == 200)
                     {
                         Session::flash('delivery_report', 'Sms Sent!');
                     }
                     else
                     {
                        Session::flash('delivery_report', 'Sms Not Sent!');
                     }
            }

  }


}
