<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Trip;
use App\Models\Park;
use App\Models\Operator;
use App\Models\BusType;
use App\Models\TripStop;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;
use Auth;

class TripStopsController extends Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {

        // $op_id = $request->session()->get('operator_id');
        // if(empty($op_id)){
        //     $request->session()->flash('select', 'Please select an Operator');
        //     return redirect()->action('OperatorsController@index');
        // }
       
        $page_title = 'trip stops';
        $trips = TripStop::all();
       //$trips = TripStop::with(['trip_id', 'stops','trip'])->paginate(15)->toArray();
       // dd($request);
         
        return view('trip-stops.index', compact('trips', 'page_title', 'opr'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $page_title = 'Add trip stop';

        $boardable_parks = Auth::user()->parks()->get();
        $all_parks = Park::where('active', 1)->get();
        $bus_types = BusType::where('show', 1)->get();
        // return $bus_type;

        // dd(Auth::user()->parks()->get()->toArray());

        return view('trip-stops.create', compact('page_title', 'boardable_parks', 'all_parks', 'bus_types'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function show($id)
    {
        $trip = TripStop::find($id);

        $page_title = 'View trip';
        return view('trip-stops.show', compact('trip', 'page_title'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $trip = TripStop::find($id);
        

        $page_title = 'Edit trip';
        return view('trip-stops.edit', compact('trip', 'page_title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        
        $trip = TripStop::findOrFail($id);
        $trip->update($request->all());

        Session::flash('flash_message', 'Trip stop updated!');

        return redirect('trip-stops');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        TripStop::destroy($id);

        Session::flash('flash_message', 'Trip stop deleted!');

        return redirect('trip-stops');
    }

   

}
