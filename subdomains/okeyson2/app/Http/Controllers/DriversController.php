<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Illuminate\Support\Facades\Input;
use App\Models\Driver;
use App\Models\Bus;
use App\Models\Park;
use App\Models\BusType;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;
use Auth;
use Excel;


class DriversController extends Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $drivers = Driver::with('default_bus', 'default_park')->paginate(1000);
        $parks = Park::lists('name', 'id')->toArray();

        // dd($drivers->toArray());

        $page_title = 'drivers';

        return view('drivers.index', compact('drivers', 'page_title', 'parks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $page_title = 'Add driver';

        $parks = [null => 'None'] + Park::lists('name', 'id')->toArray();


        return view('drivers.create', compact('page_title', 'parks'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, ['name' => 'required', 'phone'=>'required' ]);

        $d = $request->all();
        if(empty($d['default_park_id']))
            unset($d['default_park_id']);

        Driver::create($d);

        Session::flash('flash_message', 'Driver added!');

        return redirect('drivers');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function show($id)
    {
        $driver = Driver::findOrFail($id);

        $page_title = 'View driver';
        return view('drivers.show', compact('driver', 'page_title'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
     public function importExcel()
   	{
   		if(Input::hasFile('import_file')){
   			$path = Input::file('import_file')->getRealPath();
   			$data = Excel::load($path, function($reader) {})->get();
   			if(!empty($data) && $data->count()){
   				foreach ($data as $key => $value) {
   					$insert[] = ['title' => $value->title, 'description' => $value->description];
   				}
   				if(!empty($insert)){
   					DB::table('drivers')->insert($insert);
   					dd('Insert Record successfully.');
   				}
   			}
   		}
   		return back();
   	}


    public function UploadDrivers(Request $request){


           if(Input::hasFile('userfile')){

             $path = Input::file('userfile')->getRealPath();
             $data = Excel::load($path, function($reader) {})->get();

             if(!empty($data) && $data->count()){

        foreach ($data as $key => $value) {

            if($value->driver_name == null AND $value->driver_phone_1 == null){
                continue; //bus number and platenumber cannot be null at the same time

              
            }

                 //if bus  is set
         if(empty($value->default_bus_number)){
                  $createBusType= Bus::firstOrCreate([
                      'bus_number' => $value->default_bus_number,
                      'number_plate' => $value->default_bus_plate_number,
                   ])->id;
         }
//create park
         if(empty($value->default_park)){
                  $createPark= Park::firstOrCreate([
                      'name' => strtoupper($value->default_park),
                   ])->id;
         }



try {
    $createDriverId = Driver::firstOrCreate(
  ['operator_id' => Auth::user()->operator_id,
             'name' => $value->driver_name,
             'phone' => $value->driver_phone_1,
             'phone2' => $value->driver_phone_2,

])->id;

} catch (Exception $e) {
    dd('Caught exception: ',  $e->getMessage(), "\n");
}


            if(isset($createDriverId)){
                   //update the bus
                 $driverUpdate = Driver::find($createDriverId);

                 if(isset($createPark->id)){
                    $driverUpdate->default_park_id = $createPark;
                 }
                if(isset($createBusType)){
                    $driverUpdate->bus_type_id = $createBusType;
                }

                $driverUpdate->save();
            }
            


           Session::flash('status', 'Upload was successful! Buses uploaded.');

       }

    }
}

//return redirect()->route('drivers');
            return redirect()->back();
          //return view('drivers.index', compact('flash'));
}












    public function edit($id)
    {
        $driver = Driver::findOrFail($id);

        $page_title = 'Edit driver';
        $parks = [null => 'None'] + Park::lists('name', 'id')->toArray();

        return view('drivers.edit', compact('driver', 'page_title', 'parks'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        $this->validate($request, ['name' => 'required', ]);

        $driver = Driver::findOrFail($id);
        $driver->update($request->all());

        Session::flash('flash_message', 'Driver updated!');

        return redirect('drivers');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        Driver::destroy($id);

        Session::flash('flash_message', 'Driver deleted!');

        return redirect('drivers');
    }

}
