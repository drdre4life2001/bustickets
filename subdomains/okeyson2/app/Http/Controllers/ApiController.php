<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Libraries\Utility;
use App\Models\Agent;
use App\Models\User;
use App\Models\AgentTransaction;
use App\Models\Park;
use App\Models\Seat;
use App\Models\Operator;
use App\Models\Trip;
use App\Models\SubTrip;
use App\Models\DailyTrip;
use App\Models\Booking;
use App\Models\BusBooking;
use App\Models\Passenger;
use App\Models\PaymentMethod;
use App\Models\Setting;

class ApiController extends Controller
{
    
    public function __construct()
    {
        parent::__construct();
    }

    public function sendSms($phone, $msg)
    { 
        
       //settings
        $cid ="";
        $user = "buscom_sms";
        $senderArray = explode(" ", $this->operator->name);
        $senderId = $senderArray[0];
        $to = $phone;
        $pass ="bus.com.ng";   
       $ch = curl_init();
       $postdata = 'user='.$user.'&pass='.$pass.'&from='.$senderId.'&to='.$to.'&msg='.$msg; //initialize the request variable
     // echo $postdata;

       $url = 'http://cloud.nuobjects.com/api/send/'; //this is the url of the gateway's interface
        $ch = curl_init(); //initialize curl handle
        curl_setopt($ch, CURLOPT_URL, $url); //set the url
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1); //return as a variable
        curl_setopt($ch, CURLOPT_POST, 1); //set POST method
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata); //set the POST variables
        $response = curl_exec($ch); // grab URL and pass it to the browser. Run the whole process and return the response
        curl_close($ch); //close the curl handle

        $strHeader = get_headers($url)[0];

        $statusCode = substr($strHeader, 9, 3 );
       
       return $statusCode;

    }

    public function postLogin(Request $request)
    {
        Utility::$response = Utility::checkParameters($request, array('username', 'password'));
        if(Utility::$response['status'] == 404)
            return response()->json(Utility::$response);       


        //Get password of submitted email if it exists in DB
        $user = User::with('parks')->where('email', $request->username)->first();
        
        if(empty($user)) {
           Utility::$response['status'] = 404;
           Utility::$response['message'] = trans('auth.no_account');

           return response()->json(Utility::$response);
        }

        // echo($request->password);
        // exit;

        // if($agent->password == $request->passsword)
        if(true)
        {	

        	$user->wallet_balance = 0;
        	
        	Utility::$response['status'] = 200;
            Utility::$response['data'] = $user;
            Utility::$response['message'] = trans('auth.success');
            return response()->json(Utility::$response);

        }
        else
        {
            Utility::$response['status'] = 500;
            Utility::$response['message'] = trans('auth.failed');
            
            return response()->json(Utility::$response);
        }
        
        
    }


    public function postGetDashData(Request $request)
    {
        Utility::$response = Utility::checkParameters($request, array('agent_id'));
        if(Utility::$response['status'] == 404)
            return response()->json(Utility::$response);       


        //Get password of submitted email if it exists in DB
        $agent = Agent::find($request->agent_id);
        
        if(empty($agent)) {
           Utility::$response['status'] = 404;
           Utility::$response['message'] = trans('auth.no_account');

           return response()->json(Utility::$response);
        }

        // dd($agent->password == $request->passsword);

        // if($agent->password == $request->passsword)
        $agent->wallet_balance = $agent->balance;
        
        //getting dashboard data
        $bookings_values_sum = $agent->transactions()->where('type', 'BOOKING')->sum('value');
        $bookings_paid_sum = $agent->transactions()->where('type', 'BOOKING')->sum('amount_paid');
        $bookings_count = $agent->transactions()->where('type', 'BOOKING')->count();

        $topup_values_sum = $agent->transactions()->where('type', 'TOPUP')->sum('value');
        $topup_paid_sum = $agent->transactions()->where('type', 'TOPUP')->sum('amount_paid');
        $topup_count = $agent->transactions()->where('type', 'TOPUP')->count();

        //commission
        $agent->commission = $topup_values_sum - $topup_paid_sum;

        
        $data = compact('agent', 'bookings_values_sum', 'bookings_paid_sum', 'bookings_count', 'topup_values_sum', 'topup_paid_sum', 'topup_count');

        Utility::$response['status'] = 200;
        Utility::$response['data'] = $data;
        Utility::$response['message'] = 'Success';
        return response()->json(Utility::$response);
        
    }


    public function postGetParks(Request $request)
    {
        

        //Get password of submitted email if it exists in DB
        // $agent = Agent::where('username', $request->username)->first();
        $parks = Park::with('state')->where('active',1)->where('boardable',1)->get();
        
        Utility::$response['status'] = 200;
        Utility::$response['data'] = $parks;
        Utility::$response['message'] = trans('auth.success');
        return response()->json(Utility::$response);
        
    }

    public function postGetDestinations(Request $request)
    {
        
        Utility::$response = Utility::checkParameters($request, array('park_id'));
        if(Utility::$response['status'] == 404)
            return response()->json(Utility::$response);       


        //Get password of submitted email if it exists in DB
        // $agent = Agent::where('username', $request->username)->first();
         $dests = Park::with('state')->where('active',1)->where('id', '<>', $request->park_id)->get();
        $operators = Operator::where('active', 1)->get();
        $parks = Park::with('state')->where('active',1)->where('boardable',1)->get();
        
        Utility::$response['status'] = 200;
        Utility::$response['data'] = ['dests' => $dests, 'parks' => $parks, 'operators'=>$operators];
        Utility::$response['message'] = trans('auth.success');
        return response()->json(Utility::$response);
        
    }

    public function postGetDestinationsNew(Request $request)
    {
        
        Utility::$response = Utility::checkParameters($request, array('agent_id'));
        if(Utility::$response['status'] == 404)
            return response()->json(Utility::$response);       


        //Get password of submitted email if it exists in DB
        $agent = User::find($request->agent_id);
        $parks = $agent->destinations()->get();
        $operators = Operator::where('active', 1)->get();
        
        Utility::$response['status'] = 200;
        Utility::$response['data'] = ['dests' => $parks, 'operators'=>$operators];
        Utility::$response['message'] = trans('auth.success');
        return response()->json(Utility::$response);
        
    }


    public function postGetTrips(Request $request)
    {
        
        Utility::$response = Utility::checkParameters($request, array('park_id', 'dest_name', 'date'));
        if(Utility::$response['status'] == 404)
            return response()->json(Utility::$response);  


        $op_id = $this->operator->id;   

        $dest = Park::where('name', $request->dest_name)->first();

        // dd($dest);
        $trip_date = date('Y-m-d', strtotime($request->date));

        $trips = SubTrip::with(['parenttrip.operator', 'parenttrip.destpark', 'sourcepark', 'destpark', 'parenttrip.bus', 'parenttrip.default_bus',
                                'parenttrip.dailyTrips'=>function($query) use ($trip_date){
                                    $query->where('trip_date', $trip_date);
                                     
                                },'parenttrip.dailyTrips.bus','parenttrip.dailyTrips.driver','parenttrip.dailyTrips.seats'])
                                        ->where('source_park_id', $request->park_id)
                                        ->where('dest_park_id', $dest->id)
                                        ->where('active',1)
                                        ->get();


        // dd($trips->toArray());                                        
        
        
        Utility::$response['status'] = 200;
        Utility::$response['data'] = ['trips' => $trips];
        Utility::$response['message'] = trans('auth.success');
        return response()->json(Utility::$response);
        
    }


    public function postGetTripsWithPark(Request $request)
    {
        
        Utility::$response = Utility::checkParameters($request, array('park_name', 'dest_name', 'date'));
        if(Utility::$response['status'] == 404)
            return response()->json(Utility::$response);  


        $op_id = $this->operator->id;   

        $dest = Park::where('name', $request->dest_name)->first();
        $park = Park::where('name', $request->park_name)->first();

        // dd($dest);
        $trip_date = date('Y-m-d', strtotime($request->date));

        $trips = SubTrip::with(['parenttrip.operator', 'parenttrip.destpark', 'sourcepark', 'destpark', 'parenttrip.bus.bus_type', 'parenttrip.default_bus.bus_type',
                                'parenttrip.dailyTrips'=>function($query) use ($trip_date){
                                    $query->where('trip_date', $trip_date);
                                     
                                },'parenttrip.dailyTrips.bus.bus_type','parenttrip.dailyTrips.driver','parenttrip.dailyTrips.seats'])
                                        ->where('source_park_id', $park->id)
                                        ->where('dest_park_id', $dest->id)
                                        ->where('active',1)
                                        ->get();


        // dd($trips->toArray());                                        
        
        
        Utility::$response['status'] = 200;
        Utility::$response['data'] = ['trips' => $trips];
        Utility::$response['message'] = trans('auth.success');
        return response()->json(Utility::$response);
        
    }

    public function postBookTrip(Request $request)
    {
        
        Utility::$response = Utility::checkParameters($request, array('trip_id', 'date', 'num_pass', 'contact_name', 'contact_phone', 'nok', 'nok_phone', 'payment_method'));
        if(Utility::$response['status'] == 404)
            return response()->json(Utility::$response);     

        $trip = SubTrip::with('parenttrip.default_bus', 'parenttrip.bus')->find($request->trip_id);
        // dd($trip->toArray());

        $trip_name = $trip->parenttrip->name;
        $fare = $trip->fare;

        $booked_seats = array();
        $dt = DailyTrip::where('trip_id', $trip->parenttrip->id)->where('trip_date', date('Y-m-d', strtotime($request->date)))->first(); 
        if(!empty($dt)){
            if(!empty($dt->seats())){
                foreach ($dt->seats()->whereNotNull('booking_id')->get() as $seat) {
                    $booked_seats[] = $seat->seat_no;
                }

                //$booked_seats = substr($booked_seats, 0, strlen($booked_seats) - 1);
            }
        }

        //checking for booked seats...
        if(in_array($request->seat, $booked_seats)){

            Utility::$response['status'] = 500;
            Utility::$response['message'] = 'Contact person seat is already taken. Please choose another one.';
            return response()->json(Utility::$response);     
        }


        $selSeats = array();
        $selSeats[] = $request->seat;
        $psg_count = 1;



        //passengers...
        $arr = explode("$$$$", $request->pass_details);
        for ($i=0; $i < count($arr); $i++) { 
            $a = explode("&&&&", $arr[$i]);
            if(count($a) == 4){
                $psg_name = $a[0];
                $psg_gender = $a[1];
                $psg_seat = $a[2];
                if($a[3] != 'Infant')
                    $psg_count++;
                
                if(in_array($psg_seat, $booked_seats)){

                    Utility::$response['status'] = 500;
                    Utility::$response['message'] = 'Passenger '.($i+1).'\'s seat is already taken. Please choose another one.';
                    return response()->json(Utility::$response);     
                }


                $selSeats[] = $psg_seat;   
            }
            

        }



        
            
            
            if($request->trip_type == 'Round Trip')
                $fare = $trip->round_trip_fare;
            else
                $fare = $trip->fare;


            switch ($request->passport_type) {
                case 'Virgin Passport':
                    # code...
                    $fare = ($request->trip_type == 'Round Trip')?$trip->round_trip_virgin_passport_fare:$trip->virgin_passport_fare;
                    break;
                
                case 'ID card/ No Passport':
                    # code...
                    $fare = ($request->trip_type == 'Round Trip')?$trip->round_trip_no_passport_fare:$trip->no_passport_fare;
                    break;
                
                default:
                    # code...
                    break;
            }



            $final_cost = $psg_count * $fare;



            //luggage...
            $luggSetting = Setting::where('key','track_luggage')->first();
            $track_luggage = ($luggSetting->value == 'true')?TRUE:FALSE ;
            $luggage_limit = intval(Setting::where('key', 'luggage_limit')->first()->value);
            $lugg_cost = 0;

            if($track_luggage){

                $luggage_limit = intval(Setting::where('key', 'luggage_limit')->first()->value);
                $cpk = 0;
                if($trip->is_intl_trip)
                    $cpk = intval(Setting::where('key', 'intl_cost_per_kg')->first()->value);
                else
                    $cpk = intval(Setting::where('key', 'local_cost_per_kg')->first()->value);


                $total_lugg_limit = $psg_count * $luggage_limit;
                $lugg_cost = ($request->luggage_weight - $total_lugg_limit) * $cpk;
                if($lugg_cost < 0)
                    $lugg_cost = 0;


                $final_cost += $lugg_cost;
            }





        $pMethod = PaymentMethod::where('name', $request->payment_method)->first();

        // dd($pMethod->id);

        $booking_code = $trip->parenttrip->operator->code.rand(100000,999999);

        $booking = BusBooking::firstOrCreate([
                    'trip_id' => $request->trip_id,
                    'date' => date('Y-m-d', strtotime($request->date)),
                    'passenger_count' => $request->num_pass,
                    'contact_name' => $request->contact_name,
                    'contact_phone' => $request->contact_phone,
                    'contact_email' => $request->contact_email,
                    'contact_address' => $request->address,
                    'next_of_kin' => $request->nok,
                    'next_of_kin_phone' => $request->nok_phone,
                    'gender' => $request->gender,
                    'status' => 'PAID',
                    'paid_date' => date('Y-m-d H:i:s'),
                    'final_cost' => $final_cost,
                    'unit_cost' => $trip->fare,
                    'user_id' => $request->user_id,
                    'booking_code'=>$booking_code,
                    'payment_method_id'=>$pMethod->id,
                    'departure_time' => date('Y-m-d', strtotime($request->departure_time)),
                    'return_date' => $request->return_date,
                    'is_round_trip' => ($request->trip_type == 'Round Trip'),
                    'passport_type' => $request->passport_type,
                    'luggage_weight' => $request->luggage_weight,
                    'luggage_cost' => $lugg_cost,
                    'source'=>"MPOS"
            ]);


        $selSeats = array();
        $selSeats[] = $request->seat;




        //passengers...
        $arr = explode("$$$$", $request->pass_details);
        for ($i=0; $i < count($arr); $i++) { 
            $a = explode("&&&&", $arr[$i]);
            if(count($a) == 4){
                $psg_name = $a[0];
                $psg_gender = $a[1];
                $psg_seat = $a[2];
                if($a[3] == 'Infant')
                    $psg_infant = 1;
                else
                    $psg_infant = 0;
                $selSeats[] = $psg_seat;

                $response =  Passenger::firstOrCreate([
                    'booking_id'=>$booking->id,
                    'name'=>$psg_name,
                    'gender'=>$psg_gender,
                    'infant'=>$psg_infant,
                ]);    
            }
            


        }


        //updating to daily trips...
            if(empty($dt)){
                // creating default...
                $inserts = [
                        'trip_id'=>$trip->parenttrip->id,
                        'trip_date'=>date('Y-m-d', strtotime($request->date)),
                        'booked_seats'=>0,
                        'total_seats'=>$trip->parenttrip->bus->bus_type->no_of_seats,

                    ];
                $driver_id = $bus_id = '';
                if(!empty($trip->parenttrip->bus)){
                    $inserts['driver_id'] = $trip->parenttrip->bus->default_driver_id;
                    $inserts['bus_id'] = $trip->parenttrip->bus->id;
                }

                $dt = DailyTrip::firstOrCreate($inserts);

                // dd($dt);
            }

            $dt->booked_seats = $dt->booked_seats + $request->num_pass;
            $dt->total_fare = $dt->total_fare + ($booking->final_cost);
            $dt->save();


            //adding seats...
            
            
            foreach ($selSeats as $s) {
                $seat = new Seat;
                $seat->seat_no = $s;
                $seat->daily_trip_id = $dt->id;
                $seat->booking_id = $booking->id;
                $seat->save();
                
            }

            //updating bookings daily trip...
            $booking->daily_trip_id = $dt->id;
            $booking->save();

            $op = $this->operator->name;


            $booking = BusBooking::with('trip.sourcepark', 'trip.destpark', 'trip.parenttrip.bus.bus_type', 'seats', 'paymentmethod', 'passengers','ticketer', 'daily_trip.bus', 'daily_trip.driver')->find($booking->id);


            $booked_seats = '';
            foreach ($booking['seats'] as $seat) {
               $booked_seats .= $seat['seat_no'].',';
            }

            $booked_seats = substr($booked_seats, 0, strlen($booked_seats) - 1);
            
        //sending sms....
        $msg = $op . " purchase notice : "." \nBook Code: ". $booking->booking_code. " \nFrom: "." ".
               $booking->trip->sourcepark->name
                ."to ".$booking->trip->destpark->name.
                ". \nName: ".$booking->contact_name
                .". \nBus: ".$booking->daily_trip->bus->bus_number.
               ". \nDeparture Date: ". $booking->date.
               ". \nSeats: ". $booked_seats.
               ". \nPassengers: ".$booking->passenger_count.
               ". \nFinal Cost: NGN". $booking->final_cost." \nContact:012905460";
                
           // actual sending
          $res = $this->sendSms($booking->contact_phone,$msg);

        
        
        Utility::$response['status'] = 200;
        Utility::$response['data'] = ['booking'=>$booking];
        Utility::$response['message'] = trans('auth.success');
        return response()->json(Utility::$response);
        
    }


    public function postTopup(Request $request)
    {
        Utility::$response = Utility::checkParameters($request, array('agent_id', 'amount'));
        if(Utility::$response['status'] == 404)
            return response()->json(Utility::$response);       


        //Get password of submitted email if it exists in DB
        $agent = Agent::find($request->agent_id);
        
        if(empty($agent)) {
           Utility::$response['status'] = 404;
           Utility::$response['message'] = trans('auth.no_account');

           return response()->json(Utility::$response);
        }

        
        $at  = new AgentTransaction;
        $at->value = ($agent->percentage * $request->amount / 100) + $request->amount;
        // $at->value = $request->amount;
        $at->amount_paid = $request->amount;
        $at->approved = true;
        $at->type = 'TOPUP';

        $agent->transactions()->save($at);
        $agent->balance = $agent->balance + $at->value;
        $agent->save();
        
        Utility::$response['status'] = 200;
        Utility::$response['data'] = $agent->balance;
        Utility::$response['message'] = 'Success';
        return response()->json(Utility::$response);
        
    }

    public function postTransactionHistory(Request $request)
    {
        Utility::$response = Utility::checkParameters($request, array('agent_id'));
        if(Utility::$response['status'] == 404)
            return response()->json(Utility::$response);       


        //Get password of submitted email if it exists in DB
        $agent = Agent::find($request->agent_id);
        
        if(empty($agent)) {
           Utility::$response['status'] = 404;
           Utility::$response['message'] = trans('auth.no_account');

           return response()->json(Utility::$response);
        }

        $transactions = null;
        if(isset($request->type) && !empty($request->type)){
            $transactions = $agent->transactions()->where('type', $request->type)->orderBy('created_at', 'desc')->get();
        }else{
            $transactions = $agent->transactions()->orderBy('created_at', 'desc')->get();
        }   

        
        Utility::$response['status'] = 200;
        Utility::$response['data'] = array('transactions'=>$transactions);
        Utility::$response['message'] = 'Success';
        return response()->json(Utility::$response);
        
    }


    public function postCheckBooking(Request $request){

        Utility::$response = Utility::checkParameters($request, array('booking_code', 'contact_phone'));
        if(Utility::$response['status'] == 404)
            return response()->json(Utility::$response);   


        $booking = BusBooking::with('trip.sourcepark', 'trip.destpark', 'paymentmethod', 'passengers')->where('booking_code', $request->booking_code)->first();
        if(empty($booking)) {
           Utility::$response['status'] = 404;
           Utility::$response['message'] = 'Booking not found!';

           return response()->json(Utility::$response);
        }


        if($booking->contact_phone != $request->contact_phone)   {
           Utility::$response['status'] = 404;
           Utility::$response['message'] = 'Booking not found!';

           return response()->json(Utility::$response);
        }


        Utility::$response['status'] = 200;
        Utility::$response['data'] = $booking;
        Utility::$response['message'] = 'Success';
        return response()->json(Utility::$response);      


    }


    public function postBookingHistory(Request $request)
    {
        Utility::$response = Utility::checkParameters($request, array('user_id'));
        if(Utility::$response['status'] == 404)
            return response()->json(Utility::$response);       


        //Get password of submitted email if it exists in DB
        $agent = User::find($request->user_id);
        
        if(empty($agent)) {
           Utility::$response['status'] = 404;
           Utility::$response['message'] = trans('auth.no_account');

           return response()->json(Utility::$response);
        }

        $bookings = $agent->bookings()->with('trip.sourcepark', 'trip.destpark', 'trip.parenttrip.bus.bus_type', 'seats', 'paymentmethod', 'passengers','ticketer', 'daily_trip.bus', 'daily_trip.driver')->orderBy('created_at', 'desc')->get();
           

        
        Utility::$response['status'] = 200;
        Utility::$response['data'] = $bookings;
        Utility::$response['message'] = 'Success';
        return response()->json(Utility::$response);
        
    }


    
    

}
