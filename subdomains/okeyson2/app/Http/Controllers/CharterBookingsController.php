<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\CharterBooking;
use App\Models\Park;
use App\Models\Bus;
use App\Models\Driver;
use App\Models\PaymentMethod;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;
use Auth;

class CharterBookingsController extends Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $charterbookings = CharterBooking::orderBy('created_at', 'desc')->paginate(150);

        $page_title = 'charterbookings';

        return view('charter-bookings.index', compact('charterbookings', 'page_title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $page_title = 'Add charterbooking';

        $parks = Auth::user()->parks()->get()->lists('name', 'id')->toArray();
    
        $buses = Bus::lists('number_plate', 'id')->toArray();
        $drivers = Driver::lists('name', 'id')->toArray();
        $payment_methods = PaymentMethod::where('active', true)->where('is_fulfilment', true)->lists('name', 'id')->toArray();
        $statuses = ['PENDING'=>'PENDING', 'PAID'=>'PAID', 'CANCELLED'=>'CANCELLED'];

        return view('charter-bookings.create', compact('page_title', 'parks', 'buses', 'drivers', 'payment_methods', 'statuses'));
    }

    public function sendSms($phone, $msg)
    { 
        
       //settings
        $cid ="";
        $user = "buscom_sms";
        $senderArray = explode(" ", $this->operator->name);
        $senderId = $senderArray[0];
        $to = $phone;
        $pass ="bus.com.ng";   
       $ch = curl_init();
       $postdata = 'user='.$user.'&pass='.$pass.'&from='.$senderId.'&to='.$to.'&msg='.$msg; //initialize the request variable
     // echo $postdata;

       $url = 'http://cloud.nuobjects.com/api/send/'; //this is the url of the gateway's interface
        $ch = curl_init(); //initialize curl handle
        curl_setopt($ch, CURLOPT_URL, $url); //set the url
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1); //return as a variable
        curl_setopt($ch, CURLOPT_POST, 1); //set POST method
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata); //set the POST variables
        $response = curl_exec($ch); // grab URL and pass it to the browser. Run the whole process and return the response
        curl_close($ch); //close the curl handle

        $strHeader = get_headers($url)[0];

        $statusCode = substr($strHeader, 9, 3 );
       
       return $statusCode;

    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {

        $data = $request->all();
        
        $data['travel_date'] = date('Y-m-d', strtotime($data['travel_date']));
        
        if(isset($data['paid_date']))
            $data['paid_date'] = date('Y-m-d', strtotime($data['paid_date']));
        $data['booking_code'] = $this->operator->code.rand(100000,999999);
        $data['receipt_no'] = $request->receipt_no;
        $data['user_id'] = Auth::user()->id;
        $data['contact_name'] = $request->contact_name;
        $data['contact_email'] = $request->contact_email;
        $data['number_of_passengers'] = $request->number_of_passengers;
        $data['status'] =$request->status;
        $op = $this->operator->name;
        $data['user_id'] = Auth::user()->id;
        $c = CharterBooking::create($data);

        $bus = Bus::where('id',$data['bus_id'])->get(['bus_number'])->toArray();
        $park = Park::where('id',$data['park_id'])->get(['name'])->toArray();

     
     
        // send sms
        $phone = $data['contact_phone'];
        $msg = $op . " purchase notice: "." Book Code: ".$data['booking_code']. " Pickup point: ".
                $data['pickup_location'] ." Destination: ".  $data['destination'] .
                ".  Name: ".$data['contact_name'].
                ".  Park: ".$park[0]['name'].
                ".  Bus: ". $bus[0]['bus_number'].
                ".  Passengers: ". $data['number_of_passengers'].
                ".  Status: ". $data['status'].
                ".  Departure Date: ".$data['travel_date'].
                ".  Agreed Price: NGN ". $data['agreed_price'];
        $statusCode = $this->sendSms($phone,$msg);
         
         if($statusCode == 200)
         {
             Session::flash('delivery_report', 'Sms Sent!');
         }
         else
         {
            Session::flash('delivery_report', 'Sms Not Sent!');
         }


        Session::flash('flash_message', 'CharterBooking added!');

        return redirect()->action('CharterBookingsController@show', [$c->id,$statusCode]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function show($id)
    {
        $charterbooking = CharterBooking::with('bus', 'park', 'driver', 'ticketer')->findOrFail($id);
        //dd($charterbooking);
        $op =$this->operator->name;
        $opDets = explode(".",$this->operator->details);
        $opD = $opDets[0];
        $opOffice = explode(".",$this->operator->address);
        $opOff = $opOffice[0];
        $tels = explode(",",$this->operator->phone);
        $telHead = $tels[0];
       
        $imgg =$this->operator->img;
        $routes =$opDets[1];
        $condition =  $opDets[2];
        $condition2 = $opDets[3];
        $condition3 = $opDets[4];
        $condition4 = $opDets[5];
        
        $page_title = 'View charterbooking';
        return view('charter-bookings.show', compact('charterbooking', 'page_title', 'op','opD','opOff','telHead','opOff2','tel2','imgg','routes','condition','condition2','condition3','condition4'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $charterbooking = CharterBooking::findOrFail($id);

        $parks = Park::lists('name', 'id')->toArray();
        $buses = Bus::lists('number_plate', 'id')->toArray();
        $drivers = Driver::lists('name', 'id')->toArray();
        $payment_methods = PaymentMethod::lists('name', 'id')->toArray();
        $statuses = ['PENDING'=>'PENDING', 'PAID'=>'PAID', 'CANCELLED'=>'CANCELLED'];



        $page_title = 'Edit charterbooking';
        return view('charter-bookings.edit', compact('charterbooking', 'page_title', 'parks', 'buses', 'drivers', 'payment_methods', 'statuses'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        
        $charterbooking = CharterBooking::findOrFail($id);

        $data = $request->all();
        $data['travel_date'] = date('Y-m-d', strtotime($data['travel_date']));
        
        if(isset($data['paid_date']))
            $data['paid_date'] = date('Y-m-d', strtotime($data['paid_date']));

        // dd($data);

        $charterbooking->update($data);

        Session::flash('flash_message', 'CharterBooking updated!');

        return redirect()->action('CharterBookingsController@show', [$charterbooking->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        CharterBooking::destroy($id);

        Session::flash('flash_message', 'CharterBooking deleted!');

        return redirect('charter-bookings');
    }

    public function getBusDetails(Request $request){
      
        $busseats = Bus::with('bus_type')->where('id',$request->bus_id)->get()->toArray();
        return $busseats[0]['no_of_seats'];
        
  }

   public function getCharterCustDetails(Request $request){

        $phone = $request->ph;
        $booking = CharterBooking::where('contact_phone', $phone)->orderBy('created_at', 'desc')->first();
        return $booking->contact_name."|".$booking->contact_email;
  }

}
