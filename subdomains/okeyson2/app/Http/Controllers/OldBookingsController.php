<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;
use App\Libraries\Utility;
use Jenssegers\Agent\Agent;
use App\Models\Park;
use App\Models\Passenger;
use App\Models\Transaction;
use App\Models\Booking;
use App\Models\CharteredBooking;
use App\Models\Trip;
use Illuminate\Support\Facades\Redirect;

use Illuminate\Pagination\LengthAwarePaginator as Paginator;



 /**
     * Log user in 
     * @param  Request $request [the section of post to fetch, 1->original juice, 2->elsewhere, 0->all post]
     * @todo  None for now
     * @return json status,data
     */
class BookingsController extends Controller
{  

    public function postSaveBooking(Request $request){
        Utility::$response = Utility::checkParameters($request, array('trip_id', 'passenger_count', 'final_cost', 'contact_phone'));
        if(!Utility::$response['status'])
            return response()->json(Utility::$response);
        
        // return($request->next_of_kin_phone);  
        // exit(); 
        $response =  Booking::firstOrCreate([
            'trip_id'=>$request->trip_id,
            'date'=>$request->date,
            'passenger_count'=>$request->passenger_count,
            'unit_cost'=>$request->unit_cost,
            'final_cost'=>$request->final_cost,
            'paid_date'=>$request->paid_date,
            'contact_name'=>$request->contact_name,
            'contact_phone'=>$request->contact_phone,
            'next_of_kin'=>$request->next_of_kin_name,
            'next_of_kin_phone'=>$request->next_of_kin_phone,
        ]);

        if($response){
             Utility::$response['status'] = true;
            Utility::$response['data'] = $response;
            return response()->json(Utility::$response);
        }else{
            Utility::$response['status'] = false;
            return response()->json(Utility::$response);
        } 

    }

    public function postSaveCharter(Request $request){
      Utility::$response = Utility::checkParameters($request, array('operator', 'start'));
        if(!Utility::$response['status'])
            return response()->json(Utility::$response);

        $response =  CharteredBooking::firstOrCreate([
            'pickup_location' => $request->pickup,
            'destination' => $request->dropoff,
            'contact_name' => $request->contact_name,
            'travel_date' => date('Y-m-d', strtotime($request->start)),
            'operator_id' => $request->operator,
            'contact_mobile' => $request->contact_phone,
            'next_of_kin_mobile' => $request->phone_kin,
            'no_of_passengers' => $request->passenger,
            'payment_method_id' => $request->payment_method_id
        ]);

         if($response){
             Utility::$response['status'] = true;
            Utility::$response['data'] = $response;
            return response()->json(Utility::$response);
        }else{
            Utility::$response['status'] = false;
            return response()->json(Utility::$response);
        } 

    }

     public function postSavePassengers(Request $request){
        Utility::$response = Utility::checkParameters($request, array('booking_id', 'name'));
        if(!Utility::$response['status'])
            return response()->json(Utility::$response);

        $response =  Passenger::firstOrCreate([
                'booking_id'=>$request->booking_id,
                'name'=>$request->name
                ]);

        if($response){
             Utility::$response['status'] = true;
            Utility::$response['data'] = $response;
            return response()->json(Utility::$response);
        }else{
            Utility::$response['status'] = false;
            return response()->json(Utility::$response);
        }  
    }

    public function postSaveTransactions(Request $request){
        Utility::$response = Utility::checkParameters($request, array('status', 'response', 'booking_id'));
        if(!Utility::$response['status'])
            return response()->json(Utility::$response);


        $trans = new Transaction;
        $trans->booking_id = $request->booking_id;
        $trans->status = $request->status;
        $trans->response = $request->response;
        $response = $trans->save();

        if(isset($request->payment_method)){
            Booking::where('id', $request->booking_id)
              ->update(['status' => 'PAID', 'paid_date'=>date('Y-m-d H:i:s'), 'payment_method_id'=>2]);
        }


        if($response){
             Utility::$response['status'] = true;
            Utility::$response['data'] = $response;
            return response()->json(Utility::$response);
        }else{
            Utility::$response['status'] = false;
            return response()->json(Utility::$response);
        }  
    }



}