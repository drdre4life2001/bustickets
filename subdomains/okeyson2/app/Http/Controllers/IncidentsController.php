<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Incident;
use App\Models\IncidentType;
use App\Models\Bus;
use App\Models\Park;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;
use Auth;

class IncidentsController extends Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $incidents = Incident::with('incidentType', 'park', 'bus')->paginate(1000);
     
        $page_title = 'incidents';

        return view('incidents.index', compact('incidents', 'page_title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $page_title = 'Add incident';

        $incident_types = IncidentType::lists('name', 'id')->toArray();
        $buses = ['None'=>'None']+ Bus::lists('bus_number', 'id')->toArray();

        $parks = Auth::user()->parks()->lists('parks.name', 'parks.id')->toArray();;

        return view('incidents.create', compact('page_title', 'incident_types', 'buses', 'parks'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, ['incident_type_id' => 'required', ]);

        $request['operator_id'] = Auth::user()->operator_id;

        Incident::create($request->all());

        Session::flash('flash_message', 'Incident added!');

        return redirect('incidents');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function show($id)
    {
        $incident = Incident::findOrFail($id);

        $page_title = 'View incident';
        return view('incidents.show', compact('incident', 'page_title'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $incident = Incident::findOrFail($id);

        $incident_types = IncidentType::lists('name', 'id')->toArray();
        $buses = Bus::lists('bus_number', 'id')->toArray();
        $parks = Auth::user()->parks()->lists('parks.name', 'parks.id')->toArray();;

        $page_title = 'Edit incident';
        return view('incidents.edit', compact('incident', 'page_title', 'incident_types', 'buses', 'parks'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        $this->validate($request, ['incident_type_id' => 'required', ]);

        $incident = Incident::findOrFail($id);
        $incident->update($request->all());

        Session::flash('flash_message', 'Incident updated!');

        return redirect('incidents');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        Incident::destroy($id);

        Session::flash('flash_message', 'Incident deleted!');

        return redirect('incidents');
    }

}
