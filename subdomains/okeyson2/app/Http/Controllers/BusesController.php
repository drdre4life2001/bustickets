<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Illuminate\Support\Facades\Input;
use App\Models\Bus;
use App\Models\Park;
use App\Models\BusType;
use App\Models\Driver;
use App\Models\Trip; 
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;
use Excel;
use Auth;

class BusesController extends Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth');
    }




    public function UploadBuses(Request $request){


           if(Input::hasFile('userfile')){
             $path = Input::file('userfile')->getRealPath();
             $data = Excel::load($path, function($reader) {})->get();

             if(!empty($data) && $data->count()){

        foreach ($data as $key => $value) {

            if($value->number_plate == null AND $value->bus_number == null){
                continue; //bus number and platenumber cannot be null at the same time
            }

         // if destination park exists create trip
         if(!empty($value->destination_park) AND !empty($value->source_park)){
            
           //create trip names
            $source_park = strtoupper($value->source_park.' [in '. $value->source_park_state).']';
            $dest_park = strtoupper($value->destination_park.' [in '. $value->destination_park_state).']';
            $tripName = strtoupper($source_park.'_'.$dest_park);

            //check if trips exist
             $source_park_check= Park::where('name', ucfirst($source_park))->where('active', 1)->first();
             $dest_park_check = Park::where('name', ucfirst($dest_park))->where('active', 1)->first();

            //now we have the trip name, lets create it if it doesn't already exists
            if($source_park_check AND $dest_park_check){
                $createTrip =  Trip::firstOrCreate([
                                'operator_id' => Auth::user()->operator_id,
                                'name' => $trip_name,
                                'source_park_id' => $source_park_check->id,
                                'dest_park_id' => $dest_park_check->id,
                                //'fare' => $value->fare,
                                'active'=>1
                                ])->id;
                
                 //create trips
                if($createTrip){

                }

            }
               
              

         }


         //if bus model is set
         if(empty($value->bus_model)){
                  $createBusType= BusType::firstOrCreate([
                      'name' => strtoupper($value->bus_model),
                   ])->id;
         }
//create park
         if(empty($value->default_park)){
                  $createPark= Park::firstOrCreate([
                      'name' => strtoupper($value->default_park),
                   ])->id;
                   // $insertMore[] = ['default_park_id' => $createPark->id];
         }

//create driver
         if(empty($value->default_driver_phone)){
                  $createDriver= Driver::firstOrCreate([
                      'name' => strtoupper($value->default_driver_name),
                      'phone' => $value->default_driver_phone,
                   ])->id;
                   // $insertMore[] = ['default_driver_id' => $createDriver->id];
         } 


          
           //then add to $insert and save
           //$createBus = DB::table('buses')->insertGetId($insert);
//dd($createBus);
$createBusId = Bus::create(
  ['operator_id' => Auth::user()->operator_id,
             'number_plate' => $value->number_plate,
             'chassis_number' => $value->chassis_number,
             'engine_number' => $value->engine_number,
             'bus_number' => $value->bus_number,
             'no_of_seats' => $value->no_of_seats,
             'bus_roof' => $value->bus_roof,
             'vin' => $value->vin,
             'default_park'=>$value->default_park,
             'source_park' => $value->source_park,
             'source_park_state' => $value->source_state,
             'destination_park' => $value->destination_park,
             'destination_park_state' => $value->destination_state,
             'bus_model' => $value->bus_model,
             'default_driver_id'=>1,
             'other_details' => $value->other_details,

])->id;

           
           if(isset($createBusId)){
                   //update the bus

                 $busUpdate = Bus::find($createBusId);

                 if(isset($createPark->id)){
                    $busUpdate->default_park_id = $createPark;
                 }
                if(isset($createBusType)){
                    $busUpdate->bus_type_id = $createBusType;
                }
                if(isset($createDriver)){
                    $busUpdate->default_driver_id = $createDriver;
                }
                if(isset($createTrip)){
                    $busUpdate->default_trip_id = $createTrip;
                }

                if($busUpdate->save()){
                      \Session::flash('status', 'Upload was successful! Buses uploaded.');
                }

            }  


         

       }

    }
}

//return redirect()->route('buses');
return redirect()->back();
         // return view('drivers.index', compact('flash'));
}


    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $buses = Bus::with('default_driver','bus_type', 'default_park', 'default_trip')->paginate(1000);
        
        $parks = Park::orderBy('name','asc')->lists('name', 'id')->toArray();
        $bus_types = BusType::orderBy('name','asc')->lists('name', 'id')->toArray();
        $drivers = Driver::orderBy('name','asc')->lists('name', 'id')->toArray();
        $ts = Trip::get()->toArray();
        $trips = [];
        foreach ($ts as $trip) {
            $trips[$trip['id']] = str_ireplace("OKEYSON_", '', $trip['name']);
        }

        $page_title = 'buses';

//dd($buses);
        return view('buses.index', compact('buses', 'page_title', 'parks', 'bus_types', 'drivers', 'trips'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $page_title = 'Add bus';

        $parks = [null => 'None'] + Park::lists('name', 'id')->toArray();
        $bus_types = [null => 'None'] + BusType::lists('name', 'id')->toArray();
        $drivers = [null => 'None'] + Driver::lists('name', 'id')->toArray();
        $ts = Trip::get()->toArray();
        $trips = [];
        foreach ($ts as $trip) {
            $trips[$trip['id']] = str_ireplace("OKEYSON_", '', $trip['name']);
        }


        return view('buses.create', compact('page_title', 'parks', 'bus_types', 'drivers', 'trips'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {

        $d = $request->all();
        if(empty($d['default_park_id']))
            unset($d['default_park_id']);
        if(empty($d['default_trip_id']))
            unset($d['default_trip_id']);
        if(empty($d['default_driver_id']))
            unset($d['default_driver_id']);

        $this->validate($request, ['number_plate' => 'required' ]);

        Bus::create($d);

        Session::flash('flash_message', 'Bus added!');

        return redirect('buses');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function show($id)
    {
        $bus = Bus::findOrFail($id);

        $page_title = 'View bus';
        return view('buses.show', compact('bus', 'page_title'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $bus = Bus::findOrFail($id);

        $page_title = 'Edit bus';
        $parks = [null => 'None'] + Park::lists('name', 'id')->toArray();
        $bus_types = [null => 'None'] + BusType::lists('name', 'id')->toArray();
        $drivers = [null => 'None'] + Driver::lists('name', 'id')->toArray();
        $ts = Trip::get()->toArray();
        $trips = [null => 'None'];
        foreach ($ts as $trip) {
            $trips[$trip['id']] = str_ireplace("OKEYSON_", '', $trip['name']);
        }

        return view('buses.edit', compact('bus', 'page_title', 'parks', 'bus_types', 'drivers', 'trips'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        // $this->validate($request, ['number_plate' => 'required', ]);

        $bus = Bus::findOrFail($id);
        $bus->update($request->all());

        Session::flash('flash_message', 'Bus updated!');

        return redirect('buses');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        Bus::destroy($id);

        Session::flash('flash_message', 'Bus deleted!');

        return redirect('buses');
    }

    public function updateBusMileage(Request $request)
    {
      $busId =  $request->busid;
      $busmileage = $request->bus_mileage;

//dd($request->all());

      // update bus mileage
        $bus = Bus::findOrFail($busId);

        $bus->update(['bus_mileage'=>$request->bus_mileage]);

        Session::flash('flash_message', 'Bus mileage updated!');
        // redirect to daily trips
        return back()->withInput();
    }

}
