<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddExtraLuggages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('extra_luggages', function (Blueprint $table) {
            
            $table->increments('id');
            $table->integer('booking_id');
            $table->double('luggage_weight')->default(0);
            $table->double('amount');

            $table->timestamps();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('extra_luggages');
        
    }
}
