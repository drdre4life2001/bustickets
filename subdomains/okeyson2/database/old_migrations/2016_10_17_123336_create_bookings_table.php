<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBookingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('bookings', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('trip_id')->unsigned();
			$table->date('date');
			$table->integer('passenger_count');
			$table->float('unit_cost', 10, 0);
			$table->float('final_cost', 10, 0);
			$table->integer('payment_method_id')->unsigned()->nullable()->index();
			$table->integer('bank_id')->unsigned()->nullable()->index();
			$table->string('status')->default('PENDING');
			$table->string('source')->nullable();
			$table->dateTime('paid_date')->nullable();
			$table->string('booking_code')->nullable()->default('');
			$table->string('contact_name');
			$table->string('contact_phone');
			$table->string('contact_email', 50)->nullable();
			$table->text('contact_address', 65535)->nullable();
			$table->string('gender');
			$table->string('next_of_kin')->nullable()->default('');
			$table->string('next_of_kin_phone')->default('');
			$table->integer('parent_booking_id')->unsigned()->nullable()->index();
			$table->integer('user_id')->unsigned()->nullable();
			$table->integer('daily_trip_id')->unsigned()->nullable()->index();
			$table->float('luggage_weight', 10, 3);
			$table->string('receipt_no');
			$table->string('passport_no');
			$table->string('occupation');
			$table->softDeletes();
			$table->timestamps();
			$table->boolean('is_round_trip')->default(0);
			$table->string('departure_time', 40)->nullable();
			$table->integer('trip_type')->nullable()->default(0);
			$table->string('return_date', 45)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('bookings');
	}

}
