<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateParkStopsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('park_stops', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
			$table->boolean('is_active')->default(0);
			$table->integer('park_id')->unsigned()->nullable()->index();
			$table->softDeletes();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('park_stops');
	}

}
