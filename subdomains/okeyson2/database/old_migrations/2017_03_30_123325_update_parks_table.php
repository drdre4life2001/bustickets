<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateParksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('parks', function (Blueprint $table) {
          $table->integer('state_id')->unsigned()->after('operator_id');

          $table->foreign('state_id')->references('id')->on('states');

      });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
      Schema::table('parks', function (Blueprint $table) {

          $table->dropColumn('state_id');

      });
  }
}
