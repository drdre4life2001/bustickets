<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIntlFieldsToTripsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trips', function (Blueprint $table) {
            $table->boolean('is_intl_trip')->default(FALSE)->after('active');
            $table->double('virgin_passport_addition')->default(0.00)->after('is_intl_trip');
            $table->double('no_passport_addition')->default(0.00)->after('virgin_passport_addition');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trips', function (Blueprint $table) {
            
            $table->dropColumn('no_passport_addition');
            $table->dropColumn('virgin_passport_addition');
            $table->dropColumn('is_intl_trip');

        });
    }
}
