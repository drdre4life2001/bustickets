<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddHolderDimenToBusTypes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bus_types', function (Blueprint $table) {
            //
            $table->integer('holder_width')->default(573)->after('seat_height');
            $table->integer('holder_height')->default(317)->after('holder_width');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bus_types', function (Blueprint $table) {
            //
            $table->dropColumn("holder_width");
            $table->dropColumn("holder_height");
        });
    }
}
