<?php
/**
 * CharterTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Bustickets.ng API Documentation
 *
 * Manage your bus tickets with bustickets.ng
 *
 * OpenAPI spec version: 1.0.0
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace Swagger\Client;

/**
 * CharterTest Class Doc Comment
 *
 * @category    Class */
// * @description Charter
/**
 * @package     Swagger\Client
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class CharterTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {

    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {

    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {

    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {

    }

    /**
     * Test "Charter"
     */
    public function testCharter()
    {

    }

    /**
     * Test attribute "pickup"
     */
    public function testPropertyPickup()
    {

    }

    /**
     * Test attribute "dropoff"
     */
    public function testPropertyDropoff()
    {

    }

    /**
     * Test attribute "contact_name"
     */
    public function testPropertyContactName()
    {

    }

    /**
     * Test attribute "start"
     */
    public function testPropertyStart()
    {

    }

    /**
     * Test attribute "operator"
     */
    public function testPropertyOperator()
    {

    }

    /**
     * Test attribute "phone_kin"
     */
    public function testPropertyPhoneKin()
    {

    }

    /**
     * Test attribute "kin"
     */
    public function testPropertyKin()
    {

    }

    /**
     * Test attribute "passenger"
     */
    public function testPropertyPassenger()
    {

    }

    /**
     * Test attribute "budget"
     */
    public function testPropertyBudget()
    {

    }

    /**
     * Test attribute "pick_up_address"
     */
    public function testPropertyPickUpAddress()
    {

    }

    /**
     * Test attribute "drop_off_address"
     */
    public function testPropertyDropOffAddress()
    {

    }

    /**
     * Test attribute "departure_date"
     */
    public function testPropertyDepartureDate()
    {

    }

}
