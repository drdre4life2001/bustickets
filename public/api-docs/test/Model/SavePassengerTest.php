<?php
/**
 * SavePassengerTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Bustickets.ng API Documentation
 *
 * Manage your bus tickets with bustickets.ng
 *
 * OpenAPI spec version: 1.0.0
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace Swagger\Client;

/**
 * SavePassengerTest Class Doc Comment
 *
 * @category    Class */
// * @description SavePassenger
/**
 * @package     Swagger\Client
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class SavePassengerTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {

    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {

    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {

    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {

    }

    /**
     * Test "SavePassenger"
     */
    public function testSavePassenger()
    {

    }

    /**
     * Test attribute "trip_id"
     */
    public function testPropertyTripId()
    {

    }

    /**
     * Test attribute "contact_name"
     */
    public function testPropertyContactName()
    {

    }

    /**
     * Test attribute "seat"
     */
    public function testPropertySeat()
    {

    }

    /**
     * Test attribute "gender"
     */
    public function testPropertyGender()
    {

    }

    /**
     * Test attribute "booking_id"
     */
    public function testPropertyBookingId()
    {

    }

}
