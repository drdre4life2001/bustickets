<?php
/**
 * SaveSeat
 *
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swaagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Bustickets.ng API Documentation
 *
 * Manage your bus tickets with bustickets.ng
 *
 * OpenAPI spec version: 1.0.0
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Swagger\Client\Model;

use \ArrayAccess;

/**
 * SaveSeat Class Doc Comment
 *
 * @category    Class
 * @package     Swagger\Client
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class SaveSeat implements ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      * @var string
      */
    protected static $swaggerModelName = 'saveSeat';

    /**
      * Array of property to type mappings. Used for (de)serialization
      * @var string[]
      */
    protected static $swaggerTypes = [
        'trip_id' => 'int',
        'seat_no' => 'int',
        'departure_date' => 'string',
        'booking_id' => 'int'
    ];

    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    /**
     * Array of attributes where the key is the local name, and the value is the original name
     * @var string[]
     */
    protected static $attributeMap = [
        'trip_id' => 'trip_id',
        'seat_no' => 'seat_no',
        'departure_date' => 'departure_date',
        'booking_id' => 'booking_id'
    ];


    /**
     * Array of attributes to setter functions (for deserialization of responses)
     * @var string[]
     */
    protected static $setters = [
        'trip_id' => 'setTripId',
        'seat_no' => 'setSeatNo',
        'departure_date' => 'setDepartureDate',
        'booking_id' => 'setBookingId'
    ];


    /**
     * Array of attributes to getter functions (for serialization of requests)
     * @var string[]
     */
    protected static $getters = [
        'trip_id' => 'getTripId',
        'seat_no' => 'getSeatNo',
        'departure_date' => 'getDepartureDate',
        'booking_id' => 'getBookingId'
    ];

    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    public static function setters()
    {
        return self::$setters;
    }

    public static function getters()
    {
        return self::$getters;
    }

    

    

    /**
     * Associative array for storing property values
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     * @param mixed[] $data Associated array of property values initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['trip_id'] = isset($data['trip_id']) ? $data['trip_id'] : null;
        $this->container['seat_no'] = isset($data['seat_no']) ? $data['seat_no'] : null;
        $this->container['departure_date'] = isset($data['departure_date']) ? $data['departure_date'] : null;
        $this->container['booking_id'] = isset($data['booking_id']) ? $data['booking_id'] : null;
    }

    /**
     * show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalid_properties = [];

        if ($this->container['trip_id'] === null) {
            $invalid_properties[] = "'trip_id' can't be null";
        }
        if ($this->container['seat_no'] === null) {
            $invalid_properties[] = "'seat_no' can't be null";
        }
        if ($this->container['departure_date'] === null) {
            $invalid_properties[] = "'departure_date' can't be null";
        }
        if ($this->container['booking_id'] === null) {
            $invalid_properties[] = "'booking_id' can't be null";
        }
        return $invalid_properties;
    }

    /**
     * validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {

        if ($this->container['trip_id'] === null) {
            return false;
        }
        if ($this->container['seat_no'] === null) {
            return false;
        }
        if ($this->container['departure_date'] === null) {
            return false;
        }
        if ($this->container['booking_id'] === null) {
            return false;
        }
        return true;
    }


    /**
     * Gets trip_id
     * @return int
     */
    public function getTripId()
    {
        return $this->container['trip_id'];
    }

    /**
     * Sets trip_id
     * @param int $trip_id id of trip
     * @return $this
     */
    public function setTripId($trip_id)
    {
        $this->container['trip_id'] = $trip_id;

        return $this;
    }

    /**
     * Gets seat_no
     * @return int
     */
    public function getSeatNo()
    {
        return $this->container['seat_no'];
    }

    /**
     * Sets seat_no
     * @param int $seat_no Selected Seat Number
     * @return $this
     */
    public function setSeatNo($seat_no)
    {
        $this->container['seat_no'] = $seat_no;

        return $this;
    }

    /**
     * Gets departure_date
     * @return string
     */
    public function getDepartureDate()
    {
        return $this->container['departure_date'];
    }

    /**
     * Sets departure_date
     * @param string $departure_date
     * @return $this
     */
    public function setDepartureDate($departure_date)
    {
        $this->container['departure_date'] = $departure_date;

        return $this;
    }

    /**
     * Gets booking_id
     * @return int
     */
    public function getBookingId()
    {
        return $this->container['booking_id'];
    }

    /**
     * Sets booking_id
     * @param int $booking_id id of booking
     * @return $this
     */
    public function setBookingId($booking_id)
    {
        $this->container['booking_id'] = $booking_id;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     * @param  integer $offset Offset
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     * @param  integer $offset Offset
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     * @param  integer $offset Offset
     * @param  mixed   $value  Value to be set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     * @param  integer $offset Offset
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(\Swagger\Client\ObjectSerializer::sanitizeForSerialization($this), JSON_PRETTY_PRINT);
        }

        return json_encode(\Swagger\Client\ObjectSerializer::sanitizeForSerialization($this));
    }
}


