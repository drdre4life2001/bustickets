# Swagger\Client\CustomersApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getCustomerDataFromPhonePost**](CustomersApi.md#getCustomerDataFromPhonePost) | **POST** /get-customer-data-from-phone | Get customer&#39;s data
[**saveBookedSeatPost**](CustomersApi.md#saveBookedSeatPost) | **POST** /save-booked-seat | Save booked seat
[**saveCustomerPost**](CustomersApi.md#saveCustomerPost) | **POST** /save-customer | save booking&#39;s made by customer
[**savePsgPost**](CustomersApi.md#savePsgPost) | **POST** /save-psg | save passenger&#39;s details


# **getCustomerDataFromPhonePost**
> \Swagger\Client\Model\InlineResponse2008[] getCustomerDataFromPhonePost($phone)

Get customer's data

Provides details of customer from their registered phone number

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\CustomersApi();
$phone = new \Swagger\Client\Model\Phone(); // \Swagger\Client\Model\Phone | An array of text strings

try {
    $result = $api_instance->getCustomerDataFromPhonePost($phone);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomersApi->getCustomerDataFromPhonePost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **phone** | [**\Swagger\Client\Model\Phone**](../Model/.md)| An array of text strings | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse2008[]**](../Model/InlineResponse2008.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **saveBookedSeatPost**
> saveBookedSeatPost($save_seat)

Save booked seat

save seat's booked by customer

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\CustomersApi();
$save_seat = new \Swagger\Client\Model\SaveSeat(); // \Swagger\Client\Model\SaveSeat | 

try {
    $api_instance->saveBookedSeatPost($save_seat);
} catch (Exception $e) {
    echo 'Exception when calling CustomersApi->saveBookedSeatPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **save_seat** | [**\Swagger\Client\Model\SaveSeat**](../Model/\Swagger\Client\Model\SaveSeat.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **saveCustomerPost**
> \Swagger\Client\Model\InlineResponse20010[] saveCustomerPost($save_customer)

save booking's made by customer

Save Customer's details

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\CustomersApi();
$save_customer = new \Swagger\Client\Model\SaveCustomer(); // \Swagger\Client\Model\SaveCustomer | 

try {
    $result = $api_instance->saveCustomerPost($save_customer);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomersApi->saveCustomerPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **save_customer** | [**\Swagger\Client\Model\SaveCustomer**](../Model/\Swagger\Client\Model\SaveCustomer.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse20010[]**](../Model/InlineResponse20010.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **savePsgPost**
> \Swagger\Client\Model\InlineResponse20011[] savePsgPost($save_passenger)

save passenger's details

Save Customer's details

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\CustomersApi();
$save_passenger = new \Swagger\Client\Model\SavePassenger(); // \Swagger\Client\Model\SavePassenger | 

try {
    $result = $api_instance->savePsgPost($save_passenger);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomersApi->savePsgPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **save_passenger** | [**\Swagger\Client\Model\SavePassenger**](../Model/\Swagger\Client\Model\SavePassenger.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse20011[]**](../Model/InlineResponse20011.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

