# Swagger\Client\BankAccountsApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**banksGet**](BankAccountsApi.md#banksGet) | **GET** /banks | Bank Accounts


# **banksGet**
> \Swagger\Client\Model\InlineResponse2002[] banksGet()

Bank Accounts

This endpoint gives the list of bank accounts used by Transport Company Limited for all Bus.com.ng and Bustickets.ng operations

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\BankAccountsApi();

try {
    $result = $api_instance->banksGet();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BankAccountsApi->banksGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Swagger\Client\Model\InlineResponse2002[]**](../Model/InlineResponse2002.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

