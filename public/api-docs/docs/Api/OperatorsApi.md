# Swagger\Client\OperatorsApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getOperatorTripPost**](OperatorsApi.md#getOperatorTripPost) | **POST** /get-operator-trip | Get operators trip
[**getOperatorsPost**](OperatorsApi.md#getOperatorsPost) | **POST** /get-operators | Operators


# **getOperatorTripPost**
> \Swagger\Client\Model\InlineResponse2003[] getOperatorTripPost()

Get operators trip

Get trips available for each operators

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\OperatorsApi();

try {
    $result = $api_instance->getOperatorTripPost();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OperatorsApi->getOperatorTripPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Swagger\Client\Model\InlineResponse2003[]**](../Model/InlineResponse2003.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getOperatorsPost**
> \Swagger\Client\Model\InlineResponse2001[] getOperatorsPost()

Operators

This Operators endpoint returns information about operators. Example of operators are Okeyson, Efex, Ifex, Cross-Country and so on.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\OperatorsApi();

try {
    $result = $api_instance->getOperatorsPost();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OperatorsApi->getOperatorsPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Swagger\Client\Model\InlineResponse2001[]**](../Model/InlineResponse2001.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

