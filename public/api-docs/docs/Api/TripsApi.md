# Swagger\Client\TripsApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getCustomerBookingIdPost**](TripsApi.md#getCustomerBookingIdPost) | **POST** /get-customer-booking-id | trips from booking id
[**getDestsPost**](TripsApi.md#getDestsPost) | **POST** /get-dests | Destinations
[**getTripPost**](TripsApi.md#getTripPost) | **POST** /get-trip | Get details of available trips
[**saveBookingPost**](TripsApi.md#saveBookingPost) | **POST** /save-booking | save booking&#39;s made by customer
[**searchTripPost**](TripsApi.md#searchTripPost) | **POST** /search-trip | Search for available trips


# **getCustomerBookingIdPost**
> \Swagger\Client\Model\InlineResponse2007[] getCustomerBookingIdPost($save_customer)

trips from booking id

Save trips by booking id

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\TripsApi();
$save_customer = new \Swagger\Client\Model\SaveCustomer1(); // \Swagger\Client\Model\SaveCustomer1 | 

try {
    $result = $api_instance->getCustomerBookingIdPost($save_customer);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TripsApi->getCustomerBookingIdPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **save_customer** | [**\Swagger\Client\Model\SaveCustomer1**](../Model/\Swagger\Client\Model\SaveCustomer1.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse2007[]**](../Model/InlineResponse2007.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getDestsPost**
> \Swagger\Client\Model\InlineResponse2005[] getDestsPost($source_park_id)

Destinations

This Destination endpoint returns the list of destination parks in Alphabetical order.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\TripsApi();
$source_park_id = 56; // int | The ID of the park that you are retrieving its destination.

try {
    $result = $api_instance->getDestsPost($source_park_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TripsApi->getDestsPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **source_park_id** | **int**| The ID of the park that you are retrieving its destination. |

### Return type

[**\Swagger\Client\Model\InlineResponse2005[]**](../Model/InlineResponse2005.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getTripPost**
> \Swagger\Client\Model\InlineResponse2007[] getTripPost($save_booking)

Get details of available trips

This endpoint gets available trips

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\TripsApi();
$save_booking = new \Swagger\Client\Model\SaveBooking(); // \Swagger\Client\Model\SaveBooking | 

try {
    $result = $api_instance->getTripPost($save_booking);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TripsApi->getTripPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **save_booking** | [**\Swagger\Client\Model\SaveBooking**](../Model/\Swagger\Client\Model\SaveBooking.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse2007[]**](../Model/InlineResponse2007.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **saveBookingPost**
> \Swagger\Client\Model\InlineResponse2009[] saveBookingPost($save_booking)

save booking's made by customer

Save the booking's made by customer

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\TripsApi();
$save_booking = new \Swagger\Client\Model\SaveBooking1(); // \Swagger\Client\Model\SaveBooking1 | 

try {
    $result = $api_instance->saveBookingPost($save_booking);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TripsApi->saveBookingPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **save_booking** | [**\Swagger\Client\Model\SaveBooking1**](../Model/\Swagger\Client\Model\SaveBooking1.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse2009[]**](../Model/InlineResponse2009.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **searchTripPost**
> \Swagger\Client\Model\InlineResponse2006[] searchTripPost($from_park, $to_park)

Search for available trips

This endpoint search for available trips

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\TripsApi();
$from_park = 56; // int | The source park.
$to_park = 56; // int | The destination park.

try {
    $result = $api_instance->searchTripPost($from_park, $to_park);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TripsApi->searchTripPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **from_park** | **int**| The source park. |
 **to_park** | **int**| The destination park. |

### Return type

[**\Swagger\Client\Model\InlineResponse2006[]**](../Model/InlineResponse2006.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

