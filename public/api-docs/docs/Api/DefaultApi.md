# Swagger\Client\DefaultApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**saveCharterPost**](DefaultApi.md#saveCharterPost) | **POST** /save-charter | Save Charter tags:  - Trips


# **saveCharterPost**
> \Swagger\Client\Model\InlineResponse20012[] saveCharterPost($charter)

Save Charter tags:  - Trips

save the charter

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\DefaultApi();
$charter = new \Swagger\Client\Model\Charter(); // \Swagger\Client\Model\Charter | 

try {
    $result = $api_instance->saveCharterPost($charter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DefaultApi->saveCharterPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **charter** | [**\Swagger\Client\Model\Charter**](../Model/\Swagger\Client\Model\Charter.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse20012[]**](../Model/InlineResponse20012.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

