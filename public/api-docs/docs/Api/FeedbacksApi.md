# Swagger\Client\FeedbacksApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getFeedbacksPost**](FeedbacksApi.md#getFeedbacksPost) | **POST** /get-feedbacks | Feedbacks sent by customers


# **getFeedbacksPost**
> \Swagger\Client\Model\InlineResponse2004[] getFeedbacksPost()

Feedbacks sent by customers

This endpoint gives the list of feedback messages sent by customers on bus.com.ng

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\FeedbacksApi();

try {
    $result = $api_instance->getFeedbacksPost();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling FeedbacksApi->getFeedbacksPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Swagger\Client\Model\InlineResponse2004[]**](../Model/InlineResponse2004.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

