# Swagger\Client\ParksApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getParksPost**](ParksApi.md#getParksPost) | **POST** /get-parks | Trips
[**singleParksPost**](ParksApi.md#singleParksPost) | **POST** /single-parks | Destinations


# **getParksPost**
> \Swagger\Client\Model\InlineResponse200[] getParksPost()

Trips

This Parks endpoint returns information about parks. The response includes the name of each park and lists the parks in alphabetical order.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\ParksApi();

try {
    $result = $api_instance->getParksPost();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ParksApi->getParksPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Swagger\Client\Model\InlineResponse200[]**](../Model/InlineResponse200.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **singleParksPost**
> \Swagger\Client\Model\InlineResponse2005[] singleParksPost($park_id)

Destinations

Details of a particular park.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\ParksApi();
$park_id = 56; // int | The ID of the park.

try {
    $result = $api_instance->singleParksPost($park_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ParksApi->singleParksPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **park_id** | **int**| The ID of the park. |

### Return type

[**\Swagger\Client\Model\InlineResponse2005[]**](../Model/InlineResponse2005.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

