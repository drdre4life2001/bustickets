# SaveSeat

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**trip_id** | **int** | id of trip | 
**seat_no** | **int** | Selected Seat Number | 
**departure_date** | **string** |  | 
**booking_id** | **int** | id of booking | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


