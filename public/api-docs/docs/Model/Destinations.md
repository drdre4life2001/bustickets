# Destinations

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | **int** | status code of transaction | [optional] 
**data** | [**null[]**](.md) | An array of information containing destination id and destination name | [optional] 
**message** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


