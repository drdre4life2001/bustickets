# InlineResponse2009

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | **int** | status code of transaction | [optional] 
**data** | [**null[]**](.md) | An array of information of operators, parks and trips | [optional] 
**message** | **string** | response expected after successful transactions | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


