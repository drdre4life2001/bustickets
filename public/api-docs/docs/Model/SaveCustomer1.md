# SaveCustomer1

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**booking_code** | **string** | booking code | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


