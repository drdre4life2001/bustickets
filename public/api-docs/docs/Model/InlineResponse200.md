# InlineResponse200

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | **int** | A response of the transaction | 
**data** | **int** | An array of information containing park id, state id, address, boardable, deleted_at, created_at and updated_at. Also checks if park accepts nysc transportation. | 
**message** | **string** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


