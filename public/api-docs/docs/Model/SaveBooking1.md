# SaveBooking1

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**trip_id** | **int** |  | 
**date** | **string** |  | 
**unit_cost** | **int** |  | 
**final_cost** | **int** |  | 
**contact_name** | **string** |  | 
**contact_email** | **string** |  | 
**contact_phone** | **string** |  | 
**next_of_kin_name** | **string** |  | 
**customer_id** | **string** |  | 
**gender** | **string** |  | 
**departure_date** | **string** |  | 
**passport_cost** | **string** |  | 
**next_of_kin_phone** | **string** |  | 
**source_park_id** | **int** |  | 
**seat** | **int** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


