# Charter

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**pickup** | **string** |  | 
**dropoff** | **string** |  | 
**contact_name** | **string** |  | 
**start** | **string** |  | 
**operator** | **int** |  | 
**phone_kin** | **string** |  | 
**kin** | **string** |  | 
**passenger** | **int** |  | 
**budget** | **int** |  | 
**pick_up_address** | **string** |  | 
**drop_off_address** | **string** |  | 
**departure_date** | **string** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


