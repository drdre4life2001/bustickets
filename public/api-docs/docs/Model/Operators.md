# Operators

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Unique identifier representing a specific operator. | [optional] 
**name** | **string** | The name of the park, in capital letters. | [optional] 
**img** | **string** | The name of the image logo of the operator. Example, okeyson.jpg | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


