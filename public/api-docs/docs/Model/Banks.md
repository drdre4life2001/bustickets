# Banks

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | **int** | status code of transaction | [optional] 
**data** | [**null[]**](.md) | An array of information of banks and cash  offices containing id, name, account_number, account_name, rank, active park, deleted_at, created_at and updated_at | [optional] 
**message** | **string** | response expected after successful transactions | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


