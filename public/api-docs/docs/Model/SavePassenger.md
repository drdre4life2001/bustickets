# SavePassenger

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**trip_id** | **int** | id of trip | 
**contact_name** | **string** | Passenger Name | 
**seat** | **int** | Selected Seat Number | 
**gender** | **string** |  | 
**booking_id** | **int** | id of booking | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


