<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Route::get('/', function () {
//     return view('backend.layout.app');
// });

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

//Route::controller('online', 'OnlineBookingApiController');
// Route::controller('auth', 'AuthController');


Route::match(['get', 'post'], 'auth-user', ['uses' => 'ApiController@postLogin', 'as' => 'auth-user'])/*->middleware('api.auth')*/;
Route::match(['get', 'post'], 'get-daily-trips', ['uses' => 'ApiController@postGetDailyTrips', 'as' => 'get-daily-trips'])/*->middleware('token.auth')*/;
Route::match(['get', 'post'], 'book-trip', ['uses' => 'ApiController@BookTrip', 'as' => 'auth-user'])/*->middleware('token.auth')*/;
Route::match(['get', 'post'], 'my-bookings', ['uses' => 'ApiController@postGetTicketerBookings', 'as' => 'my-bookings'])/*->middleware('token.auth')*/;
Route::match(['get', 'post'], 'seats-buses', ['uses' => 'ApiController@postGetDailyTripsSeats', 'as' => 'seats-buses'])/*->middleware('token.auth')*/;

Route::group(['middleware' => 'web'], function () {

    Route::match(['get', 'post'], 'passenger-bookings/view', ['uses' => 'BookingsController@viewAllBookingsByAPassenger', 'as' => 'passenger-booking']);


//api routes
    Route::group(['prefix' => 'api/v1/', 'middleware' => 'auth:api'], function () {
        //get all parks
        Route::post('/get-parks', ['uses' => 'OnlineBookingApiController@postGetParks']);
        //get all active operators
        Route::post('/get-operators', ['uses' => 'OnlineBookingApiController@postOperators']);

        //search for trips
        Route::post('/search', ['uses' => 'OnlineBookingApiController@postSearchTrip']);

        //get one trip
        Route::post('/get-trip', ['uses' => 'OnlineBookingApiController@postGetTrip']);
        Route::post('/get-feedbacks', ['uses' => 'OnlineBookingApiController@postGetCustomerFeedBack']);

        //get a trip from its booking ID
        Route::post('/trip-from-booking-id', ['uses' => 'OnlineBookingApiController@postGetTripFromBookingId']);
        Route::post('/get-trip-from-chartered-booking-id', ['uses' => 'OnlineBookingApiController@postGetTripFromCharteredBookingId']);
        //save booking
        Route::post('/save-booking', ['uses' => 'OnlineBookingApiController@postSaveBooking']);
        //save charter
        Route::post('/save-charter', ['uses' => 'OnlineBookingApiController@postSaveCharter']);
        Route::post('/save-charter', ['uses' => 'OnlineBookingApiController@postSaveTransactions']);

        //save passengers
        Route::post('/save-passengers', ['uses' => 'OnlineBookingApiController@postSavePassengers']);
        Route::post('/save-customer', ['uses' => 'OnlineBookingApiController@postSaveCustomer']);
        Route::post('/get-customer-bookings', ['uses' => 'OnlineBookingApiController@postGetCustomerBookings']);
        Route::post('/get-customer-data', ['uses' => 'OnlineBookingApiController@postGetCustomerStats']);
        Route::post('/save-customer-feed-back', ['uses' => 'OnlineBookingApiController@postSaveCustomerFeedBack']);
        Route::post('/save-customer-feedback', ['uses' => 'OnlineBookingApiController@postSaveCustomerFeedBack']);

        Route::post('/get-operator-booking-rule', ['uses' => 'OnlineBookingApiController@postGetOperatorBookingRule']);
        Route::post('/save-booked-seat', ['uses' => 'OnlineBookingApiController@postSaveBookedSeat']);
        Route::post('/get-customer-booking-by-id', ['uses' => 'OnlineBookingApiController@postGetCustomerBookingById']);

        //trips
        Route::post('/trips/get-parks', ['uses' => 'OnlineBookingApiController@postGetCustomerBookingById']);

    });

    Route::match(['get', 'post'], 'drivers/upload', ['uses' => 'DriversController@UploadDrivers']);
    Route::match(['get', 'post'], 'buses/upload', ['uses' => 'BusesController@UploadBuses']);
    Route::match(['get', 'post'], 'trips/upload', ['uses' => 'TripsController@UploadTrips']);

    Route::auth();

    Route::match(['get', 'post'], 'upload-trip', ['uses' => 'TripsController@UploadTrips', 'as' => 'upload-trip']);
    Route::match(['get', 'post'], 'upload-file', ['uses' => 'TripsController@uploadfile', 'as' => 'upload-file']);
    Route::match(['get', 'post'], 'trips/search', ['uses' => 'TripsController@Search', 'as' => 'search']);
    Route::match(['get', 'post'], 'trips/adjust_prices', ['uses' => 'TripsController@AdjustPrices', 'as' => 'adjust-prices']);
    Route::match(['get', 'post'], 'booking/add/{tripid}/{date}/{is_flexi?}/{dt_id?}', ['uses' => 'BookingsController@AdminBook', 'as' => 'admin-book']);
    Route::match(['get', 'post'], 'booking/flexibook/{tripid}/{date}', ['uses' => 'BookingsController@FlexiBook', 'as' => 'flexi-book']);
    Route::match(['get', 'post'], 'quick-book', ['uses' => 'BookingsController@NewBook', 'as' => 'new-book']);
    Route::match(['get', 'post'], 'book-details/{dt_id}/{trip_id}', ['uses' => 'BookingsController@BookDetails', 'as' => 'book-details']);


    Route::match(['get', 'post'], 'booking/agent/{user}', ['uses' => 'BookingsController@AgentBooking', 'as' => 'agent-booking']);


    Route::match(['get', 'post'], 'booking/cancel/{code}', ['uses' => 'BookingsController@cancel', 'as' => 'cancel-booking']);
    Route::match(['get', 'post'], 'bookings/status', ['uses' => 'BookingsController@Status', 'as' => 'status']);
    Route::match(['get', 'post'], 'bookings/notes', ['uses' => 'BookingsController@Note', 'as' => 'booking-note']);
    Route::match(['get', 'post'], 'bookings/add-extra-luggage', ['uses' => 'BookingsController@AddExtraLuggage', 'as' => 'add-extra-luggage']);

    Route::match(['get', 'post'], 'bookings/update-status', ['uses' => 'BookingsController@UpdateStatus', 'as' => 'update-status']);
    Route::match(['get', 'post'], 'bookings/status/{id}', ['uses' => 'BookingsController@Status', 'as' => 'status']);

//Route::match(['get', 'post'], 'invoice-reports', ['uses' => 'BookingsController@invoiceReports', 'as' => 'invoice-reports']);

    Route::match(['get', 'post'], 'invoice-reports/{status?}/{park_id?}', ['uses' => 'BookingsController@invoiceReports', 'as' => 'invoice-reports']);
    Route::match(['get', 'post'], 'summarised-invoice-reports', ['uses' => 'BookingsController@summarised_invoice_reports', 'as' => 'summarised-invoice-reports']);
    Route::match(['get', 'post'], 'traveler-reports/{park_id?}', ['uses' => 'BookingsController@TravelerReports', 'as' => 'traveler-reports']);
    Route::match(['get', 'post'], 'booking-reports/{status?}/{park_id?}', ['uses' => 'BookingsController@BookingReports', 'as' => 'booking-reports']);
    Route::match(['get', 'post'], 'charter-reports/{status?}/{park_id?}', ['uses' => 'BookingsController@CharterBookingReports', 'as' => 'charter-reports']);
    Route::match(['get', 'post'], 'ticketers-reports/{status?}/{park_id?}/{dest_id?}', ['uses' => 'BookingsController@TicketersReports', 'as' => 'ticketers-reports']);
    Route::match(['get', 'post'], 'manifest-reports/{status?}/{park_id?}', ['uses' => 'BookingsController@ManifestReports', 'as' => 'manifest-reports']);
    Route::match(['get', 'post'], 'park-reports/{park_id?}/{dest_id?}', ['uses' => 'BookingsController@ParkReports', 'as' => 'park-reports']);
    Route::match(['get', 'post'], 'payment-reports/{sel_p_id?}/{park_id?}/{dest_id?}', ['uses' => 'BookingsController@PaymentReports', 'as' => 'payment-reports']);
    Route::match(['get', 'post'], 'trips-reports/{status?}/{park_id?}', ['uses' => 'BookingsController@TripsReports', 'as' => 'trips-reports']);
    Route::match(['get', 'post'], 'send_sms/{status?}/{park_id?}', ['uses' => 'BookingsController@chairman_sms', 'as' => 'send_sms']);
    Route::match(['get', 'post'], 'sent_sms/{status?}/{park_id?}', ['uses' => 'SmsController@sent_sms', 'as' => 'sent_sms']);

    Route::match(['get', 'post'], 'destinations-reports/{status?}/{park_id?}', ['uses' => 'BookingsController@destinationsReports', 'as' => 'destinations-reports']);
    Route::match(['get', 'post'], 'drivers-reports/{status?}/{park_id?}', ['uses' => 'BookingsController@DriversReports', 'as' => 'drivers-reports']);
    Route::match(['get', 'post'], 'vehicles-reports/{status?}/{park_id?}', ['uses' => 'BookingsController@BusesReports', 'as' => 'vehicles-reports']);

    Route::match(['get', 'post'], 'incident-reports/{type_id?}/{bus_id?}/{park_id?}', ['uses' => 'BookingsController@IncidentReports', 'as' => 'incident-reports']);
    Route::match(['get', 'post'], 'luggages-reports/{luggage_range?}', ['uses' => 'BookingsController@LuggagesReports', 'as' => 'luggages-reports']);


    Route::match(['get', 'post'], 'transload', ['uses' => 'BookingsController@Transload', 'as' => 'transload']);
    Route::match(['get', 'post'], 'transload-bus-details', ['uses' => 'BookingsController@TransloadBusDetails', 'as' => 'transload-bus_details']);

    Route::match(['get', 'post'], 'get-phone-bookings-count', ['uses' => 'BookingsController@PhoneBookingCount', 'as' => 'get-phone-bookings-count']);


    Route::match(['get', 'post'], 'get-bus-trips', ['uses' => 'BookingsController@GetBusTrips', 'as' => 'get-bus-trips']);
    Route::match(['get', 'post'], 'get-bus-manifests', ['uses' => 'BookingsController@GetBusManifests', 'as' => 'get-bus-manifests']);
    Route::match(['get', 'post'], 'get-bus-trip-details', ['uses' => 'BookingsController@GetBusTripDetails', 'as' => 'get-bus-trip-details']);
    Route::match(['get', 'post'], 'get-drivers-trips', ['uses' => 'BookingsController@GetDriversTrips', 'as' => 'get-drivers-trips']);
    Route::match(['get', 'post'], 'get-ticketers-bookings', ['uses' => 'BookingsController@getTicketersBookings', 'as' => 'get-ticketers-bookings']);

    Route::match(['get', 'post'], 'get-payment-bookings', ['uses' => 'BookingsController@getPaymentBookings', 'as' => 'get-payment-bookings']);

    Route::match(['get', 'post'], 'get-park-bookings', ['uses' => 'BookingsController@getParkBookings', 'as' => 'get-park-bookings']);

    Route::match(['get', 'post'], 'trip-schedules/{park_id?}/{start_date?}', ['uses' => 'DailyTripsController@tripSchedules', 'as' => 'trip-schedules']);
    Route::match(['get', 'post'], 'driver-dest-report/{park_id?}', ['uses' => 'DailyTripsController@driversDestRport', 'as' => 'driver-dest-report']);
    Route::match(['get', 'post'], 'update-trip-schedule/{daily_trip_id?}', ['uses' => 'DailyTripsController@addTripSchedule', 'as' => 'update-trip-schedule']);
    Route::match(['get', 'post'], 'remove-bus-from-schedule/{daily_trip_id?}/{date}', ['uses' => 'DailyTripsController@removeBusFromSchedule', 'as' => 'remove-bus-from-schedule']);

    Route::match(['get', 'post'], 'manifest/{d_trip_id?}/{trip_type?}', ['uses' => 'DailyTripsController@manifest', 'as' => 'manifest']);


    Route::match(['get', 'post'], 'tick-report/{d_trip_id?}', ['uses' => 'DailyTripsController@generateTicketerReport', 'as' => 'tick-report']);

    Route::match(['get', 'post'], 'populate-empty-trips/{start_date}', ['uses' => 'DailyTripsController@populateEmptyTrips', 'as' => 'populate-empty-trips']);
    Route::match(['get', 'post'], 'translate-prev-trip/{start_date}', ['uses' => 'DailyTripsController@translatePrevTrip', 'as' => 'translate-prev-trip']);
    Route::match(['get', 'post'], 'get-user-park', ['uses' => 'BookingsController@getUserPark', 'as' => 'get-user-park']);


    Route::get('/', 'UsersController@dashboard');
    Route::get('/dashboard/{daterange?}', 'UsersController@dashboard');

    Route::match(['get', 'post'], 'change-password', ['uses' => 'UsersController@ChangePassword', 'as' => 'change-password']);

    Route::resource('state', 'StateController');
    Route::resource('roles', 'RolesController');
    Route::resource('trips', 'TripsController');
    Route::resource('trip-stops', 'TripStopsController');


    Route::match(['get', 'post'], 'parks/select/{id}', ['uses' => 'ParksController@Select', 'as' => 'select']);
    Route::match(['get', 'post'], 'operators/select/{id}', ['uses' => 'OperatorsController@Select', 'as' => 'select']);
    Route::match(['get', 'post'], 'get-cust-details', ['uses' => 'BookingsController@getCustDetails', 'as' => 'get-cust-details']);
    Route::match(['get', 'post'], 'get-charter-cust-details', ['uses' => 'CharterBookingsController@getCharterCustDetails', 'as' => 'get-charter-cust-details']);
    Route::match(['get', 'post'], 'get-bus-details', ['uses' => 'BookingsController@getBusDetails', 'as' => 'get-bus-details']);

    Route::match(['get', 'post'], 'get-charter-bus-details', ['uses' => 'CharterBookingsController@getBusDetails', 'as' => 'get-charter-bus-details']);

    Route::match(['get', 'post'], 'get-trip-details', ['uses' => 'TripsController@getTripDetails', 'as' => 'get-trip-details']);
    Route::match(['get', 'post'], 'get-trip-dets', ['uses' => 'DailyTripsController@getTripDets', 'as' => 'get-trip-dets']);
    Route::match(['get', 'post'], 'check-trip', ['uses' => 'TripsController@tripExists', 'as' => 'check-trip']);
    Route::match(['get', 'post'], 'get-all-trip-details', ['uses' => 'TripsController@getAllTripDetails', 'as' => 'get-all-trip-details']);
    Route::match(['get', 'post'], 'save-trip-stops', ['uses' => 'TripsController@saveTripStops', 'as' => 'save-trip-stops']);
    Route::match(['get', 'post'], 'get-trip-stops', ['uses' => 'TripsController@getTripStops', 'as' => 'get-trip-stops']);
    Route::match(['get', 'post'], 'send-sms', ['uses' => 'BookingsController@sendSms', 'as' => 'send-sms']);
    Route::match(['get', 'post'], 'get-trip-from', ['uses' => 'TripsController@getTripFrom', 'as' => 'get-trip-from']);
    Route::match(['get', 'post'], 'get-trip-to', ['uses' => 'TripsController@getTripTo', 'as' => 'get-trip-to']);
    Route::get('trip-stops', 'TripStopsController@index');

    Route::get('my-parks', 'ParksController@parks');

    Route::get('destinations', 'ParksController@destinations');
    Route::post('add-stop', 'TripsController@addStop');
    Route::post('add-new-stop', 'TripsController@addNewStop');
    Route::post('edit-stop', 'TripsController@editStop');
    Route::match(['get', 'post'], 'delete-stop', ['uses' => 'TripsController@deleteStop', 'as' => 'delete-stop']);

  //  Route::post('delete-stop', 'TripsController@deleteStop');

    Route::get('ticket/{booking_id}', 'BookingsController@ticket');
    Route::match(['get', 'post'], 'notify-all-drivers', ['uses' => 'TripsController@notifyAllDrivers', 'as' => 'notify-all-drivers']);
    Route::match(['get', 'post'], 'update-bus-mileage', ['uses' => 'BusesController@updateBusMileage', 'as' => 'update-bus-mileage']);
    Route::match(['get', 'post'], 'get-schedule-driver-details', ['uses' => 'DailyTripsController@getScheduleDriverDetails', 'as' => 'get-schedule-driver-details']);
    Route::get('drivers', ['uses' => 'DriversController@index']);

});

Route::controller('api', 'ApiController');
Route::controller('sync', 'SyncController');


Route::group(['middleware' => ['web']], function () {
    Route::resource('states', 'StatesController');
});
Route::group(['middleware' => ['web']], function () {
    Route::resource('states', 'StatesController');
});
Route::group(['middleware' => ['web']], function () {
    Route::resource('states', 'StatesController');
});
Route::group(['middleware' => ['web']], function () {
    Route::resource('parks', 'ParksController');
});
Route::group(['middleware' => ['web']], function () {
    Route::resource('settings', 'SettingsController');
});
Route::group(['middleware' => ['web']], function () {
    Route::resource('userlogs', 'UserLogsController');
});
Route::group(['middleware' => ['web']], function () {
    Route::resource('users', 'UsersController');
});
Route::group(['middleware' => ['web']], function () {
    Route::resource('settings', 'SettingsController');
});
Route::group(['middleware' => ['web']], function () {
    Route::resource('settings', 'SettingsController');
});
Route::group(['middleware' => ['web']], function () {
    Route::resource('users', 'UsersController');
});
Route::group(['middleware' => ['web']], function () {
    Route::resource('users', 'UsersController');
});
Route::group(['middleware' => ['web']], function () {
    Route::resource('settings', 'SettingsController');
});
Route::group(['middleware' => ['web']], function () {
    Route::resource('userlogs', 'UserLogsController');
});
Route::group(['middleware' => ['web']], function () {
    Route::resource('parks', 'ParksController');
});
Route::group(['middleware' => ['web']], function () {
    Route::resource('states', 'StatesController');
});
Route::group(['middleware' => ['web']], function () {
    Route::resource('roles', 'RolesController');
});

Route::group(['middleware' => ['web']], function () {
    Route::resource('states', 'StatesController');
});
Route::group(['middleware' => ['web']], function () {
    Route::resource('parks', 'ParksController');
});
Route::group(['middleware' => ['web']], function () {
    Route::resource('userlogs', 'UserLogsController');
});
Route::group(['middleware' => ['web']], function () {
    Route::resource('settings', 'SettingsController');
});
Route::group(['middleware' => ['web']], function () {
    Route::resource('users', 'UsersController');
});
Route::group(['middleware' => ['web']], function () {
    Route::resource('trips', 'TripsController');
});
Route::group(['middleware' => ['web']], function () {
    Route::resource('operators', 'OperatorsController');
});
Route::group(['middleware' => ['web']], function () {
    Route::get('profile', 'OperatorsController@showProfile');
});

Route::group(['middleware' => ['web']], function () {
    Route::resource('bookings', 'BookingsController');
});
Route::group(['middleware' => ['web']], function () {
    Route::resource('agents', 'AgentsController');
});
Route::group(['middleware' => ['web']], function () {
    Route::resource('devices', 'DevicesController');
});

Route::group(['middleware' => ['web']], function () {
    Route::resource('cash-offices', 'CashOfficesController');
});
Route::group(['middleware' => ['web']], function () {
    Route::resource('drivers', 'DriversController');
});
/*
Route::group(['middleware' => ['web']], function () {
	Route::resource('drivers', 'DriversController');
});*/
Route::group(['middleware' => ['web']], function () {
    Route::resource('buses', 'BusesController');
});
Route::group(['middleware' => ['web']], function () {
    Route::resource('daily-trips', 'DailyTripsController');
});
Route::group(['middleware' => ['web']], function () {
    Route::resource('discounts', 'DiscountController');
});

// laolu added this
Route::group(['middleware' => ['web']], function () {
    Route::resource('trips-stops', 'TripStopsController');
});
Route::group(['middleware' => ['web']], function () {
    Route::resource('incident-types', 'IncidentTypesController');
});
Route::group(['middleware' => ['web']], function () {
    Route::resource('incidents', 'IncidentsController');
});
Route::group(['middleware' => ['web']], function () {
    Route::resource('charter-bookings', 'CharterBookingsController');
});
Route::group(['middleware' => ['web']], function () {
    Route::resource('charter-bookings', 'CharterBookingsController');
});
