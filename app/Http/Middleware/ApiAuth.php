<?php

namespace App\Http\Middleware;

use App\Libraries\Utility;
use Closure;
use Illuminate\Support\Facades\Auth;

class ApiAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @param  string|null $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        $api_key = env('API_KEY');
        $request_api_key = $request->header('api_key');
        if ($request_api_key !== $api_key) {
            Utility::$response['status'] = 404;
            Utility::$response['message'] = 'Invalid API Key!';
            return response()->json(Utility::$response);
        }

        return $next($request);
    }
}
