<?php

namespace App\Http\Middleware;

use App\Libraries\Encryption;
use App\Libraries\Utility;
use Closure;
use Illuminate\Support\Facades\Auth;

class TokenAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @param  string|null $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        $c_api_key = env('API_KEY');
        $request_token = $request->header('token');
        $today = date('Y-m-d H:i:s');
        $enc = new Encryption();
        $decrypted_token = $enc->Decrypt($request_token);
        $decrypted_token_array = explode('|', $decrypted_token);
        $api_key = isset($decrypted_token_array[1]) ? $decrypted_token_array[1] : '';
        $user_id = isset($decrypted_token_array[0]) ? $decrypted_token_array[0] : '';
        $expiry_date = isset($decrypted_token[3]) ? $decrypted_token[3] : '';
        if (empty($request_token)) {
            Utility::$response['status'] = 404;
            Utility::$response['message'] = 'Token Empty!';
            return response()->json(Utility::$response);
        } elseif ($today > $expiry_date) {
            Utility::$response['status'] = 404;
            Utility::$response['message'] = 'Token Expired!';
            return response()->json(Utility::$response);
        } elseif ($api_key !== $c_api_key) {
            Utility::$response['status'] = 404;
            Utility::$response['message'] = 'Invalid Token!';
            return response()->json(Utility::$response);
        }/*else{
            $new_expiry_date = strtotime("+12 hours", strtotime($today));
            $new_string = $user_id.'|'.$c_api_key.'|'.$today.'|'.$new_expiry_date;
            $new_token = $enc->Encrypt($new_string);
        }*/

        return $next($request);
    }
}
