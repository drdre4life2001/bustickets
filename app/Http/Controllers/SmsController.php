<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Libraries\Utility;
use App\Models\Booking;
use App\Models\BookingNote;
use App\Models\Operator;
use App\Models\Trip;
use App\Models\SubTrip;
use App\Models\Park;
use App\Models\User;
use App\Models\Passenger;
use App\Models\PaymentMethod;
use App\Models\DailyTrip;
use App\Models\Setting;
use App\Models\Driver;
use App\Models\Bus;
use App\Models\Seat;
use App\Models\Discount;
use App\Models\BusBooking;
use App\Models\CharterBooking;
use App\Models\Incident;
use App\Models\IncidentType;
use App\Models\Role;
use App\Models\ExtraLuggage;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Session;
use Auth;
use DB;


class SmsController extends Controller
{

    public function sent_sms(Request $request, $status = 'ALL', $park_id = ''){

        // dd($request);

        $to = env("Chairman") ;

        $total_revenue = $request->total_revenue;
        $total_expenditure = $request->total_expenditure;
        $net_revenue = $request->net_revenue;
        $source_park =  session('source_park')->name;
        //dd($source_park);

        $msg = "$source_park Report:\nTotal Revenue: " . $total_revenue. " \nTotal Expenses: " . $total_expenditure  . "\nNet Revenue: " . $net_revenue;

        $start_date = date('Y-m-d 00:00:00');
        $end_date = date('Y-m-d 23:59:59');

        if(isset($request->daterange)){
            $arr = explode(' ', $request->daterange);
            $start_date = $arr[0].' 00:00:00';
            $end_date = $arr[1].' 23:59:59';
        }

        $op_id = session('operator')->id; //operator

        $query = DailyTrip::with(['driver','ticketer', 'trip.sourcepark', 'trip.destpark', 'trip.subTrips.destpark','bus', 'expenses', 'bookings'=>function($query){ $query->where('status', 'PAID'); }])
            ->where('trip_date', '>=', $start_date)
            ->where('trip_date', '<=', $end_date)
            ->where('operator_id', session('operator')->id)
            ->where('source_park_id', session('source_park')->id)
            ->orderBy('trip_date', 'desc');

        if($status != 'ALL')
            $query = $query->where("status", $status);
        if ($park_id != 0) {
            $trip_ids = Trip::where('source_park_id', $park_id)->lists('id')->toArray();
            $query = $query->whereIn('trip_id', $trip_ids);
        }

        // if(!empty(var))
        $dailytrips = $query->get();

        $page_title = 'Daily Trips Reports';

        $statuses = ['LOADING'=>'LOADING', 'TRANSITED'=>'TRANSITED', 'POSTPONED'=>'POSTPONED', 'CANCELLED'=>'CANCELLED'];
        $parks = Park::where('boardable', 1)
            ->where('operator_id', session('operator')->id)
            ->lists('name', 'id')->toArray();



        return view('reports.sent_sms', compact('dailytrips', 'page_title', 'start_date', 'end_date', 'statuses', 'status', 'park_id', 'parks'));


    }
}
