<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Discount;
use Carbon\Carbon;
use Session;

class DiscountController extends Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $discounts = Discount::where('operator_id', session('operator')->id)->paginate(100);

        $page_title = 'discounts';

        return view('discounts.index', compact('discounts', 'page_title'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page_title = 'Add Discount';

        return view('discounts.create', compact('page_title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [

            'age_one' => 'required', 
            'age_two' => 'required', 
            'discount' => 'required', 
            'operator_id' => 'required', 
            
            ]);

        Discount::create($request->all());

        Session::flash('flash_message', 'Discount added!');

        return redirect('discounts');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $discount = Discount::findOrFail($id);

        $page_title = 'Edit discount';
        return view('discounts.edit', compact('discount', 'page_title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $discount = Discount::findOrFail($id);
        $discount->update($request->all());

        Session::flash('flash_message', 'Discount updated!');

        return redirect('discounts');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         Discount::destroy($id);

        Session::flash('flash_message', 'Discount deleted!');

        return redirect('discounts');
    }
}
