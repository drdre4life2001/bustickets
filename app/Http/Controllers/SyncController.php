<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Libraries\Utility;
use App\Models\Setting;
use App\Models\State;
use App\Models\Park;
use App\Models\Trip;
use App\Models\Booking;
use App\Models\Passenger;

class SyncController extends Controller
{
    
    public function __construct(Request $request)
    {
        parent::__construct();

        Utility::$response = Utility::checkParameters($request, array('access_code'));
        if(Utility::$response['status'] == 404){
            echo json_encode(Utility::$response);    
            exit;   
        }

        if($this->operator->access_code != $request->access_code) {
           Utility::$response['status'] = 404;
           Utility::$response['message'] = 'Who are you????  Your intrusion has been logged...and we are watching.';

           echo json_encode(Utility::$response);

           exit;
        }

    }

    public function postStatus(Request $request)
    {
        
        

    	$last_sync_time = Setting::where('key', 'last_sync_time')->first()->value;
        $sync_status = Setting::where('key', 'sync_status')->first()->value;

        // if($sync_status == 'IDLE')
        //     Setting::where('key', 'sync_status')->update(['value'=>'WORKING']);
    	
    	Utility::$response['status'] = 200;
        Utility::$response['data'] = compact('last_sync_time', 'sync_status');
        return response()->json(Utility::$response);
        
        
    }

    public function postUpdateStatus(Request $request)
    {
        
        Utility::$response = Utility::checkParameters($request, array('sync_time'));
        if(Utility::$response['status'] == 404)
            return response()->json(Utility::$response);       

        Setting::where('key', 'last_sync_time')->update(['value'=>$request->sync_time]);
        Setting::where('key', 'sync_status')->update(['value'=>'IDLE']);
        
        Utility::$response['status'] = 200;
        Utility::$response['message'] = 'success';
        return response()->json(Utility::$response);
        
        
    }


    public function postUpdateStates(Request $request)
    {
        Utility::$response = Utility::checkParameters($request, array('states'));
        if(Utility::$response['status'] == 404)
            return response()->json(Utility::$response);       

        
        foreach ($request->states as $state) {
            State::where('id', $state['id'])->update($state);

        }

        

        Utility::$response['status'] = 200;
        Utility::$response['message'] = 'Success';
        Utility::$response['data'] = $request->states;
        return response()->json(Utility::$response);
        
    }


    public function postUpdateParks(Request $request)
    {
        Utility::$response = Utility::checkParameters($request, array('parks'));
        if(Utility::$response['status'] == 404)
            return response()->json(Utility::$response);       

        
        foreach ($request->parks as $park) {
            Park::where('id', $park['id'])->update($park);

        }

        

        Utility::$response['status'] = 200;
        Utility::$response['message'] = 'Success';
        Utility::$response['data'] = $request->parks;
        return response()->json(Utility::$response);
        
    }

    public function postFetchTrips(Request $request)
    {
        Utility::$response = Utility::checkParameters($request, array('last_sync_time'));
        if(Utility::$response['status'] == 404)
            return response()->json(Utility::$response);       

        
        $trips = Trip::where('updated_at', '>', $request->last_sync_time)->get()->toArray();

        

        Utility::$response['status'] = 200;
        Utility::$response['message'] = 'Success';
        Utility::$response['data'] = $trips;
        return response()->json(Utility::$response);
        
    }


    public function postUpdateBookings(Request $request)
    {
        Utility::$response = Utility::checkParameters($request, array('booking'));
        if(Utility::$response['status'] == 404)
            return response()->json(Utility::$response);       

        
        // $passengers = $request->booking['']
        // $passenger = Passenger::


        if(!empty($request->booking['operator_booking_id']))
            $booking = Booking::find($request->booking['operator_booking_id']);
        else
            $booking = new Booking;

        $booking->trip_id = $request->booking['trip']['operator_trip_id'];
        $booking->date = $request->booking['date'];
        $booking->passenger_count = $request->booking['passenger_count'];
        $booking->unit_cost = $request->booking['unit_cost'];
        $booking->final_cost = $request->booking['final_cost'];

        if(!empty($request->booking['payment_method_id']))
            $booking->payment_method_id = $request->booking['payment_method_id'];

        $booking->status = $request->booking['status'];
        // $booking->source = $request->booking['source'];

        if(!empty($request->booking['paid_date']))
            $booking->paid_date = $request->booking['paid_date'];

        $booking->booking_code = $request->booking['booking_code'];
        $booking->contact_name = $request->booking['contact_name'];
        $booking->contact_phone = $request->booking['contact_phone'];
        $booking->contact_email = $request->booking['contact_email'];
        $booking->next_of_kin = $request->booking['next_of_kin'];
        $booking->next_of_kin_phone = $request->booking['next_of_kin_phone'];
        $booking->created_at = $request->booking['created_at'];
        $booking->updated_at = $request->booking['updated_at'];
        $booking->save();

        // saving passengers...
        if(empty($request->booking['operator_booking_id']) && !empty($request->booking['passengers'])){ // new booking...
            $passengers = $request->booking['passengers'];
            foreach ($passengers as $p) {
                $pass = new Passenger;

                $pass->name = $p['name'];
                $booking->passengers()->save($pass);

            }    
        }




        

        Utility::$response['status'] = 200;
        Utility::$response['message'] = 'Success';
        Utility::$response['data'] = $booking;
        return response()->json(Utility::$response);
        
    }
   

    

}
