<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Park;
use App\Models\ParkStop;
use Auth;
use App\Models\State;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;

class ParksController extends Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $parks = Park::paginate(100);

        $page_title = 'parks';
        $is_destination  = true;

        return view('parks.index', compact('parks', 'page_title', 'is_destination'));
    }

    public function parks()
    {
        $parks = Park::where('boardable', true)->where('active', true)->paginate(1000);

        $page_title = 'Your Parks';
        $is_destination  = false;

        return view('parks.index', compact('parks', 'page_title', 'is_destination'));
    }

    public function destinations()
    {
        $parks = Park::with('stops')->where('boardable', false)->where('active', true)->paginate(1000);

        $page_title = 'Your Parks';
        $is_destination  = true;

        return view('parks.index', compact('parks', 'page_title', 'is_destination'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $page_title = 'Add park';
        $states = State::lists('name', 'id')->toArray();

        return view('parks.create', compact('page_title', 'states'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, ['name' => 'required', 'state_id' => 'required', ]);

        $d = $request->all();
        

        if(Auth::user()->role_id != 1) {
            $d['boardable'] = false;
        }
        

       // dd($d);

        Park::create($d);

        Session::flash('flash_message', 'Park added!');

        return redirect('destinations');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function show($id)
    {
        $park = Park::findOrFail($id);

        $page_title = 'View park';
        return view('parks.show', compact('park', 'page_title'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $park = Park::findOrFail($id);
        $states = State::lists('name', 'id')->toArray();

        $page_title = 'Edit park';
        return view('parks.edit', compact('park', 'page_title', 'states'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        $this->validate($request, ['name' => 'required', 'state_id' => 'required', ]);

        $park = Park::findOrFail($id);
        $park->update($request->all());

        Session::flash('flash_message', 'Park updated!');

        return redirect('parks');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        Park::destroy($id);

        Session::flash('flash_message', 'Park deleted!');

        return redirect('parks');
    }

    public function addStop(Request $request){

        ParkStop::create($request->all());

        Session::flash('flash_message', 'Park added!');

        return redirect()->action('ParksController@show', [$request->park_id]);

    }

    public function editStop(Request $request){

        $ps = ParkStop::findOrFail($request->id);
        $ps->update($request->all());

        Session::flash('flash_message', 'Stop edited!');

        return redirect()->action('ParksController@show', [$request->park_id]);

    }

    public function deleteStop(Request $request){

        ParkStop::destroy($request->id);
        
        Session::flash('flash_message', 'Stop deleted!');

        return redirect()->action('ParksController@show', [$request->park_id]);

    }

    //Let an Operator Admin Select A Park or Terminal to inspect
     public function Select(Request $request, $id){
  
        $park = Park::find($id);
        session()->put('source_park', $park);

        Session::flash('flash_message', 'You have successfully selected <b>'.$park->name.'</b>');
        return back();

    }

}
