<?php

namespace App\Http\Controllers;

use App\Libraries\Encryption;
use App\Models\Discount;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Libraries\Utility;
use App\Models\Agent;
use App\Models\User;
use App\Models\AgentTransaction;
use App\Models\Park;
use App\Models\Seat;
use App\Models\Operator;
use App\Models\Trip;
use App\Models\SubTrip;
use App\Models\DailyTrip;
use App\Models\Booking;
use App\Models\BusBooking;
use App\Models\Passenger;
use App\Models\PaymentMethod;
use App\Models\Setting;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Session\Session;

class ApiController extends Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function sendSms($phone, $msg)
    {

        //settings
        $cid = "";
        $user = "buscom_sms";
        $senderArray = explode(" ", $this->operator->name);
        $senderId = $senderArray[0];
        $to = $phone;
        $pass = "bus.com.ng";
        $ch = curl_init();
        $postdata = 'user=' . $user . '&pass=' . $pass . '&from=' . $senderId . '&to=' . $to . '&msg=' . $msg; //initialize the request variable
        // echo $postdata;

        $url = 'http://cloud.nuobjects.com/api/send/'; //this is the url of the gateway's interface
        $ch = curl_init(); //initialize curl handle
        curl_setopt($ch, CURLOPT_URL, $url); //set the url
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //return as a variable
        curl_setopt($ch, CURLOPT_POST, 1); //set POST method
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata); //set the POST variables
        $response = curl_exec($ch); // grab URL and pass it to the browser. Run the whole process and return the response
        curl_close($ch); //close the curl handle

        $strHeader = get_headers($url)[0];

        $statusCode = substr($strHeader, 9, 3);

        return $statusCode;

    }

    public function postLogin(Request $request)
    {
        $request = (object)$request->input();
        $r_api_key = $request->api_key;
        $c_api_key = env('API_KEY');
        if ($r_api_key !== $c_api_key) {
            Utility::$response['status'] = 404;
            Utility::$response['message'] = 'Invalid API Key';
            return response()->json(Utility::$response);
        }
        /*Utility::$response = Utility::checkParameters($request, array('username', 'password'));
        if (Utility::$response['status'] == 404) {
            return response()->json(Utility::$response);
        }*/
        //Get password of submitted email if it exists in DB
        $user = User::with('parks')->where('email', $request->username)->first();
        if (empty($user)) {
            Utility::$response['status'] = 404;
            Utility::$response['message'] = trans('auth.no_account');
            return response()->json(Utility::$response);
        } elseif (!empty($user)) {
            $key = env('API_KEY');
            $date = date('Y-m-d H:i:s');
            $expiry_date_ = strtotime("+12 hours", strtotime($date));
            $expiry_date = date("Y-m-d h:i:s", $expiry_date_);
            $string = $user->id . '|' . $key . '|' . $date . '|' . $expiry_date;
            $enc = new Encryption();
            $token = $enc->Encrypt($string);
            $user->wallet_balance = 0;
            Utility::$response['status'] = 200;
            Utility::$response['token'] = $token;
            Utility::$response['data'] = $user;
            Utility::$response['message'] = trans('auth.success');
            return response()->json(Utility::$response);
        } else {
            Utility::$response['status'] = 500;
            Utility::$response['message'] = trans('auth.failed');
            return response()->json(Utility::$response);
        }
    }

    public function postGetDailyTrips(Request $request)
    {
        Utility::$response = Utility::checkParameters($request, array('ticketer_id'));
        if (Utility::$response['status'] == 404) {
            return response()->json(Utility::$response);
        }
        $request = (object)$request->input();
        $trip_date = date('Y-m-d');
        $trips = DailyTrip::where('ticketer_id', $request->ticketer_id)/*->where('trip_date', $trip_date)*/
        ->with(['bus', 'trip', 'driver', 'seats'])->get();
        Utility::$response['status'] = 200;
        Utility::$response['data'] = ['trips' => $trips];
        Utility::$response['message'] = 'Daily Trips';
        return response()->json(Utility::$response);

    }

    public function postGetTicketerBookings(Request $request)
    {
        /*Utility::$response = Utility::checkParameters($request, array('ticketer_id'));
        if (Utility::$response['status'] == 404) {
            return response()->json(Utility::$response);
        }*/
        $request = (object)$request->input();
        $user_id = $request->ticketer_id;
        $trips = DB::select(
            "SELECT id, (SELECT name FROM trips WHERE id = trip_id) as trip,
            (SELECT name FROM operators WHERE id = operator_id) as operator, 
            (SELECT name FROM parks WHERE id = source_park_id) as park, 
            (SELECT name FROM passengers WHERE booking_id = id) as passengers,
            date,unit_cost, final_cost, status,booking_code,contact_name,contact_phone,
            contact_address,seat_no,created_at 
            FROM bookings WHERE user_id = '$user_id' ORDER BY created_at DESC");

        Utility::$response['status'] = 200;
        Utility::$response['data'] = ['trips' => $trips];
        Utility::$response['message'] = 'Daily Trips';
        return response()->json(Utility::$response);

    }

    public function postGetDashData(Request $request)
    {
        Utility::$response = Utility::checkParameters($request, array('agent_id'));
        if (Utility::$response['status'] == 404)
            return response()->json(Utility::$response);


        //Get password of submitted email if it exists in DB
        $agent = Agent::find($request->agent_id);

        if (empty($agent)) {
            Utility::$response['status'] = 404;
            Utility::$response['message'] = trans('auth.no_account');

            return response()->json(Utility::$response);
        }

        // dd($agent->password == $request->passsword);

        // if($agent->password == $request->passsword)
        $agent->wallet_balance = $agent->balance;

        //getting dashboard data
        $bookings_values_sum = $agent->transactions()->where('type', 'BOOKING')->sum('value');
        $bookings_paid_sum = $agent->transactions()->where('type', 'BOOKING')->sum('amount_paid');
        $bookings_count = $agent->transactions()->where('type', 'BOOKING')->count();

        $topup_values_sum = $agent->transactions()->where('type', 'TOPUP')->sum('value');
        $topup_paid_sum = $agent->transactions()->where('type', 'TOPUP')->sum('amount_paid');
        $topup_count = $agent->transactions()->where('type', 'TOPUP')->count();

        //commission
        $agent->commission = $topup_values_sum - $topup_paid_sum;


        $data = compact('agent', 'bookings_values_sum', 'bookings_paid_sum', 'bookings_count', 'topup_values_sum', 'topup_paid_sum', 'topup_count');

        Utility::$response['status'] = 200;
        Utility::$response['data'] = $data;
        Utility::$response['message'] = 'Success';
        return response()->json(Utility::$response);

    }

    public function postGetParks(Request $request)
    {


        //Get password of submitted email if it exists in DB
        // $agent = Agent::where('username', $request->username)->first();
        $parks = Park::with('state')->where('active', 1)->where('boardable', 1)->get();

        Utility::$response['status'] = 200;
        Utility::$response['data'] = $parks;
        Utility::$response['message'] = trans('auth.success');
        return response()->json(Utility::$response);

    }

    public function postGetDestinations(Request $request)
    {

        Utility::$response = Utility::checkParameters($request, array('park_id'));
        if (Utility::$response['status'] == 404)
            return response()->json(Utility::$response);


        //Get password of submitted email if it exists in DB
        // $agent = Agent::where('username', $request->username)->first();
        $dests = Park::with('state')->where('active', 1)->where('id', '<>', $request->park_id)->get();
        $operators = Operator::where('active', 1)->get();
        $parks = Park::with('state')->where('active', 1)->where('boardable', 1)->get();

        Utility::$response['status'] = 200;
        Utility::$response['data'] = ['dests' => $dests, 'parks' => $parks, 'operators' => $operators];
        Utility::$response['message'] = trans('auth.success');
        return response()->json(Utility::$response);

    }

    public function postGetDestinationsNew(Request $request)
    {

        Utility::$response = Utility::checkParameters($request, array('agent_id'));
        if (Utility::$response['status'] == 404)
            return response()->json(Utility::$response);


        //Get password of submitted email if it exists in DB
        $agent = User::find($request->agent_id);
        $parks = $agent->destinations()->get();
        $operators = Operator::where('active', 1)->get();

        Utility::$response['status'] = 200;
        Utility::$response['data'] = ['dests' => $parks, 'operators' => $operators];
        Utility::$response['message'] = trans('auth.success');
        return response()->json(Utility::$response);

    }

    public function postGetTrips(Request $request)
    {

        Utility::$response = Utility::checkParameters($request, array('park_id', 'dest_name', 'date'));
        if (Utility::$response['status'] == 404)
            return response()->json(Utility::$response);


        $op_id = $this->operator->id;

        $dest = Park::where('name', $request->dest_name)->first();
        // dd($dest);
        $trip_date = date('Y-m-d', strtotime($request->date));

        $trips = SubTrip::with(['parenttrip.operator', 'parenttrip.destpark', 'sourcepark', 'destpark', 'parenttrip.bus', 'parenttrip.default_bus',
            'parenttrip.dailyTrips' => function ($query) use ($trip_date) {
                $query->where('trip_date', $trip_date);

            }, 'parenttrip.dailyTrips.bus', 'parenttrip.dailyTrips.driver', 'parenttrip.dailyTrips.seats'])
            ->where('source_park_id', $request->park_id)
            ->where('dest_park_id', $dest->id)
            ->where('active', 1)
            ->get();
        Utility::$response['status'] = 200;
        Utility::$response['data'] = ['trips' => $trips];
        Utility::$response['message'] = trans('auth.success');
        return response()->json(Utility::$response);

    }

    public function postGetTripsWithPark(Request $request)
    {

        Utility::$response = Utility::checkParameters($request, array('park_name', 'dest_name', 'date'));
        if (Utility::$response['status'] == 404)
            return response()->json(Utility::$response);


        $op_id = $this->operator->id;

        $dest = Park::where('name', $request->dest_name)->first();
        $park = Park::where('name', $request->park_name)->first();

        // dd($dest);
        $trip_date = date('Y-m-d', strtotime($request->date));

        $trips = SubTrip::with(['parenttrip.operator', 'parenttrip.destpark', 'sourcepark', 'destpark', 'parenttrip.bus.bus_type', 'parenttrip.default_bus.bus_type',
            'parenttrip.dailyTrips' => function ($query) use ($trip_date) {
                $query->where('trip_date', $trip_date);

            }, 'parenttrip.dailyTrips.bus.bus_type', 'parenttrip.dailyTrips.driver', 'parenttrip.dailyTrips.seats'])
            ->where('source_park_id', $park->id)
            ->where('dest_park_id', $dest->id)
            ->where('active', 1)
            ->get();


        // dd($trips->toArray());                                        


        Utility::$response['status'] = 200;
        Utility::$response['data'] = ['trips' => $trips];
        Utility::$response['message'] = trans('auth.success');
        return response()->json(Utility::$response);

    }

    public function postBookTrip(Request $request)
    {

        Utility::$response = Utility::checkParameters($request, array('trip_id', 'date', 'num_pass', 'contact_name', 'contact_phone', 'nok', 'nok_phone', 'payment_method'));
        if (Utility::$response['status'] == 404)
            return response()->json(Utility::$response);

        $trip = SubTrip::with('parenttrip.default_bus', 'parenttrip.bus')->find($request->trip_id);
        // dd($trip->toArray());

        $trip_name = $trip->parenttrip->name;
        $fare = $trip->fare;

        $booked_seats = array();
        $dt = DailyTrip::where('trip_id', $trip->parenttrip->id)->where('trip_date', date('Y-m-d', strtotime($request->date)))->first();
        if (!empty($dt)) {
            if (!empty($dt->seats())) {
                foreach ($dt->seats()->whereNotNull('booking_id')->get() as $seat) {
                    $booked_seats[] = $seat->seat_no;
                }

                //$booked_seats = substr($booked_seats, 0, strlen($booked_seats) - 1);
            }
        }

        //checking for booked seats...
        if (in_array($request->seat, $booked_seats)) {

            Utility::$response['status'] = 500;
            Utility::$response['message'] = 'Contact person seat is already taken. Please choose another one.';
            return response()->json(Utility::$response);
        }


        $selSeats = array();
        $selSeats[] = $request->seat;
        $psg_count = 1;


        //passengers...
        $arr = explode("$$$$", $request->pass_details);
        for ($i = 0; $i < count($arr); $i++) {
            $a = explode("&&&&", $arr[$i]);
            if (count($a) == 4) {
                $psg_name = $a[0];
                $psg_gender = $a[1];
                $psg_seat = $a[2];
                if ($a[3] != 'Infant')
                    $psg_count++;

                if (in_array($psg_seat, $booked_seats)) {

                    Utility::$response['status'] = 500;
                    Utility::$response['message'] = 'Passenger ' . ($i + 1) . '\'s seat is already taken. Please choose another one.';
                    return response()->json(Utility::$response);
                }


                $selSeats[] = $psg_seat;
            }


        }


        if ($request->trip_type == 'Round Trip')
            $fare = $trip->round_trip_fare;
        else
            $fare = $trip->fare;


        switch ($request->passport_type) {
            case 'Virgin Passport':
                # code...
                $fare = ($request->trip_type == 'Round Trip') ? $trip->round_trip_virgin_passport_fare : $trip->virgin_passport_fare;
                break;

            case 'ID card/ No Passport':
                # code...
                $fare = ($request->trip_type == 'Round Trip') ? $trip->round_trip_no_passport_fare : $trip->no_passport_fare;
                break;

            default:
                # code...
                break;
        }


        $final_cost = $psg_count * $fare;


        //luggage...
        $luggSetting = Setting::where('key', 'track_luggage')->first();
        $track_luggage = ($luggSetting->value == 'true') ? TRUE : FALSE;
        $luggage_limit = intval(Setting::where('key', 'luggage_limit')->first()->value);
        $lugg_cost = 0;

        if ($track_luggage) {

            $luggage_limit = intval(Setting::where('key', 'luggage_limit')->first()->value);
            $cpk = 0;
            if ($trip->is_intl_trip)
                $cpk = intval(Setting::where('key', 'intl_cost_per_kg')->first()->value);
            else
                $cpk = intval(Setting::where('key', 'local_cost_per_kg')->first()->value);


            $total_lugg_limit = $psg_count * $luggage_limit;
            $lugg_cost = ($request->luggage_weight - $total_lugg_limit) * $cpk;
            if ($lugg_cost < 0)
                $lugg_cost = 0;


            $final_cost += $lugg_cost;
        }


        $pMethod = PaymentMethod::where('name', $request->payment_method)->first();

        // dd($pMethod->id);

        $booking_code = $trip->parenttrip->operator->code . rand(100000, 999999);

        $booking = BusBooking::firstOrCreate([
            'trip_id' => $request->trip_id,
            'date' => date('Y-m-d', strtotime($request->date)),
            'passenger_count' => $request->num_pass,
            'contact_name' => $request->contact_name,
            'contact_phone' => $request->contact_phone,
            'contact_email' => $request->contact_email,
            'contact_address' => $request->address,
            'next_of_kin' => $request->nok,
            'next_of_kin_phone' => $request->nok_phone,
            'gender' => $request->gender,
            'status' => 'PAID',
            'paid_date' => date('Y-m-d H:i:s'),
            'final_cost' => $final_cost,
            'unit_cost' => $trip->fare,
            'user_id' => $request->user_id,
            'booking_code' => $booking_code,
            'payment_method_id' => $pMethod->id,
            'departure_time' => date('Y-m-d', strtotime($request->departure_time)),
            'return_date' => $request->return_date,
            'is_round_trip' => ($request->trip_type == 'Round Trip'),
            'passport_type' => $request->passport_type,
            'luggage_weight' => $request->luggage_weight,
            'luggage_cost' => $lugg_cost,
            'source' => "MPOS"
        ]);


        $selSeats = array();
        $selSeats[] = $request->seat;


        //passengers...
        $arr = explode("$$$$", $request->pass_details);
        for ($i = 0; $i < count($arr); $i++) {
            $a = explode("&&&&", $arr[$i]);
            if (count($a) == 4) {
                $psg_name = $a[0];
                $psg_gender = $a[1];
                $psg_seat = $a[2];
                if ($a[3] == 'Infant')
                    $psg_infant = 1;
                else
                    $psg_infant = 0;
                $selSeats[] = $psg_seat;

                $response = Passenger::firstOrCreate([
                    'booking_id' => $booking->id,
                    'name' => $psg_name,
                    'gender' => $psg_gender,
                    'infant' => $psg_infant,
                ]);
            }


        }


        //updating to daily trips...
        if (empty($dt)) {
            // creating default...
            $inserts = [
                'trip_id' => $trip->parenttrip->id,
                'trip_date' => date('Y-m-d', strtotime($request->date)),
                'booked_seats' => 0,
                'total_seats' => $trip->parenttrip->bus->bus_type->no_of_seats,

            ];
            $driver_id = $bus_id = '';
            if (!empty($trip->parenttrip->bus)) {
                $inserts['driver_id'] = $trip->parenttrip->bus->default_driver_id;
                $inserts['bus_id'] = $trip->parenttrip->bus->id;
            }

            $dt = DailyTrip::firstOrCreate($inserts);

            // dd($dt);
        }

        $dt->booked_seats = $dt->booked_seats + $request->num_pass;
        $dt->total_fare = $dt->total_fare + ($booking->final_cost);
        $dt->save();


        //adding seats...


        foreach ($selSeats as $s) {
            $seat = new Seat;
            $seat->seat_no = $s;
            $seat->daily_trip_id = $dt->id;
            $seat->booking_id = $booking->id;
            $seat->save();

        }

        //updating bookings daily trip...
        $booking->daily_trip_id = $dt->id;
        $booking->save();

        $op = $this->operator->name;


        $booking = BusBooking::with('trip.sourcepark', 'trip.destpark', 'trip.parenttrip.bus.bus_type', 'seats', 'paymentmethod', 'passengers', 'ticketer', 'daily_trip.bus', 'daily_trip.driver')->find($booking->id);


        $booked_seats = '';
        foreach ($booking['seats'] as $seat) {
            $booked_seats .= $seat['seat_no'] . ',';
        }

        $booked_seats = substr($booked_seats, 0, strlen($booked_seats) - 1);

        //sending sms....
        $msg = $op . " purchase notice : " . " \nBook Code: " . $booking->booking_code . " \nFrom: " . " " .
            $booking->trip->sourcepark->name
            . "to " . $booking->trip->destpark->name .
            ". \nName: " . $booking->contact_name
            . ". \nBus: " . $booking->daily_trip->bus->bus_number .
            ". \nDeparture Date: " . $booking->date .
            ". \nSeats: " . $booked_seats .
            ". \nPassengers: " . $booking->passenger_count .
            ". \nFinal Cost: NGN" . $booking->final_cost . " \nContact:012905460";

        // actual sending
        $res = $this->sendSms($booking->contact_phone, $msg);


        Utility::$response['status'] = 200;
        Utility::$response['data'] = ['booking' => $booking];
        Utility::$response['message'] = trans('auth.success');
        return response()->json(Utility::$response);

    }

    public function BookTrip(Request $request)
    {
        $request = $request->input();
        $request = (object)$request;
        /*Utility::$response = Utility::checkParameters($request, array('passenger_count', 'operator_id','contact_phone', 'contact_name', 'next_of_kin_phone','selSeats','ticketer_id','final_cost'));
        if (Utility::$response['status'] == 404) {
            return response()->json(Utility::$response);
        }*/
        $excludePassenger = Discount::where('operator_id',
            $request->operator_id)->where('discount', 100)->first();
        $trip_id = $request->trip_id;
        $trip = SubTrip::with('parenttrip.default_bus', 'parenttrip.bus')->find($trip_id);
        if ($request->is_round_trip) {
            $fare = $trip->round_trip_fare;
        } else {
            $fare = $trip->fare;
        }
        $unit_cost = $fare;
        $totalcost = 0;
        $k = 1;
        for ($i = 1; $i < 10; $i++) {
            $p = 'psg' . $i;
            $pssg = isset($request->$p) ? $request->$p : '';
            $ag = 'age' . $i;
            $age = isset($request->$ag) ? $request->$ag : '';
            if (!empty($pssg))
                $k = $i + 1;
            if (!empty($age))
                $k = $i + 1;
        }
        $no_seats = count(explode(',', $request->selSeats));
        $multiplier = (int)$request->passenger_count;
        if ($no_seats != $multiplier) {
            Utility::$response['message'] = 'Passenger Count and Number of passengers inputed do not match';
            return response()->json(Utility::$response);
        }
        $status = 'PENDING';
        $paid_date = NULL;
        $booking_code = $trip->parenttrip->operator->code . mt_rand(1000000000000000, 99999999999999999999999);
        if ($request->is_round_trip) {
            $fare = $trip->round_trip_fare;
        } else {
            $fare = $trip->fare;
        }

        switch ($request->passport_type) {
            case 'Virgin passport':
                $fare = ($request->is_round_trip) ? $trip->round_trip_virgin_passport_fare : $trip->virgin_passport_fare;
                break;

            case 'ID card/ No passport':
                $fare = ($request->is_round_trip) ? $trip->round_trip_no_passport_fare : $trip->no_passport_fare;
                break;

            default:
                break;
        }

        $flexi = false;
        $final_cost = $final_cost = ($flexi) ? $request->final_cost : $fare;
        $lugg_cost = 0;
        $luggSetting = Setting::where('key', 'track_luggage')->where('operator_id', $request->operator_id)->first();
        $track_luggage = $luggSetting ? TRUE : FALSE;
        $lugg_limit = Setting::where('key', 'luggage_limit')->where('operator_id', $request->operator_id)->first();
        $luggage_limit = $lugg_limit ? true : false;
        $discount_settings = Setting::where('key', 'discount_active')->where('operator_id', $request->operator_id)->first();
        $track_discounts = $discount_settings ? TRUE : FALSE;

        if ($track_luggage && $request->luggage_weight != "" && $flexi != true) {
            if ($trip->is_intl_trip) {
                $cpk = intval(session('settings')->intl_cost_per_kg);
            } else {
                $cpk = intval(session('settings')->local_cost_per_kg);
            }

            //The luggage limit is multiplied by no of paying passengers
            $total_lugg_limit = $multiplier * $luggage_limit;
            $lugg_cost = ($request->luggage_weight - $total_lugg_limit) * $cpk;
            if ($lugg_cost < 0) {
                $lugg_cost = 0;
            }
            $final_cost += $lugg_cost;
        }

        // infant logic comes here
        $totalPassengerPrice = 0;
        for ($i = 1; $i < $k; $i++) {
            $p = 'psg' . $i;
            $psggender = 'psggender' . $i;
            $psgage = 'age' . $i;

            $psg_name = $request->$p;
            $psg_gender = $request->$psggender;
            $filledAge = $request->$psgage;
            if ($filledAge == '') {
                $filledAge = 30;
            }
            $psg_age = intval($filledAge);
            // AGE CONDITION
            if ($track_discounts && $flexi != true) {
                $opId = session('operator')->id;
                $theDiscount = DB::select(DB::raw("select * from discounts where operator_id = '$opId' AND '$psg_age' between age_one and age_two"));

                if (!empty($theDiscount)) {
                    $theDiscount = $theDiscount[0];
                    $this_discount = (100 - $theDiscount->discount) / 100;
                    //dd($this_discount);
                    if ($psg_age >= intval($theDiscount->age_one) && $psg_age <= intval($theDiscount->age_two)) {   //calculate discount amt of trip's fare
                        $pFare = $this_discount * floatval($fare);
                        $totalPassengerPrice += $pFare;
                    }
                } else {
                    $pFare = floatval($fare);
                    $totalPassengerPrice += $pFare;
                }
            } else {
                $ppFare = floatval($fare);
                $totalPassengerPrice += floatval($fare);

            }
        }
        $final_cost += $totalPassengerPrice;
        if ($flexi == true) {
            $final_cost = floatval($request->final_cost);
        }
        //the fare for transloaded passengers is null
        //this is because they have already paid at
        //the inital source park they are coming from.
        if (isset($request->initial_source_park_id) || $request->initial_source_park_id != '') {
            $fare = 0.00;
        }
        $date = date('Y-m-d');
        $booking = BusBooking::firstOrCreate([
            'trip_id' => $trip_id,
            'operator_id' => $request->operator_id,
            'source_park_id' => $request->source_park_id,
            'date' => date('Y-m-d', strtotime($date)),
            'passenger_count' => $request->passenger_count,
            // 'departure_time' => $request->departure_time,
            'return_date' => isset($request->return_date) ? $request->return_date : "",
            'contact_name' => $request->contact_name,
            'contact_phone' => $request->contact_phone,
            'is_round_trip' => $request->is_round_trip,
            'contact_email' => $request->contact_email,
            'contact_address' => $request->contact_address,
            'next_of_kin' => $request->next_of_kin,
            'next_of_kin_phone' => $request->next_of_kin_phone,
            'status' => $status,
            'paid_date' => $paid_date,
            'final_cost' => $final_cost,
            'unit_cost' => $fare,
            'gender' => $request->gender,
            'seat_no' => explode(',', $request->selSeats)[0],
            'booking_code' => $booking_code,
            'user_id' => $request->ticketer_id,
            'passport_type' => $request->passport_type,
            'luggage_weight' => $request->luggage_weight,
            'luggage_cost' => $lugg_cost,
            'is_round_trip' => ($request->is_round_trip),
            'initial_source_park_id' => $request->initial_source_park_id,
            'transfer_type' => $request->transfer_type,
            'nationality' => $request->nationality,
            'occupation' => $request->occupation,
            'passport_no' => $request->passport_no,
            'passport_place_issued' => $request->passport_place_issued,
            'passport_date_issued' => $request->passport_date_issued,
            'passport_expiry_date' => $request->passport_expiry_date,
            'is_flexi' => ($flexi) ? 1 : 0
        ]);
        if (empty($dt)) {
            $inserts = [
                'trip_id' => $trip->parenttrip->id,
                'trip_date' => date('Y-m-d', strtotime($date)),
                'booked_seats' => 0,
                'total_seats' => $trip->parenttrip->bus->no_of_seats,
                'trip_transit' => 'First Bus',
                'operator_id' => $request->operator_id,
                'source_park_id' => $request->source_park_id
            ];
            $driver_id = $bus_id = '';
            if (!empty($trip->parenttrip->bus)) {
                $inserts['driver_id'] = $trip->parenttrip->bus->default_driver_id;
                $inserts['bus_id'] = $trip->parenttrip->bus->id;
            }
            $dt = DailyTrip::firstOrCreate($inserts);
        }

        $dt->booked_seats = $dt->booked_seats + $request->passenger_count;
        $dt->total_fare = $dt->total_fare + ($final_cost);
        $dt->save();
        $psSeat = explode(',', $request->selSeats);

        //$s is what we use to assign seats to passengers
        $s = 0;
        for ($i = 1; $i < $k; $i++) {
            // dd($k);
            $s += 1;
            $p = 'psg' . $i;
            $psggender = 'psggender' . $i;
            // psgtype means infant or not
            $psgtype = 'age' . $i;
            $psg_name = $request->$p;
            $psg_gender = $request->$psggender;
            $fillAged = $request->$psgtype;
            //We are checking if the age field was filled. If it wasnt then we assume the passenger is an adult
            if ($fillAged == '') {
                $fillAged = 30;
            }
            $psg_age = intval($fillAged);
            //dump($psg_age);
            if ($track_discounts && $excludePassenger->age_one != '' && $excludePassenger->age_two != '') {
                //if passenger is an infant assign a seat of zero and minus 1 from $s to make sure a seat is available
                if ($psg_age >= $excludePassenger->age_one && $psg_age <= $excludePassenger->age_two) {
                    $psg_seat = 0;
                    $s--;
                } else {
                    $psg_seat = explode(',', $request->selSeats)[$s];
                }
            } else {
                $psg_seat = explode(',', $request->selSeats)[$i];
            }
            Passenger::firstOrCreate([
                'booking_id' => $booking->id,
                'name' => $psg_name,
                'gender' => $psg_gender,
                'age' => $psg_age,
                'seat_no' => $psg_seat,
                'daily_trip_id' => $dt->id,
                'operator_id' => $request->operator_id,
            ]);
        }

        //adding seats...
        $selSeats = explode(',', $request->selSeats);
        foreach ($selSeats as $s) {
            $seat = new Seat;
            $seat->seat_no = $s;
            $seat->daily_trip_id = $dt->id;
            $seat->booking_id = $booking->id;
            $seat->save();

        }

        //updating bookings daily trip...
        $booking->daily_trip_id = $dt->id;
        $booking->save();
        Utility::$response['status'] = 200;
        Utility::$response['data'] = $booking;
        Utility::$response['message'] = 'Booking Saved Successfully';
        return response()->json(Utility::$response);
    }

    public function postTopup(Request $request)
    {
        Utility::$response = Utility::checkParameters($request, array('agent_id', 'amount'));
        if (Utility::$response['status'] == 404)
            return response()->json(Utility::$response);


        //Get password of submitted email if it exists in DB
        $agent = Agent::find($request->agent_id);

        if (empty($agent)) {
            Utility::$response['status'] = 404;
            Utility::$response['message'] = trans('auth.no_account');

            return response()->json(Utility::$response);
        }


        $at = new AgentTransaction;
        $at->value = ($agent->percentage * $request->amount / 100) + $request->amount;
        // $at->value = $request->amount;
        $at->amount_paid = $request->amount;
        $at->approved = true;
        $at->type = 'TOPUP';

        $agent->transactions()->save($at);
        $agent->balance = $agent->balance + $at->value;
        $agent->save();

        Utility::$response['status'] = 200;
        Utility::$response['data'] = $agent->balance;
        Utility::$response['message'] = 'Success';
        return response()->json(Utility::$response);

    }

    public function postTransactionHistory(Request $request)
    {
        Utility::$response = Utility::checkParameters($request, array('ticketer_id'));
        if (Utility::$response['status'] == 404)
            return response()->json(Utility::$response);


        //Get password of submitted email if it exists in DB
        $agent = Agent::find($request->agent_id);

        if (empty($agent)) {
            Utility::$response['status'] = 404;
            Utility::$response['message'] = trans('auth.no_account');

            return response()->json(Utility::$response);
        }

        $transactions = null;
        if (isset($request->type) && !empty($request->type)) {
            $transactions = $agent->transactions()->where('type', $request->type)->orderBy('created_at', 'desc')->get();
        } else {
            $transactions = $agent->transactions()->orderBy('created_at', 'desc')->get();
        }


        Utility::$response['status'] = 200;
        Utility::$response['data'] = array('transactions' => $transactions);
        Utility::$response['message'] = 'Success';
        return response()->json(Utility::$response);

    }

    public function postCheckBooking(Request $request)
    {

        Utility::$response = Utility::checkParameters($request, array('booking_code', 'contact_phone'));
        if (Utility::$response['status'] == 404)
            return response()->json(Utility::$response);


        $booking = BusBooking::with('trip.sourcepark', 'trip.destpark', 'paymentmethod', 'passengers')->where('booking_code', $request->booking_code)->first();
        if (empty($booking)) {
            Utility::$response['status'] = 404;
            Utility::$response['message'] = 'Booking not found!';

            return response()->json(Utility::$response);
        }


        if ($booking->contact_phone != $request->contact_phone) {
            Utility::$response['status'] = 404;
            Utility::$response['message'] = 'Booking not found!';

            return response()->json(Utility::$response);
        }


        Utility::$response['status'] = 200;
        Utility::$response['data'] = $booking;
        Utility::$response['message'] = 'Success';
        return response()->json(Utility::$response);


    }

    public function postBookingHistory(Request $request)
    {
        /* Utility::$response = Utility::checkParameters($request, array('user_id'));
         if (Utility::$response['status'] == 404)
             return response()->json(Utility::$response);
 */

        //Get password of submitted email if it exists in DB
        $agent = User::find($request->user_id);

        if (empty($agent)) {
            Utility::$response['status'] = 404;
            Utility::$response['message'] = trans('auth.no_account');

            return response()->json(Utility::$response);
        }

        $bookings = $agent->bookings()->with('trip.sourcepark', 'trip.destpark', 'trip.parenttrip.bus.bus_type', 'seats', 'paymentmethod', 'passengers', 'ticketer', 'daily_trip.bus', 'daily_trip.driver')->orderBy('created_at', 'desc')->get();

        Utility::$response['status'] = 200;
        Utility::$response['data'] = $bookings;
        Utility::$response['message'] = 'Success';
        return response()->json(Utility::$response);

    }


}
