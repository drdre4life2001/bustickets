<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Libraries\Utility;
use App\Models\Booking;
use App\Models\BookingNote;
use App\Models\Operator;
use App\Models\Trip;
use App\Models\SubTrip;
use App\Models\Park;
use App\Models\User;
use App\Models\Passenger;
use App\Models\PaymentMethod;
use App\Models\DailyTrip;
use App\Models\Setting;
use App\Models\Driver;
use App\Models\Bus;
use App\Models\Seat;
use App\Models\Discount;
use App\Models\BusBooking;
use App\Models\CharterBooking;
use App\Models\Incident;
use App\Models\IncidentType;
use App\Models\Role;
use App\Models\ExtraLuggage;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Session;
use Auth;
use DB;

class BookingsController extends Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth');
    }


    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {

        $start_date = date('Y-m-d', strtotime('-2weeks'));
        $end_date = date('Y-m-d');

        if(isset($request->daterange)){
            $arr = explode(' ', $request->daterange);
            $start_date = $arr[0];
            $end_date = $arr[1];
        }

        $op_id = session('operator')->id;
        $source_park_id = session('source_park')->id;

        $bookings = BusBooking::with('trip.sourcepark', 'trip.destpark','trip.bus','seats', 'trip.parenttrip.bus', 'daily_trip.bus')
                        ->where('created_at', '>=', $start_date.' 00:00:00')
                        ->where('created_at', '<=', $end_date.' 23:59:59')
                        ->where('operator_id', session('operator')->id)
                        ->where('source_park_id', session('source_park')->id)
                        ->orderBy('created_at', 'desc')->paginate(4000)->toArray();

        $bookings = ($bookings['data']);

        $page_title = 'All Bookings';
        return view('bookings.index', compact('bookings', 'page_title', 'start_date', 'end_date'));
    }
    
    
    public function invoiceReports(Request $request, $status = 'ALL', $park_id = ''){

        $start_date = date('Y-m-d 00:00:00', strtotime('-4weeks'));
        $end_date = date('Y-m-d 23:59:59');

        if(isset($request->daterange)){
            $arr = explode(' ', $request->daterange);
            $start_date = $arr[0].' 00:00:00';
            $end_date = $arr[1].' 23:59:59';
        }

        $op_id = session('operator')->id; //operator

        $query = DailyTrip::with(['driver','ticketer', 'trip.sourcepark', 'trip.destpark', 'trip.subTrips.destpark','bus', 'expenses', 'bookings'=>function($query){ $query->where('status', 'PAID'); }])
                        ->where('trip_date', '>=', $start_date)
                        ->where('trip_date', '<=', $end_date)
                        ->where('booked_seats', '>', 0)
                        ->where('operator_id', session('operator')->id)
                        ->where('source_park_id', session('source_park')->id)
                        ->orderBy('trip_date', 'desc');
        if($status != 'ALL')
            $query = $query->where("status", $status);
        if ($park_id != 0) {
            $trip_ids = Trip::where('source_park_id', $park_id)->lists('id')->toArray();
             $query = $query->whereIn('trip_id', $trip_ids);
         }
        $dailytrips = $query->get();
        $page_title = 'Invoice Reports';

        $statuses = ['LOADING'=>'LOADING', 'TRANSITED'=>'TRANSITED', 'POSTPONED'=>'POSTPONED', 'CANCELLED'=>'CANCELLED'];
        $parks = Park::where('boardable', 1)
        ->where('operator_id', session('operator')->id)
        ->lists('name', 'id')->toArray();

        return view('reports.invoice_reports', compact('dailytrips', 'page_title', 'start_date', 'end_date', 'statuses', 'status', 'park_id', 'parks'));

   }
  public function OLDinvoiceReports(Request $request, $bus_id = '0', $park_id = ''){

        $start_date = date('Y-m-d 00:00:00', strtotime('-2weeks'));
        $end_date = date('Y-m-d 23:59:59');

        if(isset($request->daterange)){
            $arr = explode(' ', $request->daterange);
            $start_date = $arr[0].' 00:00:00';
            $end_date = $arr[1].' 23:59:59';
        }

        $buses = Bus::lists('bus_number', 'id')->toArray();

        $op_id = session('operator')->id; //operator ID
        $source_park_id =  session('source_park')->id;

        $query = DailyTrip::with('driver', 'trip.sourcepark', 'trip.destpark', 'trip.busType', 'bus')
                        ->groupBy('bus_id')
                        ->where('status', 'TRANSITED')
                        ->where('created_at', '>=', $start_date)
                        ->where('created_at', '<=', $end_date)
                        ->where('operator_id', $op_id)
                        ->where('source_park_id', $source_park_id)
                        ->orderBy('created_at', 'desc');
        if(!empty($bus_id)){
            $query = $query->where("bus_id", $bus_id);
            $bus = $buses[$bus_id];
        }

        $dailytrips = $query->get();

        $page_title = 'Invoice Reports';

        $parks = Park::where('boardable', true)->where('operator_id', $op_id)->where('active', true)->lists('name', 'id');

        return view('reports.invoice_reports', compact('dailytrips', 'parks', 'park_id', 'page_title', 'start_date', 'end_date', 'buses', 'bus_id', 'bus'));


   }

public function summarised_invoice_reports(Request $request){
        if (isset($request->daterange)) {
            $arr = explode(' ', $request->daterange);
            $start_date = $arr[0] . ' 00:00:00';
            $end_date = $arr[1] . ' 23:59:59';
        }else{
            $start_date = date('Y-m-d 00:00:00', strtotime('-4weeks'));
            $end_date = date('Y-m-d 23:59:59');
        }
	    $op_id = session('operator')->id; //operator ID
        $park = session('source_park')->id;
        $parks_query = "SELECT (select name FROM parks WHERE id = source_park_id) as park, 
                          (SELECT name FROM parks WHERE id = (select dest_park_id FROM trips WHERE id = trip_id)) as destination, 
                          (SELECT SUM(passenger_count)) as passengers, (SELECT SUM(final_cost)) as revenue FROM bookings 
                           WHERE created_at >= '$start_date' AND created_at <= '$end_date' AND operator_id = '$op_id' AND source_park_id = '$park' AND status = 'PAID' GROUP BY destination";
        $parks_data = DB::select($parks_query);
        $revenue_query = "SELECT (SELECT SUM(final_cost)) as total_revenue,(SELECT SUM(passenger_count)) as total_passengers FROM bookings 
                            WHERE created_at >= '$start_date' AND created_at <= '$end_date' AND operator_id = '$op_id' AND source_park_id = '$park' AND status = 'PAID'";
        $revenue_data = DB::select($revenue_query);
        return view('reports.summary',compact('parks_data','revenue_data','start_date','end_date'));
    }


 public function GetBusTripDetails(Request $request){

        extract($request->all());

        $excludePassenger = Discount::where('operator_id', Auth::user()->operator_id)->where('discount', 100)->first();

         $query = DailyTrip::with('driver', 'trip.sourcepark', 'trip.destpark', 'trip.busType', 'bus')
                        ->where('operator_id', session('operator')->id)
                        ->where('source_park_id', session('source_park')->id)
                        ->where('created_at', '>=', $request->start_date.' 00:00:00')
                        ->where('created_at', '<=', $request->end_date.' 23:59:59')
                        ->orderBy('created_at', 'desc');

            if ($request->bus_id != '') {
                $query = $query->where('bus_id', $request->bus_id);
            }
            if ($request->park_id != '') {
                $trip_ids = Trip::where('source_park_id', $park_id)->lists('id')->toArray();
                $query = $query->whereIn('trip_id', $trip_ids);
            }


             $daily_trips = $query->get(); //done
        return view('reports.get_bus_trip_details', compact('daily_trips','bus_id', 'bus_number', 'excludePassenger', 'start_date', 'end_date'));

   }


    
    public function checkbooking(Request $request){
        //accept booking code and redirect to booking
    }

    public function viewAllBookingsByAPassenger(Request $request){
        //accept phone number and list all bookings by this passenger
            //their could be better orm ways to sort out

        if ($request->isMethod('get')){
            $request->phone = 'no search'; //so that it will return empty array
        }


        $passenger = Passenger::where('phone', $request->phone)->first();

        $daterangetime = '2weeks';

        $start_date = date('Y-m-d 00:00:00', strtotime($daterangetime));
            $end_date = date('Y-m-d 23:59:59');

        if(isset($request->daterange)){
            $arr = explode(' ', $request->daterange);
            $start_date = $arr[0].' 00:00:00';
            $end_date = $arr[1].' 23:59:59';
        }

        if(isset($passenger->name)){
            $page_title = 'All Bookings By '.ucwords($passenger->name). ' | ' .$request->phone.' for the past '.$daterangetime ;

        }else{
            $page_title = 'Search to find all bookings by a customer using their phone number' ;

        }

   
     $bookings = BusBooking::with('trip.sourcepark', 'trip.destpark','trip.bus','seats', 'trip.parenttrip.bus', 'daily_trip.bus')
                        ->where('created_at', '>=', $start_date.' 00:00:00')
                        ->where('created_at', '<=', $end_date.' 23:59:59')
                        ->where('contact_phone', $request->phone)
                        ->orderBy('created_at', 'desc')->paginate(4000)
                        ->toArray();
        $bookings = ($bookings['data']);

        return view('bookings.check_passenger', compact('bookings', 'page_title', 'start_date', 'end_date'));

    }

    public function Status(Request $request, $status = ''){

        $start_date = date('Y-m-d 00:00:00', strtotime('-2weeks'));
        $end_date = date('Y-m-d 23:59:59');

        if(isset($request->daterange)){
            $arr = explode(' ', $request->daterange);
            $start_date = $arr[0].' 00:00:00';
            $end_date = $arr[1].' 23:59:59';

        }

        $page_title = ucwords(strtolower($status).' Bookings');

        $bookings = BusBooking::with('trip.sourcepark', 'trip.destpark', 'paymentmthd',
        'trip.parenttrip','seats', 'trip.parenttrip.bus','daily_trip.bus')
                        ->where('status', $status)
                        ->where('created_at', '>=', $start_date)
                        ->where('created_at', '<=', $end_date)
                        ->where('operator_id', session('operator')->id)
                        ->where('source_park_id', session('source_park')->id)
                        ->orderBy('created_at', 'desc')->paginate(4000)
                        ->toArray();
        $bookings = ($bookings['data']);
  return view('bookings.index', compact('bookings', 'page_title', 'start_date', 'end_date'));

    }


    public function cancel(Request $request, $booking_code){

        $booking = BusBooking::with('trip.sourcepark', 'trip.destpark', 'trip.parenttrip')->where('booking_code', $booking_code)->first();
        $booking->status = 'CANCELLED';
        $booking->save();
        //remvoe booked seats...
        foreach ($booking->seats()->get() as $seat) {
            $seat->delete();
        }

        $daily_trip = $booking->daily_trip;
        if(!empty($daily_trip)){
            $daily_trip->booked_seats = $daily_trip->booked_seats - $booking->passenger_count;
            $daily_trip->total_fare = $daily_trip->total_fare - $booking->final_cost;

            $daily_trip->save();
        }

        Session::flash('flash_message', 'Booking has been cancelled!');
       // nothing is happening to the amount


        return redirect('bookings/'.$booking->id);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $page_title = 'Add booking';

        return view('bookings.create', compact('page_title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        //dd($request->all());
        Booking::create($request->all());
        // get last instance of passenger
        Session::flash('flash_message', 'Booking added!');

        return redirect('bookings');
    }

    public function sendSms(Request $request)
    {

       //settings
        $cid ="";
        $user = "buscom_sms";
        $senderArray = explode(" ", session('operator')->name);
        $senderId = $senderArray[0];
        $to = $request->phone;
        $pass ="bus.com.ng";
       $ch = curl_init();
       $postdata = 'user='.$user.'&pass='.$pass.'&from='.$senderId.'&to='.$to.'&msg='.$request->msgg; //initialize the request variable
     // echo $postdata;

       $url = 'http://cloud.nuobjects.com/api/send/'; //this is the url of the gateway's interface
        $ch = curl_init(); //initialize curl handle
        curl_setopt($ch, CURLOPT_URL, $url); //set the url
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1); //return as a variable
        curl_setopt($ch, CURLOPT_POST, 1); //set POST method
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata); //set the POST variables
        $response = curl_exec($ch); // grab URL and pass it to the browser. Run the whole process and return the response
        curl_close($ch); //close the curl handle

        $strHeader = get_headers($url)[0];

        $statusCode = substr($strHeader, 9, 3 );

       return $statusCode;

    }

    function send_sms($to, $msg)
    {

        $cid = "";
        $user = "info@bus.com.ng";
        // $senderArray = 'Buscomng';
        $senderId =env("SMS_USERNAME");
        $to = $to;
        $pass = env("SMS_PASS");
        $ch = curl_init();
        $postdata = 'username=' . $user . '&password=' . $pass . '&message=' . $msg . '&sender=' . $senderId . '&mobiles=' . $to; //initialize the request variable
        $nmsg = urlencode($msg);
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://portal.nigerianbulksms.com/api/?username=$user&password=$pass&message='$nmsg'&sender=$senderId&mobiles=$to",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",

            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        return $response;

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function show($id)
    {


        $booking = BusBooking::with(['trip.sourcepark','trip.destpark', 'trip.operator','trip.parenttrip.bus.bus_type', 'seats', 'passengers','ticketer', 'daily_trip.bus', 'extra_luggages'])->findOrFail($id)->toArray();
        $page_title = "All Regular Bookings";
        $op =session('operator')->name;
        $tels = explode(",",session('operator')->phone);
        $addr = explode(".",session('operator')->address);
        $free_age_range = Discount::where('operator_id', session('operator')->id)->where('discount', 100)->first();
        // get operator details
        $opImg = session('operator')->img;

        $dets =explode(".", session('operator')->details);

        $notes =  BookingNote::with(['user'])->get();

        $payment_methods = [null => ' -Choose- '] + PaymentMethod::where('is_fulfilment', 1)->where('active', 1)->lists('name', 'id')->toArray();
        $booked_seats = '';
        foreach ($booking['seats'] as $seat) {
           $booked_seats .= $seat['seat_no'].',';
        }

        $booked_seats = substr($booked_seats, 0, strlen($booked_seats) - 1);
       $stop = session()->get('stop');
       
       $initial_source_park = Park::where('id', $booking['id'])->first();

       // dump($booking);

        $page_title = 'View booking details';
        return view('bookings.show', compact('booking', 'page_title', 'initial_source_park', 'notes', 'payment_methods', 'booked_seats','stop', 'op','tels','addr','opImg','dets', 'free_age_range'));
    }

    public function ticket($id)
    {
        $lugW = session('settings')->luggage_limit;
        $aCpk = session('settings')->intl_cost_per_kg;
        $lCpk = session('settings')->local_cost_per_kg;
        $free_age_range = Discount::where('operator_id', Auth::user()->operator_id)->where('discount', 100)->first();

            $l =floatval($lugW);  // luggage threshold
            $lC = floatval($lCpk); //local cpk

            $booking = BusBooking::with(['trip.sourcepark','trip.destpark',
            'trip.operator','trip.parenttrip.bus.bus_type', 'seats', 'passengers',
            'ticketer', 'extra_luggages', 'daily_trip.bus', 'daily_trip.driver'])->findOrFail($id)->toArray();

            $page_title = "All Regular Bookings";
            $op =session('operator')->name;
            $opImg = session('operator')->img;
            $notes =  BookingNote::with(['user'])->get();

            $payment_methods = [null => ' -Choose- '] + PaymentMethod::where('is_fulfilment', 1)->where('active', 1)->lists('name', 'id')->toArray();

            $booked_seats = '';
            foreach ($booking['seats'] as $seat) {
               $booked_seats .= $seat['seat_no'].',';
           }

           $booked_seats = substr($booked_seats, 0, strlen($booked_seats) - 1);
           $stop = session()->get('stop');

           $page_title = 'View booking details';
           return view('bookings.ticket', compact('booking', 'page_title', 'notes', 'payment_methods', 'booked_seats','stop', 'op','opImg','l','lC', 'free_age_range'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function edit($id)
    {
       $booking = BusBooking::with(['trip.sourcepark','trip.destpark', 'trip.operator','trip.parenttrip.bus.bus_type', 'seats', 'passengers', 'ticketer'])->findOrFail($id);
      //dd($booking->passengers);
        $trip = SubTrip::with('parenttrip.default_bus', 'parenttrip.bus','parenttrip')->find($booking->trip->id);
        $page_title = 'Edit booking';

        $booked_seats = '';
        $dt = DailyTrip::where('trip_id', $trip->parenttrip->id)->where('trip_date', date('Y-m-d', strtotime($booking->date)))->first();
        if(!empty($dt)){
            if(!empty($dt->seats())){
                foreach ($dt->seats()->whereNotNull('booking_id')->get() as $seat) {
                    $booked_seats .= $seat->seat_no.',';
                }

                $booked_seats = substr($booked_seats, 0, strlen($booked_seats) - 1);
            }
        }

        $sel_seats = '';
            foreach ($booking['seats'] as $seat) {
               $sel_seats .= $seat['seat_no'].',';
           }

        $sel_seats = substr($sel_seats, 0, strlen($sel_seats) - 1);

        // dump($booked_seats);

        return view('bookings.edit', compact('booking','trip','booked_seats', 'sel_seats', 'page_title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function update($id, Request $request)
    {

         //dd($request->all());

        $booking = BusBooking::findOrFail($id);

        $selSeats = explode(',', $request->selSeats);
        if(intval($request->passenger_count) != count($selSeats)){
            return redirect()->back();
        }
        //removing previous seats...
        foreach ($booking->seats()->get() as $seat) {
            $seat->delete();
        }
        //adding new seats...
        $booking['seat_no'] = $selSeats[0];
        // dd($booking);

        foreach ($selSeats as $s) {
            $seat = new Seat;
            $seat->seat_no = $s;
            $seat->daily_trip_id = $booking->daily_trip_id;
            $seat->booking_id = $booking->id;
            $seat->save();

        }
        //updating passengers...
        $s=0;
        for ($i=1; $i <= $booking->passengers()->count() ; $i++) {
                $s += 1;
                $pId = 'psg_id'.$i;
                $p = 'psg'.$i;
                $psggender = 'psggender'.$i;

                $psgId = $request->$pId;
                $psg_name = $request->$p;
                $psg_gender = $request->$psggender;

                $pass = Passenger::find($psgId);
                $pass->name = $psg_name;
                $pass->gender = $psg_gender;
                $pass->seat_no = $selSeats[$s];

                $pass->save();
            }
        $booking->update($request->all());

        Session::flash('flash_message', 'Booking updated!');

        return redirect()->action('BookingsController@show', [$booking->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        Booking::destroy($id);

        Session::flash('flash_message', 'Booking deleted!');

        return redirect('bookings');
    }

    public function AdminBook(Request $request, $trip_id, $date, $is_flexi, $daily_trip_id = ''){

      
        $page_title = 'Add booking';
        $trip = SubTrip::with('parenttrip.default_bus', 'parenttrip.bus')->find($trip_id);

        $permissions = Auth::user()->role->permissions;
        $flexi = ($is_flexi && !(strpos($permissions, 'book_from_any_park') === FALSE)); // being sure of the permisson...
        $cpk =0;
        $excludePassenger = Discount::where('operator_id',
        Auth::user()->operator_id)->where('discount', 100)->first();
        $trip_name = $trip->parenttrip->name;
        $booked_seats = '';
        if(empty($daily_trip_id))
            $dt = DailyTrip::with('bus')->where('trip_id', $trip->parenttrip->id)->where('trip_date', date('Y-m-d', strtotime($date)))->first();
        else
            $dt = DailyTrip::with('bus')->findOrFail($daily_trip_id);
        if(!empty($dt)){
            if(!empty($dt->seats())){
                foreach ($dt->seats()->whereNotNull('booking_id')->get() as $seat) {
                    $booked_seats .= $seat->seat_no.',';
                }

                $booked_seats = substr($booked_seats, 0, strlen($booked_seats) - 1);
            }
        }
        //$luggSetting = Setting::where('key','track_luggage')->first();
        $track_luggage = (session('settings')->track_luggage == 'True')?TRUE:FALSE;
        $luggage_limit = intval(session('settings')->luggage_limit);
        $track_discounts = (session('settings')->discount_active == 'true')?TRUE:FALSE;
        $infant_range = [];
        if ($request->isMethod('post'))
        {
            if($request->is_round_trip)
                $fare = $trip->round_trip_fare;
            else
                $fare = $trip->fare;

            $unit_cost = $fare;
            $totalcost = 0;

            $request->session()->put('stop', $request->tripStop);
            $this->validate($request, [
                'passenger_count' => 'required',
                'contact_phone' => 'required',
                'contact_name' => 'required',
                //'next_of_kin' => 'required',
                'next_of_kin_phone' => 'required',
                //'payment_method_id' => 'required',
            ]);

            $k = 1;
           for ($i=1; $i < 10 ; $i++) {
            $p = 'psg'.$i;
            $pssg = $request->$p;
            $ag = 'age'.$i;
            $age = $request->$ag;
            if(!empty($pssg))
                $k=$i+1;
            if(!empty($age))
                $k=$i+1;
        }

            // will be used when computing final cost, such that infants can be excluded
            $no_seats = count(explode(',', $request->selSeats));
            // exclude infants
            $multiplier =  (int)$request->passenger_count;
            
               if($no_seats != $multiplier)
                    return redirect()->back()->with('error', 'Passenger Count and Number of passengers inputed do not match');

            
                    $status  = 'PENDING';
                    $paid_date = NULL;
        
            $booking_code = $trip->parenttrip->operator->code.rand(100000,999999);

            if($request->is_round_trip)
                $fare = $trip->round_trip_fare;
            else
                $fare = $trip->fare;

            switch ($request->passport_type) {
                case 'Virgin passport':
                    # code...
                    $fare = ($request->is_round_trip)?$trip->round_trip_virgin_passport_fare:$trip->virgin_passport_fare;
                    break;

                case 'ID card/ No passport':
                    # code...
                    $fare = ($request->is_round_trip)?$trip->round_trip_no_passport_fare:$trip->no_passport_fare;
                    break;

                default:
                    # code...
                    break;
            }


            $final_cost = $final_cost = ($flexi)?$request->final_cost: $fare;
            //luggage...
            $lugg_cost = 0;

            if($track_luggage && $request->luggage_weight != "" && $flexi != true){
                //$luggage_limit = intval(Setting::where('key', 'luggage_limit')->first()->value);
                $cpk = 0;
                if($trip->is_intl_trip){
                    $cpk = intval(session('settings')->intl_cost_per_kg);
                }
                else{

                    $cpk = intval(session('settings')->local_cost_per_kg);

                }

                    //The luggage limit is multiplied by no of paying passengers
                $total_lugg_limit = $multiplier * $luggage_limit;
                //dd($total_lugg_limit);
                $lugg_cost = ($request->luggage_weight - $total_lugg_limit) * $cpk;
                if($lugg_cost < 0)
                    $lugg_cost = 0;


                $final_cost += $lugg_cost;
            }

              // infant logic comes here
                $totalPassengerPrice = 0;
                for ($i=1; $i < $k ; $i++)
                {
                    $p = 'psg'.$i;
                    $psggender = 'psggender'.$i;
                    $psgage ='age'.$i;

                    $psg_name = $request->input($p);
                    $psg_gender = $request->input($psggender);
                    $filledAge = $request->input($psgage);
                    if($filledAge == ''){
                        $filledAge = 30;
                    }
                    $psg_age = intval($filledAge);

                    // AGE CONDITION
                    if($track_discounts && $flexi != true) {
                        $opId = session('operator')->id;
                        $theDiscount = DB::select(DB::raw("select * from discounts where operator_id = '$opId' AND '$psg_age' between age_one and age_two"));

                        if (!empty($theDiscount)) {

                             $theDiscount = $theDiscount[0];

                                $this_discount = (100 - $theDiscount->discount)/100;

                            if($psg_age >= intval($theDiscount->age_one) && $psg_age <= intval($theDiscount->age_two))
                            {   //calculate discount amt of trip's fare
                                $pFare = $this_discount * floatval($fare);
                              
                                $totalPassengerPrice+= $pFare;
                                // add discounted price to original fare
                            }
                        }else{
                            $pFare = floatval($fare);
                            $totalPassengerPrice+= $pFare;
                        }
                    }
                    else
                    {
                        $ppFare = floatval($fare);
                        $totalPassengerPrice+=floatval($fare);

                    }



                }
            $final_cost += $totalPassengerPrice;
            if ($flexi == true) {
                $final_cost = floatval($request->final_cost);
            }

            //the fare for transloaded passengers is null
            //this is because they have already paid at
            //the inital source park they are coming from.
            if(isset($request->initial_source_park_id) || $request->initial_source_park_id != ''){
                $fare = 0.00;
            }
            $booking = BusBooking::firstOrCreate([
                    'trip_id' => $trip_id,
                    'operator_id' => session('operator')->id,
                    'source_park_id' => session('source_park')->id,
                    'date' => date('Y-m-d', strtotime($date)),
                    'passenger_count' => $request->passenger_count,
                    'return_date'=>isset($request->return_date)? $request->return_date:"",
                    'contact_name' => $request->contact_name,
                    'contact_phone' => $request->contact_phone,
                    'is_round_trip'=>$request->is_round_trip,
                    'contact_email' => $request->contact_email,
                    'contact_address' => $request->contact_address,
                    'next_of_kin' => $request->next_of_kin,
                    'next_of_kin_phone' => $request->next_of_kin_phone,
                    'status' => $status,
                    'paid_date' => $paid_date,
                    'final_cost' =>$final_cost,
                    'unit_cost' => $fare,
                    'gender'=> $request->gender,
                    'seat_no'=> explode(',', $request->selSeats)[0],
                    'booking_code' => $booking_code,
                    'user_id' => Auth::user()->id,
                    'passport_type' => $request->passport_type,
                    'luggage_weight' => $request->luggage_weight,
                    'luggage_cost' => $lugg_cost,
                    'is_round_trip' => ($request->is_round_trip),
                    'initial_source_park_id' => $request->initial_source_park_id,
                    'transfer_type' => $request->transfer_type,
                    'nationality' => $request->nationality,
                    'occupation' => $request->occupation,
                    'passport_no' => $request->passport_no,
                    'passport_place_issued' => $request->passport_place_issued,
                    'passport_date_issued' => $request->passport_date_issued,
                    'passport_expiry_date' => $request->passport_expiry_date,
                    'is_flexi' =>  ($flexi)?1:0
                ]);
//
                //    'trip_type' => $trip->is_intl_trip,
            //updating to daily trips...
            if(empty($dt)){
                // creating default...
                $inserts = [
                        'trip_id'=>$trip->parenttrip->id,
                        'trip_date'=>date('Y-m-d', strtotime($date)),
                        'booked_seats'=>0,
                        'total_seats'=>$trip->parenttrip->bus->no_of_seats,
                        'trip_transit'=>'First Bus',
                        'operator_id' => session('operator')->id,
                        'source_park_id' => session('source_park')->id
                        // 'departure_time'=>$trip->parenttrip->departure_time,
                    ];
                $driver_id = $bus_id = '';
                if(!empty($trip->parenttrip->bus)){
                    $inserts['driver_id'] = $trip->parenttrip->bus->default_driver_id;
                    $inserts['bus_id'] = $trip->parenttrip->bus->id;
                }
                $dt = DailyTrip::firstOrCreate($inserts);
            }

            $dt->booked_seats = $dt->booked_seats + $request->passenger_count;
            $dt->total_fare = $dt->total_fare + ($final_cost);
            $dt->save();

           $psSeat =  explode(',', $request->selSeats);
            //$s is what we use to assign seats to passengers
             $s=0;
             for ($i=1; $i < $k ; $i++) {
                $s += 1;
                $p = 'psg'.$i;
                $psggender = 'psggender'.$i;
                // psgtype means infant or not
                $psgtype = 'age'.$i;

                $psg_name = $request->$p;
                $psg_gender = $request->$psggender;
                $fillAged = $request->$psgtype;
                 //We are checking if the age field was filled. If it wasnt then we assume the passenger is an adult
                if($fillAged == ''){
                     $fillAged = 30;
                }
                $psg_age = intval($fillAged);

                 if($track_discounts && $excludePassenger->age_one != '' && $excludePassenger->age_two != '') {
                 //if passenger is an infant assign a seat of zero and minus 1 from $s to make sure a seat is available
                    if($psg_age >= $excludePassenger->age_one && $psg_age <= $excludePassenger->age_two){
                        $psg_seat = 0;
                        $s--;
                    }else{
                        $psg_seat = explode(',', $request->selSeats)[$s];
                    }
                 }else{
                     $psg_seat = explode(',', $request->selSeats)[$i];
                 }

                // echo $booking->id.'<br>';
                $response =  Passenger::firstOrCreate([
                    'booking_id'=>$booking->id,
                    'name'=>$psg_name,
                    'gender'=>$psg_gender,
                    'age'=>$psg_age,
                    'seat_no' => $psg_seat,
                    'daily_trip_id' => $dt->id,
                    'operator_id' => session('operator')->id,
                ]);
            }

            //adding seats...
            $selSeats = explode(',', $request->selSeats);
            foreach ($selSeats as $s) {
                $seat = new Seat;
                $seat->seat_no = $s;
                $seat->daily_trip_id = $dt->id;
                $seat->booking_id = $booking->id;
                $seat->save();

            }

            //updating bookings daily trip...
            $booking->daily_trip_id = $dt->id;
            $booking->save();
            $request_type = isset($request->type) ? $request->type: '';
            if($request_type == 'api'){
                Utility::$response['status'] = 200;
                Utility::$response['data'] = $booking;
                Utility::$response['message'] = trans('auth.success');
                return response()->json(Utility::$response);
            }
            Session::flash('flash_message', 'Booking has been saved!');
            $rt =  $request->is_round_trip;
            return redirect('bookings/'.$booking->id)->with('rt');

        }

        $payment_methods = [null => ' -Choose- '] + PaymentMethod::where('active', 1)->lists('name', 'id')->toArray();
        $all_parks  = Park::where('active', true)->orderBy('name','asc')->get();
        return view('bookings.create', compact('page_title', 'excludePassenger', 'infant_range', 'trip_name', 'trip', 'fare', 'date', 'payment_methods', 'booked_seats', 'dt','flexi','all_parks', 'track_luggage','luggage_limit','cpk'));
    }


    public function NewBook(Request $request){

        if ($request->isMethod('post')) {

            $dt_id = $request->dt_id;
            $dt = DailyTrip::with('trip.subTrips.destpark')->find($request->dt_id);

            $dests = [];
            foreach ($dt->trip->subTrips as $st) {
                $dests[$st->id] = $st->destpark->name;
            }

            return view('bookings.bus_dests', compact('dests', 'dt_id'));


        }

        if(time() > strtotime("2:00pm"))
            $date = (date('Y-m-d', strtotime('+1day')));
        else
            $date = (date('Y-m-d'));

        $dts = DailyTrip::with('bus')->where('operator_id', session('operator')->id)->where('source_park_id', session('source_park')->id)->where('trip_date', $date)->get();
        $buses = [''=>'-Choose-'];
        foreach ($dts as $dt) {
            $buses[$dt->id]  = !empty($dt->bus->number_plate) ? $dt->bus->number_plate : '';
        }
        return view('bookings.book', compact('buses'));

    }


    public function BookDetails(Request $request, $daily_trip_id, $trip_id){


        if(time() > strtotime("2:00pm"))
            $date = (date('Y-m-d', strtotime('+1day')));
        else
            $date = (date('Y-m-d'));

        $trip = SubTrip::with('parenttrip.default_bus', 'parenttrip.bus')->find($trip_id);
        $trip_name = $trip->parenttrip->name;


        $permissions = Auth::user()->role->permissions;
        $excludePassenger = Discount::where('operator_id', Auth::user()->operator_id)->where('discount', 100)->first();


        $booked_seats = '';
        if(empty($daily_trip_id))
            $dt = DailyTrip::with('trip.subTrips.destpark', 'bus')->where('operator_id', session('operator')->id)->where('trip_id', $trip->parenttrip->id)->where('trip_date', date('Y-m-d', strtotime($date)))->first();
        else
            $dt = DailyTrip::with('trip.subTrips.destpark', 'bus')->findOrFail($daily_trip_id);

        if(!empty($dt)){
            if(!empty($dt->seats())){
                foreach ($dt->seats()->whereNotNull('booking_id')->get() as $seat) {
                    $booked_seats .= $seat->seat_no.',';
                }

                $booked_seats = substr($booked_seats, 0, strlen($booked_seats) - 1);
            }
        }

        $dests = array();
        foreach ($dt->trip->subTrips as $st) {
            $dests[$st->id] = $st->destpark->name;
        }

        $track_luggage = (session('settings')->track_luggage == 'True')?TRUE:FALSE ;
        $track_infant = (session('settings')->track_infant == 'True')?TRUE:FALSE;
        $track_discounts = (session('settings')->discount_active == 'true')?TRUE:FALSE;

        $cpk =0;
        if($track_luggage){

            $luggage_limit = intval(session('settings')->luggage_limit);
            $cpk = 0;
            if($trip->is_intl_trip)
                $cpk = intval(session('settings')->intl_cost_per_kg);
            else
                $cpk = intval(session('settings')->local_cost_per_kg);


        }

        if ($request->isMethod('post'))
        {
            $this->validate($request, [
                'passenger_count' => 'required',
                'contact_phone' => 'required',
                'contact_name' => 'required',
                'next_of_kin_phone' => 'required',
                'payment_method' => 'required',
            ]);

            $payment_method_id = 0;

            $payment_method = $request->payment_method;
            if($payment_method != 'Book on Hold'){
                $status = 'PAID';
                $paid_date = date('Y-m-d H:i:s');
                $sms_status = 'PURCHASE NOTICE';

            }
            else{
                $status = 'PENDING';
                $paid_date = NULL;
                $sms_status = 'BOOK ON HOLD';
                $payment_method_id = 3;
            }

            if ($payment_method == 'Cash') {
                $payment_method_id = 2;
            }elseif ($payment_method == 'Card') {
                $payment_method_id = 3;
            }
            $flexi = $request->is_flexi;
            $flexi = ($flexi && !(strpos($permissions, 'book_from_any_park') === FALSE)); // being sure of the permisson...

            // will be used when computing final cost, such that infants can be excluded
            $infantCount =0;

            $k = 1;
            for ($i=1; $i < 10 ; $i++) {
                $p = 'psg'.$i;
                $psgtype = 'psgtype'.$i;
                $pssg = $request->$p;
                $psgTyp = $request->$psgtype;
                if(!empty($pssg)){
                    $k=$i+1;

                }

                if(!empty($psgTyp) &&  $psgTyp==1){
                    $infantCount+=1;
                }
            }
            // exclude infants

            $multiplier =  (int)$request->passenger_count;
            $no_seats = count(explode(',', $request->selSeats));

            // exit;
               if($no_seats != $multiplier){
                    return redirect()->back()->with('error', 'Passenger Count and Number of passengers inputed do not match');
               }

            $booking_code = $trip->parenttrip->operator->code.rand(100000,999999);
            if($request->is_round_trip)
                $fare = $trip->round_trip_fare;
            else
                $fare = $trip->fare;

             switch ($request->passport_type) {
                case 'Virgin passport':
                    # code...
                    $fare = ($request->is_round_trip)?$trip->round_trip_virgin_passport_fare:$trip->virgin_passport_fare;
                    break;

                case 'ID card/ No passport':
                    # code...
                    $fare = ($request->is_round_trip)?$trip->round_trip_no_passport_fare:$trip->no_passport_fare;
                    break;

                default:
                    # code...
                    break;
            }

            $final_cost = ($flexi)?$request->final_cost:$fare;

            //luggage...
            $lugg_cost = 0;

            if($track_luggage && $request->luggage_weight != '' && $flexi != true){

                $luggage_limit = intval(session('settings')->luggage_limit);
                $cpk = 0;
                if($trip->is_intl_trip)
                    $cpk = intval(session('settings')->intl_cost_per_kg);
                else
                    $cpk = intval(session('settings')->local_cost_per_kg);


                $total_lugg_limit = $multiplier * $luggage_limit;
                $lugg_cost = ($request->luggage_weight - $total_lugg_limit) * $cpk;
                if($lugg_cost < 0)
                    $lugg_cost = 0;


                $final_cost += $lugg_cost;
            }

             // infant logic comes here
                $totalPassengerPrice = 0;
                for ($i=1; $i < $k ; $i++)
                {
                $p = 'psg'.$i;
                $psggender = 'psggender'.$i;
                $psgage ='age'.$i;
                //
                $psg_name = $request->input($p);
                $psg_gender = $request->input($psggender);
                $quickAge = $request->input($psgage);

 
                if($request->is_round_trip){
                    $psOccupation ='occupation'.$i;
                    $psNationality ='nationality'.$i;
                    $psPassport_date_issued ='passport_date_issued'.$i;
                    $psPassport_number ='passport_number'.$i;
                    $psPassport_place_issued ='passport_place_issued'.$i;
                    $psPassport_expiry_date ='passport_expiry_date'.$i;
                    $psg_psPhone = 'phone'.$i;
                //
                    $psg_psOccupation = $request->input($psOccupation);
                    $psg_psNationality = $request->input($psNationality);
                    $psg_psPassport_date_issued = $request->input($psPassport_date_issued);
                    $psg_psPassport_number = $request->input($psPassport_number);
                    $psg_psPassport_place_issued = $request->input($psPassport_place_issued);
                    $psg_psPassport_expiry_date = $request->input($psPassport_expiry_date);
                    $psg_psPhone = $request->input($psg_psPhone);
                }
                
                if($quickAge == ''){
                    $quickAge = 30;
                }
                $psg_age = intval($quickAge);


                // AGE CONDITION
                if($track_discounts && $psg_age != '' && $flexi != true) {

                    $opId = session('operator')->id;

                        $theDiscount = DB::select(DB::raw("select * from discounts where operator_id = '$opId' AND '$psg_age' between age_one and age_two"));

                        if (!empty($theDiscount)) {

                             $theDiscount = $theDiscount[0];

                                $this_discount = (100 - $theDiscount->discount)/100;
                               
                            if($psg_age >= intval($theDiscount->age_one) && $psg_age <= intval($theDiscount->age_two))
                            {   //calculate discount amt of trip's fare
                                $pFare = $this_discount * floatval($fare);                              
                                $totalPassengerPrice+= $pFare;
                                // add discounted price to original fare
                            }
                        }else{
                            $pFare = floatval($fare);
                            $totalPassengerPrice+= $pFare;
                        }
                    }
                    else
                    {
                        $ppFare = floatval($fare);
                        $totalPassengerPrice+=floatval($fare);

                    }

                }

            $final_cost += $totalPassengerPrice;
            if ($flexi == true) {
                $final_cost = floatval($request->final_cost);
            }

            $booking = BusBooking::firstOrCreate([
                    'trip_id' => $trip_id,
                    'operator_id' => session('operator')->id,
                    'source_park_id' => session('source_park')->id,
                    'date' => date('Y-m-d', strtotime($date)),
                    'passenger_count' => $request->passenger_count,
                    'return_date'=>isset($request->return_date)? $request->return_date:"",
                    'contact_name' => $request->contact_name,
                    'contact_phone' => $request->contact_phone,
                    'is_round_trip'=>$request->is_round_trip,
                    'contact_address' => $request->contact_address,
                    'next_of_kin' => $request->next_of_kin,
                    'next_of_kin_phone' => $request->next_of_kin_phone,
                    'status' => $status,
                    'paid_date' => $paid_date,
                    'final_cost' =>$final_cost,
                    'unit_cost' => $fare,
                    'gender'=> $request->gender,
                    'seat_no'=> explode(',', $request->selSeats)[0],
                    'booking_code' => $booking_code,
                    'user_id' => Auth::user()->id,
                    'passport_type' => $request->passport_type,
                    'luggage_weight' => $request->luggage_weight,
                    'luggage_cost' => $lugg_cost,
                    'payment_method_id' => $payment_method_id,
                    'is_flexi' =>  ($flexi)?1:0
                ]);
            //updating to daily trips...
            if(empty($dt)){
                // creating default...
                $inserts = [
                        'trip_id'=>$trip->parenttrip->id,
                        'trip_date'=>date('Y-m-d', strtotime($date)),
                        'booked_seats'=>0,
                        'total_seats'=>$trip->parenttrip->bus->no_of_seats,
                        'trip_transit'=>'First Bus',
                        'operator_id' => session('operator')->id,
                        'source_park_id' => session('source_park')->id
                        // 'departure_time'=>$trip->parenttrip->departure_time,

                    ];
                $driver_id = $bus_id = '';
                if(!empty($trip->parenttrip->bus)){
                    $inserts['driver_id'] = $trip->parenttrip->bus->default_driver_id;
                    $inserts['bus_id'] = $trip->parenttrip->bus->id;
                }

                $dt = DailyTrip::firstOrCreate($inserts);

            }

            $dt->booked_seats = $dt->booked_seats + $request->passenger_count;
            $dt->total_fare = $dt->total_fare + ($final_cost);
            $dt->save();

                $s=0;
               for ($i=1; $i < $k ; $i++) {

                $s +=1;
                $p = 'psg'.$i;
                $psggender = 'psggender'.$i;
                // psgtype means infant or not
                $psgtype = 'age'.$i;
                $psg_name = $request->$p;
                $psg_gender = $request->$psggender;
                $quickAge2 = $request->$psgtype;

                if($quickAge2 == ''){
                     $quickAge2 = 30;
                 }
                 $psg_age = intval($quickAge2);
                if($request->is_round_trip){
                    $psOccupation ='occupation'.$i;
                    $psNationality ='nationality'.$i;
                    $psPassport_date_issued ='passport_date_issued'.$i;
                    $psPassport_number ='passport_number'.$i;
                    $psPassport_place_issued ='passport_place_issued'.$i;
                    $psPassport_expiry_date ='passport_expiry_date'.$i;
                //
                    $psg_psOccupation = $request->input($psOccupation);
                    $psg_psNationality = $request->input($psNationality);
                    $psg_psPassport_date_issued = $request->input($psPassport_date_issued);
                    $psg_psPassport_number = $request->input($psPassport_number);
                    $psg_psPassport_place_issued = $request->input($psPassport_place_issued);
                    $psg_psPassport_expiry_date = $request->input($psPassport_expiry_date);
                    $psg_psPhone = $request->input($psPhone);
                }

                if($track_discounts && $excludePassenger->age_one != '' && $excludePassenger->age_two != '') {
                 //if passenger is an infant assign a seat of zero and minus 1 from $s to make sure a seat is available
                    if($psg_age >= $excludePassenger->age_one && $psg_age <= $excludePassenger->age_two){
                        $psg_seat = 0;
                        $s--;
                    }else{
                        $psg_seat = explode(',', $request->selSeats)[$s];
                    }
                 }else{
                     $psg_seat = explode(',', $request->selSeats)[$i];
                 }
                  if($request->is_round_trip){
                $response =  Passenger::firstOrCreate([
                    'booking_id'=>$booking->id,
                    'name'=>$psg_name,
                    'gender'=>$psg_gender,
                    'age'=>$psg_age,
                    'seat_no' => $psg_seat,
                    'daily_trip_id' => $dt->id,
                    'operator_id' => session('operator')->id,
                    'occupation' => $psg_psOccupation,
                    'nationality' => $psg_psNationality,
                    'passport_type' => $psg_psPassport_date_issued,
                    'passport_no' => $psg_psPassport_number,
                    'passport_date_issued' => $psg_psPassport_date_issued,
                    'passport_place_issued' => $psg_psPassport_place_issued,
                    'passport_expiry_date' => $psg_psPassport_expiry_date,
                    'phone' => $psg_psPhone
                ]);
            }else{
                $response =  Passenger::firstOrCreate([
                    'booking_id'=>$booking->id,
                    'name'=>$psg_name,
                    'gender'=>$psg_gender,
                    'age'=>$psg_age,
                    'seat_no' => $psg_seat,
                    'daily_trip_id' => $dt->id,
                    'operator_id' => session('operator')->id
                ]);
            }
            }


            //adding seats...
            $selSeats = explode(',', $request->selSeats);
            foreach ($selSeats as $s) {
                $seat = new Seat;
                $seat->seat_no = $s;
                $seat->daily_trip_id = $dt->id;
                $seat->booking_id = $booking->id;
                $seat->save();

            }

            //updating bookings daily trip...
            $booking->daily_trip_id = $dt->id;
            $booking->save();


            $booked_seats = '';
            foreach ($booking->seats as $seat) {
               $booked_seats .= $seat->seat_no.',';

            }
            $booked_seats = substr($booked_seats, 0, strlen($booked_seats) - 1);
            //sending sms....
            $msg = session('operator')->name . " ".$sms_status." : "." \nBook Code: ". $booking->booking_code. " \nFrom: "." ".
               $booking->trip->sourcepark->name
                ."to ".$booking->trip->destpark->name.
                ". \nName: ".$booking->contact_name
                .".\nBus: ".$booking->daily_trip->bus->number_plate.
               ". \nDeparture Date: ". $booking->date.
               ". \nSeats: ". $booked_seats.
               ". \nPassengers: ".$booking->passenger_count.
               ". \nFinal Cost: NGN". $booking->final_cost." \nCustomer Care Contact:" .session('operator')->phone;

           // actual sending
          $res = $this->send_sms($booking->contact_phone,$msg);
    
          return 'success--'.$booking->id;

        }

        $payment_methods = [null => ' -Choose- '] + PaymentMethod::where('active', 1)->lists('name', 'id')->toArray();


        return view('bookings.book_details', compact('page_title', 'fare', 'date', 'excludePassenger', 'payment_methods', 'booked_seats', 'dt','flexi', 'dests', 'trip', 'trip_name','track_luggage', 'luggage_limit','cpk'));

    }


    public function FlexiBook(Request $request, $trip_id, $date){

        $page_title = 'Flexi booking';
        $trip = SubTrip::with('parenttrip.default_bus', 'parenttrip.bus')->find($trip_id);
        // dd($trip->toArray());
        $trip_name = $trip->parenttrip->name;
        $fare = $trip->fare;

        $booked_seats = '';
        $dt = DailyTrip::where('trip_id', $trip->parenttrip->id)->where('trip_date', date('Y-m-d', strtotime($date)))->first();
        if(!empty($dt)){
            if(!empty($dt->seats())){
                foreach ($dt->seats()->whereNotNull('booking_id')->get() as $seat) {
                    $booked_seats .= $seat->seat_no.',';
                }

                $booked_seats = substr($booked_seats, 0, strlen($booked_seats) - 1);
            }
        }
        // dd($trip);
        if ($request->isMethod('post')) {

            $this->validate($request, [
                'passenger_count' => 'required',
                'contact_phone' => 'required',
                'contact_name' => 'required',
                'next_of_kin' => 'required',
                'next_of_kin_phone' => 'required',
            ]);

            $k = 1;
            for ($i=1; $i < 10 ; $i++) {
                $p = 'psg'.$i;
                $pssg = $request->$p;
                // dd($pssg);
                if(!empty($pssg))
                    $k=$i+1;
                    //echo $i.''.$pssg.'<br>';
            }
            // exit;

               if((int)$request->passenger_count != $k)
                    return redirect()->back()->with('error', 'Passenger Count and Names of passengers inputed do not match');             
                    $status  = 'PENDING';
                    $paid_date = NULL;
               
            $booking_code = $trip->parenttrip->operator->code.rand(100000,999999);

            $booking = Booking::firstOrCreate([
                    'trip_id' => $trip_id,
                    'date' => date('Y-m-d', strtotime($date)),
                    'passenger_count' => $request->passenger_count,
                    'contact_name' => $request->contact_name,
                    'contact_phone' => $request->contact_phone,
                    // 'contact_elo' => $request->contact_email,
                    'contact_address' => $request->contact_address,
                    'next_of_kin' => $request->next_of_kin,
                    'next_of_kin_phone' => $request->next_of_kin_phone,
                    'status' => $status,
                    'paid_date' => $paid_date,
                    'final_cost' => $request->final_cost,
                    'unit_cost' => $fare,
                    'booking_code' => $booking_code,
                    'user_id' => Auth::user()->id,
                ]);

            for ($i=1; $i < $k ; $i++) {
                $p = 'psg'.$i;
                $psg_name = $request->$p;
                // echo $booking->id.'<br>';
                $response =  Passenger::firstOrCreate([
                    'booking_id'=>$booking->id,
                    'name'=>$psg_name,
                ]);
            }
            //updating to daily trips...
            $dt = DailyTrip::where('trip_id', $trip_id)->where('trip_date', date('Y-m-d', strtotime($date)))->first();

            if(empty($dt)){
                // creating default...
                $inserts = [
                        'trip_id'=>$trip_id,
                        'trip_date'=>date('Y-m-d', strtotime($date)),
                        'booked_seats'=>$trip->parenttrip->no_of_seats,
                        'total_seats'=>$trip->parenttrip->no_of_seats,

                    ];
                $driver_id = $bus_id = '';
                if(!empty($trip->parenttrip->default_bus)){
                    $inserts['driver_id'] = $trip->parenttrip->default_bus->driver_id;
                    $inserts['bus_id'] = $trip->parenttrip->default_bus->id;
                }

                $dt = DailyTrip::firstOrCreate($inserts);
            }

            $dt->booked_seats = $dt->booked_seats - $request->passenger_count;
            $dt->total_fare = $dt->total_fare + ($request->passenger_count*$fare);
            $dt->save();
            $selSeats = explode(',', $request->selSeats);
            foreach ($selSeats as $s) {
                $seat = new Seat;
                $seat->seat_no = $s;
                $seat->daily_trip_id = $dt->id;
                $seat->booking_id = $booking->id;
                $seat->save();

            }

            //updating bookings daily trip...
            $booking->daily_trip_id = $dt->id;
            $booking->save();


            Session::flash('flash_message', 'Booking has been saved!');

            return redirect('bookings/'.$booking->id);

        }

        $payment_methods = [null => ' -Choose- '] + PaymentMethod::where('active', 1)->lists('name', 'id')->toArray();

        $flexi = TRUE;

        return view('bookings.create', compact('page_title', 'trip_name', 'trip', 'fare', 'date', 'payment_methods', 'booked_seats','flexi'));
    }

    public function AgentBooking(Request $request, $user_id){
        $user = User::find($user_id);
        $page_title = $user->first_name.' Bookings';

        $bookings = Booking::with(['trip'])->where('user_id', $user_id)->get();
        return view('bookings.agentIndex', compact('bookings', 'page_title'));
    }

    public function UpdateStatus(Request $request){


        if ($request->isMethod('post')) {

            // echo $request->status;

            $res =Booking::where('id', $request->booking_id)
                    ->update(['status' => 'PAID', 'payment_method_id'=>$request->payment_method_id, 'receipt_no'=>isset($request->receiptNo)?$request->receiptNo:'','paid_date'=>date('Y-m-d H:m:s')]);

            if($res)
              echo $request->status;
        }
   }

   public function Note(Request $request){

        if ($request->isMethod('post')) {

            $user = Auth::user();
            $store = DB::table('booking_notes')->insert([
                ['booking_id' => $request->booking_id, 'notes' => $request->notes, 'user_id'=> $user->id, 'created_at'=>date('Y-m-d H:i:s')],
            ]);

            if($store){

                $notes =  BookingNote::with(['user'])->get();

                 $view = view('bookings.notes', compact('notes'));
                 return $view;
            }
        }
   }

   public function TravelerReports(Request $request, $park_id=""){

        $start_date = date('Y-m-d', strtotime('-2weeks'));
        $end_date = date('Y-m-d');

        if(isset($request->daterange)){
            $arr = explode(' ', $request->daterange);
            $start_date = $arr[0];
            $end_date = $arr[1];
        }
         $query = BusBooking::groupBy('contact_phone')
                        ->where('status', 'PAID')
                        ->where('created_at', '>=', $start_date.' 00:00:00')
                        ->where('operator_id', session('operator')->id)
                        ->where('source_park_id', session('source_park')->id)
                        ->where('created_at', '<=', $end_date.' 23:59:59');

        if ($park_id != '') {
            $trip_ids = Trip::where('source_park_id', $park_id)->where('operator_id', session('source_park')->id)->lists('id')->toArray();
             $query = $query->whereIn('trip_id', $trip_ids);
        }
        $bookings = $query->orderBy('created_at', 'desc')->paginate(4000)
                        ->toArray();

        $bookings = ($bookings['data']);
        $page_title = 'Traveler Reports';

         $parks = Park::where('boardable', 1)->where('operator_id', session('operator')->id)->lists('name', 'id')->toArray();

        return view('reports.traveler_reports', compact('bookings', 'parks', 'park_id', 'page_title', 'start_date', 'end_date'));
   }
   public function BookingReports(Request $request, $status = 'ALL', $park_id = ''){

        $start_date = date('Y-m-d', strtotime('-2weeks'));
        $end_date = date('Y-m-d');

        if(isset($request->daterange)){
            $arr = explode(' ', $request->daterange);
            $start_date = $arr[0];
            $end_date = $arr[1];
        }

        $op_id = session('operator')->id;
        $source_park_id = session('source_park')->id;
        $query = BusBooking::with('trip.sourcepark', 'trip.destpark')
                        ->where('created_at', '>=', $start_date.' 00:00:00')
                        ->where('created_at', '<=', $end_date.' 23:59:59')
                        ->where('operator_id', $op_id)
                        ->where('source_park_id', $source_park_id)
                        ->orderBy('created_at', 'desc');
        if($status != 'ALL')
            $query = $query->where('operator_id', $op_id)
            ->where('source_park_id', $source_park_id)
            ->where("status", $status);

        if ($park_id != 0) {
            $trip_ids = Trip::where('source_park_id', $park_id)->where('operator_id', $op_id)->lists('id')->toArray();
             $query = $query->whereIn('trip_id', $trip_ids);
         }

        $bookings = $query->paginate(10000)
                        ->toArray();

        $bookings = ($bookings['data']);
        $page_title = 'Bookings Reports';
        $parks = Park::where('boardable', 1)->where('operator_id', $op_id)->lists('name', 'id')->toArray();
        return view('reports.booking_reports', compact('bookings', 'page_title', 'start_date', 'end_date', 'statuses', 'status', 'park_id', 'parks'));
   }


   public function CharterBookingReports(Request $request, $status = 'ALL', $park_id = ''){

        $start_date = date('Y-m-d', strtotime('-2weeks'));
        $end_date = date('Y-m-d');

        if(isset($request->daterange)){
            $arr = explode(' ', $request->daterange);
            $start_date = $arr[0];
            $end_date = $arr[1];
        }

        $op_id = session('operator')->id;
        $source_park_id = session('source_park')->id;
        $query = CharterBooking::with('park')
                        ->where('created_at', '>=', $start_date)
                        ->where('created_at', '<=', $end_date)
                        ->where('operator_id', $op_id)
                        ->where('source_park_id', $source_park_id)
                        ->orderBy('created_at', 'desc');
        if($status != 'ALL')
            $query = $query->where("status", $status);

        if(!empty($park_id))
            $query = $query->where("park_id", $park_id);

        $bookings = $query->paginate(4000)
                        ->toArray();

        $bookings = ($bookings['data']);
        $page_title = 'Charter Bookings Reports';

        $parks = Park::where('boardable', 1)->where('operator_id', $op_id)->lists('parks.name', 'parks.id')->toArray();
        return view('reports.charter_booking_reports', compact('bookings', 'page_title', 'start_date', 'end_date', 'statuses', 'status', 'park_id', 'parks'));
   }

   public function IncidentReports(Request $request, $type_id = '', $bus_id = '', $park_id = ''){

        $start_date = date('Y-m-d', strtotime('-2weeks'));
        $end_date = date('Y-m-d 23:59:59');

        if(isset($request->daterange)){
            $arr = explode(' ', $request->daterange);
            $start_date = $arr[0];
            $end_date = $arr[1].' 23:59:59';;
        }

        $op_id = session('operator')->id; //operator ID
        $source_park_id =  session('source_park')->id;

        $query = Incident::with('incidentType')
                        ->where('created_at', '>=', $start_date)
                        ->where('created_at', '<=', $end_date)
                        ->where('operator_id', $op_id)
                        ->where('park_id', $source_park_id)
                        ->orderBy('created_at', 'desc');

        if(!empty($type_id))
            $query = $query->where("incident_type_id", $type_id)->where('operator_id', $op_id);

        if(!empty($bus_id))
            $query = $query->where("bus_id", $bus_id)->where('operator_id', $op_id);

        if(!empty($park_id))
            $query = $query->where("park_id", $park_id)->where('operator_id', $op_id);

        $incidents = $query->get();

        $page_title = 'Incident Reports';

        $parks = Auth::user()->parks()->lists('parks.name', 'parks.id')->toArray();
        $buses = Bus::lists('bus_number', 'id')->toArray();
        $types = IncidentType::where('is_active', true)->lists('name', 'id')->toArray();



        return view('reports.incident_reports', compact('incidents', 'page_title', 'start_date', 'end_date', 'type_id', 'bus_id','park_id','types','parks', 'buses'));
   }


   public function TicketersReports(Request $request, $ticketer_id = '', $park_id = '', $dest_id=''){

        $start_date = date('Y-m-d 00:00:00', strtotime('-2weeks'));
        $end_date = date('Y-m-d');

        if(isset($request->daterange)){
            $arr = explode(' ', $request->daterange);
            $start_date = $arr[0];
            $end_date = $arr[1];
        }

        $op_id = session('operator')->id; //operator ID
        $source_park_id =  session('source_park')->id;

        $ticketers = User::where('exposed', 1)->where('source_park_id', session('source_park')->id)->lists('first_name', 'id')->toArray(); //add role condition
        // $ticketer_role = Role::where('name','Ticketer')->first()->id
        $query = User::where('role_id', 2)->where('operator_id', session('operator')->id);

        if(!empty($ticketer_id)){
            $query = $query->where("id", $ticketer_id);
            $ticketer = User::find($ticketer_id);

            $parks = $ticketer->park()->lists('name', 'parks.id');
            $destinations = $parks = Park::where('active', 1)
            ->where('operator_id', session('operator')->id)
            ->lists('name', 'id')->toArray();

            $users = $query->get();
        }else{
            $users = $query->get();
            $parks = $parks = Park::where('boardable', 1)->where('active', 1)->lists('name', 'id')->toArray();
            $destinations = $parks = Park::where('active', 1)
            ->where('operator_id', session('operator')->id)
            ->lists('name', 'id')->toArray();

        }

        $page_title = 'Ticketers Reports';

        return view('reports.ticketers_reports', compact('users', 'page_title', 'start_date', 'end_date', 'ticketers', 'ticketer_id', 'ticketer', 'parks', 'destinations', 'park_id', 'dest_id'));


   }

 public function destinationsReports(Request $request, $status = 'ALL', $park_id = ''){

        $start_date = date('Y-m-d 00:00:00');
        $end_date = date('Y-m-d 23:59:59');

        if(isset($request->daterange)){
            $arr = explode(' ', $request->daterange);
            $start_date = $arr[0].' 00:00:00';
            $end_date = $arr[1].' 23:59:59';
        }

        $op_id = session('operator')->id; //operator

        $query = DailyTrip::with(['driver','ticketer', 'trip.sourcepark', 'trip.destpark', 'trip.subTrips.destpark','bus', 'expenses', 'bookings'=>function($query){ $query->where('status', 'PAID'); }])
                        ->where('trip_date', '>=', $start_date)
                        ->where('trip_date', '<=', $end_date)
                        ->where('operator_id', session('operator')->id)
                        ->where('source_park_id', session('source_park')->id)
                        ->orderBy('trip_date', 'desc');

        if($status != 'ALL')
            $query = $query->where("status", $status);
        if ($park_id != 0) {
            $trip_ids = Trip::where('source_park_id', $park_id)->lists('id')->toArray();
             $query = $query->whereIn('trip_id', $trip_ids);
         }

        // if(!empty(var))
        $dailytrips = $query->get();

        $page_title = 'Daily Trips Reports';

        $statuses = ['LOADING'=>'LOADING', 'TRANSITED'=>'TRANSITED', 'POSTPONED'=>'POSTPONED', 'CANCELLED'=>'CANCELLED'];
        $parks = Park::where('boardable', 1)
        ->where('operator_id', session('operator')->id)
        ->lists('name', 'id')->toArray();

        return view('reports.destinations_reports', compact('dailytrips', 'page_title', 'start_date', 'end_date', 'statuses', 'status', 'park_id', 'parks'));


   }




    public function TripsReports(Request $request, $status = 'ALL', $park_id = ''){

        $start_date = date('Y-m-d 00:00:00');
        $end_date = date('Y-m-d 23:59:59');

        if(isset($request->daterange)){
            $arr = explode(' ', $request->daterange);
            $start_date = $arr[0].' 00:00:00';
            $end_date = $arr[1].' 23:59:59';
        }

        $op_id = session('operator')->id; //operator

        $query = DailyTrip::with(['driver','ticketer', 'trip.sourcepark', 'trip.destpark', 'trip.subTrips.destpark','bus', 'expenses', 'bookings'=>function($query){ $query->where('status', 'PAID'); }])
                        ->where('trip_date', '>=', $start_date)
                        ->where('trip_date', '<=', $end_date)
                        ->where('operator_id', session('operator')->id)
                        ->where('source_park_id', session('source_park')->id)
                        ->orderBy('trip_date', 'desc');

        if($status != 'ALL')
            $query = $query->where("status", $status);
        if ($park_id != 0) {
            $trip_ids = Trip::where('source_park_id', $park_id)->lists('id')->toArray();
             $query = $query->whereIn('trip_id', $trip_ids);
         }
        $dailytrips = $query->get();



        $page_title = 'Daily Trips Reports';

        $statuses = ['LOADING'=>'LOADING', 'TRANSITED'=>'TRANSITED', 'POSTPONED'=>'POSTPONED', 'CANCELLED'=>'CANCELLED'];
        $parks = Park::where('boardable', 1)
        ->where('operator_id', session('operator')->id)
        ->lists('name', 'id')->toArray();
        return view('reports.trips_reports', compact('dailytrips', 'page_title', 'start_date', 'end_date', 'statuses', 'status', 'park_id', 'parks'));


   }

    public function chairman_sms(Request $request, $status = 'ALL', $park_id = ''){

        $to = env("Chairman") ;

        $total_revenue = $request->total_revenue;
        $total_expenditure = $request->total_expenditure;
        $net_revenue = $request->net_revenue;
        $source_park =  session('source_park')->name;
        //dd($source_park);

        $msg = "$source_park Report:\nTotal Revenue: " . $total_revenue. " \nTotal Expenses: " . $total_expenditure  . "\nNet Revenue: " . $net_revenue;

        $start_date = date('Y-m-d 00:00:00');
        $end_date = date('Y-m-d 23:59:59');

        if(isset($request->daterange)){
            $arr = explode(' ', $request->daterange);
            $start_date = $arr[0].' 00:00:00';
            $end_date = $arr[1].' 23:59:59';
        }

        $op_id = session('operator')->id; //operator

        $query = DailyTrip::with(['driver','ticketer', 'trip.sourcepark', 'trip.destpark', 'trip.subTrips.destpark','bus', 'expenses', 'bookings'=>function($query){ $query->where('status', 'PAID'); }])
            ->where('trip_date', '>=', $start_date)
            ->where('trip_date', '<=', $end_date)
            ->where('operator_id', session('operator')->id)
            ->where('source_park_id', session('source_park')->id)
            ->orderBy('trip_date', 'desc');

        if($status != 'ALL')
            $query = $query->where("status", $status);
        if ($park_id != 0) {
            $trip_ids = Trip::where('source_park_id', $park_id)->lists('id')->toArray();
            $query = $query->whereIn('trip_id', $trip_ids);
        }
        $dailytrips = $query->get();
        $tf= [];
        $msg = 'm';
        $sms = $this->send_sms($to, $msg);
        $page_title = 'Daily Trips Reports';

        $statuses = ['LOADING'=>'LOADING', 'TRANSITED'=>'TRANSITED', 'POSTPONED'=>'POSTPONED', 'CANCELLED'=>'CANCELLED'];
        $parks = Park::where('boardable', 1)
            ->where('operator_id', session('operator')->id)
            ->lists('name', 'id')->toArray();
        if ($request->isMethod('post')) {
            //dd($chairman);
            return view('reports.sent_sms', compact('dailytrips', 'page_title', 'start_date', 'end_date', 'statuses', 'status', 'park_id', 'parks'));

            $sms = $this->send_sms($to, $msg);

        }

        return view('reports.send_sms', compact('dailytrips', 'page_title', 'start_date', 'end_date', 'statuses', 'status', 'park_id', 'parks'));


    }

    public function DriversReports(Request $request, $driver_id = '', $park_id = '0',$dest_id='0'){

        $start_date = date('Y-m-d 00:00:00', strtotime('-2weeks'));
        $end_date = date('Y-m-d 23:59:59');

        if(isset($request->daterange)){
            $arr = explode(' ', $request->daterange);
            $start_date = $arr[0].' 00:00:00';
            $end_date = $arr[1].' 23:59:59';
        }

        $drivers = Driver::lists('name', 'id')->toArray();

        $op_id = session('operator')->id; //operator ID
        $source_park_id =  session('source_park')->id;

        $query = DailyTrip::with(
          'driver',
        'trip.sourcepark',
        'trip.destpark',
        'trip.busType',
        'bus')
                        ->groupBy('driver_id')
                        ->where('created_at', '>=', $start_date)
                        ->where('created_at', '<=', $end_date)
                        ->where('operator_id', session('operator')->id)
                        ->where('source_park_id', $source_park_id)
                        ->orderBy('created_at', 'desc');
        if(!empty($driver_id)){
            $query = $query->where("driver_id", $driver_id);
            $driver = $drivers[$driver_id];
        }

        $parks = Park::where('boardable', true)
        ->where('operator_id', session('operator')->id)
        ->where('active', true)->lists('name', 'id');

        $dailytrips = $query->get();
        $page_title = 'Drivers Reports';
        return view('reports.drivers_reports', compact('dailytrips', 'page_title', 'parks', 'park_id', 'destinations', 'start_date', 'end_date', 'drivers', 'driver_id', 'driver'));


   }

   public function BusesReports(Request $request, $bus_id = '', $park_id = ''){

        $start_date = date('Y-m-d 00:00:00', strtotime('-2weeks'));
        $end_date = date('Y-m-d 23:59:59');

        if(isset($request->daterange)){
            $arr = explode(' ', $request->daterange);
            $start_date = $arr[0].' 00:00:00';
            $end_date = $arr[1].' 23:59:59';
        }

        $buses = Bus::lists('bus_number', 'id')->toArray();

        $op_id = session('operator')->id;

        $op_id = session('operator')->id; //operator ID
        $source_park_id =  session('source_park')->id;


        $query = DailyTrip::with('driver', 'trip.sourcepark', 'trip.destpark', 'trip.busType', 'bus')
                        ->groupBy('bus_id')
                        ->where('created_at', '>=', $start_date)
                        ->where('created_at', '<=', $end_date)
                        ->where('operator_id', session('operator')->id)
                        ->where('source_park_id', $source_park_id)
                        ->orderBy('created_at', 'desc');


        if(!empty($bus_id)){
            $query = $query->where("bus_id", $bus_id);
            $bus = $buses[$bus_id];
        }
        $dailytrips = $query->get();

        $page_title = 'Buses Reports';

        $parks = Park::where('boardable', 1)->lists('name', 'id')->toArray();

        return view('reports.buses_reports', compact('dailytrips', 'page_title', 'start_date', 'end_date', 'buses', 'bus_id', 'bus', 'parks', 'park_id'));
   }

   public function ManifestReports(Request $request, $bus_id = '0', $park_id = ''){

        $start_date = date('Y-m-d 00:00:00', strtotime('-2weeks'));
        $end_date = date('Y-m-d 23:59:59');

        if(isset($request->daterange)){
            $arr = explode(' ', $request->daterange);
            $start_date = $arr[0].' 00:00:00';
            $end_date = $arr[1].' 23:59:59';
        }

        $buses = Bus::lists('bus_number', 'id')->toArray();

        $op_id = session('operator')->id;
        $source_park_id = session('source_park')->id;


        $op_id = session('operator')->id; //operator ID
        $source_park_id =  session('source_park')->id;

        $query = DailyTrip::with('driver', 'trip.sourcepark', 'trip.destpark', 'trip.busType', 'bus')
                        ->groupBy('bus_id')
                        ->where('status', 'TRANSITED')
                        ->where('created_at', '>=', $start_date)
                        ->where('created_at', '<=', $end_date)
                        ->where('operator_id', $op_id)
                        ->where('source_park_id', $source_park_id)
                        ->orderBy('created_at', 'desc');
        if(!empty($bus_id)){
            $query = $query->where("bus_id", $bus_id);
            $bus = $buses[$bus_id];
        }

        $dailytrips = $query->get();

        $page_title = 'Manifest Reports';

        $parks = Park::where('boardable', true)->where('operator_id', $op_id)->where('active', true)->lists('name', 'id');

        return view('reports.manifest_reports', compact('dailytrips', 'parks', 'park_id', 'page_title', 'start_date', 'end_date', 'buses', 'bus_id', 'bus'));


   }

   public function ParkReports(Request $request, $park_id = '0',$dest_id='0'){

        $start_date = date('Y-m-d', strtotime('-2weeks'));
        $end_date = date('Y-m-d') ;

        if(isset($request->daterange)){
            $arr = explode(' ', $request->daterange);
            $start_date = $arr[0];
            $end_date = $arr[1];
        }

        $page_title = 'Park Reports';

              $op_id = session('operator')->id;
        $parks = Park::where('id', Auth::user()->source_park_id)->lists('name', 'id');

        if(!empty($park_id)){
            $items = Park::where('boardable', true)->where('id', $park_id)->where('operator_id', $op_id)->where('active', true)->get();
            $destinations = Park::where('active', true)->lists('name', 'id');
        }
        else{
            $items = Park::where('id', Auth::user()->source_park_id)->get();
            $destinations = [];
        }


        return view('reports.park_reports', compact('items', 'page_title', 'start_date', 'end_date', 'destinations', 'parks', 'park_id','dest_id'));


   }

   public function PaymentReports(Request $request, $sel_p_id = '0', $park_id = '', $dest_id = ''){

        $start_date = date('Y-m-d', strtotime('-1week'));
        $end_date = date('Y-m-d');

        if(isset($request->daterange)){
            $arr = explode(' ', $request->daterange);
            $start_date = $arr[0];
            $end_date = $arr[1];
        }

        $op_id = session('operator')->id;

        $payment_methods = PaymentMethod::where('is_fulfilment', true)->lists('name', 'id')->toArray(); //add role condition
        $query = PaymentMethod::where('is_fulfilment', true);

        if(!empty($sel_p_id)){
            $query = $query->where("id", $sel_p_id);
            $payment_method = $payment_methods[$sel_p_id];
        }

        $items = $query->get();

        $page_title = 'Payment Reports';

        $parks = Park::where('boardable', true)->where('active', true)->lists('name','id');
        $destinations = Auth::user()->destinations()->lists('name', 'parks.id');
        return view('reports.payment_reports', compact('items', 'page_title', 'start_date', 'end_date', 'payment_methods', 'sel_p_id', 'payment_method', 'parks', 'destinations', 'park_id', 'dest_id'));


   }
   public function LuggagesReports(Request $request, $luggage_range='', $park_id=''){

        $start_date = date('Y-m-d 00:00:00', strtotime('-2weeks'));
        $end_date = date('Y-m-d 23:59:59');

        if(isset($request->daterange)){
            $arr = explode(' ', $request->daterange);
            $start_date = $arr[0].' 00:00:00';
            $end_date = $arr[1].' 23:59:59';
        }
      $excludePassenger = Discount::where('operator_id', session('operator')->id)->where('discount', 100)->first();

        $parks = Park::where('boardable', true)->where('active', true)->lists('name','id');


        $bookings = BusBooking::where('operator_id', session('operator')->id)
                            ->where('source_park_id', session('source_park')->id)
                            ->where('created_at', '>=', $start_date)
                            ->where('created_at', '<=', $end_date)
                            ->orderBy('created_at', 'desc');


        if($luggage_range != ''){
            $range = explode('-',$luggage_range);
            if(count($range) != 2)
                return redirect()->back();

            $bookings = $bookings->where('luggage_weight', '>=', $range[0])->where('luggage_weight', '<=', $range[1]);

        }
        $total_luggage_weight = $bookings->pluck('luggage_weight')->sum();
        $total_luggage_cost = $bookings->pluck('luggage_cost')->sum();
        $bookings = $bookings->get();
        $total_extra_luggage_weight = 0;
        $total_extra_luggage_cost = 0;
        foreach ($bookings as $key => $value) {

            $total_extra_luggage_weight += $value->extra_luggages->sum('luggage_weight');
            $total_extra_luggage_cost += $value->extra_luggages->sum('amount');
        }

        $page_title = 'Luggages Report';

        return view('reports.luggages_reports', compact('bookings', 'excludePassenger', 'page_title', 'start_date', 'end_date', 'total_luggage_weight', 'total_luggage_cost', 'parks', 'park_id', 'total_extra_luggage_weight', 'total_extra_luggage_cost', 'parks', 'park_id' ));
   }
   public function getCustDetails(Request $request){

        $phone = $request->ph;

        $booking = Booking::where('contact_phone', $phone)->orderBy('created_at', 'desc')->first();


        if(!empty($booking))
            return $booking->contact_name."|".$booking->contact_email."|".$booking->contact_address."|".
                    $booking->gender."|".$booking->next_of_kin."|".$booking->next_of_kin_phone;
        else
            return "";

  }

  public function getBusDetails(Request $request){

        $bus = Bus::with('bus_type')->find($request->bus_id);

        return response()->json($bus);
  }

   public function PhoneBookingCount(Request $request){

        $bookings = $bookings = BusBooking::with('trip.sourcepark', 'trip.destpark')
                        ->where('status',  'PAID')
                        ->where('contact_phone',  $request->phone)
                        ->where('operator_id', session('operator')->id)
                            ->where('source_park_id', session('source_park')->id)
                        ->where('created_at', '>=', $request->start_date.' 00:00:00')
                        ->where('created_at', '<=', $request->end_date. ' 23:59:59')
                        ->orderBy('created_at', 'desc')->get();

        $phone = $request->phone;


        return view('reports.contact_phone_breakdown', compact('bookings', 'phone'));

   }

   public function GetBusTrips(Request $request){

        extract($request->all());
         $query = DailyTrip::with('driver', 'trip.sourcepark', 'trip.destpark', 'trip.busType', 'trip.subTrips.destpark', 'bus')
                        ->where('operator_id', session('operator')->id)
                            ->where('source_park_id', session('source_park')->id)
                         ->where('created_at', '>=', $request->start_date.' 00:00:00')
                        ->where('created_at', '<=', $request->end_date.' 23:59:59')
                        ->orderBy('created_at', 'desc');

        if($request->bus_id != ''){
            $query->where('bus_id', $request->bus_id);
        }

        if($request->park_id != '' && $request->bus_id == '') {
             $trip_ids = Trip::where('source_park_id', session('source_park')->id)->lists('id')->toArray();
             $query = $query->whereIn('trip_id', $trip_ids);
        }
                $daily_trips = $query->get();
        return view('reports.bus_trips', compact('daily_trips', 'bus_id', 'bus_number', 'start_date', 'end_date'));

   }

   public function GetBusManifests(Request $request){

        extract($request->all());

               $excludePassenger = Discount::where('operator_id', Auth::user()->operator_id)->where('discount', 100)->first();

         $query = DailyTrip::with('driver', 'trip.sourcepark', 'trip.destpark', 'trip.busType', 'bus')
                        ->where('operator_id', session('operator')->id)
                        ->where('source_park_id', session('source_park')->id)
                        ->where('created_at', '>=', $request->start_date.' 00:00:00')
                        ->where('created_at', '<=', $request->end_date.' 23:59:59')
                        ->orderBy('created_at', 'desc');

            if ($request->bus_id != '') {
                $query = $query->where('bus_id', $request->bus_id);
            }
            if ($request->park_id != '') {
                $trip_ids = Trip::where('source_park_id', $park_id)->lists('id')->toArray();
                $query = $query->whereIn('trip_id', $trip_ids);
            }
             $daily_trips = $query->get();
        return view('reports.bus_manifests', compact('daily_trips', 'bus_id', 'bus_number', 'excludePassenger', 'start_date', 'end_date'));

   }

   public function GetDriversTrips(Request $request){

        extract($request->all());
        $end_date .= ' 23:59:59';

        $trip_ids = [];

        $park_name = '';

        if(!empty($request->park_id) && $request->park_id != 'ALL'){
            $park_name = Park::find($request->park_id)->name;
            $trip_ids = Trip::where('source_park_id', $request->park_id)->lists('id')->toArray();

            $daily_trips = DailyTrip::with('driver', 'trip.sourcepark', 'trip.destpark', 'trip.busType', 'bus', 'expenses')
                        //->where('status', 'TRANSITED')
                        ->whereIn('trip_id',  $trip_ids)
                        ->where('driver_id', $request->driver_id)
                        ->where('created_at', '>=', $request->start_date.' 00:00:00')
                        ->where('created_at', '<=', $request->end_date.' 23:59:59')
                        ->orderBy('created_at', 'desc')->get();

        }else {

        $daily_trips = DailyTrip::with('driver', 'trip.sourcepark', 'trip.destpark', 'trip.busType', 'bus', 'expenses')
                        //->where('status', 'TRANSITED')
                        ->where('driver_id', $request->driver_id)
                        ->where('created_at', '>=', $request->start_date.' 00:00:00')
                        ->where('created_at', '<=', $request->end_date.' 23:59:59')
                        ->orderBy('created_at', 'desc')->get();

        }
        return view('reports.driver_trips', compact('daily_trips', 'driver_id', 'driver_name', 'start_date', 'end_date'));


   }

   public function getTicketersBookings(Request $request){

        extract($request->all());

        $trip_ids = [];

        $park_name = $dest_name = 'ALL';

        if(!empty($request->park_id)){
            $trip_ids = SubTrip::where('source_park_id', $request->park_id)->lists('id')->toArray();

            $park_name = Park::find($request->park_id)->name;

        }

        if(!empty($request->dest_id)){
            $d_trip_ids = SubTrip::where('dest_park_id', $request->dest_id)->lists('id')->toArray();

            if(!empty($trip_ids))
                $trip_ids = array_intersect($trip_ids, $d_trip_ids);
            else
                $trip_ids = $d_trip_ids;

            $dest_name = Park::find($dest_id)->name;
        }
        $pendings = BusBooking::where('user_id', $user_id)
                        ->where('status', 'PENDING')
                        ->where('operator_id', session('operator')->id)
                        ->where('source_park_id', session('source_park')->id)
                        ->where('created_at', '>=', $request->start_date.' 00:00:00')
                        ->where('created_at', '<=', $request->end_date.' 23:59:59')
                        ->orderBy('created_at', 'desc');

        if(!empty($trip_ids))
            $pendings = $pendings->whereIn('trip_id',  $trip_ids);

        $pendings = $pendings->get();

        $paids = BusBooking::where('user_id', $user_id)
                        ->where('status', 'PAID')
                        ->where('operator_id', session('operator')->id)
                        ->where('source_park_id', session('source_park')->id)
                        ->where('created_at', '>=', $request->start_date.' 00:00:00')
                        ->where('created_at', '<=', $request->end_date.' 23:59:59')
                        ->orderBy('created_at', 'desc');

        if(!empty($trip_ids))
            $paids = $paids->whereIn('trip_id',  $trip_ids);

        $paids = $paids->get();

        $cancels = BusBooking::where('user_id', $user_id)
                        ->where('status', 'CANCELLED')
                        ->where('operator_id', session('operator')->id)
                        ->where('source_park_id', session('source_park')->id)
                         ->where('created_at', '>=', $request->start_date.' 00:00:00')
                        ->where('created_at', '<=', $request->end_date.' 23:59:59')
                        ->orderBy('created_at', 'desc');

        if(!empty($trip_ids))
            $cancels = $cancels->whereIn('trip_id',  $trip_ids);

        $cancels = $cancels->get();

        return view('reports.ticketer_bookings', compact('pendings', 'paids', 'cancels', 'user_id', 'user_name', 'start_date', 'end_date', 'park_name', 'dest_name'));

   }

   public function getPaymentBookings(Request $request){

        extract($request->all());

        $trip_ids = [];

        $park_name = $dest_name = 'ALL';

        if(!empty($park_id)){
            $trip_ids = SubTrip::where('source_park_id', $park_id)->lists('id')->toArray();

            $park_name = Park::find($park_id)->name;

        }
        if(!empty($dest_id)){
            $d_trip_ids = SubTrip::where('dest_park_id', $dest_id)->lists('id')->toArray();

            if(!empty($trip_ids))
                $trip_ids = array_intersect($trip_ids, $d_trip_ids);
            else
                $trip_ids = $d_trip_ids;

            $dest_name = Park::find($dest_id)->name;
        }

        $total_pass_count = $bookings = BusBooking::
                        where('status',  'PAID')
                        ->where('operator_id', session('operator')->id)
                        ->where('source_park_id', session('source_park')->id)
                        ->where('created_at', '>=', $request->start_date.' 00:00:00')
                        ->where('created_at', '<=', $request->end_date. ' 23:59:59');

        if(!empty($trip_ids))
            $total_pass_count = $total_pass_count->whereIn('trip_id',  $trip_ids);

        $b_ids = $total_pass_count->lists('id')->toArray();

        $total_pass_count = Passenger::whereIn('booking_id', $b_ids)->count() + count($b_ids);
        $pass_b = $bookings = BusBooking::
                        where('status',  'PAID')
                        ->where('payment_method_id',  $request->id)
                        ->where('operator_id', session('operator')->id)
                        ->where('source_park_id', session('source_park')->id)
                        ->where('created_at', '>=', $request->start_date.' 00:00:00')
                        ->where('created_at', '<=', $request->end_date. ' 23:59:59');

        if(!empty($trip_ids))
            $pass_b = $pass_b->whereIn('trip_id',  $trip_ids);

        $b_ids = $pass_b->lists('id')->toArray();
        // dump(count($b_ids));
        // dump(Passenger::whereIn('booking_id', $b_ids)->count());
        $pass_count = Passenger::whereIn('booking_id', $b_ids)->count() + count($b_ids);

        $pass_count_b = $pass_b->sum('passenger_count');

        $pass_amount = $pass_b->sum('final_cost');
        $bookings = $bookings = BusBooking::with('trip.sourcepark', 'trip.destpark', 'paymentmthd')
                        ->where('status',  'PAID')
                        ->where('payment_method_id',  $request->id)
                        ->where('operator_id', session('operator')->id)
                        ->where('source_park_id', session('source_park')->id)
                        ->where('created_at', '>=', $request->start_date. ' 00:00:00')
                        ->where('created_at', '<=', $request->end_date. ' 23:59:59');


        if(!empty($trip_ids))
            $bookings = $bookings->whereIn('trip_id',  $trip_ids);

        $bookings = $bookings->orderBy('created_at', 'desc')->get();




        return view('reports.payment_bookings', compact('bookings', 'name', 'total_pass_count', 'pass_count', 'pass_count_b', 'start_date', 'end_date', 'park_name', 'dest_name', 'pass_amount'));

   }


   public function getParkBookings(Request $request){

        extract($request->all());
        $trip_ids = [];

        $park_name = $dest_name = 'ALL';

        $free_age_range = Discount::where('operator_id', Auth::user()->operator_id)->where('discount', 100)->first();

        if(!empty($park_id)){
            $trip_ids = Trip::where('source_park_id', $park_id)->lists('id')->toArray();

            $park_name = Park::find($park_id)->name;

        }

        if(!empty($dest_id)){

            $d_trip_ids = SubTrip::where('source_park_id', $park_id)
                                ->where('dest_park_id', $dest_id)
                                ->where('operator_id', session('operator')->id)
                                ->lists('parent_trip_id')
                                ->toArray();
            if(!empty($trip_ids))
                $trip_ids = array_intersect($trip_ids, $d_trip_ids);
            else
                $trip_ids = $d_trip_ids;

            $dest_name = Park::find($dest_id)->name;
        }
         $daily_trips = DailyTrip::with(['driver', 'trip.sourcepark', 'trip.destpark', 'trip.busType', 'trip.subTrips.destpark', 'bus', 'bus', 'bookings'=>function($query){ $query->where('status', 'PAID'); }])
                        ->whereIn('trip_id',  $trip_ids)
                        ->where('operator_id', session('operator')->id)
                        ->where('source_park_id', session('source_park')->id)
                        ->where('created_at', '>=', $request->start_date. ' 00:00:00')
                        ->where('created_at', '<=', $request->end_date. ' 23:59:59')
                        ->orderBy('created_at', 'desc')->get();

        $dt2 = DailyTrip::with('driver', 'trip.sourcepark', 'trip.destpark', 'trip.busType', 'trip.subTrips.destpark', 'bus', 'bus')
                        ->where('status','TRANSITED')
                        ->where('operator_id', session('operator')->id)
                        ->where('source_park_id', session('source_park')->id)
                        ->whereIn('trip_id',  $trip_ids)
                        ->where('created_at', '>=', $request->start_date. ' 00:00:00')
                        ->where('created_at', '<=', $request->end_date. ' 23:59:59')
                        ->orderBy('created_at', 'desc')->get();

        $allpassengers = [];

        foreach ($daily_trips as $key => $dailytrip) {
                $dailytrip->bookings->count();
                $allpassengers['adults'] = $dailytrip->bookings->count();
                $allpassengers['infants'] = 0;
                //dd($allpassengers);

                foreach($dailytrip->bookings as $booking) {
                    foreach($booking->passengers as $passenger) {

                        if ($free_age_range != null) {
                            if($passenger->age >= $free_age_range->age_one && $passenger->age <= $free_age_range->age_two){

                                //dump($passenger);
                                $allpassengers['infants'] = $allpassengers['infants'] + 1;

                            }else{

                                //dump('not');
                                $allpassengers['adults'] = $allpassengers['adults'] + 1;

                            }
                            //dump($passengers->age . '|' .$free_age_range->age_one . '|'  .$free_age_range->age_two);
                        }else{
                            $allpassengers['adults'] = $allpassengers['adults'] + 1;
                        }

                    }
                }
        }

        return view('reports.park_details', compact('daily_trips', 'name', 'total_pass_count', 'pass_count', 'start_date', 'end_date', 'park_name', 'dest_name', 'park_id','dt2', 'allpassengers'));

   }

   public function Transload(Request $request){

        if ($request->isMethod('post')) {

            // dump($request->all());

            $booking = BusBooking::with('daily_trip')->find($request->booking_id);


            //checking if same seat is sel...
            $selSeats = array();
            for ($i=1; $i <= $booking->passenger_count; $i++) {
                $s = intval($request->input('seat'.$i)) + 1;

                if(in_array($s, $selSeats)){

                    return "You cannot select the same seat '<b>".$s."</b>' for more than one passenger.";
                }else
                    $selSeats[] = $s;
            }

            //updating seats...
            $index = 1;
            foreach ($booking->seats()->get() as $seat) {
                $seat->seat_no = $request->input('seat'.$index++);
                $seat->save();
            }

            $fmr_dt = $booking->daily_trip;
            $fmr_dt->total_fare = $fmr_dt->total_fare - $booking->final_cost;
            $fmr_dt->booked_seats = $fmr_dt->booked_seats - $booking->passenger_count;
            $fmr_dt->save();


            $new_dt = DailyTrip::find($request->dt_id);
            $new_dt->total_fare = $new_dt->total_fare + $booking->final_cost;
            $new_dt->booked_seats = $new_dt->booked_seats + $booking->passenger_count;
            $new_dt->save();

            $booking->daily_trip_id = $new_dt->id;
            $booking->final_cost = $request->final_cost;
            $booking->trip_id = $request->strip_id;
            $booking->save();

            return 'success';

        }

        $booking_id = $request->booking_id;
        $booking = Booking::with('daily_trip')->find($booking_id);
        $numPass = $booking->passenger_count;
        $fare = $booking->final_cost;

        $seats = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15];

        $bus = $booking->daily_trip->bus_id;

        $dts = DailyTrip::with('bus')->where('trip_date', $booking->date)->where('bus_id', '<>', $booking->daily_trip->bus_id)->get();

        $buses = [''=>'-Choose-'];
        foreach ($dts as $dt) {
            $buses[$dt->id]  = $dt->bus->bus_number;
        }



        return view('bookings.transload', compact('buses', 'booking_id', 'numPass', 'seats', 'fare'));

   }

   public function TransloadBusDetails(Request $request){

        extract($request->all());

        $dt = DailyTrip::with('trip.subTrips.destpark')->find($dt_id);

        $dests = array();
        foreach ($dt->trip->subTrips as $st) {
            $dests[$st->id] = $st->destpark->name;
        }

        $booked_seats = array();

        if(!empty($dt)){
            $bookings = $dt->bookings()->get();
            if(!empty($bookings)){
                foreach ($bookings as $b) {
                    foreach ($b->seats()->whereNotNull('booking_id')->get() as $seat) {
                        $booked_seats[] = $seat->seat_no;

                    }
                }


            }
        }

        $seats = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15];
        $avail_seats = array_diff($seats, $booked_seats);

        return view('bookings.transloadBusDetails', compact('dests', 'avail_seats', 'numPass', 'fare'));

   }

  public function getUserPark(Request $request)
  {

   $user = $request->user;
   

  }


   public function AddExtraLuggage(Request $request){

        

        $e = ExtraLuggage::Create([
                'booking_id'=>$request->booking_id,
                'luggage_weight'=>$request->luggage_weight,
                'amount'=>$request->amount
            ]);


        return redirect()->action('BookingsController@show', [$request->booking_id]);


    }





}

