<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Models\Operator;
use App\Models\Park;
use App\Models\Setting;
use View;
use Auth;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct()
    {
         if(!session('operator')  ){
        // Fetch the Site Settings object
            if(Auth::check()){

                $this->operator = Operator::where('id', Auth::user()->operator_id)->first();
                $this->settings = Setting::where('operator_id', Auth::user()->operator_id)->first();
                $this->source_park = Park::find(Auth::user()->source_park_id);
                session()->put('operator', $this->operator);
                session()->put('settings', $this->settings);

                session()->put('source_park', $this->source_park);

            }
            else{
                $this->operator = null;
            }


            View::share('operator', $this->operator);
         }
    }




    public function send_sms($phone, $msg)
    {

       //settings
        $cid ="";
        $user = "buscom_sms";
        $senderArray = explode(" ", session('operator')->name);
        $senderId = $senderArray[0];
        $to = $phone;
        $pass ="bus.com.ng";
        $ch = curl_init();
       $postdata = 'user='.$user.'&pass='.$pass.'&from='.$senderId.'&to='.$to.'&msg='.$msg; //initialize the request variable
     // echo $postdata;

        $url = 'http://cloud.nuobjects.com/api/send/'; //this is the url of the gateway's interface
        $ch = curl_init(); //initialize curl handle
        curl_setopt($ch, CURLOPT_URL, $url); //set the url
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1); //return as a variable
        curl_setopt($ch, CURLOPT_POST, 1); //set POST method
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata); //set the POST variables
        $response = curl_exec($ch); // grab URL and pass it to the browser. Run the whole process and return the response
        curl_close($ch); //close the curl handle

        $strHeader = get_headers($url)[0];

        $statusCode = substr($strHeader, 9, 3 );

       return $statusCode;

    }

}
