<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\PaymentMethod;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;

class PaymentMethodsController extends Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $paymentmethods = PaymentMethod::paginate(15);

        $page_title = 'paymentmethods';

        return view('payment-methods.index', compact('paymentmethods', 'page_title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $page_title = 'Add paymentmethod';

        return view('payment-methods.create', compact('page_title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, ['name' => 'required', ]);

        PaymentMethod::create($request->all());

        Session::flash('flash_message', 'PaymentMethod added!');

        return redirect('paymentmethods');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function show($id)
    {
        $paymentmethod = PaymentMethod::findOrFail($id);

        $page_title = 'View paymentmethod';
        return view('payment-methods.show', compact('paymentmethod', 'page_title'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $paymentmethod = PaymentMethod::findOrFail($id);

        $page_title = 'Edit paymentmethod';
        return view('payment-methods.edit', compact('paymentmethod', 'page_title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        $this->validate($request, ['name' => 'required', ]);

        $paymentmethod = PaymentMethod::findOrFail($id);
        $paymentmethod->update($request->all());

        Session::flash('flash_message', 'PaymentMethod updated!');

        return redirect('paymentmethods');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        PaymentMethod::destroy($id);

        Session::flash('flash_message', 'PaymentMethod deleted!');

        return redirect('paymentmethods');
    }

}
