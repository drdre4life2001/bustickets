<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Device;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;

class DevicesController extends Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {   

       
        $devices = Device::paginate(100);

        $page_title = 'devices';

        return view('devices.index', compact('devices', 'page_title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $page_title = 'Add device';

        return view('devices.create', compact('page_title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        

        $request['operator_id'] = $this->operator->id;
        Device::create($request->all());

        Session::flash('flash_message', 'Device added!');

        return redirect('devices');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function show($id)
    {
        $device = Device::findOrFail($id);

        $page_title = 'View device';
        return view('devices.show', compact('device', 'page_title'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $device = Device::findOrFail($id);

        $page_title = 'Edit device';
        return view('devices.edit', compact('device', 'page_title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        
        $device = Device::findOrFail($id);
        $device->update($request->all());

        Session::flash('flash_message', 'Device updated!');

        return redirect('devices');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        Device::destroy($id);

        Session::flash('flash_message', 'Device deleted!');

        return redirect('devices');
    }

}
