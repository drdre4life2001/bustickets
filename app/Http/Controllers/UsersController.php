<?php
namespace App\Http\Controllers;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\User;
use App\Models\Role;
use App\Models\Park;
use App\Models\Trip;
use App\Models\Bus;
use App\Models\Driver;
use App\Models\Operator;
use App\Models\BusBooking;
use App\Models\Booking;
use App\Models\CharterBooking;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;
use Auth;
use DB;

class UsersController extends Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $users = User::where('operator_id', session('operator')->id)->where('source_park_id', session('source_park')->id)->where('exposed', true)->paginate(100);
        
        $page_title = 'users';

        return view('users.index', compact('users', 'page_title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //dd(Auth::user()->role_id);
        $page_title = 'Add user';

        //BUS ADMIN USER
        if(Auth::user()->role_id == 1) {
            $roles = [null => ''] + Role::lists('name', 'id')->toArray();
        }elseif(Auth::user()->role_id == 3) {
            //OPERATOR PARK ADMIN
            $roles = [null => ''] + Role::where('id', '!=', 1)->where('id', '!=', 4)->lists('name', 'id')->toArray();
        }elseif(Auth::user()->role_id == 4){
            //OPERATOR SUPER ADMIN
            $roles = [null => ''] + Role::where('id', '!=', 1)->lists('name', 'id')->toArray();
        }
        $operators = [null => '-Choose-'] + Operator::lists('name', 'id')->toArray();
       
        $parks = Park::lists('name', 'id')->toArray();


        return view('users.create', compact('page_title', 'roles', 'parks', 'operators'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
   public function store(Request $request)
    {
        $this->validate($request, ['last_name' => 'required', 'first_name' => 'required', 'email' => 'required', 'phone' => 'required', 'role_id' => 'required', ]);
        //dd($request->all());
        $request['password'] = bcrypt($request->password);
        $request['source_park_id'] = $request->parks;

        $user = User::where('email',$request->email)->first();
    if (is_null($user)) {

        $user = User::create($request->all());

        //$user->parks()->sync($request->parks);

        Session::flash('flash_message', 'User added!');

        return redirect('users');
    }
    else
    {
         Session::flash('flash_message', 'User exists!');
          return redirect()->back()->withInput();
    }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function show($id)
    {
        $user = User::findOrFail($id);

        $page_title = 'View user';
        return view('users.show', compact('user', 'page_title'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function edit($id)
    {
        //BUS ADMIN USER
        if(Auth::user()->role_id == 1) {
            $roles = [null => ''] + Role::lists('name', 'id')->toArray();
        }elseif(Auth::user()->role_id == 3) {
            //OPERATOR PARK ADMIN
            $roles = [null => ''] + Role::where('id', '!=', 1)->where('id', '!=', 4)->lists('name', 'id')->toArray();
        }elseif(Auth::user()->role_id == 4){
            //OPERATOR SUPER ADMIN
            $roles = [null => ''] + Role::where('id', '!=', 1)->lists('name', 'id')->toArray();
        }

        $parks = Park::lists('name', 'id')->toArray();



        $user = User::findOrFail($id);
        //$sel_parks = $user->parks()->pluck('parks.id')->toArray();
        $sel_dests = $user->destinations()->pluck('parks.id')->toArray();
        $operators = [null => ''] + Operator::lists('name', 'id')->toArray();
        //dd($sel_parks);

        // dd();

        // dd($sel_parks);


        $page_title = 'Edit user';
        return view('users.edit', compact('user', 'page_title', 'roles', 'parks', 'sel_dests','operators'));
    }



     public function ChangePassword(Request $request)
     {

        $id = Auth::user()->id;

        $user = User::findOrFail($id);

        $message = "";
        $cancel = false;

        if ($request->isMethod('post')){

            // dump($request->all());
            // dump($user->password);
            // dump(bcrypt($request->password));

            // checking former password ...
            // if($user->password != bcrypt($request->password)) {

            //     $message .= "<br/>Incorrect former password";
            //     $cancel = true;
            // }

            if($request->new_password != $request->password_again){
                $message .= "<br/>Incorrect former password";
                $cancel = true;
            }


            if(!$cancel){
                $user->update(['password' => bcrypt($request->new_password)]);
                $message = "Password changed sucessfully! <a href='".url('dashboard')."' class='btn btn-primary'>Continue</a>";
            }






        }


        $page_title = 'Change Password';
        return view('users.change_password', compact('user', 'page_title', 'message' ));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        //dd($request->all());
        $this->validate($request, ['last_name' => 'required', 'first_name' => 'required', 'email' => 'required', 'phone' => 'required', 'role_id' => 'required', ]);

        $request['source_park_id'] = $request->parks;

        if(isset($request->password) && !empty($request->password))
            $request['password'] = bcrypt($request->password);

        $user = User::findOrFail($id);
        $user->update($request->all());

        //$user->parks()->sync($request->parks);
        $user->destinations()->sync($request->destinations);

        Session::flash('flash_message', 'User updated!');

        return redirect('users');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        User::destroy($id);

        Session::flash('flash_message', 'User deleted!');

        return redirect('users');
    }

       public function dashboard( $daterange = "1 month"){

        $end_date = date('Y-m-d H:i:s');
        $start_date = date('Y-m-1');

        //dd(session('operator')->id);

        if(Auth::user()->role_id != 1) {

           $totalCount = $bookings = BusBooking::where('operator_id', session('operator')->id)
                        ->where('source_park_id', session('source_park')->id)
                        ->where('created_at', '>=', $start_date.' 00:00:00')
                        ->where('created_at', '<=', $end_date)
                        ->count();

        $paidCount = $bookings = BusBooking::where('operator_id', session('operator')->id)
                        ->where('source_park_id', session('source_park')->id)
                        ->where('created_at', '>=', $start_date.' 00:00:00')
                        ->where('created_at', '<=', $end_date)
                        ->where('status', 'PAID')
                        ->count();
        $paidSum = $bookings = BusBooking::where('operator_id', session('operator')->id)
                        ->where('source_park_id', session('source_park')->id)
                        ->where('created_at', '>=', $start_date.' 00:00:00')
                        ->where('created_at', '<=', $end_date)
                        ->where('status', 'PAID')
                        ->sum('final_cost');
        $charteredSum = $bookings = CharterBooking::where('operator_id', session('operator')->id)
                        ->where('source_park_id', session('source_park')->id)
                        ->where('created_at', '>=', $start_date.' 00:00:00')
                        ->where('created_at', '<=', $end_date)
                        ->where('status', 'PAID')
                        ->sum('agreed_price');

        // reflection of chartered sum in total
        $paidSum+=$charteredSum;

        $pendCount = $bookings = BusBooking::where('operator_id', session('operator')->id)
                        ->where('source_park_id', session('source_park')->id)
                        ->where('created_at', '>=', $start_date.' 00:00:00')
                        ->where('created_at', '<=', $end_date)
                        ->where('status', 'PENDING')
                       ->count();


        $busCount = $bookings = BusBooking::where('operator_id', session('operator')->id)
                ->where('source_park_id', session('source_park')->id)
                ->where('created_at', '>=', $start_date.' 00:00:00')
                ->where('created_at', '<=', $end_date)
                ->where('booked_by', 'PENDING')
                ->count();



            $pendSum = $bookings = BusBooking::where('operator_id', session('operator')->id)
                        ->where('source_park_id', session('source_park')->id)
                        ->where('created_at', '>=', $start_date.' 00:00:00')
                        ->where('created_at', '<=', $end_date)
                        ->where('status', 'PENDING')
                        ->sum('final_cost');

        $op_id = session('operator')->id;
         //dd($op_id);

        $totalPaidPassengers = BusBooking::where('operator_id', session('operator')->id)
                        ->where('source_park_id', session('source_park')->id)
                        ->where('created_at', '>=', $start_date.' 00:00:00')
                        ->where('created_at', '<=', $end_date)
                        ->where('status', 'PAID')
                        ->sum('passenger_count');

        $bookings = BusBooking::where('operator_id', session('operator')->id)
        ->where('source_park_id', session('source_park')->id)
        ->with('trip.sourcepark', 'trip.destpark')->orderBy('created_at', 'desc')->orderBy('created_at', 'desc')->paginate(5)->toArray();
        $bookings = ($bookings['data']);

        // chartered bookings
        $cBookings = CharterBooking::where('operator_id', session('operator')->id)
        ->where('source_park_id', session('source_park')->id)
        ->orderBy('created_at', 'desc')->take(5)->get();
        $cBookingsNo = count($cBookings);

        $trips = Trip::where('operator_id', session('operator')->id)->with('sourcepark', 'destpark')
        ->where('source_park_id', session('source_park')->id)
        ->orderBy('created_at', 'desc')->orderBy('created_at', 'desc')->paginate(5)->toArray();
        $trips = ($trips['data']);

        // get the role of the user
        $userRole = Auth::user()->role;
        $roleNo = $userRole->id;

        /// get data to be displayed in dashboard

       // trips created by particular operator/all. (presently using all)
       //$tripsCreated = Trip::where('operator_id','=',$op_id); (trips created by particular operator)
        $tripsCreated = Trip::where('operator_id', session('operator')->id)->where('source_park_id', session('source_park')->id)->get();
        $tripsCreatedNo = count($tripsCreated);

        // no buses owned by particular operator/all. (presently using all)
        //$busesOwned = Bus::where('operator_id','=',$op_id); (buses created by particular operator)
       $busesOwned = Bus::where('operator_id', session('operator')->id)->where('source_park_id', session('source_park')->id)->get();
       $busesOwnedNo = count($busesOwned);

        // number of drivers owned by bus operator(),
       $allDrivers = Driver::where('operator_id', session('operator')->id)->where('source_park_id', session('source_park')->id)->get();
       $allDriversNo = count($allDrivers);

         // number of destinations created by bus operator (particular operatorr/bus company)
       $dests = DB::table('trips')
            ->select('dest_park_id')
            ->where('operator_id', session('operator')->id)
            ->groupBy('dest_park_id')
            ->get();
       $destNo = count($dests);

       }else{
        $totalCount = $bookings = BusBooking::where('created_at', '>=', $start_date.' 00:00:00')
                        ->where('created_at', '<=', $end_date)
                        ->count();

        $paidCount = $bookings = BusBooking::where('created_at', '>=', $start_date.' 00:00:00')
                        ->where('created_at', '<=', $end_date)
                        ->where('status', 'PAID')
                        ->count();
        $paidSum = $bookings = BusBooking::where('created_at', '>=', $start_date.' 00:00:00')
                        ->where('created_at', '<=', $end_date)
                        ->where('status', 'PAID')
                        ->sum('final_cost');
        $charteredSum = $bookings = CharterBooking::where('created_at', '>=', $start_date.' 00:00:00')
                        ->where('created_at', '<=', $end_date)
                        ->where('status', 'PAID')
                        ->sum('agreed_price');

        // reflection of chartered sum in total
        $paidSum+=$charteredSum;

        $pendCount = $bookings = BusBooking::where('created_at', '>=', $start_date.' 00:00:00')
                        ->where('created_at', '<=', $end_date)
                        ->where('status', 'PENDING')
                       ->count();

            $busCount = $bookings = BusBooking::where('created_at', '>=', $start_date.' 00:00:00')
                ->where('created_at', '<=', $end_date)
                ->where('booked_by', 'bus.com.ng')
                ->count();
        //dd($pendCount);

        $pendSum = $bookings = BusBooking::where('created_at', '>=', $start_date.' 00:00:00')
                        ->where('created_at', '<=', $end_date)
                        ->where('status', 'PENDING')
                        ->sum('final_cost');

        //$op_id = session('operator')->id;
         //dd($op_id);

        $totalPaidPassengers = BusBooking::where('created_at', '>=', $start_date.' 00:00:00')
                        ->where('created_at', '<=', $end_date)
                        ->where('status', 'PAID')
                        ->sum('passenger_count');

        $bookings = BusBooking::with('trip.sourcepark', 'trip.destpark')->orderBy('created_at', 'desc')->orderBy('created_at', 'desc')->paginate(5)->toArray();
        $bookings = ($bookings['data']);

        // chartered bookings
        $cBookings = CharterBooking::orderBy('created_at', 'desc')->take(5)->get();
        $cBookingsNo = count($cBookings);

        $trips = Trip::with('sourcepark', 'destpark')->orderBy('created_at', 'desc')->orderBy('created_at', 'desc')->paginate(5)->toArray();
        $trips = ($trips['data']);

        // get the role of the user
        $userRole = Auth::user()->role;
        $roleNo = $userRole->id;

        /// get data to be displayed in dashboard

       // trips created by particular operator/all. (presently using all)
       //$tripsCreated = Trip::where('operator_id','=',$op_id); (trips created by particular operator)
        $tripsCreated = Trip::all();
        $tripsCreatedNo = count($tripsCreated);

        // no buses owned by particular operator/all. (presently using all)
        //$busesOwned = Bus::where('operator_id','=',$op_id); (buses created by particular operator)
       $busesOwned = Bus::all();
       $busesOwnedNo = count($busesOwned);

        // number of drivers owned by bus operator(),
       $allDrivers = Driver::all();
       $allDriversNo = count($allDrivers);

         // number of destinations created by bus operator (particular operatorr/bus company)
       $dests = DB::table('trips')
            ->select('dest_park_id')
            ->groupBy('dest_park_id')
            ->get();
       $destNo = count($dests);

       }
        //dd($bookings);
        if ($userRole->name == '')
        {
           return view('users.operatordashboard', compact('bookings', 'start_date', 'end_date', 'paidCount',
                  'paidSum', 'pendCount', 'pendSum', 'totalCount','cBookings','cBookingsNo', 'busCount', 'busesOwnedNo', 'allDriversNo', 'tripsCreatedNo', 'destNo', 'trips', 'charteredSum', 'totalPaidPassengers'));

        }else if($userRole->name == 1 || $userRole->id == 3 || $userRole->id == 3){

            return view('users.superdashboard', compact('bookings', 'start_date', 'end_date', 'busCount', 'busesOwnedNo', 'tripsCreatedNo', 'allDriversNo','destNo', 'pendSum', 'totalCount', 'trips', 'paidCount',
                  'paidSum', 'pendCount','cBookings','cBookingsNo', 'charteredSum', 'totalPaidPassengers'));

        }

        return view('users.dashboard', compact('bookings', 'start_date', 'end_date', 'busCount','paidCount',
                  'paidSum', 'pendCount', 'pendSum', 'totalCount','cBookings','cBookingsNo', 'charteredSum', 'totalPaidPassengers'));
    }


}
