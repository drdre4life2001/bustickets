<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SubTrip extends Model
{

    protected $fillable = ['name', 'source_park_id', 'dest_park_id','state_id', 'trip_transit',
    'round_trip_fare', 'state_id', 'operator_id', 'round_trip_status', 'fare', 'parent_trip_id', 'updated_at', 'created_at',
    'is_parent_trip', 'is_intl_trip', 'virgin_passport_fare', 'no_passport_fare', 'round_trip_virgin_passport_fare', 'round_trip_no_passport_fare'];

    // //Add extra attribute
    // protected $attributes = ['human_name'];
    // // public $nname = '';

    // //Make it available in the json response
    // protected $appends = ['human_name'];

    // //implement the attribute
    // public function getHumanNameAttribute()
    // {
    //     $arr = explode('_', $this->name);
    //     return $arr[1]." -> ".$arr[2]. ' [N'.$arr[3].']';

    // }

    public function operator(){
        return $this->belongsTo('App\Models\Operator');

    }

    public function parenttrip(){
        return $this->belongsTo('App\Models\Trip', 'parent_trip_id');
    }

    public function sourcepark(){
        return $this->belongsTo('App\Models\Park', 'source_park_id');
    }

    public function destpark(){
        return $this->belongsTo('App\Models\Park', 'dest_park_id');
    }
     public function booking(){
        return $this->hasMany('App\Models\Booking');
    }

    public function bus(){
        return $this->belongsTo("App\Models\Bus");
    }

    public function driver(){
        return $this->belongsTo("App\Models\Driver");
    }


    public function default_bus(){

        return $this->hasOne('App\Models\Bus', 'default_trip_id');

    }





}
