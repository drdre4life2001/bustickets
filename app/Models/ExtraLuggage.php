<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExtraLuggage extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'extra_luggages';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['booking_id', 'luggage_weight', 'amount'];

    

}