<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ParkStop extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'park_stops';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'park_id', 'is_active'];

}
