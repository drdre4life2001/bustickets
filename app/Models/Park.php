<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Park extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'parks';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'operator_id', 'state_id', 'address', 'active', 'boardable'];

    public function state(){
        return $this->belongsTo('App\Models\State', 'state_id');
    }

    public function stops(){

        return $this->hasMany('App\Models\ParkStop');
    }

}