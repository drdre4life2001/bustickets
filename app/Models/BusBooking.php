<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BusBooking extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'bookings';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */

    protected $fillable = ['trip_id', 
    'operator_id', 'source_park_id', 'date', 'passenger_count', 'nationality','occupation', 'passport_no','passport_expiry_date',
    'passport_date_issued','passport_place_issued', 'unit_cost', 'final_cost', 'status', 'paid_date', 'booking_code', 'contact_phone', 'contact_name','contact_email', 'next_of_kin', 'next_of_kin_phone', 'source','trip_type', 'user_id', 'contact_address', 'payment_method_id', 'gender', 'is_round_trip', 'departure_time','receipt_no','return_date', 
    'luggage_weight','luggage_cost',
    'passport_type', 
    'is_flexi', 
    'seat_no',
    'initial_source_park_id', 
    'transfer_type',];

    
    public function trip(){
        return $this->belongsTo('App\Models\SubTrip', 'trip_id');
    }
    
    public function paymentmthd(){
        return $this->belongsTo('App\Models\PaymentMethod', 'payment_method_id');
    }

    public function paymentmethod(){
        return $this->belongsTo('App\Models\PaymentMethod', 'payment_method_id');
    }

    public function transactions(){
        return $this->hasMany('App\Models\Transaction');
    }

    public function passengers(){
        return $this->hasMany('App\Models\Passenger', 'booking_id');
    }

    public function seats(){
        return $this->hasMany('App\Models\Seat', 'booking_id');
    }

    public function daily_trip(){
        return $this->belongsTo('App\Models\DailyTrip');
    }

    public function ticketer(){
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function extra_luggages(){
        return $this->hasMany('App\Models\ExtraLuggage', 'booking_id');
    }
}
