<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'settings';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['key', 'value', 'description', 'exposed', 'operator_id', 'luggage_limit', 'intl_cost_per_kg', 'local_cost_per_kg', 'daily_trip_expense_headers', 'discount_active', 'track_luggage', 'track_infant', 'infant_discount', 'infant_age_range'];

}
