<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Driver extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'drivers';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'operator_id', 'source_park_id', 'phone', 'phone_2', 'address', 'default_park_id'];

    public function default_bus(){

        return $this->hasOne('App\Models\Bus', 'default_driver_id');

    }

    
    public function default_park(){

        return $this->belongsTo('App\Models\Park', 'default_park_id');

    }

}
