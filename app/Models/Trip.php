<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;



class Trip extends Model
{

    protected $fillable = ['name', 'source_park_id', 'dest_park_id', 'departure_time', 'fare', 'bus_id', 'driver_id', 'operator_id', 'trip_transit', 'ac', 'tv', 'insurance', 'passport', 'security', 'updated_at', 'created_at'];

    // //Add extra attribute
    // protected $attributes = ['human_name'];
    // // public $nname = '';

    // //Make it available in the json response
    // protected $appends = ['human_name'];

    // //implement the attribute
    // public function getHumanNameAttribute()
    // {
    //     $arr = explode('_', $this->name);
    //     return $arr[1]." -> ".$arr[2]. ' [N'.$arr[3].']';

    // }
    public function operator(){
        return $this->belongsTo('App\Models\Operator');

    }

    public function busType(){

        return $this->belongsTo('App\Models\BusType');

    }

    public function sourcepark(){
        return $this->belongsTo('App\Models\Park', 'source_park_id');
    }

    public function destpark(){
        return $this->belongsTo('App\Models\Park', 'dest_park_id');
    }
     public function booking(){
        return $this->hasMany('App\Models\Booking');
    }

    public function bus(){
        return $this->belongsTo("App\Models\Bus");
    }

    public function driver(){
        return $this->belongsTo("App\Models\Driver");
    }


    public function default_bus(){

        return $this->hasOne('App\Models\Bus', 'default_trip_id');

    }

    public function subTrips(){
        return $this->hasMany('App\Models\SubTrip', 'parent_trip_id');
    }

    public function dailyTrips(){
        return $this->hasMany('App\Models\DailyTrip', 'trip_id');
    }





}
