<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Incident extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'incidents';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['incident_type_id', 'operator_id', 'details', 'park_id', 'bus_id', 'expense_amount'];

    public function incidentType(){
        return $this->belongsTo('App\Models\IncidentType');
    }

    public function park(){
        return $this->belongsTo('App\Models\Park');
    }

    public function bus(){
        return $this->belongsTo('App\Models\Bus');
    }

}
