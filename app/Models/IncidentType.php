<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class IncidentType extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'incident_types';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'details', 'is_active'];

}
