<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomerFeedback extends Model
{
	protected $table = "customers-feedback";

    protected $fillable = ['title', 'operator_id', 'booking_id', 'feedback', 'rating'];

    public function operator()
    {
    	return $this->belongsTo(Operator::class);
    }
}
