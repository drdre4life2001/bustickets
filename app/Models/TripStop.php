<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TripStop extends Model
{
    //
    protected $table = 'trip_stops';

     /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['trip_id', 'stops'];

     public function trip(){
        return $this->belongsTo('App\Models\Trip','trip_id');

    }
}
