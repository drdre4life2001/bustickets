<?php

namespace App\Libraries;

class Utility
{
    
    /**
     * This is the template for all responses the api will serve
     * @var array
     */
    public static $response = ['status'=> 404, 'data' => [], 'message' => '' ];

    /**
     * This sets default parameters for requests
     * @param Request $request this is the request object
     * @param Array $default wanted fields tied to thier defaults
     * @return  Request $request this is the refined request object
     */
    public static function setDefaults($request, $default)
    {

        foreach ($default as $key => $value) {
            $request[$key] = ( is_null( $request[$key] ) ) ? $default[$key] : $request[$key];
        }
        return $request;
    }

    /**
     * This sets default parameters for requests
     * @param Request $request this is the request object
     * @param Array $default wanted fields tied to thier defaults
     * @return  Request $request this is the refined request object
     */
    public static function checkParameters($request, $required_params)
    {
        $missing = array();
        Utility::$response['status'] = 200;

        foreach ($required_params as $param) {
            if( is_null( $request[$param] ) )
                $missing[] = $param;   
            
        }
        
        if(!empty($missing)){
            Utility::$response['status'] = 404;
            Utility::$response['message'] = trans('app.missing-req-params', array('missing-params'=>implode(",", $missing)));

            return Utility::$response;

        }else
            return Utility::$response;
    }


    function send_sms($to, $msg)
    {

        $cid = "";
        $user = "info@bus.com.ng";
        // $senderArray = 'Buscomng';
        $senderId = env('SMS_USERNAME');
        $to = $to;

        $pass = env('SMS_PASS');
        $ch = curl_init();
        // dd($msg);
        $postdata = 'username=' . $user . '&password=' . $pass . '&message=' . $msg . '&sender=' . $senderId . '&mobiles=' . $to; //initialize the request variable
        // echo $postdata;
        $nmsg = urlencode($msg);


        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://portal.nigerianbulksms.com/api/?username=$user&password=$pass&message='$nmsg'&sender=$senderId&mobiles=$to",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",

            ),
        ));

        $response = curl_exec($curl);
        // dd($response);
        $err = curl_error($curl);

        curl_close($curl);
        return $response;

    }

}
