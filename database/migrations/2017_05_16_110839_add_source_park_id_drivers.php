<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSourceParkIdDrivers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('drivers', function (Blueprint $table) {
            
            $table->integer('source_park_id')->after('operator_id')->unsigned();;
            
            $table->foreign('source_park_id')->references('id')->on('parks');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('drivers', function (Blueprint $table) {
            
            $table->dropColumn('source_park_id');
        });
    }
}
