<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToBusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('buses', function (Blueprint $table) {
            //
           $table->string('chassis_number')->nullable()->after('number_plate');
           $table->string('engine_number')->nullable()->after('chassis_number');
           $table->integer('no_of_seats')->nullable()->after('engine_number');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('buses', function (Blueprint $table) {
            //
            $table->dropColumn("chassis_number");
            $table->dropColumn("engine_number");
            $table->dropColumn("no_of_seats");
        });
    }
}
