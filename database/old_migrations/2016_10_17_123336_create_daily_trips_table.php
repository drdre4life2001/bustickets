<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDailyTripsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('daily_trips', function(Blueprint $table)
		{
			$table->increments('id');
			$table->date('trip_date')->index();
			$table->integer('trip_id')->unsigned()->index();
			$table->integer('driver_id')->unsigned()->nullable()->index();
			$table->integer('bus_id')->unsigned()->nullable()->index();
			$table->integer('booked_seats')->default(0);
			$table->integer('total_seats')->default(0);
			$table->float('total_fare', 10, 0)->default(0);
			$table->float('driver_allowance', 10, 0)->default(0);
			$table->float('other_expense', 10, 0)->default(0);
			$table->string('status')->default('LOADING')->index();
			$table->string('departure_time', 25)->nullable();
			$table->string('trip_transit', 25)->nullable();
			$table->integer('ticketer_id')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('daily_trips');
	}

}
