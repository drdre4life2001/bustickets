<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDiscountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('discounts', function(Blueprint $table)
        {

            $table->increments('id');
            $table->integer('operator_id')->unsigned();
            $table->integer('age_one');
            $table->integer('age_two');
            $table->integer('discount');

            $table->foreign('operator_id')->references('id')->on('operators');

            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('discounts');
    }
}
