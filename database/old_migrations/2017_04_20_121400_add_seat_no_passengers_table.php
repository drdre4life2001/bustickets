<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSeatNoPassengersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('passengers', function (Blueprint $table) {
            
            $table->integer('seat_no')->after('gender');
            $table->integer('daily_trip_id')->after('age')->unsigned();
            
            $table->foreign('daily_trip_id')->references('id')->on('daily_trips');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('passengers', function (Blueprint $table) {
            
            $table->dropColumn('seat_no');
             $table->dropColumn('daily_trip_id');

        });
    }
}
