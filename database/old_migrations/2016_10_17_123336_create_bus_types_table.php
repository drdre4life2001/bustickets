<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBusTypesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('bus_types', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
			$table->boolean('show')->default(1);
			$table->integer('no_of_seats')->default(14);
			$table->integer('no_of_columns')->default(4);
			$table->integer('no_of_rows')->default(4);
			$table->integer('seat_width')->default(125);
			$table->integer('seat_height')->default(45);
			$table->string('no_seats')->default('-1, 0');
			$table->softDeletes();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('bus_types');
	}

}
