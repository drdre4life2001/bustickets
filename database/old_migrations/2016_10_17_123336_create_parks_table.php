<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateParksTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('parks', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('state_id')->unsigned()->index();
			$table->string('name');
			$table->text('address', 65535);
			$table->boolean('active')->default(1);
			$table->boolean('boardable')->default(1);
			$table->softDeletes();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('parks');
	}

}
