<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateIncidentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('incidents', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('incident_type_id')->unsigned()->index();
			$table->text('details', 65535)->nullable();
			$table->integer('park_id')->nullable();
			$table->integer('bus_id')->nullable();
			$table->float('expense_amount', 10, 0)->default(0);
			$table->softDeletes();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('incidents');
	}

}
