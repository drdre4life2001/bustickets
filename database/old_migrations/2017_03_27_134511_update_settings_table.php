<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('settings', function (Blueprint $table) {
            
           $table->integer('luggage_limit')->nullable();
           $table->integer('intl_cost_per_kg')->nullable();
           $table->integer('local_cost_per_kg')->nullable();
           $table->string('daily_trip_expense_headers')->nullable();
           $table->string('track_luggage')->nullable();
           $table->string('track_infant')->nullable();
           $table->integer('infant_discount')->nullable();
           $table->string('infant_age_range')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('settings', function (Blueprint $table) {
            
            $table->dropColumn("luggage_limit");
            $table->dropColumn("intl_cost_per_kg");
            $table->dropColumn("local_cost_per_kg");
            $table->dropColumn("daily_trip_expense_headers");
            $table->dropColumn("track_luggage");
            $table->dropColumn("track_infant");
            $table->dropColumn("infant_discount");
            $table->dropColumn("infant_age_ranges");
            
        });
    }
}
