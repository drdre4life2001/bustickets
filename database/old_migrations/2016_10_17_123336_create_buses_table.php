<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBusesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('buses', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('bus_number');
			$table->string('engine_number')->nullable();
			$table->string('chassis_number')->nullable();
			$table->string('bus_model')->nullable();
			$table->string('number_plate');
			$table->integer('bus_type_id')->unsigned()->nullable()->index();
			$table->integer('driver_id')->unsigned()->nullable()->index();
			$table->integer('default_driver_id')->unsigned()->nullable()->index();
			$table->integer('no_of_seats')->unsigned()->index();
			$table->string('vin')->nullable();
			$table->text('other_details', 65535)->nullable();
			$table->integer('default_park_id')->unsigned()->nullable()->index();
			$table->integer('default_driver_id')->unsigned()->nullable()->index();
			$table->integer('default_trip_id')->unsigned()->nullable()->index();
			$table->timestamps();
			$table->string('bus_roof')->nullable();
			$table->string('bus_mileage')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('buses');
	}

}
