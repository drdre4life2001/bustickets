<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddWaybillToDailytrips extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
   public function up()
    {
        Schema::table('daily_trips', function (Blueprint $table) {
            
            $table->double("way_bill")->default(0.00)->after('other_expense');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('daily_trips', function (Blueprint $table) {
            
            $table->dropColumn('way_bill');

        });
    }
}