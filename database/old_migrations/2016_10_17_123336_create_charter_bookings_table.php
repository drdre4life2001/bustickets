<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCharterBookingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('charter_bookings', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('booking_code');
			$table->date('travel_date');
			$table->text('pickup_location', 65535);
			$table->text('destination', 65535);
			$table->integer('park_id')->unsigned()->nullable()->index();
			$table->float('agreed_price', 10, 0)->nullable();
			$table->integer('bus_id')->unsigned()->nullable()->index();
			$table->integer('driver_id')->unsigned()->nullable()->index();
			$table->integer('user_id');
			$table->string('contact_name');
			$table->string('contact_phone');
			$table->string('contact_email')->nullable();
			$table->integer('number_of_passengers')->default(0);
			$table->string('status')->nullable();
			$table->date('paid_date')->nullable();
			$table->integer('payment_method_id')->unsigned()->nullable()->index();
			$table->string('receipt_no');
			$table->text('comment', 65535)->nullable();
			$table->softDeletes();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('charter_bookings');
	}

}
