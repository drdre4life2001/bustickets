<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSubTripsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('sub_trips', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
			$table->integer('operator_id')->unsigned()->index('subtrips_operator_id_index');
			$table->integer('source_park_id')->unsigned()->index('subtrips_source_park_id_index');
			$table->integer('dest_park_id')->unsigned()->index('subtrips_dest_park_id_index');
			$table->float('fare');
			$table->integer('state_id')->unsigned();
			$table->integer('parent_trip_id')->unsigned()->index('subtrips_parent_trip_id_index');
			$table->boolean('active')->default(1);
			$table->integer('operator_trip_id');
			$table->softDeletes();
			$table->timestamps();
			$table->string('trip_transit')->nullable();
			$table->float('round_trip_fare')->nullable();
			$table->integer('round_trip_status')->nullable();
			$table->integer('is_parent_trip')->nullable()->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('sub_trips');
	}

}
