<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDailyExpensesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('daily_trip_expenses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("daily_trip_id");
            $table->string("expense_name");
            $table->double("amount")->default(0.00);
            $table->timestamps();

            $table->foreign('daily_trip_id')->references('id')->on('daily_trips');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('daily_trip_expenses');
    }
}
