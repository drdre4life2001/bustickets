<div class="mainbar">

  <div class="container">

    <div class="row">
    <div class="col-md-9 col-sm-6" style="margin-top: 13px">

    <button type="button" class="btn mainbar-toggle" data-toggle="collapse" data-target=".mainbar-collapse">
      <i class="fa fa-bars"></i>
    </button>

    <div class="mainbar-collapse collapse">

      <ul class="nav navbar-nav mainbar-nav">

        <li class="">
          <a href="{{ url('/') }}">
            <i class="fa fa-line-chart"></i>
            Dashboard
          </a>
        </li>

<!--         <li class="">
          <a href="{{ url('trips/search') }}">
            <i class="fa fa-search"></i>
            Book a Ticket
          </a>
        </li> -->
{{--Ticketer's dashboard --}}    
  @if(false)
        <li class="dropdown ">
          <a href="#about" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">
            <i class="fa fa-search"></i>
            Book a Ticket
            <span class="caret"></span>
          </a>
          <ul class="dropdown-menu">   
            <li><a href="{{ url('trips/search') }}"><i class="fa fa-search nav-icon"></i> Regular Ticket </a></li>
            <li><a href="{{ url('charter-bookings/create') }}"><i class="fa fa-users nav-icon"></i> Charter Booking</a></li>
          </ul>
        </li>
        <li class="dropdown ">
          <a href="#about" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">
            <i class="fa fa-bus"></i>
            Bookings
            <span class="caret"></span>
          </a>
          <ul class="dropdown-menu">   
            <li><a href="{{ url('bookings') }}"><i class="fa fa-user nav-icon"></i>All Bookings</a></li>
            <li><a href="{{ url('bookings/status/PAID') }}"><i class="fa fa-user nav-icon"></i> Paid Bookings</a></li>
            <li><a href="{{ url('bookings/status/PENDING') }}"><i class="fa fa-user nav-icon"></i> Pending Bookings</a></li>
            <li><a href="{{ url('bookings/status/CANCELLED') }}"><i class="fa fa-user nav-icon"></i> Cancelled Bookings</a></li>
            <li><a href="{{ url('daily-trips') }}"><i class="fa fa-user nav-icon"></i>Trip Schedules</a></li>
            <li><a href="{{ url('charter-bookings') }}"><i class="fa fa-users nav-icon"></i> Charter Bookings</a></li>
          </ul>
        </li>


 @else

    <?php $permissions = Auth::user()->role->permissions; ?>




 <!--               super dashboard             -->
@if(Auth::user()->role_id != 1))
      @if(!(strpos($permissions, 'book_from_any_park') === FALSE))
        <li class="dropdown ">

          <a href="#about" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">
            <i class="fa fa-search"></i>
            Book a Ticket
            <span class="caret"></span>
          </a>
          <ul class="dropdown-menu">   
            <li><a href="{{ url('quick-book') }}"><i class="fa fa-search nav-icon"></i> Quick Book </a></li>
            <li><a href="{{ url('trips/search') }}"><i class="fa fa-search nav-icon"></i> Regular Ticket </a></li>
            <li><a href="{{ url('charter-bookings/create') }}"><i class="fa fa-users nav-icon"></i> Charter Booking</a></li>
            <li><a href="{{ url('passenger-bookings/view') }}"><i class="fa fa-search nav-icon"></i> Verify booking </a></li>
          </ul>
        </li>
      @endif

      @endif


        <li class="dropdown ">
          <a href="#about" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">
            <i class="fa fa-bus"></i>
            Bookings
            <span class="caret"></span>
          </a>
          <ul class="dropdown-menu">   
            @if(!(strpos($permissions, 'view_bookings') === FALSE))
            <li><a href="{{ url('bookings') }}"><i class="fa fa-user nav-icon"></i>All Bookings</a></li>
            @endif

            @if(!(strpos($permissions, 'view_bookings') === FALSE))
            <li><a href="{{ url('bookings/status/PAID') }}"><i class="fa fa-user nav-icon"></i> Paid Bookings</a></li>
            @endif

            @if(!(strpos($permissions, 'view_bookings') === FALSE))
            <li><a href="{{ url('bookings/status/PENDING') }}"><i class="fa fa-user nav-icon"></i> Pending Bookings</a></li>
            @endif

            @if(!(strpos($permissions, 'view_bookings') === FALSE))
            <li><a href="{{ url('bookings/status/CANCELLED') }}"><i class="fa fa-user nav-icon"></i> Cancelled Bookings</a></li>
            @endif

            @if(!(strpos($permissions, 'view_bookings') === FALSE))
            <li><a href="{{ url('daily-trips') }}"><i class="fa fa-user nav-icon"></i> Daily Trips</a></li>
            @endif

            @if(!(strpos($permissions, 'view_charter_bookings') === FALSE))
            <li><a href="{{ url('charter-bookings') }}"><i class="fa fa-users nav-icon"></i> Charter Bookings</a></li>
            @endif

          </ul>
        </li>
        <li class="dropdown ">
          <a href="#about" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">
            <i class="fa fa-road"></i>
            Trips
            <span class="caret"></span>
          </a>

          <ul class="dropdown-menu"> 
            @if(!(strpos($permissions, 'view_all_trips') === FALSE))  
              <li><a href="{{ url('trip-schedules') }}"><i class="fa fa-user nav-icon"></i> Trip Schedules</a></li>
            @endif
            @if(!(strpos($permissions, 'view_all_trips') === FALSE))  
              <li><a href="{{ url('driver-dest-report') }}"><i class="fa fa-user nav-icon"></i> Driver Destination Reports</a></li>
            @endif


            @if(!(strpos($permissions, 'view_all_trips') === FALSE))
            <li><a href="{{ url('trips') }}"><i class="fa fa-user nav-icon"></i> Manage Trips </a></li>
            @endif


            @if(!(strpos($permissions, 'add_trip') === FALSE))
            <li><a href="{{ url('trips/create') }}"><i class="fa fa-user nav-icon"></i> Add a Trip</a></li>
            @endif


            @if(!(strpos($permissions, 'add_trip') === FALSE))
            <li><a href="{{ url('trips') }}"><i class="fa fa-user nav-icon"></i> Adjust Trip prices </a></li>
            @endif
            
          </ul>
        </li>


        @if(Auth::user()->role_id == 1))
         <li class="dropdown ">
          <a href="#about" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">
            <i class="fa fa-users"></i>
            Operators
            <span class="caret"></span>
          </a>

          <ul class="dropdown-menu">   
            <li><a href="{{ url('operators') }}"><i class="fa fa-user nav-icon"></i> List Operators</a></li>
            <li><a href="{{ url('operators/create') }}"><i class="fa fa-user nav-icon"></i> Add Operators</a></li>
            <!--li><a href="{{ url('devices') }}"><i class="fa fa-user nav-icon"></i> Manage devices </a></li-->
          </ul>
        </li>
        @endif



<!-- 
         <li class="">
          <a href="{{ url('operators') }}">
            <i class="fa fa-dashboard"></i>
            Operator
          </a>
        </li> -->

        <li class="dropdown ">
          <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">
          <i class="fa fa-gear"></i> 
          Setups
            <span class="caret"></span>
          </a>

          <ul class="dropdown-menu">

          @if(!(strpos($permissions, 'edit_trip') === FALSE))
            <li class="dropdown-header">Locations</li>

            <!--<li>
              <a href="{{ url('/states') }}">
              <i class="fa fa-location-arrow nav-icon"></i> 
              States
              </a>
            </li>-->
          @if(Auth::user()->role_id ==1)
            <li>
              <a href="{{ url('my-parks') }}">
              <i class="fa fa-location-arrow nav-icon"></i> 
              Parks
              </a>
            </li>

            

            <li>
              <a href="{{ url('destinations') }}">
              <i class="fa fa-location-arrow nav-icon"></i> 
              Destinations
              </a>
            </li>
            @endif

            @endif

            <li class="divider"></li>

            @if(!(strpos($permissions, 'manage_drivers') === FALSE))

            <li>
              <a href="{{ url('/drivers') }}">
              <i class="fa fa-user nav-icon"></i> 
              Drivers
              </a>
            </li>
            @endif


            @if(!(strpos($permissions, 'manage_buses') === FALSE))
            <li>
              <a href="{{ url('/buses') }}">
              <i class="fa fa-bus nav-icon"></i> 
              Buses
              </a>
            </li>
            @endif

            <li class="divider"></li>


            @if(!(strpos($permissions, 'manage_incidents') === FALSE))
            <li>
              <a href="{{ url('/incidents') }}">
              <i class="fa fa-bolt nav-icon"></i> 
              View incidents
              </a>
            </li>

            <li>
              <a href="{{ url('/incident-types') }}">
              <i class="fa fa-bolt nav-icon"></i> 
              Manage incident types
              </a>
            </li>
            @endif


            <li class="divider"></li>


            @if(!(strpos($permissions, 'setup_roles') === FALSE))
            <li class="dropdown-header">Users & Roles</li>

            @if(Auth::user()->role_id == 1)
            <li>
              <a href="{{ url('/roles') }}">
              <i class="fa fa-users"></i> 
              &nbsp;&nbsp;Roles
              </a>
            </li>
            @endif

            @if(session('settings')->discount_active == true)
            <li>
              <a href="{{ url('/discounts') }}">
              <i class="fa fa-user"></i> 
              &nbsp;&nbsp;Discounts
              </a>
            </li>
            @endif

            <li>
              <a href="{{ url('/users') }}">
              <i class="fa fa-user"></i> 
              &nbsp;&nbsp;Users
              </a>
            </li>
            
            
            <li>
              <a href="{{ url('/settings') }}">
              <i class="fa fa-gear nav-icon"></i> 
              App settings
              </a>
            </li>
            @endif

            <!--li class="divider"></li>

            <li class="dropdown-header">Settings</li>

            <li>
              <a href="{{ url('/settings') }}">
              <i class="fa fa-users"></i> 
              &nbsp;&nbsp;App Settings
              </a>
            </li-->

            

          </ul>
        </li>


        @if(!(strpos($permissions, '_report') === FALSE))

        <li class="dropdown ">
          <a href="#about" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">
            <i class="fa fa-files-o"></i>
            Reports
            <span class="caret"></span>
          </a>

          <ul class="dropdown-menu">   

            @if(!(strpos($permissions, 'view_daily_trips_report') === FALSE))
            <li>
              <a href="{{ route('trips-reports') }}">
                <i class="fa fa-road"></i> 
                &nbsp;&nbsp;Daily Trips Report
              </a>
            </li>
            @endif



            @if(!(strpos($permissions, 'view_drivers_report') === FALSE))
            <li>
              <a href="{{ route('drivers-reports') }}">
                <i class="fa fa-user"></i> 
                &nbsp;&nbsp;Drivers Report
              </a>
            </li>
            @endif



            @if(!(strpos($permissions, 'view_buses_report') === FALSE))
            <li>
              <a href="{{ route('vehicles-reports') }}">
                <i class="fa fa-bus"></i> 
                &nbsp;&nbsp;Buses Reports
              </a>
            </li>
            @endif



            @if(!(strpos($permissions, 'view_manifests_report') === FALSE))
            <li>
              <a href="{{ route('manifest-reports') }}">
                <i class="fa fa-files-o"></i> 
                &nbsp;&nbsp;Manifest Reports
              </a>
            </li>
            @endif



            @if(!(strpos($permissions, 'view_travelers_report') === FALSE))
            <li>

              <a href="{{ route('traveler-reports') }}" >
                <i class="fa fa-users"></i> 
                &nbsp;&nbsp;Travelers Reports
              </a>
            </li>
            @endif



            @if(!(strpos($permissions, 'view_bookings_report') === FALSE))
            <li>
              <a href="{{ route('booking-reports') }}">
                <i class="fa fa-files-o"></i> 
                &nbsp;&nbsp;Bookings Reports
              </a>
            </li>
            @endif




            @if(!(strpos($permissions, 'view_charter_bookings_report') === FALSE))
            <li>
              <a href="{{ route('charter-reports') }}">
                <i class="fa fa-files-o"></i> 
                &nbsp;&nbsp;Charter Bookings Reports
              </a>
            </li>
            @endif



            @if(!(strpos($permissions, 'view_parks_report') === FALSE))
            <li>
              <a href="{{ route('park-reports') }}">
                <i class="fa fa-map-marker"></i> 
                &nbsp;&nbsp;Parks Reports
              </a>
            </li>
            @endif



            @if(!(strpos($permissions, 'view_ticketers_report') === FALSE))
            <li>
              <a href="{{ route('ticketers-reports') }}">
                <i class="fa fa-files-o"></i> 
                &nbsp;&nbsp;Ticketers Reports
              </a>
            </li>
            @endif



            @if(!(strpos($permissions, 'view_payments_report') === FALSE))
            <li>
              <a href="{{ route('payment-reports') }}">
                <i class="fa fa-shopping-cart"></i> 
                &nbsp;&nbsp;Payment Reports
              </a>
            </li>
            @endif



            @if(!(strpos($permissions, 'view_incidents_report') === FALSE))
            <li>
              <a href="{{ route('incident-reports') }}">
                <i class="fa fa-bolt"></i> 
                &nbsp;&nbsp;Incident Reports
              </a>
            </li>
            @endif


            @if(!(strpos($permissions, 'view_incidents_report') === FALSE))
            <li>
              <a href="{{ route('luggages-reports') }}">
                <i class="fa fa-bolt"></i> 
                &nbsp;&nbsp;Luggage Reports
              </a>
            </li>
            @endif

            @if(!(strpos($permissions, 'view_incidents_report') === FALSE))
            <li>
              <a href="{{ route('invoice-reports') }}">
                <i class="fa fa-bolt"></i> 
                &nbsp;&nbsp;Invoice Reports
              </a>
            </li>
            @endif

@if(!(strpos($permissions, 'view_incidents_report') === FALSE))
                <li>
                  <a href="{{ route('summarised-invoice-reports') }}">
                    <i class="fa fa-bolt"></i>
                    &nbsp;&nbsp;Summarised Invoice Report
                  </a>
                </li>
              @endif



          </ul>
        </li>

        @endif

        
        



        <!-- <li class="dropdown ">
          <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">
            <i class="fa fa-files-o"></i>
            Sample Pages
            <span class="caret"></span>
          </a>

          <ul class="dropdown-menu">
            <li><a href="./page-profile.html"><i class="fa fa-user nav-icon"></i> Profile</a></li>
            <li><a href="./page-invoice.html"><i class="fa fa-money nav-icon"></i> Invoice</a></li>
            <li><a href="./page-pricing.html"><i class="fa fa-dollar nav-icon"></i> Pricing Plans</a></li>
            <li><a href="./page-support.html"><i class="fa fa-question nav-icon"></i> Support Page</a></li>
            <li><a href="./page-gallery.html"><i class="fa fa-picture-o nav-icon"></i> Gallery</a></li>
            <li><a href="./page-settings.html"><i class="fa fa-cogs nav-icon"></i> Settings</a></li>
            <li><a href="./page-calendar.html"><i class="fa fa-calendar nav-icon"></i> Calendar</a></li>
          </ul>
        </li>   -->

      <!--   <li class="dropdown active">
          <a href="#contact" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">
            <i class="fa fa-external-link"></i>
            Extra Pages
            <span class="caret"></span>
          </a>

          <ul class="dropdown-menu" role="menu">
            <li>
              <a href="./page-notifications.html">
              <i class="fa fa-bell"></i> 
              &nbsp;&nbsp;Notifications
              </a>
            </li>     

            <li>
              <a href="./ui-icons.html">
              <i class="fa fa-smile-o"></i> 
              &nbsp;&nbsp;Font Icons
              </a>
            </li> 

            <li class="dropdown-submenu">
              <a tabindex="-1" href="#">
              <i class="fa fa-ban"></i> 
              &nbsp;&nbsp;Error Pages
              </a>

              <ul class="dropdown-menu">
                <li>
                  <a href="./page-404.html">
                  <i class="fa fa-ban"></i> 
                  &nbsp;&nbsp;404 Error
                  </a>
                </li>

                <li>
                  <a href="./page-500.html">
                  <i class="fa fa-ban"></i> 
                  &nbsp;&nbsp;500 Error
                  </a>
                </li>
              </ul>
            </li>

            <li class="dropdown-submenu">

              <a tabindex="-1" href="#">
              <i class="fa fa-lock"></i> 
              &nbsp;&nbsp;Login Pages
              </a>

              <ul class="dropdown-menu">
                <li>
                  <a href="./account-login.html">
                  <i class="fa fa-unlock"></i> 
                  &nbsp;&nbsp;Login
                  </a>
                </li>

                <li>
                  <a href="./account-login-social.html">
                  <i class="fa fa-unlock"></i> 
                  &nbsp;&nbsp;Login Social
                  </a>
                </li>

                <li>
                  <a href="./account-signup.html">
                  <i class="fa fa-star"></i> 
                  &nbsp;&nbsp;Signup
                  </a>
                  </li>

                <li>
                  <a href="./account-forgot.html">
                  <i class="fa fa-envelope"></i> 
                  &nbsp;&nbsp;Forgot Password
                  </a>
                </li>
              </ul>
            </li> 

            <li class="divider"></li>

            <li>
              <a href="./page-blank.html">
              <i class="fa fa-square-o"></i> 
              &nbsp;&nbsp;Blank Page
              </a>
            </li> 

          </ul>
        </li> -->

     @endif
        
      </ul>

    </div> <!-- /.navbar-collapse -->  
    </div> 

    <div class="col-md-3 col-sm-6" style="margin-top: 10px">
          @include('layouts.includes.sel_operator')
    </div>
    </div>

  </div> <!-- /.container --> 

</div> <!-- /.mainbar -->

