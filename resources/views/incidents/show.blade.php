@extends('layouts.master')

@section('content')

    <h1>Incident</h1>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>ID.</th> <th>{{ trans('incidents.incident_type_id') }}</th><th>{{ trans('incidents.details') }}</th><th>{{ trans('incidents.park_id') }}</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{ $incident->id }}</td> <td> {{ $incident->incident_type_id }} </td><td> {{ $incident->details }} </td><td> {{ $incident->park_id }} </td>
                </tr>
            </tbody>    
        </table>
    </div>

@endsection