@extends('layouts.master')

@section('content')

    <h1>Driver</h1>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>ID.</th> 
                    <th>{{ trans('drivers.name') }}</th>
                    <th>{{ trans('drivers.phone') }}</th>
                    <th>Phone 2</th>
                    <th>{{ trans('drivers.address') }}</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{ $driver->id }}</td> 
                    <td> {{ $driver->name }} </td>
                    <td> {{ $driver->phone }} </td>
                    <td> {{ $driver->phone_2 }} </td>
                    <td> {{ $driver->address }} </td>
                </tr>
            </tbody>    
        </table>
    </div>

@endsection