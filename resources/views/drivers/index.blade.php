@extends('layouts.master')

@section('content')

    <div class="content-header">
        <h2 class="content-header-title">Drivers </h2>
        <ol class="breadcrumb">
          <li><a href="{{ url('/') }}">Dashboard</a></li>
          <li class="active">Drivers </li>
        </ol>
      </div> <!-- /.content-header -->



      <div class="row">

        <div class="col-md-10 col-sm-8">

          	<!--	<a href="{{ URL::to('downloadExcel/xls') }}"><button class="btn btn-success">Download Excel xls</button></a>
          		<a href="{{ URL::to('downloadExcel/xlsx') }}"><button class="btn btn-success">Download Excel xlsx</button></a>
          		<a href="{{ URL::to('downloadExcel/csv') }}"><button class="btn btn-success">Download CSV</button></a>
            -->
            <form style="border: 4px solid #a1a1a1;margin-top: 15px;padding: 10px;"
              action="drivers/upload" class="form-horizontal" method="post" enctype="multipart/form-data">
          			<input type="file" name="userfile" />
                <br/>
          			<button class="btn btn-primary">Import File</button>
          		</form>


          <!--<form action="drivers/upload" method="POST">

              <div class="form-group">
                  <div class="col-sm-6">
                    <input type="file"name="userfile" value="upload drivers"/>
                    <input type="submit" class="btn-default" value="Upload" />
                  </div>
              </div>

          </form> -->
          <div class="portlet">

            <div class="portlet-content">

              <div class="table-responsive">

              <table
                class="
                table
                table-striped
                table-bordered
                table-hover
                table-highlight
                table-checkable"
                data-provide="datatable"
                data-display-rows="10"
                data-info="true"
                data-search="true"
                data-length-change="true"
                data-paginate="true">

                  <thead>
                    <tr>
                      <th>S.No</th>
                      <th>{{ trans('drivers.name') }}</th>
                      <th>{{ trans('drivers.phone') }}</th>
                      <th>{{ trans('drivers.address') }}</th>
                      <th>Default Bus</th>
                      <th>Default Park</th>
                      <th>Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    {{-- */$x=0;/* --}}
                    @foreach($drivers as $item)
                        {{-- */$x++;/* --}}
                        <tr>
                            <td>{{ $x }}</td>
                            <td><a href="{{ url('drivers', $item->id) }}">{{ $item->name }}</a></td><td>{{ $item->phone }}</td>
                            <td>{{ $item->address }}</td>
                            <td>
                              {{ (!empty($item->default_bus))?$item->default_bus->bus_number:'' }}
                              <!--a type="button" class=" " data-toggle="modal" data-target="#busModal{{$item->id}}"> Assign to a Bus</a-->
                            </td>
                            <td>
                              {{ (!empty($item->default_park))?$item->default_park->name:'' }}
                              <a type="button" class=" " data-toggle="modal" data-target="#parkModal{{$item->id}}"> Assign to a Park</a
                            </td>
                            <td>
                                <a href="{{ url('drivers/' . $item->id . '/edit') }}" title="Edit">
                                    <button type="submit" class="btn btn-default btn-xs"><i class="fa fa-edit"></i></button>
                                </a>
                                {!! Form::open([
                                    'method'=>'DELETE',
                                    'url' => ['drivers', $item->id],
                                    'style' => 'display:inline'
                                ]) !!}
                                    {!! Form::button('<i class="fa fa-times"></i>', ['class' => 'btn btn-danger btn-xs', 'escape'=>false,'title'=>'Delete', 'type'=>'submit']) !!}
                                {!! Form::close() !!}
                            </td>
                        </tr>



<div class = "modal fade" id="parkModal{{$item->id}}" tabindex = "-1" role = "dialog" aria-labelledby = "myModalLabel" aria-hidden = "true">
   <div class = "modal-dialog">
      <div class = "modal-content">
         <div class = "modal-header">
            <button type = "button" class = "close" data-dismiss = "modal" aria-hidden = "true">
                  &times;
            </button>
            <h4 class = "modal-title" id = "myModalLabel">
               Assign to a default Park
            </h4>
         </div>
         {!! Form::model($item, [
                    'method' => 'PATCH',
                    'url' => ['drivers', $item->id],
                    'class' => 'form-horizontal'
                ]) !!}
         <div class = "modal-body" id="contentArea">
              <input type="hidden" id="SubBtn" name="_token" value="<?php echo csrf_token(); ?>">
              <div class="form-group {{ $errors->has('default_park_id') ? 'has-error' : ''}}">
                {!! Form::label('default_park_id', trans('buses.default_park_id'), ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::select('default_park_id', $parks,null, ['class' => 'form-control select22', 'id'=>"select22", 'required' => 'required']) !!}
                    {!! $errors->first('default_park_id', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
         </div>
         <div class = "modal-footer">
            <button type = "button" class = "btn btn-default" data-dismiss = "modal">
               Cancel
            </button>
            <button type = "submit" id="NoteSub" class = "btn btn-primary">
               Assign Driver
            </button>
         </form>

         </div>
      </div><!-- /.modal-content -->
   </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

                    @endforeach
                  </tbody>
                </table>
              </div> <!-- /.table-responsive -->


            </div> <!-- /.portlet-content -->

          </div> <!-- /.portlet -->



        </div> <!-- /.col -->

        <div class="col-md-2 col-sm-4">

            <ul id="myTab" class="nav nav-pills nav-stacked">
              <li class="active">
                <a href="{{ url('drivers/create') }}">
                  <i class="fa fa-plus"></i>
                  Add New Driver
                </a>
              </li>
              <li class="active">
              <a href="{{ URL::to('/downloads/drivers_template.xlsx')  }}" >
              <i class="fa fa-download"></i>
                Download Excel
              </a>
            </li>
          </ul>

        </div>







      </div> <!-- /.row -->



@endsection
