@extends('layouts.master')

@section('content')

<style type="text/css">
  #holder{  
   height:200px;   
   width:550px;
   background-color:#F5F5F5;
   border:1px solid #A4A4A4;
   margin-left:10px;  
  }
   #place {
   position:relative;
   margin:7px;
   }
     #place a{
   font-size:0.6em;
   }
     #place li
     {
         list-style: none outside none;
         position: absolute;   
     }    
     #place li:hover
     {
        background-color:yellow;      
     } 
   #place .seat{
   background:url("{{ asset('bckend/img/available_seat_img.gif') }}") no-repeat scroll 0 0 transparent;
   height:33px;
   width:33px;
   display:block;  
   }

   #place .no-seat{
   /*background:url("{{ asset('bckend/img/available_seat_img.gif') }}") no-repeat scroll 0 0 transparent;*/
   height:33px;
   width:33px;
   display:block;  
   }


      #place .selectedSeat
      { 
    background-image:url('{{ asset('bckend/img/booked_seat_img.gif') }}');        
      }
     #place .selectingSeat
      { 
    background-image:url("{{asset('bckend/img/selected_seat_img.gif') }}");        
      }
    #place .row-3, #place .row-4{
    margin-top:10px;
    }
   #seatDescription{
   padding:0px;
   }
    #seatDescription li{
    verticle-align:middle;    
    list-style: none outside none;
     padding-left:35px;
    height:35px;
    float:left;
    }
    </style>


<div class="content-header">
        <h2 class="content-header-title">Destination Details and stops </h2>
        <ol class="breadcrumb">
          <li><a href="{{ url('/') }}">Dashboard</a></li>
          <li><a href="{{ url('destinations') }}">Destinations </a></li>
          <li class="active">Destination Details and stops </li>
        </ol>
      </div> <!-- /.content-header -->

<div class="container">
    
<br><br>

<div class="alert alert-success hide" id="paidAlertDiv">
  Booking has been marked as PAID. <!--You can now print the Ticket-->
</div>

<div class="col-md-6 col-sm-8">
    
    <div class="portlet">

            
            <div class="portlet-content">

              <dl>
                <dt>Name </dt>
                <dd>{{ $park->name }}</dd>

                <dt>Addresss </dt>
                <dd>{{ $park->address}}</dd>

                <dt>State </dt>
                <dd>{{ $park->state->name }}</dd>



                
                <dt>Created date</dt>
               
                <dd> {{ date('D, d/m/Y', strtotime($park->created_at)) }} </dd>
                
                
                

              </dl>

             


              

            </div> <!-- /.portlet-content -->

          </div>
</div>

<div class="col-md-6 col-sm-3">

    <div class="portlet">

            <div class="portlet-header">

              <h3>
                <i class="fa fa-reorder"></i>
                Stops
              </h3>

            </div> <!-- /.portlet-header -->

            <div class="portlet-content">           

              <div class="table-responsive">

              <table 
                class="table table-striped table-bordered table-hover table-highlight table-checkable" 
                data-provide="datatable1" 
                data-display-rows="10"
                data-info="true"
                data-search="true"
                data-length-change="true"
                data-paginate="true"
              >
                  <thead>
                    <tr>
                      <th>Name</th>
                      <th>&nbsp;</th>
                    </tr>
                  </thead>
                  <tbody>
                    {{-- */$x=0;/* --}}
                    @foreach($park->stops as $stop)
                       <tr> 
                        <td>{{$stop->name}}</td>
                        <td>
                            <div class = "modal fade" id="editStopModal{{ $stop->id }}" tabindex = "-1" role = "dialog" aria-labelledby = "myModalLabel" aria-hidden = "true">
   <div class = "modal-dialog">
      <div class = "modal-content">
         <div class = "modal-header">
            <button type = "button" class = "close" data-dismiss = "modal" aria-hidden = "true">
                  &times;
            </button>
            <h4 class = "modal-title" id = "myModalLabel">
               Add Stop
            </h4>
         </div>
         <form action="{{ url('edit-stop') }}" method="post" id="bookingNotes33">
         <div class = "modal-body" id="contentArea">
              <input type="hidden" id="SubBtn" name="_token" value="<?php echo csrf_token(); ?>">
          <input type="hidden" name="id" value="{{ $stop->id }}">
          <input type="hidden" name="park_id" value="{{ $stop->park_id }}">
            Name: <input type="text" name="name" class="form-control" value="{{ $stop->name }}" />
         </div>
         <div class = "modal-footer">
            <button type = "button" class = "btn btn-default" data-dismiss = "modal">
               Close
            </button>
            <button type = "submit" id="" class = "btn btn-primary">
               Save
            </button>
         </form>

         </div>
      </div><!-- /.modal-content -->
   </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

                            <button type="button" class="btn btn-default  btn-xs" data-toggle="modal" data-target="#editStopModal{{ $stop->id }}""> 
                <i class="fa fa-edit"></i>Edit
            </button>
                            {!! Form::open([
                                    'method'=>'POST',
                                    'url' => 'delete-stop',
                                    'style' => 'display:inline'
                                ]) !!}
                                <input type="hidden" name="id" value="{{ $stop->id }}">
                                <input type="hidden" name="park_id" value="{{ $stop->park_id }}">
                                    {!! Form::button('<i class="fa fa-times"></i>', ['class' => 'btn btn-danger btn-xs', 'escape'=>false,'title'=>'Delete', 'type'=>'submit']) !!}
                                {!! Form::close() !!}


                        </td>
                      </tr>      
                    @endforeach

                   
                  </tbody>
                </table>
              </div> <!-- /.table-responsive -->


              

            </div> <!-- /.portlet-content -->

          </div> <!-- /.portlet -->


              <button type="button" class="btn btn-success " data-toggle="modal" data-target="#addStopModal"> 
                Add Stop
            </button>
          

    
</div>
    




    
</div>


<div class = "modal fade" id="addStopModal" tabindex = "-1" role = "dialog" aria-labelledby = "myModalLabel" aria-hidden = "true">
   <div class = "modal-dialog">
      <div class = "modal-content">
         <div class = "modal-header">
            <button type = "button" class = "close" data-dismiss = "modal" aria-hidden = "true">
                  &times;
            </button>
            <h4 class = "modal-title" id = "myModalLabel">
               Add Stop
            </h4>
         </div>
         <form action="{{ url('add-stop') }}" method="post" id="bookingNotes33">
         <div class = "modal-body" id="contentArea">
              <input type="hidden" id="SubBtn" name="_token" value="<?php echo csrf_token(); ?>">
          <input type="hidden" name="park_id" value="{{ $park->id }}">
            Name: <input type="text" name="name" class="form-control" />
         </div>
         <div class = "modal-footer">
            <button type = "button" class = "btn btn-default" data-dismiss = "modal">
               Close
            </button>
            <button type = "submit" id="" class = "btn btn-primary">
               Save
            </button>
         </form>

         </div>
      </div><!-- /.modal-content -->
   </div><!-- /.modal-dialog -->
</div><!-- /.modal -->





@endsection