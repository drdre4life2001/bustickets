@extends('layouts.master')

@section('content')

    <h1>Operator - {{ $operator->name }} </h1>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>Parks</th> <th>Address</th> <th>Actions</th>
                </tr>
            </thead>
            <tbody>
             @foreach($operator->parks->where('boardable', 1) as $park)
                <tr>
                    <td>{{$park->name}}</td> 
                    <td>{{ $park->address }}  </td>
                    <td>  </td>
                </tr>
                @endforeach
            </tbody>    
        </table>
    </div>



@endsection