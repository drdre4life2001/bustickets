@extends('layouts.master')

@section('content')

    <div class="content-header">
        <h2 class="content-header-title">Profile </h2>
        <ol class="breadcrumb">
          <li><a href="{{ url('/') }}">Dashboard</a></li>
        </ol>
      </div> <!-- /.content-header -->

      

      <div class="row">

        <div class="col-md-10 col-sm-8">

          <div class="portlet">

            <div class="portlet-content">

                {!! Form::model($operator, [
                    'method' => 'PATCH',
                    'url' => ['operators', $operator->id],
                    'class' => 'form-horizontal'
                ]) !!}

                            <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                {!! Form::label('name', 'Name: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('name', null, ['class' => 'form-control']) !!}
                {!! $errors->first('name', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('access_code') ? 'has-error' : ''}}">
                {!! Form::label('access_code', 'Access Code: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('access_code', null, ['class' => 'form-control']) !!}
                {!! $errors->first('access_code', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('address') ? 'has-error' : ''}}">
                {!! Form::label('address', 'Address: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::textarea('address', null, ['class' => 'form-control']) !!}
                {!! $errors->first('address', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('contact_person') ? 'has-error' : ''}}">
                {!! Form::label('contact_person', 'Contact Person: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('contact_person', null, ['class' => 'form-control']) !!}
                {!! $errors->first('contact_person', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('phone') ? 'has-error' : ''}}">
                {!! Form::label('phone', 'Phone: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('phone', null, ['class' => 'form-control']) !!}
                {!! $errors->first('phone', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
                {!! Form::label('email', 'Email: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('email', null, ['class' => 'form-control']) !!}
                {!! $errors->first('email', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('details') ? 'has-error' : ''}}">
                {!! Form::label('details', 'Details: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::textarea('details', null, ['class' => 'form-control']) !!}
                {!! $errors->first('details', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('website') ? 'has-error' : ''}}">
                {!! Form::label('website', 'Website: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('website', null, ['class' => 'form-control']) !!}
                {!! $errors->first('website', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('img') ? 'has-error' : ''}}">
                {!! Form::label('img', 'Img: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('img', null, ['class' => 'form-control']) !!}
                {!! $errors->first('img', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('commission') ? 'has-error' : ''}}">
                {!! Form::label('commission', 'Commission: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::number('commission', null, ['class' => 'form-control']) !!}
                {!! $errors->first('commission', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('portal_link') ? 'has-error' : ''}}">
                {!! Form::label('portal_link', 'Portal Link: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('portal_link', null, ['class' => 'form-control']) !!}
                {!! $errors->first('portal_link', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>
           


                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-3">
                        {!! Form::submit('Update', ['class' => 'btn btn-primary form-control']) !!}
                    </div>
                </div>
                {!! Form::close() !!}

                @if ($errors->any())
                    <ul class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                @endif

             </div> <!-- /.portlet-content -->

          </div> <!-- /.portlet -->

        

        </div> <!-- /.col -->


      </div> <!-- /.row -->   

@endsection