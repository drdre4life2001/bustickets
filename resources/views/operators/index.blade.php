@extends('layouts.master')

@section('content')
    
    @if(Session::has('select'))
    <div class="alert alert-danger">
        <a class="close" data-dismiss="alert" href="#" aria-hidden="true">×</a>
        <strong>Oh snap!</strong> {{Session::get('select')}}
      </div>
    @endif
    <div class="content-header">
        <h2 class="content-header-title">Operators </h2>
        <ol class="breadcrumb">
          <li><a href="{{ url('/') }}">Dashboard</a></li>
          <li class="active">Operators </li>
        </ol>
      </div> <!-- /.content-header -->

      

      <div class="row">

        <div class="col-md-12 col-sm-12">

          <div class="portlet">

            <div class="portlet-content">           

              <div class="table-responsive">

              <table 
                class="table table-striped table-bordered table-hover table-highlight table-checkable" 
                data-provide="datatable" 
                data-display-rows="10"
                data-info="true"
                data-search="true"
                data-length-change="true"
                data-paginate="true"
              >
                  <thead>
                    <tr>
                      <th>S.No</th><th>Name</th><th>Access Code</th><th>Contact Person</th><th>Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    {{-- */$x=0;/* --}}
                    @foreach($operators as $item)
                        {{-- */$x++;/* --}}
                        <tr>
                            <td>{{ $x }}</td>
                            <td><a href="{{ url('operators', $item->id) }}">{{ $item->name }}</a></td><td>{{ $item->access_code }}</td><td>{{ $item->contact_person }}</td>
                            <td>
                              <a href="{{ url('operators/select/'.$item->id) }}"><small>SELECT</small></a>
                                <a href="{{ url('operators/' . $item->id . '/edit') }}" title="Edit">
                                    <button type="submit" class="btn btn-default btn-xs"><i class="fa fa-edit"></i></button>
                                </a> 
                                {!! Form::open([
                                    'method'=>'DELETE',
                                    'url' => ['operators', $item->id],
                                    'style' => 'display:inline'
                                ]) !!}
                                    {!! Form::button('<i class="fa fa-times"></i>', ['class' => 'btn btn-danger btn-xs', 'escape'=>false,'title'=>'Delete', 'type'=>'submit']) !!}
                                {!! Form::close() !!}
                            </td>
                        </tr>
                    @endforeach
                  </tbody>
                </table>
              </div> <!-- /.table-responsive -->
              

            </div> <!-- /.portlet-content -->

          </div> <!-- /.portlet -->

        

        </div> <!-- /.col -->

        <div class="col-md-2 col-sm-4">

            <ul id="myTab" class="nav nav-pills nav-stacked">
            <li class="active">
              <a href="{{ url('operators/create') }}">
                <i class="fa fa-plus"></i> 
                Add New Operator
              </a>
            </li>
          </ul>

        </div>

      </div> <!-- /.row -->    



@endsection
