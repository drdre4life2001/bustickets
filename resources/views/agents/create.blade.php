@extends('layouts.master')

@section('content')

   <div class="content-header">
        <h2 class="content-header-title">Add Ticketer </h2>
        <ol class="breadcrumb">
          <li><a href="{{ url('/') }}">Dashboard</a></li>
          <li><a href="{{ url('agents') }}">Ticketers </a></li>
          <li class="active">Add Ticketer </li>
        </ol>
      </div> <!-- /.content-header -->

      

      <div class="row">

        <div class="col-md-10 col-sm-8">

          <div class="portlet">

           @if(Session::has('flash_message'))
            <div class="alert alert-danger">
                <a class="close" data-dismiss="alert" href="#" aria-hidden="true">×</a>
                <strong>Oh snap!</strong> {{Session::get('flash_message')}}
              </div>
            @endif

            <div class="portlet-content">

                {!! Form::open(['url' => 'agents', 'class' => 'form-horizontal']) !!}
                    
               <!--  <div class="form-group {{ $errors->has('user_id') ? 'has-error' : ''}}">
                {!! Form::label('user_id', 'User: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    <select name="user_id" class="form-control select22">
                        <option value="">Choose User</option>
                        @foreach($agents as $user)
                        <option value="{{ $user->id }}">{{ $user->name }}</option>
                        @endforeach
                    </select>
                {!! $errors->first('user_id', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>



                            <div class="form-group {{ $errors->has('user_id') ? 'has-error' : ''}}">
                {!! Form::label('user_id', 'User Id: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::number('user_id', null, ['class' => 'form-control']) !!}
                {!! $errors->first('user_id', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div> -->
            <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                {!! Form::label('name', 'Name: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('name', null, ['class' => 'form-control']) !!}
                {!! $errors->first('name', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>

            <div class="form-group {{ $errors->has('username') ? 'has-error' : ''}}">
                {!! Form::label('username', 'Username: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('username', null, ['class' => 'form-control']) !!}
                {!! $errors->first('username', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>

           

            <div class="form-group {{ $errors->has('password') ? 'has-error' : ''}}">
                {!! Form::label('password', 'Password: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::password('password', null, ['class' => 'form-control']) !!}
                {!! $errors->first('password', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>

            <div class="form-group {{ $errors->has('phone') ? 'has-error' : ''}}">
                {!! Form::label('phone', 'Phone: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('phone', null, ['class' => 'form-control']) !!}
                {!! $errors->first('phone', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
                {!! Form::label('name', 'Email: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('email', null, ['class' => 'form-control']) !!}
                {!! $errors->first('email', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>

            <div class="form-group {{ $errors->has('address') ? 'has-error' : ''}}">
                {!! Form::label('address', 'Address: ', ['class' => 'col-sm-3 control-label']) !!}

                <div class="col-sm-6">
                    {!! Form::textarea('address', null, ['class' => 'form-control']) !!}
                {!! $errors->first('address', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>


            

            <div class="form-group {{ $errors->has('park_id') ? 'has-error' : ''}}">
                {!! Form::label('park_id', 'Park: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    <select name="park_id" class="form-control select22">
                        <option value="">Choose Park</option>
                        @foreach($parks as $park)
                        <option value="{{ $park->id }}"   >{{ $park->name }}</option>
                        @endforeach
                    </select>
                {!! $errors->first('park_id', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>

            <div class="form-group {{ $errors->has('device') ? 'has-error' : ''}}">
                {!! Form::label('device_id', 'Device: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    <select name="device_id" class="form-control select22">
                        <option value="">Choose Device</option>
                        @foreach($devices as $device)
                        <option value="{{ $device->id }}" >{{ $device->name }}</option>
                        @endforeach
                    </select>
                {!! $errors->first('device_id', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>


            
                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-3">
                        {!! Form::submit('Create', ['class' => 'btn btn-primary form-control']) !!}
                    </div>
                </div>
                {!! Form::close() !!}

                @if ($errors->any())
                    <ul class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                @endif
             </div> <!-- /.portlet-content -->

          </div> <!-- /.portlet -->

        

        </div> <!-- /.col -->

        <div class="col-md-2 col-sm-4">

            <ul id="myTab" class="nav nav-pills nav-stacked">
            <li class="active">
              <a href="{{ url('agents') }}">
                <i class="fa fa-bars"></i> 
                List Ticketers
              </a>
            </li>
          </ul>

        </div>

      </div> <!-- /.row -->   

@endsection