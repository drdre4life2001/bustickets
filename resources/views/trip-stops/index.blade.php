@extends('layouts.master')

@section('content')
  <script>
  $(document).ready(function(){
  
  // get the trip id
  $.each($('#from'), function (index, value) {
       var trip_id = $('#tId').html();
       // use this to load 'from'  
        $.ajax({
                url: '{{ route("get-trip-from") }}',
                dataType: 'text',
                type: 'GET',
                contentType: 'application/x-www-form-urlencoded',
                data: {"trip_id":trip_id},
                success: function (rslts, textStatus, jQxhr)
                {
                  var r = rslts.split("_");
                  $("#from").html(r[1]);
                  $("#to").html(r[2]);                        
                },
                error: function (jqXhr,textStatus, errorThrown)
                {
                                          
                }
               });                 
              });

  });
  </script>

    <div class="content-header">
        <h2 class="content-header-title">Trip Stops </h2>
        <ol class="breadcrumb">
          <li><a href="{{ url('/') }}">Dashboard</a></li>
          <li class="active">Trip Stops </li>
        </ol>
      </div> <!-- /.content-header -->


      

      <div class="row">

         <div class="col-md-2 col-sm-4 col-offset-md-10">

            <ul id="myTab" class="nav nav-pills nav-stacked">
            <li class="active">
              <a href="{{ url('trip-stops/create') }}">
                <i class="fa fa-plus"></i> 
                Add Trip Stops
              </a>
            </li>
          </ul>

        </div>

        <div class="col-md-12 col-sm-8">

          <div class="portlet">

            <div class="portlet-content">           

              <div class="table-responsive">
             
              <table  
                class="table table-striped table-bordered table-hover table-highlight table-checkable" 
                data-provide="datatable" 
                data-display-rows="10"
                data-info="true"
                data-search="true"
                data-length-change="true"
                data-paginate="true"
                id="tStops"
              >
                  <span id='flash' ></span>
                  <thead>
                    <tr>
                      <th>Trip Id</th>
                      <th>From</th>
                      <th>To</th>
                      <th>Stops</th>
                      <th>Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    {{-- */$x=0;/* --}}
                    @foreach($trips as $item)
                        {{-- */$x++;/* --}}
                        <tr>
                            <td id="tId">{{ $item['trip_id'] }}</td>
                            <td id="from"></td>
                            <td id="to"></td>
                            <td>{{ $item['stops'] }}</td>
                            
                            <td>
                                 
                                <a href="{{ url('trip-stops/' . $item['id'] . '/edit') }}" title="Edit">
                                    <button type="submit" class="btn btn-default"><i class="fa fa-edit"></i></button>
                                </a> 

                                <!--
                                {!! Form::open([
                                    'method'=>'DELETE',
                                    'url' => ['trips', $item['id']],
                                    'style' => 'display:inline'
                                ]) !!}
                                    {!! Form::button('<i class="fa fa-times"></i>', ['class' => 'btn btn-danger btn-xs', 'onclick'=>'ConfirmDelete()', 'escape'=>false,'title'=>'Delete', 'type'=>'submit']) !!}
                                {!! Form::close() !!}

                                -->
                            </td>
                        </tr>
                        

  <script src="{{ url('bckend/js/libs/jquery-1.10.1.min.js') }}"></script>



                    @endforeach
                  </tbody>
                </table>
              </div> <!-- /.table-responsive -->
              

            </div> <!-- /.portlet-content -->

          </div> <!-- /.portlet -->

        <!-- Modal -->


        </div> <!-- /.col -->

       

      </div> <!-- /.row -->    





@endsection
