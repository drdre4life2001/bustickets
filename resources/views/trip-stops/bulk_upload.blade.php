@extends('layouts.master')

@section('content')

 <div class="content-header">
        <h2 class="content-header-title">Trips </h2>
        <ol class="breadcrumb">
          <li><a href="{{ url('/') }}">Dashboard</a></li>
          <li class="active">Trips </li>
        </ol>
      </div> <!-- /.content-header -->


      

      <div class="row">

        <div class="col-md-10 col-sm-8">

          <div class="portlet">

            <div class="portlet-content">  

                @if(!empty($flash))
                    @foreach($flash as $f)
                  <div class="alert alert-info">
                        <?php echo $f; ?></div>
                    @endforeach
                @endif

             <h1>Bulk Upload Trips</h1>
        

                <!-- <form action="{{ route('upload-trip') }}" method="post" type='file'> -->
                 {!! Form::open(array('route'=>'upload-trip','method'=>'POST', 'files'=>true)) !!}

                    
                    {!! csrf_field() !!}
                  
                    <!-- <input type="file" name="userfile"> -->
                    {!! Form::file('userfile') !!}


                    <br>

                    <input type="submit" value="Upload Trips" class="btn btn-danger">
                </form>

            </div>
        </div>
        </div>
    </div>


@endsection