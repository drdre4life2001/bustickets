@extends('layouts.master')

@section('content')

<script src="{{ asset('bckend/js/libs/jquery-1.10.1.min.js') }}"></script>


    <div class="content-header">
        <h2 class="content-header-title">Edit Passenger Details </h2>
        <ol class="breadcrumb">
          <li><a href="{{ url('/') }}">Dashboard</a></li>
          <li><a href="{{ url('bookings') }}">Bookings </a></li>
          <li class="active">Edit Passenger Details </li>
        </ol>
      </div> <!-- /.content-header -->

      

      <div class="row">

        <div class="col-md-10 col-sm-8">

          <div class="portlet">

            <div class="portlet-content">

                {!! Form::model($booking, [
                    'method' => 'PATCH',
                    'url' => ['bookings', $booking->id],
                    'class' => 'form-horizontal'
                ]) !!}

            <!--div class="form-group {{ $errors->has('date') ? 'has-error' : ''}}">
                {!! Form::label('date', 'Date: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::date('date', null, ['class' => 'form-control']) !!}
                {!! $errors->first('date', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('passenger_count') ? 'has-error' : ''}}">
                {!! Form::label('passenger_count', 'Passenger Count: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('passenger_count', null, ['class' => 'form-control']) !!}
                {!! $errors->first('passenger_count', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('unit_cost') ? 'has-error' : ''}}">
                {!! Form::label('unit_cost', 'Unit Cost: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::number('unit_cost', null, ['class' => 'form-control']) !!}
                {!! $errors->first('unit_cost', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('final_cost') ? 'has-error' : ''}}">
                {!! Form::label('final_cost', 'Final Cost: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::number('final_cost', null, ['class' => 'form-control']) !!}
                {!! $errors->first('final_cost', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
                {!! Form::label('status', 'Status: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('status', null, ['class' => 'form-control']) !!}
                {!! $errors->first('status', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('paid_date') ? 'has-error' : ''}}">
                {!! Form::label('paid_date', 'Paid Date: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::input('datetime-local', 'paid_date', null, ['class' => 'form-control']) !!}
                {!! $errors->first('paid_date', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('booking_code') ? 'has-error' : ''}}">
                {!! Form::label('booking_code', 'Booking Code: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('booking_code', null, ['class' => 'form-control']) !!}
                {!! $errors->first('booking_code', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div-->

            <!-- <div class="form-group {{ $errors->has('departure_time') ? 'has-error' : ''}}">
                {!! Form::label('departure_time', 'Departure Time: ', ['class' => 'col-sm-3 control-label']) !!}


                <div class="col-sm-6 input-group bootstrap-timepicker">
                <input id="tp-ex-1" type="text" class="form-control" name="departure_time" value="{{ $booking['departure_time'] }}">
                    <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                   
                {!! $errors->first('departure_time', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div> -->
          

            <div class="form-group {{ $errors->has('contact_name') ? 'has-error' : ''}}">
                {!! Form::label('contact_name', 'Contact Name: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('contact_name', null, ['class' => 'form-control']) !!}
                {!! $errors->first('contact_name', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>
            
            <div class="form-group {{ $errors->has('contact_phone') ? 'has-error' : ''}}">
                {!! Form::label('contact_phone', 'Contact Phone: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('contact_phone', null, ['class' => 'form-control','maxlength'=>'11']) !!}
                {!! $errors->first('contact_phone', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>

            <div class="form-group {{ $errors->has('contact_email') ? 'has-error' : ''}}">
                {!! Form::label('contact_email', 'Contact Email: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::email('contact_email', null, ['class' => 'form-control']) !!}
                {!! $errors->first('contact_email', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>

             <div class="form-group {{ $errors->has('contact_address') ? 'has-error' : ''}}">
                {!! Form::label('contact_address', 'Contact Address: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!!  Form::textarea('contact_address', null, ['class' => 'form-control', 'size' => '7x5'])  !!}
                {!! $errors->first('contact_address', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>

            <div class="form-group {{ $errors->has('next_of_kin') ? 'has-error' : ''}}">
                {!! Form::label('next_of_kin', 'Next Of Kin: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('next_of_kin', null, ['class' => 'form-control']) !!}
                {!! $errors->first('next_of_kin', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('next_of_kin_phone') ? 'has-error' : ''}}">
                {!! Form::label('next_of_kin_phone', 'Next Of Kin Phone: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('next_of_kin_phone', null, ['class' => 'form-control','maxlength'=>'11']) !!}
                {!! $errors->first('next_of_kin_phone', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>
 <div class="form-group {{ $errors->has('passenger_count') ? 'has-error' : ''}}">
                {!! Form::label('passenger_count', 'Passenger Count: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">

                    {!! Form::number('passenger_count', $booking['passenger_count'], ['class' => 'form-control','readonly'=>'readonly']) !!}

                {!! $errors->first('passenger_count', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>
            <!-- <div class="form-group {{ $errors->has('luggage_weight') ? 'has-error' : ''}}">
                {!! Form::label('luggage_weight', 'Luggage Weight: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('luggage_weight', null, ['class' => 'form-control','maxlength'=>'11']) !!}
                {!! $errors->first('luggage_weight', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div> -->


            
        <!-- just added -->

        <!-- just added-->

                
                

                @if ($errors->any())
                    <ul class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                @endif


          <hr>  
           
             <h3>Other passengers</h3>
              <!-- laolu added this-->
              
              <!-- laolu's addition stops here-->  
<table border="0" cellspacing="2">
    <tr class="eachField">
        <td style="width:200px;" align="right"> 
            <td>
                <!-- <input type="text" id="current Name" value="" /> -->
            </td>
        </td>
    </tr>
    
    <!-- <tr>
        <td align="right"></td>
        <td>
            <a href="#" id="add" value="Add">Add Passenger</a> | 
            <a href="#" id="del" value="Del" >Remove</a>
        </td>
    </tr> -->
    <tr>
        <td style="height:3px" colspan="2"></td>
    </tr>
    <tr style="background-color: #383838">
        <td></td>
    </tr>
              <?php $i = 0; ?>
              @foreach($booking->passengers as $p)



              <tr class="eachField">
                <td align="right" style="width:100px;">Passenger {{ ++$i + 1 }}&nbsp;&nbsp;&nbsp; </td>
                <td style="width:200px;"> 
                  <input type="hidden" name="psg_id{{ $i }}" value="{{ $p->id }}">
                  <input type="text" value="{{ $p->name }}" name="psg{{ $i }}" id="current Name" class="form-control">
                </td>
                <td style="width:150px;"> 
                  <select class="form-control" required="" id="gender" name="psggender{{ $i }}">
                    <option value=""> -gender- </option>
                    <option value="male" {{ ($p->gender == 'male')?'selected="selected"':'' }} >MALE</option>
                    <option value="female" {{ ($p->gender == 'female')?'selected="selected"':'' }}>FEMALE</option>
                  </select>
                </td>
                
              </tr>

               
              @endforeach
    <tr></tr>
    <tr>
    </tr>
</table>
   
<br><br><br>

<input type="hidden" name="bookedSeats" value="{{$booked_seats}}" id="bookedSeats"/>
<input type="hidden" name="no_of_seats" value="{{$trip->parenttrip->bus->no_of_seats}}" id="no_of_seats"/>
<input type="hidden" name="selSeats" value="{{ $sel_seats }}" id="selSeats" />




<h3>Edit seats</h3>
<hr/>


      <div style="font-size:1.2em;"> Edit seats by clicking the corresponding seat in the layout below:</div>

       <div id=""  class="col-sm-8">
                   <ul id="bus-seat"></ul>
       </div>


   <div class="col-sm-3" id="seat-explain">
     <!-- <ul id="seatDescription"> -->
      <li class=""><label><i class="fa fa-car" aria-hidden="true"></i></label> Available Seat</li>
      <li class=""><label><i class="fa fa-car" aria-hidden="true"></i></label> Booked Seat</li>
      <li class=""><label><i class="fa fa-car" aria-hidden="true"></i></label> Selected Seat</li>
    <!--  </ul> -->
   </div>

   <br>

  

  <!--div style="width:580px;text-align:left;margin:5px">  
    <input type="button" id="btnShowNew" value="Show Selected Seats" /><input type="button" id="btnShow" value="Show All" />            </div-->

              

                    <div class="col-sm-offset-3 col-sm-3">
                    <br><br><br>
                        {!! Form::submit('Update', ['class' => 'btn btn-primary form-control', 'id'=>'submitBtn']) !!}
                    </div>
              


                {!! Form::close() !!}

             </div> <!-- /.portlet-content -->

          </div> <!-- /.portlet -->

        

        </div> <!-- /.col -->

        <div class="col-md-2 col-sm-4">
            <a href="{{ route('cancel-booking', $booking['booking_code']) }}" class="btn btn-block btn-primary">
              Cancel Booking
            </a>
            <!--ul id="myTab" class="nav nav-pills nav-stacked">
            <li class="active">
                {!! Form::open([
                    'method'=>'DELETE',
                    'url' => ['bookings', $booking->id],
                    'style' => 'display:inline'
                ]) !!}
                    {!! Form::button('<i class="fa fa-times"></i> Delete Booking', ['class' => 'btn btn-danger btn-xs', 'escape'=>false,'title'=>'Delete', 'type'=>'submit']) !!}
                {!! Form::close() !!}
            </li>
            <li>
              <a href="{{ url('bookings') }}">
                <i class="fa fa-bars"></i> 
                List Bookings
              </a>
            </li>
            <li class="">
              <a href="{{ url('bookings/create') }}">
                <i class="fa fa-plus"></i> 
                Add Booking
              </a>
            </li>
          </ul-->

        </div>

      </div> <!-- /.row -->  
      <script src="{{ url('bckend/js/libs/jquery-1.10.1.min.js') }}"></script>
    
      
      <script>
      
      var more = 1;
      
      $('#add').click(function () {
        var psgCount = $("#passenger_count").val();
        psgCount = parseInt(psgCount);
        psgCount+=1;
        $("#passenger_count").val(psgCount);

        var noo = $('.eachField').length;
        console.log(noo+1);
    var table = $(this).closest('table');
    if (table.find('input:text').length < 7) {

       table.append('<tr class="eachField"> <td style="width:100px;" align="right">Passenger '+(noo + 1)+'&nbsp;&nbsp;&nbsp; </td/><td style="width:200px;"> <input type="text" class="form-control" id="current Name" name="psg'+noo+'" value="" /></td>&nbsp;&nbsp;&nbsp;<td style="width:100px;"> <select name="psggender'+noo+'" id="gender" required class="form-control"><option value=""> -gender- </option><option value="male">MALE</option><option value="female">FEMALE</option></select></td><td align="left"><input class="form-control age-control" placeholder="age" style="width:60px;" type ="text" name="age'+noo+'" id="age'+noo+'"></td></tr>');  

        more+1;
    }
  /// listen for events after add click

  $('#age'+noo).change(function(){
      calcPassengers();
     
  });

    return false;
});

$('#del').click(function () {
    var psgCount = $("#passenger_count").val();
    psgCount = parseInt(psgCount); 
        psgCount-=1;
      
        $("#passenger_count").val(psgCount);
    var noo = $('.eachField').length
    console.log(noo-1);
    var table = $(this).closest('table');
    if (table.find('input:text').length >= 1) {
        table.find('input:text').last().closest('tr').remove();
    }

    return false;
});

</script> 
<script type="text/javascript">
    $(function () {
    var bookedSeat = $('#bookedSeats').val();
    var chosenSeats = $('#selSeats').val();
    var d = [];
    var s = [];
    var noOfSeats = $('#no_of_seats').val();
    noOfSeats = parseInt(noOfSeats);
        if(bookedSeat != ""){
             d = bookedSeat.split(',').map(function(el){ return +el;});
            //alert(d);
        }
        if(chosenSeats != ""){
             s = chosenSeats.split(',').map(function(el){ return +el;});
            //alert(d);
        }
        
        var diff = $(d).not(s).get();
        
       // alert(diff);
        
        
      var formArray = s;
      

      function displayBusSeat(li, div) {
            
        for (var i = 1; i <= li; i++) {
          $('#bus-seat').append('<li class="li-seat clearfix"></li>');
        }
        for (var i = 0; i <= div; i++) {
          if (diff.indexOf(i) > -1) {
                    $('.li-seat').append('<div class="forbuses"><input type="checkbox" id='+i+' disabled/><label for='+i+'><i class="fa fa-car" aria-hidden="true"></i>'+i+'</label></div>');
          }
          else if (s.indexOf(i) > -1) {
                    $('.li-seat').append('<div class="forbuses"><input type="checkbox" id='+i+' checked/><label for='+i+'><i class="fa fa-car" aria-hidden="true"></i>'+i+'</label></div>');
          }
          else if (i == 0) {
            continue;
          }
          else{
            $('.li-seat').append('<div class="forbuses"><input type="checkbox" id='+i+' /><label for='+i+'><i class="fa fa-car" aria-hidden="true"></i>'+i+'</label></div>');
          }
        }

      }

        displayBusSeat(1, noOfSeats);
        
      $('input[type="checkbox"]').change(function() {
          var newValue = parseInt($(this).attr('id'));
            if (this.checked) {
                formArray.push(newValue);
                console.log(formArray);
            }else {
                    var index = $.inArray(newValue, formArray);
                    formArray.splice(index, 1);
                    //formArray = ary.filter(function(e) { return e !== newValue })
              console.log(formArray);
            }
                $("#selSeats").val(formArray.join(','));
                  checkChosenSeats(formArray);
        });

            function checkChosenSeats(str){
                var passenger_count = $("#passenger_count").val();
                if(str.length == passenger_count){
                  $('#submitBtn').removeClass('disabled');
                  console.log('equal');
                }else{
                   $('#submitBtn').addClass('disabled');
                   console.log('not equal');
                }
            }



  });

    
    </script>


    <style type="text/css">
    li {
      list-style: none;
    }
#seat-explain label {
      display: inline-block;
      position: relative;
      width: 50px;
      text-align: center;
      font-size: 14px;
      font-weight: bold;
      line-height: 1.5rem;
      padding: 10px;
      border-radius: 5px;
      animation-duration: 300ms;
      animation-fill-mode: both;
  }
#seat-explain li:first-of-type label {  
      background: steelblue;
  }
#seat-explain li:nth-of-type(2) label {  
      background: #F42536;
  }
#seat-explain li:last-of-type label {  
      background: #bada55;
  }
  </style>

@endsection