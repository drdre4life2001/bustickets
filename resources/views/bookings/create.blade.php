@extends('layouts.master')

@section('content')
 <script>

  $(document).ready(function(){
    $("#retDate_div").hide();

    $('input:radio[name="is_round_trip"]').on('change',function(){
    if($(this).val() == 1)
    {
      $("#retDate_div").show();
    }
    else
    {
      $("#retDate_div").hide();
    }
});


    var bookedSeat = $('#bookedSeats').val();
    var d = [];
    var noOfSeats = $('#no_of_seats').val();
    noOfSeats = parseInt(noOfSeats);
        if(bookedSeat != ""){
             d = bookedSeat.split(',').map(function(el){ return +el;});
            //alert(d);
        }


       formArray = [];


      function displayBusSeat(li, div) {

        for (var i = 1; i <= li; i++) {
          $('#bus-seat').append('<li class="li-seat clearfix"></li>');
        }
        for (var i = 0; i <= div; i++) {
          if (d.indexOf(i) > -1) {
                    $('.li-seat').append('<div class="forbuses"><input type="checkbox" id='+i+' disabled/><label for='+i+'><i class="fa fa-car" aria-hidden="true"></i>'+i+'</label></div>');
          }
          else if (i == 0) {
            continue;
          }
          else{
            $('.li-seat').append('<div class="forbuses"><input type="checkbox" id='+i+' /><label for='+i+'><i class="fa fa-car" aria-hidden="true"></i>'+i+'</label></div>');
          }
        }

      }

        displayBusSeat(1, noOfSeats);
      $('input[type="checkbox"]').change(function() {
          var newValue = parseInt($(this).attr('id'));
            if (this.checked) {
                formArray.push(newValue);
                console.log(formArray);
            }else {
                    var index = $.inArray(newValue, formArray);
                    formArray.splice(index, 1);
                    //formArray = ary.filter(function(e) { return e !== newValue })
              console.log(formArray);
            }
                $("#selSeats").val(formArray.join(','));
                  checkChosenSeats(formArray);
        });

            function checkChosenSeats(str){
                var passenger_count = $("#passenger_count").val();
                if(str.length == passenger_count){
                  $('#submitBtn').removeClass('disabled');
                  console.log('equal');
                }else{
                   $('#submitBtn').addClass('disabled');
                   console.log('not equal');
                }
            }


  });
  </script>
<link rel="stylesheet" href="{{ url('backend/js/plugins/datepicker/datepicker.css') }}" />

<script src="{{ asset('bckend/js/libs/jquery-1.10.1.min.js') }}"></script>
<!-- <script>
  $(document).ready(function(){
  var tripId = {{$trip->id}}
  var dest = new String("{{ $trip->destpark->name }}");
  dest = dest.substr(0, dest.indexOf('['))
   // get all asociated stops with trip
    $.ajax({
                url: '{{ route("get-trip-stops") }}',
                dataType: 'text',
                type: 'GET',
                contentType: 'application/x-www-form-urlencoded',
                data: {"trip_id":tripId},
                success: function (rslts, textStatus, jQxhr)
                {

                  var selectValues = rslts.split(",");
                    $.each(selectValues, function(key, value) {
                     $('#tripStop')
                     .append($("<option></option>")
                     .attr("value",value)
                     .text(value));
                     });
                     $('#tripStop')
                     .append($("<option></option>")
                     .attr("value",dest)
                     .text(dest));

                },
                error: function (jqXhr,textStatus, errorThrown)
                {
                  // alert("Stops have not been set for this trip")
                }
    });
  });
</script> -->
<script>
function goBack() {
    window.history.back();
}
</script>
   <div class="content-header">
        <h2 class="content-header-title">{{ ($flexi)?'Flexi':'' }} Book a Trip </h2>
        <ol class="breadcrumb">
          <li><a href="{{ url('/') }}">Dashboard</a></li>
          <li><a href="{{ url('bookings') }}">Bookings </a></li>
          <li><a  href="{{ url('quick-book') }}">Quick Book</a></li>
          <li class="active"><a href ="" onclick="goBack()">Back</a> </li>
        </ol>
      </div> <!-- /.content-header -->



      <div class="row">

        <div class="col-md-9 col-sm-7">

          <div class="portlet">

            <div class="portlet-content">

            @if(Session::has('error'))
                <div class="alert alert-danger">
                    <a class="close" data-dismiss="alert" href="#" aria-hidden="true">Ã—</a>
                    <strong>Oh snap!</strong>  {!! session('error') !!}
                  </div>

            @endif

            @if ($errors->any())
                    <ul class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                @endif

                {!! Form::open(['class' => 'form-horizontal']) !!}


            <!-- <div class="form-group {{ $errors->has('trip_stop') ? 'has-error' : ''}}">
                {!! Form::label('trip_stop', 'Trip Stop: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                <select id ="tripStop" name="tripStop" class="form-control">

                </select>
                {!! $errors->first('trip_name', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div> -->


<h3>Choose seats</h3>
<hr/>


      <div style="font-size:1.2em;">
        Choose seats by clicking the corresponding seat in the layout below:
      </div>
       <div id=""  class="col-sm-9">
                   <ul id="bus-seat"></ul>
       </div>


   <div class="col-sm-3" id="seat-explain">
     <!-- <ul id="seatDescription"> -->
      <li class=""><label><i class="fa fa-car" aria-hidden="true"></i></label> Available Seat</li>
      <li class=""><label><i class="fa fa-car" aria-hidden="true"></i></label> Booked Seat</li>
      <li class=""><label><i class="fa fa-car" aria-hidden="true"></i></label> Selected Seat</li>
    <!--  </ul> -->
   </div>

   <hr /><br>

   <br style="clear:both;" />


            @if($trip->round_trip_status)
             <div class="form-group {{ $errors->has('trip_type') ? 'has-error' : ''}}">
                {!! Form::label('is_round_trip', 'Trip Type : ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
             <div class="checkbox">
                <label>{!! Form::radio('is_round_trip', '0', true) !!} One Way</label>
            </div>
            <div class="checkbox">
                <label>{!! Form::radio('is_round_trip', '1') !!} Round Trip</label>
            </div>
                {!! $errors->first('is_round_trip', '<span class="parsley-error-list">:message</span>') !!}
                </div>

            </div>
           @else

            <input type="hidden" name="is_round_trip" value="0" id="is_round_trip">

           @endif

           <hr/>
            <div class="form-group {{ $errors->has('booking_date') ? 'has-error' : ''}}">
                {!! Form::label('booking_date', 'Departure Date: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                <input type="text" name="trip" value="{{ $date }}" class="form-control" disabled>
                {!! $errors->first('booking_date', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>

            <div class="form-group {{ $errors->has('initial_source_park_id') ? 'has-error' : ''}}">
                {!! Form::label('initial_source_park_id', 'Initial Source Park: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                 <select name="initial_source_park_id" class="form-control">
                                <option value="">Choose Park</option>
                                @foreach($all_parks as $park)
                                <option value="{{ $park->id }}" >{{ $park->name }}</option>
                                @endforeach
                    </select>
                {!! $errors->first('initial_source_park_id', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>
<div class="form-group {{ $errors->has('initial_source_park_id') ? 'has-error' : ''}}">
                {!! Form::label('transfer_type', 'Transfer Type: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                 <select name="transfer_type" class="form-control">
                                <option value="">Choose Type</option>
                                <option value="transloading" >Transloading from initial source park</option>
                                <option value="returning" >Returning to initial source park</option>
                    </select>
                {!! $errors->first('transfer_type', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>




            <!-- <div class="form-group {{ $errors->has('departure_time') ? 'has-error' : ''}}">
                {!! Form::label('departure_time', 'Departure Time: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6 input-group bootstrap-timepicker">
                <input id="tp-ex-1" type="text" class="form-control" name="departure_time" value="{{ (!empty($dt))?$dt->departure_time:$trip->parenttrip->departure_time }}">
                    <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>

                {!! $errors->first('departure_time', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div> -->

            <div class="form-group {{ $errors->has('return_date') ? 'has-error' : ''}}" id ="retDate_div">
                {!! Form::label('return_date', 'Return Date: ', ['class' => 'col-sm-3 control-label']) !!}

                <div id="dp-ex-3" class="input-group date col-sm-6" data-auto-close="true"  data-date-format="dd-mm-yyyy" data-date-autoclose="true">
                    <input class="form-control" type="text" name="return_date" value="{{ $date }}">
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                </div>
                {!! $errors->first('return_date', '<span class="parsley-error-list">:message</span>') !!}
                <!-- <span class="help-block">dd-mm-yyyy</span> -->

            </div>


            @if($flexi)
            <div class="form-group {{ $errors->has('contact_name') ? 'has-error' : ''}}">
                {!! Form::label('final_cost', 'Final Price: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('final_cost', $trip->fare, ['class' => 'form-control']) !!}
                {!! $errors->first('final_cost', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>

            @endif




            <!--div class="form-group {{ $errors->has('paid_date') ? 'has-error' : ''}}">
                {!! Form::label('paid_date', 'Paid Date: ', ['class' => 'col-sm-3 control-label']) !!}

                <div class="col-sm-4">

                <div id="dp-ex-3" class="input-group date" data-auto-close="true"  data-date-format="dd-mm-yyyy" data-date-autoclose="true">
                    <input class="form-control" type="text" name="paid_date">
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                </div>
                </div>
            </div-->

            <div class="form-group {{ $errors->has('contact_phone') ? 'has-error' : ''}}">
                {!! Form::label('contact_phone', 'Passenger Phone: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('contact_phone', null, ['class' => 'form-control', 'maxlength'=>'11']) !!}
                    <span> Mobile (0xxxxxxxxxx)</span>
                {!! $errors->first('contact_phone', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>

             <div class="form-group {{ $errors->has('contact_name') ? 'has-error' : ''}}">
                {!! Form::label('contact_name', 'Passenger Name: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('contact_name', null, ['class' => 'form-control']) !!}
                {!! $errors->first('contact_name', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>


            <!-- <div class="form-group {{ $errors->has('contact_email') ? 'has-error' : ''}}">
                {!! Form::label('contact_email', 'Passenger Email: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::email('contact_email', null, ['class' => 'form-control']) !!}
                {!! $errors->first('contact_email', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div> -->

             <div class="form-group {{ $errors->has('contact_address') ? 'has-error' : ''}}">
                {!! Form::label('contact_address', 'Passenger Address: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!!  Form::textarea('contact_address', null, ['class' => 'form-control', 'size' => '7x5'])  !!}
                {!! $errors->first('contact_address', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>

            <div class="form-group {{ $errors->has('contact_address') ? 'has-error' : ''}}">
                {!! Form::label('gender', 'Gender: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                   <select name="gender" id="gender" class="form-control">
                     <option value="male">MALE</option>
                     <option value="female">FEMALE</option>
                   </select>
                {!! $errors->first('gender', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>
            @if($trip->is_intl_trip)

    <div style="display: none" id="is_intl_trip"></div>

            <div class="form-group {{ $errors->has('passport_type') ? 'has-error' : ''}}">
                {!! Form::label('passport_type', 'Passport type: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
             <div class="checkbox">
                <label>{!! Form::radio('passport_type', 'Regular', true) !!} Regular</label>
            </div>
            <div class="checkbox">
                <label>{!! Form::radio('passport_type', 'Virgin passport') !!} Virgin passport</label>
            </div>
            <div class="checkbox">
                <label>{!! Form::radio('passport_type', 'ID card/ No passport') !!} ID card/ No passport</label>
            </div>
                {!! $errors->first('passport_type', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>


            <div class="form-group {{ $errors->has('occupation') ? 'has-error' : ''}}">
                {!! Form::label('occupation', 'Occupation: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('occupation', null, ['class' => 'form-control']) !!}
                    <span> Occupation (eg. Bussiness man)</span>
                {!! $errors->first('occupation', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>





            <div class="form-group {{ $errors->has('initial_source_park_id') ? 'has-error' : ''}}">
                {!! Form::label('nationality', 'Nationality: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">

<select  name="nationality" value="Nigeria" class="form-control">
      <option value="">Choose Type</option>
    <option value="Afghanistan">Afghanistan</option>
    <option value="Albania">Albania</option>
    <option value="Algeria">Algeria</option>
    <option value="American Samoa">American Samoa</option>
    <option value="Andorra">Andorra</option>
    <option value="Angola">Angola</option>
    <option value="Anguilla">Anguilla</option>
    <option value="Antartica">Antarctica</option>
    <option value="Antigua and Barbuda">Antigua and Barbuda</option>
    <option value="Argentina">Argentina</option>
    <option value="Armenia">Armenia</option>
    <option value="Aruba">Aruba</option>
    <option value="Australia">Australia</option>
    <option value="Austria">Austria</option>
    <option value="Azerbaijan">Azerbaijan</option>
    <option value="Bahamas">Bahamas</option>
    <option value="Bahrain">Bahrain</option>
    <option value="Bangladesh">Bangladesh</option>
    <option value="Barbados">Barbados</option>
    <option value="Belarus">Belarus</option>
    <option value="Belgium">Belgium</option>
    <option value="Belize">Belize</option>
    <option value="Benin">Benin</option>
    <option value="Bermuda">Bermuda</option>
    <option value="Bhutan">Bhutan</option>
    <option value="Bolivia">Bolivia</option>
    <option value="Bosnia and Herzegowina">Bosnia and Herzegowina</option>
    <option value="Botswana">Botswana</option>
    <option value="Bouvet Island">Bouvet Island</option>
    <option value="Brazil">Brazil</option>
    <option value="British Indian Ocean Territory">British Indian Ocean Territory</option>
    <option value="Brunei Darussalam">Brunei Darussalam</option>
    <option value="Bulgaria">Bulgaria</option>
    <option value="Burkina Faso">Burkina Faso</option>
    <option value="Burundi">Burundi</option>
    <option value="Cambodia">Cambodia</option>
    <option value="Cameroon">Cameroon</option>
    <option value="Canada">Canada</option>
    <option value="Cape Verde">Cape Verde</option>
    <option value="Cayman Islands">Cayman Islands</option>
    <option value="Central African Republic">Central African Republic</option>
    <option value="Chad">Chad</option>
    <option value="Chile">Chile</option>
    <option value="China">China</option>
    <option value="Christmas Island">Christmas Island</option>
    <option value="Cocos Islands">Cocos (Keeling) Islands</option>
    <option value="Colombia">Colombia</option>
    <option value="Comoros">Comoros</option>
    <option value="Congo">Congo</option>
    <option value="Congo">Congo, the Democratic Republic of the</option>
    <option value="Cook Islands">Cook Islands</option>
    <option value="Costa Rica">Costa Rica</option>
    <option value="Cota D'Ivoire">Cote d'Ivoire</option>
    <option value="Croatia">Croatia (Hrvatska)</option>
    <option value="Cuba">Cuba</option>
    <option value="Cyprus">Cyprus</option>
    <option value="Czech Republic">Czech Republic</option>
    <option value="Denmark">Denmark</option>
    <option value="Djibouti">Djibouti</option>
    <option value="Dominica">Dominica</option>
    <option value="Dominican Republic">Dominican Republic</option>
    <option value="East Timor">East Timor</option>
    <option value="Ecuador">Ecuador</option>
    <option value="Egypt">Egypt</option>
    <option value="El Salvador">El Salvador</option>
    <option value="Equatorial Guinea">Equatorial Guinea</option>
    <option value="Eritrea">Eritrea</option>
    <option value="Estonia">Estonia</option>
    <option value="Ethiopia">Ethiopia</option>
    <option value="Falkland Islands">Falkland Islands (Malvinas)</option>
    <option value="Faroe Islands">Faroe Islands</option>
    <option value="Fiji">Fiji</option>
    <option value="Finland">Finland</option>
    <option value="France">France</option>
    <option value="France Metropolitan">France, Metropolitan</option>
    <option value="French Guiana">French Guiana</option>
    <option value="French Polynesia">French Polynesia</option>
    <option value="French Southern Territories">French Southern Territories</option>
    <option value="Gabon">Gabon</option>
    <option value="Gambia">Gambia</option>
    <option value="Georgia">Georgia</option>
    <option value="Germany">Germany</option>
    <option value="Ghana">Ghana</option>
    <option value="Gibraltar">Gibraltar</option>
    <option value="Greece">Greece</option>
    <option value="Greenland">Greenland</option>
    <option value="Grenada">Grenada</option>
    <option value="Guadeloupe">Guadeloupe</option>
    <option value="Guam">Guam</option>
    <option value="Guatemala">Guatemala</option>
    <option value="Guinea">Guinea</option>
    <option value="Guinea-Bissau">Guinea-Bissau</option>
    <option value="Guyana">Guyana</option>
    <option value="Haiti">Haiti</option>
    <option value="Heard and McDonald Islands">Heard and Mc Donald Islands</option>
    <option value="Holy See">Holy See (Vatican City State)</option>
    <option value="Honduras">Honduras</option>
    <option value="Hong Kong">Hong Kong</option>
    <option value="Hungary">Hungary</option>
    <option value="Iceland">Iceland</option>
    <option value="India">India</option>
    <option value="Indonesia">Indonesia</option>
    <option value="Iran">Iran (Islamic Republic of)</option>
    <option value="Iraq">Iraq</option>
    <option value="Ireland">Ireland</option>
    <option value="Israel">Israel</option>
    <option value="Italy">Italy</option>
    <option value="Jamaica">Jamaica</option>
    <option value="Japan">Japan</option>
    <option value="Jordan">Jordan</option>
    <option value="Kazakhstan">Kazakhstan</option>
    <option value="Kenya">Kenya</option>
    <option value="Kiribati">Kiribati</option>
    <option value="Democratic People's Republic of Korea">Korea, Democratic People's Republic of</option>
    <option value="Korea">Korea, Republic of</option>
    <option value="Kuwait">Kuwait</option>
    <option value="Kyrgyzstan">Kyrgyzstan</option>
    <option value="Lao">Lao People's Democratic Republic</option>
    <option value="Latvia">Latvia</option>
    <option value="Lebanon" selected>Lebanon</option>
    <option value="Lesotho">Lesotho</option>
    <option value="Liberia">Liberia</option>
    <option value="Libyan Arab Jamahiriya">Libyan Arab Jamahiriya</option>
    <option value="Liechtenstein">Liechtenstein</option>
    <option value="Lithuania">Lithuania</option>
    <option value="Luxembourg">Luxembourg</option>
    <option value="Macau">Macau</option>
    <option value="Macedonia">Macedonia, The Former Yugoslav Republic of</option>
    <option value="Madagascar">Madagascar</option>
    <option value="Malawi">Malawi</option>
    <option value="Malaysia">Malaysia</option>
    <option value="Maldives">Maldives</option>
    <option value="Mali">Mali</option>
    <option value="Malta">Malta</option>
    <option value="Marshall Islands">Marshall Islands</option>
    <option value="Martinique">Martinique</option>
    <option value="Mauritania">Mauritania</option>
    <option value="Mauritius">Mauritius</option>
    <option value="Mayotte">Mayotte</option>
    <option value="Mexico">Mexico</option>
    <option value="Micronesia">Micronesia, Federated States of</option>
    <option value="Moldova">Moldova, Republic of</option>
    <option value="Monaco">Monaco</option>
    <option value="Mongolia">Mongolia</option>
    <option value="Montserrat">Montserrat</option>
    <option value="Morocco">Morocco</option>
    <option value="Mozambique">Mozambique</option>
    <option value="Myanmar">Myanmar</option>
    <option value="Namibia">Namibia</option>
    <option value="Nauru">Nauru</option>
    <option value="Nepal">Nepal</option>
    <option value="Netherlands">Netherlands</option>
    <option value="Netherlands Antilles">Netherlands Antilles</option>
    <option value="New Caledonia">New Caledonia</option>
    <option value="New Zealand">New Zealand</option>
    <option value="Nicaragua">Nicaragua</option>
    <option value="Niger">Niger</option>
    <option value="Nigeria">Nigeria</option>
    <option value="Niue">Niue</option>
    <option value="Norfolk Island">Norfolk Island</option>
    <option value="Northern Mariana Islands">Northern Mariana Islands</option>
    <option value="Norway">Norway</option>
    <option value="Oman">Oman</option>
    <option value="Pakistan">Pakistan</option>
    <option value="Palau">Palau</option>
    <option value="Panama">Panama</option>
    <option value="Papua New Guinea">Papua New Guinea</option>
    <option value="Paraguay">Paraguay</option>
    <option value="Peru">Peru</option>
    <option value="Philippines">Philippines</option>
    <option value="Pitcairn">Pitcairn</option>
    <option value="Poland">Poland</option>
    <option value="Portugal">Portugal</option>
    <option value="Puerto Rico">Puerto Rico</option>
    <option value="Qatar">Qatar</option>
    <option value="Reunion">Reunion</option>
    <option value="Romania">Romania</option>
    <option value="Russia">Russian Federation</option>
    <option value="Rwanda">Rwanda</option>
    <option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option>
    <option value="Saint LUCIA">Saint LUCIA</option>
    <option value="Saint Vincent">Saint Vincent and the Grenadines</option>
    <option value="Samoa">Samoa</option>
    <option value="San Marino">San Marino</option>
    <option value="Sao Tome and Principe">Sao Tome and Principe</option>
    <option value="Saudi Arabia">Saudi Arabia</option>
    <option value="Senegal">Senegal</option>
    <option value="Seychelles">Seychelles</option>
    <option value="Sierra">Sierra Leone</option>
    <option value="Singapore">Singapore</option>
    <option value="Slovakia">Slovakia (Slovak Republic)</option>
    <option value="Slovenia">Slovenia</option>
    <option value="Solomon Islands">Solomon Islands</option>
    <option value="Somalia">Somalia</option>
    <option value="South Africa">South Africa</option>
    <option value="South Georgia">South Georgia and the South Sandwich Islands</option>
    <option value="Span">Spain</option>
    <option value="SriLanka">Sri Lanka</option>
    <option value="St. Helena">St. Helena</option>
    <option value="St. Pierre and Miguelon">St. Pierre and Miquelon</option>
    <option value="Sudan">Sudan</option>
    <option value="Suriname">Suriname</option>
    <option value="Svalbard">Svalbard and Jan Mayen Islands</option>
    <option value="Swaziland">Swaziland</option>
    <option value="Sweden">Sweden</option>
    <option value="Switzerland">Switzerland</option>
    <option value="Syria">Syrian Arab Republic</option>
    <option value="Taiwan">Taiwan, Province of China</option>
    <option value="Tajikistan">Tajikistan</option>
    <option value="Tanzania">Tanzania, United Republic of</option>
    <option value="Thailand">Thailand</option>
    <option value="Togo">Togo</option>
    <option value="Tokelau">Tokelau</option>
    <option value="Tonga">Tonga</option>
    <option value="Trinidad and Tobago">Trinidad and Tobago</option>
    <option value="Tunisia">Tunisia</option>
    <option value="Turkey">Turkey</option>
    <option value="Turkmenistan">Turkmenistan</option>
    <option value="Turks and Caicos">Turks and Caicos Islands</option>
    <option value="Tuvalu">Tuvalu</option>
    <option value="Uganda">Uganda</option>
    <option value="Ukraine">Ukraine</option>
    <option value="United Arab Emirates">United Arab Emirates</option>
    <option value="United Kingdom">United Kingdom</option>
    <option value="United States">United States</option>
    <option value="United States Minor Outlying Islands">United States Minor Outlying Islands</option>
    <option value="Uruguay">Uruguay</option>
    <option value="Uzbekistan">Uzbekistan</option>
    <option value="Vanuatu">Vanuatu</option>
    <option value="Venezuela">Venezuela</option>
    <option value="Vietnam">Viet Nam</option>
    <option value="Virgin Islands (British)">Virgin Islands (British)</option>
    <option value="Virgin Islands (U.S)">Virgin Islands (U.S.)</option>
    <option value="Wallis and Futana Islands">Wallis and Futuna Islands</option>
    <option value="Western Sahara">Western Sahara</option>
    <option value="Yemen">Yemen</option>
    <option value="Yugoslavia">Yugoslavia</option>
    <option value="Zambia">Zambia</option>
    <option value="Zimbabwe">Zimbabwe</option>
</select>

                {!! $errors->first('transfer_type', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>


            <div class="form-group {{ $errors->has('passport_no') ? 'has-error' : ''}}">
                {!! Form::label('passport_no', 'Passport Number: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('passport_no', null, ['class' => 'form-control']) !!}
                    <span> </span>
                {!! $errors->first('passport_no', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('passport_date_issued') ? 'has-error' : ''}}">
                {!! Form::label('passport_date_issued', 'Passport Date Issued: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6 input-group date" data-auto-close="true"  data-date-format="dd-mm-yyyy" data-date-autoclose="true">
                    {!! Form::text('passport_date_issued', null, ['class' => 'form-control']) !!}
                         <span class="input-group-addon"><i class="fa fa-calendar"></i></span>

                {!! $errors->first('passport_date_issued', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>




            <div class="form-group {{ $errors->has('passport_place_issued') ? 'has-error' : ''}}">
                {!! Form::label('passport_place_issued', 'Passport Place Issued: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('passport_place_issued', null, ['class' => 'form-control']) !!}
                    <span> PassPort Number</span>
                {!! $errors->first('passport_place_issued', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('passport_expiry_date') ? 'has-error' : ''}}">
                {!! Form::label('passport_expiry_date', 'Passport Expiry Date: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('passport_expiry_date', null, ['class' => 'form-control']) !!}
                    <span> Expiry Date</span>
                {!! $errors->first('passport_expiry_date', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>





            @endif

             <div class="form-group {{ $errors->has('next_of_kin') ? 'has-error' : ''}}">
                {!! Form::label('next_of_kin', 'Next Of Kin Name: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('next_of_kin', null, ['class' => 'form-control', 'required'=>'required']) !!}
                {!! $errors->first('next_of_kin', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('next_of_kin_phone') ? 'has-error' : ''}}">
                {!! Form::label('next_of_kin_phone', 'Next Of Kin Phone: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('next_of_kin_phone', null, ['class' => 'form-control','maxlength'=>'11', 'required'=>'required']) !!} <span> Mobile (0xxxxxxxxxx)</span>
                {!! $errors->first('next_of_kin_phone', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>

            @if($track_luggage)
            <div class="form-group {{ $errors->has('passenger_count') ? 'has-error' : ''}}">
                {!! Form::label('luggage_weight', 'Luggage Weight (in kg): ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('luggage_weight', null, ['class' => 'form-control', 'id'=>'luggWeightFld', 'onkeyup'=>'calcFinalFare()']) !!}
                {!! $errors->first('luggage_weight', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>
            @endif

            <!-- <div class="form-group {{ $errors->has('payment_method_id') ? 'has-error' : ''}}">
                {!! Form::label('payment_method_id', 'Payment Method: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::select('payment_method_id', $payment_methods,null, ['class' => 'form-control']) !!}
                {!! $errors->first('payment_method_id', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div> -->

            <div class="form-group {{ $errors->has('passenger_count') ? 'has-error' : ''}}">
                {!! Form::label('passenger_count', 'Passenger Count: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::number('passenger_count', 1, ['class' => 'form-control','readonly'=>'readonly']) !!}
                {!! $errors->first('passenger_count', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>




             <hr>

             <h3>Other passengers</h3>
<div class="table-responsive">
    <table border="0" cellspacing="2" class="table table-bordered table-striped table-highlight">
       <thead>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
            </thead>
            <tbody>

    <tr class="eachField">
        <td style="width:200px;" align="right">
            <td>
                <!-- <input type="text" id="current Name" value="" /> -->
            </td>
        </td>
    </tr>

    <tr>
        <td ></td>
        <td ><a href="#" id="add" value="Add">Add Passenger</a> </td>
        <td ><a href="#" id="del" value="Del" >Remove</a></td>
        <td></td>
    </tr>

    </tbody>
</table>

</div>










<br><br>
<br><br><br>

</div>
        </div>
<div>

            <input type="hidden" name="bookedSeats" value="{{$booked_seats}}" id="bookedSeats"/>
            <input type="hidden" name="no_of_seats" value="{{$trip->parenttrip->bus->no_of_seats}}" id="no_of_seats"/>
            <input type="hidden" name="selSeats" value="" id="selSeats" />

</div>


  <!--<script>
$('#popUpWarning').hide();
 $('#submitBtn').click(function(){
    //
    $('#popUpWarning').show();
 });

</script>

  <div id="popUpWarning" class="alert alert-danger" role="alert">
    <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
    <span class="sr-only">Warning:</span>
    <b style="font-size: 15px;"> You should see a pop-up now,</b> if you don't then your browser blocked it. Disable pop-up blocking in your browser for this page.
  </div> -->
<br style="clear:both;" /><br/><br/>
                <div class="form-group">

                    <div class="col-sm-offset-3 col-sm-3">
                        {!! Form::submit('Book', ['id'=>'submitBtn',  'class' => 'disabled btn btn-primary form-control']) !!}
                    </div>
                </div>
                {!! Form::close() !!}


             </div> <!-- /.portlet-content -->

          </div> <!-- /.portlet -->



        </div> <!-- /.col -->

        <div class="col-md-3 col-sm-5">

           <div class="portlet">

            <div class="portlet-header">

              <h3>
                <i class="fa fa-reorder"></i>
                Trip Details
              </h3>

            </div> <!-- /.portlet-header -->

            <div class="portlet-content">

              <dl>
                <dt>From</dt>
                <dd>{{ $trip->sourcepark->name }}</dd>
                <dt>To</dt>
                <dd>{{ $trip->destpark->name }}</dd>
               <!--  <dt>Departure Time</dt> -->
                <!-- <dd> {{ $trip->parenttrip->departure_time }} </dd> -->

                <dt>Bus</dt>
                <dd> {{ (!empty($dt))?$dt->bus->number_plate:$trip->parenttrip->bus->number_plate }} </dd>

                <dt>Fare</dt>
                <dd> &#8358; {{ number_format($trip->fare) }} </dd>

                @if($trip->is_intl_trip)
                <dd id ="vp"> &#8358; {{ number_format($trip->virgin_passport_fare) }} (Virgin Passport)</dd>
                <dd id ="np"> &#8358; {{ number_format($trip->no_passport_fare) }} (No Passport)</dd>
                <dd id="rtvp"> &#8358; {{ number_format($trip->round_trip_virgin_passport_fare) }} (Round Trip(Virgin Passport Fare))</dd>
                <dd id="rtnp"> &#8358; {{ number_format($trip->round_trip_no_passport_fare) }} (Round Trip(No Passport Fare))</dd>
                @endif



                @if($trip->round_trip_status)

                  <dt>Round Trip Fare</dt>
                  <dd> &#8358; {{ number_format($trip->round_trip_fare) }} </dd>

                @endif

              </dl>



            </div> <!-- /.portlet-content -->

          </div>

        </div>

      </div> <!-- /.row -->

 <script src="{{ url('bckend/js/libs/jquery-1.10.1.min.js') }}"></script>


      <script>

      var more = 1;
//       function checkChosenSeats(){
//                var passenger_count = $("#passenger_count").val();
//                if(str.length == passenger_count){
//                  $('#submitBtn').removeClass('disabled');
//                  console.log('equal');
//                }else{
//                   $('#submitBtn').addClass('disabled');
//                   console.log('not equal');
//                }
//            }


      $('#add').click(function () {

        var psgCount = $("#passenger_count").val();
        psgCount = parseInt(psgCount);
        psgCount+=1;
        $("#passenger_count").val(psgCount);

        var noo = $('.eachField').length;
        console.log(noo+1);
        var table = $(this).closest('table');
      //  if (table.find('input:text').length < 7) {

        if ($('#is_intl_trip').length > 0) {
                var normal_table = '<tr class="eachField">'
+'<td>Passenger '+(noo + 1)+' &nbsp;&nbsp;&nbsp; </td/>'
+'<td> <input type="text" class="form-control" id="current Name" placeholder="name" name="psg'+noo+'" value="" /> <br/> <input type="text" name="occupation'+noo+'" class="form-control" placeholder="occupation" > <br/> <input type="text" name="passport_number'+noo+'" class="form-control"  placeholder="passport number"> <br/> <input type="text" name="passport_expiry_date'+noo+'" class="form-control" placeholder="passport expiry"></td>'+
'<td>'+
'<select name="psggender'+noo+'" id="gender" required class="form-control"><option value=""> -gender- </option><option value="male">MALE</option><option value="female">FEMALE</option></select> <br/> <input type="text" name="nationality'+noo+'" class="form-control"  placeholder="nationality"></td>'
+'<td><input class="form-control age-control" value="30" placeholder="age" style="width:60px;" type="number" name="age'+noo+'" id="age'+noo+'"> <br/> <input type="text" name="passport_date_issued'+noo+'" class="form-control"  placeholder="passport date issued"> <br/> <input type="text" name="passport_place_issued'+noo+'" class="form-control" placeholder="passport place issued"></td></tr>';

          }else{
            var normal_table = '<tr class="eachField">'
+'<td>Passenger '+(noo + 1)+' &nbsp;&nbsp;&nbsp; </td/>'
+'<td> <input type="text" class="form-control" id="current Name" placeholder="name" name="psg'+noo+'" value="" /></td>'+
'<td>'+
'<select name="psggender'+noo+'" id="gender" required class="form-control"><option value=""> -gender- </option><option value="male">MALE</option><option value="female">FEMALE</option></select> </td>'
+'<td><input class="form-control age-control" value="30" placeholder="age" style="width:60px;" type="number" name="age'+noo+'" id="age'+noo+'"></td></tr>';

          }



       table.append(normal_table);

        more+1;

            if(formArray.length ==  psgCount){
               $('#submitBtn').removeClass('disabled');
            }else{
                   $('#submitBtn').addClass('disabled');
            }
    //}
  /// listen for events after add click

  @if (!empty($excludePassenger))

    $('#age'+noo).change(function(){
        calcPassengers();

        var psgCount = $("#passenger_count").val();
          if(formArray.length ==  psgCount){
               $('#submitBtn').removeClass('disabled');
            }else{
                   $('#submitBtn').addClass('disabled');
            }

    });

  @endif

    return false;
});

$('#del').click(function () {
    var psgCount = $("#passenger_count").val();
    psgCount = parseInt(psgCount);
    if (psgCount > 1) {
        psgCount-=1;

    }

    $("#passenger_count").val(psgCount);
    var noo = $('.eachField').length
    console.log(noo-1);
    var table = $(this).closest('table');
    if (table.find('input:text').length >= 1) {
        table.find('input:text').last().closest('tr').remove();
    }

    var psgCount = $("#passenger_count").val();
          if(formArray.length ==  psgCount){
               $('#submitBtn').removeClass('disabled');
            }else{
                   $('#submitBtn').addClass('disabled');
            }

    return false;
});


   function calcPassengers(){

       // var psgCount = $("#passenger_count").val();
        var psgCount = 1;
        var cost = {{ $trip->fare }};
        var age_one = {{$excludePassenger->age_one or "null"}};

        //var excludeRange;
        var totalFare = cost;


        $( ".age-control" ).each(function( index ) {

            var age = parseInt($(this).val());

            if (age_one != null) {
              var age_two = {{$excludePassenger->age_two or 'null' }}
              if(age >= age_one && age <= age_two) {
                //psgCount = psgCount <= 1?1:psgCount-1;
              }else{
                  psgCount += 1;
              }
            }

            // //If age is greater than 3, then passenger will be added in passenger count
            // //else if is below 3, passenger is considered a baby and wont be added to count
            // // if(age > infant_range)
            // //   psgCount += 1;

            // //Calculate Discount for infants between the ages 3 and 9
            // // if(age > 3 && age <= 9)
            // //   totalFare += (cost * 0.9);
            // // else if(age > 9)
            // //   totalFare += cost;


            // console.log(totalFare);

        });

        $("#passenger_count").val(psgCount);
        // $("#totalFare").html(totalFare);

    }

</script>

    <script>
      $(document).ready(function(){
    // this part does the auto remember of customer details
    $("#contact_phone").on('blur', function()
   {
      var phone =  $("#contact_phone").val();
      if(phone !="")
      {
           // use phone value to perform ajax request
            $.ajax({
                    url: '{{ route("get-cust-details") }}',
                    dataType: 'text',
                    type: 'GET',
                    contentType: 'application/x-www-form-urlencoded',
                    data: "ph="+phone,
                    success: function (rslts, textStatus, jQxhr)
                    {

                      if(rslts != ""){
                          var rsltsArr = rslts.split("|");
                          // DISPLAY THOSE RESULTS
                          $("#contact_name").val(rsltsArr[0]);
                          $("#contact_email").val(rsltsArr[1]);
                          $("#contact_address").val(rsltsArr[2]);
                          if(rsltsArr[3] == "male")
                          {
                            $("#gender option:eq(0)").attr("selected", "selected");
                          }
                          else
                          {
                            $("#gender option:eq(1)").attr("selected", "selected");
                          }
                          $("#next_of_kin").val(rsltsArr[4]);
                          $("#next_of_kin_phone").val(rsltsArr[5]);
                          if ($("#passport_no" ).length )
                          {
                            $("#passport_no").val(rsltsArr[6]);
                          }
                          if ($("#occupation" ).length )
                          {
                             $("#occupation").val(rsltsArr[7]);
                          }

                          calcPassengers();
                      }

                    },
                    error: function (jqXhr,textStatus, errorThrown)
                    {
                        console.log(errorThrown);
                        $("#contact_name").val("");
                        $("#contact_email").val("");
                        $("#contact_address").val("");
                         $("#next_of_kin").val("");
                        $("#next_of_kin_phone").val("");
                    }
      });
    }
  });



});

    </script>

<script>
  $(document).ready(function(){
    $('input:radio[name="is_round_trip"]').on('change',function(){
        calcFinalFare();
    });

    $('input:radio[name="passport_type"]').on('change',function(){
        calcFinalFare();
    });

  });


  function calcFinalFare(){

     var roundtrip = $('input[name=is_round_trip]:checked').val();
     var passType = $('input[name=passport_type]:checked').val();
     var passenger_count = $("#passenger_count").val();

     // console.log(roundtrip);
     // console.log(passType);
     var luggage_cost = 0;

     @if($track_luggage)
        var totalLuggLimit = passenger_count * {{ $luggage_limit }};
        var luggweight = $("#luggWeightFld").val();
        luggage_cost = (luggweight - totalLuggLimit) * {{ $cpk }};
        if(luggage_cost < 0)
            luggage_cost  = 0;




     @endif

     if(roundtrip == 1){

        if(passType == 'Virgin passport')
            $("#fareDisp").html(currency_format(({{ $trip->round_trip_virgin_passport_fare }} * passenger_count)+ luggage_cost));
        else if(passType == 'ID card/ No passport')
            $("#fareDisp").html(currency_format(({{ $trip->round_trip_no_passport_fare }} * passenger_count)+ luggage_cost));
        else
            $("#fareDisp").html(currency_format(({{ $trip->round_trip_fare }} * passenger_count)+ luggage_cost).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,"));

     }else{

         if(passType == 'Virgin passport')
            $("#fareDisp").html(currency_format(({{ $trip->virgin_passport_fare }} * passenger_count)+ luggage_cost));
        else if(passType == 'ID card/ No passport')
            $("#fareDisp").html(currency_format(({{ $trip->no_passport_fare }} * passenger_count)+ luggage_cost));
        else
            $("#fareDisp").html(currency_format(({{ $trip->fare }} * passenger_count)+ luggage_cost));

     }


  }


  </script>

  <style type="text/css">
    li {
      list-style: none;
    }
#seat-explain label {
      display: inline-block;
      position: relative;
      width: 50px;
      text-align: center;
      font-size: 14px;
      font-weight: bold;
      line-height: 1.5rem;
      padding: 10px;
      border-radius: 5px;
      animation-duration: 300ms;
      animation-fill-mode: both;
  }
#seat-explain li:first-of-type label {
      background: steelblue;
  }
#seat-explain li:nth-of-type(2) label {
      background: #F42536;
  }
#seat-explain li:last-of-type label {
      background: #bada55;
  }
  </style>

@endsection
