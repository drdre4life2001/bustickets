@extends('layouts.master')

@section('content')

    <div class="content-header">
        <h2 class="content-header-title">{{ $page_title }} </h2>
        <ol class="breadcrumb">
          <li><a href="{{ url('/') }}">Dashboard</a></li>
          <li class="active">{{ $page_title }} </li>
          <li><a  href="{{ url('quick-book') }}">Quick Book</a></li>
          <li><a  href="{{ url('trips/search') }}">Book another Ticket</a></li>
          
        </ol>




        <div style="margin-top: -45px;">

          <div class="btn-group pull-right">
              <div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
                  <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
                  <span></span> <b class="caret"></b>
              </div>
          </div>
          <h4 class="heading-inline pull-right">
          &nbsp;&nbsp;<small>Choose date range</small>
          &nbsp;&nbsp;</h4>
      </div>

      <br><br>


      </div> <!-- /.content-header -->
    
      

      <div class="row">

        <div class="col-md-12 col-sm-12">


        <form class="navbar-form navbar-left" action="/passenger-bookings/view" method="post" role="search">
         <div class="row">
  <div class="col-lg-6 text-center">
    <div class="input-group col-md-12">
      <input type="text" class="form-control" name="phone" placeholder="Enter phone number">
      <span class="input-group-btn">
        <button class="btn btn-default" type="submit">Go!</button>
      </span>
    </div><!-- /input-group -->
  </div><!-- /.col-lg-6 -->
</div><!-- /.row -->
        </form>



          <div class="portlet">
          
           @if(Session::has('flash_message'))
              <div class="alert alert-success">
              <a class="close" data-dismiss="alert" href="#" aria-hidden="true">×</a>
              <strong>Well done!</strong> {!! session('flash_message') !!}
            </div>

            @endif

            <div class="portlet-content">  

              

              <div class="table-responsive">

              <table 
                class="table table-striped table-bordered table-hover table-highlight table-checkable" 
                data-provide="datatable" 
                data-info="true"
                data-search="true"
                data-length-change="true"
                data-paginate="true"
       
              >
                  <thead>
                    <tr>
                      <th>Booking code</th>
                      <th>From</th>
                      <th>To</th>
                      <th style="width:200px;" >Travel Details</th>
                      <th>Passenger Count</th>
                      <th>Final Cost</th>
                      <th>Contact Person</th>
                      <th>Contact Phone</th>
                      <th>Status</th>
                      <th>Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                  <b>Number of Bookings:</b> <br><br>
                    {{-- */$x=0;/* --}}
                    @foreach($bookings as $item)
                      
                        <tr>
                            <td>{{ $item['booking_code'] }}</td>
                            <td>{{ $item['trip']['sourcepark']['name'] }}</td>
                            <td>{{ $item['trip']['destpark']['name'] }}</td>
                            <td>
                            <b>Date:</b> {{ date('D, d/m/Y', strtotime($item['date'])) }}<br>
                            <b>Bus No:</b>  {{$item['daily_trip']['bus']['number_plate'] }}<br>
                            <b>Seat(s):</b> @foreach($item['seats'] as $s){{ $s['seat_no'] }} &nbsp; @endforeach
                            </td>
                            <td>{{ $item['passenger_count']}}</td>
                            <td>&#8358;{{ number_format($item['final_cost']) }} @if($item['is_flexi']) <strong>(Flexi)</strong> @endif</td>
                            <td>{{ $item['contact_name'] }}</td>
                            <td>{{ $item['contact_phone'] }}</td>
                            <td>{{ $item['status']}} </td>
                            <td>
                                <a href="{{ url('bookings/' . $item['id'] ) }}" title="View">
                                    <button type="submit" class="btn btn-primary"><i class="fa fa-eye"></i></button>
                                </a> 

                                

                               @if( $item['status'] == 'PAID' || $item['status'] == 'PENDING') 
                               <a href="{{ url('bookings/' . $item['id'] . '/edit') }}" title="Edit Passenger Details">
                                    <button type="submit" class="btn btn-default"><i class="fa fa-edit"></i></button>
                                </a> 
                                <br/>
                                <a type="button" class="btn btn-success assignDriver" title="Transload" href="#" data-toggle="modal" data-target="#transLoadModal" onclick="transload({{ $item['id'] }});" > <i class="fa fa-bus"></i></a> 
                                 <a type="button" class="btn btn-warning" title="Add Extra Luggage" href="#" data-toggle="modal" data-target="#lugggageModal{{ $item['id'] }}" > <i class="fa fa-suitcase"></i></a> 

                               @endif 
                              
                                <!--

                                {!! Form::open([
                                    'method'=>'DELETE',
                                    'url' => ['bookings', $item['id']],
                                    'style' => 'display:inline'
                                ]) !!}
                                    {!! Form::button('<i class="fa fa-times"></i>', ['class' => 'btn btn-danger btn-xs', 'escape'=>false,'title'=>'Delete', 'type'=>'submit', 'onclick'=>'ConfirmDelete()']) !!}
                                {!! Form::close() !!}
                                -->
                            </td>
                        </tr>


<div class = "modal fade" id="lugggageModal{{ $item['id'] }}" tabindex = "-1" role = "dialog" aria-labelledby = "myModalLabel" aria-hidden = "true">
   <div class = "modal-dialog">
      <div class = "modal-content">
         <div class = "modal-header">
            <button type = "button" class = "close" data-dismiss = "modal" aria-hidden = "true">
                  &times;
            </button>
            <h4 class = "modal-title" id = "myModalLabel">
               Add Extra Luggage
            </h4>
         </div>
         <form action="{{ route('add-extra-luggage') }}" method="post" id="bookingNotes">
         <div class = "modal-body" id="contentArea">
              <input type="hidden" id="SubBtn" name="_token" value="<?php echo csrf_token(); ?>">
          
          <input type="hidden" name="booking_id" value="{{ $item['id'] }}">
            
             <div class="form-group {{ $errors->has('luggage_weight') ? 'has-error' : ''}}">
                {!! Form::label('luggage_weight', 'Luggage Weight: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    <input type="text" name="luggage_weight" class="form-control" required />
                {!! $errors->first('luggage_weight', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>

            <br style="clear:both;" />

             <div class="form-group {{ $errors->has('amount') ? 'has-error' : ''}}">
                {!! Form::label('amount', 'Amount: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    <input type="text" name="amount" class="form-control" required />
                {!! $errors->first('amount', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>

            <br style="clear:both;" />


         </div>
         <div class = "modal-footer">
            <button type = "button" class = "btn btn-default" data-dismiss = "modal">
               Close
            </button>
            <button type = "submit" id="NoteSub" class = "btn btn-primary">
               Submit changes
            </button>
         </form>

         </div>
      </div><!-- /.modal-content -->
   </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


                    @endforeach
                  </tbody>

                </table>
              </div> <!-- /.table-responsive -->
              

            </div> <!-- /.portlet-content -->

          </div> <!-- /.portlet -->

        

        </div> <!-- /.col -->

      

      </div> <!-- /.row -->   




      <div class = "modal fade" id="transLoadModal" tabindex = "-1" role = "dialog" aria-labelledby = "myModalLabel" aria-hidden = "true">
   <div class = "modal-dialog">
      <div class = "modal-content">
         <div class = "modal-header">
            <button type = "button" class = "close" data-dismiss = "modal" aria-hidden = "true">
                  &times;
            </button>
            <h4 class = "modal-title" id = "myModalLabel">
               Transload passengers to another Bus
            </h4>
         </div>
         
        
         <div class = "modal-body" id="transloadContentArea">

         </div>
              
      </div><!-- /.modal-content -->
   </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

 


<script type="text/javascript">


    function transload(booking_id){

        $("#transloadContentArea").html("Please wait....");

        $.ajax({
                url: '{{ url("transload") }}',
                dataType: 'text',
                type: 'GET',
                contentType: 'application/x-www-form-urlencoded',
                data: {"booking_id":booking_id},
                success: function (response)
                {
                  $("#transloadContentArea").html(response);
                     
                },
                error: function (jqXhr,textStatus, errorThrown)
                {
                   
                } 
          });

    }


function updateUrl(name, value){
            window.location.search = jQuery.query.set(name, value);

            return false;
       } 


$(function() {

    function cb(start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    }
    cb(moment("{{ $end_date }}"), moment("{{ $end_date }}"));

    $('#reportrange').daterangepicker({
        ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cb);

    $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
        console.log(picker.startDate.format('YYYY-MM-DD'));
        console.log(picker.endDate.format('YYYY-MM-DD'));
        
        updateUrl('daterange', picker.startDate.format('YYYY-MM-DD')+' '+picker.endDate.format('YYYY-MM-DD'));
    });

});
</script>


@endsection
