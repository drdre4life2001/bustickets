        <div class="alert alert-success collapse" id="success-notif">
            Passenger has been transloaded to the new bus.
        </div>

        <div class="alert alert-danger collapse" id="error-notif">
            
        </div>

        <form class="form-horizontal" action="{{ url('transload') }}" method="POST" id="transloadForm{{ $booking_id }}">
         <div class = "modal-body" id="TranscontentArea">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <input type="hidden"  name="booking_id" value="{{ $booking_id }}">
              
              <div class="form-group {{ $errors->has('default_driver_id') ? 'has-error' : ''}}">
                {!! Form::label('dt_id', 'Choose new bus', ['class' => 'col-sm-3 control-label ui-select2']) !!}
                <div class="col-sm-6">
                    {!! Form::select('dt_id', $buses, null, ['class' => 'form-control', 'required' => 'required', 'onchange'=>'loadBusDetails(this)']) !!}
                    {!! $errors->first('dt_id', '<p class="help-block">:message</p>') !!}
                </div>
            </div>

            <div id="bus-details"></div>




         </div>
         <div class = "modal-footer">
            <button type = "button" id="TransCloseBtn" class = "btn btn-default" data-dismiss = "modal">
               Cancel
            </button>
            <button type = "submit" id="TransSubBtn" class = "btn btn-primary collapse">
               Transload
            </button>
            <button type = "button" id="TransRefreshBtn" class = "btn btn-default collapse" onclick="location.reload();">
               Refresh
            </button>
         

         </div>


         </form>




         <script type="text/javascript">
           function loadBusDetails(elem) {
              var dt_id = elem.value;
              console.log(elem);

              $("#bus-details").html("Please wait....");

              $.ajax({
                url: '{{ url("transload-bus-details") }}',
                dataType: 'text',
                type: 'GET',
                contentType: 'application/x-www-form-urlencoded',
                data: {"dt_id":dt_id, 'numPass':'{{ $numPass }}', 'fare':'{{ $fare }}'},
                success: function (response)
                {
                  $("#TransSubBtn").removeClass('collapse'); 
                  $("#bus-details").html(response);
                     
                },
                error: function (jqXhr,textStatus, errorThrown)
                {
                   
                } 
              });


           }


          $(document).ready(function(){ 
           $('#transloadForm{{ $booking_id }}').ajaxForm({
                beforeSubmit: genPreSubmit,
                success: function(response){
                  
                  if(response == 'success'){
                  // if(false){
                      $("#TransSubBtn").addClass('collapse');  
                      $("#TransCloseBtn").addClass('collapse');  
                      $("#TransRefreshBtn").removeClass('collapse');  
                      $('#TranscontentArea').addClass('collapse');
                      $("#error-notif").addClass("collapse");
                      $("#success-notif").removeClass("collapse");

                  }else{

                      $("#error-notif").html(response);
                      $("#error-notif").removeClass("collapse");
                      $("#TransSubBtn").html('Transload');



                  }
                  
                  
                },
                error: function(response){
                  
                   $("#error-notif").html(response);
                      $("#error-notif").removeClass("collapse");
                      $("#TransSubBtn").html('Transload');
                  
                },
                reset: true
            });
          });


        function genPreSubmit(){
        
          $("#TransSubBtn").html('please wait...');
         

        }

        $('.ui-select2').select2({ allowClear: true, placeholder: "Select..." });

         </script>