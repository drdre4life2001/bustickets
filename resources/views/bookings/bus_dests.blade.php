            <div class="form-group {{ $errors->has('default_driver_id') ? 'has-error' : ''}}">
                {!! Form::label('strip_id', 'Choose Destination', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::select('strip_id', $dests, null, ['class' => 'form-control select22', 'id'=>'destSelect', 'required' => 'required', 'onchange'=>'loadBookDetails(this)']) !!}
                    {!! $errors->first('strip_id', '<p class="help-block">:message</p>') !!}
                </div>
            </div>

            


            <div id="bookDetails"></div>


            <script type="text/javascript">

            function loadBookDetails(elem) {
              console.log(elem);
              var trip_id = elem.value;
              console.log(trip_id);

              $("#bookDetails").html("Please wait....");

              $.ajax({
                url: '{{ url("book-details/".$dt_id) }}/'+trip_id,
                dataType: 'text',
                type: 'GET',
                contentType: 'application/x-www-form-urlencoded',
                success: function (response)
                {
                  $("#bookDetails").html(response);
                     
                },
                error: function (jqXhr,textStatus, errorThrown)
                {
                   
                } 
              });


           }


           $(document).ready(function(){

                console.log('chaings...');

                $("#destSelect").trigger("change");
           });

           </script>
