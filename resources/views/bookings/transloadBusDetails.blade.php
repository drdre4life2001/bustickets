<div class="form-group {{ $errors->has('default_driver_id') ? 'has-error' : ''}}">
                {!! Form::label('strip_id', 'Choose Destination', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::select('strip_id', $dests, null, ['class' => 'form-control', 'required' => 'required']) !!}
                    {!! $errors->first('strip_id', '<p class="help-block">:message</p>') !!}
                </div>
            </div>

            <div class="form-group {{ $errors->has('default_driver_id') ? 'has-error' : ''}}">
                {!! Form::label('fare', 'Total Fare', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('final_cost', $fare, ['class' => 'form-control']) !!}
                    {!! $errors->first('fare', '<p class="help-block">:message</p>') !!}
                </div>
            </div>


            @for($i = 1; $i <= $numPass;  $i++)

            <div class="form-group {{ $errors->has('default_driver_id') ? 'has-error' : ''}}">
                {!! Form::label('seat'.$i, 'Choose Seat '.$i, ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::select('seat'.$i, $avail_seats, null, ['class' => 'form-control', 'required' => 'required']) !!}
                    {!! $errors->first('seat'.$i, '<p class="help-block">:message</p>') !!}
                </div>
            </div>

            @endfor