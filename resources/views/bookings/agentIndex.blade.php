@extends('layouts.master')

@section('content')
  
    <div class="content-header">
        <h2 class="content-header-title">Bookings </h2>
        <ol class="breadcrumb">
          <li><a href="{{ url('/') }}">Dashboard</a></li>
          <li class="active">Bookings </li>
        </ol>
      </div> <!-- /.content-header -->
    
      

      <div class="row">

        <div class="col-md-10 col-sm-8">

          <div class="portlet">
          
           @if(Session::has('flash_message'))
              <div class="alert alert-success">
              <a class="close" data-dismiss="alert" href="#" aria-hidden="true">×</a>
              <strong>Well done!</strong> {!! session('flash_message') !!}
            </div>

            @endif

            <div class="portlet-content">           

              <div class="table-responsive">

              <table 
                class="table table-striped table-bordered table-hover table-highlight table-checkable" 
                data-provide="datatable" 
                data-display-rows="10"
                data-info="true"
                data-search="true"
                data-length-change="true"
                data-paginate="true"
              >
                  <thead>
                    <tr>
                      <th>S.No</th>
                      <th>Trip</th>
                      <th>Date</th>
                      <th>Passenger Count</th>
                      <th>Unit Cost</th>
                      <th>Contact Person</th>
                      <th>Contact Phone</th>
                      <th>Status</th>
                      <th>Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    {{-- */$x=0;/* --}}
                    @foreach($bookings as $item)

                        {{-- */$x++;/* --}}
                        <tr>
                            <td>{{ $x }}</td>
                            <td><a href="{{ url('trips', $item->trip->id) }}">{{ $item->trip->name }}</a></td>
                            <td><a href="{{ url('bookings', $item->id) }}">{{ $item->date }}</a></td>
                            <td>{{ $item->passenger_count }}</td>
                            <td>{{ $item->unit_cost }}</td>
                            <td>{{ $item->contact_name }}</td>
                            <td>{{ $item->contact_phone }}</td>
                            <td>{{ $item->status }}</td>
                            <td>
                                <a href="{{ url('bookings/' . $item->id . '/edit') }}" title="Edit">
                                    <button type="submit" class="btn btn-default btn-xs"><i class="fa fa-edit"></i></button>
                                </a> 
                                {!! Form::open([
                                    'method'=>'DELETE',
                                    'url' => ['bookings', $item->id ],
                                    'style' => 'display:inline'
                                ]) !!}
                                    {!! Form::button('<i class="fa fa-times"></i>', ['class' => 'btn btn-danger btn-xs', 'escape'=>false,'title'=>'Delete', 'type'=>'submit']) !!}
                                {!! Form::close() !!}
                            </td>
                        </tr>
                    @endforeach
                  </tbody>
                </table>
              </div> <!-- /.table-responsive -->
              

            </div> <!-- /.portlet-content -->

          </div> <!-- /.portlet -->

        

        </div> <!-- /.col -->

        <div class="col-md-2 col-sm-4">

            <ul id="myTab" class="nav nav-pills nav-stacked">
            <li class="active">
              <a href="{{ url('bookings/create') }}">
                <i class="fa fa-plus"></i> 
                Add New Booking
              </a>
            </li>
          </ul>

        </div>

      </div> <!-- /.row -->    



@endsection
