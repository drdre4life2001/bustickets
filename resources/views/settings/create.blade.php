@extends('layouts.master')

@section('content')

   <div class="content-header">
        <h2 class="content-header-title">Add Setting </h2>
        <ol class="breadcrumb">
          <li><a href="{{ url('/') }}">Dashboard</a></li>
          <li><a href="{{ url('settings') }}">Settings </a></li>
          <li class="active">Add Setting </li>
        </ol>
      </div> <!-- /.content-header -->



      <div class="row">

        <div class="col-md-10 col-sm-8">

          <div class="portlet">

            <div class="portlet-content">

                {!! Form::open(['url' => 'settings', 'class' => 'form-horizontal']) !!}
<div class="well ">
<h3 class="text-center">Luggage limit settings </h3>
            <div class="form-group {{ $errors->has('luggage_limit') ? 'has-error' : ''}}">
                {!! Form::label('luggage_limit  </i>',
                  'Free Luggage limit (in kg): ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    <input type="number" name="luggage_limit" placeholder="The maximum weight of luggage you can accept free." class="form-control">
                    {!! $errors->first('luggage_limit', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>

            <div class="form-group {{ $errors->has('track_luggage') ? 'has-error' : ''}}">
                {!! Form::label('track_luggage', 'Do You Track Luggages?: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                <select class="form-control" name="track_luggage">
                    <option value="">Select</option>
                    <option value="True">Yes</option>
                    <option value="False">No</option>
                </select>
                    {!! $errors->first('track_luggage', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>
             <div class="form-group {{ $errors->has('intl_cost_per_kg') ? 'has-error' : ''}}">
                {!! Form::label('intl_cost_per_kg', 'International cost per kg (in Naira): ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    <input type="number" name="intl_cost_per_kg" class="form-control" placeholder="How much you charge per kg for international trips">
                    {!! $errors->first('intl_cost_per_kg', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('local_cost_per_kg') ? 'has-error' : ''}}">
                {!! Form::label('local_cost_per_kg', 'Local cost per kg: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    <input type="number" name="local_cost_per_kg" class="form-control" placeholder="How much you charge per kg for local trips">
                    {!! $errors->first('local_cost_per_kg', '<span class="parsley-error-list">:message</span>') !!}
                </div>
              </div>
    </div>
            <div class="form-group {{ $errors->has('daily_trip_expense_headers') ? 'has-error' : ''}}">
                {!! Form::label('daily_trip_expense_headers', 'Add New Trip Expenses (Separate with comma)', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    <textarea name="daily_trip_expense_headers" placeholder="eg. driver's dispatch, Fuel Expense" class="form-control"></textarea>
                    {!! $errors->first('daily_trip_expense_headers', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>

           <!--  <script type="text/javascript">


            if (typeof jQuery == 'undefined') {
                var script = document.createElement('script');
                script.type = "text/javascript";
                script.src = "http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js";
                document.getElementsByTagName('head')[0].appendChild(script);

                alert("No jquery");
            }


            //hide by default
            $('#whatDiscountForChildren').hide();

            //show only when they agree that they have children discounts
            $('#discountForChildren').change(function(){
                alert("Dddd0");
                if($(this).val() == 'True'){
                    $('#whatDiscountForChildren').show();
                }else{
                  $('#whatDiscountForChildren').hide();
                }
                });
            </script> -->
             

            <div class="form-group {{ $errors->has('discount_active') ? 'has-error' : ''}}">
                {!! Form::label('discount_active', 'Enable discount?: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                <select class="form-control" name="discount_active">
                    <option value="">Select</option>
                    <option value="true">Yes</option>
                    <option value="false">No</option>
                </select>
                    {!! $errors->first('discount_active', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>

          <!--  <div class="form-group {{ $errors->has('exposed') ? 'has-error' : ''}}">
                {!! Form::label('exposed', 'Exposed: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                                <div class="checkbox">
                <label>{!! Form::radio('exposed', '1') !!} Yes</label>
            </div>
            <div class="checkbox">
                <label>{!! Form::radio('exposed', '0', true) !!} No</label>
            </div>
                    {!! $errors->first('exposed', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div> -->


                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-3">
                        {!! Form::submit('Create', ['class' => 'btn btn-primary form-control']) !!}
                    </div>
                </div>
                {!! Form::close() !!}

                @if ($errors->any())
                    <ul class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                @endif
             </div> <!-- /.portlet-content -->

          </div> <!-- /.portlet -->



        </div> <!-- /.col -->

        <div class="col-md-2 col-sm-4">

            <ul id="myTab" class="nav nav-pills nav-stacked">
            <li class="active">
              <a href="{{ url('settings') }}">
                <i class="fa fa-bars"></i>
                List Settings
              </a>
            </li>
          </ul>

        </div>

      </div> <!-- /.row -->


      <script type="text/javascript">

      $(document).ready(function(){
           var rangeOne
            var rangeTwo
          $('#age_range_one, #age_range_two').blur(function(){

              runUpdate()
          });

          function runUpdate(){
            rangeOne = $('#age_range_one').val();
            rangeTwo = $('#age_range_two').val();
            $('#infant_age_range').val(rangeOne + ',' + rangeTwo);
          }
      });
      
      </script>

@endsection
