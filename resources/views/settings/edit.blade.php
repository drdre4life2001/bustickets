@extends('layouts.master')

@section('content')

    <div class="content-header">
        <h2 class="content-header-title">Edit Setting </h2>
        <ol class="breadcrumb">
          <li><a href="{{ url('/') }}">Dashboard</a></li>
          <li><a href="{{ url('settings') }}">Settings </a></li>
          <li class="active">Edit Setting </li>
        </ol>
      </div> <!-- /.content-header -->

      

      <div class="row">

        <div class="col-md-10 col-sm-8">

          <div class="portlet">

            <div class="portlet-content">

                {!! Form::model($setting, [
                    'method' => 'PATCH',
                    'url' => ['settings', $setting->id],
                    'class' => 'form-horizontal'
                ]) !!}

<div class="well ">
<h3 class="text-center">Luggage limit settings </h3>
                <div class="form-group {{ $errors->has('track_luggage') ? 'has-error' : ''}}">
                {!! Form::label('track_luggage', 'Activate Luggages?: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                <select name="track_luggage" class="form-control">
                    <option value="">Select</option>
                    <option value="True" @if($setting->track_luggage == "True") selected="selected" @endif>Yes</option>
                    <option value="False" @if($setting->track_luggage == "False") selected="selected" @endif>No</option>
                </select>
                    {!! $errors->first('track_luggage', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>

            <div class="form-group {{ $errors->has('luggage_limit') ? 'has-error' : ''}}">
                {!! Form::label('luggage_limit  </i>',
                  'Free Luggage limit (in kg): ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    <input type="number" name="luggage_limit" class="form-control" value="{{ $setting->luggage_limit }}">
                    {!! $errors->first('luggage_limit', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>

             <!-- <div class="form-group {{ $errors->has('intl_cost_per_kg') ? 'has-error' : ''}}">
                
                <div class="col-sm-6">
                    <input type="number" name="intl_cost_per_kg" class="form-control" value="{{ $setting->intl_cost_per_kg }}">
                </div>
            </div>
 -->            <div class="form-group {{ $errors->has('local_cost_per_kg') ? 'has-error' : ''}}">
                {!! Form::label('local_cost_per_kg', 'Local cost per kg (in Naira): ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    <input type="number" name="local_cost_per_kg" class="form-control" value="{{ $setting->local_cost_per_kg }}">
                    {!! $errors->first('local_cost_per_kg', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>
    </div>
            <div class="form-group {{ $errors->has('daily_trip_expense_headers') ? 'has-error' : ''}}">
                {!! Form::label('daily_trip_expense_headers', 'Add New Trip Expenses (Separate with comma)', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    <textarea name="daily_trip_expense_headers" class="form-control">{{ $setting->daily_trip_expense_headers or '' }}</textarea>
                    {!! $errors->first('daily_trip_expense_headers', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>

             <div class="form-group {{ $errors->has('discount_active') ? 'has-error' : ''}}">
                {!! Form::label('discount_active', 'Enable discount?: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                <select class="form-control" name="discount_active">
                    <option value="">Select</option>
                    <option value="true" @if($setting->discount_active == "true") selected="selected" @endif>Yes</option>
                    <option value="false" @if($setting->discount_active == "false") selected="selected" @endif>No</option>
                </select>
                    {!! $errors->first('discount_active', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>



                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-3">
                        {!! Form::submit('Update', ['class' => 'btn btn-primary form-control']) !!}
                    </div>
                </div>
                {!! Form::close() !!}

                @if ($errors->any())
                    <ul class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                @endif

             </div> <!-- /.portlet-content -->

          </div> <!-- /.portlet -->

        

        </div> <!-- /.col -->

        <div class="col-md-2 col-sm-4">

            <ul id="myTab" class="nav nav-pills nav-stacked">
<!--
            <li class="active">
                {!! Form::open([
                    'method'=>'DELETE',
                    'url' => ['settings', $setting->id],
                    'style' => 'display:inline'
                ]) !!}
                    {!! Form::button('<i class="fa fa-times"></i> Delete Setting', ['class' => 'btn btn-danger btn-xs', 'escape'=>false,'title'=>'Delete', 'type'=>'submit']) !!}
                {!! Form::close() !!}
            </li>
-->
            <li>
              <a href="{{ url('settings') }}">
                <i class="fa fa-bars"></i> 
                List Settings
              </a>
            </li>
            

          </ul>

        </div>

      </div> <!-- /.row --> 

      <!-- <script type="text/javascript">

      $(document).ready(function(){
           var rangeOne
            var rangeTwo
          $('#age_range_one, #age_range_two').blur(function(){

              runUpdate()
          });

          function runUpdate(){
            rangeOne = $('#age_range_one').val();
            rangeTwo = $('#age_range_two').val();
            $('#infant_age_range').val(rangeOne + ',' + rangeTwo);
          }
      });
      </script> -->
          

@endsection