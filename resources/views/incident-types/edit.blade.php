@extends('layouts.master')

@section('content')

    <div class="content-header">
        <h2 class="content-header-title">Edit Incident-type </h2>
        <ol class="breadcrumb">
          <li><a href="{{ url('/') }}">Dashboard</a></li>
          <li><a href="{{ url('incident-types') }}">Incident-types </a></li>
          <li class="active">Edit Incident-type </li>
        </ol>
      </div> <!-- /.content-header -->

      

      <div class="row">

        <div class="col-md-10 col-sm-8">

          <div class="portlet">

            <div class="portlet-content">

                {!! Form::model($incident-type, [
                    'method' => 'PATCH',
                    'url' => ['incident-types', $incident-type->id],
                    'class' => 'form-horizontal'
                ]) !!}

                            <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                {!! Form::label('name', trans('incident-types.name'), ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required']) !!}
                    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('details') ? 'has-error' : ''}}">
                {!! Form::label('details', trans('incident-types.details'), ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::textarea('details', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('details', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('is_active') ? 'has-error' : ''}}">
                {!! Form::label('is_active', trans('incident-types.is_active'), ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                                <div class="checkbox">
                <label>{!! Form::radio('is_active', '1') !!} Yes</label>
            </div>
            <div class="checkbox">
                <label>{!! Form::radio('is_active', '0', true) !!} No</label>
            </div>
                    {!! $errors->first('is_active', '<p class="help-block">:message</p>') !!}
                </div>
            </div>


                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-3">
                        {!! Form::submit('Update', ['class' => 'btn btn-primary form-control']) !!}
                    </div>
                </div>
                {!! Form::close() !!}

                @if ($errors->any())
                    <ul class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                @endif

             </div> <!-- /.portlet-content -->

          </div> <!-- /.portlet -->

        

        </div> <!-- /.col -->

        <div class="col-md-2 col-sm-4">

            <ul id="myTab" class="nav nav-pills nav-stacked">
            <li class="active">
                {!! Form::open([
                    'method'=>'DELETE',
                    'url' => ['incident-types', $incident-type->id],
                    'style' => 'display:inline'
                ]) !!}
                    {!! Form::button('<i class="fa fa-times"></i> Delete Incident-type', ['class' => 'btn btn-danger btn-xs', 'escape'=>false,'title'=>'Delete', 'type'=>'submit']) !!}
                {!! Form::close() !!}
            </li>
            <li>
              <a href="{{ url('incident-types') }}">
                <i class="fa fa-bars"></i> 
                List Incident-types
              </a>
            </li>
            <li class="">
              <a href="{{ url('incident-types/create') }}">
                <i class="fa fa-plus"></i> 
                Add Incident-type
              </a>
            </li>
          </ul>

        </div>

      </div> <!-- /.row -->   

@endsection