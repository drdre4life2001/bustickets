@extends('layouts.master')

@section('content')

    <div class="content-header">
        <h2 class="content-header-title">Incident-types </h2>
        <ol class="breadcrumb">
          <li><a href="{{ url('/') }}">Dashboard</a></li>
          <li class="active">Incident-types </li>
        </ol>
      </div> <!-- /.content-header -->

      

      <div class="row">

        <div class="col-md-10 col-sm-8">

          <div class="portlet">

            <div class="portlet-content">           

              <div class="table-responsive">

              <table 
                class="table table-striped table-bordered table-hover table-highlight table-checkable" 
                data-provide="datatable" 
                data-display-rows="10"
                data-info="true"
                data-search="true"
                data-length-change="true"
                data-paginate="true"
              >
                  <thead>
                    <tr>
                      <th>S.No</th><th>{{ trans('incident-types.name') }}</th><th>{{ trans('incident-types.details') }}</th><th>{{ trans('incident-types.is_active') }}</th><th>Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    {{-- */$x=0;/* --}}
                    @foreach($incidenttypes as $item)
                        {{-- */$x++;/* --}}
                        <tr>
                            <td>{{ $x }}</td>
                            <td><a href="{{ url('incident-types', $item->id) }}">{{ $item->name }}</a></td><td>{{ $item->details }}</td><td>{{ $item->is_active }}</td>
                            <td>
                                <a href="{{ url('incident-types/' . $item->id . '/edit') }}" title="Edit">
                                    <button type="submit" class="btn btn-default btn-xs"><i class="fa fa-edit"></i></button>
                                </a> 
                                {!! Form::open([
                                    'method'=>'DELETE',
                                    'url' => ['incident-types', $item->id],
                                    'style' => 'display:inline'
                                ]) !!}
                                    {!! Form::button('<i class="fa fa-times"></i>', ['class' => 'btn btn-danger btn-xs', 'escape'=>false,'title'=>'Delete', 'type'=>'submit']) !!}
                                {!! Form::close() !!}
                            </td>
                        </tr>
                    @endforeach
                  </tbody>
                </table>
              </div> <!-- /.table-responsive -->
              

            </div> <!-- /.portlet-content -->

          </div> <!-- /.portlet -->

        

        </div> <!-- /.col -->

        <div class="col-md-2 col-sm-4">

            <ul id="myTab" class="nav nav-pills nav-stacked">
            <li class="active">
              <a href="{{ url('incident-types/create') }}">
                <i class="fa fa-plus"></i> 
                Add New Incident-type
              </a>
            </li>
          </ul>

        </div>

      </div> <!-- /.row -->    



@endsection
