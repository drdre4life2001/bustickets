@extends('layouts.master')

@section('content')

    <h1>Incident-type</h1>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>ID.</th> <th>{{ trans('incident-types.name') }}</th><th>{{ trans('incident-types.details') }}</th><th>{{ trans('incident-types.is_active') }}</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{ $incident-type->id }}</td> <td> {{ $incident-type->name }} </td><td> {{ $incident-type->details }} </td><td> {{ $incident-type->is_active }} </td>
                </tr>
            </tbody>    
        </table>
    </div>

@endsection