@extends('layouts.master')

@section('content')

   <div class="content-header">
        <h2 class="content-header-title">Add Bus </h2>
        <ol class="breadcrumb">
          <li><a href="{{ url('/') }}">Dashboard</a></li>
          <li><a href="{{ url('buses') }}">Buss </a></li>
          <li class="active">Add Bus </li>
        </ol>
      </div> <!-- /.content-header -->

      

      <div class="row">

        <div class="col-md-10 col-sm-8">

          <div class="portlet">

            <div class="portlet-content">

                {!! Form::open(['url' => 'buses', 'class' => 'form-horizontal']) !!}

            <div class="form-group {{ $errors->has('bus_number') ? 'has-error' : ''}}">
                {!! Form::label('bus_number', trans('buses.bus_number'), ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('bus_number', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('bus_number', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('number_plate') ? 'has-error' : ''}}">
                {!! Form::label('number_plate', trans('buses.number_plate'), ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('number_plate', null, ['class' => 'form-control', 'required' => 'required']) !!}
                    {!! $errors->first('number_plate', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('chassis_number') ? 'has-error' : ''}}">
                {!! Form::label('chassis_number', 'Chassis number', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('chassis_number', null, ['class' => 'form-control', 'required' => 'required']) !!}
                    {!! $errors->first('chassis_number', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('engine_number') ? 'has-error' : ''}}">
                {!! Form::label('engine_number', 'Engine number', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('engine_number', null, ['class' => 'form-control', 'required' => 'required']) !!}
                    {!! $errors->first('engine_number', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('no_of_seats') ? 'has-error' : ''}}">
                {!! Form::label('no_of_seats', 'No of seats', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::number('no_of_seats', null, ['class' => 'form-control', 'value'=>'15', 'required' => 'required']) !!}
                    {!! $errors->first('no_of_seats', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('bus_type_id') ? 'has-error' : ''}}">
                {!! Form::label('bus_type_id', trans('buses.bus_type_id'), ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::select('bus_type_id', $bus_types,null, ['class' => 'form-control select22', 'id'=>"select2", 'required' => 'required']) !!}
                    {!! $errors->first('bus_type_id', '<p class="help-block">:message</p>') !!}
                </div>
            </div>

           <!--  <div class="form-group {{ $errors->has('bus_roof') ? 'has-error' : ''}}">
                {!! Form::label('bus_roof', "Bus Roof", ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('bus_roof',null, ['class' => 'form-control', 'id'=>"select22", 'required' => 'required']) !!}
                    {!! $errors->first('bus_roof', '<p class="help-block">:message</p>') !!}
                </div>
            </div> -->

            <div class="form-group {{ $errors->has('vin') ? 'has-error' : ''}}">
                {!! Form::label('vin', trans('buses.vin'), ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('vin', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('vin', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('other_details') ? 'has-error' : ''}}">
                {!! Form::label('other_details', trans('buses.other_details'), ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::textarea('other_details', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('other_details', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('default_park_id') ? 'has-error' : ''}}">
                {!! Form::label('default_park_id', trans('buses.default_park_id'), ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::select('default_park_id', $parks,null, ['class' => 'form-control select22', 'id'=>"select22", 'placeholder'=>'Choose or leave blank to make it Flexible']) !!}
                    {!! $errors->first('default_park_id', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('default_driver_id') ? 'has-error' : ''}}">
                {!! Form::label('default_driver_id', 'Default Driver', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::select('default_driver_id', $drivers, null, ['class' => 'form-control select22', 'id'=>"select22", 'placeholder'=>'Choose or leave blank to make it Flexible']) !!}
                    {!! $errors->first('default_driver_id', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('default_trip_id') ? 'has-error' : ''}}">
                {!! Form::label('default_trip_id', 'Default trip', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::select('default_trip_id', $trips, null, ['class' => 'form-control select22', 'id'=>"select22", 'placeholder'=>'Choose or leave blank to make it Flexible']) !!}
                    {!! $errors->first('default_trip_id', '<p class="help-block">:message</p>') !!}
                </div>
            </div>


                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-3">
                        {!! Form::submit('Create', ['class' => 'btn btn-primary form-control']) !!}
                    </div>
                </div>
                {!! Form::close() !!}

                @if ($errors->any())
                    <ul class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                @endif
             </div> <!-- /.portlet-content -->

          </div> <!-- /.portlet -->

        

        </div> <!-- /.col -->

        <div class="col-md-2 col-sm-4">

            <ul id="myTab" class="nav nav-pills nav-stacked">
            <li class="active">
              <a href="{{ url('buses') }}">
                <i class="fa fa-bars"></i> 
                List Buss
              </a>
            </li>
          </ul>

        </div>

      </div> <!-- /.row -->   

@endsection