@extends('layouts.master')

@section('content')

    <h1>Bus</h1>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>ID.</th> 
                    <th>{{ trans('buses.bus_number') }}</th>
                    <th>{{ trans('buses.number_plate') }}</th>
                    <th>Chassis Number</th>
                    <th>{{ trans('buses.bus_type_id') }}</th>
                    <th>Bus Roof</th>
                    
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{ $bus->id }}</td> 
                    <td> {{ $bus->bus_number }} </td>
                    <td> {{ $bus->number_plate }} </td>
                    <td> {{ $bus->chassis_number }} </td>
                    <td> {{ $bus->bus_type_id }} </td>
                    <td> {{ $bus->bus_roof }} </td>
                </tr>
            </tbody>    
        </table>
    </div>

@endsection