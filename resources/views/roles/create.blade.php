@extends('layouts.master')

@section('content')

   <div class="content-header">
        <h2 class="content-header-title">Add Role </h2>
        <ol class="breadcrumb">
          <li><a href="{{ url('/') }}">Dashboard</a></li>
          <li><a href="{{ url('roles') }}">Roles </a></li>
          <li class="active">Add Role </li>
        </ol>
      </div> <!-- /.content-header -->

      

      <div class="row">

        <div class="col-md-10 col-sm-8">

          <div class="portlet">

            <div class="portlet-content">

                {!! Form::open(['url' => 'roles', 'class' => 'form-horizontal']) !!}

                            <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                {!! Form::label('name', 'Name: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required']) !!}
                    {!! $errors->first('name', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>

            <fieldset>Permissions</fieldset>
            <hr />
            
            <?php foreach (config('constant.all_permissions') as $group_key => $perms): ?>
  
            <div class="form-group">
                  <label class="col-md-3 control-label"><?php echo $group_key; ?></label>
                  <div class="col-md-9">
                    <?php foreach ($perms as $key => $value): ?>
                      <div class="checkbox">
                          <label>
                              <input type="checkbox" id="example-checkbox1" name="perm[]" value="<?php echo $key; ?>" > <?php echo $value; ?>
                          </label>
                      </div>
                      <?php endforeach; ?>

                      
                  </div>
              </div>

            <?php endforeach; ?>


                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-3">
                        {!! Form::submit('Create', ['class' => 'btn btn-primary form-control']) !!}
                    </div>
                </div>
                {!! Form::close() !!}

                @if ($errors->any())
                    <ul class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                @endif
             </div> <!-- /.portlet-content -->

          </div> <!-- /.portlet -->

        

        </div> <!-- /.col -->

        <div class="col-md-2 col-sm-4">

            <ul id="myTab" class="nav nav-pills nav-stacked">
            <li class="active">
              <a href="{{ url('roles') }}">
                <i class="fa fa-bars"></i> 
                List Roles
              </a>
            </li>
          </ul>

        </div>

      </div> <!-- /.row -->   

@endsection