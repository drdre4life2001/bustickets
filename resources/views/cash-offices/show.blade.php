@extends('layouts.master')

@section('content')

    <h1>Cash-office</h1>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>ID.</th> <th>{{ trans('cash-offices.name') }}</th><th>{{ trans('cash-offices.contact_name') }}</th><th>{{ trans('cash-offices.address') }}</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{ $cash-office->id }}</td> <td> {{ $cash-office->name }} </td><td> {{ $cash-office->contact_name }} </td><td> {{ $cash-office->address }} </td>
                </tr>
            </tbody>    
        </table>
    </div>

@endsection