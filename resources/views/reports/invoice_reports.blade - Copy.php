@extends('layouts.master')

@section('content')

<div class="content-header">
        <h2 class="content-header-title">Invoice Reports</h2>
        <ol class="breadcrumb">
          <li><a href="./index.html">Dashboard</a></li>
          <li class="active">Reports</li>
        </ol>
      </div> <!-- /.content-header -->

      

    

        <div class="col-md-12 col-sm-8">

          <div class="tab-content stacked-content">

            <div class="tab-pane fade in active" id="profile-tab">
              
              
              <h4 class="heading-inline pull-left">
          &nbsp;&nbsp;<small>Choose Bus</small>
          &nbsp;&nbsp;</h4>
              <div class="btn-group pull-left">
                  <div class="form-group">
                    <select id="select-input" class="form-control" onchange="refreshReport();">
                      <option value="" @if(empty($bus_id)) selected="selected" @endif > -ALL- </option>
                      @foreach($buses as $key => $value)
                        <option value="{{ $key }}" @if($key == $bus_id) selected="selected" @endif>{{ $value }}</option>
                      @endforeach
                    </select>
              </div>
              </div>
              <h4 class="heading-inline pull-left">
          &nbsp;&nbsp;<small>Choose Park</small>
          &nbsp;&nbsp;</h4>
              <div class="btn-group pull-left">
                  <div class="form-group">
                    <select id="park-input" class="form-control" onchange="refreshReport();">
                      <option value="0" @if(empty($park_id)) selected="selected" @endif > -ALL- </option>
                      @foreach($parks as $key => $value)
                        <option value="{{ $key }}" @if($park_id == $key) selected="selected" @endif>{{ $value }}</option>
                      @endforeach
                      
                    </select>
                  </div>

                  
              </div>

              <div class="btn-group pull-right">
                  <div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
                      <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
                      <span></span> <b class="caret"></b>
                  </div>
              </div>
          <h4 class="heading-inline pull-right">
          &nbsp;&nbsp;<small>Choose date range</small>
          &nbsp;&nbsp;</h4>



              <hr />

              <br /><br />

              <h3 class="">{{ (isset($bus))?$bus.' Invoice':"All Invoices" }} Report</h3>
              <!-- <h5 style="float:right;"></h5> -->
              <br />



              <div class="table-responsive">



              <table id="exportTable"
                class="table table-striped table-bordered table-hover table-highlight table-checkable" 
                data-provide="datatable" 
                data-display-rows="10"
                data-info="true"
                data-search="true"
                data-length-change="true"
                data-paginate="true"
              >
                  <thead>
                    
                    <tr>
                      
                      <th>Trip Details</th>
                      <th>Bookings Details</th>
                      <th>Total Revenue</th>
                      <th>Total Expenses</th>
                      <th>Net Revenue</th>
                      <th>Breakdown</th>
                      
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>




              
              
              <table id="exportTable"
                class="table table-striped table-bordered table-hover table-highlight table-checkable" 
                data-provide="datatable" 
                data-display-rows="10"
                data-info="true"
                data-search="true"
                data-length-change="true"
                data-paginate="true"
              >
            
                  <thead>
                    
                    <tr class="titlerow">
                      <th>Bus</th>
                      <th>Number of passengers</th>
                      <th>Number of Trips</th>
                      <th>Total Fare</th>
                      <th>Commission (N70/passenger)</th>
                      <th width="35%">Destinations</th>
                    </tr>
                  </thead>
                  <tbody>
                      
                     
                  @foreach($dailytrips as $item)
                      <?php if(empty($item->bus)) continue; ?>
                     
                        <tr  id="trips_num{{ $item->bus_id }}" class="loadBusInfo" data-bus-id="{{ $item->bus_id }}" data-bus-number="{{ $item->bus->number_plate }}" >
                            <td class="rowDataSd">
                              {{ (!empty($item->bus))?$item->bus->number_plate:"" }}
                            </td>
                            
                            <td colspan="6" class="rowDataSd" >
                              loading....
                            </td> 
								    
                        </tr>
                    @endforeach
                          <tr class="totalColumn">
					            <td class="totalCol"></td>
					            <td class="totalCol">Total:</td>
					            <td class="totalCol">Total:</td>
					            <td class="totalCol">Total Fare:</td>
					            <td class="totalCol">Total Commission:</td>
					            <td class="totalCol"></td>
					        </tr> 
					
                    
                      
                  </tbody>
                </table>


            </div> <!-- /.tab-pane --> 

                <!-- <div class="btn-group">
                <button data-toggle="dropdown" class="btn btn-info dropdown-toggle" type="button">
                  <i class="fa fa-download"></i> Download <span class="caret"></span>
                </button>
                <ul role="menu" class="dropdown-menu">
                  <li><a href="javascript:;">Download as PDF</a></li>
                  <li><a href="javascript:;">Download as Excel</a></li>
                  
                </ul>
              </div> <span> </span>-->

            </div>

        </div> <!-- /.col -->

      </div> <!-- /.row -->


<script type="text/javascript">

function updateUrl(name, value){
            window.location.search = jQuery.query.set(name, value);

            return false;
       } 

function refreshReport(){

  console.log($('#select-input').val());
  // if($('#select-input').val() == "")
  //   window.location = '{{ route("manifest-reports") }}';
  if($('#select-input').val() != ""){
    window.location = '{{ route("invoice-reports") }}/'+$('#select-input').val()+'/'+$('#park-input').val();
  }else if($('#select-input').val() == "" && $('#park-input').val() != "") {
    window.location = '{{ route("invoice-reports") }}/0'+'/'+$('#park-input').val();
  }


}


$(function() {
    function cb(start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    }
    cb(moment("{{ $start_date }}"), moment("{{ $end_date }}"));

    $('#reportrange').daterangepicker({
        ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cb);

    $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
        console.log(picker.startDate.format('YYYY-MM-DD'));
        console.log(picker.endDate.format('YYYY-MM-DD'));
        
        updateUrl('daterange', picker.startDate.format('YYYY-MM-DD')+' '+picker.endDate.format('YYYY-MM-DD'));
    });
    
    
 


     $( ".loadBusInfo" ).each(function( index, element ) {
         var bus_id = $(element).attr('data-bus-id');
         var bus_number = $(element).attr('data-bus-number');
        // alert(bus_number);
         // $( element ).html( '<span style="font-style:italic;font-size:80%;"><img width="15px" src="{{ asset("desktop/img/ajax-loader-fb.gif") }}">');
         $.ajax
         (
             {
                type: "POST",
                url: "{{ url('get-bus-trip-details') }}",
                data: ({ rnd : Math.random() * 100000, park_id:'{{ $park_id }}', bus_id: bus_id, bus_number: bus_number,start_date: '{{ $start_date }}' , end_date: '{{ $end_date }}', _token: '{{ csrf_token() }}' }),
                success: function(html)
                {
                     $( element ).html(html);
                     $(element).removeClass('loadBusInfo');
   /*                  
                     //start
  var totals=[0,0,0,0,0,0]; //always update this evil array whenever you increase or decrease column number
        $(document).ready(function(){

            var $dataRows=$("#sum_table tr:not('.totalColumn, .titlerow')");

            $dataRows.each(function() {
                $(this).find('.rowDataSd').each(function(i){        
                    totals[i]+=parseInt( $(this).html());
                });
            });
            $("#sum_table td.totalCol").each(function(i){  
                $(this).html("total: "+totals[i]);
            });

        }); 
        
        */
        //end
                }
          });
          
      
          
         
      
      


     });
     
     

});
</script>




 @endsection     