@extends('layouts.master')

@section('content')

<div class="content-header">
        <h2 class="content-header-title">Reports</h2>
        <ol class="breadcrumb">
          <li><a href="./index.html">Dashboard</a></li>
          <li class="active">Reports</li>
        </ol>
      </div> <!-- /.content-header -->

      

      <div class="row">

       

        <div class="col-md-12 col-sm-8">

          <div class="tab-content stacked-content">

            <div class="tab-pane fade in active" id="profile-tab">
              
              <h3 class="">Payments Report</h3>

              <h4 class="heading-inline pull-left">
          &nbsp;&nbsp;<small>Choose Payment Method</small>
          &nbsp;&nbsp;</h4>
              <div class="btn-group pull-left">
                  <div class="form-group">
                    <select id="select-input" class="form-control" onchange="refreshReport();">
                      <option value="0" > -ALL- </option>
                      @foreach($payment_methods as $key => $value)
                        <option value="{{ $key }}" @if($sel_p_id == $key) selected="selected" @endif>{{ $value }}</option>
                      @endforeach
                      
                    </select>
              </div>
              </div>


              <h4 class="heading-inline pull-left">
          &nbsp;&nbsp;<small>Park</small>
          &nbsp;&nbsp;</h4>
              <div class="btn-group pull-left">
                  <div class="form-group">
                    <select id="parkSel" class="form-control" onchange="refreshReport();">
                      <option value="0" > -ALL- </option>
                      @foreach($parks as $key => $value)
                        <option value="{{ $key }}" @if($park_id == $key) selected="selected" @endif>{{ $value }}</option>
                      @endforeach
                      
                    </select>
              </div>
              </div>


              <h4 class="heading-inline pull-left">
          &nbsp;&nbsp;<small>Destination</small>
          &nbsp;&nbsp;</h4>
              <div class="btn-group pull-left">
                  <div class="form-group">
                    <select id="destSel" class="form-control" onchange="refreshReport();">
                      <option value="0" > -ALL- </option>
                      @foreach($destinations as $key => $value)
                        <option value="{{ $key }}" @if($dest_id == $key) selected="selected" @endif>{{ $value }}</option>
                      @endforeach
                      
                    </select>
              </div>
              </div>


              <div class="btn-group pull-right">
              <div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
                  <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
                  <span></span> <b class="caret"></b>
              </div>
          </div>
          <h4 class="heading-inline pull-right">
          &nbsp;&nbsp;<small>Choose date range</small>
          &nbsp;&nbsp;</h4>



              <hr />

              <br /><br />
              <div class="well">
                <h4><span class="text-primary">Summary</span></h4>
                
                  <p>
                    @foreach($payment_methods as $key => $value)
                      <?php if(!empty($sel_p_id) && $sel_p_id != $key ) continue;  ?>
                      <span class="text-primary">Total {{ $value }} bookings: </span><strong  id="total{{ $value }}">0</strong> | 
                    
                      <span class="text-primary">Total passengers with {{ $value }} bookings:: </span><strong id="pass{{ $value }}">0</strong> | 

                    @endforeach  
                    
                  </p>

              </div>

              <div class="table-responsive">

              <table id="reportTable"
                class="table table-striped table-bordered table-hover table-highlight table-checkable" 
                data-provide="datatable1" 
                data-display-rows="10"
                data-info="true"
                data-search="true"
                data-length-change="true"
                data-paginate="true"
              >
                  <thead>
                    <tr>
                      <th>Total Passengers</th>
                      <th>Payment Method</th>
                      <th>Billed Passengers</th>
                      <th>Non-Billed Passengers</th>
                      <th>Total Amount</th>
                      <th>Park</th>
                      <th>Destination</th>
                      <th></th>
                      
                      
                    </tr>
                  </thead>
                  <tbody>
                    {{-- */$x=0;/* --}}
                    @foreach($items as $item)

                        {{-- */$x++;/* --}}
                        <tr id="trips_num{{ $item->id }}" class="loadDetailInfo" data-id="{{ $item->id }}" data-name="{{ $item->name }}" >
                            <?php if(empty($item->name)) continue; ?>
                            <td>{{ $item->name }}</td>
                            <td colspan=7 >
                              loading....
                              
                            </td>
                        </tr>
                    @endforeach
                  </tbody>
                </table>


            </div> <!-- /.tab-pane --> 

               <!--  <div class="btn-group">
                <button data-toggle="dropdown" class="btn btn-info dropdown-toggle" type="button">
                  <i class="fa fa-download"></i> Download <span class="caret"></span>
                </button>
                <ul role="menu" class="dropdown-menu">
                  <li><a href="javascript:;">Download as PDF</a></li>
                  <li><a href="javascript:;">Download as Excel</a></li>
                  
                </ul>
              </div> -->

            </div>

        </div> <!-- /.col -->

      </div> <!-- /.row -->


<script type="text/javascript">

function updateUrl(name, value){
            window.location.search = jQuery.query.set(name, value);

            return false;
       } 

function refreshReport(){

  console.log($('#select-input').val());
  window.location = '{{ route("payment-reports") }}/'+$('#select-input').val()+'/'+$('#parkSel').val()+'/'+$('#destSel').val();


}


$(function() {

    function cb(start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    }
    cb(moment("{{ $start_date }}"), moment("{{ $end_date }}"));

    $('#reportrange').daterangepicker({
        ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cb);

    $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
        console.log(picker.startDate.format('YYYY-MM-DD'));
        console.log(picker.endDate.format('YYYY-MM-DD'));
        
        updateUrl('daterange', picker.startDate.format('YYYY-MM-DD')+' '+picker.endDate.format('YYYY-MM-DD'));
    });

    $( ".loadDetailInfo" ).each(function( index, element ) {
         var id = $(element).attr('data-id');
         var name = $(element).attr('data-name');
         // $( element ).html( '<span style="font-style:italic;font-size:80%;"><img width="15px" src="{{ asset("desktop/img/ajax-loader-fb.gif") }}">');
         $.ajax
         (
             {
                type: "POST",
                url: "{{ url('get-payment-bookings') }}",
                data: ({ rnd : Math.random() * 100000, id: id, name: name,start_date: '{{ $start_date }}' , end_date: '{{ $end_date }}', _token: '{{ csrf_token() }}', park_id:'{{ $park_id }}', dest_id: '{{ $dest_id }}' }),
                success: function(html)
                {
                     $( element ).html(html);
                     $(element).removeClass('loadDetailInfo');

                }
          });

     });


});

setTimeout(function(){
  $("#reportTable").dataTable ( {
        dom: 'Bfrtip',
        "ordering": false,
        buttons: [
            //'excel', 'pdf', 'print',
            {
                extend: 'excelHtml5',
                exportOptions: {
                    columns: [ 0, 1, 2, 3,4 ]
                }
            },
            {
                extend: 'pdfHtml5',
                exportOptions: {
                    columns: [ 0, 1, 2, 3,4 ]
                }
            },
            {
                extend: 'print',
                exportOptions: {
                    columns: [ 0, 1, 2, 3,4 ]
                }
            },
        ]
    })
}, 5000);

</script>





 @endsection     