<td>
  {{  $park_name }}
</td>
<td>
  {{ $dest_name }}
</td>
<td>
  {{ number_format(count($daily_trips)) }}
</td>
<td>{{ number_format($daily_trips->sum('booked_seats')) }}<br/>
<strong> {{ number_format($daily_trips->sum('booked_seats')) }}</strong> Adults<br/>
<strong>{{$allpassengers['infants']}}</strong> Infants<br/>
</td>
{{-- */$extlug =0;$sum=0;/* --}}
@foreach($daily_trips as $item)
         @foreach($item->bookings as $b)
          {{-- */
            $extlug += $b->extra_luggages->sum('amount');
           /* --}}
        @endforeach
        @foreach($item->expenses as $e)
                {{-- */
                 $sum+= $e->amount;
                /* --}}
          @endforeach

@endforeach



<td>
  <span style="color:green;">  &#8358;{{ number_format($daily_trips->sum('total_fare') ) }} </span>
</td>
<td>
  <span style="color:green;">  &#8358;
      @if(!empty($daily_trips->sum('booked_seats'))) {{ number_format($daily_trips->sum('total_fare') / $daily_trips->sum('booked_seats'))  }} @else 0.00 @endif  </span>
</td>
<td>
  {{-- */$x=0;/* --}}
  <div class = "modal fade" id="modal{{ $park_id }}" tabindex = "-1" role = "dialog" aria-labelledby = "myModalLabel" aria-hidden = "true">
   <div class = "modal-dialog" style="width:1000px;">
      <div class = "modal-content">
         <div class = "modal-header">
            <button type = "button" class = "close" data-dismiss = "modal" aria-hidden = "true">
                  &times;
            </button>
            <h4 class = "modal-title" id = "myModalLabel">
               Trips for {{ $park_name }} Payments between {{ date('D, M d, Y',strtotime($start_date)) }} and {{ date('D, M d, Y',strtotime($end_date)) }}  :: {{ count($daily_trips) }} Trip(s)
            </h4>
         </div>

         <div class="modal-body">

         <div class="table-responsive">

              <table id="sDataTable" 
                class="table table-striped table-bordered table-hover table-highlight table-checkable" 
                data-provide="datatable" 
                data-display-rows="10"
                data-info="true"
                data-search="true"
                data-length-change="true"
                data-paginate="true"
              >
                  <thead>
                    <tr>
                      <th>Trip Details</th>
                      <th>Bookings Details</th>
                      <th>Driver Details</th>
                      <th>Financial Details</th>
                      <th>Status</th>
                      <th>Actions</th>

                    </tr>
                  </thead>
                  <tbody>
                    {{-- */$x=0;/* --}}
                    @foreach($daily_trips as $item)
                      
                      
                         
                        {{-- */$x++;/* --}}
                        <tr>
                            <td>
                              <strong>Trip Date:</strong> {{ date('D, M d, Y',strtotime($item->trip_date)) }}<br/>
                              <strong>From:</strong> {{ $item->trip->sourcepark->name }}<br/>
                              <strong>To:</strong> @foreach($item->trip->subTrips as $st) {{ $st->destpark->name }}<br/> @endforeach
                              <strong>Time:</strong> {{ date('h:iA', strtotime($item->trip->departure_time)) }}<br/>
                              <strong>Bus Type:</strong> {{ $item->bus->bus_type->name }}<br/>
                              <strong>Fare:</strong> &#8358;{{ number_format($item->trip->fare, 2) }}<br/>
                            </td>
                            <td>
                              <strong>Booked seats:</strong> {{ $item->booked_seats }}<br/>
                              <strong>Total seats:</strong> {{ $item->total_seats }}<br/>
                              <strong>Total Fare:</strong><span style="color:green;"> &#8358;{{ number_format($item->total_fare, 2) }}</span><br/>
                            </td>
                            <td>
                              <strong>Driver:</strong> {{ (!empty($item->driver))?$item->driver->name:'' }}<br/>
                              <strong>Bus Number:</strong> {{ (!empty($item->bus))?$item->bus->bus_number:"" }}<br/>
                              <strong>Bus number plate:</strong> {{ (!empty($item->bus))?$item->bus->number_plate:'' }}<br/>
                              
                            </td>
                            <td>
                              @foreach($item->expenses as $e)
                                <?php if(empty($e->amount)) continue; ?>
                                <strong>{{ $e->expense_name }}:</strong><span style="color:red;"> &#8358;{{ number_format($e->amount, 2) }}</span><br/>

                              @endforeach
                              <strong>Net Revenue:</strong><span style="color:green;"> &#8358;{{ number_format((($item->total_fare+$extlug) - $sum), 2) }}</span><br/>
                              
                            </td>
                            
                            <td>{{ $item->status }}</td>
                            <td>
                              
                              <a type="button" href="{{ route('manifest', $item->id) }}" target="_blank" > View Manifest</a> <br/>
                              
                            </td>
                        </tr>
                    @endforeach
                  </tbody>
                </table>


            </div> <!-- /.tab-pane --> 
            </div>
         
         <div class = "modal-footer">
            <button type = "button" class = "btn btn-default" data-dismiss = "modal">
               Close
            </button>
         </div>
      </div><!-- /.modal-content -->
   </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

  <a type="button" href="#" class=" " data-toggle="modal" data-target="#modal{{ $park_id }}">View Details</a>
</td>

<script type="text/javascript">

  $(function() {
     
    var tTrips = parseInt($("#totalTripsCont").html()) + parseInt({{ count($dt2) }});
    $("#totalTripsCont").html(tTrips);

    var tTrips = parseInt($("#totalPassCont").html()) + parseInt({{ $daily_trips->sum('booked_seats') }});
    $("#totalPassCont").html(tTrips);

    var tTrips = parseInt($("#totalRevCont").html()) + parseInt({{ $daily_trips->sum('total_fare') }});
    $("#totalRevCont").html(tTrips);

    
    

  });
</script>





