@extends('layouts.master')

@section('content')

<div class="content-header">
        <h2 class="content-header-title">Reports</h2>
        <ol class="breadcrumb">
          <li><a href="./index.html">Dashboard</a></li>
          <li class="active">Reports</li>
        </ol>
      </div> <!-- /.content-header -->

      

      <div class="row">


        <div class="col-md-12 col-sm-8">

          <div class="tab-content stacked-content">

            <div class="tab-pane fade in active" id="profile-tab">
              
              <h3 class="">Invoice Report</h3>
              <br/>

              <hr />
        </div>
      </div>

      <div class="row">


        <div class="col-md-12 col-sm-8">



               <div class="tab-pane fade in active" id="profile-tab">
              
              
              <h4 class="heading-inline pull-left">
          &nbsp;&nbsp;<small>Choose Status</small>
          &nbsp;&nbsp;</h4>
              <div class="btn-group pull-left">
                  <div class="form-group">
                     <select id="select-input" class="form-control" onchange="refreshReport();">
                      <option value="ALL" @if($status == 'ALL') selected="selected" @endif > -ALL- </option>
                      @foreach($statuses as $s)
                        <option value="{{ $s }}" @if($status == $s) selected="selected" @endif>{{ $s }}</option>
                      @endforeach
                    </select>
              </div>
              </div>

               <h4 class="heading-inline pull-left">
          &nbsp;&nbsp;<small>Choose Park</small>
          &nbsp;&nbsp;</h4>
              <div class="btn-group pull-left">
                  <div class="form-group">
                    <select id="park-input" class="form-control" onchange="refreshReport();">
                      <option value="0" @if(empty($park_id)) selected="selected" @endif > -ALL- </option>
                      @foreach($parks as $key => $value)
                        <option value="{{ $key }}" @if($park_id == $key) selected="selected" @endif>{{ $value }}</option>
                      @endforeach
                      
                    </select>
                  </div>

                  
              </div>

              <div class="btn-group pull-right">
                  <div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
                      <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
                      <span></span> <b class="caret"></b>
                  </div>
              </div>
          <h4 class="heading-inline pull-right">
          &nbsp;&nbsp;<small>Choose date range</small>
          &nbsp;&nbsp;</h4>



              <hr />

              <br /><br />

              <!-- <div class="well">
                <h4><span class="text-primary">Summary</span></h4> -->
                
                 
                      {{-- */$x=0; $extlug =0;$expnz=0; /* --}}
                      @foreach($dailytrips as $dailytrip)
                      
                          @foreach($dailytrip->bookings as $b)
                      {{-- */
                                        $extlug += $b->extra_luggages->sum('amount');
                       /* --}}
                                    @endforeach
                      
                      @foreach($dailytrip->expenses as $exps)
                      {{-- */
                                        $expnz += $exps->amount;
                       /* --}}
                                    @endforeach
                                   
                                @endforeach
                      
                     

             <!--  </div> -->

              <div class="table-responsive">

              <table id="exportTable"
                class="table table-striped table-bordered table-hover table-highlight table-checkable" 
                data-provide="datatable" 
                data-display-rows="10"
                data-info="true"
                data-search="true"
                data-length-change="true"
                data-paginate="true"
              >
                  <thead>
                    
                    <tr>
                      
                      <th>Bus Number</th>
                      <th>No. of Passengers</th>
                      <th>Commission (x70)</th>
                      <th>Total Revenue</th>
                      <th>Trip Date</th>
                      <th>Breakdown</th>
                      
                    </tr>
                  </thead>
                  <tbody>
                    {{-- */$x=0; $tf =0; $de=0; $fuel=0; $ae = 0; $oe = 0; $transited = 0; $commission=0 /* --}}
                    @foreach($dailytrips as $item)
                        {{-- */  

                          //dd($item->toArray());

                          $tf += $item->total_fare;     
                          $de += $item->driver_allowance;     
                          $fuel += $item->fuel_expense;     
                          $oe += $item->other_expense;

                          $ae += $item->expenses()->sum('amount');  

                          $x +=  intVal($item->booked_seats);

                          $commission += ($item->booked_seats) * 70;

                          if($item->status == 'TRANSITED')
                            ++$transited;


                        /* --}}
                        <tr>
                            <td>
                               {{ (!empty($item->bus))?$item->bus->number_plate:'' }}
                            </td>
                            <td>
                              {{ $item->booked_seats }}                              
                            </td>
                            <td>
                              <span> &#8358; {{ number_format(($item->booked_seats) * 70) }}</span>
                            </td>
                            <td>
                              &#8358;{{ number_format($item->total_fare, 2) }}
                            </td>
                            <td>
                              {{ date('d-m-y', strtotime($item->created_at)) }}
                            </td>
                            <td style="font-size: 12px">
                               <!-- show the breakkdown -->                                
                                <?php
                                    $destAmount = array();
                                    $destCount = array();
                                ?>
                                
                                 @foreach($item->bookings as $b)
                                    
                                  <?php 
                                  if(isset($destAmount[$b->trip->destpark->name]))
                                     $destAmount[$b->trip->destpark->name] += $b->final_cost; 
                                   else
                                    $destAmount[$b->trip->destpark->name] = $b->final_cost;

                                  if(isset($destCount[$b->trip->destpark->name]))
                                     $destCount[$b->trip->destpark->name] += $b->passenger_count; 
                                   else
                                   $destCount[$b->trip->destpark->name] = $b->passenger_count; 
                                  ?> 
                                 @endforeach
                                 
                                 @foreach($destAmount as $k=>$v)
                                    <b>{{dd($k)}}, </b>
                                
                                 @endforeach
                            
                            </td>
                            
                            
                        </tr>
                    @endforeach
                        <tr class="row-subtotal">
                        <td colspan="1"><strong>Total</strong></td>
                        <td>{{$x}}</td>
                        <td>
                              
                              <span style="color:green;font-weight:bold;"> &#8358;{{$commission}}</span>
                            </td>
                            <td>
                              <span style="color:green;font-weight:bold;"> &#8358;{{ number_format($tf, 2) }}</span>
                              
                            </td>
                            <td></td>
                            <td>                              
                            </td>
                      </tr>
                      
                  </tbody>
                </table>



                <!-- setting totals on top -->
                <script type="text/javascript">

                  $(function() {
                    // $("#totalRev").html("&#8358;{{ number_format($tf, 2) }}");
                    // $("#totalExp").html("&#8358;{{ number_format(($ae), 2) }}");
                    // $("#netRev").html("&#8358;{{ number_format((($tf + $extlug) - $expnz), 2) }}");
                    // $("#totalTrips").html("{{ number_format($transited) }}");
                  });
                </script>


            </div> <!-- /.tab-pane --> 

                

            </div>

        </div> <!-- /.col -->

      </div> <!-- /.row -->


<script type="text/javascript">

function updateUrl(name, value){
            window.location.search = jQuery.query.set(name, value);

            return false;
       } 

function refreshReport(){

  console.log($('#select-input').val());
  console.log($('#park-input').val());
  
  window.location = '{{ route("invoice-reports") }}/'+$('#select-input').val()+'/'+$('#park-input').val();


}


$(function() {

    function cb(start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    }
    cb(moment("{{ $start_date }}"), moment("{{ $end_date }}"));

    $('#reportrange').daterangepicker({
        ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cb);

    $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
        console.log(picker.startDate.format('YYYY-MM-DD'));
        console.log(picker.endDate.format('YYYY-MM-DD'));
        
        updateUrl('daterange', picker.startDate.format('YYYY-MM-DD')+' '+picker.endDate.format('YYYY-MM-DD'));
    });

});
</script>




 @endsection     