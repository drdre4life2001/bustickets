@extends('layouts.master')

@section('content')

    <div class="content-header">
        <h2 class="content-header-title">Reports</h2>
        <ol class="breadcrumb">
            <li><a href="./index.html">Dashboard</a></li>
            <li class="active">Reports</li>
        </ol>
    </div> <!-- /.content-header -->
    <div class="row">
        <div class="col-md-12 col-sm-8">
            <div class="tab-content stacked-content">
                <div class="tab-pane fade in active" id="profile-tab">
                    <h3 class="">Summarised Invoice Report</h3>
                    <br/>

                    <hr/>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-sm-8">
                    <div class="tab-pane fade in active" id="profile-tab">
                        <div class="btn-group pull-right">
                            <div id="reportrange" class="pull-right"
                                 style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
                                <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
                                <span></span> <b class="caret"></b>
                            </div>
                        </div>
                        <h4 class="heading-inline pull-right">&nbsp;&nbsp;
                            <small>Choose date range</small>
                            &nbsp;&nbsp;</h4>
                        <hr/>
                        <br/><br/>
                    <!--  </div> -->

                        <div class="table-responsive">

                            <table id="exportTable"
                                   class="table table-striped table-bordered table-hover table-highlight table-checkable"
                                   data-provide="datatable"
                                   data-display-rows="10"
                                   data-info="true"
                                   data-search="true"
                                   data-length-change="true"
                                   data-paginate="true"
                            >
                                <thead>

                                <tr>

                                    <th>Total Passengers</th>
                                    <th>Destinations</th>
                                    <th>Date Range</th>
                                    <th>Total Revenue</th>
                                    <th>Bus.com.ng Commission</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>{{ number_format($revenue_data[0]->total_passengers) }}</td>
                                    <td>
                                        <table>
                                            @foreach($parks_data as $park)
                                                <tr>
                                                    <td>{{ $park->destination }}</td>
                                                    <td style="width: 26px"> </td>
                                                    <td>{{ $park->passengers }} Passengers</td>
                                                </tr>
                                            @endforeach
                                        </table>
                                    </td>
                                    <td>{{ $start_date }} - {{ $end_date }}</td>
                                    <td>&#x20A6; {{ number_format($revenue_data[0]->total_revenue, 2) }}</td>
                                    <td>&#x20A6; {{ number_format($revenue_data[0]->total_passengers * 70 , 2) }}</td>
                                </tr>
                                </tbody>
                            </table>
                            <!-- setting totals on top -->
                        </div> <!-- /.tab-pane -->


                    </div>

                </div> <!-- /.col -->

            </div> <!-- /.row -->


            <script type="text/javascript">

                function updateUrl(name, value) {
                    window.location.search = jQuery.query.set(name, value);

                    return false;
                }

                function refreshReport() {

                    console.log($('#select-input').val());
                    console.log($('#park-input').val());

                    window.location = '{{ route("invoice-reports") }}/' + $('#select-input').val() + '/' + $('#park-input').val();


                }


                $(function () {

                    function cb(start, end) {
                        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                    }

                    cb(moment("{{ $start_date }}"), moment("{{ $end_date }}"));

                    $('#reportrange').daterangepicker({
                        ranges: {
                            'Today': [moment(), moment()],
                            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                            'This Month': [moment().startOf('month'), moment().endOf('month')],
                            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                        }
                    }, cb);

                    $('#reportrange').on('apply.daterangepicker', function (ev, picker) {
                        console.log(picker.startDate.format('YYYY-MM-DD'));
                        console.log(picker.endDate.format('YYYY-MM-DD'));

                        updateUrl('daterange', picker.startDate.format('YYYY-MM-DD') + ' ' + picker.endDate.format('YYYY-MM-DD'));
                    });

                });
            </script>




@endsection
