{{ count($bookings) }} <a type="button" href="#" class=" " data-toggle="modal" data-target="#passModal{{ $phone }}">booking(s)</a> &#8358;{{ number_format($bookings->sum('final_cost'),2) }}

<div class = "modal fade" id="passModal{{ $phone }}" tabindex = "-1" role = "dialog" aria-labelledby = "myModalLabel" aria-hidden = "true">
   <div class = "modal-dialog">
      <div class = "modal-content">
         <div class = "modal-header">
            <button type = "button" class = "close" data-dismiss = "modal" aria-hidden = "true">
                  &times;
            </button>
            <h4 class = "modal-title" id = "myModalLabel">
               Booking details
            </h4>
         </div>

         <div class="modal-body">

         <div class="table-responsive">

              <table 
                class="table table-striped table-bordered table-hover table-highlight table-checkable" 
                data-provide="datatable" 
                data-display-rows="10"
                data-info="true"
                data-search="true"
                data-length-change="true"
                data-paginate="true"
              >
                  <thead>
                    <tr>
                      <th>Booking code</th>
                      <th>From</th>
                      <th>To</th>
                      <th>Date</th>
                      <th>Final Cost</th>
                      <th>Status</th>
                      <th>Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    {{-- */$x=0;/* --}}
                    @foreach($bookings as $booking)

                        {{-- */$x++;/* --}}
                        <tr>
                            <td>{{ $booking->booking_code }}</td>
                            <td>{{ $booking->trip->sourcepark->name }}</td>
                            <td>{{ $booking->trip->destpark->name }}</td>
                            <td>{{ date('M dS', strtotime($booking->date)) }}</td>
                            <td>&#8358;{{ number_format($booking->final_cost) }}</td>
                            <td>{{ $booking->status}} </td>
                            <td>
                                <a href="{{ url('bookings/' . $booking->id ) }}" target="_blank" title="View">
                                    <button type="submit" class="btn btn-primary"><i class="fa fa-eye"></i></button>
                                </a> 

                               
                            </td>
                        </tr>
                    @endforeach
                  </tbody>
                </table>


            </div> <!-- /.tab-pane --> 
            </div>
         
         <div class = "modal-footer">
            <button type = "button" class = "btn btn-default" data-dismiss = "modal">
               Close
            </button>
         </div>
      </div><!-- /.modal-content -->
   </div><!-- /.modal-dialog -->
</div><!-- /.modal -->