@extends('layouts.master')

@section('content')

<div class="content-header">
        <h2 class="content-header-title">Manifest Reports</h2>
        <ol class="breadcrumb">
          <li><a href="./index.html">Dashboard</a></li>
          <li class="active">Reports</li>
        </ol>
      </div> <!-- /.content-header -->

      

      <div class="row">

       

        <div class="col-md-12 col-sm-8">

          <div class="tab-content stacked-content">

            <div class="tab-pane fade in active" id="profile-tab">
              
              
              <h4 class="heading-inline pull-left">
          &nbsp;&nbsp;<small>Choose Bus</small>
          &nbsp;&nbsp;</h4>
              <div class="btn-group pull-left">
                  <div class="form-group">
                    <select id="select-input" class="form-control" onchange="refreshReport();">
                      <option value="" @if(empty($bus_id)) selected="selected" @endif > -ALL- </option>
                      @foreach($buses as $key => $value)
                        <option value="{{ $key }}" @if($key == $bus_id) selected="selected" @endif>{{ $value }}</option>
                      @endforeach
                    </select>
              </div>
              </div>
              <h4 class="heading-inline pull-left">
          &nbsp;&nbsp;<small>Choose Park</small>
          &nbsp;&nbsp;</h4>
              <div class="btn-group pull-left">
                  <div class="form-group">
                    <select id="park-input" class="form-control" onchange="refreshReport();">
                      <option value="0" @if(empty($park_id)) selected="selected" @endif > -ALL- </option>
                      @foreach($parks as $key => $value)
                        <option value="{{ $key }}" @if($park_id == $key) selected="selected" @endif>{{ $value }}</option>
                      @endforeach
                      
                    </select>
                  </div>

                  
              </div>

              <div class="btn-group pull-right">
                  <div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
                      <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
                      <span></span> <b class="caret"></b>
                  </div>
              </div>
          <h4 class="heading-inline pull-right">
          &nbsp;&nbsp;<small>Choose date range</small>
          &nbsp;&nbsp;</h4>



              <hr />

              <br /><br />

              <h3 class="">{{ (isset($bus))?$bus.' Manifest':"All Manifests" }} Report</h3>
              <!-- <h5 style="float:right;"></h5> -->
              <br />



              <div class="table-responsive">

              <table 
                class="table table-striped table-bordered table-hover table-highlight table-checkable" 
                data-provide="datatable1" 
                data-display-rows="10"
                data-info="true"
                data-search="true"
                data-length-change="true"
                data-paginate="true"
              >
                  <thead>
                    
                    <tr>
                      
                      <th>Bus</th>
                      <th>Number of passengers</th>
                      <th>Number of Trips</th>
                      <th>Total Fare</th>
                      <th>&nbsp;</th>
                      
                    </tr>
                  </thead>
                  <tbody>
                    {{-- */$x=0; $tf =0; $de=0; $fuel=0; $oe = 0; /* --}}
                    @foreach($dailytrips as $item)
                      <?php if(empty($item->bus)) continue; ?>
                        {{-- */  
                          $tf += $item->total_fare;     
                          $de += $item->driver_allowance;     
                          $fuel += $item->fuel_expense;     
                          $oe += $item->other_expense;     

                        /* --}}
                        <tr  id="trips_num{{ $item->bus_id }}" class="loadBusInfo" data-bus-id="{{ $item->bus_id }}" data-bus-number="{{ $item->bus->number_plate }}" >
                            <td>
                              {{ (!empty($item->bus))?$item->bus->number_plate:"" }}
                            </td>
                            
                            <td colspan=6 >
                              loading....
                              
                            </td>
                            
                        </tr>
                    @endforeach
                        
                      
                  </tbody>
                </table>


            </div> <!-- /.tab-pane --> 

                <!-- <div class="btn-group">
                <button data-toggle="dropdown" class="btn btn-info dropdown-toggle" type="button">
                  <i class="fa fa-download"></i> Download <span class="caret"></span>
                </button>
                <ul role="menu" class="dropdown-menu">
                  <li><a href="javascript:;">Download as PDF</a></li>
                  <li><a href="javascript:;">Download as Excel</a></li>
                  
                </ul>
              </div> -->

            </div>

        </div> <!-- /.col -->

      </div> <!-- /.row -->


<script type="text/javascript">

function updateUrl(name, value){
            window.location.search = jQuery.query.set(name, value);

            return false;
       } 

function refreshReport(){

  console.log($('#select-input').val());
  // if($('#select-input').val() == "")
  //   window.location = '{{ route("manifest-reports") }}';
  if($('#select-input').val() != ""){
    window.location = '{{ route("manifest-reports") }}/'+$('#select-input').val()+'/'+$('#park-input').val();
  }else if($('#select-input').val() == "" && $('#park-input').val() != "") {
    window.location = '{{ route("manifest-reports") }}/0'+'/'+$('#park-input').val();
  }


}


$(function() {

    function cb(start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    }
    cb(moment("{{ $start_date }}"), moment("{{ $end_date }}"));

    $('#reportrange').daterangepicker({
        ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cb);

    $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
        console.log(picker.startDate.format('YYYY-MM-DD'));
        console.log(picker.endDate.format('YYYY-MM-DD'));
        
        updateUrl('daterange', picker.startDate.format('YYYY-MM-DD')+' '+picker.endDate.format('YYYY-MM-DD'));
    });

     $( ".loadBusInfo" ).each(function( index, element ) {
         var bus_id = $(element).attr('data-bus-id');
         var bus_number = $(element).attr('data-bus-number');
        // alert(bus_number);
         // $( element ).html( '<span style="font-style:italic;font-size:80%;"><img width="15px" src="{{ asset("desktop/img/ajax-loader-fb.gif") }}">');
         $.ajax
         (
             {
                type: "POST",
                url: "{{ url('get-bus-manifests') }}",
                data: ({ rnd : Math.random() * 100000, park_id:'{{ $park_id }}', bus_id: bus_id, bus_number: bus_number,start_date: '{{ $start_date }}' , end_date: '{{ $end_date }}', _token: '{{ csrf_token() }}' }),
                success: function(html)
                {
                     $( element ).html(html);
                     $(element).removeClass('loadBusInfo');

                }
          });

     });

});
</script>




 @endsection     