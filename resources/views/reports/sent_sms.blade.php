@extends('layouts.master')

@section('content')

    <div class="content-header">
        <h2 class="content-header-title">Reports</h2>

    </div> <!-- /.content-header -->



    <div class="row">


        <div class="col-md-12 col-sm-8">

            <div class="tab-content stacked-content">

                <div class="tab-pane fade in active" id="profile-tab">

                    <h3 class="">Daily Trips Report</h3>
                    <br/>

                    <hr />
                </div>
            </div>

            <div class="row">


                <div class="col-md-12 col-sm-8">



                    <div class="tab-pane fade in active" id="profile-tab">


                        <h4 class="heading-inline pull-left">
                            &nbsp;&nbsp;<small>Choose Status</small>
                            &nbsp;&nbsp;</h4>
                        <div class="btn-group pull-left">
                            <div class="form-group">
                                <select id="select-input" class="form-control" onchange="refreshReport();">
                                    <option value="ALL" @if($status == 'ALL') selected="selected" @endif > -ALL- </option>
                                    @foreach($statuses as $s)
                                        <option value="{{ $s }}" @if($status == $s) selected="selected" @endif>{{ $s }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <h4 class="heading-inline pull-left">
                            &nbsp;&nbsp;<small>Choose Park</small>
                            &nbsp;&nbsp;</h4>
                        <div class="btn-group pull-left">
                            <div class="form-group">
                                <select id="park-input" class="form-control" onchange="refreshReport();">
                                    <option value="0" @if(empty($park_id)) selected="selected" @endif > -ALL- </option>
                                    @foreach($parks as $key => $value)
                                        <option value="{{ $key }}" @if($park_id == $key) selected="selected" @endif>{{ $value }}</option>
                                    @endforeach

                                </select>
                            </div>


                        </div>

                        <div class="btn-group pull-right">
                            <div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
                                <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
                                <span></span> <b class="caret"></b>
                            </div>
                        </div>
                        <h4 class="heading-inline pull-right">
                            &nbsp;&nbsp;<small>Choose date range</small>
                            &nbsp;&nbsp;</h4>



                        <hr />

                        <br /><br />

                        <div class="well">
                            <h4><span class="text-primary">Summary</span></h4>

                            <p>
                                <span class="text-primary">Total Revenue: </span><strong id="totalRev"> </strong>|
                                <span class="text-primary">Total Expense: </span><strong  id="totalExp"> </strong> |
                                <span class="text-primary">Net Revenue: </span><strong id="netRev"> </strong> |
                                <span class="text-primary">Total Transited Trips: </span> <strong id="totalTrips"> </strong> |
                                {{-- */$x=0; $extlug =0;$expnz=0; /* --}}
                                @foreach($dailytrips as $dailytrip)

                                    @foreach($dailytrip->bookings as $b)
                                        {{-- */
                                                          $extlug += $b->extra_luggages->sum('amount');
                                         /* --}}
                                    @endforeach

                                    @foreach($dailytrip->expenses as $exps)
                                        {{-- */
                                                          $expnz += $exps->amount;
                                         /* --}}
                                    @endforeach

                                @endforeach

                                <span class="text-primary">Extra Luggages Revenue: </span><strong >{{$extlug}} </strong>
                            </p>

                        </div>

                        <div class="table-responsive">

                            <table id="exportTable"
                                   class="table table-striped table-bordered table-hover table-highlight table-checkable"
                                   data-provide="datatable"
                                   data-display-rows="10"
                                   data-info="true"
                                   data-search="true"
                                   data-length-change="true"
                                   data-paginate="true"
                            >
                                <thead>

                                <tr>

                                    <th>Trip Details</th>
                                    <th>Bookings Details</th>
                                    <th>Total Revenue</th>
                                    <th>Total Expenses</th>
                                    <th>Net Revenue</th>
                                    <th>Breakdown</th>

                                </tr>
                                </thead>
                                <tbody>
                                {{-- */$x=0; $tf =0; $de=0; $fuel=0; $ae = 0; $oe = 0; $transited = 0; /* --}}
                                @foreach($dailytrips as $item)


                                    {{-- */

                                      //dd($item->toArray());

                                      $tf += $item->total_fare;
                                      $de += $item->driver_allowance;
                                      $fuel += $item->fuel_expense;
                                      $oe += $item->other_expense;

                                      $ae += $item->expenses()->sum('amount');

                                      $x +=  intVal($item->booked_seats);

                                      if($item->status == 'TRANSITED')
                                        ++$transited;


                                    /* --}}
                                    <tr>
                                        <td>
                                            <strong>Trip Date:</strong> {{ date('D, M d, Y',strtotime($item->trip_date)) }}<br/>
                                            <strong>From:</strong> {{ $item->trip->sourcepark->name }}<br/>
                                            <strong>To:</strong> @foreach($item->trip->subTrips as $st) {{ $st->destpark->name }}<br/> @endforeach
                                            <strong>Time:</strong> {{ date('h:iA', strtotime($item->trip->departure_time)) }}<br/>
                                            <strong>Bus Type:</strong> {{ (!empty($item->bus))?$item->bus->bus_type->name:'' }}<br/>
                                            <strong>Fare:</strong> &#8358;{{ number_format($item->trip->fare, 2) }}<br/>
                                        </td>
                                        <td>
                                            <strong>Driver:</strong> {{ (!empty($item->driver))?$item->driver->name:'' }}<br/>
                                            <strong>Bus Number:</strong> {{ (!empty($item->bus))?$item->bus->bus_number:"" }}<br/>
                                            <strong>Bus number plate:</strong> {{ (!empty($item->bus))?$item->bus->number_plate:'' }}<br/>
                                            <strong>Booked seats:</strong> {{ $item->booked_seats }}<br/>
                                            <strong>Total seats:</strong> {{ $item->total_seats }}<br/>

                                        </td>
                                        <td>

                                            <span style="color:green;"> &#8358;{{ number_format($item->total_fare, 2) }}</span>
                                        </td>
                                        <td>
                                            @foreach($item->expenses as $e)
                                                <?php if(empty($e->amount)) continue; ?>
                                                <strong>{{ $e->expense_name }}:</strong><span style="color:red;"> &#8358;{{ number_format($e->amount, 2) }}</span><br/>

                                            @endforeach

                                            {{-- */$x=0; $extExp=0; /* --}}
                                            @foreach($item->bookings as $bk)
                                                @foreach($bk->extra_luggages as $lug)

                                                    {{-- */
                                                            $extExp += $lug->amount;
                                                       /* --}}

                                                @endforeach

                                            @endforeach

                                        </td>
                                        <td>
                                            <span style="color:green;font-weight:bold;"> &#8358;{{ number_format(($item->total_fare+$extExp) - $item->expenses()->sum('amount'), 2) }}</span>

                                        </td>
                                        <td>
                                            <!-- show the breakkdown -->
                                            <b><u>DESTINATION BREAKDOWN</u></b><br><br>

                                            <?php
                                            $destAmount = array();
                                            $destCount = array();
                                            ?>

                                            @foreach($item->bookings as $b)

                                                <?php
                                                if(isset($destAmount[$b->trip->destpark->name]))
                                                    $destAmount[$b->trip->destpark->name] += $b->final_cost;
                                                else
                                                    $destAmount[$b->trip->destpark->name] = $b->final_cost;

                                                if(isset($destCount[$b->trip->destpark->name]))
                                                    $destCount[$b->trip->destpark->name] += $b->passenger_count;
                                                else
                                                    $destCount[$b->trip->destpark->name] = $b->passenger_count;
                                                ?>
                                            @endforeach

                                            @foreach($destAmount as $k=>$v)
                                                {{$k}} &nbsp;&nbsp;&nbsp;&nbsp; - &#8358;{{ number_format($v) }} &nbsp;&nbsp;&nbsp;&nbsp;  ({{ $destCount[$k] }} pax) <br>

                                            @endforeach
                                            @if(!empty($item->ticketer))
                                                <br/><br/><b><u>TICKETER</u> </b><br>
                                                {{ $item->ticketer->first_name.' '.$item->ticketer->last_name }}
                                            @endif
                                        </td>


                                    </tr>
                                @endforeach
                                <tr class="row-subtotal">
                                    <td colspan="1"></td>
                                    <td><strong>Subtotal:</strong></td>
                                    <td>

                                        <span style="color:green;font-weight:bold;"> &#8358;{{ number_format($tf, 2) }}</span>
                                    </td>
                                    <td>
                                        <span style="color:red;font-weight:bold;"> - &#8358;{{ number_format($ae, 2) }}</span>

                                    </td>
                                    <td>
                                        <span style="color:green;font-weight:bold;">&#8358;{{ number_format((($tf + $extlug) - $expnz), 2) }}</span>

                                    </td>
                                    <td>
                                        &nbsp;

                                    </td>
                                </tr>

                                </tbody>

                            </table>

                            <!-- setting totals on top -->
                            <script type="text/javascript">

                                $(function() {
                                    $("#totalRev").html("&#8358;{{ number_format($tf, 2) }}");
                                    $("#totalExp").html("&#8358;{{ number_format(($ae), 2) }}");
                                    $("#netRev").html("&#8358;{{ number_format((($tf + $extlug) - $expnz), 2) }}");
                                    $("#totalTrips").html("{{ number_format($transited) }}");
                                });
                            </script>

                            <?php

                            $to = env("Chairman") ;
                            $total_revenue =  number_format($tf, 2) ;
                            $total_expenditure =  number_format(($ae), 2);
                            $net_revenue =  number_format((($tf + $extlug) - $expnz), 2) ;
                            $source_park =  $parks[1];
                            //dd($source_park);
                            $msg = "$source_park Report:\nTotal Revenue: " . $total_revenue. " \nTotal Expenses: " . $total_expenditure  . "\nNet Revenue: " . $net_revenue;


                                $cid = "";
                                $user = "info@bus.com.ng";
                                // $senderArray = 'Buscomng';
                                $senderId = env('SMS_USERNAME');
                                $to = $to;

                                $pass = env('SMS_PASS');
                                $ch = curl_init();
                                // dd($msg);
                                $postdata = 'username=' . $user . '&password=' . $pass . '&message=' . $msg . '&sender=' . $senderId . '&mobiles=' . $to; //initialize the request variable
                                // echo $postdata;
                                $nmsg = urlencode($msg);


                                $curl = curl_init();

                                curl_setopt_array($curl, array(
                                    CURLOPT_URL => "http://portal.nigerianbulksms.com/api/?username=$user&password=$pass&message='$nmsg'&sender=$senderId&mobiles=$to",
                                    CURLOPT_RETURNTRANSFER => true,
                                    CURLOPT_ENCODING => "",
                                    CURLOPT_MAXREDIRS => 10,
                                    CURLOPT_TIMEOUT => 30,
                                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                                    CURLOPT_CUSTOMREQUEST => "GET",
                                    CURLOPT_HTTPHEADER => array(
                                        "cache-control: no-cache",

                                    ),
                                ));

                                $response = curl_exec($curl);
                                // dd($response);
                                $err = curl_error($curl);

                                curl_close($curl);
                                return $response;




                            ?>



                        </div> <!-- /.tab-pane -->



                    </div>

                </div> <!-- /.col -->

            </div> <!-- /.row -->


            <script type="text/javascript">

                function updateUrl(name, value){
                    window.location.search = jQuery.query.set(name, value);

                    return false;
                }

                function refreshReport(){

                    console.log($('#select-input').val());
                    console.log($('#park-input').val());

                    {{--window.location = '{{ route("trips-reports") }}/'+$('#select-input').val()+'/'+$('#park-input').val();--}}


                }


                $(function() {

                    function cb(start, end) {
                        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                    }
                    cb(moment("{{ $start_date }}"), moment("{{ $end_date }}"));

                    $('#reportrange').daterangepicker({
                        ranges: {
                            'Today': [moment(), moment()],
                            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                            'This Month': [moment().startOf('month'), moment().endOf('month')],
                            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                        }
                    }, cb);

                    $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
                        console.log(picker.startDate.format('YYYY-MM-DD'));
                        console.log(picker.endDate.format('YYYY-MM-DD'));

                        updateUrl('daterange', picker.startDate.format('YYYY-MM-DD')+' '+picker.endDate.format('YYYY-MM-DD'));
                    });

                });
            </script>




@endsection