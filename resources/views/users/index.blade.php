@extends('layouts.master')

@section('content')

    <div class="content-header">
        <h2 class="content-header-title">Users </h2>
        <ol class="breadcrumb">
          <li><a href="{{ url('/') }}">Dashboard</a></li>
          <li class="active">Users </li>
        </ol>
      </div> <!-- /.content-header -->



      <div class="row">

        <div class="col-md-10 col-sm-8">

          <div class="portlet">

            <div class="portlet-content">

              <div class="table-responsive">

              <table
                class="table table-striped table-bordered table-hover table-highlight table-checkable"
                data-provide="datatable"
                data-display-rows="10"
                data-info="true"
                data-search="true"
                data-length-change="true"
                data-paginate="true"
              >
                  <thead>
                    <tr>
                      <th>S.No</th><th>Last Name</th><th>First Name</th><th>Email</th><th>Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    {{-- */$x=0;/* --}}
                    @foreach($users as $item)
                        {{-- */$x++;/* --}}
                        @if(Auth::user()->role_id == 3 && $item->role_id == 4)
                         @continue
                        @endif
                        <tr>
                            <td>{{ $x }}</td>
                            <td><a href="{{ url('users', $item->id) }}">{{ $item->last_name }}</a></td>
                            <td>{{ $item->first_name }}</td>
                            <td>{{ $item->email }}</td>
                            <td>
                                <a href="{{ url('users/' . $item->id . '/edit') }}" title="Edit">
                                    <button type="submit" class="btn btn-default btn-xs"><i class="fa fa-edit"></i></button>
                                </a>
                                {!! Form::open([
                                    'method'=>'DELETE',
                                    'url' => ['users', $item->id],
                                    'style' => 'display:inline',
                                ]) !!}
                                    {!! Form::button('<i class="fa fa-times"></i>',
                                      ['class' => 'btn btn-danger btn-xs',
                                      'escape'=>false,
                                      'title'=>'Delete',
                                        'onclick'=>'confirm("Are you sure you want to delete this ticketer");',
                                      'type'=>'submit']) !!}
                                {!! Form::close() !!}
                            </td>
                        </tr>
                    @endforeach
                  </tbody>
                </table>
              </div> <!-- /.table-responsive -->


            </div> <!-- /.portlet-content -->

          </div> <!-- /.portlet -->



        </div> <!-- /.col -->

        <div class="col-md-2 col-sm-4">

            <ul id="myTab" class="nav nav-pills nav-stacked">
            <li class="active">
              <a href="{{ url('users/create') }}">
                <i class="fa fa-plus"></i>
                Add New User
              </a>
            </li>
            @if(Auth::user()->role_id == 1)
            <li class="active">
              <a href="{{ url('users/'. Auth::user()->id. '/edit') }}">
                <i class="fa fa-user"></i>
                Edit Admin
              </a>
            </li>
            @endif
          </ul>

        </div>

      </div> <!-- /.row -->



@endsection
