@extends('layouts.auth')

@section('content')

<div class="account-body">

      <h3 class="account-body-title">Change Password.</h3>

      <h5 class="account-body-subtitle">Please change your password</h5>

      
      
      <form method="POST" action="{{ url('change-password') }}" class="form account-form" >          

        {!! csrf_field() !!}


        <div class="alert alert-info">
          {!! $message !!}
        </div>

        

        <div class="form-group">
          <label for="login-password" class="placeholder-hidden">Former Password</label>
          <input type="password" class="form-control" name="password" id="login-password" placeholder="Your former Password" tabindex="2">
            @if ($errors->has('password'))
                <span class="parsley-error-list">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
            @endif
        </div> <!-- /.form-group -->

        <div class="form-group">
          <label for="login-password" class="placeholder-hidden">New Password</label>
          <input type="password" class="form-control" name="new_password" id="login-password" placeholder="New Password" tabindex="2">
            @if ($errors->has('new_password'))
                <span class="parsley-error-list">
                    <strong>{{ $errors->first('new_password') }}</strong>
                </span>
            @endif
        </div> <!-- /.form-group -->


         <div class="form-group">
          <label for="login-password" class="placeholder-hidden">Former Password</label>
          <input type="password" class="form-control" name="password_again" id="login-password" placeholder="New Password again" tabindex="2">
            @if ($errors->has('password_again'))
                <span class="parsley-error-list">
                    <strong>{{ $errors->first('password_again') }}</strong>
                </span>
            @endif
        </div> <!-- /.form-group -->

        

        <div class="form-group">
          <button type="submit" class="btn btn-primary btn-block btn-lg" tabindex="4">
            Change &nbsp; <i class="fa fa-play-circle"></i>
          </button>
        </div> <!-- /.form-group -->

        <a href="#" onclick="window.history.back();">Cancel</a>

      </form>


    </div> <!-- /.account-body -->

    <div class="account-footer">
     <!--  <p>
      Don't have an account? &nbsp;
      <a href="{{ url('/register') }}" class="">Create an Account!</a>
      </p> -->
    </div> <!-- /.account-footer -->


@endsection
