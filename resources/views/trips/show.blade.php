@extends('layouts.master')

@section('content')

<style type="text/css">
  #holder{  
   height:200px;   
   width:550px;
   background-color:#F5F5F5;
   border:1px solid #A4A4A4;
   margin-left:10px;  
  }
   #place {
   position:relative;
   margin:7px;
   }
     #place a{
   font-size:0.6em;
   }
     #place li
     {
         list-style: none outside none;
         position: absolute;   
     }    
     #place li:hover
     {
        background-color:yellow;      
     } 
   #place .seat{
   background:url("{{ asset('bckend/img/available_seat_img.gif') }}") no-repeat scroll 0 0 transparent;
   height:33px;
   width:33px;
   display:block;  
   }

   #place .no-seat{
   /*background:url("{{ asset('bckend/img/available_seat_img.gif') }}") no-repeat scroll 0 0 transparent;*/
   height:33px;
   width:33px;
   display:block;  
   }


      #place .selectedSeat
      { 
    background-image:url('{{ asset('bckend/img/booked_seat_img.gif') }}');        
      }
     #place .selectingSeat
      { 
    background-image:url("{{asset('bckend/img/selected_seat_img.gif') }}");        
      }
    #place .row-3, #place .row-4{
    margin-top:10px;
    }
   #seatDescription{
   padding:0px;
   }
    #seatDescription li{
    verticle-align:middle;    
    list-style: none outside none;
     padding-left:35px;
    height:35px;
    float:left;
    }
    </style>


<div class="content-header">
        <h2 class="content-header-title">Destination Details and stops </h2>
        <ol class="breadcrumb">
          <li><a href="{{ url('/') }}">Dashboard</a></li>
          <li><a href="{{ url('destinations') }}">Destinations </a></li>
          <li class="active">Destination Details and stops </li>
        </ol>
      </div> <!-- /.content-header -->

<div class="container">
    
<br><br>

<div class="alert alert-success hide" id="paidAlertDiv">
  Booking has been marked as PAID. <!--You can now print the Ticket-->
</div>

<div class="col-md-4 col-sm-2">
    
    <div class="portlet">

            
            <div class="portlet-content">

              <dl>
                <dt>Name </dt>
                <dd>{{ $trip->name }}</dd>

                <dt>Source</dt>
                <dd>{{ $trip->sourcepark->name}}</dd>

                <dt>Destination </dt>
                <dd>{{ $trip->destpark->name}}</dd>

                <dt>Created date</dt>
               
                <dd> {{ date('D, d/m/Y', strtotime($trip->created_at)) }} </dd>

              </dl>

             


              

            </div> <!-- /.portlet-content -->

          </div>
</div>

<div class="col-md-8 col-sm-4">

    <div class="portlet">

            <div class="portlet-header">

              <h3>
                <i class="fa fa-reorder"></i>
                Stops
              </h3>

            </div> <!-- /.portlet-header -->

            <div class="portlet-content">           

              <div class="table-responsive">

              <table 
                class="table table-stripped table-bordered table-hover table-highlight table-checkable" 
                data-provide="datatable1" 
                data-display-rows="10"
                data-info="true"
                data-search="true"
                data-length-change="true"
                data-paginate="true"
              >
                  <thead>
                    <tr>
                      <th>Source</th>
                      <th>Destination</th>
                      <th>Fare</th>
                      <th>Round Trip Fare</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                    {{-- */$x=0;/* --}}
                    @foreach($trip->subTrips as $stop)
                       <tr> 
                        <td>{{$stop->sourcepark->name}}</td>
                        <td>{{$stop->destpark->name}}</td>
                        <td>{{ number_format($stop->fare) }}</td>
                        <td>{{ ($stop->round_trip_status == 1)? number_format($stop->round_trip_fare) : "" }}</td>
                        
                        
                        <td>
                            <div class = "modal fade" id="editStopModal{{ $stop->id }}" tabindex = "-1" role = "dialog" aria-labelledby = "myModalLabel" aria-hidden = "true">
   <div class = "modal-dialog">
      <div class = "modal-content">
         <div class = "modal-header">
            <button type = "button" class = "close" data-dismiss = "modal" aria-hidden = "true">
                  &times;
            </button>
            <h4 class = "modal-title" id = "myModalLabel">
               Add Stop
            </h4>
         </div>
         <form action="{{ url('edit-stop') }}" method="post" id="bookingNotes33">
         <div class = "modal-body" id="contentArea">
              <input type="hidden" id="SubBtn" name="_token" value="<?php echo csrf_token(); ?>">
              <input type="hidden" name="parent_trip_id" value="{{ $trip->id }}">
              <input type="hidden" name="state_id" value="{{ $trip->destpark->state_id }}">
              <input type="hidden" name="source_park_id" value="{{ $trip->sourcepark->id }}">
              <input type="hidden" name="stop_id" value="{{ $stop->id }}">
          
             <div class="form-group {{ $errors->has('dest_park') ? 'has-error' : ''}}">
                {!! Form::label('dest_park_id', 'Destination Park: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    <select name="dest_park_id" class="form-control" required="">
                        <option value="">Choose Park</option>
                        @foreach($all_parks as $park)
                        <option value="{{ $park->id }}" @if($stop->dest_park_id == $park->id) selected="selected" @endif >{{ $park->name }}</option>
                        @endforeach
                    </select>
                {!! $errors->first('dest_park_id', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>
            <br style="clear:both;" />

             <div class="form-group {{ $errors->has('dest_park') ? 'has-error' : ''}}">
                {!! Form::label('fare', 'Fare: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    <input type="text" name="fare" class="form-control" required value="{{ $stop->fare }}" />
                {!! $errors->first('fare', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>

         </div>
         <div class = "modal-footer">
            <button type = "button" class = "btn btn-default" data-dismiss = "modal">
               Close
            </button>
            <button type = "submit" id="" class = "btn btn-primary">
               Save
            </button>
         </form>

         </div>
      </div><!-- /.modal-content -->
   </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

                            <button type="button" class="btn btn-default  btn-xs" data-toggle="modal" data-target="#editStopModal{{ $stop->id }}""> 
                <i class="fa fa-edit"></i>Edit
            </button>
                            {!! Form::open([
                                    'method'=>'POST',
                                    'url' => 'delete-stop',
                                    'style' => 'display:inline'
                                ]) !!}
                                <input type="hidden" name="id" value="{{ $stop->id }}">
                                <input type="hidden" name="parent_trip_id" value="{{ $stop->parent_trip_id }}">
                                    {!! Form::button('<i class="fa fa-times"></i>', ['class' => 'btn btn-danger btn-xs', 'escape'=>false,'title'=>'Delete', 'type'=>'submit']) !!}
                                {!! Form::close() !!}


                        </td>
                      </tr>      
                    @endforeach

                   
                  </tbody>
                </table>
              </div> <!-- /.table-responsive -->


              

            </div> <!-- /.portlet-content -->

          </div> <!-- /.portlet -->


              <button type="button" class="btn btn-success " data-toggle="modal" data-target="#addStopModal"> 
                Add Stop
            </button>
          

    
</div>
    




    
</div>


<div class = "modal fade" id="addStopModal" tabindex = "-1" role = "dialog" aria-labelledby = "myModalLabel" aria-hidden = "true">
   <div class = "modal-dialog">
      <div class = "modal-content">
         <div class = "modal-header">
            <button type = "button" class = "close" data-dismiss = "modal" aria-hidden = "true">
                  &times;
            </button>
            <h4 class = "modal-title" id = "myModalLabel">
               Add Stop
            </h4>
         </div>
         <form action="{{ url('add-stop') }}" method="post" id="bookingNotes33">
         <div class = "modal-body" id="contentArea">
              <input type="hidden" id="SubBtn" name="_token" value="<?php echo csrf_token(); ?>">
          <input type="hidden" name="parent_trip_id" value="{{ $trip->id }}">
          <input type="hidden" name="state_id" value="{{ $trip->destpark->state_id }}">
          <input type="hidden" name="source_park_id" value="{{ $trip->sourcepark->id }}">
          
             <div class="form-group {{ $errors->has('dest_park') ? 'has-error' : ''}}">
                {!! Form::label('dest_park_id', 'Destination Park: ', ['class' => 'col-sm-4 control-label']) !!}
                <div class="col-sm-7">
                    <select name="dest_park_id" class="form-control" required="">
                        <option value="">Choose Park</option>
                        @foreach($all_parks as $park)
                        <option value="{{ $park->id }}">{{ $park->name }}</option>
                        @endforeach
                    </select>
                {!! $errors->first('dest_park_id', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>
            <br style="clear:both;" />

             <div class="form-group {{ $errors->has('dest_park') ? 'has-error' : ''}}">
                {!! Form::label('fare', 'Regular Fare: ', ['class' => 'col-sm-4 control-label']) !!}
                <div class="col-sm-7">
                    <input type="text" name="fare" class="form-control" required />
                {!! $errors->first('fare', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>

            <br style="clear:both;" />

            <div class="form-group {{ $errors->has('has_round_trip') ? 'has-error' : ''}}">
                {!! Form::label('has_round_trip', 'Activate Round Trip? : ', ['class' => 'col-sm-4 control-label']) !!}
                <div class="col-sm-7">
             <div class="checkbox">
                <label>{!! Form::radio('has_round_trip', '1') !!} Yes</label>
            </div>
            <div class="checkbox">
                <label>{!! Form::radio('has_round_trip', '0', true) !!} No</label>
            </div>
                {!! $errors->first('has_round_trip', '<span class="parsley-error-list">:message</span>') !!}
                </div>
                
            </div>

            <br style="clear:both;" />

            
            <div class="form-group {{ $errors->has('round_trip_fare') ? 'has-error' : ''}}">
                {!! Form::label('round_trip_fare', 'Round Trip Fare: ', ['class' => 'col-sm-4 control-label']) !!}
                <div class="col-sm-7">
                    {!! Form::number('round_trip_fare', null, ['class' => 'form-control']) !!}
                {!! $errors->first('round_trip_fare', '<span class="parsley-error-list">:message</span>') !!}
                </div>
                
            </div>

            <br style="clear:both;" />


            <div class="form-group {{ $errors->has('ac') ? 'has-error' : ''}}">
                {!! Form::label('is_intl_trip', 'Is International Trip?: ', ['class' => 'col-sm-4 control-label']) !!}
                <div class="col-sm-7">
             <div class="checkbox">
                <label>{!! Form::radio('is_intl_trip', '1') !!} Yes</label>
            </div>
            <div class="checkbox">
                <label>{!! Form::radio('is_intl_trip', '0', true) !!} No</label>
            </div>
                {!! $errors->first('is_intl_trip', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>

            <br style="clear:both;" />

            <div class="intl_div" id="intl_div">
            <hr>  
             <h3> International Rates</h3>
             <div class="form-group {{ $errors->has('virgin_passport_fare') ? 'has-error' : ''}}">
                {!! Form::label('virgin_passport_fare', 'Virgin Passport Fare:', ['class' => 'col-sm-4 control-label']) !!}
                <div class="col-sm-7">
                     {!! Form::text('virgin_passport_fare',  0.00, ['class' => 'form-control', 'id'=>"vPass"] ) !!}
                     <p class="help-block">Leave blank or 0 is not applicable.</p>
                    {!! $errors->first('virgin_passport_fare', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <br style="clear:both;" />
            <div class="form-group {{ $errors->has('round_trip_virgin_passport_fare') ? 'has-error' : ''}}">
                {!! Form::label('round_trip_virgin_passport_fare', 'Virgin Passport Round Trip Fare:', ['class' => 'col-sm-4 control-label']) !!}
                <div class="col-sm-7">
                     {!! Form::text('round_trip_virgin_passport_fare',  0.00, ['class' => 'form-control', 'id'=>"vPass"] ) !!}
                     <p class="help-block">Leave blank or 0 is not applicable.</p>
                    {!! $errors->first('round_trip_virgin_passport_fare', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <br style="clear:both;" />
            <div class="form-group {{ $errors->has('no_passport_fare') ? 'has-error' : ''}}">
                {!! Form::label('no_passport_fare', 'Id Card/ No Passport Fare:', ['class' => 'col-sm-4 control-label']) !!}
                <div class="col-sm-7">
                     {!! Form::text('no_passport_fare',  0.00, ['class' => 'form-control', 'id'=>"noPass"] ) !!}
                     <p class="help-block">Leave blank or 0 is not applicable.</p>
                    {!! $errors->first('no_passport_fare', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <br style="clear:both;" />

            <div class="form-group {{ $errors->has('round_trip_no_passport_fare') ? 'has-error' : ''}}">
                {!! Form::label('round_trip_no_passport_fare', 'Id Card/ No Passport Round Trip Fare:', ['class' => 'col-sm-4 control-label']) !!}
                <div class="col-sm-7">
                     {!! Form::text('round_trip_no_passport_fare',  0.00, ['class' => 'form-control', 'id'=>"noPass"] ) !!}
                     <p class="help-block">Leave blank or 0 is not applicable.</p>
                    {!! $errors->first('round_trip_no_passport_fare', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
             <hr>
            </div>

            <br style="clear:both;" />
            
            
            
         </div>
         <div class = "modal-footer">
            <button type = "button" class = "btn btn-default" data-dismiss = "modal">
               Close
            </button>
            <button type = "submit" id="" class = "btn btn-primary">
               Save
            </button>
         </form>

         </div>
      </div><!-- /.modal-content -->
   </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


 <script>
  $(document).ready(function(){
    $("#intl_div").hide();
    $('input:radio[name="is_intl_trip"]').on('change',function(){
    if($(this).val() == 1)
    {
      $("#intl_div").show(); 
    }
    else
    {
      $("#intl_div").hide();
    }
});

  });
  </script>
    


@endsection