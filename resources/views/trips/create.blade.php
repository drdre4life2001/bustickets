@extends('layouts.master')

@section('content')

  


   <div class="content-header">
        <h2 class="content-header-title">Add Trip </h2>
        <ol class="breadcrumb">
          <li><a href="{{ url('/') }}">Dashboard</a></li>
          <li><a href="{{ url('trips') }}">Trips </a></li>
          <li class="active">Add Trip </li>
        </ol>
      </div> <!-- /.content-header -->

      

      <div class="row">

        <div class="col-md-10 col-sm-8">

          <div class="portlet">

            <div class="portlet-content">

                {!! Form::open(['url' => 'trips', 'class' => 'form-horizontal']) !!}

                <input type="hidden" name="operator_id" value="1">


            <div class="form-group {{ $errors->has('source_park') ? 'has-error' : ''}}">
                {!! Form::label('source_park', 'Source Park: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    <select name="source_park" class="form-control">
                        <option value="">Choose Park</option>
                        <option value="{{ $park->id }}">{{ $park->name }}</option>
                    </select>
                {!! $errors->first('source_park', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>


             <div class="form-group {{ $errors->has('dest_park') ? 'has-error' : ''}}">
                {!! Form::label('dest_park', 'Destination Park: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    <select name="dest_park" class="form-control">
                        <option value="">Choose Park</option>
                        @foreach($all_parks as $park)
                        <option value="{{ $park->id }}">{{ $park->name }}</option>
                        @endforeach
                    </select>
                {!! $errors->first('dest_park', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>
                     

           
            <div class="form-group {{ $errors->has('fare') ? 'has-error' : ''}}">
                {!! Form::label('fare', 'Fare: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::number('fare', null, ['class' => 'form-control']) !!}
                {!! $errors->first('fare', '<span class="parsley-error-list">:message</span>') !!}
                </div>
                
            </div>

            <div class="form-group {{ $errors->has('rt_activate') ? 'has-error' : ''}}">
                {!! Form::label('rt_activate', 'Activate Round Trip? : ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
             <div class="checkbox">
                <label>{!! Form::radio('rt_activate', '1') !!} Yes</label>
            </div>
            <div class="checkbox">
                <label>{!! Form::radio('rt_activate', '0', true) !!} No</label>
            </div>
                {!! $errors->first('rt_activate', '<span class="parsley-error-list">:message</span>') !!}
                </div>
                
            </div>

           
            <div class="form-group {{ $errors->has('rt_fare') ? 'has-error' : ''}}">
                {!! Form::label('rt_fare', 'Round Trip Fare: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::number('rt_fare', null, ['class' => 'form-control']) !!}
                {!! $errors->first('rt_fare', '<span class="parsley-error-list">:message</span>') !!}
                </div>
                
            </div>





             <div class="form-group {{ $errors->has('ac') ? 'has-error' : ''}}">
                {!! Form::label('is_intl_trip', 'Is International Trip?: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
             <div class="checkbox">
                <label>{!! Form::radio('is_intl_trip', '1') !!} Yes</label>
            </div>
            <div class="checkbox">
                <label>{!! Form::radio('is_intl_trip', '0', true) !!} No</label>
            </div>
                {!! $errors->first('is_intl_trip', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>

            <div class="intl_div" id="intl_div">
            <hr>  
             <h3> International Rates</h3>
             <div class="form-group {{ $errors->has('virgin_passport_fare') ? 'has-error' : ''}}">
                {!! Form::label('virgin_passport_fare', 'Virgin Passport Fare:', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                     {!! Form::text('virgin_passport_fare',  0.00, ['class' => 'form-control', 'id'=>"vPass"] ) !!}
                     <p class="help-block">Leave blank or 0 is not applicable.</p>
                    {!! $errors->first('virgin_passport_fare', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('round_trip_virgin_passport_fare') ? 'has-error' : ''}}">
                {!! Form::label('round_trip_virgin_passport_fare', 'Virgin Passport Round Trip Fare:', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                     {!! Form::text('round_trip_virgin_passport_fare',  0.00, ['class' => 'form-control', 'id'=>"vPass"] ) !!}
                     <p class="help-block">Leave blank or 0 is not applicable.</p>
                    {!! $errors->first('round_trip_virgin_passport_fare', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('no_passport_fare') ? 'has-error' : ''}}">
                {!! Form::label('no_passport_fare', 'Id Card/ No Passport Fare:', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                     {!! Form::text('no_passport_fare',  0.00, ['class' => 'form-control', 'id'=>"noPass"] ) !!}
                     <p class="help-block">Leave blank or 0 is not applicable.</p>
                    {!! $errors->first('no_passport_fare', '<p class="help-block">:message</p>') !!}
                </div>
            </div>

            <div class="form-group {{ $errors->has('round_trip_no_passport_fare') ? 'has-error' : ''}}">
                {!! Form::label('round_trip_no_passport_fare', 'Id Card/ No Passport Round Trip Fare:', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                     {!! Form::text('round_trip_no_passport_fare',  0.00, ['class' => 'form-control', 'id'=>"noPass"] ) !!}
                     <p class="help-block">Leave blank or 0 is not applicable.</p>
                    {!! $errors->first('round_trip_no_passport_fare', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
             <hr>
            </div>







           <!--  <div class="form-group {{ $errors->has('departure_time') ? 'has-error' : ''}}">
                {!! \Html::decode(Form::label('departure_time', 'Departure Time: <br/><small>Optional</small>', ['class' => 'col-sm-3 control-label'])); !!}



                <div class="col-sm-6 input-group bootstrap-datepicker">
                <input id="tp-ex-1" type="text" class="form-control" name="departure_time" value="">
                    <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                   
                   

                {!! $errors->first('departure_time', '<span class="parsley-error-list">:message</span>') !!}
                </div>
                <div class="col-sm-3 input-group">

                </div>
                
            </div> ->

            <!-- <div class="form-group {{ $errors->has('trip_transit') ? 'has-error' : ''}}">
                {!! Form::label('trip_transit', 'Trip Transit: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('trip_transit', null, ['class' => 'form-control', 'id'=>'tripTransit']) !!}
                {!! $errors->first('trip_transit', '<span class="parsley-error-list">:message</span>') !!}
                (First Bus, Second Bus, etc)
                </div>
            </div> -->

            <div class="form-group {{ $errors->has('default_driver_id') ? 'has-error' : ''}}">
                {!! Form::label('bus_id', 'Default Bus', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::select('bus_id', $buses, null, ['class' => 'form-control', 'id'=>"busInput", 'placeholder' => '-Choose-']) !!}
                    {!! $errors->first('bus_id', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            

            

             <div class="form-group {{ $errors->has('no_of_seats') ? 'has-error' : ''}}">
                {!! Form::label('no_of_seats', 'No Of Seats: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('no_of_seats', null, ['class' => 'form-control', 'disabled'=>'disabled', 'id'=>'no_of_seats']) !!}
                {!! $errors->first('no_of_seats', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>

            <div class="form-group {{ $errors->has('numberPlate') ? 'has-error' : ''}}">
                {!! Form::label('number_plate', 'Number Plate: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('numberPlate', null, ['class' => 'form-control', 'disabled'=>'disabled', 'id'=>'numberPlate']) !!}
                {!! $errors->first('no_of_seats', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>

            <div class="form-group {{ $errors->has('bus_type') ? 'has-error' : ''}}">
                {!! Form::label('bus_type', 'Bus Type: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('bus_type', null, ['class' => 'form-control', 'disabled'=>'disabled', 'id'=>'busType']) !!}
                {!! $errors->first('bus_type', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>
             <div class="form-group {{ $errors->has('bus_type') ? 'has-error' : ''}}">
                {!! Form::label('bus_roof', 'Bus Roof: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('bus_roof', null, ['class' => 'form-control', 'disabled'=>'disabled', 'id'=>'bus_roof']) !!}
                {!! $errors->first('bus_roof', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>

            <div class="form-group {{ $errors->has('vin') ? 'has-error' : ''}}">
                {!! Form::label('vin', 'VIN: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('vin', null, ['class' => 'form-control', 'disabled'=>'disabled', 'id'=>'vin']) !!}
                {!! $errors->first('vin', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>

            <div class="form-group {{ $errors->has('default_driver_id') ? 'has-error' : ''}}">
                {!! Form::label('driver_id', 'Default Driver', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::select('driver_id', $drivers, null, ['class' => 'form-control', 'id'=>"driverSelect", 'placeholder' => '-Choose-']) !!}
                    {!! $errors->first('driver_id', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
             <div class="form-group {{ $errors->has('ac') ? 'has-error' : ''}}">
                {!! Form::label('ac', 'Ac: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
             <div class="checkbox">
                <label>{!! Form::radio('ac', '1') !!} Yes</label>
            </div>
            <div class="checkbox">
                <label>{!! Form::radio('ac', '0', true) !!} No</label>
            </div>
                {!! $errors->first('ac', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('passport') ? 'has-error' : ''}}">
                {!! Form::label('passport', 'Passport: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                                <div class="checkbox">
                <label>{!! Form::radio('passport', '1') !!} Yes</label>
            </div>
            <div class="checkbox">
                <label>{!! Form::radio('passport', '0', true) !!} No</label>
            </div>
                {!! $errors->first('passport', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>
            
            <!--<div class="form-group {{ $errors->has('tv') ? 'has-error' : ''}}">
                {!! Form::label('tv', 'Tv: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                                <div class="checkbox">
                <label>{!! Form::radio('tv', '1') !!} Yes</label>
            </div>
            <div class="checkbox">
                <label>{!! Form::radio('tv', '0', true) !!} No</label>
            </div>
                {!! $errors->first('tv', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>
            

           
            

            <div class="form-group {{ $errors->has('insurance') ? 'has-error' : ''}}">
                {!! Form::label('insurance', 'Insurance: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                                <div class="checkbox">
                <label>{!! Form::radio('insurance', '1') !!} Yes</label>
            </div>
            <div class="checkbox">
                <label>{!! Form::radio('insurance', '0', true) !!} No</label>
            </div>
                {!! $errors->first('insurance', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('passport') ? 'has-error' : ''}}">
                {!! Form::label('passport', 'Passport: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                                <div class="checkbox">
                <label>{!! Form::radio('passport', '1') !!} Yes</label>
            </div>
            <div class="checkbox">
                <label>{!! Form::radio('passport', '0', true) !!} No</label>
            </div>
                {!! $errors->first('passport', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('tv') ? 'has-error' : ''}}">
                {!! Form::label('tv', 'Tv: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                                <div class="checkbox">
                <label>{!! Form::radio('tv', '1') !!} Yes</label>
            </div>
            <div class="checkbox">
                <label>{!! Form::radio('tv', '0', true) !!} No</label>
            </div>
                {!! $errors->first('tv', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>
            
 -->
 
 {{ Form::hidden('active', '1') }}
           
              {{-- <div class="form-group {{ $errors->has('active') ? 'has-error' : ''}}">
                {!! Form::label('active', 'Active: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                                <div class="checkbox">
                <label>{!! Form::radio('active', '1', true) !!} Yes</label>
            </div>
         <div class="checkbox">
                <label>{!! Form::radio('active', '0') !!} No</label>
            </div> 
                {!! $errors->first('active', '<span class="parsley-error-list">:message</span>') !!}
                </div>
                
            </div> --}}



           
    

                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-3">
                        {!! Form::submit('Create', ['class' => 'btn btn-primary form-control']) !!}
                    </div>
                </div>
                {!! Form::close() !!}

                @if ($errors->any())
                    <ul class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                @endif
             </div> <!-- /.portlet-content -->

          </div> <!-- /.portlet -->

        

        </div> <!-- /.col -->

        <div class="col-md-2 col-sm-4">

            <ul id="myTab" class="nav nav-pills nav-stacked">
            <li class="active">
              <a href="{{ url('trips') }}">
                <i class="fa fa-bars"></i> 
                List Trips
              </a>
            </li>
          </ul>

        </div>

      </div> <!-- /.row -->  

      <script type="text/javascript">
        $(function () {

            $("#busInput").on('change', function(){
                console.log($("#busInput").val());
                var bus_id = $("#busInput").val();
                 if(bus_id !="")
                  {
                       // use phone value to perform ajax request
                        $.ajax({
                                url: '{{ route("get-bus-details") }}',
                                dataType: 'text',
                                type: 'GET',
                                contentType: 'application/x-www-form-urlencoded',
                                data: "bus_id="+bus_id,
                                success: function (resp, textStatus, jQxhr)
                                {
                                   var bus = $.parseJSON(resp);   
                                   console.log(bus.number_plate) ;
                                   // $("#driverSelect option[value='"+  +"']").prop('selected', true);
                                   $("#driverSelect").val(bus.default_driver_id);
                                   $("#numberPlate").val(bus.number_plate);
                                   $("#vin").val(bus.vin);
                                   $("#busType").val(bus.bus_type.name);
                                   $("#no_of_seats").val(bus.no_of_seats);
                                   $("#bus_roof").val(bus.bus_roof);
                                  // var rsltsArr = rslts.split("|");
                                  //   // DISPLAY THOSE RESULTS
                                  //   $("#contact_name").val(rsltsArr[0]);
                                  //   $("#contact_email").val(rsltsArr[1]);
                                  //   $("#contact_address").val(rsltsArr[2]);
                                  //   $("#next_of_kin").val(rsltsArr[3]);
                                  //   $("#next_of_kin_phone").val(rsltsArr[4]);  
                                },
                                error: function (jqXhr,textStatus, errorThrown)
                                {
                                    console.log(errorThrown);
                                }
                  });
                }
            });

        });
          
      </script> 


  <script>
  $(document).ready(function(){
    $("#intl_div").hide();
    $('input:radio[name="is_intl_trip"]').on('change',function(){
    if($(this).val() == 1)
    {
      $("#intl_div").show(); 
    }
    else
    {
      $("#intl_div").hide();
    }
});

  });
  </script>
    

@endsection