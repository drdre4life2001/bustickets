@extends('layouts.master')

@section('content')



    <div class="content-header">
        <h2 class="content-header-title">Drivers Destinations for {{ date('D, d/m/Y', strtotime($start_date)) }}</h2>
        <ol class="breadcrumb">
          <li><a href="{{ url('/') }}">Dashboard</a></li>
          <li class="active">Driver's Destination Report </li>
        </ol>
      </div> <!-- /.content-header -->

      

      <div class="row">

        <div class="col-md-12 col-sm-12">


        <h4 class="heading-inline pull-left">
          &nbsp;&nbsp;<small>Choose Park</small>
          &nbsp;&nbsp;</h4>
              <div class="btn-group pull-left">
                  <div class="form-group">
                    <select id="park-input" class="form-control" onchange="refreshReport();">
                      <option value="0" @if(empty($park_id)) selected="selected" @endif > -ALL- </option>
                      @foreach($parks as $key => $value)
                        <option value="{{ $key }}" @if($park_id == $key) selected="selected" @endif>{{ $value }}</option>
                      @endforeach
                      
                    </select>
                  </div>

                  
              </div>


              <div class="btn-group pull-right">
                  <div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
                      <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
                      <span></span> <b class="caret"></b>
                  </div>
              </div>
              <h4 class="heading-inline pull-right">
          &nbsp;&nbsp;<small>Choose date</small>
          &nbsp;&nbsp;</h4>

              <br/><br/><br/>


             

              <a href="{{ url('trip-schedules') }}" class="btn btn-primary pull-right" style="margin-left:10px;" onclick="return confirm('This will populate all trips with default buses or no drivers for the selected date.')" >View Trip Schedules</a> &nbsp;&nbsp;
              
              
              <br/><br/>



          <div class="portlet">

            <div class="portlet-content">           


           
              <div class="table-responsive">

              <table 
                class="table table-striped table-bordered table-hover table-highlight table-checkable" 
                data-provide="datatable" 
                data-display-rows="10"
                data-info="true"
                data-search="true"
                data-length-change="true"
                data-paginate="true"
              >
                  <thead>
                    <tr>
                      
                      <th>Driver</th>
                      <th>Bus</th>
                      <th>Transit</th>
                      <th>From</th>
                      <th>Destination</th>
                      
                      
                    </tr>
                  </thead>
                  <tbody>
                    {{-- */$x=0; $tf =0; $de=0; $fuel=0; $oe = 0; /* --}}
                    @foreach($dailytrips as $item)
                        {{-- */

                          
                          


                          /* --}}
                        <tr>
                            <td>
                              {{ (!empty($item->driver))?$item->driver->name:'' }}

                              
                              
                            </td>
                            <td>

                              {{ (!empty($item->bus))?$item->bus->number_plate:'' }}

                              
                            </td>
                            <td>

                              {{ (!empty($item->trip_transit))?$item->trip_transit:"First Bus" }}

                              
                            </td>

                            <td>
                              {{ $item->trip->sourcepark->name }}
                            </td>

                            <td>

                              @for($i=0; $i < count($item->trip['subTrips']); $i++) 
                                {{ $item->trip['subTrips'][$i]['destpark']['name'] }}<br/> 
                              @endfor

                              
                            </td>
                            
                            
                            
                        </tr>




                    @endforeach
                  </tbody>
                </table>


                

              </div> <!-- /.table-responsive -->

              


              

            </div> <!-- /.portlet-content -->

          </div> <!-- /.portlet -->


          

        

        </div> <!-- /.col -->

      

      </div> <!-- /.row -->   


 <!-- setting totals on top -->
                <script type="text/javascript">

                  $(function() {
                    $("#totalRev").html("&#8358;{{ number_format($tf, 2) }}");
                    $("#totalExp").html("&#8358;{{ number_format(($de + $fuel + $oe), 2) }}");
                    $("#netRev").html("&#8358;{{ number_format(($tf - $de- $fuel - $oe), 2) }}");
                    $("#totalTrips").html("{{ number_format(count($dailytrips)) }}");
                  });
                </script>

<script type="text/javascript">

function updateUrl(name, value){
            window.location.search = jQuery.query.set(name, value);

            return false;
       } 

function refreshReport(){

  console.log($('#select-input').val());
  window.location = '{{ url("driver-dest-report") }}/'+$('#park-input').val();


}


$(function() {

    function cb(start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    }
    cb(moment("{{ $start_date }}"), moment("{{ $end_date }}"));

    $('#reportrange').daterangepicker({
        ranges: {
           'Next Tomorrow': [moment().add(2, 'days'), moment().add(2, 'days')],
           'Tomorrow': [moment().add(1, 'days'), moment().add(1, 'days')],
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cb);

    $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
        console.log(picker.startDate.format('YYYY-MM-DD'));
        console.log(picker.endDate.format('YYYY-MM-DD'));
        
        updateUrl('daterange', picker.startDate.format('YYYY-MM-DD')+' '+picker.endDate.format('YYYY-MM-DD'));
    });

});
</script>





@endsection
