@extends('layouts.master')

@section('content')
<script>
$(document).ready(function()
{
   //sortData();
});
function printDiv(divName) {
     var printContents = document.getElementById(divName).innerHTML;
     var originalContents = document.body.innerHTML;

     document.body.innerHTML = printContents;

     window.print();

     document.body.innerHTML = originalContents;
}

 // Table data sorting starts....
        function sortData(){
            //alert();
            var tableData = document.getElementById('manifest').getElementsByTagName('tbody').item(0);
            var rowData = tableData.getElementsByTagName('tr');            
            for(var i = 0; i < rowData.length - 1; i++){
                for(var j = 0; j < rowData.length - (i + 1); j++){
                             if(parseInt(rowData.item(j).getElementsByTagName('td').item(0).innerHTML) > parseInt(rowData.item(j+1).getElementsByTagName('td').item(0).innerHTML)){
                        tableData.insertBefore(rowData.item(j+1),rowData.item(j));
                    }
                }
            }
        }
</script>
    <div class="content-header">
        <!--h2 class="content-header-title">Trip Manifest </h2-->
        <ol class="breadcrumb">
          <li><a href="{{ url('/') }}">Dashboard</a></li>
          <li><a href="{{ url('/daily-trips') }}">Daily trips</a></li>
          <li class="active">Manifest </li>
        </ol>
      </div> <!-- /.content-header -->

      

      <div class="row">


        <div class="col-md-10">

        <div class="portlet">

            <div class="portlet-content" id ="print-area-1">
              <!-- <p class="pull-right">EFOSA EXPRESS LTD <small>RC120517</small>  </p> --> 
            <center><img src ="{{ asset('logos/'.session('operator')->img)}}" }}" height="50px;" width="115px;"><br>
              <b>VEHICLE PASSENGERS MANIFEST</b>
            </center>
             
            
            <center>
            <table class="table" align="center" style="font-size:10px;">
            <tr>
             <td width="50%" align="left"><b>Lagos Office:</b> {{session('operator')->address}} 
             <br><b>Tel:</b> {{session('operator')->phone}}<br>
              <b>Email:</b> {{ session('operator')->email }}<br><b>Website:</b> {{ session('operator')->website }}</td>
            
            </tr>
            <tr>
           <td width="100%" colspan="2" align="justify">
             
              <b>MAKE OF VEHICLE: </b>{{ $dailytrip->bus->bus_type->name }}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <b>TYPE OF VEHICLE: </b>{{ $dailytrip->bus->bus_type->name }}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <b>COLOUR:</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <b>NUMBER PLATE: </b>{{ $dailytrip->bus->number_plate }}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <b>ENGINE NO:</b>{{ $dailytrip->bus->engine_number }}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <b>CHASSIS NO:</b>{{ $dailytrip->bus->chassis_number }}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <b>DEPARTURE POINT: </b> {{ $dailytrip->trip->sourcepark->name }}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
            <b>DESTINATION: </b>{{ $dailytrip->trip->destpark->name }}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <b>DEPARTURE TIME: </b> {{ $dailytrip->trip->departure_time }}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
            <b>ARRIVAL TIME:</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <b>DATE:</b> {{date('d-m-y', strtotime($dailytrip->trip_date))}}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
            </td>
            </tr>
            </table>
            </center>
            
           
      
          <div>
          <table class="table table-bordered" id="manifest" style="font-size:10px;line-height: 0.9em;">
            <thead>
              <tr>
              <th>S/N.</th>
                <th>Seat No.</th>
                <th>PASSENGER'S<br/> NAME</th>
                <th>NATIONALITY</th>
                <th>PASSPORT<br/> NUMBER</th>
                <th>PLACE <br/> ISSUED</th>
                <th>DATE <br/> ISSUED</th>
                <th>EXPIRY <br/> DATE</th>
                <th>OCCUPATION</th>
                <th>PHONE NO</th>
               {{-- <th>NEXT OF KIN <br/> NAME</th> --}}
                <th>NEXT OF KIN <br/> PHONE</th>
                <th>DESTINATION</th>
                {{-- <th>ADDRESS</th> --}}
                
              </tr>
            </thead>  
            <tbody>
            <div align ="center" style="font-size: 10px;">
              @foreach($dailytrip->bookings as $b)
                
              <?php 
              if(isset($destAmount[$b->trip->destpark->name])){
			     $destAmount[$b->trip->destpark->name] += $b->final_cost; 
              }else{
			   	 $destAmount[$b->trip->destpark->name] = $b->final_cost;
			   }
               
                //dd($destAmount);

              if(isset($destCount[$b->trip->destpark->name]))
                 $destCount[$b->trip->destpark->name] += $b->passenger_count; 
               else
               $destCount[$b->trip->destpark->name] = $b->passenger_count; 
              ?> 
             @endforeach
             
             @if(isset($destAmount))
	             @foreach($destAmount as $k=>$v)
	                {{$k}} - ({{ $destCount[$k] }} pax) &nbsp;&nbsp;
	             @endforeach
             @endif
             </div>
                {{-- */$x=0; $tf =0; $de=0; $fuel=0; $oe = 0; /* --}}
             @foreach($passengerObj as $pass)
                {{-- */ $x++; /* --}}
                <tr>
                <td> {{ $x}}  </td>
                <td> {{ $pass->seat_no}}  </td>
                <td>{{ $pass->contact_name }} [{{ ucfirst(substr($pass->gender,0,1)) }}] </td>  
                <td>{{ $pass->nationality }} </td>    
                <td>{{ $pass->passport_no }}</td>
                <td>{{ $pass->passport_place_issued }}</td>
                <td>{{ $pass->passport_date_issued }}</td>
                <td>{{ $pass->passport_expiry_date }}</td>
                <td>{{ $pass->occupation }}</td>
                <td>{{ $pass->contact_phone }}</td>
                {{--<td>{{ $pass->next_of_kin }}</td> --}}
                <td>{{ $pass->next_of_kin_phone }}</td>
                <td>{{ $pass->destPark }}</td>   
                {{--<td>{{ $pass->contact_address }}</td> --}}
                
                </tr>
                @if(isset($pass->children))
                    @foreach($pass->children as $child)
                 {{-- */ $x++; /* --}}
                        <tr>
                            <td> {{ $x}}  </td>
                            <td>  </td>
                            <td>{{ $child->contact_name }} [{{ ucfirst(substr($child->gender,0,1)) }}] @if(($child->age >= $excludePassenger->age_one) && ($child->age <= $excludePassenger->age_two)) (Infant) @endif</td> 
                            <td>{{ $child->nationality }}</td>
                            <td>{{ $child->passport_no }}</td>
			                <td>{{ $child->passport_place_issued }}</td>
			                <td>{{ $child->passport_date_issued }}</td>
			                <td>{{ $child->passport_expiry_date }}</td>
                            <td>{{ $child->occupation }}</td>
                            <td>{{ $child->contact_phone }}</td>
                            {{--<td>{{ $child->next_of_kin }}</td> --}}
                            <td>{{ $child->next_of_kin_phone }}</td>
                            {{--<td>{{ $child->contact_address }}</td> --}}
                            <td>{{ $child->destPark }}</td>
                            
                            </tr>
                    @endforeach
                @endif
             @endforeach
            </tbody>
          </table>
          </div> <!-- /.table-resonsive -->


        
          <table width="100%" style="font-size:9px;">
            <tr>
                {{-- */$x=0; $extlug =0;$sum=0; /* --}}
                      
                          @foreach($dailytrip->bookings as $b)
                      {{-- */
                                        $extlug += $b->extra_luggages->sum('amount');
                       /* --}}
                                    @endforeach
                @foreach($dailytrip->expenses as $e)
                {{-- */
                 $sum+= $e->amount;
                /* --}}
              @endforeach
                                   
          <td width="33%" align="left"><b>Total Amount: </b> &#8358;{{ number_format($dailytrip->total_fare+$extlug- ($sum)) }}</td><br>-->
            
              @if(isset($dailytrip->driver->name))  
            <td width="40%"    align="left"> 
           <b>Driver's Name: </b>{{ $dailytrip->driver->name }} </td>
            @endif
            
            @if(isset($dailytrip->driver->name))  
            <td width="40%" align="left"><b>Driver's Name</b> ______________________</td>
            @endif
            </tr>
            <tr>
            <td width="40%" align="left"><b>Driver's Phone</b> ______________________</td>
            </tr>
            <tr>
            <td width="33%" align="left"><b>Driver's Next of kin</b> __________________</td>
            <td width="33%" align="left"><b>Driver's Next of kin phone</b> _____________________</td>
            <td width="33%" align="left"><b>Driver's Signature</b> ____________________</td>
            </tr>  



            
          </table>
         

          
          

         

          </div>
        </div>

        </div> <!-- /.col -->


        <div class="col-md-2 col-sidebar-right">

            <p><a onclick="printDiv('print-area-1')" id="" class="btn btn-info"> <i class="fa fa-print"></i> Print Manifest <br /></a></p>

          <p><a class="btn btn-primary" href="{{ url('manifest/'.$manifest_id.'/local') }}"> <i class="fa fa-print"></i> Local Manifest <br /></a></p>
          <p><a class="btn btn-primary" href="{{ url('manifest/'.$manifest_id.'/international') }}"> <i class="fa fa-print"></i> Intl Manifest <br /></a></p>
          <!-- <p><a href="javascript:;" class="btn btn-lg btn-tertiary btn-block">Close Invoice</a></p> -->


          <br><br>

            


          <br />


          
        </div> <!-- /.col -->


      </div> <!-- /.row -->




@endsection
