@extends('layouts.master')

@section('content')

    <script type="text/javascript">


        // script to send sms
        function sendSMS(tripid) {

            $.ajax({
                url: '{{ route("get-trip-dets") }}',
                dataType: 'text',
                type: 'GET',
                contentType: 'application/x-www-form-urlencoded',
                data: {"id": tripid, "date": "{{ $start_date }}"},
                success: function (rslts, textStatus, jQxhr) {
                    if (rslts == 200) {
                        $("#updateStatusDiv" + tripid).addClass('alert alert-success').html('SMS sent');
                        $("#updateStatusDiv" + tripid).delay(5000).fadeOut();
                    }
                },
                error: function (jqXhr, textStatus, errorThrown) {
                    $("#updateStatusDiv").addClass('alert alert-danger').html('SMS not sent');
                    $("#updateStatusDiv").delay(5000).fadeOut();
                }
            });
        }
    </script>

    <div class="content-header">
        <h2 class="content-header-title">Trip Schedules</h2>
        <ol class="breadcrumb">
            <li><a href="{{ url('/') }}">Dashboard</a></li>
            <li><a href="{{ url('/trips/create') }}">Add a Trip</a></li>
            <li><a href="{{ url('quick-book') }}">Quick Book</a></li>
            <li class="active">Trip Schedules</li>
        </ol>
    </div> <!-- /.content-header -->



    <div class="row">

        <div class="col-md-12 col-sm-12">


            <h4 class="heading-inline pull-left">
                &nbsp;&nbsp;
                <small>Choose Park</small>
                &nbsp;&nbsp;</h4>
            <div class="btn-group pull-left">
                <div class="form-group">
                    <select id="park-input" class="form-control" onchange="refreshReport();">
                        <option value="0" @if(empty($park_id)) selected="selected" @endif > -ALL-</option>
                        @foreach($parks as $key => $value)
                            <option value="{{ $key }}"
                                    @if($park_id == $key) selected="selected" @endif>{{ $value }}</option>
                        @endforeach

                    </select>
                </div>


            </div>


            <div class="btn-group pull-right">
                <div id="reportrange" class="pull-right"
                     style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
                    <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
                    <span></span> <b class="caret"></b>
                </div>
            </div>
            <h4 class="heading-inline pull-right">
                &nbsp;&nbsp;
                <small>Choose date</small>
                &nbsp;&nbsp;</h4>

            <br/><br/><br/>


            <span class="pull-left">
                You have a total of <strong>{{ $trips_count }} active Trips</strong> and <strong><span
                            id="schCount">0</span> Trip Schedules</strong> for the selected date.
                <br/>
              </span>


            <a href="{{ url('driver-dest-report') }}" class="btn btn-default pull-right">View Driver Destination
                Report</a>

            <br/><br/>


            <div class="portlet">

                <div class="portlet-content">

                    @if(Session::has('error'))
                        <div class="alert alert-danger">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            {{ Session::get('error') }}
                        </div>

                    @endif

                    <div class="table-responsive">

                        <table
                                class="table table-striped table-bordered table-hover table-highlight table-checkable"
                                data-provide="datatable"
                                data-display-rows="100"
                                data-info="true"
                                data-search="true"
                                data-length-change="true"
                                data-paginate="true"
                        >
                            <thead>
                            <tr>

                                <th>From</th>
                                <th>Default Bus<br/>
                                    <small>[Driver]</small>
                                </th>
                                <th>Date</th>
                                <th width="10%">No of Buses</th>
                                <th width="30%">Buses</th>
                            </tr>
                            </thead>
                            <tbody>
                            {{-- */$x=0; $i =0; /* --}}
                            @if(!empty($trips))
                                @foreach($trips as $item)

                                            {{-- */ $x += count($item->dailyTrips); /* --}}

                                            <tr>
                                                <td>
                                                    <strong>From:</strong> &nbsp;&nbsp;&nbsp;{{ $item->sourcepark->name }}<br/>

                                                    <strong>To:</strong> &nbsp;
                                                    {{ $item->destpark->name }}
                                                    <br>
                                                    <strong>Fare:</strong> &nbsp;
                                                    {{ $item->fare }}

                                                    <br>
                                                    <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#myModal{{$item->id}}"> View Stops</button>

                                                    <div id="myModal{{$item->id}}" class="modal fade" role="dialog">
                                                        <div class="modal-dialog">

                                                            <!-- Modal content-->
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                    <h4 class="modal-title">Stops</h4>
                                                                </div>
                                                                <div class="modal-body">
                                                                    &nbsp;
                                                                    @foreach($item->subTrips as $st)
                                                                        @if($st->destpark)
                                                                            &nbsp;&nbsp;&nbsp;<strong>{{ $st->destpark->name }}</strong><br/>

                                                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                            <small>One way: N{{ number_format($st->fare) }}

                                                                                @if($st->round_trip_status)
                                                                                    <br/>
                                                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;
                                                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                                    &nbsp;&nbsp;Round trip:
                                                                                    N{{ number_format($st->round_trip_fare) }}
                                                                                @endif
                                                                                <br/>
                                                                            </small>
                                                                        @endif
                                                                    @endforeach


                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>


                                                    <br/>


                                        <!-- <strong>Departure Time:</strong> {{ date('h:iA', strtotime($item->departure_time)) }}<br/> -->

                                        </td>

                                        <td>
                                            {{ (!empty($item->bus))?$item->bus->number_plate:'' }}<br/>
                                            <small>[{{ (!empty($item->driver))?$item->driver->name:"" }}]</small>

                                        </td>


                                        <td>

                                            {{ date('D, d/m/Y', strtotime($start_date)) }}

                                        </td>
                                        <td>

                                            {{--{{ count($item->dailyTrips) }}<br/>--}}
                                            <a href="#" class="btn btn-primary" data-toggle="modal"
                                               data-target="#busModal{{$item->id}}">Add a bus</a>
                                            <br/><br/>
                                            <div id="updateStatusDiv{{$item->id}}"></div>
                                            @if(count($item->dailyTrips) > 0)
                                                <button class="btn btn-primary" id="sendSMS" name="sendSMS"
                                                        onclick="sendSMS({{$item->id}})">Send SMS</a>
                                            @endif
                                        </td>

                                        <td>

                                            <?php $ii = 0; ?>

                                            @foreach($item->dailyTrips as $dt)



                                                {{ ++$ii }}. <strong>{{ $dt->bus->number_plate }} </strong>
                                                <a href="#" class="" data-toggle="modal"
                                                   data-target="#busEditModal{{$dt->id}}">edit</a>&nbsp;
                                                @if(count($dt->bookings()->get()) == 0 )
                                                    | {!! Form::open([
                                    'method'=>'DELETE',
                                    'url' => ['daily-trips', $dt->id],
                                    'style' => 'display:inline'
                                ]) !!}
                                                    {!! Form::button('<i class="fa fa-times"></i>', ['class' => 'btn btn-danger btn-xs', 'escape'=>false,'title'=>'Delete', 'type'=>'submit', 'onclick'=>'return confirm("Are you sure you want to remove this trip schedule?");']) !!}
                                                    {!! Form::close() !!}

                                                @endif
                                            <!--a href="{{ route('remove-bus-from-schedule', ['dtid' =>$dt->id, 'date'=>$start_date ])}}"> remove </a-->
                                                <br/>
                                                <small>[{{ $dt->driver->name or '' }}]
                                                    -> {{ $dt->trip_transit }}</small><br/>



                                                <div class="modal fade" id="busEditModal{{$dt->id}}" tabindex="-1"
                                                     role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal"
                                                                        aria-hidden="true">
                                                                    &times;
                                                                </button>
                                                                <h4 class="modal-title" id="myModalLabel">
                                                                    Edit Schedule {{ $dt->id }}
                                                                </h4>
                                                            </div>
                                                            {!! Form::model($item, [
                                                                       'method' => 'POST',
                                                                       'url' => ['update-trip-schedule', $dt->id],
                                                                       'class' => 'form-horizontal'
                                                                   ]) !!}
                                                            <div id="msg"></div>
                                                            <input type="hidden" name="trip_id" id="trip_id"
                                                                   value="{{ $item->id }}">
                                                            <input type="hidden" name="trip_date" id="trip_date"
                                                                   value="{{ $start_date }}">
                                                            @if(!empty($item->bus->bus_type->no_of_seats))
                                                                <input type="hidden" name="total_seats" id="total_seats"
                                                                       value="{{ $item->bus->bus_type->no_of_seats }}">
                                                            @endif
                                                            <div class="modal-body" id="contentArea">
                                                                <input type="hidden" name="_token" id="token"
                                                                       value="<?php echo csrf_token(); ?>">


                                                                <div class="form-group {{ $errors->has('bus_id') ? 'has-error' : ''}}">
                                                                    {!! Form::label('bus_id', 'Bus', ['class' => 'col-sm-3 control-label']) !!}
                                                                    <div class="col-sm-8">
                                                                        {!! Form::select('bus_id', $buses, $dt->bus_id, ['class' => 'form-control','required' => 'required', 'onChange'=>'chooseDefaultDriver(this, "driverEdit'.$dt->id.'")']) !!}
                                                                        {!! $errors->first('bus_id', '<p class="help-block">:message</p>') !!}
                                                                    </div>
                                                                </div>
                                                                <br style="clear:both;"/>
                                                                <div class="form-group {{ $errors->has('driver_id') ? 'has-error' : ''}}">
                                                                    {!! Form::label('bus_id', 'Driver', ['class' => 'col-sm-3 control-label']) !!}
                                                                    <div class="col-sm-8">
                                                                        {!! Form::select('driver_id', $drivers, $dt->driver_id, ['class' => 'form-control','required' => 'required', 'id'=>'driverEdit'.$dt->id]) !!}
                                                                        {!! $errors->first('bus_id', '<p class="help-block">:message</p>') !!}
                                                                    </div>
                                                                </div>
                                                            <!-- <br style="clear:both;" />
            <div class="form-group {{ $errors->has('bus_id') ? 'has-error' : ''}}">
                {!! \Html::decode(Form::label('departure_time', 'Departure Time: <br/><small>Optional</small>', ['class' => 'col-sm-3 control-label'])); !!}
                                                                    <div class="col-sm-8">
                                                                        <div class="col-sm-6 input-group bootstrap-timepicker">
                                                                          <input  type="text" class="form-control" name="departure_time" value="" id="tp-ex-2">
                                                                              <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>


                                                                          {!! $errors->first('departure_time', '<span class="parsley-error-list">:message</span>') !!}
                                                                    </div>
                                                              </div>
                                                          </div> -->
                                                                <br style="clear:both;"/>
                                                                <div class="form-group {{ $errors->has('bus_id') ? 'has-error' : ''}}">
                                                                    {!! Form::label('bus_id', 'Trip Transit', ['class' => 'col-sm-3 control-label']) !!}
                                                                    <div class="col-sm-8">

                                                                    <!-- <input type="text" name="trip_transit" value="{{ $dt->trip_transit }}" class="form-control" required> -->
                                                                    {!! Form::select('trip_transit', $transits, $dt->trip_transit , ['class' => 'form-control','required' => 'required']) !!}
                                                                    <!-- <span>e.g. First bus, second bus</span> -->
                                                                        {!! $errors->first('bus_id', '<p class="help-block">:message</p>') !!}
                                                                    </div>
                                                                </div>
                                                                <br style="clear:both;"/>


                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-default"
                                                                        data-dismiss="modal">
                                                                    Cancel
                                                                </button>
                                                                <button type="submit" id="NoteSub"
                                                                        class="btn btn-primary">
                                                                    Assign Bus
                                                                </button>
                                                                </form>

                                                            </div>
                                                        </div><!-- /.modal-content -->
                                                    </div><!-- /.modal-dialog -->
                                                </div><!-- /.modal -->


                                                <hr/>

                                            @endforeach

                                        </td>


                                    </tr>

                                    <div class="modal fade" id="busModal{{$item->id}}" tabindex="-1" role="dialog"
                                         aria-labelledby="myModalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal"
                                                            aria-hidden="true">
                                                        &times;
                                                    </button>
                                                    <h4 class="modal-title" id="myModalLabel">
                                                        Add a Bus to this Trip
                                                    </h4>
                                                </div>
                                                {!! Form::model($item, [
                                                           'method' => 'POST',
                                                           'url' => ['update-trip-schedule'],
                                                           'class' => 'form-horizontal'
                                                       ]) !!}

                                                <input type="hidden" name="trip_id" value="{{ $item->id }}">
                                                <input type="hidden" name="trip_date" value="{{ $start_date }}">
                                                <input type="hidden" name="total_seats"
                                                       value="{{ $item->bus->bus_type->no_of_seats }}">

                                                <div class="modal-body" id="contentArea">
                                                    <input type="hidden" name="_token"
                                                           value="<?php echo csrf_token(); ?>">


                                                    <div class="form-group {{ $errors->has('bus_id') ? 'has-error' : ''}}">
                                                        {!! Form::label('bus_id', 'Bus', ['class' => 'col-sm-3 control-label']) !!}
                                                        <div class="col-sm-8">
                                                            {!! Form::select('bus_id', $buses, $item->bus_id, ['class' => 'form-control','required' => 'required', 'onChange'=>'chooseDefaultDriver(this, "driverNew'.$item->id.'")']) !!}
                                                            {!! $errors->first('bus_id', '<p class="help-block">:message</p>') !!}
                                                        </div>
                                                    </div>
                                                    <br style="clear:both;"/>
                                                    <div class="form-group {{ $errors->has('bus_id') ? 'has-error' : ''}}">
                                                        {!! Form::label('bus_id', 'Driver', ['class' => 'col-sm-3 control-label']) !!}
                                                        <div class="col-sm-8">
                                                            {!! Form::select('driver_id', $drivers, isset($item->driver_id) ? $item->driver_id:'', ['class' => 'form-control','required' => 'required', 'id'=>'driverNew'.isset($item->id) ? $item->id : '']) !!}
                                                        </div>
                                                    </div>
                                                <!-- <br style="clear:both;" />
            <div class="form-group {{ $errors->has('bus_id') ? 'has-error' : ''}}">
                {!! Html::decode(Form::label('departure_time', 'Departure Time: <br/><small>Optional</small>', ['class' => 'col-sm-3 control-label'])) !!}
                                                        <div class="col-sm-8">
                                                            <div class="col-sm-6 input-group bootstrap-timepicker">
                                                              <input id="tp-ex-2" type="text" class="form-control" name="departure_time" value="{{ $item->departure_time }}">
                          <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>


                      {!! $errors->first('departure_time', '<span class="parsley-error-list">:message</span>') !!}
                                                        </div>
                                                  </div>
                                              </div> -->
                                                    <br style="clear:both;"/>
                                                    <div class="form-group {{ $errors->has('bus_id') ? 'has-error' : ''}}">
                                                        {!! Form::label('bus_id', 'Trip Transit', ['class' => 'col-sm-3 control-label']) !!}
                                                        <div class="col-sm-8">
                                                            <!-- <input type="text" name="trip_transit" value="" class="form-control" required> -->
                                                        {!! Form::select('trip_transit', $transits, null , ['class' => 'form-control','required' => 'required']) !!}
                                                        <!-- <span>e.g. First bus, second bus</span> -->
                                                            {!! $errors->first('bus_id', '<p class="help-block">:message</p>') !!}
                                                        </div>
                                                    </div>
                                                    <br style="clear:both;"/>


                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">
                                                        Cancel
                                                    </button>
                                                    <button type="submit" id="NoteSub" class="btn btn-primary">
                                                        Assign Bus
                                                    </button>
                                                    </form>

                                                </div>
                                            </div><!-- /.modal-content -->
                                        </div><!-- /.modal-dialog -->
                                    </div><!-- /.modal -->
                                @endforeach
                            @endif
                            </tbody>
                        </table>


                    </div> <!-- /.table-responsive -->


                    <script type="text/javascript">
                        $(function () {

                            $("#schCount").html('{{ $x }}');


                        });
                    </script>


                </div> <!-- /.portlet-content -->

            </div> <!-- /.portlet -->


        </div> <!-- /.col -->


    </div> <!-- /.row -->


    <!-- setting totals on top -->


    <script type="text/javascript">

        function updateUrl(name, value) {
            window.location.search = jQuery.query.set(name, value);

            return false;
        }

        function refreshReport() {

            console.log($('#select-input').val());
            window.location = '{{ url("trip-schedules") }}/' + $('#park-input').val();


        }


        $(function () {

            function cb(start, end) {
                $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            }

            cb(moment("{{ $start_date }}"), moment("{{ $end_date }}"));

            $('#reportrange').daterangepicker({
                ranges: {
                    'Next 4 Days': [moment().add(2, 'days'), moment().add(4, 'days')],
                    'Next 3 Days': [moment().add(2, 'days'), moment().add(3, 'days')],
                    'Next Tomorrow': [moment().add(2, 'days'), moment().add(2, 'days')],
                    'Tomorrow': [moment().add(1, 'days'), moment().add(1, 'days')],
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            }, cb);

            $('#reportrange').on('apply.daterangepicker', function (ev, picker) {
                console.log(picker.startDate.format('YYYY-MM-DD'));
                console.log(picker.endDate.format('YYYY-MM-DD'));

                updateUrl('daterange', picker.startDate.format('YYYY-MM-DD') + ' ' + picker.endDate.format('YYYY-MM-DD'));
            });

        });


        function chooseDefaultDriver(elem, driverElemId) {

            $.ajax({
                url: '{{ url("get-schedule-driver-details") }}',
                type: 'GET',
                contentType: 'application/x-www-form-urlencoded',
                data: "bus_id=" + elem.value,
                success: function (rslts, textStatus, jQxhr) {

                    $("#" + driverElemId).val(rslts);


                },
                error: function (jqXhr, textStatus, errorThrown) {

                }
            });

        }


    </script>





@endsection
