<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Course Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during course API calls for various
    | messages that we need to display to the user.
    |
    */

    'missing-req-params' => 'The following required parameters are missing: :missing-params',
    

];
